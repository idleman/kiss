<?php
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');
$mediaRoot = define_media_root();
global $conf;
if(file_exists(File::dir().SLASH.'disabled.maintenance')){
	header('Location: index.php');
	exit();
}
?>
<!DOCTYPE html>
<html class="maintenance-page">
	<head>
		<title>MAINTENANCE</title>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="img/favicon.png" />
		<link href="css/main.css" rel="stylesheet">
		<!-- Plugin css files -->
		<?php echo Plugin::callCss($mediaRoot,$cacheVersion); ?>
	</head>
	<body>
	<?php if(isset($_['error']) && !empty($_['error'])): ?>
		<div class="alert alert-danger" role="alert">
			<h4 class="alert-heading">ERREUR :</h4>
			<p><?php echo $_['error']; ?></p>
		</div>
	<?php endif; ?>

		<div class="icon"><i class="fas fa-cog fa-spin"></i></div>
		<h1>MAINTENANCE</h1>
		<div class="container-fluid">
			<div class="row w-100 justify-content-md-center">
				<div class="content-block col-md-6 col-sm-12">
					<?php echo html_entity_decode(file_get_contents(File::dir().SLASH.'enabled.maintenance')); ?>
				</div>
			</div>
			<p>Merci de revenir dans quelques instants</p>
			<p><i>L'équipe Technique</i></p>
			<small class="admin-access">
				<a href="index.php?admin_login=1" class="admin-link">- Accès technicien -</a>
			</small>
		</div>
	</body>
</html>