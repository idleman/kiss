# Introduction
Kiss is another Open Source PHP base ERP availaible for all types of projects.

This project was created by @idleman and with the collaboration of sys1 firm and delivered under MIT licence

It comes with a hook system which allows you to customize this tool to infinity while taking advantage of the official modules already in place.

Kiss was designed to be simple and light by default, you only activate the features you need instead of starting from a complex and heavy base, that's why you will have very few features at installation as long as you have not activated the plugins that interest you.

After install the project contain only the followings stuff :

* Basic db authentication
* Basic navigation menu
* Account settings
* Plugin settings
* Log settings
* Firm settings
* Rank/Rights settings (by firm)

Project is delivered with some officials plugins often used in projects and you can enable :

* Client manager
* Contacts directory
* Active directory accounts
* Export/import functions
* Planning
* Documents manager
* Wiki
* Dashboard
* Code factory
* ...