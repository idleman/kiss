<?php
/**
 * Define a user for a  firm.
 * @author valentin carruesco
 * @category Core
 * @license MIT
 */
class UserFirmRank extends Entity{
	public $id,$firm,$user,$rank;
	protected $TABLE_NAME = 'user_firm_rank';
	
	public $entityLabel = 'Rang d\'utilisateur pour un établissement';
	protected $fields = array(
		'id' => array('label'=>'Identifiant', 'type'=>'key'),
		'firm' => array('label'=>'Établissement', 'type'=>'firm','link'=>'class/Firm.class.php'),
		'user' => array('label'=>'Utilisateur', 'type'=>'user'),
		'rank' => array('label'=>'Rang', 'type'=>'rank','link'=>'class/Rank.class.php')
	);

	//Colonnes indexées
	public $indexes = array('firm', 'rank');
}