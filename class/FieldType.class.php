<?php
/**
* Type de champs possibles dans l'application
* @author valentin carruesco
* @category Core
* @license MIT
*/

class FieldType{
	public $label,$slug,$sqlType,$description,$icon,$default,$input;

	public function toArray(){
		return json_decode(json_encode($this), true);
	}

	public function __toString() {
   		return json_encode($this->toArray());
	}

	//traduit une valeur en champ/label/légende HTML en fonction de son type
	public static function toHtml($field,$types=null,$options = array('allowCustomLabel'=>true)){
		if(!isset($types)) $types = self::available();
		if(!isset($field['id'])) $field['id'] = '';

		$type = isset($field['type']) && isset($types[$field['type']]) ? $types[$field['type']] : $types['text'];

		$label = '';
		if(!empty($field['label'])){
			$label = '<label class="mb-0"';
			if(!empty($field['id'])) $label .= 'for="'.$field['id'].'"';
			$label .= '>'.$field['label'].'</label>';
		}

		if(!isset($field['attributes'])) $field['attributes'] = array();
		$field['attributes'] = array_merge($type->default_attributes,$field['attributes']);

		if(empty($field['value']) && !empty($field['default'])) $field['value'] = $field['default'];
		if(!empty($field['placeholder'])) $field['attributes']['placeholder'] = $field['placeholder'];
		if(!empty($field['required'])) $field['attributes']['required'] = $field['required'];

		$func = $type->onInput;
		$inputOptions = array_merge(array('type'=>$type,'label'=>$label), $options);
		$input = $func($field,$inputOptions);

		return array(
			'label' => isset($type->customLabel) && $type->customLabel && $options['allowCustomLabel']  ? '' : $label,
			'legend' => isset($field['legend']) ? $field['legend'] : '',
			'input' => $input,
			'data' => $field
		);
	}

	public static function toForm($fields,$types=null){
		$htmlFields = array();
		if(!isset($types)) $types = self::available();
		uasort ($fields,function($a,$b){
			if(!isset($a['sort'])) $a['sort'] = 100;
			if(!isset($b['sort'])) $b['sort'] = 100;
			return $a['sort'] - $b['sort'];
		});

		foreach ($fields as $key => $field) {
			if(!isset($field['id'])) $field['id'] = $key;
			$htmlFields[] = self::toHtml($field,$types);
		}
		return $htmlFields;
	}

	//liste des types de champs disponibles
	public static function available($key=null){
		$types = array();

		//Texte
		$type_text = new self();
		$type_text->slug = 'text';
		$type_text->label = 'Texte';
		$type_text->sqlType = 'string';
		$type_text->label = 'Texte';
		$type_text->icon = 'fas fa-font';
		$type_text->description = 'Texte court mono ligne';
		$type_text->default = '';
		$type_text->default_attributes = array(
			'class'=>'"form-control"',
			'type'=>'"text"'
		);

		$type_text->filter = array(
			'operators' => array (
				'like' =>array("view" =>'text'),
				'not like' =>array("view" =>'text'),
				'=' =>array("view" =>'text'),
				'!='  =>array("view" =>'text'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		/* Exemples d'utilisation des méthodes de convertion de types

		Type        |     BDD        | onLoad     | onRawDisplay       | onHtmlDisplay             | onInput                                                | fromRawDisplay
		====================================================================================================================================================================
		Date        |    12345       | d/m/y      | d/m/y              | d/m/y                     | <input data-type="date" ... value="{{onLoad}}">        | 12345
		User        |    theo.lecoq  | theo.lecoq | Théo Lecoq         | Théo Lecoq                | <input data-type="user" ... value="{{onLoad}}">        | theo.lecoq
		Dictionary |    6           | 6          | Véhicules > bateau | Véhicules > bateau        | <input data-type="dictionary" ... value="{{onLoad}}"> | 6
		password    |    ==x$yh..    | toto       | toto               | toto                      | <input data-type="password" ... value="{{onLoad}}">    | ==x$yh..
		url         |    http://...  | http://... | http://...         | <a href="http://..."></a> | <input data-type="url" ... value="{{onLoad}}">         | http://..

			onLoad : Utilisé pour converti une valeur de la base en une valeur comprehensible par l'erp et
			ses composants (pas forcement human readable)
			onRawDisplay : Valeur human readable brute sans mise en forme ou html, utilisé par exemple dans les export text/ excel
			fromRawDisplay : utilisé pour convertir la valeur affichée brute en valeur technique BDD  (inverse de onRawDisplay) utilisé apr exemple pour les import depuis excel
			onHtmlDisplay : Valeur human readable avec mise en forme ou html, utilisé pour affichage sur les pages, un parametre decoration peut être mise a true pour avoir des décoration contextuelles (ex : une icone de calendrier devant la date)
			onInput : utilisé pour afficher l'input approprié, cette méthode doit s'appuyer sur le onLoad
			onSave : utilisé pour une transformation de la valeur avant enregistrement en base
		*/
		$type_text->onLoad = function($value,$options = array()){
			return $value;
		};
		$type_text->onRawDisplay = function($value,$options = array()){
			return $value;
		};
		$type_text->onHtmlDisplay = function($value,$options = array()){
			$raw = $options['type']->onRawDisplay;
			$html = $raw($value,$options);
			return $html;
		};

		$type_text->onInput = function($field=array(),$options=array()){

			$attributes = array_merge($options['type']->default_attributes,$field['attributes']);

			if(isset($field['id'])) $field['attributes']['id'] ='"'.$field['id'].'"';

			$html = '';

			$html.= '<input  value="'.(isset($field['value']) ? str_replace('"','&quot;',$field['value']) : '').'"';
			foreach ($field['attributes'] as $key => $value) {
				$html.= ' ';
				$html.= $key;
				if(!empty($value) && is_string($value)) $html.= '='.$value;
				$html.= ' ';
			}

			$html.= '>';
			return $html;
		};
		$types[$type_text->slug] = $type_text;


		//File
		$type_file = new self();
		$type_file->slug = 'file';
		$type_file->label = 'Fichier';
		$type_file->sqlType = '';
		$type_file->default_attributes = array_merge($type_text->default_attributes,array(
			'class'=>'"component-file-default bg-white shadow-sm rounded-sm"',
			'data-type'=>'"file"',
			'data-extension'=>'"jpg,png,bmp,jpeg,gif,svg,webp,docx,xlsx,pptx,msg,eml,pdf,zip,doc,xls,ppt,txt,csv,mp3,wav,mp4,avi,flv"',
			'data-action'=>'{{action}}',
			'data-id'=>'"'.uniqid(rand(0,100)).'"',
			'data-data'=> "'{}'",
		));
		$type_file->settings = array(
			'action'=>array('type'=>'text','default'=>"dynamicform_handle_file",'attributes'=> array('class'=>'"form-control hidden"','disabled'=>true)),
			'extension'=> array('type'=>'text','label'=>'Extensions permises','placeholder'=>'"Séparation par \',\'"','attributes'=> array('class'=>'"form-control"')),
			'size'=> array('type'=>'integer','label'=>'Taille max (octets)','attributes'=> array('class'=>'"form-control"')),
			'limit'=> array('type'=>'integer','label'=>'Nombre max de fichiers','attributes'=> array('class'=>'"form-control"')),
			'storage'=> array('type'=>'filepicker','label'=>'Dossier d\'enregistrement','attributes'=> array('class'=>'"form-control"','data-editable'=>true)),
			'access'=> array(
				'type'=>'list',
				'label'=>'Contrôle des droits et accès à faire sur la section',
				'values'=>function(){
					$scopes = array();
					foreach(Right::availables() as $slug=>$scope)
						$scopes[$slug] = $scope['label'];
					return $scopes;
				},
				'attributes'=> array('class'=>'"form-control"')
			),
			/*'label'=> array('type'=>'text','label'=>'Libellé visible dans la dropzone du composant','placeholder'=>'"Faites glisser vos fichiers ici"','attributes'=> array('class'=>'"form-control"')),
			'readonly'=> array('type'=>'boolean','label'=>'Lecture seule','attributes'=> array('class'=>'"form-control"')),*/
		);


		$type_file->onInput = function($field=array(),$options=array()){
			$attributes = array_merge($options['type']->default_attributes,$field['attributes']);

			if(isset($field['id'])) $field['attributes']['id'] ='"'.$field['id'].'"';

			//gestion des scopes, uid et slug si dynamics
			$data = array();
			//id entité si présente
			$data['id'] = isset($field['options']['uid']) ? $field['options']['uid'] : '';
			//id scope de l'entité si présente
			$data['scope'] = isset($field['options']['scope']) ? $field['options']['scope'] : '';
			//slug du composant
			$data['slug'] = isset($field['slug']) ? $field['slug'] : '';

			//Gestion des data-data depuis le champ (ex: type image en settings globaux)
			if(!empty($field['attributes']) && !empty($field['attributes']['data-data'])){
				$fieldData = json_decode($field['attributes']['data-data'], true);
				$data = !empty($fieldData) ? json_encode(array_merge($data, $fieldData)) : json_encode($data);
			}
			$field['attributes']['data-data'] = $data;

			$html = '';

			$html.= '<input ';
			foreach ($field['attributes'] as $key => $value) {
				$html.= ' ';
				$html.= $key;
				if(!empty($value)) $html.= '='.$value;
				$html.= ' ';
			}

			$html.= '>';
			return $html;
		};

		$type_file->onSave = function($value,$options=array()){

			$path = $options['options']['scope'].SLASH.$options['options']['uid'];
			$limit = 1;
			if(!empty($options['field']->meta)){
				$meta = json_decode($options['field']->meta,true);

				if(!empty($meta['limit']) && $meta['limit']>1) $limit =$meta['limit'] ;
				if(!empty($meta['storage']))
					$path = template($meta['storage'],$options['options'],true);
			}

			File::save_component($options['field']->slug,$path.($limit==1?'':SLASH.$options['field']->slug).SLASH.'{{label}}');

			return null;
		};

		$type_file->onRawDisplay = function($value,$options = array()){
			$value = '';

			$meta = json_decode($options['meta'],true);

			$templateData = array();
			foreach($options as $key=>$option){
				if(!is_string($option)) continue;
				$templateData[$key] = $option;
			}

			$path = File::dir().template($meta['storage'],$templateData,true).'/*';
			foreach (glob($path) as $file) {
				$filePath = str_replace(File::dir(),'',$file);
				$value .= PHP_EOL.basename($file).' ('.ROOT_URL.'action.php?action='.$meta['action'].'&type=download&path='.base64_encode($filePath).')';
			}

			return $value;
		};

		$type_file->onHtmlDisplay = function($value,$options = array()){
			$html = '';

			$meta = json_decode($options['meta'],true);

			$templateData = array();
			foreach($options as $key=>$option){
				if(!is_string($option)) continue;
				$templateData[$key] = $option;
			}

			$path = File::dir().template($meta['storage'],$templateData,true).'/*';
			$html .=  '<ul class="list-group list-group-flush shadow-sm">';

			foreach (glob($path) as $file) {
				$filePath = str_replace(File::dir(),'',$file);
				$html .=  '<li class="list-group-item p-1"><a href="';
				$html .= 'action.php?action='.$meta['action'].'&type=download&path='.base64_encode($filePath);
				$html.= '">'.basename($file).'<a></li>';
			}
			$html.='</ul>';

			return $html;
		};

		$type_file->icon = 'fas fa-file-upload';
		$type_file->description = 'Envoi de fichier';
		$type_file->default = '';
		$types[$type_file->slug] = $type_file;


		//Image
		$type = new self();
		$type->slug = 'image';
		$type->label = 'Image';
		$type->sqlType = '';
		$type->default_attributes = array_merge($type_text->default_attributes,array(
			'class'=>'"component-file-cover bg-white shadow-sm rounded-sm"',
			'data-type'=>'"file"',
			'data-limit'=>'"1"',
			'data-extension'=>'"jpg,png,bmp,jpeg,gif,svg,webp"',
			'data-action'=>'{{action}}',
			'data-id'=>'"'.uniqid(rand(0,100)).'"',
			'data-data'=> "'{}'" ,
		));
		$type->settings = array(
			'action'=>array('type'=>'text','default'=>"dynamicform_handle_file",'attributes'=> array('class'=>'"form-control hidden"','disabled'=>true)),
			'extension'=> array('type'=>'text','label'=>'Extensions permises','placeholder'=>'"Séparation par \',\'"','attributes'=> array('class'=>'"form-control"')),
			'size'=> array('type'=>'integer','label'=>'Taille max (octets)','attributes'=> array('class'=>'"form-control"')),
			'storage'=> array('type'=>'filepicker','label'=>'Dossier d\'enregistrement','attributes'=> array('class'=>'"form-control"','data-editable'=>true)),
			'access'=> array(
				'type'=>'list',
				'label'=>'Contrôle des droits et accès à faire sur la section',
				'values'=>function(){
					$scopes = array();
					foreach(Right::availables() as $slug=>$scope)
						$scopes[$slug] = $scope['label'];
					return $scopes;
				},
				'attributes'=> array('class'=>'"form-control"')
			),
			/*'label'=> array('type'=>'text','label'=>'Libellé visible dans la dropzone du composant','placeholder'=>'"Faites glisser vos fichiers ici"','attributes'=> array('class'=>'"form-control"')),
			'readonly'=> array('type'=>'boolean','label'=>'Lecture seule','attributes'=> array('class'=>'"form-control"')),*/
		);

		$type->onInput = $type_file->onInput;

		$type->onSave = $type_file->onSave;

		$type->onHtmlDisplay = function($value,$options = array()){
			$html = '<div class="fieldtype-image-cover">';
			$meta = json_decode($options['meta'],true);

			$templateData = array();
			foreach($options as $key=>$option){
				if(!is_string($option)) continue;
				$templateData[$key] = $option;
			}

			$path = File::dir().template($meta['storage'],$templateData,true).'/*';

			$files = glob($path);

			$file = $files[0];
			$filePath = str_replace(File::dir(),'',$file);
			$action = 'action.php?action='.$meta['action'].'&type=download&path='.base64_encode($filePath);

			$html .= '<img class="shadow-sm rounded-sm" style="max-width: 100px;height: auto;" src="'.$action.'">';

			if(count($files)>1) $html .= '<span><i class="far fa-file-image"></i> +'.count($files).' </span>';
			$html .= '</div>';


			return $html;
		};

		$type->onRawDisplay  = $type_file->onRawDisplay;

		$type->icon = 'far fa-image';
		$type->description = "Envoi d'image";
		$type->default = '';
		$types[$type->slug] = $type;



		//User
		$type = new self();
		$type->slug = 'user';
		$type->label = 'Utilisateur';
		$type->sqlType = 'string';
		$type->default_attributes = array_merge($type_text->default_attributes,array(
			'data-type'=>'"user"',
		));


		$type->filter = array(
			'operators' => array (
				'=' =>array("view" =>'user'),
				'!='  =>array("view" =>'user'),
				'like' =>array("view" =>'user'),
				'not like'  =>array("view" =>'user'),
				'null' =>array(),
				'not null' =>array()
			)
		);


		$type->onInput  = $type_text->onInput;

		$type->onRawDisplay = function($value,$options = array()){
			if(empty($value)) return '';
			$user = User::byLogin($value);
			return !$user? '':$user->fullName();
		};
		$type->onHtmlDisplay = function($value,$options = array()){
			$raw = $options['type']->onRawDisplay;
			$html = $raw($value,$options);
			if(isset($options['decoration']) && $options['decoration']) $html = '<img src="action.php?action=core_account_avatar_download&amp;user='.$value.'&amp;extension=jpg" class="avatar-mini avatar-rounded avatar-login" title="'.$html.'"> '.$html;
			return $html;
		};
		$type->icon = 'fas fa-user';
		$type->description = 'Autocompletion sur les comptes utilisateurs';
		$type->default = '';
		$types[$type->slug] = $type;


		//Firm
		$type = new self();
		$type->slug = 'firm';
		$type->label = 'Etablissement';
		$type->sqlType = 'string';
		$type->default_attributes = array_merge($type_text->default_attributes,array(
			'data-type'=>'"firm"',
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-building';
		$type->description = 'Autocompletion sur les établissements';
		$type->default = '';
		$type->onRawDisplay  = function($value,$options = array()){
			if(empty($value) || !is_numeric($value)) return '';
			$firm = Firm::getById($value);
			return !$firm? '':$firm->label;
		};
		$type->filter = array(
			'operators' => array (
				'=' =>array("view" =>'firm'),
				'!='  =>array("view" =>'firm'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$types[$type->slug] = $type;


		//Rank
		$type = new self();
		$type->slug = 'rank';
		$type->label = 'Rang';
		$type->sqlType = 'int';
		$type->default_attributes = array_merge($type_text->default_attributes,array(
			'data-type'=>'"user"',
			'data-types'=>'"rank"',
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-user-lock';
		$type->description = 'Autocompletion sur les rangs';
		$type->default = '';

		$type->onRawDisplay = function($value,$options = array()){
			if(empty($value) || !is_numeric($value)) return '';
			$rank = Rank::getById($value);
			return !$rank? '':$rank->label;
		};
		$type->filter = array(
			'operators' => array (
				'=' =>array("view" =>'rank'),
				'!='  =>array("view" =>'rank'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$types[$type->slug] = $type;


		//Textarea
		$type_longstring = new self();
		$type_longstring->slug = 'textarea';
		$type_longstring->label = 'Texte Long';
		$type_longstring->sqlType = 'longstring';
		$type_longstring->default_attributes =  $type_text->default_attributes;

		$type_longstring->onInput  = function($field=array(),$options=array()){
			$html = '';
			$html .= '<textarea ';
			if(isset($field['id'])) $field['attributes']['id'] ='"'.$field['id'].'"';
			foreach ($field['attributes'] as $key=>$attribute) {
				if( $key=='value' ) continue;
				$html .= ' '.$key;
				if(!empty($attribute))
					$html .= '='.$attribute;
			}

			if(isset($field['value']) && is_array($field['value']))
				$field['value'] = json_encode($field['value']);

			$html .='>'.(isset($field['value']) && !empty($field['value']) ? (is_callable($field['value']) ? $field['value']() : $field['value']) : '').'</textarea>';
			return $html ;
		};
		$type_longstring->filter = array(
			'operators' => array (
				'like' =>array("view" =>'text'),
				'not like' =>array("view" =>'text'),
				'=' =>array("view" =>'text'),
				'!='  =>array("view" =>'text'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$type_longstring->icon = 'fas fa-text-width';
		$type_longstring->description = 'Texte long multi ligne';
		$type_longstring->default = '';
		$types[$type_longstring->slug] = $type_longstring;


		//Wysiwyg
		$type = new self();
		$type->slug = 'wysiwyg';
		$type->label = 'Texte enrichi';
		$type->sqlType = 'longstring';
		$type->default_attributes =  array_merge($type_longstring->default_attributes,array(
			'data-type'=>'"wysiwyg"',
			'class'=>'""',
		));
		$type->onInput = $type_longstring->onInput;


		$type->onRawDisplay = function($value,$options = array()){
			if(empty($value)) return '';
			return strip_tags($value);
		};

		$type->onHtmlDisplay = function($value,$options = array()){
			$html = html_entity_decode($value);
			return $html;
		};

		$type->filter = $type_longstring->filter;

		$type->icon = 'fas fa-spell-check';
		$type->description = 'Texte riche multi ligne';
		$type->default = '';
		$types[$type->slug] = $type;


		//Date
		$type = new self();
		$type->slug = 'date';
		$type->label = 'Date';
		$type->sqlType = 'date';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"date"',
			'title'=>'"format jj/mm/aaaa"',
			'placeholder'=>'"JJ/MM/AAAA"',
		));
		$type->onLoad  = function($value){
			return !is_null($value) || is_numeric($value) ? date('d/m/Y',$value) : '';
		};
		$type->onSave  = function($value,$options=array()){
			return !empty($value) ? timestamp_date($value) : null;
		};

		$type->onRawDisplay = function($value,$options = array()){
			$html = !is_null($value) && is_numeric($value) ? date('d/m/Y',$value) : '';
			return $html;
		};

		$type->fromRawDisplay = function($value,$options = array()){
			return timestamp_date($value);
		};


		$type->filter = array(
			'operators' => array (
				'between' =>array("view" =>'date'),
				'=' =>array("view" =>'date'),
				'!='  =>array("view" =>'date'),
				'null' =>array(),
				'not null' =>array()
			)
		);


		$type->onInput  = $type_text->onInput;
		$type->icon = 'far fa-calendar';
		$type->description = 'Date au format jj/mm/aaaa';
		$type->default = '';
		$types[$type->slug] = $type;

		//Tags
		$type = new self();
		$type->slug = 'tag';
		$type->label = 'Etiquettes';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"tag"',
			'data-multiple'=>'true'
		));

		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-tags';
		$type->description = 'Etiquettes (tags) séparés par virgules, espace ou tabulation';
		$type->default = '';


		$type->onHtmlDisplay = function($value,$options = array()){
			$html = '';
			$tags = explode(',',$value);
			$html = '<span class="badge">'.implode('</span><span class="badge">',$tags).'</span>';
			return $html;
		};
		$type->filter = array(
			'operators' => array (
				'in' =>array("view" =>'tag'),
				'not in' =>array("view" =>'tag'),
				'inline-and' =>array("view" =>'tag'),
				'inline-or' =>array("view" =>'tag'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$type->onSave  = function($value,$options=array()){
			$value = explode(',',$value);
			$value = array_filter($value);
			$value = ','.implode(',',$value).',';
			if($value==',,') $value = '';
			return $value;
		};



		$types[$type->slug] = $type;


		//File picker
		$type = new self();
		$type->slug = 'filepicker';
		$type->label = 'Parcourir';
		$type->sqlType = 'longstring';
		$type->default_attributes = array_merge($type_text->default_attributes,array(
			'data-type'=>'"filepicker"',
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'far fa-folder-open';
		$type->description = 'Rechercher dans un dossier';
		$type->default = '';

		$type->settings = array(
			'root'=> array(
				'type'=>'filepicker',
				'label'=>'Racine ciblée',
				'attributes'=> array('class'=>'"form-control"', 'data-root'=>"")
			)
		);

		$type->filter = array(
			'operators' => array (
				'=' =>array("view" =>'filepicker'),
				'!='  =>array("view" =>'filepicker'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$types[$type->slug] = $type;

		//Heure
		$type = new self();
		$type->slug = 'hour';
		$type->label = 'Heure';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"hour"',
			'title'=>'"format hh:mm"',
			'placeholder'=>'"13:37"',
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'far fa-clock';
		$type->description = 'Combo Heures/minutes';
		$type->default = '';

		$type->filter = array(
			'operators' => array (
				'<' =>array("view" =>'hour'),
				'>' =>array("view" =>'hour'),
				'=' =>array("view" =>'hour'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$types[$type->slug] = $type;


		//Dictionnaire
		$type_dictionary = new self();
		$type_dictionary->slug = 'dictionary';
		$type_dictionary->label = 'Liste configurable';
		$type_dictionary->sqlType = 'int';
		$type_dictionary->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"dictionary"',
			'data-slug'=>'"{{key}}"',
			'data-depth'=>'"1"',
			'key'=>'data-disable-label',
			'data-value'=>'',
			'class'=>'"form-control select-control"'
		));
		$type_dictionary->settings = array(
			'slug'=> array(
				'type'=>'dictionary',
				'label'=>'Liste ciblée',
				'attributes'=> array(
					'class'=>'"form-control"',
					'data-slug'=>"",
					'data-output'=>"slug"
				)
			)
		);
		$type_dictionary->onRawDisplay  = function($value,$options = array()){
			if(empty($value) || !is_numeric($value)) return '';
			$dictionary = Dictionary::getById($value);
			return !$dictionary? '':$dictionary->label;
		};

		$type_dictionary->fromRawDisplay = function($value,$options = array()){

			$filters = array('label'=>$value);
			if(isset($options['parent'])) $filters['parent'] = $options['parent'];

			$dictionary = Dictionary::load($filters);
			return !$dictionary ? 0 : $dictionary->id;
		};

		$type_dictionary->onInput  = function($field = array(),$options = array()){
			if(isset($field['id'])) $field['attributes']['id'] ='"'.$field['id'].'"';

			if(isset($field['value'])) $field['attributes']['data-value'] = $field['value'];

			//gestion des metas de dynamiques fields pour listes simples
			if(isset($field['attributes']['data-values'])){
				$field['values'] = $field['attributes']['data-values'];
				unset($field['attributes']['data-values']);
			}

			$html = '';
			$html .= '<select ';
			foreach ($field['attributes'] as $key=>$attribute) {
				$html .= $key;
				if(!empty($attribute))
					$html .= '='.$attribute;
				$html .= ' ';
			}
			$html .='>';

			if(isset($field['values'])){
				$values = is_callable($field['values']) ? $field['values']() : $field['values'];
				if(!empty($values) && is_array($values)){
					foreach($values as $key=>$value){
						$html .='<option '.(isset($field['value']) && $field['value']==$key?' selected="selected" ':'').' value="'.$key.'">'.$value.'</option>';
					}
				}
			}

			$html .='</select>';
			return $html ;
		};
		$type_dictionary->icon = 'fas fa-list-ol';
		$type_dictionary->description = 'Liste de sélection configurable récursive';
		$type_dictionary->default = '';

		$type_dictionary->filter = array(
			'attributes' => array(
				'data-display'=>'"dropdown"',
				'data-slug'=>'"{{slug}}"',
				'data-depth'=>'"{{depth}}"',
				'data-value-selector'=>'".filter-value:last-child"'
			),
			'operators' => array(
				'in' =>array(
					"view" => "checkbox-list",
					"value-separator" => ","
				),
				'not in' =>array(
					"view" => "checkbox-list",
					"value-separator" => ","
				),
				'=' =>array("view" =>'dictionary'),
				'!=' =>array("view" =>'dictionary'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$types[$type_dictionary->slug] = $type_dictionary;


		//Table JSON clé / valeur
		$type = new self();
		$type->slug = 'jsontable';
		$type->label = 'Table clé/valeur';
		$type->sqlType = 'longstring';
		$type->default_attributes = array(
			'type'=>'"text"',
			'data-type'=>'"jsontable"',
			'data-format'=>'"key-value"',
		);
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-table';
		$type->description = 'Table clé/valeur enregistré en JSON';
		$type->default = '';

		$type->onRawDisplay  = function($value){
			$stream = '';
			$table = json_decode($value,true);
			if(!is_array($table)) return '';
			foreach($table as $key=>$value){
				$stream .= ''.$key.' : '.$value.',';
			}


			return $stream;
		};

		$type->onHtmlDisplay  = function($value){
			$html = '<ul class="list-group">';
			$table = json_decode($value,true);
			if(!is_array($table)) return '';
			foreach($table as $key=>$value){
				$html .= '<li class="list-group-item p-1"><strong>'.$key.'</strong> : '.$value.'</li>';
			}

			$html .= '</ul>';
			return $html;
		};

		$types[$type->slug] = $type;

		//Liste
		$type_list = new self();
		$type_list->slug = 'list';
		$type_list->label = 'Liste classique';
		$type_list->sqlType = 'string';

		$type_list->default_attributes = array_merge($type_text->default_attributes,array(
			'class'=>'"form-control select-control"'
		));

		$type_list->filter = array(
			'attributes' => array(
				'data-values'=>'"{{filterTypeValue}}"',
			),
			'operators' => array(
				'in' =>array(
					"view" => "checkbox-list",
					"value-separator" => ",",
				),
				'not in' =>array(
					"view" => "checkbox-list",
					"value-separator" => ",",
				),
				'inline-and' =>array(
					"view" => "checkbox-list",
					"value-separator" => ",",
				),
				'inline-or' =>array(
					"view" => "checkbox-list",
					"value-separator" => ",",
				),
				'=' =>array("view" =>'list'),
				'!=' =>array("view" =>'list'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$type_list->onInput = $type_dictionary->onInput;

		$type_list->onRawDisplay  = function($value,$options = array()){
			if(!isset($options['meta'])) return '';
			$list = json_decode($options['meta'],true);
			return isset($list['values']) && isset($list['values'][$value]) ? $list['values'][$value] : '';
		};

		$type_list->icon = 'fas fa-list-ol';
		$type_list->description = 'Liste de sélection classique';
		$type_list->default = '';
		$type_list->settings = array(
			'values'=> array(
				'type'=>'jsontable',
				'label'=>'Valeurs possibles',
				'attributes'=> array(
					'data-columns' => '\'{"key":"Clé","value":"Valeur"}\'',
					'data-format' => '"key-value"'
				))
		);


		$types[$type_list->slug] = $type_list;


		//Checkbox list
		$type_checkbox = new self();
		$type_checkbox->slug = 'checkbox-list';
		$type_checkbox->label = 'Liste de cases à cocher';
		$type_checkbox->sqlType = 'longtext';
		$type_checkbox->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"checkbox-list"',
			'data-slug'=>'"{{key}}"',
			'data-display'=>'"dropdown"',
			'value'=>'"{{value}}"',
			'class'=>'"form-control"'
		));

		$type_checkbox->settings = array(
			'slug'=> array(
				'type'=>'dictionary',
				'label'=>'Liste ciblée',
				'attributes'=> array(
					'class'=>'"form-control"',
					'data-slug'=>"",
					'data-output'=>"slug",
					'data-format'=>"slug"
				)
			),
			'values'=> array(
				'type'=>'jsontable',
				'label'=>'Valeurs possibles',
				'attributes'=> array(
					'data-columns' => '\'{"key":"Clé","value":"Valeur"}\'',
					'data-format' => '"key-value"'
				)),
			'operator-delete'=> array(
				'type'=>'text',
				'value'=>'["in","not in"]',
				'attributes'=> array(
					'class' => '"hidden"',
				))
		);

		$type_checkbox->filter = array(
			'attributes' => array(
				'data-display'=>'"dropdown"',
				'data-slug'=>'"{{slug}}"',
				'data-depth'=>'"{{depth}}"',
				//'data-display'=>'"{{display}}"',
				'data-values'=>'"{{filterTypeValue}}"',
				'data-multi-level-select'=>'"{{multiLevelSelect}}"',
			),
			'operators' => array(
				'in' =>array(
					"view" => "checkbox-list",
					"value-separator" => ","
				),
				'not in' =>array(
					"view" => "checkbox-list",
					"value-separator" => ","
				),
				'inline-and' =>array(
					"view" => "checkbox-list"
				),
				'inline-or' =>array(
					"view" => "checkbox-list"
				),
				'=' =>array("view" =>'dictionary'),
				'!=' =>array("view" =>'dictionary'),
				'null' =>array(),
				'not null' =>array()
			)
		);

		$type_checkbox->onInput  = function($field=array(),$options=array()){
			$attributes = array_merge($options['type']->default_attributes,$field['attributes']);

			if(isset($field['id'])) $field['attributes']['id'] ='"'.$field['id'].'"';

			$html = '';
			$html.= '<input  value="'.(isset($field['value']) ? str_replace('"','&quot;',$field['value']) : '').'"';
			foreach ($field['attributes'] as $key => $value) {
				$html.= ' ';
				$html.= $key;

				if(!empty($value)) $html.= '='.(is_string($value)?$value:"'".json_encode($value)."'");
				$html.= ' ';
			}

			$html.= '>';
			return $html;
		};

		$type_checkbox->onSave  = function($value,$options=array()){
			$value = explode(',',$value);
			$value = array_filter($value);
			$value = ','.implode(',',$value).',';
			if($value==',,') $value = '';
			return $value;
		};

		$type_checkbox->onLoad  = function($value){
			$value = explode(',',$value);
			$value = array_filter($value);
			$value = implode(',',$value);
			return $value;
		};
		$type_checkbox->onRawDisplay  = function($value,$field) use($type_checkbox){
			$onLoad = 	$type_checkbox->onLoad;
			$value = $onLoad($value);

			$values = explode(',',$value);
			if(count($values)==0) return '';



			$results = array();

			//dictionary
			$ids = array();
			foreach($values as $i=>$id){
				if(empty($id) || !is_numeric($id)) continue;
				unset($values[$i]);
				$ids[] = $id ;
			}
			if(count($ids)!=0){
				$dictionnaries = Dictionary::loadAll(array('id:IN'=>$ids));

				foreach($dictionnaries as $dictionary){
					$results[] = $dictionary->label;
				}
			}

			//valeur en dur
			if(!empty($field['meta'])){
				$meta = json_decode($field['meta'],true);
				if(!empty($meta['values'])){
					$availableValues = $meta['values'];
					foreach($values as $id){
						if(isset($availableValues[$id])) $results[] = $availableValues[$id];
					}
				}
			};


			$value = implode(',',$results);
			return $value;
		};
		$type_checkbox->onHtmlDisplay  = function($value,$field) use($type_checkbox){

			$onRawDisplay = $type_checkbox->onRawDisplay;
			$value = explode(',',$onRawDisplay($value,$field));
			$value = array_filter($value);
			$value = '<span class="badge badge-secondary">'.implode('</span> <span class="badge badge-secondary">',$value).'</span>';
			return $value;
		};

		$type_checkbox->fromRawDisplay = function($value,$options = array()){
			$filters = array('label'=>$value);
			if(isset($options['parent'])) $filters['parent'] = $options['parent'];

			$dictionary = Dictionary::load($filters);
			return !$dictionary ? 0 : $dictionary->id;
		};

		$type_checkbox->icon = 'fas fa-list';
		$type_checkbox->description = 'Liste de cases à cocher configurable';
		$type_checkbox->default = '';
		$types[$type_checkbox->slug] = $type_checkbox;

		//Entier
		$type = new self();
		$type->slug = 'integer';
		$type->label = 'Nombre Entier';
		$type->sqlType = 'int';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'type'=>'"number"',
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-sort-numeric-down';
		$type->description = 'Nombre entier';
		$type->onSave  = function($value,$options=array()){
			$value = trim($value);
			if(!empty($value) && !is_numeric($value)) throw new Exception("Ce champ ne doit contenir que des chiffres et être entier");
			return $value;
		};

		$type->filter = array(
			'operators' => array (
				'in' => array(
					"view" =>'tag',
					"value-separator" => ","
				),
				'not in' => array(
					"view" =>'tag',
					"value-separator" => ","
				),
				'<' => array("view" =>'decimal'),
				'>' => array("view" =>'decimal'),
				'=' => array("view" =>'decimal'),
				'!=' => array("view" =>'decimal'),
				'between' => array("view" =>'decimal'),
				'null' => array(),
				'not null' => array()
			)
		);
		$type->default = '';
		$types[$type->slug] = $type;

		//Flottant
		$type = new self();
		$type->slug = 'decimal';
		$type->label = 'Décimal';
		$type->sqlType = 'decimal';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"decimal"'
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-sort-numeric-down';
		$type->description = 'Nombre décimal';
		$type->onSave  = function($value,$options=array()){
			$value = trim(str_replace(',', '.', $value));
			if(!empty($value) && !is_numeric($value)) throw new Exception("Ce champ ne doit contenir que des chiffres");
			return $value;
		};

		$type->filter = array(
			'operators' => array(
				'in' => array(
					"view" =>'tag',
					"value-separator" => ","
				),
				'not in' => array(
					"view" =>'tag',
					"value-separator" => ","
				),
				'<' => array("view" =>'decimal'),
				'>' => array("view" =>'decimal'),
				'=' => array("view" =>'decimal'),
				'!=' => array("view" =>'decimal'),
				'between' => array("view" =>'decimal'),
				'null' => array(),
				'not null' => array()
			)
		);
		$type->default = '';
		$types[$type->slug] = $type;

		//Adresse
		$type = new self();
		$type->slug = 'address';
		$type->label = 'Adresse';
		$type->sqlType = 'longstring';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"location"',
		));
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-street-view';
		$type->description = 'Adresse postale';
		$type->default = '';

		$type->filter = array(
			'operators' => array (
				'like' => array("view" =>'text'),
				'=' => array("view" =>'text'),
				'!=' => array("view" =>'text'),
				'null' => array(),
				'not null' => array()
			)
		);

		$types[$type->slug] = $type;

		//Password
		$type = new self();
		$type->slug = 'password';
		$type->label = 'Mot de passe';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"password"',
			'autocomplete'=>'"new-password"',
		));

		$type->onInput  = $type_text->onInput;
		$type->onSave  = function($value,$options=array()){
			return encrypt($value);
		};

		$type->onLoad  = function($value){
			$value = decrypt($value);
			return $value === false ? '' : $value;
		};

		$type->onRawDisplay = function($value,$options = array()){
			$value = decrypt($value);
			return $value === false ? '' : $value;
		};

		$type->icon = 'fas fa-unlock-alt';
		$type->description = 'Mot de passe caché';
		$type->default = '';
		$types[$type->slug] = $type;

		//Couleur
		$type = new self();
		$type->slug = 'color';
		$type->label = 'Couleur';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"color"',
		));
		$type->onHtmlDisplay  = function($value,$options = array()){
			if(empty($value)){
				$html = '<span class="fa-stack">
					<i class="far fa-circle fa-stack-1x"></i>
					<i class="fas fa-slash text-danger fa-stack-1x"></i>
				</span>';
			}else{
				$html = '<i class="fas fa-circle" style="color:'.$value.'"></i>';
			}
			return $html;
		};

		$type->filter = array(
			'operators' => array (
				'=' => array("view" =>'text'),
				'!=' => array("view" =>'text'),
				'null' => array(),
				'not null' => array()
			)
		);

		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-palette';
		$type->description = 'Couleur';
		$type->default = '';
		$types[$type->slug] = $type;

		//Icône
		$type = new self();
		$type->slug = 'icon';
		$type->label = 'Icône';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"icon"',
		));
		$type->onInput  = $type_text->onInput;
		$type->onHtmlDisplay  = function($value,$options = array()){
			$html = '<i class="'.$value.'"></i>';
			return $html;
		};

		$type->filter = array(
			'operators' => array (
				'=' => array("view" =>'icon'),
				'!=' => array("view" =>'icon'),
				'null' => array(),
				'not null' => array()
			)
		);

		$type->icon = 'fas fa-icons';
		$type->description = 'Icône d\'illustration';
		$type->default = '';
		$types[$type->slug] = $type;


		//Prix
		$type = new self();
		$type->slug = 'price';
		$type->label = 'Prix';
		$type->sqlType = 'decimal';

		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-euro-sign';
		$type->description = 'Devis monetaire';
		$type->default = '';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"price"',
			'step'=>'"0.01"',
			'type'=>'number'
		));

		$type->filter = array(
			'operators' => array (
				'=' => array("view" =>'price'),
				'!=' => array("view" =>'price'),
				'<' => array("view" =>'price'),
				'>' => array("view" =>'price'),
				'between' =>array("view" =>'price'),
				'in' => array(
					"view" =>'tag',
					"value-separator" => ","
				),
				'not in' => array(
					"view" =>'tag',
					"value-separator" => ","
				),
				'null' => array(),
				'not null' => array()
			)
		);

		$type->onSave  = function($value,$options=array()){
			$value = trim(str_replace(',', '.', $value));
			if(!empty($value) && !is_numeric($value)) throw new Exception("Le prix ne doit contenir que des chiffres");
			return $value;
		};
		$types[$type->slug] = $type;


		//Choix
		$type = new self();
		$type->slug = 'choice';
		$type->label = 'Choix';
		$type->customLabel = false;
		$type->sqlType = 'longstring';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"choice"',
			'class'=>''
		));

		$type->filter = array(
			'attributes' => array('data-values'=>'"{{filterTypeValue}}"'),
			'operators' => array (
				'=' => array("view" =>'choice'),
				'!=' => array("view" =>'choice'),
				'null' => array(),
				'not null' => array()
			)
		);

		$type->settings = array(
			'values'=> array(
				'type'=>'jsontable',
				'label'=>'Options possibles',
				'attributes'=> array(
					'data-columns' => '\'{"key":"Clé","value":"Valeur"}\'',
					'data-format' => '"key-value"'
				))
		);

		$type->onInput  = function($field=array(),$options=array()){
			$attributes = array_merge($options['type']->default_attributes,$field['attributes']);

			if(isset($field['id'])) $field['attributes']['id'] ='"'.$field['id'].'"';
			$html = '';
			$html.= '<input  value="'.(isset($field['value']) ? str_replace('"','&quot;',$field['value']) : '').'" data-values="'.(isset($field['values']) ? htmlspecialchars(json_encode($field['values']), ENT_QUOTES, 'UTF-8') : '').'"';

			foreach ($field['attributes'] as $key => $value) {
				$html.= ' ';
				$html.= $key;

				if(!empty($value)) $html.= '='.(is_string($value)?$value:"'".json_encode($value)."'");
				$html.= ' ';
			}

			$html.= '>';
			return $html;
		};
		$type->onRawDisplay = function($value,$options = array()){
			if(isset($options['meta'])){
				$meta = is_string($options['meta']) ? json_decode($options['meta'],true) : $options['meta'];
				if(isset($meta['values']) && isset($meta['values'][$value])){
					$value = $meta['values'][$value];
				}
			}
			return $value;
		};

		$type->onHtmlDisplay  = $type->onRawDisplay;

		$type->icon = 'far fa-dot-circle';
		$type->description = 'Choix unique';
		$type->default = '';
		$types[$type->slug] = $type;

		//Booléen
		$type = new self();
		$type->slug = 'boolean';
		$type->label = 'Vrai ou Faux';
		$type->customLabel = true;
		$type->sqlType = 'boolean';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"checkbox"',
			'class'=>'',
			'type'=>'"checkbox"'
		));

		$type->filter = array(
			'operators' => array (
				'=' => array("view" =>'boolean'),
				'null' => array(),
				'not null' => array()
			)
		);


		$type->onRawDisplay = function($value,$options = array()){
			return $value ? 'VRAI' : 'FAUX';
		};

		$type->onHtmlDisplay  = function($value,$options = array()){
			$html = $value ? '<i class="fas fa-check text-success"></i>' : '<i class="fas fa-times text-danger"></i>' ;
			return $html;
		};

		$type->onInput = function($field=array(),$option=array()){
			$html = '';

			$html.= '<div><label><input id="'.(isset($field['id'])?$field['id']:'').'" ';
			if(isset($field['value']) && $field['value'] == 1){
				$field['attributes']['checked'] = '"checked"';
			}
			foreach ($field['attributes'] as $key => $value) {
				$html.= ' ';
				$html.= $key;
				if(!empty($value)) $html.= '='.$value;
				$html.= ' ';
			}
			$html.= '>';
			if(!empty($field['label']) && $option['allowCustomLabel']) $html.= ' '.$field['label'];

			$html.= '</label></div>';
			return $html;
		};
		$type->icon = 'far fa-check-square';
		$type->description = 'Vrai ou Faux';
		$type->default = '';



		$types[$type->slug] = $type;

		//Url
		$type = new self();
		$type->slug = 'url';
		$type->label = 'Adresse web';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"url"',
		));
		$type->onSave  = function($value,$options=array()){
			if(!empty($value) &&  !filter_var($value, FILTER_VALIDATE_URL) ) throw new Exception("Mauvais format d'adresse web");
			return $value;
		};
		$type->onInput  = $type_text->onInput;
		$type->onHtmlDisplay  = function($value,$options = array()){
			if(empty($value)) return $value;
			$html = '<a href="'.$value.'">';
			$html .= $value;
			$html.= '</a>';
			return $html;
		};

		$type->filter = array(
			'operators' => array (
				'=' => array("view" =>'url'),
				'!=' => array("view" =>'url'),
				'like' => array("view" =>'text'),
				'not like' => array("view" =>'text'),
				'null' => array(),
				'not null' => array()
			)
		);

		$type->icon = 'fas fa-globe';
		$type->description = 'Adresse Url (web)';
		$type->default = '';
		$types[$type->slug] = $type;

		//Mail
		$type = new self();
		$type->slug = 'mail';
		$type->label = 'E-mail';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"mail"',
			'type'=>'"mail"',
			'pattern'=>'".+@.+"'
		));
		$type->onSave  = function($value,$options=array()){
			if(!empty($value) && !check_mail($value)) throw new Exception("Mauvais format d'e-mail");
			return $value;
		};
		$type->onHtmlDisplay  = function($value,$options = array()){
			$html = '<a href="mailto:'.$value.'">';
			$html .= $value;
			$html.= '</a>';
			return $html;
		};

		$type->filter = array(
			'operators' => array (
				'=' => array("view" =>'mail'),
				'!='  => array("view" =>'mail'),
				'like'  => array("view" =>'text'),
				'not like'  => array("view" =>'text'),
				'null' => array(),
				'not null' => array()
			)
		);

		$type->onInput  = $type_text->onInput;
		$type->icon = 'far fa-envelope-open';
		$type->description = 'E-mail';
		$type->default = '';
		$types[$type->slug] = $type;

		//Téléphone
		$type = new self();
		$type->slug = 'phone';
		$type->label = 'Téléphone';
		$type->sqlType = 'string';
		$type->default_attributes =  array_merge($type_text->default_attributes,array(
			'data-type'=>'"phone"'
		));
		$type->onSave  = function($value,$options=array()){
			if(!empty($value) && !check_phone_number($value)) throw new Exception("Mauvais format de téléphone");
			$value = normalize_phone_number($value);
			return $value;
		};
		$type->onInput  = $type_text->onInput;
		$type->icon = 'fas fa-mobile-alt';
		$type->description = 'N° Téléphone';
		$type->default = '';

		$type->filter = array(
			'operators' => array (
				'like' =>array("view" =>'text'),
				'not like' =>array("view" =>'text'),
				'=' =>array("view" =>'phone'),
				'!='  =>array("view" =>'phone'),
				'null' =>array(),
				'not null' =>array()
			)
		);
		$types[$type->slug] = $type;

		Plugin::callHook('field_types',array(&$types));

		if(isset($key)) return isset($types[$key])? $types[$key] : $types['text'];
		return $types;
	}
}

?>