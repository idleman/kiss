<?php

class Image {

	public static function toJpg($path){
		$infos = pathinfo($path);
		if($infos['extension']=='jpg' || $infos['extension']=='jpeg') return $path;
	
		//Make image with white background instead of black when converting png to jpg
		$input = self::resource($path);
		list($width, $height) = getimagesize($path);
		$output = imagecreatetruecolor($width, $height);
		$white = imagecolorallocate($output,  255, 255, 255);
		imagefilledrectangle($output, 0, 0, $width, $height, $white);
		imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
		$newPath = $infos['dirname'].SLASH.$infos['filename'].'.jpg';
	    // quality is a value from 0 (worst) to 100 (best)
		imagejpeg($output, $newPath, 100);

		unlink($path);
		return $newPath;
	}

	public static function toPng($path){
		$infos = pathinfo($path);
		if($infos['extension']=='png') return;
	
		$input = self::resource($path);
		list($width, $height) = getimagesize($path);
		$output = imagecreatetruecolor($width, $height);
		$white = imagecolorallocate($output,  255, 255, 255);
		imagefilledrectangle($output, 0, 0, $width, $height, $white);
		imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
		$newPath = $infos['dirname'].SLASH.$infos['filename'].'.png';
	    // compression is a value from 0 (no compression) to 9 (high compression)
		imagepng($output, $newPath, 9);

		unlink($path);
		return $newPath;
	}

	public static function resize($path,$width,$height,$keepRatio = true,$forceResize = true){
		$resource = self::resource($path);
		if(!is_resource($resource)) return;
		$infos = pathinfo($path);

		$oldX = imageSX($resource);
    	$oldY = imageSY($resource);

    	// Ne pas resize une image plus petite
    	if (!$forceResize && $oldX <= $width && $oldY <= $height) {
    		return;
    	}
    	if($oldX > $oldY) {
	        $thumb_w = $width;
	        $thumb_h = $oldY*($height/$oldX);
	    }
	    if($oldX < $oldY) {
	        $thumb_w = $oldX*($width/$oldY);
	        $thumb_h = $height;
	    }
	    if($oldX == $oldY) {
	        $thumb_w = $width;
	        $thumb_h = $height;
	    }
	    if(!$keepRatio){
			$thumb_w = $width;
			$thumb_h = $height;
	    }

	    $dst_resource = ImageCreateTrueColor($thumb_w,$thumb_h);
    	
    	switch (mb_strtolower($infos['extension'])) {
    	    case 'png':
    	        $resource = ImageCreateFromPNG($path);
    	        imagealphablending($dst_resource, false);
    	        imagecopyresampled($dst_resource,$resource,0,0,0,0,$thumb_w,$thumb_h,$oldX,$oldY); 
    	        imagesavealpha($dst_resource, true);
    	        $result = imagepng($dst_resource, $path, 4);
    	    break;

    	    case 'jpg':
    	    case 'jpeg':
    	        imagecopyresampled($dst_resource,$resource,0,0,0,0,$thumb_w,$thumb_h,$oldX,$oldY); 
    	        $result = imagejpeg($dst_resource,$path,80);
    	    break;
    	    
    	    default:
	    	    //On ne peut pas resize les GIF (sans faire une usine à gaz)
	    	    //On ne peut resize les BMP que depuis la v7.0 de PHP
    	    break;
    	}
	    imagedestroy($dst_resource); 
    	imagedestroy($resource);
	}

	public static function resource($path){
		$infos = pathinfo($path);
		switch (mb_strtolower($infos['extension'])){
			case 'jfif':
			case 'jpg':
			case 'jpeg':
				return imagecreatefromjpeg($path);
			break;
			case 'png':
				return imagecreatefrompng($path);
			break;
			case 'gif':
				return imagecreatefromgif($path);
			break;
			// PHP 7 only
			/*case 'bmp':
				return imagecreatefrombmp($path);
			break;*/
			default:
				return 0;
			break;
		}
	}

	/*
	Retourne une image selon l'angle définit
	Prérequis systeme : 
	- Installer le paquet imagemagick (apt-get install imagemagick) 
	
	nb: Si le pb "convert-im6.q16: not authorized `outfile.pdf' @ error/constitute.c/WriteImage/1037" apparait
	editer /etc/ImageMagick-6/policy.xml et changer sur la ligne  PDFle droit  none à read|write .*/
	public static function rotate($image,$destination,$degree){
		$command = 'convert ';
		$command .= ' "'.$image.'" ';
		$command .= ' -rotate '.$degree.' "'.$destination.'"';
		$result = shell_exec($command);
		return $result;
	}


	/*
	Concatene les images fournies en une seule
	Prérequis systeme : 
	- Installer le paquet imagemagick (apt-get install imagemagick) 
	*/
	public static function concatenate($images,$destinationPath){
		$command = 'convert -limit width 100KP ';
		foreach ($images as $image) {
			$command .= ' "'.$image.'" ';
		}
		$command .= ' -append "'.$destinationPath.'"';
		return shell_exec($command);
	}

	/*
	Convertis le tableau de chemin d'images fournis en un PDF
	Prérequis systeme : 
	- Installer le paquet imagemagick (apt-get install imagemagick) 
	
	nb: Si le pb "convert-im6.q16: not authorized `outfile.pdf' @ error/constitute.c/WriteImage/1037" apparait
	editer /etc/ImageMagick-6/policy.xml et changer sur la ligne  PDFle droit  none à read|write .*/
	public static function toPDF($images,$pdfPath){
		$command = 'convert ';
		foreach ($images as $image) {
			$command .= ' "'.$image.'" ';
		}
		$command .= ' -quality 100 "'.$pdfPath.'"';
		$result = shell_exec ($command);
	}

}
