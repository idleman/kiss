<?php
/**
 * Define a user rank.
 * @author valentin carruesco
 * @category Core
 * @license MIT
 */
class Rank extends Entity {
	public $id,$label,$description,$superadmin;
	public $entityLabel = 'Rang';
	protected $fields = array(
		'id' => array('label'=> 'Identifiant' , 'type'=>'key'),
		'label' => array('label'=> 'Libellé' , 'type'=>'text'),
		'description' => array('label'=> 'Description' , 'type'=>'textarea'),
		'superadmin' => array('label'=> 'Rang super administrateur' , 'type'=>'boolean')
	);

	function setId($id){
		$this->id = $id;
	}
	
	function getId(){
		return $this->id;
	}

	function getLabel(){
		return $this->label;
	}

	function setLabel($label){
		$this->label = $label;
	}

	function getDescription(){
		return $this->description;
	}

	function setDescription($description){
		$this->description = $description;
	}
}
