<?php
/**
 * Define a person contact.
 * @author Valentin carrueso
 * @category Plugin
 * @license MIT
 */
class ContactPerson extends Entity{

	public $id;
	public $name; //Nom (Texte)
	public $firstname; //Prénom (Texte)
	public $job; //Fonction (Texte)
	public $account; //Compte lié (uniqment si référent interne)
	public $civility; //Civilité (Texte)
	public $state; //Etat (Texte)
	public $type; //Type (Texte)
	public $phone; //Téléphone (Texte)
	public $mobile; //Téléphone Mobile (Texte)
	public $mail; //E-mail (Texte)
	public $scope; //Scope (Texte)
	public $comment; //Commentaire (Texte long)
	public $uid; //UID (Entier)
	public $meta = array(); 
	public $tag; //Etiquettes (Etiquettes)

	const TYPE_EXTERNAL = "external";
	const TYPE_CLIENT = "client";
	const TYPE_USER = "user";

	public $entityLabel = 'Contact';
	protected $TABLE_NAME = 'contact_person';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'name' => array('type'=>'text', 'label' => 'Nom'),
		'firstname' => array('type'=>'text', 'label' => 'Prénom'),
		'job' => array('type'=>'text', 'label' => 'Métier/Fonction'),
		'account' => array('label'=>'Compte utilisateur lié','type'=>'user'),
		'civility' => array('type'=>'list', 'label' => 'Civilité'),
		'state' => array('type'=>'text', 'label' => 'Etat'),
		'type' => array('type'=>'text', 'label' => 'Type'),
		//'phone' => array('type'=>'phone', 'label' => 'Téléphone'),
		//'mail' => array('type'=>'mail', 'label' => 'Email'),
		'scope' => array('type'=>'text', 'label' => 'Périmetre'),
		'comment' => array('type'=>'textarea', 'label' => 'Commentaire'),
		'uid' => array('type'=>'integer', 'label' => 'Identifiant de périmetre'),
		'tag' => array('type'=>'tag', 'label' => 'Etiquettes')
	);

	

	//Colonnes indexées
	public $indexes = array('uid','scope');

	//remplis les métas (phones, mail principaux, phone et mails secondaires) par types si spécifiés
	public function meta($types = array(),$defaultOnly = false ){
		$filters = array('scope'=>'contact_person','uid'=>$this->id);
		if(!empty($types)) $filters['type:IN'] = implode(',',$types);
		if($defaultOnly) $filters['label'] = '';
		
		foreach(Contact::loadAll($filters) as $contact){
			if($contact->type==Contact::PHONE && $contact->label=='') $this->phone = $contact->value;
			if($contact->type==Contact::MOBILE && $contact->label=='') $this->mobile = $contact->value;
			if($contact->type=="mail" && $contact->label=='') $this->mail = $contact->value;
		
			if(!isset($this->meta[$contact->type])) $this->meta[$contact->type] = array();
			$this->meta[$contact->type][] = $contact;
		}
	}

	public function getTags(){
		$tags = explode(',', $this->tag);
		return array_values(array_filter($tags));
	}

	public function setTags($tags){
		if(is_array($tags)){
			$tags = array_filter($tags);
			$tags = implode(',', $tags);
		}
		$this->tag = ','.$tags.',';
	}

	public function toArray($decoded = false){
		$fields = parent::toArray();
		$fields['phone'] = $this->phone;
		$fields['mobile'] = $this->mobile;
		$fields['mail'] = $this->mail;
		$fields['meta'] = $this->meta;
		return $fields;
	}

	//récupere la liste d'entité et leurs entitées par défaut liées (téléphone et mail)
	public static function getAll($parameters = array()){
		$query =  '';
		$data = array();

		$query .= 'SELECT *,';
		$query .= '(SELECT value FROM '.Contact::tableName().' c WHERE c.type=? AND c.scope=? AND c.uid={{table}}.id AND (c.label=? OR c.label IS NULL) LIMIT 1) as phone';
		$query .= ',(SELECT value FROM '.Contact::tableName().' c WHERE c.type=? AND c.scope=? AND c.uid={{table}}.id AND (c.label=? OR c.label IS NULL) LIMIT 1) as mobile';
		$query .= ',(SELECT value FROM '.Contact::tableName().' c WHERE c.type=? AND c.scope=? AND c.uid={{table}}.id AND (c.label=? OR c.label IS NULL) LIMIT 1) as mail';
		$query .= ' FROM {{table}}';
		
		$data[] = Contact::PHONE;
		$data[] = 'contact_person';
		$data[] = '';

		$data[] = Contact::MOBILE;
		$data[] = 'contact_person';
		$data[] = '';

		$data[] = 'mail';
		$data[] = 'contact_person';
		$data[] = '';

		$query .= ' WHERE 1 ';

		if(!empty($parameters['where'])){
			foreach($parameters['where'] as $key=>$value){
				$query .= ' AND '.$key.' = ? ';
				$data[] = $value;
			}
		}

		$rows = array();
		foreach(self::staticQuery($query,$data,true) as $item){
			$item->phone = $item->foreign('phone');
			$item->mobile = $item->foreign('mobile');
			$item->mail = $item->foreign('mail');
			$rows[] = $item;
		}
		return $rows;
	}

	//liste des Civilité possibles
	public static function civilities($key=null){
		$items = array(
			'm' => array('label'=>'M.'), 
			'mme' => array('label'=>'Mme.')
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non définis');
	}


	public static function remove($id){
		if(!isset($id)) throw new Exception("ID se suppression non spécifié");

		self::deleteById($id);
		Contact::delete(array('scope'=>'contact_person','uid'=>$id));
	}
	//Sauvegarde l'entité et ses entitées de contact liées (téléphones, mail...)
	public function save_all(){
		parent::save();
		
		$currentMeta = array('phone'=>$this->phone,'mobile'=>$this->mobile,'mail'=>$this->mail);
		//Enregistrement des mails et téléphones princpaux dans la table de contacts (si renseignés)
		if(!isset($this->phone) && !isset($this->mobile) && !isset($this->mail)) return;
		

		$metaContacts = array('phone','mail','mobile');
		//récuperation des contacts par defaut existants pour cette personne
		$this->meta($metaContacts,true);
		
		foreach ($metaContacts as $metaContact) {
			//si le champ est renseigné
			if(isset($currentMeta[$metaContact])){
				//on récupere le champ existant ou on en créé un nouveau
				$contact = !empty($this->meta[$metaContact]) ? $this->meta[$metaContact][0] : new Contact();
				

				//on ne save que si la valeur a changée
				if($contact->value != $currentMeta[$metaContact]){
					$contact->value = $currentMeta[$metaContact];
					$contact->scope = 'contact_person';
					$contact->uid = $this->id;
					$contact->type = $metaContact;
					$contact->label = '';
					$contact->save();
				}
			}
		}
	}

	public function avatar(){
		$mapping = array(
			'/pdg|directeur|dg/is' => 'job-direction.png',
			'/commercial(e)?|vente/is' => 'job-sales.png',
			'/communication|marketing|social/is' => 'job-communication.png',
			'/ingénieur|engineer|architect(e)?|recherche/is' => 'job-engineer.png',
			'/support|secrétaire|hotline/is' => 'job-support.png',
			'/ressources humaines|RH|HR/is' => 'job-rh.png',
			'/financ(e|ière|ier)|monnaie|comptab(le|bilitée)/is' => 'job-money.png',
			'/logistique|stock|atelier|livr(eur|aison)/is' => 'job-logistic.png',
		);
		$path = __ROOT__.SLASH.'img'.SLASH.'contact'.SLASH.'default-contact.png';
		foreach ($mapping as $regex => $relatedPath) {
			if( preg_match($regex,$this->job)  ){
				$path = __ROOT__.SLASH.'img'.SLASH.'contact'.SLASH.$relatedPath;
			}
		}
		return $path;
	}

	public function fullName(){
        return ucfirst($this->firstname).' '.strtoupper($this->name);
    }

    public function toReadable(){
		$text = $this->civility.' '.$this->fullName();
		if(isset($this->phone) && !empty($this->phone)) $text .=' Tel: '.$this->phone;
		if(isset($this->mail) && !empty($this->mail)) $text .=' Mail : '.$this->mail;
		return $text;
	}
}
?>