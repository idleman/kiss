<?php
/**
 * Define a user for a  firm.
 * @author valentin carruesco
 * @category Core
 * @license MIT
 */
class UserPreference extends Entity{
	public $id,$user,$key,$value;
	public $TABLE_NAME = 'user_preference';
	public $entityLabel = 'Préférence utilisateur';
	public $links = array( 
		'user' => 'User.login'
	);
	public $fields =
	array(
		'id' => array('label'=>'Identtifiant', 'type'=>'key'),
		'user' => array('label'=>'Utilisateur', 'type'=>'user'),
		'key' => array('label'=>'Clé', 'type'=>'text'),
		'value' => array('label'=>'Valeur', 'type'=>'textarea')
	);
}
?>