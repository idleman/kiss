<?php
/**
 * Define a sub firm.
 * @author valentin carruesco
 * @category Core
 * @license MIT
 */
class Firm extends Entity{
	public $id,$label,$description,$mail,$phone,$fax,$street,$street2,$city,$zipcode,$siret,$iban;

	public $entityLabel = 'Etablissement';

	public $fields =
	array(
		'id' => array('label'=>'Identifiant', 'type'=>'key'),
		'label' => array('label'=>'Libellé', 'type'=>'text'),
		'description' => array('label'=>'Description', 'type'=>'textarea'),
		'mail' => array('label'=>'E-mail', 'type'=>'mail'),
		'phone' => array('label'=>'N° Téléphone', 'type'=>'phone'),
		'fax' => array('label'=>'N° FAX', 'type'=>'phone'),
		'street' => array('label'=>'Rue', 'type'=>'textarea'),
		'street2' => array('label'=>'Complément d\'adresse', 'type'=>'textarea'),
		'city' => array('label'=>'Ville', 'type'=>'text'),
		'zipcode' => array('label'=>'Code postal', 'type'=>'text'),
		'siret' => array('label'=>'N° SIRET', 'type'=>'text'),
		'iban' => array('label'=>'N° IBAN', 'type'=>'text')
	);

	//Déclaration de l'établissement anonyme (pour les non connectés)
	public static function anonymous_firm(){
		$firm = new Firm;
		$firm->id = -1;
		$firm->label = 'Sans établissement';

		return $firm;
	}

	public static function logo_path(){
		return 'core'.SLASH.'public'.SLASH.'firm'.SLASH;
	}

	public function address(){
		return $this->street.' '.$this->street2.' '.$this->zipcode.' '.$this->city;
	}

	public function __sleep(){
		return array_merge(array_keys($this->toArray()));
	}

	public function logo($key=null){
	   	$finded = false;
		$image = array();
	   	$paths = array(
	   		'jpg' => FILE_PATH.self::logo_path().$this->id.SLASH.'logo.jpg',
	   		'jpeg' => FILE_PATH.self::logo_path().$this->id.SLASH.'logo.jpeg',
	   		'png' => FILE_PATH.self::logo_path().$this->id.SLASH.'logo.png',
	   	);

	   	$image['url'] = $image['path'] = 'img'.SLASH.'logo'.SLASH.'default-logo.png';
		foreach ($paths as $extension => $path) {
			if (file_exists( __ROOT__.$path) && !$finded) {
				$image['path'] = __ROOT__.$path;
				$image['publicPath'] = ROOT_URL.SLASH.'media'.SLASH.self::logo_path().$this->id.SLASH.'logo.'.$extension;
				$image['url'] = 'action.php?action=core_firm_logo&type=download&path='.base64_encode('core/public/firm/'.$this->id.'/logo.*');
				$finded = true;
			}
		}
		return isset($key) && isset($image[$key]) ? $image[$key] : $image['url'];
	}

	public function has_plugin($plugin){
		$states = Plugin::states();
		if(!isset($states[$plugin]) || !in_array($this->id, $states[$plugin])) return false;
		return true;
	}
}
?>