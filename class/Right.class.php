<?php

/**
 * Define a user right (right = crud on scope + firm [+ uid] ).
 * @author valentin carruesco
 * @category Core
 * @license MIT
 */
class Right extends Entity {
    public $id,$firm,$read,$edit,$delete,$recursive,$configure,$scope,$uid,$targetScope,$targetUid;
    public $entityLabel = 'Droit applicatif';
    protected $fields = array(
        'id' => array('label'=>'Identifiant', 'type'=>'key'),
        'firm' => array('label'=>'Id de l\'établissement concerné', 'type'=>'integer','link'=>'class/Firm.class.php'),
        'read' => array('label'=>'Droit de lecture', 'type'=>'boolean'),
        'edit' => array('label'=>'Droit d\'édition', 'type'=>'boolean'),
        'delete' => array('label'=>'Droit de supression', 'type'=>'boolean'),
        'recursive' => array('label'=>'Droit récursif', 'type'=>'boolean'),
        'configure' => array('label'=>'Droit de configuration', 'type'=>'boolean'),
        'uid' => array('label'=>'Identifiant de l\'entité liée', 'type'=>'string'),
        'scope' => array('label'=>'Périmetre de l\'entité liée', 'type'=>'string'),
        'targetScope' => array('label'=>'Périmetre de l\'entité cible', 'type'=>'text'),
        'targetUid' => array('label'=>'Identifiant de l\'entité cible', 'type'=>'integer')
    );

   

    //Colonnes indexées
    public $indexes = array('firm', 'targetScope', 'targetUid', 'scope', 'uid');
    
    //enregistrement d'un droit possible sur un module, une entité ...
    public static function register($scope,$options){
        //options par defaut
        $options = array_merge(array(
            'global' => true,
            'check' => function($action,$right){
                 global $myUser;
                 //Droits sur la section/le module sans cible entité
                 if($right->uid != 0) throw new Exception('Droit non ciblé, ne pas spécifier d\'id');
                 if($myUser->superadmin) throw new Exception('Vous n\'avez pas les privilèges pour définir des droits dans ce contexte');
            }
        ),$options);

        $GLOBALS['rights'][$scope] = $options;  
    }

    //récuperation des meta information d'un droit
    public static function getOption($scope) {
        if(!isset($GLOBALS['rights'][$scope])) return;
        return $GLOBALS['rights'][$scope];
    }

    //récuperation de la liste des droits possibles
    public static function availables() {
        if(!isset($GLOBALS['rights'])) return array();
        return $GLOBALS['rights'];
    }

    function __construct(){
        parent::__construct();
        $this->uid = 0;
        $this->firm = 0;
        $this->read = 0;
        $this->edit = 0;
        $this->delete = 0;
        $this->configure = 0;
        $this->recursive = 0;
    }
}
