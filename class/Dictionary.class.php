<?php
/**
* Manage application and plugins lists with key/value pair.
* 
* @author valentin carruesco
*
* @category Core
*
* @license MIT
*/
class Dictionary extends Entity {
	public $id,$slug,$label,$parent,$state,$sublistlabel,$childs;
	public $entityLabel = 'Liste configurable';
	protected $fields = array(
		'id' => array('label'=>'Identifiant', 'type' => 'key'),
		'slug' => array('label'=>'Identifiant lisible', 'type'=>'text'),
		'label' => array('label'=>'Libellé', 'type' => 'text'),
		'parent' => array('label'=>'ID liste parente', 'type'=>'integer'),
		'state' => array('label'=>'État', 'type'=>'text'),
		'sublistlabel' => array('label'=>'Libellé de la sous liste', 'type'=>'textarea')
	);

	public function __construct() {
		parent::__construct();
	}

	public function toArray($decoded=false) {
		$array = parent::toArray();
		$array['childs'] = $this->childs;
		return $array;
	}

	//Retourne une liste formatée en fonction de son slug
	//(retourne ses enfants si le parametre childs est à true)
	public static function slugToArray($slug, $childs = false, $state = self::ACTIVE){
		$listArray = array();
		$lists = self::bySlug($slug, $childs, $state);
		if(!$childs) return $lists;

		foreach($lists as $list)
			$listArray[$list->id] = $list;
		return $listArray;
	}

	//Retourne une liste en fonction de son slug (retourne ses enfants si le parametre childs est à true)
	public static function bySlug($slug, $childs = false, $state = self::ACTIVE){
		if($childs) return self::childs(array('slug'=>$slug,'state:IN'=>$state));
		return self::load(array("slug"=>$slug,'state:IN'=>$state));
	}

	public static function childs($param = array(),$options = array('depth'=>1,'format'=>'object'),$level=1) {
		$childs = array();
		
		if(isset($param['id']) && $param['id']==0){
			$parent = new Dictionary();
			$parent->id = 0;
			$parent->label = 'Racine';

		}else{
			$parent = self::load($param);
		}
		
		if(!$parent) return $childs;
		$state = isset($param['state:IN']) ? $param['state:IN'] : self::ACTIVE;
		foreach (self::loadAll(array('parent'=>$parent->id, 'state:IN'=>$state), array(' label ASC ')) as $child) {
			if($options['depth']>$level){
				$param['id'] = $child->id;
				$child->childs = self::childs($param,$options,$level+1);
			}
			$childs[] = $options['format'] == 'object' ? $child : $child->toArray();
		}
		return $childs;
	}

	public static function hierarchy($idOrSlug, $slug, &$elements=array()) {
	   if(is_numeric($idOrSlug)){
	   		$current = self::getById($idOrSlug);
	   }else{
	   		$current = self::bySlug($idOrSlug);
	   }
	  
	   if(!$current) return array();

	   if(($current->parent == 0 && $slug != '') || ($current->parent == -1 && $slug == '')) return $elements;

	   //gestion du slug racine
	   if($slug == ''){
	   		$parent = new Dictionary();
	   		$parent->id = 0;
	   //gestion des slugs classiques
	   }else{
			$parent = self::load(array('id' => $current->parent));
	   }


	   $children = self::childs(array('id'=>$parent->id));

	   $parent->childs = array();

	   foreach($children as $child){

	    	if($child->id == $current->id) {
	    		$child = !empty($elements) ? $elements : $child;
	    		$child->selected = true;
	    	}else{
	    		$child->childs = array();
	    		$child->childs = self::childs(array('id'=>$child->id));
	    	}
    		$parent->childs[] = $child;
	   }

	   $elements = $parent;
	   return self::hierarchy($parent->id, $slug, $elements);
	}

	//Supprime un dictionary et ses enfants de manières récursive
	public static function remove($slug){
		$childs = self::bySlug($slug,true);
		self::delete(array('slug'=>$slug));
		if(!is_array($childs)) return;
		foreach ($childs as $item) 
			self::remove($item->slug);
	}
}
