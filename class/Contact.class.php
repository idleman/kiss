<?php
/**
 * Define a contact
 * @author  Valentin MORREEL
 * @category Core
 * @license MIT
 */
class Contact extends Entity{
	public $id,$scope,$uid,$label,$type,$value,$state;
	public $entityLabel = 'Information de Contact';
	//Phone constants
	const PHONE = 'phone';
	const MOBILE= 'mobile';
	//Mail constants
	const PERSONAL_MAIL = 'personal_mail';
	const PROFESSIONAL_MAIL = 'professional_mail';

	public $fields = array(
		'id' => array('label'=>'Identifiant', 'type'=>'key'),
		'scope' => array('label'=>'Périmetre de l\'entité liée', 'type'=>'text'),
		'uid' => array('label'=>'Identifiant de l\'entité liée', 'type'=>'integer'),
		'label' => array('label'=>'Libellé', 'type'=>'text'),
		'type' => array('label'=>'Type d\'information de contact', 'type'=>'text'),
		'value' => array('label'=>'Valeur', 'type'=>'text'),
		'state' => array('label'=>'Etat', 'type'=>'text')
	);

	public $indexes = array('uid','scope');

	public static function types($key=null){
		$types = array(
			self::PHONE => array(
				'label' => "Tél. Fixe",
				'icon' => "",
				'color' => "",
			),
			self::MOBILE => array(
				'label' => "Tél. Mobile",
				'icon' => "",
				'color' => "",
			),
			self::PERSONAL_MAIL => array(
				'label' => "Mail perso.",
				'icon' => "",
				'color' => "",
			),
			self::PROFESSIONAL_MAIL => array(
				'label' => "Mail pro.",
				'icon' => "",
				'color' => "",
			),
		);
		Plugin::callHook('contact_types', array(&$types));
		
		return isset($key) && isset($types[$key]) ? $types[$key] : $types;
	}
}
?>