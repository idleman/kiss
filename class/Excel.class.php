<?php 

require_once (__DIR__.SLASH.'..'.SLASH.'constant.php');
//Exemple d'utilisation de XLSXWriter : https://github.com/mk-j/PHP_XLSXWriter/tree/master/examples
require_once (LIB_PATH.'XLSXWriter'.SLASH.'XLSXWriter.class.php');
require_once (LIB_PATH.'SimpleXLSX'.SLASH.'SimpleXLSX.class.php');

class Excel
{
	/** 
	 * Parse un fichier xlsx et retourne le contenu lignes / colonnes
	 *  @param  string $xlsxFile : chemin vers le fichier a parser OU flux si $isStream = true
	 *  @param  bool $isStream : définit si l'entrée est un fichier ou un flux
	 * 	@param  int $sheetIndex : numero de l'onglet excel a parser (commence a 0)
	 * 	@param  bool $cellInfos : si passé a true, ajoute des infos de cellule (type etc...) plus lent.
	 */
	
	public static function parse($xlsxFile,$isStream = false,$sheetIndex=1,$cellInfos = false) {
		$xlsx = SimpleXLSX::parse($xlsxFile,$isStream);
		if(!$xlsx)  throw new Exception(SimpleXLSX::parseError());
		if($cellInfos){
			$rows = $xlsx->rowsEx($sheetIndex);
		}else{
			$rows = $xlsx->rows($sheetIndex);
		}
		return $rows;
	}
	
	/** 
	 * Retourne le contenu d'un tableau convertit au format XLSX
	 * @param  array $rows      le tableau avec les lignes à exporter dans le fichier
	 * @param  array $mapping   le mapping qui permet d'ajouter des libellé en fct des clés de colonne
	 * Structure du tableau de mapping attendu : aaray(
	 * 		'Label' => 'clé',
	 *   	'Autre label' => 'autre clé',
	 *    	etc...
	 * );
	 * @param  string $sheetname le nom à donner à la feuille de calcul courante
	 * @return string            le stream du fichier généré
	 */
	public static function exportArray($rows, $mapping, $sheetname='Classeur 1'){ 
		$data = $header = array();
		$style = array(
			'border'	=> 'top,left,bottom,right',
			'border-style' => 'thin'
		);

		if(empty($mapping) && isset($rows[0])){
			foreach($rows[0] as $key=>$value){
				$mapping[$key] = $key;
			}
		}

		foreach ($rows as $key => $item) {
			$itemContent = array();
			if (isset($mapping)) {
				foreach ($mapping as $label => $attributes) {
					$slug = is_array($attributes) ? (isset($attributes['slug']) ? $attributes['slug'] : '') : $attributes;
					$header[$label] = (is_array($attributes) && isset($attributes['type'])) ? $attributes['type'] : 'string';
					isset($item[$slug]) ? array_push($itemContent, $item[$slug]) : array_push($itemContent, '');
				}
			} else {
				foreach ($item as $field => $value) {
					array_push($itemContent, $item[$field]);
				}
			}
			array_push($data, $itemContent);
		}	

		ob_start();
		$excel = new XLSXWriter();
		$excel->writeSheetHeader($sheetname, $header, array_merge($style, array('font-style'=> 'bold','border-color' => '#2a2a28')));
		foreach($data as $row)
			$excel->writeSheetRow($sheetname, $row, array_merge($style, array('border-color' => '#cecece')));

		$excel->writeToStdOut();
		$output = ob_get_clean();
		return $output;
	}
	
	/**
	 * Export de données sur plusieurs feuilles Excel
	 * @param array $content 	[Contenu à exporter]
	 * Format du tableau de contenu attendu :
	 * array(
	 * 		'Classeur 1' => array(
	 *   		'mapping' => array(
	 *     			'Libellé' => 'slug',
	 *     			'Libellé' => 'slug',
	 *     			'Libellé' => 'slug'
	 *       	),
	 *        	'rows' => array(
	 *         		0 => objet/entité exporté,
	 *         		1 => objet/entité exporté,
	 *         		2 => objet/entité exporté
	 *         	)
	 *    	),
	 *      'Classeur 2' => array(
	 *    		'mapping' => array(
	 *      			'Libellé' => 'slug',
	 *      			'Libellé' => 'slug',
	 *      			'Libellé' => 'slug'
	 *        	),
	 *         	'rows' => array(
	 *          		0 => objet/entité exporté,
	 *          		1 => objet/entité exporté,
	 *          		2 => objet/entité exporté
	 *          	)
	 *     	), etc...
	 * );
	 * On place généralement $response['rows'] dans l'index 'rows' d'une feuille Excel
	 * 
	 * @param string $filename 	[Nom du fichier à exporter]
	 * 
	 * @return string 			[Fichier XSLX]
	 */
	public static function exportMultipleSheets($content){ 
		$commonStyle = array('border'=> 'top,left,bottom,right', 'border-style'=> 'thin');
		$headerStyle = array_merge($commonStyle, array('font-style'=> 'bold','border-color'=> '#2a2a28'));
		$contentStyle = array_merge($commonStyle, array('border-color'=> '#cecece'));
		$styles = array(
			'header' => $headerStyle,
			'content'=> $contentStyle
		);

		$excel = new XLSXWriter();
		foreach ($content as $sheetName => $sheetContent) {
			$data = $header = array();

			foreach ($sheetContent['rows'] as $item) {
				$itemContent = array();

				if (isset($sheetContent['mapping'])) {
					foreach ($sheetContent['mapping'] as $label => $attributes) {
						$slug = is_array($attributes) ? (isset($attributes['slug']) ? $attributes['slug'] : '') : $attributes;
						$header[$label] = (is_array($attributes) && isset($attributes['type'])) ? $attributes['type'] : 'string';
						isset($item[$slug]) ? array_push($itemContent, $item[$slug]) : array_push($itemContent, '');
					}
				} else {
					foreach ($item as $field => $value) {
						array_push($itemContent, $item[$field]);
					}
				}
				array_push($data, $itemContent);
			}
			$excel->writeSheet($data, $sheetName, $header, $styles);
		}
		ob_start();
		$excel->writeToStdOut();
		$output = ob_get_clean();

		return $output;
	}
}

