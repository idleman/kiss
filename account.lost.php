<?php 
require_once (__DIR__.DIRECTORY_SEPARATOR.'header.php');
global $conf,$myUser,$myFirm,$_;
if($myUser->connected()) throw new Exception("Vous devez être déconnecté pour accéder à cette fonctionnalité");

$_['section'] = !isset($_['section']) ? 'global': $_['section'];
$accountMenu = array();
$page = basename($_SERVER['PHP_SELF']);

if(isset($_['token'])){
	$tokenInfos = explode('::',base64_decode($_['token']));
	if(count($tokenInfos)!=3) throw new Exception("Token expiré ou inexistant");
	list($login,$token,$step) = $tokenInfos;

	$baseToken = UserPreference::load(array('user'=>$login,'key'=>'lost_password'));
	if(!$baseToken || $baseToken->value!=$token) throw new Exception("expiré ou inexistant");
	if(!empty($conf->get('password_lost_mail_expire')) && (time()-$baseToken->created) > strtotime(intval($conf->get('password_lost_mail_expire')).' days', 0)) throw new Exception("Lien de récupération expiré");
	
	if(!$user = User::load(array('login'=>$login))) throw new Exception("Token expiré ou inexistant");
}
?>
<div class="row justify-content-md-center">
	<div class="col-lg-7 col-xl-5 text-center mt-3">
		<div class="row justify-content-center">
		<?php if(isset($_['token'])):
			switch($step) {
				case 1:
					?>
					<h5 class="text-muted text-uppercase col-xl-12"><i class="fas fa-unlock mr-1"></i>Récupération de mot de passe</h5>
					<div class="col-lg-xl-12 my-4">
						<h5>Bienvenue <?php echo $user->fullName(); ?>!</h6>
						<p class="mt-4 mb-2">Veuillez récupérer votre nouveau mot de passe en cliquant sur le bouton ci-dessous :</p>
						<a class="btn btn-primary w-100" href="<?php echo ROOT_URL.'/account.lost.php?token='.base64_encode($user->login.'::'.$token.'::2'); ?>"><i class="fas fa-fw fa-key mr-1"></i>Récupérer le mot de passe</a>
					</div>
					<?php
				break;

				case 2:
					//Suppression de l'indicateur de réinitialisation de mot de passe
					UserPreference::deleteById($baseToken->id);

					//Génération du nouveau mot de passe
					$newPassword = User::generate_password(12);
					$user->password = User::password_encrypt($newPassword);
					$user->save();
					?>
					<h5 class="text-muted text-uppercase col-xl-12"><i class="fas fa-unlock mr-1"></i>Mot de passe réinitialisé</h5>
					<p class="mt-2 px-3">Bonjour, <?php echo $user->fullName(); ?>,<br>vous pouvez dès à présent vous connecter avec les éléments suivants :</p>
					<ul class="list-unstyled px-5 text-left">
				        <li><strong>Identifiant :</strong> <code class="font-weight-bold bg-light p-2 pointer" onclick="select_text(this, event);copy_string($(this).text(), this);"><?php echo $user->login; ?></code></li>
				        <li><strong>Mot de passe : </strong><code class="font-weight-bold bg-light p-2 pointer" onclick="select_text(this, event);copy_string($(this).text(), this);"><?php echo $newPassword; ?></code></li>
				    </ul>
				    <a href="index.php" class="btn btn-success mt-2 w-100" ><i class="fas fa-home"></i> Retour à l'accueil</a>
				    <?php 
				break;
				default:
					throw new Exception("Token expiré ou inexistant");
				break;
			} 
		else: ?>
			<h5 class="text-muted text-uppercase col-xl-12"><i class="fas fa-unlock mr-1"></i>Mot de passe oublié</h5>
			<p class="col-xl-12 my-4">
				Veuillez remplir le formulaire ci dessous pour procéder au changement de mot de passe.<br>
				Un e-mail sera envoyé à la boite spécifiée si celle ci est liée à un compte.
			</p>
			<div class="col-xl-6 col-lg-8">
				<span class="d-block w-100 mb-1"><i class="far fa-fw fa-envelope mr-1"></i><small class="text-muted">Adresse e-mail du compte :</small></span>
				<input class="form-control m-auto text-center" placeholder="email@email.com" type="text" id="mail">
				<div class="btn btn-primary w-100 mt-2" id="lost-password-send" onclick="core_account_lost_password(this);"><i class="far fa-envelope-open"></i> Récupérer</div>
			</div>
		<?php endif; ?>
		</div>
	</div>
</div>
<?php require_once (__DIR__.DIRECTORY_SEPARATOR.'footer.php'); ?>
