<?php
require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'common.php');
global $myUser,$lastQuery,$db,$steps;
ini_set('max_execution_time', 36000);
ob_implicit_flush(true);
ini_set('memory_limit', '2048M');
setlocale( LC_ALL, "fr_FR.utf8" );

if(php_sapi_name() != 'cli')throw new Exception("Permission denied");



// Utilisation du script : lancer sous windows ou linux avec la commande :
//php actionmigration.php ../plugin/document/action.php
// le premier parametre est le chemin relatif vers le fichier action a convertir

if(!isset($argv[1])) exit('Veuillez spécifier un chemin relatif du fichier a convertir ex : php actionmigration.php ../plugin/document/action.php');
if(!file_exists($argv[1])) exit('Le fichier action "'.$argv[1].'" n\'existe pas');

$file = $argv[1];
$stream = file_get_contents($file);


$regex = '/case \'([^\']*)\':[\s|\t|\r|\n]*Action::write\(function\(&\$response\)\{(.*)\}\);[\s|\t|\r|\n]*break;/iUs';

$stream = preg_replace($regex, 'Action::register(\'$1\',function(&$response){
	$2
	});', $stream);

$stream = file_put_contents($file.'.converted',$stream);


echo 'Fichier convertis `sur le chemin '.$file.'.converted';
?>