<?php

//Fonction executée lors de l'activation du plugin
function customiser_install($id){
	if($id != 'fr.core.customiser') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function customiser_uninstall($id){
	if($id != 'fr.core.customiser') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('customiser',array('label'=>'Gestion des droits sur le plugin customiser'));


//Cette fonction comprends toutes les actions 
//du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');

//Déclaration du menu de réglages
function customiser_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('customiser','configure')) return;
	$settingMenu[]= array(
		'sort' => 1,
		'url' => 'setting.php?section=global.customiser',
		'icon' => 'fas fa-angle-right',
		'label' => 'Thème'
	);
}

//Déclaration des pages de réglages
function customiser_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

global $conf;

if(!empty($conf->get('core_theme'))){
	Plugin::addCss($conf->get('core_theme'),array('forcePath'=>true));
	$jsFile = str_replace('.css','.js',$conf->get('core_theme'));
	if(file_exists(__ROOT__.SLASH.$jsFile)) Plugin::addJs($jsFile,array('forcePath'=>true));
}
//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "customiser_install");
Plugin::addHook("uninstall", "customiser_uninstall"); 

Plugin::addHook("menu_setting", "customiser_menu_setting");    
Plugin::addHook("content_setting", "customiser_content_setting");   
 

?>