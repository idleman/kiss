<?php

Action::register('customiser_theme_save',function(&$response){
	global $myUser,$_,$conf;
	User::check_access('customiser','configure');
	$conf->put('core_theme',$_['theme']);
});

//Theme : Gestion upload Zip theme
Action::register('customiser_theme_zipTheme',function(&$response){
	User::check_access('customiser','configure');
	File::handle_component(array(
		'namespace' => 'customiser', //stockés dans file/customiser/*.*
		'access' => 'customiser', // crud sur customiser,
		'size' => '1000000000', // taille max
		'extension' => 'zip', 
		'storage' => 'theme/upload.zip'
	),$response);
});

Action::register('customiser_theme_install',function(&$response){
	global $_,$myUser ;
	require_once(__DIR__.SLASH.'Theme.class.php');
	
	User::check_access('customiser','configure');
	if(empty($_['file'])) return;

	try{
		File::save_component('file', 'theme/upload.zip');
		$zipFile = File::dir().SLASH.'theme/upload.zip';

		$zip = new ZipArchive();
		if(!$zip->open($zipFile)) throw new Exception("Impossible d'ouvrir l'import");
		
		$app = $zip->getFromName('app.json');
		if(!$app) throw new Exception('Fichier app.json manquant à la racine de l\'archive');
		
		$app = json_decode($app,true);
		$folderId = str_replace(array('..','/','\\'),array('.',''),$app['id']);
		$templateDir = Theme::dir().$folderId;

		if(file_exists($templateDir)) throw new Exception('Le thème '.$folderId.' est déja installé sur cette application');

		mkdir($templateDir,0755,true);

		unzip($zipFile,$templateDir,array('allowedExtensions'=>array('css','json','jpg','png','bmp','jpeg','webp','woff','woff2','eot','ttf','svg')));

	}catch(Exception $e){
		if(file_exists($zipFile)) unlink($zipFile);
		throw $e;
	}

});



?>