//CHARGEMENT DE LA PAGE
function init_plugin_customiser(){
	switch($.urlParam('page')){
		default:
		break;
	}
}

function customiser_preview(element){
	var card = $(element);
	var css = card.attr('data-css');
	var head = $('head');

	//Reset des cards
	$('.theme-cover.active').removeClass('active');
	$('.theme-cover span.info').text('Activer le thème');

	//Passe la card courante en active
	$('.theme-cover', card).addClass('active');
	$('span.info', card).text('Thème actif');

	$('link[href*="customiser/theme"]', head).remove();
	$('#customiser-preview').remove();
	if(css!='') head.append('<link rel="stylesheet" id="customiser-preview" type="text/css" href="'+css+'">');
}

function customiser_theme_install(){
	var file = JSON.parse($('#zipTheme').val());
	file = file[0];

	var data = {
		file: [{
			extension: file.extension,
			icon: file.icon,
			label: file.label,
			path: file.path,
			readableSize: file.readableSize,
			size: file.size,
			sort: file.sort,
			url: file.url,
		}],
		action : 'customiser_theme_install'
	};
	
	$.action(data,function(){
		$.message('success','Installé');
		$('#zipTheme').val('');
		init_components($('#zipTheme').parent());
	},function(){
		$('#zipTheme').val('');
		init_components($('#zipTheme').parent());
	});
}

function customiser_theme_save(){
	$.action({ 
		action : 'customiser_theme_save', 
		theme :  $('[name="selected-theme"]:checked').val() 
	},function(){
		$.message('success','Enregistré');
	});
}