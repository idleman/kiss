<?php
global $myUser,$conf;
User::check_access('customiser','configure');
require_once(__DIR__.SLASH.'Theme.class.php');

?>

<div class="row customiser">
	<div class="col-md-12"><br>
		<div onclick="customiser_theme_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
		<h3>Réglages Thème</h3>
		<hr>
		<div class="row row-cols-xl-4 row-cols-lg-3 row-cols-md-2 row-cols-sm-1">
		<?php foreach(Theme::getAll() as $i => $theme):
			$cover = 'media/theme/';
			if(isset($theme['cover']) && !empty($theme['cover'])){
				$cover .=   (basename($theme['folder']).'/public/'.$theme['cover']) ;
			}else{
				$cover = 'plugin/customiser/img/default-cover.jpg';
			}
		
			?>
			<div class="col mb-4">
				<label class="card pointer h-100" data-css="<?php echo ROOT_URL.$theme['css-relative-url']; ?>" onclick="customiser_preview(this);">
					<div class="theme-cover <?php echo $theme['checked']?'active':''; ?>">
				        <img src="<?php echo $cover; ?>" class="card-img-top border-bottom border-light" alt="Image de preview du thème">
				        <div class="overlay">
				           <h2 class="m-0"><?php echo $theme['label']; ?></h2>
				           <span class="info"><?php echo $theme['checked'] ? 'Thème actif' : 'Activer le thème'; ?></span>
				        </div>
				    </div>
					<div class="card-footer h-100 position-relative p-2">
						<small class="text-muted d-block mb-1"><i class="far fa-meh-blank"></i> <?php echo isset($theme['author']) ? $theme['author']['name'] : 'Auteur inconnu'; ?></small>
						
						<?php if($theme['last-update']==0): ?>
						<small class="text-muted d-block">Aucun thème</small>
					    <?php else: ?>
					    <small class="text-muted d-block" title="<?php echo 'Mis à jour le '.date("d/m/Y H:i", $theme['last-update']); ?>"><i class="far fa-calendar-alt"></i> <?php echo relative_time($theme['last-update']); ?></small>
						<?php endif; ?>
						<div>
							<input type="radio" data-type="radio" <?php echo $theme['checked'] ? "checked='checked'" : ""; ?> value="<?php echo $theme['css-relative-url']; ?>" name="selected-theme">
						</div>
				    </div>
				</label>
			</div>
		<?php endforeach; ?>
			<div class="col mb-4">
				<label class="card pointer h-100" >
					<h5 class="bg-dark text-light text-center p-3 mb-0">INSTALLER UN THEME</h5>
					<p class="bg-dark text-light text-center mb-2">Fichier <span class="font-weight-bold">.zip</span> accepté</p>
					<input  class="component-file-default bg-white shadow-sm rounded-sm"  type="text" onchange="customiser_theme_install();"  data-type="file"  data-extension="zip"  data-action="customiser_theme_zipTheme"  data-id="zipTheme" id="zipTheme" >
			
				</label>
			</div>
		</div>
	</div>
</div>

