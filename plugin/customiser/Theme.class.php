<?php
/**
 * Manage themes
 * @author Valentin carruesco
 * @category Plugin
 * @license MIT
 */
class Theme{

	public static function dir(){
		return File::dir().'theme'.SLASH;
	}

	public static function getAll(){
		global $conf;
		$themes = array(
		array(
				"label" => "Aucun thème",
				"folder" => "",
				"css-relative-url" => "",
				"path" => "",
				"checked" => $conf->get('core_theme') == "",
				"last-update" => 0
			)
		);

		$dir = self::dir();
		if(!file_exists($dir)) mkdir($dir,0755,true);
			
		

		foreach(glob($dir.SLASH.'*'.SLASH.'app.json') as $themeFile){
			$path = dirname($themeFile);
			$theme = json_decode(file_get_contents($themeFile),true);
			$theme['folder'] = $path;
			$theme['css-relative-url'] = '/media/theme/'.basename($path).'/public/main.css';
			$theme['checked'] = $conf->get('core_theme') == $theme['css-relative-url'];
			$theme['last-update'] = filemtime($themeFile);
			$themes[] = $theme;
		}
		return $themes;
	}
}
?>