<?php 
global $_,$myUser;

if(!$myUser->connected()) throw new Exception("Connexion requise", 401);
if(!isset($_['url'])) throw new Exception("Url d'entrée non définie");

$url = base64_decode($_['url']);
	try{
		$request = @get_headers($url, true);
		$error = '';
		if(!$request) $response['state'] = false; 

		if(is_array($request) && isset($request[0])){
			$headers = array();
			// Make more sane response
		    foreach($request as $h => $v)
		    {
		        if(is_int($h))
		            $headers[$h]['Status'] = $v;
		        else {
		            if(is_string($v)) $headers[0][$h] = $v;
		        }
		    }
		    $lastHeader = end($headers);

			if(!preg_match('/ 200 /i',$lastHeader['Status'])) throw new Exception($lastHeader['Status']);
		}else{
			throw new Exception('HTTP/1.1 404 Not Found'); 
		}

	}catch(Exception $e){
		$code = 0;
		list($protocol,$code,$sentence) = explode(' ',$e->getMessage()); 
		
		$mapping = array(
			400 => 'La syntaxe de la requête est erronée.',
			401 => 'Utilisateur non authentifié',
			402 => 'Paiement requis pour accéder à la ressource.',
			403 => 'Accès refusé',
			404 => 'Adresse introuvable ou momentanément indisponible',
			495 => 'Problème de certificat SSL',
			498 => 'Le jeton a expiré ou est invalide.',
			500 => 'Le serveur rencontre des erreurs internes',
			502 => 'En agissant en tant que serveur proxy ou passerelle, le serveur a reçu une réponse invalide depuis le serveur distant.',
			503 => 'Service temporairement indisponible ou en maintenance.',
		);
		$error = isset($mapping[$code]) ? $mapping[$code] : 'raison indéfinie ('.$code.' - '.$sentence.')';
	}
	
if(!empty($error)){ ?>
	<div class="alert alert-warning m-3" role="alert">
  		<strong>Erreur</strong> Le portail <a href="<?php echo $url; ?>" class="alert-link"><?php echo $url; ?></a> n'est pas accessible. Raison : <?php echo $error; ?>
	</div>
<?php }else{ ?>
	<iframe src="<?php echo $url; ?>" style="width:100%;height:100%;border:0" frameborder="0"></iframe>
<?php } ?>