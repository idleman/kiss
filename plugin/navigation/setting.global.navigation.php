<?php
global $myUser,$conf,$myFirm;
if(!$myUser->connected()) throw new Exception("Vous devez être connecté pour accéder à cette fonctionnalité",401);
require_once(__DIR__.SLASH.'MenuItem.class.php');
if($conf->get("navigation_allow_custom_menu")==0 && !$myUser->can('navigation','configure')) throw new Exception("Vous n'avez pas la permission d'afficher cette page", 403);
?>

<div class="row">
	<div class="col-md-12">
		<br>
        <div onclick="navigation_setting_save();" class="btn btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Navigation</h3>
        <hr>
	</div>
</div>

<div class="row">
    <div class="col-md-12" id="notification_preference"> 
        <div class="tab-container mb-0 noPrint">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#tab-menus" aria-controls="tab-menus" aria-selected="false">Menus</a></li>
                <?php if($myUser->can('navigation','configure')): ?>
                <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-settings" aria-controls="tab-settings" aria-selected="false">Paramètres</a></li>
                <?php endif; ?>
            </ul>
        </div>
        
        <div class="tab-content">
            <!-- Onglet Menus -->
            <div class="tab-pane show active in" id="tab-menus" role="tabpanel" aria-labelledby="tab-menus"><br>
            	<div class="col-md-12 navigation-tree p-2">
                    <h5 class="pt-1 d-inline-block m-0">Arborescence du menu 
                    	<select class="navigation-menu-selector text-primary" id="navigation-menu" onchange="navigation_tree_search()">
                    		<?php $filters = array('menu'=>'0');
                    		
                            if($myUser->can('navigation','configure')){
                    			//l'admin menu voit ses menu et les menu non attaché a un user
                    			$filters['user:IN'] = ','.$myUser->login;
                    		}else{
                    			//les non admins menu ne voient que leurs menus
                    			$filters['user'] = $myUser->login;
                    		}
                    		$menus = MenuItem::loadAll($filters, array('id'));

                            //Récuperation du menu raccourcis du user
                            $shortcutMenu = array_filter($menus, function($value) use ($myUser){
                                return $value->user==$myUser->login && $value->slug==MenuItem::MENU_SHORTCUT;
                            });
                            $shortcutMenu = !empty($shortcutMenu) ? $shortcutMenu[array_key_first($shortcutMenu)] : false;

                    		//On créé un menu raccourcis pour l'utilisateur courant si celui ci n'existe pas
                    		if(!$shortcutMenu){
                    			$shortcutMenu = new MenuItem();
                    			$shortcutMenu->label = 'Menu raccourcis';
                    			$shortcutMenu->slug = MenuItem::MENU_SHORTCUT;
                    			$shortcutMenu->user = $myUser->login;
                    			$shortcutMenu->menu = '0';
                    			$shortcutMenu->save();
                    			$menus[] = $shortcutMenu;
                    		}
                    		foreach($menus as $menu): ?>
                    			<option <?php echo (!empty($_['menu']) && $_['menu']=="mine" && $shortcutMenu->id==$menu->id ? 'selected="selected"': ''); ?> value="<?php echo $menu->id; ?>" data-slug="<?php echo $menu->slug; ?>"><?php echo $menu->label ?></option>
                    		<?php endforeach; ?>
                    	</select> pour l'établissement 
                        <select class="navigation-firm-selector text-primary" id="navigation-firm" onchange="navigation_menu_setting_search()">
                            <?php 
                            $firms = $myUser->firms;
                            $allFirms = new Firm();
                            $allFirms->id = 0;
                            $allFirms->label = 'Tous les établissements';
                            if($myUser->superadmin) $firms[] = $allFirms;
                            foreach ($firms as $firm):
                                 ?> <option <?php echo ($myFirm->id == $firm->id ? "selected='selected'":"") ?> value="<?Php echo $firm->id; ?>"><?php echo $firm->label; ?></option>
                                 <?php endforeach ?>
                        </select>





                    </h5>
                    <div class="d-inline-block float-right">
                        <div onclick="navigation_tree_new();" class="btn btn-info"><i class="fas fa-plus"></i> Nouvel élément</div>
                        <div class="btn btn-light" data-toggle="modal" data-target="#navigation-page-modal"><i class="fas fa-plus"></i> Ajouter une page existante</div>
                    </div>
                    <hr>
                    <ul class="navigation-tree-list">
                    	<li class="navigation-line {{class}} hidden" data-id="{{id}}" >
                    		<div class="navigation-item">
                    			<div class="navigation-item-header">
            	        			<span class="navigation-item-icon"><i class="{{icon}} fa-fw"></i></span> 
            	        			<h1 class="navigation-item-label">{{label}}</h1> 
                                    <h3 class="navigation-item-type navigation-item-toggle pointer text-uppercase d-inline-flex" onclick="navigation_tree_toggle(this)" title="Afficher/Cacher" >
                                        <span class="m-auto">{{target.label}}</span>
                                        <i class="ml-1 fas fa-angle-right"></i>
                                    </h3> 
            	        			<small class="navigation-item-url">{{#url}}- {{url}}{{/url}}</small>
                    			</div>

            					<div class="navigation-item-form row hidden">
                                    <div class="col-md-3">
                						<h4>Titre :</h4>
                						<div class="input-group mb-1">
                							<input type="text" class="item-icon" data-type="icon" value="{{icon}}">
                							<input type="text" class="form-control item-label" value="{{label}}" placeholder="Libellé du lien">
                						</div>
                                    </div>
                                    <div class="col-md-9">
                                        <h4>Adresse :<small class="text-muted"> - Optionnel</small></h4>
                                        <div class="input-group mb-1">
                                            <input type="text" class="form-control item-url" value="{{url}}"  placeholder="Adresse du lien">
                                        </div>
                                    </div>
                                    {{^shortcut}}
                                    <div class="col-md-3">
                                        <h4>Visibilité :<small class="text-muted"> - Laisser vide pour public</small></h4>
                						<input type="text" class="item-visibility form-control" data-type="user-tpl" data-types="rank,user" data-multiple="true" value="{{visibility}}" data-default="<?php echo $conf->get('default_menu_visibility') ?>">
                                    </div>
                                    {{/shortcut}}
                                    <div class="{{#shortcut}}col-md-12{{/shortcut}}{{^shortcut}}col-md-9{{/shortcut}}">
                                        <h4>Type d'ouverture :<small class="text-muted"> - Optionnel</small></h4>
                                        <!-- la balise form est obligatoire pour contrer le bug de jquery ui sortable, ne pas enlever -->
                                        <form>
                                            <?php foreach (MenuItem::target() as $slug => $target): ?>
                                            <label class="m-0"><input data-type="radio" name="item-target" value="<?php echo $slug; ?>" data-value="{{target.slug}}"> <i class="<?php echo $target['icon']; ?>"></i> <?php echo $target['label']; ?></label>
                                            <?php endforeach;  ?>
                                        </form>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="navigation-item-delete text-danger btn btn-light float-right p-1" onclick="navigation_tree_remove(this)" title="Supprimer l'élément de menu"><i class="far fa-trash-alt"></i> Supprimer</div>
                                        <div class="clear"></div>
                                    </div>
            					</div>
                    		</div>
                    	</li>
                    </ul>
             	</div>
             </div>
            
            <?php if($myUser->can('navigation','configure')): ?>
            <div class="tab-pane" id="tab-settings" role="tabpanel" aria-labelledby="tab-settings"><br>
                <div class="">
                    <?php echo Configuration::html('navigation'); ?>
                </div>
            </div>
            <?php endif; ?>

         </div>
    </div>
</div>


<!-- Modal page interne -->
<div class="modal fade" id="navigation-page-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pages existantes :</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                <?php 
                $pages = array();
                Plugin::callHook("menu_main", array(&$pages));
                uasort ($pages , function($a,$b){return $a['sort']>$b['sort']?1:-1;});
                foreach($pages as $page): 
                	$icon = isset($page['icon'])? $page['icon'] : 'far fa-bookmark';
                ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center" data-label="<?php echo $page['label']; ?>"  data-icon="<?php echo $page['icon']; ?>" data-url="<?php echo $page['url']; ?>">
                    	<div>
                    	<i class="<?php echo $icon; ?>"></i> <?php echo $page['label']; ?> <a class="text-muted" href="<?php echo $page['url']; ?>"> -<?php echo $page['url']; ?></a>
                    	</div>
                    	<span class="btn btn-squarred btn-mini btn-success" title="Ajouter la page <?php echo $page['label']; ?> au menu" onclick="navigation_tree_new(this);"><i class="fas fa-plus"></i></span>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>