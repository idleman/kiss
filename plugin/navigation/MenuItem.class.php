<?php
/**
 * Define a menuitem.
 * @author Valentin CARRUESCO
 * @category Plugin
 * @license MIT
 */
class MenuItem extends Entity{
	public $id,$icon,$label,$url,$classes,$sort,$firm,$parent,$user,$target,$editable,$slug,$childs,$visibility,$menu;
	public $TABLE_NAME = 'menuitem';
	public $fields =
	array(
		'id' => 'key',
		'icon' => 'string',
		'label' => 'string',
		'slug' => 'string',
		'url' => 'longstring',
		'visibility' => 'longstring',
		'classes' => 'string',
		'sort' => 'int',
		'firm' => array('type'=>'firm','label'=>'Etablissement', 'link'=>'class/Firm.class.php'),
		'user' => 'string',
		'target' => 'string',
		'editable' => 'int',
		'parent' => 'int',
		'menu' => 'int'
	);

	const MENU_SHORTCUT = 'shortcut-menu';

	public $indexes = array('slug','menu');


	function __construct(){
		parent::__construct();
		$this->editable = true;
		$this->menu = 0;
		$this->user = '';
	}

	public static function target($slug=null){
		$targets = array(
			'redirect' => array('label'=>'Redirection','slug'=>'redirect','icon'=>'fas fa-link'),
			'_blank' => array('label'=>'Redirection  (nouvel onglet)','slug'=>'_blank','icon'=>'fas fa-link'),
			'iframe' => array('label'=>'IFrame','slug'=>'iframe','icon'=>'fas fa-crop'),
		);

		$undefined = array('label'=>'Non défini','slug'=>'','icon'=>'');
		if(!isset($slug)) return $targets;

		return isset($targets[$slug]) ? $targets[$slug] : $undefined;
	}

	public function visibleFor($user){
		if(!isset($this->visibility) || empty($this->visibility) || $user->superadmin) return true;
		$visibility =  explode(',', $this->visibility);
		
		foreach($visibility as $entity){
			if(is_numeric($entity)){
				if($user->hasRank($entity)) return true;
			}else{
				if($user->login==$entity) return true;
			}
		}
		return false;
		
	}

	public static function bySlug($menuSlug,$user='',$firms = array()){
		$menuItems = array();

		$mainMenu = array();

		
		$query = 'SELECT it.*,me.slug as parentSlug FROM {{table}} it LEFT JOIN {{table}} me ON it.menu=me.id WHERE me.user=?';
		$data = array($user);
		
		if(is_array($menuSlug)){

			$query .= ' AND me.slug IN ('.implode(',',array_fill(0,count($menuSlug),'?')).') ';
			foreach($menuSlug as $slug) $data[] = $slug;
		}else{
			$query .= ' AND me.slug=? ';
			$data[] = $menuSlug;
		}

		if(!empty($firms)){
			$query .=' AND it.firm IN ('.implode(',',array_fill(0, count($firms), '?')).') ';
			foreach($firms as $firm)
				$data[] = $firm;
		}

		$query .= ' ORDER BY sort';

		
		foreach(self::staticQuery($query,$data ,true) as $dbItem){
			$item = $dbItem->toArray();
			$item['foreign'] = $dbItem->foreign();
			$mainMenu[] = $item; 
		}

		uasort ($mainMenu , function($a,$b){return $a['sort']>$b['sort']?1:-1;});
		foreach ($mainMenu as $i=>$item) {
			if(isset($item['parent'])) continue;
			$menuItems[$item['id']] = $item;
			$menuItems[$item['id']]['childs'] = array();
			unset($mainMenu[$i]);
		}

		foreach ($mainMenu as $item) {
			$menuItems[$item['parent']]['childs'][] = $item;
		}

		
		if(is_array($menuSlug)){
			//tri par slug
			$menuItemsBySlug = array();
			foreach($menuSlug as $slug){
				$menuItemsBySlug[$slug] = array();
				foreach($menuItems as $item){
					if($item['foreign']['parentSlug']!=$slug) continue;
					$menuItemsBySlug[$slug][] = $item;
				}
			}
			return $menuItemsBySlug;
		}else{
			return $menuItems;
		}
		
	}


}
?>