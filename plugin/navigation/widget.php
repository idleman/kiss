<?php 
global $myUser;

$icon = empty($widget->data('icon')) ? 'far fa-bookmark' : $widget->data('icon');
$color = empty($widget->data('color'))? '#ffffff': $widget->data('color');

$url = navigation_meta_link($widget->data('url'));
$title = navigation_meta_link($widget->data('title'));

?>
<div style="background: <?php echo $color; ?>" class="widgetNavigationContainer">
	<?php  if(!empty($widget->data('title'))): ?>
		<a href="<?php echo $url; ?>" <?php if(!empty($widget->data('redirect'))) echo 'target="_blank"'; ?> >
			<i class="<?php echo $icon; ?>"></i>
			<?php echo $title; ?>
		</a>
	<?php else: ?>
		<h4 class="noContent"><i class="far fa-compass"></i> Aucun menu spécifié</h4>
	<?php endif; ?>

</div>