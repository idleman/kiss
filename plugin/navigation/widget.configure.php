<div id="navigation-widget-form">
	<label for="">Icone / Titre / Couleur :</label>
	<div class="input-group mb-2">
		<input class="form-control" type="text" data-type="icon" value="<?php echo empty($widget->data('icon'))?'far fa-bookmark':$widget->data('icon');  ?>" id="widget-icon">
		<input class="form-control" type="text" value="<?php echo $widget->data('title'); ?>" id="widget-title">
		<input class="form-control" type="text" data-type="color" value="<?php echo empty($widget->data('color'))?'#000000':$widget->data('color'); ?>" id="widget-color">
	</div>
	<label for="">Adresse URL :</label>
	<div class="input-group mb-2">
		<div class="input-group-prepend">
			<div class="input-group-text">
				<label for="widget-redirect" class="pointer mb-0">Redirection</label>
		    	<input data-type="checkbox" name="widget-redirect" id="widget-redirect" <?php echo $widget->data('redirect') ? 'checked' : ''; ?>>
		    </div>
		</div>
		<input class="form-control text-success" type="text" value="<?php echo $widget->data('url'); ?>" id="widget-url">
		<div class="input-group-append">
			<a href="<?php echo $widget->data('url'); ?>" target="_blank" class="text-success text-decoration-none input-group-text"><i class="fas fa-globe"></i></a>
		</div>
	</div>
</div> 