function navigation_widget_configure_init(){
	var widgetTitle = $('#widget-title');
	$('#widget-title').autocomplete({
		action : 'navigation_widget_configure_autocomplete',
		skin : function(item){
			var html = '';
			var re = new RegExp(widgetTitle.val(),"gi");

			name = item.name.replace(re, function (x) {
				return '<strong>'+x+'</strong>';
			});

			html += '<h5 class="mt-1"><i class="'+item.icon+'"></i> <span>'+name+'</span></h5>'; 
			html +='<small>'+item.url+'</small></div>';
			html += '<div class="clear"></div>';
			return html;
		},
		highlight : function(item){
			return item;
		},
		onClick : function(selected,element){
			widgetTitle.val(selected.name);
			$('#widget-url').val(selected.url);
			$('#widget-icon').val(selected.icon);
			init_components();
		}
	});
}

function navigation_widget_configure_save(widget,modal){
	var data = $('#navigation-widget-form').toJson();
	data.action = 'navigation_widget_configure_save';
	data.id = modal.attr('data-widget');
	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}