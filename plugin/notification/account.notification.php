<?php 
global $myUser;
global $conf;
User::check('notification', 'read');
require_once(__DIR__.SLASH.'Notification.class.php');

$myUser->loadPreferences();
$userPreferences = Notification::settingPreferences($myUser->login);
$categories = Notification::categories()
?>
<div class="row">
	<div class="col-md-12" id="notification_preference">
		<br>
		<div onclick="notification_user_save_preference()" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
		<h3>Notifications</h3>
		<div class="clear"></div>
		<hr>
		<div class="row">
			<div class="col-md-12 notification_categories" id="notification_categories">
				<h5>Suivre les catégories : </h5>
				<div class="row">
				  <div class="col-3">
				    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				    <?php 
				    	$i = 0;
				    	foreach($categories as $categorySlug=>$category): ?>
					    	<a class="nav-link <?php echo $i==0?'active':'' ?>" id="v-pills-home-tab" data-toggle="pill" href="#category-<?php echo $categorySlug ?>" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="far fa-bookmark"></i> <?php echo $category['label'] ?></a>
					   		<?php 
					   		$i++;
				   		endforeach; ?>
				    </div>
				  </div>
				  <div class="col-9">
				    <div class="tab-content" id="v-pills-tabContent">
				     <?php 
				     $i = 0;
				     foreach($categories as $categorySlug=>$category): ?>
				      <div class="tab-pane fade show <?php echo $i==0?'active':'' ?>" id="category-<?php echo $categorySlug ?>" role="tabpanel" aria-labelledby="v-pills-home-tab">
							<?php foreach($category['items'] as $type): ?>
								<div data-slug="<?php echo $type['slug']; ?>" class="list-group-item list-group-item-action flex-column align-items-start category user-select-none">
									<div class="d-flex w-100 justify-content-between">
										<h5 class="mb-1" style="color:<?php echo $type['color']; ?>;"><i class="<?php echo $type['icon']; ?>"></i> <?php echo $type['label']; ?></h5>
									</div>
									<p class="mb-3"><?php echo $type['description']; ?>.</p>
									<ul class="notificationType mb-0">
									<?php
									$methods = array();
									Plugin::callHook("notification_methods", array(&$methods));

									foreach ($methods as $method) {
										$slug = $type['slug'].'_'.$method['slug'];
										$typePreference = $userPreferences[$type['slug']];
										$checked = isset($typePreference[$type['slug'].'_'.$method['slug']]) && $typePreference[$type['slug'].'_'.$method['slug']]==true ? 'checked="checked"': '';
										?>
										<li class="d-inline-block">
											<input data-type="checkbox" data-category="<?php echo $type['slug']; ?>" data-method="<?php echo $method['slug']; ?>" id="<?php echo $slug; ?>" <?php echo $checked ?> class="">
											<label for="<?php echo $slug; ?>" class="pointer">
												<i class="<?php echo $method['icon'] ?>"></i>
												<?php echo $method['label'] ?>
											</label><br>
											<small class="ml-1 text-muted"><?php echo $method['explain']; ?></small>
										</li>
										<?php } ?>
									</ul>
								</div>
							<?php 
							$i++;
							endforeach; ?>
				      </div>
				      <?php endforeach; ?>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div><br>
