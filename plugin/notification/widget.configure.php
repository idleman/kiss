<?php 
require_once(__DIR__.SLASH.'Notification.class.php');	

$selectedTypes = $widget->data('types');
$selectedTypes = $selectedTypes==''? array("announcement"):$selectedTypes;
$max = $widget->data('notification-number');
$max = $max =="" ? 5 : $max ;

$maxlength = $widget->data('notification-length');
$maxlength = $maxlength =="" ? 150 : $maxlength ;
?>
<div id="notification-widget-form">
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<label for="notification-number">Nb de notifications affichées :</label>
			<input type="number" class="form-control mb-2" value="<?php echo $max; ?>" id="notification-number">
		</div>
		<div class="col-md-6 col-sm-12">
			<label for="notification-length">Nb de caractères affichés :</label>
			<input type="number" class="form-control mb-2" value="<?php echo $maxlength; ?>" id="notification-length">
		</div>
		<div class="col-md-12 col-sm-12">
			<label for="">Types de notifications à afficher :</label>
			<ul class="list-group" id="widget-notification-list">
			<?php foreach(Notification::types() as $slug=>$type): ?>
				<li class="list-group-item p-0">
					<label class="m-0 pointer p-2"><input type="checkbox" <?php echo in_array($slug, $selectedTypes)?'checked="checked"':''; ?> value="<?php echo $slug; ?>" data-type="checkbox"><i class="<?php echo $type['icon']; ?>"></i> <?php echo $type['label']; ?> <small class="text-muted">- <?php echo $type['description']; ?></small></label>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div> 