<?php 
global $myUser;
require_once(__DIR__.SLASH.'Notification.class.php');
require_once(__DIR__.SLASH.'UserNotification.class.php');

$types = $widget->data('types');
$types = $types==''? array('announcement'):$types;
$max = $widget->data('notification-number');
$max = $max =="" ? 5 : $max ;

$maxlength = $widget->data('notification-length');
$maxlength = $maxlength =="" ? 250 : $maxlength ;

$userNotifications = UserNotification::byUser($myUser,array(
	'limit' => $max,
	'unread' => false,
	'types' => $types,
));

?>
<div class="notification-container">
	<h5 class="text-uppercase"><i class="far fa-bell"></i> Alertes</h5>
	<ul class="notification-widget-list">
	<?php foreach($userNotifications  as $infos):
		$infos['created-relative'] = isset($_['synthesis']) ? relative_time($infos['created'], null, 7) : relative_time($infos['created'], null, 7, true);
		$meta = json_decode($infos['meta']);
		$infos['link'] =  'index.php?module=notification#'.$infos['id'];
		$type = Notification::types($infos['type']);
	?>
		<li>
			<h5 onclick="document.location.href='<?php echo $infos['link']; ?>';" class="pointer mb-0 mt-1" style="color: <?php echo $type['color']; ?>;">
				<div><i title="<?php echo $type['label'] ?>" class="<?php echo $type['icon'] ?>"></i> <?php echo $infos['label']; ?></div>
				<small class="text-muted d-block"><i class="far fa-clock"></i> <?php echo $infos['created-relative']; ?></small>
			</h5>
            <p class=""><?php echo truncate($infos['html'],$maxlength,array('html'=>true)); ?></p>   
			<a class="btn btn-link d-block w-100 text-center" href="<?php echo $infos['link']; ?>">+ d'informations</a>
		</li>
	<?php endforeach; ?>
	</ul>
</div>