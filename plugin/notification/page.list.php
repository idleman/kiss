<?php
global $myUser;
User::check_access('notification','read');
require_once(__DIR__.SLASH.'Notification.class.php');
require_once(__DIR__.SLASH.'UserNotification.class.php');

$categories = array();
$unreads = UserNotification::get_unreads_count($myUser);

foreach(Notification::types() as $slug => $type){
    $type['slug'] = $slug;
    
    $type['unread'] = isset($unreads['detailled'][$slug]) ? $unreads['detailled'][$slug] : 0;
    if(!isset($type['category'])) $type['category'] = 'Général';
    if(!isset($categories[slugify($type['category'])])) 
        $categories[slugify($type['category'])] = array(
            'label' => $type['category'],
            'items' => array()
        );
    $categories[slugify($type['category'])]['items'][] = $type;
}
?>
<div class="row notifications-list">
    <div class="notification-summary">
        <span class="summary w-100 d-inline-flex flex-column">
            <div class="text-info">
                <i class="fas fa-bell mr-1 ml-1"></i> 
                <span id="notifCounterFilter" class="badge badge-light pointer mr-1 ml-1">0</span>
                <span id="notifWord">notification</span>
                en attente
            </div>
            <hr class="mx-0">
            <ul class="categories">
             <?php foreach($categories as $slug => $category): ?>
                <li class="category" data-category="<?php echo $slug; ?>">
                    <div class="boxTitle">
                        <input class="categoryCheckbox" 
                                type="checkbox"
                                id="checkbox_<?php echo $slug; ?>"
                                name="checkbox_<?php echo $slug; ?>"
                                data-type="checkbox">

                        <span onclick="$(this).closest('.category').toggleClass('folded');">
                            <i class="categoryIcon fas fa-chevron-right"></i>
                            <span class="categoryLabel pointer"><?php echo $category['label']; ?></span>
                        </span>
                        <span class="categoryBadge badge badge-pill badge-light float-right mt-1" data-category="<?php echo $slug; ?>">0</span>
                    </div>
                    <ul class="items" data-category="<?php echo $slug; ?>">
                    <?php foreach ($category['items'] as $key => $item): ?>
                        <li class="item" data-category="<?php echo $slug; ?>" data-item="<?php echo $item['slug']; ?>">
                            <input class="itemCheckbox"
                                    type="checkbox"
                                    data-category="<?php echo $slug; ?>"
                                    data-item="<?php echo $item['slug']; ?>"
                                    id="checkbox_<?php echo $item['slug']; ?>"
                                    name="checkbox_<?php echo $item['slug']; ?>"
                                    data-type="checkbox">
                            <div class="itemLabel d-inline">
                                <i style="color:<?php echo $item['color']; ?>" class="itemIcon <?php echo $item['icon']; ?>"></i>
                                <span class="pointer"><?php echo $item['label']; ?></span>
                            </div>
                            <span class="itemBadge badge badge-pill badge-light float-right" data-category="<?php echo $slug; ?>" data-item="<?php echo $item['slug']; ?>">0</span>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
            </ul>
        </span>
    </div>

    <div class="notification-detail">
        <div class="d-flex flex-row-reverse font-weight-light">
            <a href="#" onclick="notification_user_notification_delete_all('notifications', event);" id="deleteState">Tout supprimer <!-- <i class="far fa-trash-alt ml-1"></i> --></a>
            <a href="#" onclick="notification_user_notification_read_all('notifications', event);" class="mr-3" id="readState">Tout marquer comme lu <!-- <i class="fas fa-eye ml-1"></i> --></a>
        </div>
        <!-- Voir pour mettre en place un lazy loading -->
        <ul id="notifications" class="mid-pane notifications">
            <li data-id="{{id}}" data-category="{{category}}" data-item="{{type}}" class="hidden notification-item {{class}}">
               <h5 onclick="document.location.href='{{link}}';" class="pointer">{{label}}</h5>
               {{{html}}}
                <div class="notification-options">
                    <div class="btn-group btn-group-sm" role="group">
                        <div class="btn btn-info" title="Marquer comme {{readState}}" onclick="notification_user_notification_toggle_read(this);"><i class="fas fa-eye"></i></div> 
                        <div class="btn btn-danger btn-delete" title="Supprimer la notification" onclick="notification_user_notification_delete(this);"><i class="far fa-trash-alt"></i></div>
                    </div>
                    <small>{{{created-relative}}}</small>
                </div>
            </li>
        </ul>
    </div>
</div>
<br>