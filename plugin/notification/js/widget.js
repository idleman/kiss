
function notification_widget_configure_save(widget,modal){

	var data = $('#notification-widget-form').toJson();

	data.types = [];
	$('#widget-notification-list input[type="checkbox"]:checked').each(function(){
		data.types.push($(this).val())
	});
	
	
	data.action = 'notification_widget_configure_save';
	data.id = modal.attr('data-widget');

	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}