<?php 
global $myUser,$conf;

require_once(__DIR__.SLASH.'Notification.class.php');
User::check_access('notification','configure');
$types = array();
foreach(Notification::types() as $slug=>$type)
    $types[$slug] = html_decode_utf8($type['label']);
 
$categories = Notification::categories();

?>
<div class="row">
	<div class="col-md-12"><br>
        <h3>Réglages Notifications</h3>
        <hr>
	</div>
</div>

<div class="row">
	<div class="col-md-12" id="notification_preference"> 
		<div class="tab-container mb-0 noPrint">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#tab-general" aria-controls="tab-general" aria-selected="true" onclick="setTimeout(function(){notification_search();},0);">Notifications épinglées</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-send" aria-controls="tab-send" aria-selected="false" onclick="setTimeout(function(){init_components('#tab-send');},0);">Envoi</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-followings" aria-controls="tab-followings" aria-selected="false">Suivis</a></li>
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-settings" aria-controls="tab-settings" aria-selected="false">Paramètres</a></li>
			</ul>
		</div>

		<div class="tab-content">
			<!-- Onglet Général -->
			<div class="tab-pane show active in" id="tab-general" role="tabpanel" aria-labelledby="tab-general"><br>
				<div class="row">
					<!-- search results -->
					<div class="col-xl-12">
						<legend>Notifications épinglées :</legend>
				        <select id="filters" data-type="filter" data-label="Recherche"  data-function="notification_search">
				            <option value="label" data-filter-type="text">Libellé</option>
				            <option value="start" data-filter-type="date">Date de lancement</option>
				            <option value="end" data-filter-type="date">Date d'expiration</option>
				            <option value="type" data-filter-type="list" data-values='<?php echo json_encode($types); ?>'>Type</option>
				        </select>
				        <hr>
						<table id="notifications-pinned" class="table table-striped " data-entity-search="notification_notification_search">
				            <thead>
				                <tr>
				                    <th>#</th>
				                    <th data-sortable="type">Type</th>
				                    <th data-sortable="label">Label</th>
				                    <th data-sortable="start">Date de lancement</th>
				                    <th data-sortable="end">Date d'expiration</th>
				                    <th >Contenu</th>
				                    <th></th>
				                </tr>
				            </thead>
				            <tbody>
				                <tr data-id="{{id}}" class="hidden">
					                <td>{{id}}</td>
					                <td><i class="{{type.icon}}" style="color:{{type.color}};"></i> {{type.label}}</td>
					                <td>{{label}}</td>
					                <td>{{start}}</td>
					                <td>{{end}}</td>
					                <td>{{html}}</td>
					                <td class="text-center">
			                            <div class="btn btn-danger btn-mini btn-squarred" onclick="notification_delete(this);"><i class="far fa-trash-alt"></i></div>
					                </td>
				                </tr>
				           </tbody>
				        </table>
				         <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
				        <ul class="pagination justify-content-center"  data-range="3">
				            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');notification_notification_search();">
				                <span class="page-link">{{label}}</span>
				            </li>
				        </ul>

					</div>
				</div>
			</div>

			<!-- Onglet Envoi -->
			<div class="tab-pane" id="tab-send" role="tabpanel" aria-labelledby="tab-send"><br>
				<div class="col-md-12" id="notification-send" data-action="notification_send"> 
					<div class="btn btn-primary float-right btn-send" onclick="notification_send()"><i class="far fa-paper-plane"></i> Envoyer</div>
					<legend class="mb-0 w-auto mr-2">Paramètres d'envoi :</legend>
					<small class="text-muted">Dans cette section, vous pouvez créer une notification pour les utilisateurs et types concernés</small>
					<div class="clear"></div>
					
					<div class="row mt-1">
						<div class="col-md-4">
							<label for="start">Date de lancement <small class="text-muted"> - Laisser vide pour maintenant</small> :</label>
							<input type="text" class="form-control" id="start" data-type="date">
						</div>
						<div class="col-md-8 mb-1 user-picker">
							<label for="recipients">Destinataires <small class="text-muted"> - Laisser vide pour envoyer à tout le monde</small> :</label>
							<input type="text" class="form-control" id="recipients" data-type="user" data-multiple>
						</div>
						<div class="col-md-4">
							<label for="end">Date d'expiration <small class="text-muted"> - Par défaut à 3 mois</small> :</label> 
							<input type="text" class="form-control" id="end" data-type="date">
						</div>
						<div class="col-md-8">
							<label class="font-weight-bold text-center d-block mb-0 mt-1">OU</label>
							<label for="pinned" class="pointer mt-2 w-100 text-center"><input type="checkbox" class="" id="pinned" data-type="checkbox">Épingler<small class="text-muted"> - Concerne tout le monde</small></label> 
						</div>
					</div>
					<hr class="w-100">
					<legend class="mb-0">Détails :</legend>
					<div class="row">
						<div class="col-md-8">
							<label for="label">Sujet : </label>
							<input type="text" class="form-control" id="label" placeholder="eg. Mise à jour majeure">
						</div>
						<div class="col-md-4">
							<label for="type">Type : </label>
							<select data-type="dropdown-select" id="type" class="w-100 p-0" title="Type de notification">
							<?php foreach(Notification::types() as $slug => $type):
									$types[$slug] = $type['label'];
								?>
				            	<option value="<?php echo $slug;?>" data-title="<?php echo $type['description']; ?>" style="background-color:<?php echo $type['color']; ?>;color:#ffffff;" data-icon="<?php echo $type['icon']; ?>"><?php echo $type['label']; ?></option>
			            	<?php endforeach; ?>
				            </select>
						</div>
						
						<div class="col-md-12 mt-2">
							<label for="html" class="mb-0">Contenu :<small class="text-muted"> - Le contenu présent dans la notification (accepte le HTML)</small> :</label>
							<textarea class="" id="html" data-type="wysiwyg"></textarea>
						</div>
					</div>
				</div>
			</div>


			<!-- Onglet Suivis -->
			<div class="tab-pane" id="tab-followings" role="tabpanel" aria-labelledby="tab-followings"><br>
				<div class="btn btn-success float-right" onclick="notification_setting_save();"><i class="fas fa-check"></i> Enregistrer</div>
				<legend>Suivis par défaut :</legend>
				<div class="clear"></div>
				<p>Configuration par défaut des différentes méthodes de suivis par type de notification pour l'ensemble des utilisateurs <small class="text-muted">(la configuration personnelle de l'utilisateur prendra le pas sur cette configuration)</small></p>

				<div class="row" id="notification-default-following">
					<div class="col-3">
					    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					    <?php 
					    	$i = 0;
					    	foreach($categories as $categorySlug=>$category): ?>
						    	<a class="nav-link <?php echo $i==0?'active':'' ?>" id="v-pills-home-tab" data-toggle="pill" href="#category-<?php echo $categorySlug ?>" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="far fa-bookmark"></i> <?php echo $category['label'] ?></a>
						   		<?php 
						   		$i++;
					   		endforeach; ?>
					    </div>
					</div>
					<div class="col-9">
						<div class="tab-content" id="v-pills-tabContent">
						    <?php 
						    $i = 0;
						    foreach($categories as $categorySlug => $category): ?>
						     	<div class="tab-pane fade show <?php echo $i==0?'active':'' ?>" id="category-<?php echo $categorySlug ?>" role="tabpanel" aria-labelledby="v-pills-home-tab">
									
									<?php foreach($category['items'] as $type): ?>
										<div data-slug="<?php echo $type['slug']; ?>" class="list-group-item list-group-item-action flex-column align-items-start category user-select-none">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1" style="color:<?php echo $type['color']; ?>;"><i class="<?php echo $type['icon']; ?>"></i> <?php echo $type['label']; ?></h5>
											</div>
											<p class="mb-3"><?php echo $type['description']; ?>.</p>
											<ul class="notificationType mb-0">
											<?php
											$methods = array();
											Plugin::callHook("notification_methods", array(&$methods));

											foreach ($methods as $method) {
												$slug = $type['slug'].'_'.$method['slug'];
												$checked = $conf->get($slug) ? 'checked="checked"' : (isset($type['default_methods'][$method['slug']]) && $type['default_methods'][$method['slug']] ? 'checked="checked"' : '');
												?>
												<li class="d-inline-block">
													<input data-type="checkbox" data-category="<?php echo $type['slug']; ?>" data-method="<?php echo $method['slug']; ?>" id="<?php echo $slug; ?>" <?php echo $checked ?> class="">
													<label for="<?php echo $slug; ?>" class="pointer">
														<i class="<?php echo $method['icon'] ?>"></i>
														<?php echo $method['label'] ?>
													</label><br>
													<small class="ml-1 text-muted"><?php echo $method['explain']; ?></small>
												</li>
												<?php
											}
											?>
											</ul>
										</div>
									<?php 
								$i++;
								endforeach; ?>
						    </div>
						    <?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>

			<!-- Onglet Paramètres -->
			<div class="tab-pane" id="tab-settings" role="tabpanel" aria-labelledby="tab-settings"><br>
				<div class="btn btn-success float-right" onclick="notification_setting_save();"><i class="fas fa-check"></i> Enregistrer</div>
				<legend>Paramètres généraux :</legend>
				<div class="clear"></div>
				<?php echo Configuration::html('notification'); ?>
			</div>
		</div>
	</div>
</div>
