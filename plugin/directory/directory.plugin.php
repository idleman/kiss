<?php
require_once(__DIR__.SLASH.'QRCode.class.php');

function directory_plugin_menu(&$accountMenu){
	global $_, $myUser;
	if(!$myUser->connected()) throw new Exception('Vous devez être connecté pour accéder à cette fonctionnalité',401);
	$accountMenu[]= array(
		'sort' =>1,
		'url' => 'account.php?section=directory',
		'icon' => 'fas fa-angle-right',
		'label' => 'Annuaire',
	);
}

function directory_get_contacts(){
	
	$users = User::getAll();
	
	$userMapping = array();
	foreach ($users as $user) {
		$userMapping[$user->login] = array(
			'object'=>$user,
			'values' => array(
				'Nom' => strtoupper($user->name).' '.$user->firstname,
				'Email' => '<a href="mailto:'.$user->mail.'">'.$user->mail.'</a>',
				'Fixe' => '<a href="tel: '.$user->phone.'">'.$user->phone.'</a>',
				'Portable (pro)' => '<a href="tel: '.$user->mobile.'">'.$user->mobile.'</a>'
			)
		);
	}
	Plugin::callHook('directory_list',array(&$userMapping));

	$columns = array();
	$reference = array();
	foreach ($userMapping as  $infos) {
		if(isset($infos['values']) && count($infos['values']) > count($reference)) $reference = $infos['values'];
	}
	if(!empty($reference)) $columns = array_keys($reference);

	//Tri par ordre alphabétique
	uasort($userMapping, function($a, $b){
		return strcmp($a['object']->name, $b['object']->name);
	});

	return array($columns,$userMapping);
}

//cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html

require_once(__DIR__.SLASH.'action.php');



function directory_plugin_page(){
	global $myUser,$_;
	if(!isset($_['section']) || $_['section']!='directory') return;
	if($myUser!=false){
		list($columns,$userMapping) = directory_get_contacts();
	?>
		<div class="tab-pane fade show active">
		<h3>Annuaire</h3>
		<p>Annuaire de la société</p>
		<hr> 

		<a type="btn" href="action.php?action=directory_get_excel" class="btn btn-primary" title="Excel"><i class="far fa-file-excel"></i> Exporter l'annuaire (.csv)</a>
	   	<br/><br/>
	    	<table class="table table-striped table-bordered table-hover">
		    <thead>
			    <tr>
				    <?php foreach($columns as $column): ?>
				    	<th><?php echo $column; ?></th>
				    <?php endforeach; ?>
				    <th>Action</th>
			    </tr>
		    </thead>
		    <?php foreach($userMapping as $infos){ 
		    	$user = $infos['object'];
		    	if($user->origin != ''){
		    	?>
		    <tr data-user="<?php echo $user->login; ?>">
		    	<?php foreach($columns as $column): ?>
		    	<td><?php echo isset($infos['values'][$column])?$infos['values'][$column]:''; ?></td>
		    	<?php endforeach; ?>
		    	<td>
		    		<a type="btn" target="_blank" href="action.php?action=directory_get_qr&amp;user=<?php echo $user->login; ?>" class="btn btn-primary btn-sm" title="QR Code"><i class="fas fa-qrcode"></i></a>
		    		<a type="btn" href="action.php?action=directory_get_cardav&amp;user=<?php echo $user->login; ?>" class="btn btn-primary btn-sm" title="Cardav"><i class="far fa-address-card"></i></a>
		    		<?php if(Plugin::is_active('fr.core.sms')): ?>
		    			<div onclick="directory_send_sms(this)" class="btn btn-primary btn-sm" title="Me l'envoyer par sms"><i class="fas fa-sms"></i></div>
		    		<?php endif; ?>
		    	</td>
		    </tr>
		    <?php 
				}
			} ?>
		    </table>
		</div>
<?php } else { ?>
		<div id="main" class="wrapper clearfix">
			<article>
				<h3>Vous devez être connecté</h3>
			</article>
		</div>
<?php }
}



Plugin::addJs("/js/main.js");

Plugin::addHook("menu_account", "directory_plugin_menu");  
Plugin::addHook("content_account", "directory_plugin_page"); 


?>