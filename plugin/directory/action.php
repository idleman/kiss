<?php

	/** SMS **/
	//Envois d'un contact par sms
	Action::register('directory_send_sms',function(&$response){
		global $myUser,$_,$conf;
		if(!$myUser->connected()) throw new Exception("Vous devez être connecté",401);
		Plugin::need('sms/Sms');
		$user = User::byLogin($_['user']);
		if(empty($myUser->mobile))throw new Exception("Votre numéro de téléphone n'est pas renseigné");
		if(empty($conf->get('sms_api_url')) || empty($conf->get('sms_api_token'))) throw new Exception("Api SMS non configurée");
		$sms = new Sms($conf->get('sms_api_url'),$conf->get('sms_api_token'));
		$sms->phone = str_replace(' ','',$myUser->mobile);

		$card = ROOT_URL.'/action.php?action=directory_get_cardav&token='.sha1('vcard:'.$user->phone.'-'.$user->id.'-'.$user->login);
		$sms->message = $user->fullName().': ';
		$sms->message .= PHP_EOL;
		$sms->message .= 'fixe : '.str_replace(' ','',$user->phone);
		$sms->message .= PHP_EOL;
		$sms->message .= 'mobile : '.str_replace(' ','',$user->mobile);
		$sms->message .= PHP_EOL;
		$sms->message .= 'mail : '.$user->mail;
		$sms->message .= PHP_EOL;
		$sms->message .=' vcard: '.$card;

		Log::put('Envois à '.$sms->phone.' : '.$sms->message,'Sms');
		try{
			$response = $sms->send();
			Log::put('Envois à '.$sms->phone.' : '.json_encode($response),'Sms');
			$response['phone'] = $myUser->mobile;
		}catch(Exception $e){
			throw new Exception('ERREUR : '.$e->getMessage());
			Log::put('Envois à '.$sms->phone.' ERREUR : '.$e->getMessage(),'Sms');
		}
	});
	

	Action::register('directory_get_qr',function(&$response){
		global $myUser,$_,$conf;
		if(!$myUser->connected()) throw new Exception("Permission denied");
		header('Content-type:image/gif');
		$user = User::byLogin($_['user']);
		$meCard = 'MECARD:N:'.utf8_decode($user->fullName()).';ORG:Firm;TEL:'.str_replace(' ','',$user->phone).';URL:http\://;EMAIL:'.$user->mail.';;';
		$a = new QR($meCard);
		echo $a->image(7);
		exit();
	});

	Action::register('directory_get_cardav',function(&$response){
		global $myUser,$_,$conf;
		if(!$myUser->connected()) throw new Exception("Permission denied");
		if(isset($_['token'])){
			foreach (User::getAll() as $user) {
				if(sha1('vcard:'.$user->phone.'-'.$user->id.'-'.$user->login) == $_['user']) $target = $user;
			}
		}

		if(isset($_['user'])) $user = User::byLogin($_['user']);
		if(isset($target)) $user = $target;
		if(empty($user)) throw new Exception("Utilisateur introuvable");

		header('Content-Type: text/x-vCard');
		header('Content-Disposition: attachment; filename= "'.$user->login.'.vcf"');  

		$vCard =  'BEGIN:VCARD'.PHP_EOL;
		$vCard .= 'VERSION:3.0'.PHP_EOL;
		$vCard .= 'N:'.utf8_decode($user->name.';'.$user->firstname).PHP_EOL;
		$vCard .= 'ORG:Firm'.PHP_EOL;
		$vCard .= 'ADR;TYPE=WORK:;;122 Avenue Saint Emilion;Martignas;Aquitaine;30127;France'.PHP_EOL;
		$vCard .= 'LABEL;TYPE=WORK:122 Avenue Saint Emilion;\nMartignas, Aquitaine 30127\nFrance'.PHP_EOL;
		$vCard .= 'TEL;CELL:'.str_replace(' ','',$user->phone).PHP_EOL;
		$vCard .= 'URL:'.PHP_EOL;
		$vCard .= 'EMAIL:'.utf8_decode($user->mail).PHP_EOL;
		$vCard .= 'END:VCARD';
		header('Content-Length: '.strlen($vCard)); 
		echo $vCard;
		exit();
	});

	Action::register('directory_get_excel',function(&$response){
		global $myUser,$_,$conf;
		if(!$myUser->connected()) throw new Exception("Permission denied");
		
		list($columns,$userMapping) = directory_get_contacts();

		$excel = implode(';',$columns)."\n";
		foreach($userMapping as $user=>$infos){ 
			foreach ($infos['values'] as $key => $value): 
				$excel .= strip_tags(utf8_decode($value)).';';
			endforeach;
			$excel .= "\n";
		}
		File::downloadStream($excel,"annuaire ".date('d-m-Y').".csv",'text/csv');

		exit();
	});

?>