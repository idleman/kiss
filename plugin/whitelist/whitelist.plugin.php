<?php



//Déclaration d'un item de menu dans le menu principal
function whitelist_menu(&$menuItems){
	global $myUser,$conf;

	if(!empty($conf->get('whitelist_ips')) ){
		$allowed = explode(PHP_EOL,$conf->get('whitelist_ips'));
		$allow = false;

		foreach($allowed as $ip){
			$ip = str_replace(array('.', '*'), array('\.','.*'), $ip);
			if (preg_match('/'.$ip.'/', ip())) {
				$allow = true;
			}
		}

		if(!$allow){
			header("HTTP/1.1 401 Unauthorized");
			exit('Ip non autorisée : '.ip());
		}
	}

	if(!$myUser->can('whitelist','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=whitelist',
		'label'=>'Restriction ip (whitelist)',
		'icon'=> 'fas fa-shield-alt',
		'color'=> '#d63031'
	);
}

//Cette fonction va generer une page quand on clique sur Restriction IP (WhiteList) dans menu
function whitelist_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='whitelist') return;
	$page = !isset($_['page']) ? 'list.' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function whitelist_install($id){
	if($id != 'fr.core.whitelist') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function whitelist_uninstall($id){
	if($id != 'fr.core.whitelist') return;
	Entity::uninstall(__DIR__);
	
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('whitelist',array('label'=>'Gestion des droits sur le plugin Restriction IP (WhiteList)'));


//cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html
function whitelist_action(){
	require_once(__DIR__.SLASH.'action.php');
}
//Déclaration du menu de réglages
function whitelist_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('whitelist','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.whitelist',
		'icon' => 'fas fa-angle-right',
		'label' => 'Restriction ip (whitelist)'
	);
}

//Déclaration des pages de réglages
function whitelist_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


require_once(__DIR__.SLASH.'action.php');


//Déclaration des settings de base
//Types possibles : voir FieldType.class.php. Un simple string définit une catégorie.
Configuration::setting('whitelist',array(
    "Général",
    'whitelist_ips' => array("label"=>"Ip autorisées","legend"=>"Séparés par saut de ligne","type"=>"textarea"),
));
  


//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "whitelist_install");
Plugin::addHook("uninstall", "whitelist_uninstall"); 


Plugin::addHook("menu_main", "whitelist_menu"); 
Plugin::addHook("page", "whitelist_page"); 
Plugin::addHook("menu_setting", "whitelist_menu_setting");
Plugin::addHook("content_setting", "whitelist_content_setting");
?>