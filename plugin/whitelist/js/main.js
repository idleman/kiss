//CHARGEMENT DE LA PAGE
function init_plugin_whitelist(){
	switch($.urlParam('page')){
		default:
		break;
	}
	whitelist__search();
}

//Enregistrement des configurations
function whitelist_setting_save(){
	$.action({ 
		action: 'whitelist_setting_save', 
		fields:  $('#whitelist-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}

/**  **/
//Récuperation d'une liste   dans le tableau #s
function whitelist__search(callback){

	$('#s').fill({
		action:'whitelist__search'
	},function(response){
		if(callback!=null) callback();
	});
}

//Ajout ou modification 
function whitelist__save(){
	var data = $('#-form').toJson();
	$.action(data,function(r){
		$('#-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		$.message('success','Enregistré');
	});
}


//Suppression 
function whitelist__delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'whitelist__delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

