<?php
	 
	

	//Sauvegarde des configurations de Restriction IP (WhiteList)
	Action::register('whitelist_setting_save',function(&$response){
		global $_,$conf;
		User::check_access('whitelist','configure');
		//Si input file "multiple", possibilité de normaliser le
		//tableau $_FILES récupéré avec la fonction => normalize_php_files();
		foreach(Configuration::setting('whitelist') as $key=>$value){
			if(!is_array($value)) continue;
			$allowed[] = $key;
		}
		foreach ($_['fields'] as $key => $value) {
			if(in_array($key, $allowed))
				$conf->put($key,$value);
		}
	});
	

	


?>