<?php
global $myUser,$conf;
User::check_access('whitelist','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	
        <h3 class="mb-0"><i class="fas fa-shield-alt"></i> Réglages Restriction ip (whitelist) <div onclick="whitelist_setting_save();" class="btn btn-sm btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div></h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('whitelist'); ?>
    </div>
</div>
