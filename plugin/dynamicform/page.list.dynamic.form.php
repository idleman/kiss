<?php
global $myUser;
User::check_access('dynamicform','read');
require_once(__DIR__.SLASH.'DynamicForm.class.php');
?>

<div class="row">
    <div class="col-md-12">
        <div class="d-flex my-2 w-100">
            <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des formulaires</h4>
            <div class="my-auto ml-auto mr-0 noPrint">
                <?php if($myUser->can('dynamicform', 'edit')) : ?>
                <a href="index.php?module=dynamicform&page=sheet.dynamic.form" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                <?php endif; ?>
            </div>            
        </div>
        <div class="clear noPrint"></div>
    </div>    
</div>
<h5 class="results-count"><span></span> Résultat(s)</h5>
<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		<table id="dynamic-forms" class="table table-striped " data-entity-search="dynamicform_form_search">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Libellé</th>
                    <th>Identifiant technique</th>
                    <th>Créateur</th>
                    <th>Etablissement</th>
                    <th></th>
                </tr>
            </thead>            
            <tbody>
                <tr data-id="{{id}}" class="hidden">
	                <td class="align-middle">{{id}}</td>	               
	                <td class="align-middle" style="border-left:5px solid {{color}}"> <i class="{{icon}}"></i> {{label}}</td>
                    <td class="align-middle"><code>{{slug}}</code></td>
                    <td class="align-middle">Par <span class="font-weight-bold text-muted">{{creator}}</span> le <span class="font-weight-bold text-muted">{{created}}</span></td>
                    <td>{{firmLegend}}</td>
	                <td class="align-middle text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            
                            <a class="btn text-info" title="Éditer dynamic_form" href="index.php?module=dynamicform&page=sheet.dynamic.form&id={{id}}"><i class="fas fa-pencil-alt"></i></a>                            
                            <div class="btn text-danger" title="Supprimer dynamic_form" onclick="dynamicform_form_delete(this);"><i class="far fa-trash-alt"></i></div>
                        </div>
	                </td>
                </tr>
           </tbody>
        </table>
        <br>         
	</div>
</div>
