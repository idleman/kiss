<?php
/**
 * Define a dynamicvalue.
 * @author Charles DUBOIS
 * @category Plugin
 * @license MIT
 */
class DynamicValue extends Entity{

	public $id;
	public $field; //Champ (Entier)
	public $value; //Valeur (Texte Long)
	public $firm; //Etablissement (Entier)
	public $scope; //Scope (Texte)
	public $uid; //Uid (Texte)
	
	protected $TABLE_NAME = 'dynamicform_value';
	public $entityLabel = 'Valeur dynamique';
	public $fields = array(
		'id' => 'key',
		'field' => array('type'=>'int', 'label' => 'Champ','link'=>'plugin/dynamicform/DynamicField.class.php'),
		'value' => array('type'=>'longstring', 'label' => 'Valeur'),
		'firm' => array('type'=>'int', 'label' => 'Etablissement'),
		'scope' => array('type'=>'string', 'label' => 'Scope'),
		'uid' => array('type'=>'string', 'label' => 'Uid')
	);



	//Colonnes indexées
	public $indexes = array('field','firm','scope','uid');
}
?>