<?php

//Déclaration d'un item de menu dans le menu principal
function dynamicform_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('dynamicform','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=dynamicform',
		'label'=>'Formulaires',
		'icon'=> 'fas fa-text-height',
		'color'=> '#273c75'
	);
}

//Cette fonction va generer une page quand on clique sur dynamicform dans menu
function dynamicform_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='dynamicform') return;
	$page = !isset($_['page']) ? 'list.dynamic.form' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function dynamicform_install($id){
	if($id != 'fr.core.dynamicform') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function dynamicform_uninstall($id){
	if($id != 'fr.core.dynamicform') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('dynamicform',array('label'=>'Gestion des droits sur les formulaires dynamiques'));

function dynamic_types_filter($field,&$filterType,&$filterOptions){
	
	switch($field['type']->slug){
		 case 'user':
	    case 'firm':
	    case 'rank':
	    case 'date':
	    case 'hour':
	    case 'boolean':
	        $filterType = $field['type']->slug;
	    break;
	    case 'price':
	    case 'integer':
	    case 'float':
	        $filterType = 'number';
	    break;
	    case 'list':
	        $filterType = $field['type']->slug;
	        $filterOptions = ' data-filter-type="list" data-source="'.(isset($field['meta']) ? $field['meta'] : '').'" data-depth="1" data-disable-label ';
	    break;
	    case 'dictionary':
	        $filterType = $field['type']->slug;
	        $meta = isset($field['meta']) ? json_decode(base64_decode($field['meta']), true) : array();
	        $filterOptions = ' data-filter-type="dictionary" data-slug="'.(isset($meta['slug']) ? $meta['slug'] : '').'" data-depth="1" data-disable-label ';
	    break;
	    case 'checkbox-list':
	        $filterType = $field['type']->slug;
	        $meta = isset($field['meta']) ? json_decode(base64_decode($field['meta']), true) : array();
	        $filterOptions = ' data-filter-type="checkbox-list" data-slug="'.(isset($meta['slug']) ? $meta['slug'] : '').'" data-depth="1" data-disable-label ';
	    break;
	}
}

require_once(__DIR__.SLASH.'action.php');

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "dynamicform_install");
Plugin::addHook("uninstall", "dynamicform_uninstall"); 

Plugin::addHook("menu_main", "dynamicform_menu"); 
Plugin::addHook("page", "dynamicform_page");    

?>