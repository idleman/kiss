<?php
global $myUser;
User::check_access('dynamicform','read');
require_once(__DIR__.SLASH.'DynamicForm.class.php');
$dynamicform = DynamicForm::provide();

$defaultType = base64_encode(json_encode(FieldType::available('text')));

?>
<div class="dynamicform">
	<div id="dynamic-form-form" class="row dynamic-form-form" data-default-type="<?php echo $defaultType; ?>" data-action="dynamicform_form_save" data-id="<?php echo $dynamicform->id; ?>">
		<div class="col-md-12">

			<h3>Formulaire</h3>
			
			<div class="row">
				<div class="input-group col-md-6">
					<div class="input-group-text input-group-prepend">Libellé</div>
					<input id="label" name="label" class="form-control" placeholder="" value="<?php echo $dynamicform->label; ?>" type="text">
					<input id="color" name="color" data-type="color" title="Couleur" class="form-control" placeholder="" value="<?php echo $dynamicform->color; ?>" type="text">
					<input id="icon" name="icon" data-type="icon" class="form-control" placeholder="" value="<?php echo $dynamicform->icon; ?>" type="text">
				</div>

				<div class="input-group col-md-6">
					<div class="input-group-text input-group-prepend"><input id="allFirm" name="allFirm" <?php if(!$myUser->can('dynamicForm','configure')) echo 'readonly="readonly"'; ?> type="checkbox" data-type="checkbox"  <?php echo $dynamicform->firm == 0 ? 'checked="checked"': ''; ?> >Disponible pour tout les établissements</div>
					<div class="input-group-text input-group-prepend ml-1">Slug</div>
					<input id="slug" name="slug" <?php if(!$myUser->can('dynamicForm','configure')) echo 'readonly="readonly"'; ?> class="form-control" placeholder="" value="<?php echo $dynamicform->slug; ?>" type="text">

					<div onclick="dynamicform_form_save();" class="btn btn-success"><i class="fas fa-check"></i> Enregistrer</div>
				</div>
				
			</div>
			<br/>
		</div>
	</div>

	<div id="form-layout" class="form-layout" >
		
		<template id="layout-row">
			<div class="row">
				<div class="col-md column">
					<ul class="fields"></ul>
					<div class="btn btn-primary btn-field-add shadow-sm  p-2 pointer d-inline-block  w-100"><i class="fas fa-plus"></i> Ajouter un champ</div>
					<ul class="layout-column-menu">
						<li class="btn-layout btn-column-add" title="Ajouter une colonne"><i class="fas fa-plus"></i></li>
						<li class="btn-layout btn-column-remove" title="Supprimer une colonne"><i class="fas fa-times"></i></li>
					</ul>
				</div>
				<ul class="layout-row-menu">
					<li class="btn-layout btn-row-add" title="Ajouter une ligne"><i class="fas fa-plus"></i></li>
					<li class="btn-layout btn-row-remove" title="Supprimer une ligne"><i class="fas fa-times"></i></li>
				</ul>
			</div>
		</template>
		<template id="field-template">
			 <li 
			 	data-id="{{id}}" 
			 	data-label="{{label}}" 
			 	data-type="{{type.slug}}" 
			 	data-description="{{description}}" 
			 	data-mandatory="{{mandatory}}" 
			 	data-readonly="{{readonly}}" 
			 	data-default="{{default}}" 
				data-slug="{{slug}}"
				data-meta="{{meta}}"

			 	class="shadow-sm bg-white p-2 mb-2 field">
	                <span class="font-weight-bold"><i title="Type {{type.label}}" class="{{type.icon}}"></i> {{label}}</span>
	                <small class="text-muted">{{description}}</small>
                   
	                <span class="mandatory badge badge-pill badge-warning"><i class="fas fa-exclamation-triangle"></i> Obligatoire</span>
	                <span class="readonly badge badge-pill badge-light"><i class="fas fa-eye"></i> Lecture seule</span>


	                {{#default}}<span class="badge">Valeur défaut : {{default}}</span>{{/default}}

	                <span class="right">
                        <div class="btn-group btn-group-sm" role="group">
                            <div class="btn btn-field-edit text-muted my-0 py-0 pointer" title="Éditer ce champ" href=""><i class="fas fa-ellipsis-v"></i></div>
                            <div class="btn btn-field-move text-muted my-0 py-0" title="Déplacer ce champ"><i class="fas fa-arrows-alt"></i></div>
                            <div class="btn btn-field-remove text-danger my-0 py-0" title="Supprimer ce champ"><i class="far fa-trash-alt"></i></div>
                        </div>
	                </span>
                    <div class="clear"></div>
                </li>
		</template>
	</div>

	
</div>

<div class="dynamic-field-panel bg-white shadow p-3 fold" id="dynamic-field-form">
	<h5 class="text-muted"><i class="fas fa-bars"></i> Propriétés <i class="far fa-times-circle right pointer" onclick="$('.dynamic-field-panel').addClass('fold');"></i></h5>
    <div class="dynamic-field-main">
    	<div class="input-group mb-2">
    		<div class="input-group-prepend">
				<label class="input-group-text" for="label">Libellé</label>
			</div>
			<input id="label" name="label" class="form-control label" placeholder="" value="" type="text">
		</div>
		
		<div class="input-group mb-2">
			<div class="input-group-prepend">
				<label class="input-group-text" for="type">Type</label>
			</div>
			<select class="form-control select-control" name="type" id="type" onchange="dynamicform_fieldtype_change();">
				<?php
					$availableTypes = array();
					foreach(FieldType::available() as $key=>$type)
						$availableTypes[$key] = $type;
					uasort($availableTypes, function($a,$b){
						return $a->label > $b->label;
					});
					foreach($availableTypes as $type):
					?>
					<option data-options="<?php echo base64_encode(json_encode($type)); ?>" value="<?php echo $type->slug; ?>"><?php echo $type->label; ?></option>
				<?php endforeach; ?>
			</select>
		</div>

		<label for="mandatory">
		<input  type="checkbox" name="mandatory" id="mandatory" data-type="checkbox"> Obligatoire</label>
		<label for="readonly">
		<input type="checkbox" name="readonly" id="readonly" data-type="checkbox"> Lecture seule</label>
		
		<br/>
		<div class="input-group mb-2">
    		<div class="input-group-prepend">
				<label for="default" class="input-group-text">Valeur par défaut</label>
			</div>
			<input id="default" name="default" class="form-control" placeholder="" value="" type="text">
		</div>

		<div class="input-group mb-2">
    		<div class="input-group-prepend">
				<label class="input-group-text" for="description">Description</label>
			</div>
			<input id="description" name="description" class="form-control" placeholder="" value="" type="text">
		</div>

		<div class="btn btn-link align-center w-100 mt-2" onclick="$(this).next('.advanced').toggleClass('hidden')">PARAMETRES AVANCÉS</div>
		<div class="advanced hidden">

			<div class="input-group mb-2">
    			<div class="input-group-prepend">
					<label class="input-group-text" for="slug">Slug</label>
				</div>
				<input id="slug" name="slug" class="form-control" <?php if(!$myUser->can('dynamicForm','configure')) echo 'readonly="readonly"'; ?> placeholder="" value="" type="text">
			</div>

			<label class="font-weight-bold mt-2">OPTION DE RECHERCHES</label>
			<label><input type="checkbox" data-type="checkbox" id="show-filter">Afficher dans les filtres de recherche</label><br/>

			<label class="font-weight-bold text-muted mt-2">Désactiver les opérateurs de recherche suivants</label>
			<div id="deleted-operators">
				<?php 
		        global $databases_credentials;
		        $connector = $databases_credentials['local']['connector'];
				
				foreach($connector::operators() as $uid=>$operator): ?>
				<label><input type="checkbox" data-type="checkbox" value="<?php echo $uid ?>"><?php echo $operator["label"].' ('.$uid.')' ?></label><br/>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	
	<div class="dynamic-field-settings"></div>

	<div onclick="dynamicform_field_save();" class="btn btn-success w-100 mt-2"><i class="fas fa-check"></i> Enregistrer</div>

</div>