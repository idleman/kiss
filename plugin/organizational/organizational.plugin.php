<?php


//Déclaration d'un item de menu dans le menu principal
function organizational_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('organizational','read')) return;

	$item = array(
    'sort'=>4,
    'url'=>'index.php?module=organizational',
    'label'=>'Organigramme',
    'icon'=> 'fas fa-sitemap',
    'color'=> '#4c5d9e',
    'parent'=> 'organization'
    );
	$menuItems[] = $item;
}

//Cette fonction va generer une page quand on clique sur organizational dans menu
function organizational_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='organizational') return;
	$page = !isset($_['page']) ? 'sheet' : $_['page'];
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function organizational_install($id){
	if($id != 'organizational') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function organizational_uninstall($id){
	if($id != 'organizational') return;
	Entity::uninstall(__DIR__);
}

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');



function organizational_setting_menu(&$settingMenu){
    global $myUser;
    if(!$myUser->can('organizational','read')) return;

    $settingMenu[]= array(
        'sort' =>0,
        'url' => 'setting.php?section=organizational',
        'icon' => 'fas fa-angle-right',
        'label' => 'Organigramme'
    );
}

//Déclaration des pages de réglages
function organizational_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


Configuration::setting('organizational',array(
    "Paramètre généraux",
    'organizational_root' => array("label"=>"PDG de l'organigramme","legend"=>"L'utilisateur de niveau 0 de l'entreprise","type"=>"user","placeholder"=>"eg. Jean Bernard"),
    'organizational_hidden_users_ranks' => array("label"=>"Liste d'utilisateurs et de rangs à masquer","type"=>"user", "attributes"=> array("data-multiple"=>true)),
    'organizational_origin' => array("label"=>"Origine", "legend" => "Origine des utilisateurs à afficher dans l'organigramme","type"=>"text","placeholder"=>"eg. active_directory"),
));






//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js");


Plugin::addCss("/css/jquery.orgchart.min.css"); 
Plugin::addJs("/js/jquery.orgchart.min.js");


//Mapping hook / fonctions
function organizational_scope(&$sections){
    $sections['organizational'] = "Lecture de l'organigramme";
}
            
Plugin::addHook("install", "organizational_install");
Plugin::addHook("uninstall", "organizational_uninstall"); 
Plugin::addHook("menu_main", "organizational_menu"); 
Plugin::addHook("page", "organizational_page");    
Plugin::addHook("content_setting", "organizational_content_setting");

Plugin::addHook("menu_setting", "organizational_setting_menu"); 
 

?>