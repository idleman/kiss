<?php 
global $_, $conf, $myUser;
User::check_access('organizational','configure');

?>
<div class="row">
	<div class="col-md-12 preference-content" id="organizational-setting-form">
		<br>
		<button onclick="organizational_setting_save()" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</button>
		<h3>Réglages Organigramme</h3>
		<div class="clear"></div>
		<hr/>
		<div class="form-row">
			<div class="form-group col-md-12">
				<?php echo Configuration::html('organizational'); ?>
			</div>
		</div>
	</div>
</div>
<br/>
