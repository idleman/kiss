//CHARGEMENT DE LA PAGE

function init_plugin_organizational(){

   // var datasource = JSON.parse($('#chart-container').attr('data-source'));
   $.action({
      action : 'organizational_employee_search'
   },function(response){
        $('#chart-container-number').raiseNumber(0, parseInt(response.count));
        var oc = $('#chart-container').orgchart({
            data : response.json,
            nodeContent: 'title',
            pan: true,
            zoom: true,
            toggleSiblingsResp : false,
           // verticalLevel : 10,
            nodeTemplate : function(data){
               
                var sstHtml = data.sst? '<i class="fas fa-plus fa-2x icon-sst-bg"></i><i class="fas fa-plus-circle icon-sst"title="Titulaire SST (Services de Santé au Travail)" data-tooltip data-placement="top"></i>': '' ;
        
                return '<div class="title">'+data.name +'</div>'+
                            '<div class="content media">'+ '<div class="img-container align-self-center">' +
                            '<img src="'+ data.avatar +'" class="avatar-rounded pr-0">'+ 
                            sstHtml + '</div>' +
                        '<div class="media-body">'+data.title+'</div>'+
                    '</div>';
            },
            createNode : function(element,data){
                var managerColors = [
                    '#ff9800',
                    '#9c27b0',
                    '#009688',
                    '#0078d7',
                    '#192a56',
                    '#e84118',
                    '#44bd32',
                    '#353b48',
                    '#8c7ae6',
                    '#718093'
                ];

                if(data.children && data.children.length > 0){
                    var color = managerColors[data.level];
                    $('.title',element).css('background-color',color);
                }
                $(element).attr('data-service',data.service);
            },
            initCompleted : function(){
                init_tooltips();
                $('.orgchart').addClass('noncollapsable');

                var panel = $('.organizational-detail-panel');
                var tpl = panel.find('.organizational-detail-template').html();

                $(document).click(function(){
                    panel.removeClass('show');
                });
                panel.click(function(event){
                    event.stopPropagation();
                });
                

                //Ouvertur des détails lors du click sur un employé
                $('#chart-container .node').click(function(event){
                    var data = $(this).data();
                    data = $.extend(data,data.nodeData);
                    delete data.nodeData;
                    panel.find('.organizational-detail').html(Mustache.render(tpl,data));
                    panel.addClass('show');
                    event.stopPropagation();
                });

                //attribution des services sur les tables
                $('#chart-container table:eq(0) table').each(function(i,table){
                    table = $(table);
                    table.attr('data-service',table.find('.node').eq(0).attr('data-service'));
                });

                //On groupe toutes les lignes ayant le même service en une seule table
                $('#chart-container table:eq(0) table[data-service]').each(function(i,table){
                    table = $(table);
                    $('table[data-service="'+table.attr('data-service')+'"]:not(:eq(0)) tr').each(function(i,tr){
                        if($(tr).parent()==table) return;
                        $(tr).detach().appendTo(table);
                    });

                });
                
                //Si un service est vide on supprime sa colonne
                $('#chart-container table:eq(0) table[data-service]').each(function(i,table){
                    if($('tr',table).length==0){
                        var column =  $(this).parent();
                        column.remove();
                    }
                });

                //Pour chaque table de service, on ajoute une en-tête avec le libellé du service
                $('#chart-container table:eq(0) table').each(function(i,table){
                    table = $(table);
                    var service = table.attr('data-service');
                    if(service == table.parents('table').attr('data-service')) return;
                      
                    table.addClass('organizational-service');
                    table.prepend('<caption class="organizational-service-label" >'+service+'</caption>');
                });

                $('#chart-preloader').remove();
                $('.topLine').each(function(){ 
                    if($(this).width()<10) $(this).remove();
                });
                $('.lines').each(function(i, container){
                    $("td.leftLine:last", container).prev('.leftLine.topLine').remove();
                })

            }
        });

        oc.$chartContainer.on('touchmove', function(event) {
            event.preventDefault();
        });
    });
}

//Enregistrement des configurations
function organizational_setting_save(){
    $.action({ 
        action: 'organizational_setting_save', 
        fields:  $('#organizational-setting-form').toJson() 
    },function(r){
        $.message('success',r.message);
    });
}
