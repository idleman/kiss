<?php
try{
	global $myUser,$conf;
	$users = User::getAll(array('right' => false, 'force' => true));

?>
<div class="col-md-12">
	<h3>Organigramme</h3>
	<hr/>
	<p>Société de <span id="chart-container-number" class="font-weight-bold">0</span> employés</p>
	<div id="chart-container"><span id="chart-preloader" class="text-info"><i class="fas fa-spinner fa-pulse m-0"></i> Chargement...</span></div>

	<div class="organizational-detail-panel">
		<div class="organizational-detail"></div>
		<div class="organizational-detail-template">
			<img src="{{avatar}}" class="avatar-large avatar-rounded">
			<h1>{{name}}</h1>
			{{#sst}}<p class="sst text-muted text-center"><i class="fas fa-plus-circle icon-sst" ></i>Titulaire SST : Sauveteur Secouriste du Travail</p>{{/sst}}
			{{#title}}<h2>{{title}}</h2>{{/title}}
			{{#service}}<h3>{{service}}</h3>{{/service}}
			{{#manager}} <small class="text-muted d-block my-2 center">Manager <span class="font-weight-bold">{{manager}}</span></small>{{/manager}}
			{{#mobile}}<a href="tel:{{mobile}}"><i class="fas fa-mobile-alt"></i> {{mobile}}</a>{{/mobile}}
			{{#mail}}<a href="mailto:{{mail}}"><i class="far fa-envelope-open"></i> {{mail}}</a>{{/mail}}
		</div>
	</div>
</div>
<?php 
}catch(Exception $e){
	echo $e->getMessage();
}