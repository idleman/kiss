<?php

	Action::register('organizational_setting_save',function(&$response){
	
			global $myUser,$conf,$_;
			User::check_access('organizational','configure');
			if(!$myUser->superadmin) return;

			foreach(Configuration::setting('organizational') as $key=>$value){
				if(!is_array($value)) continue;
				$allowed[] = $key;
			}
			foreach ($_['fields'] as $key => $value)
				if(in_array($key, $allowed)) $conf->put($key,$value);

			$response['message'] = 'Configuration enregistrée';
		
	});

	Action::register('organizational_employee_search',function(&$response){
	
			global $myUser,$conf,$_;
			User::check_access('organizational','read');
			
			$users = array();
			$rootLogin = stripslashes($conf->get('organizational_root'));
			$filterOrigin = $conf->get('organizational_origin');
			$hiddenUsersRanks = $conf->get('organizational_hidden_users_ranks');

			if(empty($rootLogin)) throw new Exception("Merci de bien vouloir configurer le nom du PDG dans la partie réglages");
			$boss = User::byLogin($rootLogin);

			$allUsers = User::getAll(array('right' => false, 'force' => true));

			foreach($allUsers as $user)
				$users[$user->login] = $user;


			$managers = array();

			//création du tableau de managers
			foreach($allUsers as $index => $user){
				if(!isset($user->manager)|| !is_object($user->manager) || $user->manager->login=='') {
					unset($allUsers[$index]);
					unset($users[$index]);
					continue;
				};

				if($user->login=='') continue;

				if (($filterOrigin && $user->origin != $filterOrigin)
					|| ($hiddenUsersRanks && strpos($hiddenUsersRanks, $user->login) !== false )) {
					unset($allUsers[$index]);
					unset($users[$index]);
					continue;
				}


				if(!isset($managers[$user->manager->login])){
					$managers[$user->manager->login] = array(
						'manager' => $users[$user->manager->login],
						'employees' => array()
					);
				}
				$managers[$user->manager->login]['employees'][] = $user;
			}

			$json = array();


			function organizational_tree($user,$users,$managers,&$json){

				$user->meta = json_decode($user->meta);
				
				$jsonUser = array(
					'name' => $user->fullName(),
					'manager' => isset($user->manager) ? $user->manager->fullName() : null ,
					'title' => isset($user->function)? $user->function:'Employé',
					'mobile' => $employee['mobile'] = !empty($user->mobile) ? $user->mobile : (isset($user->meta->personalPhone) ? $user->meta->personalPhone : $user->phone),
					'mail' => $user->mail,
					'avatar' => $user->getAvatar(),
					'service' => $user->service,
					'sst' => isset($user->meta->sst) && $user->meta->sst,
				);

				//var_dump($jsonUser['sst']);
				if(isset($managers[$user->login]) && count($managers[$user->login])!=0){
					$jsonUser['children'] = array();
					foreach($managers[$user->login]['employees'] as $subaltern){
						organizational_tree($subaltern,$users,$managers,$jsonUser['children']);

					}
				}
				
				$json[] = $jsonUser;
			}

			organizational_tree($boss,$users,$managers,$json);



			$response['json'] = (object) $json[0];
			$response['count'] = count($allUsers);
		
	});

?>
