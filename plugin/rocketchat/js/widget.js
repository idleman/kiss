/* HTML */
function rocketchat_widget_rocketchat_configure_save(widget,modal){
	var data = $('#rocketchat-widget-rocketchat-form').toJson();
	data.action = 'rocketchat_widget_rocketchat_configure_save';
	data.id = modal.attr('data-widget');

	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}