//CHARGEMENT DE LA PAGE
function init_plugin_rocketchat(){
	switch($.urlParam('page')){
		default:
		break;
	}
	
	
	$('#chats').sortable_table({
		onSort : rocketchat_chat_search
	});
}


//Enregistrement des configurations
function rocketchat_setting_save(){
	$.action({ 
		action: 'rocketchat_setting_save', 
		fields:  $('#rocketchat-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}


/** CHAT **/
//Récuperation d'une liste de chat dans le tableau #chats
function rocketchat_chat_search(callback){
	var box = new FilterBox('#filters');
	$('#chats').fill({
		action:'rocketchat_chat_search',
		filters: box.filters(),
		sort: $('#chats').sortable_table('get')
	},function(response){
		$('.results-count span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément chat
function rocketchat_chat_save(){
	var data = $('#chat-form').toJson();
	$.action(data,function(r){
		
		$.message('success','Enregistré');
	});
}


//Suppression d'élement chat
function rocketchat_chat_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	
	$.action({
		action: 'rocketchat_chat_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

