<div class="widget-rocketchat">
	<?php 
global $myUser,$conf;
try{
$chan = empty( $widget->data('chan')) ? '':  $widget->data('chan');
	
if(empty($conf->get('rocketchat_host'))) throw new Exception("Le serveur chat n'est pas configuré, merci de contacter un administrateur");
if(empty($chan)) throw new Exception('Aucun chan spécifié</h2><p>Veuillez configurer ce widget</p>');

$url = $conf->get('rocketchat_host');
$url .= $chan.'?layout=embedded';

$chan = urldecode($widget->data('chan'));
?>


<iframe class="frame-view" src="<?php echo $url; ?>"></iframe>

<?php 

}catch(Exception $e){
	?>
	<div class="text-center pt-3"><h2 class="text-muted"><i class="fab fa-rocketchat"></i> <?php echo $e->getMessage(); ?></div>
	<?php
}


?></div>