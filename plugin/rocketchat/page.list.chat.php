<?php
global $myUser,$conf;
User::check_access('rocketchat','read');

if(empty($conf->get('rocketchat_host'))) throw new Exception("Le serveur chat n'est pas configuré, merci de contacter un administrateur");

$url = $conf->get('rocketchat_host');

if(!empty($conf->get('rocketchat_embed_chan'))){
    $url .= $conf->get('rocketchat_embed_chan').'?layout=embedded';
} 


?>


<iframe class="frame-view" src="<?php echo $url; ?>"></iframe>

