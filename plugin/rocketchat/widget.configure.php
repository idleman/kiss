<?php
$chan = empty( $widget->data('chan')) ? '':  $widget->data('chan');

?>

<div id="rocketchat-widget-rocketchat-form">
	<div>
		<label class="m-0">Url :</label>
		<small>Spécifiez le chan à integrer au widget (ex : /channel/general)</small>
		<input class="form-control" type="text"  id="widget-rocketchat-chan" value="<?php echo $chan; ?>"/>
	</div>
</div> 