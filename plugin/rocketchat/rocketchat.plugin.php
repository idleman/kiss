<?php

//Déclaration d'un item de menu dans le menu principal
function rocketchat_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('rocketchat','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=rocketchat',
		'label'=>'Chat',
		'icon'=> 'fab fa-rocketchat',
		'color'=> '#3498db'
	);
}

//Cette fonction va generer une page quand on clique sur rocketchat dans menu
function rocketchat_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='rocketchat') return;
	$page = !isset($_['page']) ? 'list.chat' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function rocketchat_install($id){
	if($id != 'fr.core.rocketchat') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function rocketchat_uninstall($id){
	if($id != 'fr.core.rocketchat') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("rocketchat",array('label'=>"Gestion des droits sur le plugin rocketchat"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');

//Déclaration du menu de réglages
function rocketchat_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('rocketchat','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.rocketchat',
		'icon' => 'fas fa-angle-right',
		'label' => 'Rocketchat'
	);
}

//Déclaration des pages de réglages
function rocketchat_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Affichage du/des widget(s)
function rocketchat_widget(&$widgets){
	require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'rocketchat';
	$modelWidget->title = 'Chat';
	$modelWidget->icon = 'fab fa-rocketchat';
	$modelWidget->background = '#273c75';
	$modelWidget->load = 'action.php?action=rocketchat_widget_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js'];
	$modelWidget->css = [Plugin::url().'/css/widget.css'];

	$modelWidget->configure = 'action.php?action=rocketchat_widget_rocketchat_configure';
	$modelWidget->configure_callback = 'rocketchat_widget_rocketchat_configure_save';

	$modelWidget->description = "intégration rocket.chat";
	$widgets[] = $modelWidget;
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('rocketchat',array(
    "Général",
    'rocketchat_host' => array("label"=>"Addresse du serveur rocket.chat","type"=>"text"),
    'rocketchat_embed_chan'=> array("label"=>"Cloisoner dans le salon suivant",
    	"legend"=>"Laisser vide pour ne pas cloisonner",
    	'placeholder'=>'ex:/channel/general',"type"=>"text"),
));

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "rocketchat_install");
Plugin::addHook("uninstall", "rocketchat_uninstall"); 


Plugin::addHook("menu_main", "rocketchat_menu"); 
Plugin::addHook("page", "rocketchat_page");  
Plugin::addHook("menu_setting", "rocketchat_menu_setting");    
Plugin::addHook("content_setting", "rocketchat_content_setting");   
Plugin::addHook("widget", "rocketchat_widget");    

?>