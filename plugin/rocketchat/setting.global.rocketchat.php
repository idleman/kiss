<?php
global $myUser,$conf;
User::check_access('rocketchat','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	<div onclick="rocketchat_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Rocketchat</h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('rocketchat'); ?>
    </div>
</div>
