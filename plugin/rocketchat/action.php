<?php

	/** CHAT **/
	//Récuperation d'une liste de chat
	Action::register('rocketchat_chat_search',function(&$response){
		global $myUser,$_;
		User::check_access('rocketchat','read');
		require_once(__DIR__.SLASH.'Chat.class.php');
		
		// OPTIONS DE RECHERCHE, A ACTIVER POUR UNE RECHERCHE AVANCEE
		$query = 'SELECT * FROM '.Chat::tableName().' WHERE 1';
		$data = array();
		//Recherche simple
		if(!empty($_['filters']['keyword'])){
			$query .= ' AND label LIKE ?';
			$data[] = '%'.$_['filters']['keyword'].'%';
		}

		//Recherche avancée
		if(isset($_['filters']['advanced'])) filter_secure_query($_['filters']['advanced'],array('label'),$query,$data);

		//Tri des colonnes
		if(isset($_['sort'])) sort_secure_query($_['sort'],array('label'),$query,$data);

		//Pagination
		$response['pagination'] = Chat::paginate(20,(!empty($_['page'])?$_['page']:0),$query,$data);

		$chats = Chat::staticQuery($query,$data,true,0);
		
		foreach($chats as $chat){
			$row = $chat->toArray();
			$response['rows'][] = $row;
		}
	});
	
	//Ajout ou modification d'élément chat
	Action::register('rocketchat_chat_save',function(&$response){
		global $myUser,$_;
		User::check_access('rocketchat','edit');
		require_once(__DIR__.SLASH.'Chat.class.php');
		$item = Chat::provide();
		$item->save();
	});
	

	//Suppression d'élement chat
	Action::register('rocketchat_chat_delete',function(&$response){
		global $myUser,$_;
		User::check_access('rocketchat','delete');
		require_once(__DIR__.SLASH.'Chat.class.php');
		Chat::deleteById($_['id']);
	});

	
	//Sauvegarde des configurations de rocketchat
	Action::register('rocketchat_setting_save',function(&$response){
		global $myUser,$_,$conf;
		User::check_access('rocketchat','configure');
		//Si input file "multiple", possibilité de normlaiser le
		//tableau $_FILES récupéré avec la fonction => normalize_php_files();
		
		foreach(Configuration::setting('rocketchat') as $key=>$value){
			if(!is_array($value)) continue;
			$allowed[] = $key;
		}
		foreach ($_['fields'] as $key => $value) {
			if(in_array($key, $allowed))
				$conf->put($key,$value);
		}
	});
	
	Action::register('rocketchat_widget_load',function(&$response){
		global $myUser;
		User::check_access('rocketchat','read');
		require_once(__DIR__.SLASH.'..'.SLASH.'dashboard'.SLASH.'DashboardWidget.class.php');
		$widget = DashboardWidget::current();
		$widget->title = 'Widget Rocketchat';
		ob_start();
		//Décommenter après avoir créé widget.php
		require_once(__DIR__.SLASH.'widget.php');
		$widget->content = ob_get_clean();
		echo json_encode($widget);
		exit();
	});
	
	Action::register('rocketchat_widget_rocketchat_configure',function(&$response){
		global $myUser;
		User::check_access('rocketchat','read');
		require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
		$widget = DashboardWidget::current();
		ob_start();
		require_once(__DIR__.SLASH.'widget.configure.php');
		$content = ob_get_clean();
		echo $content ;
		exit();
	});

	Action::register('rocketchat_widget_rocketchat_configure_save',function(&$response){
		global $myUser,$_;
		User::check_access('rocketchat','read');
		require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
		$widget = DashboardWidget::getById($_['id']);
		$widget->data('chan',$_['widget-rocketchat-chan']);
		$widget->save();
	});


?>