<?php
 	class Template{
 		public static function all($template){
 			$templates = array();

 			foreach (glob(self::dir($template).'*') as $path) {
 				$relativePath = str_replace(self::dir($template),'',$path);
 				if(is_file($path)){
					$infos = self::getManifest($path);
 					$templates[$infos['label']] = array('file'=>$path,'syntax'=>$infos['syntax'],'infos' => $infos);
 				}else{
 					$templates = array_merge($templates,self::all($template.SLASH.$relativePath));
 				}
 			}

 			uasort($templates,function($a,$b){
 			
 				if(!isset($a['infos']['sort']) && !isset($b['infos']['sort'])) return $a['infos']['label'] > $b['infos']['label'] ?  1 : -1  ;
 				if( !isset($a['infos']['sort']) && isset($b['infos']['sort']) ) return 1;
 				if( isset($a['infos']['sort']) && !isset($b['infos']['sort']) ) return -1;

 				return $a['infos']['sort'] > $b['infos']['sort'] ?  1 : -1  ;
 			});

 			return $templates;
 		}

 		public static function dir($tpl = null){
 			return __DIR__.SLASH.'template'.SLASH.(isset($tpl)? $tpl.SLASH:'');
 		}

 		public static function getManifest($file){
 			$f = fopen($file, 'r');
			$line = fgets($f);
			fclose($f);
			return json_decode($line,true);
 		}

 		public static function filters($templatePath){
 			$filters = array();
			foreach (Template::all($templatePath) as $template) {
				if(empty($template['infos']['options'])) continue;
				foreach ($template['infos']['options'] as $key => $value){ 
					if(!is_array($value))$value = array('label'=>$value);
					$value['slug'] = $key;
					$filters[$key] = $value;
				}
			}
			
			ksort($filters);
			return $filters;
 		} 

 		public static function types(){
 			$types = FieldType::available();
 			$oldTypes = array();

 			$instance= new User();
 			//old pure sql types
 			global $databases_credentials;
			$sgbd = $databases_credentials[$instance->baseUid]['connector'];

 			foreach(array('dropzone') as $forbidden) 
 				if(isset($types[$forbidden])) unset($types[$forbidden]);
 			
 			foreach ($sgbd::types() as $key => $value) {
 				$oldType = new FieldType();

 				$oldType->slug =  $key;
				$oldType->label =  $key;
				$oldType->sqlType =  $key;
				$oldType->icon = 'fas fa-font';
				$oldType->description = 'Type sql '.$key;
				$oldType->default = '';
				$oldType->onInput = $types['text']->onInput;
				$oldType->default_attributes = array(
					'class'=>'"form-control"',
					'type'=>'"text"'
				);

 				$oldTypes[$key] = $oldType;

 			}

			$entityField = new FieldType();
			$entityField->label =  'Entité';
			$entityField->sqlType =  'entity-external-entity';
			$entityField->icon = 'fas fa-font';
			$entityField->description = 'Entité ';
			$entityField->default = '';
			$entityField->onInput = $types['text']->onInput;
			$entityField->default_attributes = array(
				'class'=>'"form-control"',
				'type'=>'"text"'
			);

	 		return array(
	 			'Types programmes'=>$types,
	 			'Entités'=>array('entity-external-entity'=>$entityField),
	 			'Types sql (deprecated)'=>$oldTypes);
 		}

 	}
?>