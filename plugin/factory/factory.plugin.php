<?php


//Déclaration d'un item de menu dans le menu principal
function factory_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('factory','read')) return;
	$menuItems[] = array(
		'sort'=>100,
		'url'=>'index.php?module=factory',
		'label'=>'Factory',
		'icon'=> 'fas fa-cube',
		'color'=> '#ef1e4e'
	);
}



//Cette fonction va generer une page quand on clique sur Modele dans menu
function factory_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='factory') return;
	$page = !isset($_['page']) ? 'factory' : $_['page'];
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function factory_install($id){
	if($id != 'fr.core.factory') return;
	Entity::install(__DIR__);

}

//Fonction executée lors de la désactivation du plugin
function factory_uninstall($id){
	if($id != 'fr.core.factory') return;
	Entity::uninstall(__DIR__);
}


//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


function factory_sanitize($text, $tableName=false,$allowChars='_'){
	$returned = '';
	foreach (explode(' ',trim($text)) as $word) {
		$returned .= $tableName ? mb_strtolower($word).'_' : ucfirst(mb_strtolower($word));
	}
	if($tableName) $returned = rtrim($returned, '_');

	$unwanted = array( 'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
		'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
		'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
		'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
		'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

	$returned = strtr($returned, $unwanted);
	return preg_replace('|[^a-z0-9'.preg_quote($allowChars).']|i','',$returned);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('factory',array('label'=>'Gestion des droits sur le plugin Factory'));


//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "factory_install");
Plugin::addHook("uninstall", "factory_uninstall");


Plugin::addHook("menu_main", "factory_menu"); 
Plugin::addHook("page", "factory_page");   

?>