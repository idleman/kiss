<?php 
global $myUser;
User::check_access('factory','read');
require_once(__DIR__.SLASH.'Template.class.php');
?>
<div  id="factoryForm"  data-action="factory_render">

<div class="row">
	<div class="col-xl-7">
		<h6 class="text-muted font-weight-bold">PLUGIN</h6>
		
		<div class="shadow-sm bg-white p-2">
			<div class="input-group mb-2">
				<div class="input-group-prepend">
					<span class="input-group-text">Template</span>
				</div>
				<select class="form-control" id="template" onchange="factory_change_template(event);">
					<?php foreach(glob(Template::dir().'*') as $tpl): ?>
					<option value="<?php echo basename($tpl); ?>"><?php echo basename($tpl); ?></option>
					<?php endforeach; ?>
				</select>
			</div>

			<div class="input-group mb-2">
				<div class="input-group-prepend">
				    <span class="input-group-text">Slug</span>
				 </div>
				<input type="text" class="form-control" placeholder="Libellé technique du plugin" id="plugin">

				<div class="input-group-prepend">
				    <span class="input-group-text">Libellé</span>
				 </div>
				<input type="text" class="form-control" placeholder="Libellé visible pour les humains" id="pluginLabel">

				<input type="text" data-type="color" class="form-control input-group-append" value="#273c75" id="plugin-color">
				<input type="text" data-type="icon" class="form-control input-group-append" value="far fa-question-circle" id="plugin-icon">
			</div>

			<input type="text" class="form-control mt-2 form-control-sm" placeholder="Description courte (optionnel)" id="description">
		</div>

		<h6 class="text-muted font-weight-bold mt-2">ENTITE</h6>
		<div class="list-group shadow-sm">
			<span class="list-group-item p-2">


				<div class="input-group">
				  <div class="input-group-prepend">
				    <span class="input-group-text">Classe</span>
				  </div>
				  <input type="text" class="form-control entity-picker" placeholder="Nom physique de l'entité" id="entity">
				  <div class="input-group-prepend">
				    <span class="input-group-text">Libellé</span>
				  </div>
				  <input type="text" class="form-control" placeholder="Libellé visible pour les humains" id="entityLabel">
				</div>

				
				
			</span>
			<!-- champ d'entité -->
			<ul id="entity-fields" class="entity-fields">
				
				<li class="list-group-item field p-2">
					<div class="row">
						<div class="col-xl-4">
							<input type="text" class="form-control input-small fieldSlug" placeholder="slug"> 
						</div> 
						<div class="col-xl-4">
							<input type="text" class="form-control input-small fieldLabel" placeholder="Libellé"> 
						</div> 
						<div class="col-xl-4">

							<select class="form-control input-small fieldType" onchange="factory_change_type(this)">
								<?php foreach(Template::types() as $category=>$values): ?>
									<optgroup label="<?php echo $category; ?>">
									<?php foreach($values as $key=>$value): ?>
										<option value="<?php echo $key; ?>"><?php echo $value->label; ?></option>
									<?php endforeach; ?>
									</optgroup>
								<?php endforeach; ?>
							</select>
							<input type="text" data-type="entitypicker" onchange="factory_render()" class="form-control entity-picker hidden mt-1" >
						</div>
					</div>
					<ul class="field-menu">
						<li><i class="btn-move fas fa-arrows-alt text-muted"></i></li>
						<li onclick="factory_removeline(this)"><i class="btn-remove fas fa-times text-danger"></i></li>
						<li onclick="factory_addline(this)"><i class="btn-remove fas fa-plus text-success"></i></li>
					</ul>
				</li>

			</ul>
		</div>
		<br>
		<div id="factoryFilters">
			<h6 class="text-muted font-weight-bold">OPTIONS</h6>
			<ul class="list-group shadow-sm">
				<li class="hidden list-group-item">
					<label><input onchange="factory_render()" data-checked="{{default}}" type="checkbox" data-id="{{slug}}" data-type="checkbox"> 
					{{label}} <small class="text-muted"> - {{slug}}</small></label>
				</li>
			</ul>
		</div>
		<hr>
		<a href="#" class="pointer" onclick="$(this).next().toggleClass('hidden');">Voir les conventions de nommage</a>
		<div class="hidden">
			<h5>Conventions de nommage</h5>
			<ul>
				<li><strong>Méthode et fonctions</strong> <code>function get_users(){}</code> - Séparation par "_" et tout en minuscule <i>(Snake Case)</i></li>
				<li><strong>Classes</strong> <code>class MaClasse {}</code> - Séparation par camelCase et premiere lettre en majuscule <i>(Upper Camel Case / Pascal Case)</i></li>
				<li><strong>Variables</strong> <code>$maVariable</code> - Séparation par camelCase tout en minuscule <i>(Lower Camel Case / Camel Case)</i></li>
				<li><strong>Constantes</strong> <code>const MA_CONSTANTE</code> - Séparation par "_"tout en majuscule <i>(Screaming Snake Case)</i></li>
				<li><strong>Classes css et id css</strong> <code>.ma-classe {} #mon-id{}</code> - Séparation par tiret tout en minuscule <i>(Kebab Case)</i></li>
				<li><strong>Actions</strong> <code>case 'plugin_entite_action'</code> - Séparation par "_" en minuscule <i>(Snake Case)</i></li>
			</ul>
		</div>
	</div>
	<div class="col-xl-5 bg-white p-2 shadow-sm">
		<ul class="nav nav-pills" id="factoryParts">
			<li class="nav-item hidden">
				<a class="nav-link {{#active}}active{{/active}}" data-part="{{label}}"  data-langage="{{syntax}}"  href="#">{{label}}</a>
			</li>


		</ul>
		<div id="factory-generate-button" class="btn btn-primary mb-2 mt-2 float-left btn-sm" onclick="factory_render(event,1)"><i class="fas fa-cogs"></i> Générer</div>
		<div  class="btn btn-dark ml-2 mb-2 mt-2 float-left btn-sm" onclick="copy_string($('#factoryCode').text(), $('#factoryCode'));"><i class="fas fa-copy"></i> Copier</div>

		<span id="factory-generate-message"></span>
		<div class="clear"></div>
		<pre><code id="factoryCode" onclick="select_text($('#factoryCode'),event)" class="php"></code></pre>
	</div>
</div>
</div>
<br/>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/monokai-sublime.min.css">

<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/languages/php.min.js"></script>



