{"label":"Fiche entité","syntax":"php"}
<?php 
User::check_access('{{plugin}}','read');
require_once(__DIR__.SLASH.'{{Entity}}.class.php');
${{entity}} = {{Entity}}::provide();
?>
<div class="plugin-{{plugin}}">
	<div id="{{entity-readable}}-form" class="row justify-content-md-center {{entity-readable}}-form" data-action="{{plugin_entity_deduplicate}}_save" data-id="<?php echo ${{entity}}->id; ?>">
		<div class="col-md-6 shadow-sm bg-white p-3">
			<h3>{{EntityLabel}} 
			<div onclick="{{plugin_entity_deduplicate}}_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<a href="index.php?module={{plugin}}&page=list.{{entity.readable}}" class="btn btn-small btn-dark right  mr-2">Retour</a>{{#history-save}}
			<div class="btn btn-small btn-info mb-2 btn-dark mx-2 right" data-scope="{{entity}}" data-uid="<?php echo ${{entity}}->id; ?>" data-show-important="true" data-type="history" data-tooltip title="Ouvrir l'historique"> <i class="far fa-comment-dots"></i></div>
		{{/history-save}}</h3>{{#fields}}
			{{^isChoice}}{{^isList}}{{^isBoolean}}<label for="{{key}}">{{label}}</label>
			{{input}}{{/isBoolean}}{{/isList}}{{/isChoice}}{{#isList}}<label for="{{key}}">{{label}}</label>
			<select class="form-control select-control" type="text" id="{{key}}">
			<?php foreach({{Entity}}::{{key}}s() as $slug=>$item): ?>
				<option <?php echo ${{entity}}->{{key}} == $slug ? 'selected="selected"' : '' ?> value="<?php echo $slug ?>" ><?php echo $item['label']; ?></option>
			<?php endforeach; ?>
			</select>{{/isList}}{{#isBoolean}}<br/><label for="{{key}}"><input <?php echo ${{entity}}->{{key}} == 1 ? 'checked="checked"' : '' ?>  data-type="checkbox" type="checkbox" id="{{key}}"> {{label}}</label>{{/isBoolean}}{{#isChoice}}<label for="{{key}}">{{label}}</label><br/>
			<?php foreach({{Entity}}::{{key}}s() as $slug=>$item): ?>
				<label><input <?php echo ${{entity}}->{{key}} == $slug ? 'checked="checked"' : '' ?>   value="<?php echo $slug ?>" data-type="radio" type="radio" name="{{key}}"> <?php echo $item['label']; ?></label>
			<?php endforeach; ?>	
			{{/isChoice}}{{/fields}}
			<br/>
			
		</div>
	</div>
</div>

