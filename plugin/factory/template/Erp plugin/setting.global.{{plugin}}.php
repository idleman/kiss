{"label":"Setting global","syntax":"php"}
<?php
global $myUser,$conf;
User::check_access('{{plugin}}','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	
        <h3 class="mb-0">{{#plugin.icon}}<i class="{{plugin.icon}}"></i>{{/plugin.icon}} Réglages {{Plugin.label}} <div onclick="{{plugin}}_setting_save();" class="btn btn-sm btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div></h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('{{plugin}}'); ?>
    </div>
</div>
