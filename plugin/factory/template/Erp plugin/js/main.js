{"label":"JS","syntax":"js"}
//CHARGEMENT DE LA PAGE
function init_plugin_{{plugin}}(){
	switch($.urlParam('page')){
		default:
		break;
	}
	{{#advanced-search}}$('#{{entity-readable}}s').sortable_table({
		onSort : {{plugin_entity_deduplicate}}_search
	});{{/advanced-search}}{{^advanced-search}}{{plugin_entity_deduplicate}}_search();{{/advanced-search}}
}

{{#setting}}//Enregistrement des configurations
function {{plugin}}_setting_save(){
	$.action({ 
		action: '{{plugin}}_setting_save', 
		fields:  $('#{{plugin}}-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}{{/setting}}

/** {{ENTITYLABEL}} **/
//Récuperation d'une liste  {{entityLabel}} dans le tableau #{{entity}}s
function {{plugin_entity_deduplicate}}_search(callback{{#search-export}},exportMode{{/search-export}}){{{#advanced-search}}
	var box = new FilterBox('#{{plugin_entity_deduplicate}}-filters');
	if(exportMode) $('.btn-export').addClass('btn-preloader');{{/advanced-search}}

	$('#{{entity-readable}}s').fill({
		action:'{{plugin_entity_deduplicate}}_search'{{#advanced-search}},
		filters: box.filters(),
		sort: $('#{{entity-readable}}s').sortable_table('get'){{/advanced-search}}{{#search-export}},
		export:  !exportMode ? false : exportMode{{/search-export}}
	},function(response){
		{{#advanced-search}}if(!exportMode) $('.results-count > span').text(response.pagination.total);
		{{/advanced-search}}if(callback!=null) callback();
	});
}

//Ajout ou modification {{entityLabel}}
function {{plugin_entity_deduplicate}}_save(){
	var data = $('#{{entity-readable}}-form').toJson();
	$.action(data,function(r){{{#edit-form}}
		$('#{{entity-readable}}-form').attr('data-id','');
		{{plugin_entity_deduplicate}}_search();{{/edit-form}}
		$('#{{entity-readable}}-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		$.message('success','Enregistré');
	});
}

{{#edit-form}}//Récuperation ou edition {{entityLabel}}
function {{plugin_entity_deduplicate}}_edit(element){
	var line = $(element).closest('.item-line');

	$.action({
		action: '{{plugin_entity_deduplicate}}_edit',
		id: line.attr('data-id')
	},function(r){
		$('#{{entity-readable}}-form').fromJson(r);
		init_components('#{{entity-readable}}-form');
		$('#{{entity-readable}}-form').attr('data-id',r.id);
	});
}
{{/edit-form}}
//Suppression {{entityLabel}}
function {{plugin_entity_deduplicate}}_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: '{{plugin_entity_deduplicate}}_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

