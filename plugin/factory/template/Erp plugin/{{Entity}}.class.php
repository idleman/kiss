{"label":"Entité","syntax":"php","sort":2}
<?php
/**
 * Define a {{EntityLabel}}
 * @author {{user.fullname}}
 * @category Plugin
 * @license MIT
 */
class {{Entity}} extends Entity{

	public $id;{{#fields}}
	public ${{key}}; //{{label}} ({{type.label}}){{/fields}}
	
	protected $TABLE_NAME = '{{plugin_table_deduplicate}}';
	public $entityLabel = '{{EntityLabel}}';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),{{#fields}}{{^isImage}}{{^isFile}}
		'{{key}}' => array('type'=>'{{type.slug}}','label' => '{{label_escaped}}'{{#entityRelativePath}},'link'=>'{{entityRelativePath}}'{{/entityRelativePath}}){{;}},{{/;}}{{/isFile}}{{/isImage}}{{/fields}}
	);

	//Colonnes indexées
	public $indexes = array();
	{{#fields}}{{#isList}}
	//liste des {{label}} possibles
	public static function {{key}}s($key=null){
		$items = array(
			'{{key}}-1' => array('label'=>'{{label}} 1') 
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non définis');
	}{{/isList}}{{#isChoice}}
	//liste des {{label}} possibles
	public static function {{key}}s($key=null){
		$items = array(
			'{{key}}-1' => array('label'=>'{{label}} 1') 
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non définis');
	}{{/isChoice}}{{/fields}}
}
?>