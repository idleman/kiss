{"label":"Liste entité","syntax":"php"}
<?php
global $myUser;
User::check_access('{{plugin}}','read');
require_once(__DIR__.SLASH.'{{Entity}}.class.php');

{{#fields}}{{#isList}}${{key}}s = array();
foreach ({{Entity}}::{{key}}s() as $key => ${{key}}) {
    ${{key}}s[$key] = ${{key}}['label'];
}{{/isList}}{{/fields}}


?>
<div class="plugin-{{plugin}}">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des {{entityLabel}}s</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="{{plugin_entity_deduplicate}}_search(null,true);" id="export-{{plugin}}s-btn" class="btn btn-info rounded-0 btn-squarred ml-1 btn-export" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                
                {{^edit-form}}
                <div class="my-auto ml-auto mr-0 noPrint">
                    <?php if($myUser->can('{{plugin}}', 'edit')) : ?>
                    <a href="index.php?module={{plugin}}&page=sheet.{{entity.readable}}" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                    <?php endif; ?>
                </div>
                {{/edit-form}}
            </div>
            <div class="clear noPrint"></div>
        </div>
        {{#advanced-search}}
        <div class="col-md-12">
            <select id="{{plugin_entity_deduplicate}}-filters" data-type="filter" data-label="Recherche" data-function="{{plugin_entity_deduplicate}}_search">
                <option value="main.label" data-filter-type="text">Libellé</option><!--{{#fields}}
                <option value="main.{{key}}" data-filter-type="{{type.search-slug}}"{{#isList}} data-values='<?php echo json_encode(${{key}}s); ?>' {{/isList}}>{{label}}</option>{{/fields}}-->
            </select>
        </div>
        {{/advanced-search}}
    </div>
    <h5 class="results-count my-2"><span></span> Résultat(s)
        <!-- bloc de preference de pagination -->
        <small class="text-muted right text-muted text-small"><div class="d-inline-block mr-1" data-type="pagination-preference" data-table="#{{entity-readable}}s" data-value="20" data-max-item="100"></div></small><div class="clear"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">

            {{#list-search}}
            <!-- présentation liste -->
            <ul id="{{entity-readable}}s" class="list-group" data-entity-search="{{plugin_entity_deduplicate}}_search">
                <li data-id="[-[-id-]-]" class="hidden item-line list-group-item">
                        {{#fields}}
                        <div class="align-middle">{{#isFile}}
                            <ul class="list-group list-group-flush shadow-sm">
                                [-[-#{{key}}-]-]
                                <li class="list-group-item p-1"><a href="[-[-url-]-]">[-[-label-]-]</a></li>
                                [-[-/{{key}}-]-]
                            </ul>{{/isFile}}{{#isImage}}<img class="shadow-sm rounded-sm" style="max-width: 100px;height: auto;" src="[-[-{{key}}-]-]">{{/isImage}}{{#isUrl}}<a href="[-[-{{key}}-]-]">[-[-{{key}}-]-]</a>{{/isUrl}}{{#isUser}}<img src="[-[-{{key}}.avatar-]-]" class="avatar-mini avatar-rounded avatar-login" title="[-[-{{key}}.fullName-]-]"> [-[-{{key}}.fullName-]-]{{/isUser}}{{#isColor}}<i class="fas fa-dot-circle" style="color:[-[-{{key}}-]-]"></i> {{/isColor}}{{#isBoolean}}[-[-#{{key}}-]-] <i class="fas fa-check text-success"></i> OUI[-[-/{{key}}-]-][-[-^{{key}}-]-]<i class="fas fa-times text-danger"></i> Non[-[-/{{key}}-]-]{{/isBoolean}}{{#isIcon}}<i class="[-[-{{key}}-]-]"></i> {{/isIcon}}{{#isWysiwyg}}{[-[-{{key}}-]-]}{{/isWysiwyg}}{{#isDate}}[-[-{{key}}-readable-]-]{{/isDate}}{{#isList}}[-[-{{key}}.label-]-]{{/isList}}{{#isDictionary}}[-[-{{key}}.label-]-]{{/isDictionary}}{{#isChoice}}[-[-{{key}}.label-]-]{{/isChoice}}{{^isUrl}}{{^isFile}}{{^isImage}}{{^isUser}}{{^isBoolean}}{{^isWysiwyg}}{{^isDate}}{{^isDictionary}}{{^isList}}{{^isChoice}}[-[-{{key}}-]-]{{/isChoice}}{{/isList}}{{/isDictionary}}{{/isDate}}{{/isWysiwyg}}{{/isBoolean}}{{/isUser}}{{/isImage}}{{/isFile}}{{/isUrl}}</div>{{/fields}}
                    <div class="align-middle text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            {{^edit-form}}<a class="btn text-info" title="Éditer {{entity_readable}}" href="index.php?module={{plugin}}&page=sheet.{{entity.readable}}&id=[-[-id-]-]"><i class="fas fa-pencil-alt"></i></a>{{/edit-form}}
                            {{#edit-form}}<div class="btn text-info" title="Éditer {{entity_readable}}" onclick="{{plugin_entity_deduplicate}}_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                            {{/edit-form}}<div class="btn text-danger" title="Supprimer {{entity_readable}}" onclick="{{plugin_entity_deduplicate}}_delete(this);"><i class="far fa-trash-alt"></i></div>
                        </div>
                    </div>
                </li>
            </ul>
            {{/list-search}}

    		{{^list-search}}
            <!-- présentation tableau -->
            <table id="{{entity-readable}}s" class="table table-striped " data-entity-search="{{plugin_entity_deduplicate}}_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->{{#fields}}
                        <th data-sortable="{{key}}">{{label}}</th>{{/fields}}
                        <th></th>
                    </tr>
                </thead>
                {{#edit-form}}
                <thead>
                    <tr id="{{entity-readable}}-form" data-action="{{plugin_entity_deduplicate}}_save" data-id="">
                        <th>#</th>{{#fields}}
                        <th>{{input_novalue}}</th>{{/fields}}
                        <th class="text-right"><div onclick="{{plugin_entity_deduplicate}}_save();" class="btn btn-success"><i class="fas fa-check"></i> Enregistrer</div></th>
                    </tr>
                </thead>
                {{/edit-form}}
                <tbody>
                    <tr data-id="[-[-id-]-]" class="hidden item-line">
    	                <!--<td class="align-middle">[-[-id-]-]</td>-->{{#fields}}
    	                <td class="align-middle">{{#isFile}}
                            <ul class="list-group list-group-flush shadow-sm">
                                [-[-#{{key}}-]-]
                                <li class="list-group-item p-1"><a href="[-[-url-]-]">[-[-label-]-]</a></li>
                                [-[-/{{key}}-]-]
                            </ul>{{/isFile}}{{#isImage}}<img class="shadow-sm rounded-sm" style="max-width: 100px;height: auto;" src="[-[-{{key}}-]-]">{{/isImage}}{{#isUrl}}<a href="[-[-{{key}}-]-]">[-[-{{key}}-]-]</a>{{/isUrl}}{{#isUser}}<img src="[-[-{{key}}.avatar-]-]" class="avatar-mini avatar-rounded avatar-login" title="[-[-{{key}}.fullName-]-]"> [-[-{{key}}.fullName-]-]{{/isUser}}{{#isColor}}<i class="fas fa-dot-circle" style="color:[-[-{{key}}-]-]"></i> {{/isColor}}{{#isBoolean}}[-[-#{{key}}-]-] <i class="fas fa-check text-success"></i> OUI[-[-/{{key}}-]-][-[-^{{key}}-]-]<i class="fas fa-times text-danger"></i> Non[-[-/{{key}}-]-]{{/isBoolean}}{{#isIcon}}<i class="[-[-{{key}}-]-]"></i> {{/isIcon}}{{#isWysiwyg}}{[-[-{{key}}-]-]}{{/isWysiwyg}}{{#isDate}}[-[-{{key}}-readable-]-]{{/isDate}}{{#isList}}[-[-{{key}}.label-]-]{{/isList}}{{#isDictionary}}[-[-{{key}}.label-]-]{{/isDictionary}}{{#isChoice}}[-[-{{key}}.label-]-]{{/isChoice}}{{^isUrl}}{{^isFile}}{{^isImage}}{{^isUser}}{{^isBoolean}}{{^isWysiwyg}}{{^isDate}}{{^isDictionary}}{{^isList}}{{^isChoice}}[-[-{{key}}-]-]{{/isChoice}}{{/isList}}{{/isDictionary}}{{/isDate}}{{/isWysiwyg}}{{/isBoolean}}{{/isUser}}{{/isImage}}{{/isFile}}{{/isUrl}}</td>{{/fields}}
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                {{^edit-form}}<a class="btn text-info" title="Éditer {{entity_readable}}" href="index.php?module={{plugin}}&page=sheet.{{entity.readable}}&id=[-[-id-]-]"><i class="fas fa-pencil-alt"></i></a>{{/edit-form}}
                                {{#edit-form}}<div class="btn text-info" title="Éditer {{entity_readable}}" onclick="{{plugin_entity_deduplicate}}_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                                {{/edit-form}}<div class="btn text-danger" title="Supprimer {{entity_readable}}" onclick="{{plugin_entity_deduplicate}}_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
            {{/list-search}}

            <br>
             {{#advanced-search}}<!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="[-[-value-]-]" title="Voir la page [-[-label-]-]" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');{{plugin_entity_deduplicate}}_search();">
                    <span class="page-link">[-[-label-]-]</span>
                </li>
            </ul>{{/advanced-search}}
          
    	</div>
    </div>
</div>