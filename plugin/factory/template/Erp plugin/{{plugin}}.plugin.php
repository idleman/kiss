{"label":"Plugin","syntax":"php","sort":1,"options":{"search-export":{"label":"Export de la recherche","default":true},"widget":{"label" : "Widget de dashboard","default":false},"logic-remove":{"label": "Suppression logique","default":false},"advanced-search":{"label" : "Recherche avancée", "default":true},"setting":{"label" : "Page réglages","default":true},"edit-form":{"label" : "Formulaire dans le tableau de liste","default":false},"list-search":{"label" : "Recherche en forme de liste (non tableau)","default":false},"history-save":{"label" : "Sauvegarder l'historique des modifications (save, delete)","default":false},"component":{"label" : "Générer un composant js autocomplete","default":false} }}
<?php



//Déclaration d'un item de menu dans le menu principal
function {{plugin}}_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('{{plugin}}','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module={{plugin}}',
		'label'=>'{{Plugin.label}}',
		'icon'=> '{{plugin.icon}}',
		'color'=> '{{plugin.color}}'
	);
}

//Cette fonction va generer une page quand on clique sur {{plugin.label}} dans menu
function {{plugin}}_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='{{plugin}}') return;
	$page = !isset($_['page']) ? 'list.{{entity.readable}}' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function {{plugin}}_install($id){
	if($id != 'fr.core.{{plugin}}') return;
	Entity::install(__DIR__);

	{{#fields}}{{#isDictionary}}$dictionary = new Dictionary();
    	$dictionary->slug = '{{plugin}}_{{entity}}_{{key}}';
    	if(Dictionary::rowCount(array('slug'=>$dictionary->slug)) ==0){
	    	$dictionary->label = '{{EntityLabel}} : {{label}}';
	    	$dictionary->parent = 0;
	    	$dictionary->state = Dictionary::ACTIVE;
	    	$dictionary->save();

	    	foreach(array(
	    		'none'=>'Aucun(e)',
	    	) as $key=>$value){
		    	$item = new Dictionary();
				$item->slug = '{{plugin}}_{{entity}}_{{key}}_'.$key;
				$item->label = $value;
				$item->parent = $dictionary->id;
				$item->state = Dictionary::ACTIVE;
				$item->save();
			}
	    }
    {{/isDictionary}}{{/fields}}
}

//Fonction executée lors de la désactivation du plugin
function {{plugin}}_uninstall($id){
	if($id != 'fr.core.{{plugin}}') return;
	Entity::uninstall(__DIR__);
	{{#fields}}{{#isDictionary}}$dictionary = Dictionary::bySlug('{{plugin}}_{{entity}}_{{key}}');
    	if($dictionary!= false && $dictionary->id!=0){
    		Dictionary::delete(array('parent'=>$dictionary->id));
    		Dictionary::delete(array('id'=>$dictionary->id));
    	}
    {{/isDictionary}}{{/fields}}
}

//Déclaration des sections de droits du plugin
Right::register('{{plugin}}',array('label'=>'Gestion des droits sur le plugin {{plugin.label}}'));


//cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html
function {{plugin}}_action(){
	require_once(__DIR__.SLASH.'action.php');
}
{{#setting}}//Déclaration du menu de réglages
function {{plugin}}_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('{{plugin}}','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.{{plugin}}',
		'icon' => 'fas fa-angle-right',
		'label' => '{{Plugin.label}}'
	);
}

//Déclaration des pages de réglages
function {{plugin}}_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


require_once(__DIR__.SLASH.'action.php');


//Déclaration des settings de base
//Types possibles : voir FieldType.class.php. Un simple string définit une catégorie.
Configuration::setting('{{plugin}}',array(
    "Général",
    //'{{plugin}}_enable' => array("label"=>"Activer","type"=>"boolean"),
));
{{/setting}}{{#widget}}
//Affichage du/des widget(s)
function {{plugin}}_widget(&$widgets){
	
	require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
	$modelWidget = new DashboardWidget();
	$modelWidget->model = '{{plugin}}';
	$modelWidget->title = '{{Plugin.label}}';
	$modelWidget->icon = '{{plugin.icon}}';
	$modelWidget->background = '{{plugin.color}}';
	$modelWidget->load = 'action.php?action={{plugin}}_widget_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js'];
	$modelWidget->css = [Plugin::url().'/css/widget.css'];
	$modelWidget->description = "{{description}}";
	$widgets[] = $modelWidget;
}
{{/widget}}  

/*

*/

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 
{{#component}}Plugin::addCss("/css/component.css"); 
Plugin::addJs("/js/component.js"); 
{{/component}}
//Mapping hook / fonctions
Plugin::addHook("install", "{{plugin}}_install");
Plugin::addHook("uninstall", "{{plugin}}_uninstall"); 
Plugin::addHook("menu_main", "{{plugin}}_menu"); 
Plugin::addHook("page", "{{plugin}}_page"); {{#setting}}
Plugin::addHook("menu_setting", "{{plugin}}_menu_setting");{{/setting}}{{#setting}}
Plugin::addHook("content_setting", "{{plugin}}_content_setting");{{/setting}}{{#widget}}
Plugin::addHook("widget", "{{plugin}}_widget");{{/widget}}
?>