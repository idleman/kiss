<?php global $_,$myUser,$myFirm,$conf; ?>

<div class="modal sendmail-modal fade" id="sendmail-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<!-- Modal d'envoi de fichier-->
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="far fa-envelope-open"></i> Envoyer un e-mail</h5>
				<button type="button" class="btn btn-mini btn-close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>

			<div class="modal-body">
				<div id="sendForm">
					<div class="row pt-3 pb-4 px-0 m-auto">
						<div class="col-md-8 sendmail-header">
							<div class="form-group from-block row">
								<label for="from" class="mb-0 col-md-2 col-sm-12"><span class="my-auto mx-0">Expéditeur : </span></label>
								<input placeholder="adresse@exemple.com" type="text" value="<?php echo isset($_['from']) && !empty($_['from'])?$_['from']:$myUser->mail; ?>" class="mb-1 form-control form-control-sm flex-element col-md-10" id="from" name="from">
							</div>
							<ul class="form-group recipient-block row recipientBox">
								<label for="recipient" class="mb-0 col-md-2 col-sm-12"><span class="my-auto mx-0">Destinataire(s) : </span></label>
								<li class="recipientLine p-0 col-md-10 mb-1">
									<div class="input-group input-group-sm flex-element recipient-line ml-auto">
										<div class="input-group-prepend px-2">
											<select class="recipient-type position-relative" data-type="dropdown-select" title="Type de destinataire">
												<option data-icon="fas fa-user" value="to" data-title="Destinataire direct">Pour</option>
												<option data-icon="fas fa-user-friends" value="cc" data-title="Destinataire en copie">Cc </option>
												<option data-icon="fas fa-user-secret" value="cci" data-title="Destinataire en copie cachée">Cci</option>
											</select>
										</div>
										<input type="text" data-type="to" value="" placeholder="Emails séparés par , ou ;" class="recipient form-control form-control-sm pl-2">
										<div class="input-group-append">
											<?php if(Plugin::exist('client')): ?>
											<button class="btn px-1 text-success" title="Ajouter un destinataire" onclick="sendmail_contact_add(this)"><i class="fas fa-user-plus"></i></button>
											<?php endif; ?>
											<button class="btn px-1 text-danger" title="Supprimer le destinataire" onclick="sendmail_contact_delete(this)"><i class="far fa-trash-alt"></i></button>
										</div>
									</div>
								</li>
							</ul>
							<div class="form-group subject-block row mb-0">
								<label for="subject" class="mb-0 col-md-2 col-sm-12"><span class="my-auto mx-0">Sujet : </span></label>
								<input placeholder="Objet" type="text" value="<?php echo isset($_['subject'])?$_['subject']:'' ?>" class="form-control form-control-sm flex-element col-md-10" id="subject" name="subject">
							</div>
						</div>
						<div class="col-md-4 sendmail-attachments">
							<div data-type="dropzone" data-label="Faites glisser vos pièces jointes ici" data-delete="sendmail_delete_document" class="form-control w-100 m-0 h-100" id="attachments" name="attachments"></div>
						</div>
					</div>
					<textarea data-type="wysiwyg" id="message" class="mb-0 mt-0"><?php 
						echo isset($_['message'])?$_['message']:'';
						if (!isset($_['signature']) || $_['signature']==true):
							$signature = $conf->get('sendmail_allow_custom_signature') && !$conf->get('sendmail_force_global_signature') ? sendmail_user_signature() : $conf->get('sendmail_default_signature');

							echo template($signature,array(
								'user.mail' => $myUser->mail,
								'user.firstname' => $myUser->firstname,
								'user.fullname' => $myUser->fullname(),
								'user.function' => $myUser->function,
								'user.phone' => $myUser->phone,
								'user.mobile' => $myUser->mobile,
								'firm.logo' => $myFirm->logo('publicPath'),
								'firm.label' => $myFirm->label,
								'firm.address' => $myFirm->address(),
								'firm.phone' => $myFirm->phone,
								'firm.mail' => $myFirm->mail,
								'firm.fax' => $myFirm->fax
							), true);
						endif; ?>
					</textarea>
				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-small btn-light" data-dismiss="modal">Fermer</button>
				<button type="button" class="btn btn-small btn-primary btn-send" ><i class="far fa-paper-plane"></i> <span>Envoyer</span></button>
			</div>
		</div>
	</div>
</div>

