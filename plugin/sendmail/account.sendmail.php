<?php 
global $myUser,$myFirm,$conf;
User::check_access('sendmail', 'read');
if(!$conf->get('sendmail_allow_custom_signature')) return;

?>
<div class="row">
	<div class="col-md-12 mb-2" id="order-signature-form">
		<br>
		<div onclick="sendmail_user_save_preference()" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
		<h3>E-mail</h3>
		<div class="clear"></div>
		<hr>
		<h5 class="d-inline-block">Signature e-mail personnalisée</h5><small class="text-muted"> - Signature e-mail utilisée lors de l'envoi de mail depuis <?php echo PROGRAM_NAME; ?> :</small>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<label class="my-3">Utiliser les informations utilisateur :</label>
		<small>
			<table class="table table-sm table-striped">
					<tr>
						<th>Balise</th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><code>{{user.mail}}</code></td>
						<td>E-mail de l'utilisateur connecté</td>
					</tr>
					<tr>
						<td><code>{{user.firstname}}</code></td>
						<td>Prénom de l'utilisateur connecté</td>
					</tr>
					<tr>
						<td><code>{{user.fullname}}</code></td>
						<td>Prénom et Nom de l'utilisateur connecté</td>
					</tr>
					<tr>
						<td><code>{{user.function}}</code></td>
						<td>Fonction l'utilisateur connecté</td>
					</tr>
					<tr>
						<td><code>{{user.phone}}</code></td>
						<td>N° Téléphone l'utilisateur connecté</td>
					</tr>
					<tr>
						<td><code>{{user.mobile}}</code></td>
						<td>N° Téléphone portable de l'utilisateur connecté</td>
					</tr>
					<tr>
						<td><code>{{firm.logo}}</code></td>
						<td>Logo de la société</td>
					</tr>
					<tr>
						<td><code>{{firm.label}}</code></td>
						<td>Libellé de la société</td>
					</tr>
					<tr>
						<td><code>{{firm.phone}}</code></td>
						<td>N° Téléphone portable de la société</td>
					</tr>
					<tr>
						<td><code>{{firm.fax}}</code></td>
						<td>N° Fax de la société</td>
					</tr>
					<tr>
						<td><code>{{firm.mail}}</code></td>
						<td>E-mail de la société</td>
					</tr>
					<tr>
						<td><code>{{firm.address}}</code></td>
						<td>Adresse de la société</td>
					</tr>
				</tbody>
			</table>
		</small>
	</div>
	<div class="col-md-8">
		<form id="sendmail-user-setting-form">
			<textarea name="sendmail_email_signature" id="sendmail_email_signature" data-type="wysiwyg" class="mt-2"><?php echo sendmail_user_signature(); ?></textarea>
		</form>
	</div>
</div>