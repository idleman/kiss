<?php
//Fonction executée lors de l'activation du plugin
function sendmail_install($id){
	if($id != 'fr.core.sendmail') return;
	Entity::install(__DIR__);
	
	global $conf;
	$signature = '<div>Cordialement,<br />
		<b><span style="color: #ff0000;" color="#ff0000"></span></b></div>
		<hr style="width: 100%; height: 2px;" />
		<table border="0" cellpadding="0" width="550">
			  <tr>
			    <td>
				    <p style="margin-top: 0; margin-left:20px;color: #4e4d55; font-family: Arial, sans-serif; font-size:15px;">
				    	<strong>{{user.fullname}}</strong><br>
						<strong><span style="color: #3aa8e0;">{{user.function}}</span></strong><br>
						Soci&eacute;t&eacute; <strong>SYS<span style="color: #3aa8e0;">1</span></strong><br>
						Email : {{user.mail}}<br>
				        Portable : {{user.mobile}}
			   	 	</p>
			   	</td>
			  </tr>
		</table>';
	$conf->put('sendmail_default_signature',$signature);
}

//Fonction executée lors de la désactivation du plugin
function sendmail_uninstall($id){
	if($id != 'fr.core.sendmail') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("sendmail",array('label'=>"Gestion des droits sur le plugin sendmail"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function sendmail_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('sendmail','configure')) return;
	$settingMenu[] = array(
		'sort' =>1,
		'url' => 'setting.php?section=global.sendmail',
		'icon' => 'fas fa-angle-right',
		'label' => 'Envois E-mail'
	);
}

//Déclaration des pages de réglages
function sendmail_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Déclaration du menu de réglages du compte user
function sendmail_menu_account(&$accountMenu){
	global $myUser, $conf;

	if(!$conf->get('sendmail_allow_custom_signature') || !$myUser->can('sendmail', 'read')) return;
	$accountMenu[]= array(
		'sort' => 1,
		'url' => 'account.php?section=sendmail',
		'icon' => 'fas fa-angle-right',
		'label' => 'E-mail',
	);	
}

//Déclaration des pages de réglages du compte user
function sendmail_content_account(){
	global $_;
	if(file_exists(__DIR__.SLASH.'account.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'account.'.$_['section'].'.php');
}

function sendmail_user_signature(){
	global $myUser;
	$html = '';
	if($myUser->connected()) $html = htmlentities($myUser->preference('sendmail_email_signature'));
	return $html;
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('sendmail',array(
    "Général",
    'sendmail_allow_custom_signature' => array("label"=>"Signatures personnalisées", "legend"=>"Chaque utilisateur peut créer sa propre signature HTML", "type"=>"boolean"),
    'sendmail_force_global_signature' => array("label"=>"Forcer la signature globale", "legend"=>"Les envois par e-mail récupèrent obligatoirement la signature globale", "type"=>"boolean"),
    'sendmail_default_signature' => array("label"=>"Signature ajoutée aux mails", "legend"=>"Vous pouvez récuperer les informations utilisateur avec les balises : 
		<table class='table-sm w-100 border-0 mt-2'>
			<tbody>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{user.mail}}</code></td><td class='border-0'>E-mail de l'utilisateur connecté</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{user.firstname}}</code></td><td class='border-0'>Prénom de l'utilisateur connecté</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{user.fullname}}</code></td><td class='border-0'>Prénom et Nom de l'utilisateur connecté</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{user.function}}</code></td><td class='border-0'>Fonction l'utilisateur connecté</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{user.phone}}</code></td><td class='border-0'>N° Téléphone l'utilisateur connecté</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{user.mobile}}</code></td><td class='border-0'>N° Téléphone portable de l'utilisateur connecté</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{firm.logo}}</code></td><td class='border-0'>Logo de la société</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{firm.label}}</code></td><td class='border-0'>Libellé de la société</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{firm.phone}}</code></td><td class='border-0'>N° Téléphone portable de la société</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{firm.fax}}</code></td><td class='border-0'>N° Fax de la société</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{firm.mail}}</code></td><td class='border-0'>E-mail de la société</td></tr>
				<tr class='bg-transparent border-0'><td class='border-0'><code>{{firm.address}}</code></td><td class='border-0'>Adresse de la société</td></tr>
			</tbody>
		</table>","type"=>"wysiwyg"),
));

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "sendmail_install");
Plugin::addHook("uninstall", "sendmail_uninstall"); 

Plugin::addHook("menu_setting", "sendmail_menu_setting");    
Plugin::addHook("content_setting", "sendmail_content_setting");   
    
//Affichage page dans menu du compte
Plugin::addHook("menu_account", "sendmail_menu_account");
Plugin::addHook("content_account", "sendmail_content_account");

?>