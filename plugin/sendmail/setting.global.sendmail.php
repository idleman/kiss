<?php
global $myUser,$conf;
User::check_access('sendmail','configure');
?>

<div class="row">
	<div class="col-md-12"><br>
        <div onclick="sendmail_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Sendmail</h3>
        <div class="clear"></div>
        <hr>
	</div>
</div>
<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		<?php echo Configuration::html('sendmail'); ?>
	</div>
</div>
