<?php


	Action::register('sendmail_mail_preview',function(&$response){
		global $myUser;
		if(!$myUser->connected()) throw new Exception("Permission denied");
		require_once(__DIR__.SLASH.'page.sheet.mail.php');
		exit();
	});

	//Ajout ou modification d'élément mail
	Action::register('sendmail_mail_send',function(&$response){
	
			global $myUser,$conf,$_;
			if(!$myUser->connected()) throw new Exception("Permission denied");
			
			Plugin::callHook("sendmail_before_send",array(&$_));

			if(!isset($_['from']) || empty($_['from'])) throw new Exception("Veuillez préciser l'adresse de l'expediteur");
			if(!isset($_['recipient']) || empty($_['recipient'])) throw new Exception("Veuillez préciser au moins un destinataire");
			
			$_['recipient'] = array_merge_recursive($_['recipient'], array('to'=>array(),'cc'=>array(),'cci'=>array()));
			foreach($_['recipient'] as $type => $mails)
				foreach($mails as $i => $mail)
					if(empty($mail) || !filter_var($mail, FILTER_VALIDATE_EMAIL)) throw new Exception('Le mail "'.$mail.'" n\'est pas correctement formaté');

			//Envoi du mail
			$mail = new Mail();
			$mail->recipients = $_['recipient'];
			$mail->expeditor = $_['from'];
			$mail->reply = $_['from'];
			$mail->title = html_entity_decode($_['subject'],ENT_QUOTES,'UTF-8');
			$mail->message = html_entity_decode($_['message']);
			
			if(!empty($_['files'])){
				$files = json_decode($_['files'],true);

				if($files!=false && count($files)!=0){
					foreach ($files as $file) {
						$path = isset($file['temporary']) ? File::temp().SLASH.str_replace(array('/', '\\'), SLASH, $file['path']) : str_replace(array('/', '\\'), SLASH, $file['path']);

						if(strpos($path, File::dir()) === false)
						    $path = File::dir().SLASH.$path;
						
						//Vérification de l'existence fichier
						if(!file_exists($path)) continue;
						
						$mail->attachments[] = array(
							'name' => $file['name'], 
							'type' => mime_content_type($path), 
							'stream'=> file_get_contents($path)
						);
					}
				}
			}
			$mail->send();

			Plugin::callHook("sendmail_after_send",array($_, $mail));

			$response['message'] = 'E-mail envoyé';
			$response['data'] = $_;
		
	});

	//Sauvegarde des configurations de sendmail
	Action::register('sendmail_setting_save',function(&$response){
	
			global $myUser,$_,$conf;
			User::check_access('sendmail','configure');

			foreach(Configuration::setting('sendmail') as $key=>$value)
				if(is_array($value)) $allowed[] = $key;

			foreach ($_['fields'] as $key => $value) {
				if(!in_array($key, $allowed)) continue;
				if($key=='sendmail_default_signature') $value = html_entity_decode($value);
				
				$conf->put($key,$value);
			}
		
	});

	//Sauvegarde des préférences user de sendmail
	Action::register('sendmail_user_save_preference',function(&$response){
	
			global $myUser,$_,$conf;
			User::check_access('sendmail','edit');
			if(!isset($_['preferences']) || !isset($_['preferences']['sendmail_email_signature'])) throw new Exception("Vous devez saisir une signature e-mail");
			
			$myUser->preference('sendmail_email_signature',html_entity_decode($_['preferences']['sendmail_email_signature']));
		
	});

?>