<?php require_once(__ROOT__.SLASH.'header.php'); 
	$url = base64_encode('index.php?module=wiki');
?>
<div class="wiki-login-box">
	<form class="login-form" data-action="login" data-url="<?php echo $url; ?>">
		<i class="far fa-bookmark wiki-logo"></i>
		<input type="text" class="form-control form-control-sm" id="login" name="login">
		<input type="password" data-type="password" class="form-control form-control-sm" name="password" id="password">
		<div class="btn btn-light" id="login-button" onclick="core_login(this);">Connexion</div>
	</form>
</div>
<?php require_once(__ROOT__.SLASH.'footer.php'); ?>