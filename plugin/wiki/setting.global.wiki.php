<?php
global $myUser,$conf;
User::check_access('wiki','configure');
?>

<div class="row">
	<div class="col-md-12">
		<br>
        <?php if($myUser->can('wiki', 'edit')) : ?>
        <div onclick="wiki_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <?php endif; ?>
        <h3>Réglages Wiki</h3>
        <div class="clear"></div>
        <hr/>
	</div>
</div>

<div class="row" id="wiki-settings-form" action="wiki_setting_save">
	<!-- search results -->
	<div class="col-xl-12">
			<div class="col-md-6">
				<label><strong>Logo du wiki</strong><small class="text-muted"> - Le logo affiché en haut à gauche. L'image sera redimensionnée en 38px par 38px.</small> : </label>
				<input type="file" data-type="image" value="action.php?action=wiki_logo_download" class="form-control-file" id="wiki_logo" name="wiki_logo" data-delete="wiki_logo_delete(this)">
			</div><br>
			<?php echo Configuration::html('wiki'); ?>
	</div>
</div>
