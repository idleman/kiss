<?php
//require_once('header.php');
global $myUser, $myFirm, $conf;
if(!$myUser->can('wiki','read')){
    require_once(__DIR__.SLASH.'page.login.php');
    exit();
}
require_once(__DIR__.SLASH.'WikiCategory.class.php');
require_once(__DIR__.SLASH.'WikiPage.class.php');
WikiCategory::synchronize();
$myUser->loadPreferences();

$menu = array(
    'home' => array(
        'label' => 'Accueil',
        'icon' => 'fas fa-fw fa-home',
        'css' => 'wiki_home_item',
        'onclick' => 'wiki_page_home()'
    ),
    'search' => array(
        'label' => '<span>Recherche</span><input class="form-control hidden" placeholder="Mot clé..." type="text">',
        'icon' => 'fas fa-fw fa-search',
        'css' => 'wiki_search_item',
        'onclick'=> 'wiki_search()'
    )
);
?>
<div id="wiki-summary">
    <h2><i title="Sommaire" class="far fa-list-alt"></i> Sommaire</h2>
    <ul></ul>
</div>

<div id="drag-overlay">
    <div id="overlay-text"><i class="far fa-file-alt"></i>&nbsp;&nbsp;Glissez vos fichiers ici.</div>
    <div id="overlay-icon"><i class="fas fa-cloud-upload-alt"></i></div>
</div>

<div id="sideMenu" class="noPrint">
    <div class="wiki-header">
        <img src="action.php?action=wiki_logo_download" class="wiki-logo">
        <div class="wiki-brand">
            <div class="wiki-brand-firm">
                <?php $firmName = empty($conf->get('wiki_name'))?$myFirm->label:$conf->get('wiki_name'); ?>
                <span class="firm-label" title="<?php echo $firmName; ?>"><?php echo $firmName; ?></span>
                <div class="brand-option dropdown">
                    <i class="fas fa-caret-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu py-1" aria-labelledby="dropdownMenuButton">
                        <h6 class="dropdown-header">Général :</h6>
                        <a class="dropdown-item px-3" href="index.php"><i class="fas fa-fw fa-home"></i> Accueil</a>
                        <!-- <a class="dropdown-item" href="http://no-url.com" target="_blank"><i class="fas fa-book"></i> Documentation</a> -->
                        <!-- <a class="dropdown-item" href="index.php?module=wiki&page=shortcut" target="_blank"><i class="far fa-keyboard"></i> Raccourcis clavier</a> -->
                        <?php if($myUser->can('wiki', 'edit')) : ?>
                        <a class="dropdown-item px-3" href="setting.php?section=global.wiki"><i class="fas fa-fw fa-cogs"></i> Réglages</a>
                        <?php endif; ?>
                        <div class="dropdown-divider my-1"></div>
                        <!-- <a class="dropdown-item px-3" href="action.php?action=logout&url=<?php echo base64_encode('index.php?module=wiki'); ?>"><i class="fas fa-fw fa-sign-out-alt"></i> Déconnexion</a> -->
                        <a class="dropdown-item px-3" onclick="core_logout('<?php echo base64_encode('index.php?module=wiki'); ?>');"><i class="fas fa-fw fa-sign-out-alt"></i> Déconnexion</a>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="wiki-brand-user"><i class="far fa-meh-blank"></i> <?php echo $myUser->fullName(); ?></div>
        </div>
        <div class="night-mode-toggler" title="Activer / Désactiver le mode nuit du Wiki">
            <div class="toggle-box">
                <input <?php if(!empty($myUser->preference('wiki_night_mode'))) echo 'checked="checked"'; ?> type="checkbox" name="-toggler" id="night-mode-check" onchange="toggle_night_mode(this);" />
                <label for="night-mode-check" class="toggle-box-label"></label>
            </div>
        </div>
    </div>

    <div id="wiki-main-menu">
        <ul>
            <?php foreach($menu as $link): ?>
            <li class="<?php echo $link['css']; ?>" onclick="<?php echo $link['onclick']; ?>"><i class="<?php echo $link['icon']; ?>"></i> <?php echo $link['label']; ?> 
            <?php if (isset($link['bubble'])): ?>
                <div class="wiki-bubble"><?php echo $link['bubble']; ?></div>{{/bubble}}
            <?php endif; ?>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>
    
    <div id="wiki-categories">
        <h3>Catégories
            <?php if($myUser->can('wiki', 'edit')) : ?>
            <i class="fas fa-plus wiki-add-category" title="Ajouter une nouvelle catégorie" onclick="wiki_category_edit(this,event);"></i>
            <?php endif; ?>
        </h3>
        <ul id="categories">
            <li data-category="{{slug}}" data-id="{{id}}" title="{{label}}" onclick="wiki_category_open($(this).attr('data-category'));" class="hidden category category-{{state}}">
                <div class="icon-bubble" style="background-color:{{color}};"><i class="{{icon}}"></i></div> {{{label}}}
               
                <div class="category-option dropdown">
                    <i class=" fas fa-ellipsis-h" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <h6 class="dropdown-header">Catégorie :</h6>
                        <?php if($myUser->can('wiki', 'edit')) : ?>
                        <a class="dropdown-item" href="#" onclick="wiki_page_add(this, event);"><i class="far fa-file-alt"></i> Créer une page</a>
                        <a class="dropdown-item" href="#" onclick="wiki_category_edit(this,event);"><i class="fas fa-pencil-alt"></i> Éditer</a>
                        <a class="dropdown-item text-danger" href="#" onclick="wiki_category_delete(this,event);"><i class="far fa-trash-alt"></i> Supprimer</a>
                        <div class="dropdown-divider"></div>
                        <?php endif; ?>
                        <a class="dropdown-item" href="#" onclick="wiki_category_download(this,event);"><i class="far fa-file-archive"></i> Télécharger</a>
                    </div>
                </div>
                <ul></ul>
            </li>
        </ul>
   
        <ul id="pageModel" class="hidden">
            <li data-category="{{categorySlug}}" title="{{label}}" data-page="{{slug}}" data-id="{{id}}" class="page" onclick="wiki_page_open($(this).attr('data-category'),$(this).attr('data-page'),event);"><i class="far fa-sticky-note"></i> {{label}}</li>
        </ul>
        <div class="wiki-preloader"><i class="far fa-meh-blank fa-spin"></i><br>Recherche en cours...</div>
        
        <a class="dropdown-item hidden" href="#" id="upload-button">
            <div><i class="fas fa-plus"></i> Fichier</div>
            <form class="box" method="post" action="action.php?action=wiki_file_upload" enctype="multipart/form-data">
                <input data-label="envoyer" type="file" name="file[]"  multiple />
            </form>
        </a>
    </div>
</div>

<div id="editor"></div>

