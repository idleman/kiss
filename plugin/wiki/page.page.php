<div class="wiki-breadcrumb" data-page="<?php echo $page->id; ?>">
	<span><i style="color:<?php echo $category->color; ?>" class="<?php echo $category->icon; ?>"></i> <?php echo $category->label; ?></span>
	<div class="slash">/</div>
	<div class="form-control" id="page-label"><?php echo $page->label; ?></div>
	<small class="noPrint" title="Modifié par <?php echo $page->updater(); ?>, <?php echo $page->updated(); ?>"> Créé par <i class="far fa-meh-blank"></i> <?php echo $page->author(); ?> <?php echo $page->created(); ?></small>
</div>

<?php if($page->id!=0): ?>
<ul class="page-option noPrint">
	<li class="page-summary-menu">
		<div class="page-option-item page-option-summary noPrint" title="Afficher le sommaire de la page">
			<i onclick="wiki_page_summary(this);" title="Sommaire" class="far fa-list-alt"></i>
		</div>
	</li>
	<li class="page-empty-menu"></li>
	<li class="page-save-menu hidden">
		<div class="page-option-item page-option-save" title="Valider et sauvegarder les changements" onclick="wiki_page_save();">
			<i class="fas fa-check"></i>
		</div>
	</li>
	<li class="page-editor-menu">
		<div class="page-option-item page-option-menu shown dropdown noPrint">
			<i class=" fas fa-ellipsis-h" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<h6 class="dropdown-header">Page :</h6>
				<a class="dropdown-item" title="Éditer la page" href="#" onclick="wiki_page_edit();"><i class="fas fa-pencil-alt"></i> Éditer</a>
				<a class="dropdown-item text-danger" title="Supprimer la page" href="#" onclick="wiki_page_delete(this,event);"><i class="far fa-trash-alt"></i> Supprimer</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#" title="Imprimer la page" onclick="window.print();"><i class="fas fa-print"></i> Imprimer</a>
				<a class="dropdown-item" href="#" title="Télécharger la page" onclick="wiki_page_download(this,event);"><i class="far fa-file-archive"></i> Télécharger</a>
			</div>
		</div>
	</li>
</ul>
<?php endif; ?>

<?php
if($page->id==0): ?>
	<h1><i class="far fa-grin-beam-sweat"></i> Page "<?php echo $page->label; ?>" introuvable</h1>
	<p>Cette page n'existe pas ou n'existe plus, en tout cas on la trouve pas ...<p>
<?php else :
$page->content();
if( $page->content == '' ): ?>
	<div onclick="wiki_page_edit();" class="btn btn-primary float-right"><i class="fas fa-pencil-alt"></i> Éditer la page</div>
	<h1><i class="far fa-sticky-note"></i> <?php echo $page->label; ?></h1>
	<div class="clear"></div>
	<p class="mt-3">Cette page est bien vide...<p>
<?php endif; ?>
<div class="wiki-page-content">
	<div id="content-html">
<?php echo $page->html(); ?>
	</div>
	<textarea class="hidden" id="content-text"><?php echo $page->content; ?></textarea>
</div>

<?php endif; ?>