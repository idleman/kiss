<?php 
require_once __DIR__.SLASH.'WikiCategory.class.php'; 

$category = new WikiCategory() ;
$category->icon = 'far fa-bookmark';
$category->color = WikiCategory::color();
if(isset($_['id'])) $category = WikiCategory::getById($_['id']);

?>
<div onclick="wiki_category_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
<h1>Édition d'une catégorie</h1>
<div class="clear"></div>
<div id="category-form" class="row category-form" data-action="wiki_category_save" data-id="<?php echo $category->id; ?>">
	<div class="col-md-12">
		<label for="label">Titre :</label>
		<div class="input-group">
			<div class="input-group-prepend icon-chooser">
				<input id="icon" class="form-control" data-type="icon" value="<?php echo $category->icon; ?>" type="text">
			</div>
			<input id="label" class="form-control" placeholder="eg. New Category" value="<?php echo $category->label; ?>" type="text">
		
			<input id="color" name="color" class="form-control"  value="<?php echo $category->color; ?>" type="text" data-type="color">
			
		</div>
		<br>
		
	</div>
</div>