<?php
//Déclaration d'un item de menu dans le menu principal
function wiki_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('wiki','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=wiki',
		'label'=>'Wiki',
		'icon'=> 'fab fa-wikipedia-w',
		'color'=> '#e83535'
	);
}

//Cette fonction va generer une page
//quand on clique sur wiki dans menu
function wiki_page(){
	global $_;

	if(!isset($_['module']) || $_['module'] != 'wiki') return;
	$page = !isset($_['page']) ? 'index' : $_['page'];
	$file = __DIR__.SLASH.'page.index.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function wiki_install($id){
	if($id != 'fr.core.wiki') return;
	global $conf;
	Entity::install(__DIR__);
	if(!file_exists(WikiPage::workspace()))	
		File::copy(__DIR__.SLASH.'template', WikiPage::workspace());

	$conf->put('wiki_ext','jpg,jpeg,bmp,gif,png,pdf,docx,xlsx,txt,md,docx,doc,xls,xlsx,ppt,pptx');
	$conf->put('wiki_max_size',10);
}

//Fonction executée lors de la désactivation du plugin
function wiki_uninstall($id){
	if($id != 'fr.core.wiki') return;

	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("wiki",array('label'=>"Gestion des droits sur le plugin wiki"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function wiki_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('wiki','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.wiki',
		'icon' => 'fas fa-angle-right',
		'label' => 'Wiki'
	);
}

//Déclaration des pages de réglages
function wiki_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

function wiki_rewrite($path){
	global $_;
	$parameters = explode('/',$path);

	if(!isset($parameters[0]) || $parameters[0] != 'wiki') return;
	$_['module'] = array_shift($parameters);

	

	require_once(__ROOT__.SLASH.'header.php');
	require_once(__DIR__.SLASH.'page.index.php');
	require_once(__ROOT__.SLASH.'footer.php');
}

function wiki_os_encode($text){
	return (get_OS() === 'WIN') ? utf8_decode($text) : $text;
}

function wiki_os_decode($text){
	return (get_OS() === 'WIN') ? utf8_encode($text) : $text;
}

//Déclaration des settings de base
Configuration::setting('wiki',array(
    "Paramètres généraux",
    'wiki_name' => array("label"=>"Nom du Wiki","legend"=>"Nom affiché en haut à gauche. Par défaut, nom de l'établissement","type"=>"text", "placeholder"=>"eg. The Wiki"),
    'wiki_home' => array("label"=>"Page d'accueil","legend"=>"La page affichée à l'accueil sur le Wiki (format : categorie/page)","type"=>"text", "placeholder"=>'eg. Documentation/Syntaxe'),
    "Paramètres fichiers",
    'wiki_ext' => array("label"=>"Extensions fichiers autorisées","legend"=>"(séparées par virgules)","type"=>"text", "placeholder"=>"eg. jpg, jpeg, png, pdf..."),
    'wiki_max_size' => array("label"=>"Taille maximum autorisée","legend"=>"La taille maximum des fichiers mis en ligne (en Mo)","type"=>"int", "placeholder"=>"eg. 25"),
    'wiki_default_content' => array("label"=>"Contenu par défaut","legend"=>"Contenu affiché à la création d'une page","type"=>"textarea"),
));



//Déclation des assets
global $conf;
if(!$conf->get('offline_mode')) Plugin::addCss("https://fonts.googleapis.com/css?family=Roboto:300,400,700");
Plugin::addCss("/css/main.css"); 
Plugin::addCss("/css/simplemde.min.css"); 
Plugin::addJs("/js/simplemde.min.js"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "wiki_install");
Plugin::addHook("uninstall", "wiki_uninstall"); 


Plugin::addHook("menu_main", "wiki_menu"); 
Plugin::addHook("page", "wiki_page");   
Plugin::addHook("rewrite", "wiki_rewrite");  
Plugin::addHook("menu_setting", "wiki_menu_setting");    
Plugin::addHook("content_setting", "wiki_content_setting");   
 

?>