<?php if($myUser->can('wiki','edit')): ?>
	<div onclick="wiki_page_add(null, event);" class="btn btn-primary float-right"><i class="far fa-file-alt"></i> Ajouter une page</div>
<?php endif; ?>
<h1><i style="color:<?php echo $category->color; ?>" class="<?php echo $category->icon; ?>"></i> <?php echo $category->label; ?></h1>
<div class="clear"></div>

<h3 class="wiki-title">Récemment ajouté</h3>
<?php if(count($pages) == 0 ): ?>
<p class="no-pages">
	La catégorie est vide...<br>
	Ajoutez une nouvelle page en cliquant sur le bouton <i>"Ajouter une page"</i>.
<p>
<?php else:  ?>
<ul class="category-recent">
	<?php foreach($recents as $page): ?>
	<li data-category="<?php echo $page->join('category')->slug; ?>" data-page="<?php echo $page->slug; ?>" onclick="wiki_page_open($(this).attr('data-category'),$(this).attr('data-page'),event);">
		<h5><?php echo $page->label; ?></h5>
		<small class="wiki-small"> Par <?php echo $page->author(); ?> <?php echo $page->created(); ?></small>
		<small class="last-update wiki-small"> - (Modifié par <?php echo $page->updater(); ?> <?php echo $page->updated(); ?>)</small>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>