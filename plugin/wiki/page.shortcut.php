<table class="table">
	<thead>
		<tr>
			<th align="left">Raccourcis</th>
			<th align="left">Action</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td align="left"><em>Cmd-'</em></td>
			<td align="left">toggle Blockquote</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-B</em></td>
			<td align="left">toggle Bold</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-E</em></td>
			<td align="left">clean Block</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-H</em></td>
			<td align="left">toggle Heading Smaller</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-I</em></td>
			<td align="left">toggle Italic</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-K</em></td>
			<td align="left">draw Link</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-L</em></td>
			<td align="left">toggle UnorderedList</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-P</em></td>
			<td align="left">toggle Preview</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-Alt-C</em></td>
			<td align="left">toggle CodeBlock</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-Alt-I</em></td>
			<td align="left">draw Image</td>
		</tr>
		<tr>
			<td align="left"><em>Cmd-Alt-L</em></td>
			<td align="left">toggle Ordered List</td>
		</tr>
		<tr>
			<td align="left"><em>Shift-Cmd-H</em></td>
			<td align="left">toggle Heading Bigger</td>
		</tr>
		<tr>
			<td align="left"><em>F9</em></td>
			<td align="left">toggle Side By Side</td>
		</tr>
		<tr>
			<td align="left"><em>F11</em></td>
			<td align="left">toggle Full Screen</td>
		</tr>
	</tbody>
</table>