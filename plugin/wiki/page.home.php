<?php
global $conf;

require_once(__DIR__.SLASH.'WikiCategory.class.php');
require_once(__DIR__.SLASH.'WikiPage.class.php');
$updated = WikiPage::loadAll(array(),array('updated DESC'),array('10'), array('*'), 1);
?>
<h1><i style="color:<?php echo random_hex_pastel_color('Accueil'); ?>;" class="fas fa-home"></i> Accueil</h1>

<?php if($conf->get('wiki_home') != ''): 
	$homePage = explode('/', $conf->get('wiki_home'));
	if(count($homePage) >= 2):
		list($category,$page) = $homePage;

		$category = WikiCategory::load(array('slug'=>$category));
		if($category && $home = WikiPage::load(array('category'=>$category->id,'slug'=>$page)))
			$home->content();
?>
	<div class="wiki-home-page">
		<?php echo isset($home) && $home ? $home->html() : ''; ?>
	</div>
	<?php endif; ?>
<?php endif; ?>

<?php if(count($updated) == 0): ?>
	<p>Aucune activitée récente enregistrée...<p>
<?php else:  ?>
<div class="wiki-home-activity">
	<h3 class="wiki-title">Récemment Ajouté / Modifié </h3>

	<ul class="category-recent">
	<?php foreach($updated as $page): ?>
		<li data-category="<?php echo $page->join('category')->slug; ?>"  data-page="<?php echo $page->slug; ?>" onclick="wiki_page_open($(this).attr('data-category'),$(this).attr('data-page'),event);">
			<h5><i class="far fa-sticky-note"></i> <?php echo $page->label; ?></h5>
			<small class="wiki-small">
				<span class="font-weight-bold"><i class="<?php echo $page->join('category')->icon; ?>"></i> <?php echo $page->join('category')->label; ?></span>
				<?php if($page->created == $page->updated): ?>
				<span> - Ajouté <?php echo $page->created(); ?> par <i class="far fa-meh-blank"></i> <?php echo $page->author(); ?></span>
				<?php else: ?>
				<span> - Edité <?php echo $page->updated(); ?> par <i class="far fa-meh-blank"></i> <?php echo $page->updater(); ?></span>
				<?php endif; ?>
			</small>
		</li>
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>
<div class="clear"></div>
