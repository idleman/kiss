<?php
require_once(__DIR__.SLASH.'WikiCategory.class.php');
require_once(__DIR__.SLASH.'WikiPage.class.php');

$results = array();
$terms = explode(' ',trim($_['term']));

$rterms = array();
foreach ($terms as $value) {
	if(empty(trim($value)) || strlen($value)<=2) continue;
	$rterms[] = str_replace('/','\/',preg_quote($value));
}


if(count($rterms)>0){
	foreach(WikiPage::loadAll(array(),array('label'),array(), array('*'), 1) as $page){
		$page->content();
		$content = $page->label.' - '.$page->content;
	
		if(preg_match_all('/'.implode('|',$rterms).'/iUs',$content, $matches, PREG_OFFSET_CAPTURE)){
			
			foreach ($matches as $match) {
				foreach ($match as $word) {
					$offset = $word[1];
					$word = $word[0];
					$length = strlen($page->content);
					$start = $offset-50;
					$start = $start < 0 ? 0 : $start;
					$end = $start+100 > $length -1 ? $length -1: 100;

					$excerpt = substr($page->content,$start,$end).'...';
					$excerpt = htmlentities($excerpt);
					$excerpt = preg_replace('|(.*)('.$word.')(.*)|iUs', '$1<span class="wiki-search-highlight">$2</span>$3', $excerpt);

					$results[$page->id] = array(
						'type' => 'page', 
						'item' => $page,
						'excerpt' => $excerpt
					); 
				}
			}
		}
	}

	foreach(WikiCategory::staticQuery('SELECT * FROM {{table}} WHERE label REGEXP ? ',array(implode('|',$rterms)),true) as $category){
		$results[$category->id] = array(
			'type' => 'category', 
			'item' => $category,
			'excerpt' => 'Catégorie'
		);
	}
}
?>
<h3 class="search-title"><i class="fas fa-search"></i> RECHERCHE</h3>
<small class="tags-container <?php if(empty($results)) echo 'light-border-bottom'; ?>">
	<?php if(isset($terms[0]) && !empty($terms[0])): ?>
		<h6 class="d-inline-block user-select-none">Tags : </h6>
		<?php foreach ($terms as $term):
			if(empty($term)) continue;
			$bgColor = random_hex_pastel_color($term);
			$color = get_light($bgColor) >= 0.65 ? '#4c4c4c' : '#fdfdfd';
			?>
			<span class="ml-1 tag-item" style="<?php echo 'background-color:'.$bgColor.';color:'.$color.';'; ?>"><i class="fas fa-tag"></i> <?php echo htmlentities($term); ?> <i class="fas fa-times delete-tag" onclick="wiki_tag_delete(this);"></i></span>
		<?php endforeach; ?>
	<?php endif; ?>
</small>
<?php if(empty($results) && isset($terms[0]) && empty($terms[0])): ?>
	<p class="mt-2"><i class="far fa-sad-tear"></i> Aucun mot-clé renseigné...<p>
<?php elseif(empty($results)): ?>
	<p class="mt-2"><i class="far fa-grin-beam-sweat"></i> Oops ! Rien trouvé..<p>
<?php else: ?>

<h3 class="wiki-title mt-0"><?php echo count($results); ?> Résultats</h3>

<ul class="category-recent">
<?php foreach($results as $result): 
	$item = $result['item'];
	if($result['type'] == 'page'): ?>

	<li data-category="<?php echo $item->join('category')->slug; ?>" data-page="<?php echo $item->slug; ?>" onclick="wiki_page_open($(this).attr('data-category'),$(this).attr('data-page'),event);">
		<h5><i class="far fa-sticky-note"></i> <?php echo $item->label; ?></h5>
		<small class="wiki-small">
			<span class="font-weight-bold"><i class="far fa-bookmark"></i> <?php echo $item->join('category')->label; ?></span>
			<span> - Edité <?php echo $item->created(); ?> par <i class="far fa-meh-blank"></i> <?php echo $item->author(); ?></span>
		</small>
		<br>
		<small>...<?php echo $result['excerpt']; ?></small>
	</li>

	<?php else: ?>

	<li data-category="<?php echo $item->slug; ?>" onclick="wiki_category_open($(this).attr('data-category'),$(this).attr('data-page'),event);">
		<h5><i class="far fa-bookmark"></i> <?php echo $item->label; ?></h5>
		<small class="wiki-small">
			<span class="font-weight-bold"><i class="far fa-bookmark"></i> <?php echo $item->label; ?></span>
			<span> - Edité <?php echo $item->created(); ?> par <i class="far fa-meh-blank"></i> <?php echo $item->author(); ?></span>
		</small>
		<br>
	</li>

	<?php endif; ?>
<?php endforeach; ?>
</ul>

<?php endif; ?>