<?php
/**
 * Define a category.
 * @author Valentin CARRUESCO
 * @category Plugin
 * @license MIT
 */
require_once(__DIR__.SLASH.'WikiPage.class.php');

class WikiCategory extends Entity{
	public $id,$label,$icon,$color,$state,$path,$slug,$sort=0;
	protected $TABLE_NAME = 'wiki_category';
	public $fields =
	array(
		'id' => 'key',
		'label' => 'string',
		'slug' => 'string',
		'icon' => 'string',
		'color' => 'string',
		'state' => 'string',
		'sort' => 'int',
		'path' => 'string'
	);
	const DEFAULT_CATEGORY = 'Non classé';

	public function __construct(){
		$this->color = self::color();
		parent::__construct();
	}

	public static function synchronize(){
		$workspace = WikiPage::workspace();
		$defaultCategory = $workspace.SLASH.wiki_os_encode(self::DEFAULT_CATEGORY);
		if(!file_exists($defaultCategory)) mkdir($defaultCategory,0755,true);

		$existing =  array();

		//MAJ de la base pour les dossiers supprimés
		foreach(self::loadAll() as $category){
			if(!file_exists($workspace.SLASH.wiki_os_encode($category->path)))
				self::deleteById($category->id);
			$existing[$category->path] = $category;
		}

		//MAJ de la base pour les nouveaux dossiers
		foreach(glob($workspace.SLASH.'*') as $i=>$folder){
			$path = str_replace($workspace.SLASH,'',wiki_os_decode($folder));
			if(isset($existing[$path])) continue;
			if(is_dir($folder)){
				$category = new self();
				$category->label = wiki_os_decode(basename($folder));
				$category->slug = slugify($category->label);
				$category->path = $path;
				$category->sort = $i;
				$category->icon = 'far fa-bookmark';
				$category->save();
			} else {
				rename($folder, $defaultCategory.SLASH.basename($folder));
			}
		}
	}

	public function pages(){
		$pages = array();
		$workspace = WikiPage::workspace();

		$existing =  array();
		foreach(WikiPage::loadAll(array('category'=>$this->id)) as $page){
			if(!file_exists($workspace.SLASH.wiki_os_encode($page->path))){
				WikiPage::deleteById($page->id);
				continue;
			}
			$existing[$page->path] = $page;
		}
		
		foreach(glob($workspace.SLASH.wiki_os_encode($this->path).SLASH.'*.md') as $i=>$file){
			$path = str_replace($workspace.SLASH,'',wiki_os_decode($file));
			if(!is_file($file) || isset($existing[$path]) || !file_exists($file)) continue;
			
			$page = new WikiPage();
			$page->label = preg_replace('|\.md|i','',wiki_os_decode(basename($file)));
			$page->slug = slugify($page->label);
			$page->path = $path;
			$page->content = file_get_contents(wiki_os_decode($file));
			$page->category = $this->id;
			$page->sort = $i;
			$page->state = WikiPage::PUBLISHED;
			$page->save();
		}

		$pages =  WikiPage::loadAll(array('category'=>$this->id),array('sort','label'));
		return $pages;
	}

	public static function deleteById($id){
		$category = self::getById($id);
		parent::deleteById($id);
		$workspace = WikiPage::workspace();
		$path = $workspace.SLASH.wiki_os_encode($category->path);
		if(file_exists($path) && is_dir($path)) self::remove_directory($path);
	}

	public static function remove_directory($dir){
		if(!is_dir($dir)) return;
		foreach (glob($dir.SLASH.'*') as $file) { 
			if(is_dir($file)){
				self::remove_directory($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dir);
	}

	public static function color(){
		$colors = array(
			'#4E5C6E',
			'#19B7FF',
			'#19B7FF',
			'#7F6BFF',
			'#FC7419',
			'#FC2D2D',
			'#FFE100',
			'#14CF9F',
			'#EE84F0',
			'#2F362F'
		);
		return $colors[array_rand($colors)];
	}

	public function author(){
		return User::byLogin($this->creator)->fullName();
	}
	public function created(){
		return relative_time($this->created);
	}
	
	public function updater(){
		return User::byLogin($this->updater)->fullName();
	}
	public function updated(){
		return relative_time($this->updated);
	}
}
?>