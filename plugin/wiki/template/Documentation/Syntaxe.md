## Syntaxe


# Titre 1
## Titre 2
### Titre 3
#### Titre 4
##### Titre 5
###### Titre 6

### Table


header 1 | header 2
-------- | --------
cell 1.1 | cell 1.2
cell 2.1 | cell 2.2

### Liens

Lien automatique : <http://example.com>
Un email : <me@example.com>

[link](http://example.com)

([link](/index.php)) in parentheses

[`link`](http://example.com)

### Code & citations

    <?php

    $message = 'Hello World!';
    echo $message;

---

    > not a quote
    - not a list item
    [not a reference]: http://foo.com

Un `example en surbrillance`


```
<?php

$message = 'fenced code block';
echo $message;
```

~~~
tilde
~~~

```php
echo 'language identifier';
```

> Une citation

### Format de texte
**Du gras**, _de l'italique_, ~~du barré~~

- un paragraphe

### listes

Liste non numérotée : 
- Un
    - sous liste 1
    - sous liste 2
- deux

Liste  numérotée : 
1. Un
2. deux

### Echappements

Pour échapper un symbole markdown : \*non gras\*.

### Média
![Markdown Logo][image]

[image]: /md.png
