<?php
global $myUser,$conf;
User::check_access('import','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	
        <h3 class="mb-0"><i class="fas fa-file-import"></i> Réglages Import <div onclick="import_setting_save();" class="btn btn-sm btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div></h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('import'); ?>
    </div>
</div>
