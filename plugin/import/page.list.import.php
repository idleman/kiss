<?php
global $myUser;
User::check_access('import','read');
require_once(__DIR__.SLASH.'Import.class.php');


?>
<div class="plugin-import" data-mode="<?php echo $myUser->can('import','configure') ? 'configure':'execute' ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des imports</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="import_search(null,true);" id="export-imports-btn" class="btn btn-info rounded-0 btn-squarred ml-1 btn-export" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                <div class="my-auto ml-auto mr-0 noPrint configure-only">
                    <a href="index.php?module=import&page=sheet.import" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                </div>
                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
            <select id="import-filters" data-type="filter" data-label="Recherche" data-function="import_search">
                <option value="main.label" data-filter-type="text">Libellé</option>
                <?php if($myUser->can('import','configure') ): ?><option value="main.entity" data-filter-type="text">Entitée</option><?php endif;?>
                <option value="main.hasheader" data-filter-type="boolean">En-tête présentes</option>
            </select>
        </div>
        
    </div>
    <h5 class="results-count my-2"><span></span> Résultat(s)
        <!-- bloc de preference de pagination -->
        <small class="text-muted right text-muted text-small"><div class="d-inline-block mr-1" data-type="pagination-preference" data-table="#imports" data-value="20" data-max-item="100"></div></small><div class="clear"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">

            

    		
            <!-- présentation tableau -->
            <table id="imports" class="table table-striped " data-entity-search="import_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->
                        <th data-sortable="label">Libellé</th>
                        <th class="configure-only" data-sortable="entity">Entitée</th>
                         <!--<th data-sortable="startLine">Ligne de départ</th>
                        <th data-sortable="startColumn">Colonne de départ</th>-->
                        <th data-sortable="hasheader">Nom de colonnes présentes</th>
                        <!--<th data-sortable="file">Excel</th>
                         <th data-sortable="beforeCode">Code executé avant import</th>
                        <th data-sortable="afterCode">Code exécuté après import</th>-->
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	                <!--<td class="align-middle">{{id}}</td>-->
    	                <td class="align-middle">{{label}}</td>
    	                <td class="configure-only" class="align-middle">{{entity}}</td>
    	                <!--<td class="align-middle">{{startLine}}</td>
    	                <td class="align-middle">{{startColumn}}</td>-->
    	                <td class="align-middle">{{#hasheader}} <i class="fas fa-check text-success"></i> OUI{{/hasheader}}{{^hasheader}}<i class="fas fa-times text-danger"></i> Non{{/hasheader}}</td>
    	                 <!-- <td class="align-middle">
                            <ul class="list-group list-group-flush shadow-sm">
                                {{#file}}
                                <li class="list-group-item p-1"><a href="{{url}}">{{label}}</a></li>
                                {{/file}}
                            </ul></td>
    	                <td class="align-middle">{{beforeCode}}</td>
    	                <td class="align-middle">{{afterCode}}</td>-->
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                <a class="btn text-info configure-only" title="Éditer import" href="index.php?module=import&page=sheet.import&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <a class="btn text-success execute-only" title="Utiliser import" href="index.php?module=import&page=sheet.import&id={{id}}"><i class="fas fa-file-import"></i></a>
                                <div class="btn text-danger configure-only" title="Supprimer import" onclick="import_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
            

            <br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');import_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>