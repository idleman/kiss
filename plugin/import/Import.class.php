<?php
/**
 * Define a Import
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class Import extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $entity; //Entitée (Texte)
	public $startLine; //Ligne de départ (Nombre Entier)
	public $headerLine; //Ligne d'en-tête (Nombre Entier)
	public $hasheader; //Nom de colonnes présentes (Vrai ou Faux)
	public $file; //Excel (Fichier)
	public $meta; //Meta (Texte long)

	
	protected $TABLE_NAME = 'import';
	public $entityLabel = 'Import';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text', 'label' => 'Libellé'),
		'entity' => array('type'=>'text', 'label' => 'Entitée'),
		'startLine' => array('type'=>'integer', 'label' => 'Ligne de départ'),
		'headerLine' => array('type'=>'integer', 'label' => 'Ligne d\'en-tête'),
		'hasheader' => array('type'=>'boolean', 'label' => 'En-tête présente'),
		'meta' => array('type'=>'textarea', 'label' => 'Infos contextuelles')
	);

	public $links = array(
	);

	//liste des Macro possibles possibles
	public static function macros($key=null){

		$macros = array();
		Plugin::callHook('import_macros',array(&$macros));

		if(!isset($key)) return $macros;
		return isset($macros[$key]) ? $macros[$key] : false;
	}

	static function remove($id){
		require_once(__DIR__.SLASH.'ImportMapping.class.php');
		Import::delete(array('id'=>$id));
		ImportMapping::delete(array('import'=>$id));
		$excel = File::dir().'import'.SLASH.$id;
		if(file_exists($excel )) delete_folder_tree($excel,true);
	}

	public static function templates($extension = null){
		$templates = array();
		foreach(glob(__DIR__.SLASH.'template'.SLASH.'*.class.php') as $file){
			require_once($file);
			$class = str_replace('.class.php','',basename($file));
			$instance = new $class();
			$templates[$instance->extension] = $instance;
		}
		if(!isset($extension)) return $templates;
		return isset($templates[$extension]) ? $templates[$extension] : false;
	}

	//Colonnes indexées
	public $indexes = array();
	
}
?>