<?php 
User::check_access('import','read');
require_once(__DIR__.SLASH.'Import.class.php');
require_once(__DIR__.SLASH.'ImportMapping.class.php');

global $myUser;

$import = Import::provide();
if($import->id==0){
	$import->label = 'Sans titre';
	$import->startLine = 2;
	$import->headerLine = 1;
	$import->hasheader = 1;
}

$allowedExtensions = array();
foreach(Import::templates() as $template)
	$allowedExtensions[] = $template->extension;



?>
<div class="plugin-import" data-mode="<?php echo $myUser->can('import','configure') ? 'configure':'execute' ?>">
	<div id="import-form" class="row justify-content-md-center import-form" data-action="import_save" data-id="<?php echo $import->id; ?>">
		<div class="col-md-8">
			
			<div class="row">
				<div class="col-md-12 shadow-sm bg-white p-3">
					<h3>Import 
						
						<a href="index.php" class="btn btn-small btn-dark right  mr-2">Retour</a>
						<div class="btn btn-small btn-info mb-2 btn-dark mx-2 right" data-scope="import" data-uid="<?php echo $import->id; ?>" data-show-important="true" data-type="history" data-tooltip title="Ouvrir l'historique"> <i class="far fa-comment-dots"></i></div>
					</h3>
					<label for="label">Libellé</label>
					<input  value="<?php echo $import->label; ?>" <?php echo $myUser->can('import','configure') ? '':'readonly' ?> class="form-control"  type="text" onchange="import_save()"  id="label" >
					
						<div class="configure-only mt-2">
							<label for="entity">Donnée ERP à renseigner</label>
							<input  value="<?php echo $import->entity; ?>" onchange="import_save()" data-type="entitypicker" class="form-control"  type="text"  id="entity" >
						</div>

						<div class="update-only my-2 file-container">
							<label for="file">Fichier Excel</label>
							<small class="text-muted configure-only">Fichier excel d'exemple  à importer (au moins une ligne de données (fichiers autorisés: <?php echo implode(',',$allowedExtensions); ?>))</small>
							<small class="text-muted execute-only">Fichier excel à importer (fichiers autorisés: <?php echo implode(',',$allowedExtensions); ?>)</small>
							<input  class="component-file-default bg-white shadow-sm rounded-sm"  type="text" data-label="Faites glisser votre fichier excel ici" data-type="file"  data-extension="xlsx,xls"  data-action="import_file" data-limit="1"  data-id="file"  data-data='{"scope":"import","slug":"import"}'  id="file" >
						</div>

						<div class="configure-only">
							<label for="hasheader"><input <?php echo $import->hasheader == 1 ? 'checked="checked"' : '' ?> onchange="import_save()"  data-type="checkbox" type="checkbox" id="hasheader"> Mon fichier comporte une en-tête</label>
							
							<div id="template-settings"></div>
						

						
						
							<label for="headerLine">N° de ligne d'en-tête (si existante)</label>
							<small class="text-muted">Optionnel (defaut: 1)</small>
							<input onchange="import_save()" value="<?php echo $import->headerLine; ?>" class="form-control"  type="number"  id="headerLine" >

							<label for="startLine">N° de ligne de départ de données</label>
							<small class="text-muted">Optionnel (defaut: 2)</small>
							<input onchange="import_save()" value="<?php echo $import->startLine; ?>" class="form-control"  type="number"  id="startLine" >
							<hr/>
						</div>
						

					</div>
				</div>

				<div class="row mt-3 hasfile-only configure-only">
					<div class="col-md-12 shadow-sm bg-white">
						<h4 class="my-3"><i class="far fa-file-excel text-success"></i> EXCEL <i class="fas fa-long-arrow-alt-right"></i> <i class="fas fa-database text-warning"></i> BASE </h4>

						<div class="btn btn-dark w-100 btn-import-deduction my-2" onclick="import_mapping_deduction()"><i class="fas fa-search-plus"></i> Déduire les champs</div>

						 <!-- présentation tableau -->
			            <table id="import-mappings" class="table table-striped " data-entity-search="import_mapping_search">
			                <thead>
			                    <tr>
			                        <th data-sortable="excelColumn">Colonne Excel</th>
			                        <th data-sortable="modifier">Macro de modification</th>
			                        <th data-sortable="entityField">Colonne ERP</th>
			                        <th data-sortable="uniticy">Si valeur non unique</th>
			                        <th><div class="btn btn-sm text-danger right" data-tooltip title="Supprimer toutes les lignes" onclick="import_mapping_delete('all');"><i class="far fa-trash-alt"></i></div></th>
			                    </tr>
			                </thead>
			               
			                <tbody>
			                    <tr data-id="{{id}}" class="hidden item-line">
			    	                <td class="align-middle import-label-column">
			    	                	<input type="text" data-id="excelColumn" onchange="import_mapping_save(this)" class="border-0 d-inline-block bg-transparent excelColumn"  value="{{excelColumn}}">
			    	                	{{#excelColumnName}} <small class="text-muted">- {{excelColumnName}}</small>{{/excelColumnName}}

			    	                	<label><input type="checkbox" class="fixedValueCheckbox" data-type="checkbox" onclick="import_mapping_fixed_change(this);"> Valeur fixe</label>
			    	                	<input type="text" data-id="excelColumnFixedValue"  onchange="import_mapping_save(this)" class="form-control hidden excelColumnFixedValue">
			    	                </td>
			    	                <td class="align-middle"><select class="form-control" onchange="import_mapping_save(this)" data-id="modifier">
			                        	<option value="">-</option>
			                        	<?php foreach(Import::macros() as $key=>$macro): ?>
			                        		<option value="<?php echo $key; ?>"><?php echo $macro['label']; ?></option>
			                        	<?php endforeach; ?>
			                        </select></td>
			    	                <td class="align-middle">
			    	                	<select class="form-control entityField" onchange="import_entity_field_change(this)" data-id="entityField"></select>
			    	                	<select class="form-control entitySubField mt-1 hidden" onchange="import_mapping_save(this)" data-id="entitySubField"></select>
			    	                	<input type="text" data-tpl-type="entitypicker" data-id="entityRelated" onchange="import_mapping_save(this)" class="entityRelated hidden form-control input-small mt-1">
			    	                	<input type="text" data-tpl-type="checkbox-list" data-depth="2" onchange="import_mapping_save(this)" data-multi-level-select="true" data-id="entityRelatedField" class="entityRelatedField hidden form-control input-small mt-1">

			    	                	
			    	                </td>
			    	                <td>
			    	                	<select class="form-control unique mt-1 " onchange="import_mapping_save(this)" data-id="unique">
			    	                		<?php foreach(ImportMapping::uniqueness() as $key=>$unique): ?>
			    	                			<option value="<?php echo $key; ?>"><?php echo $unique['label']; ?></option>
			    	                		<?php endforeach; ?>
			    	                	</select>
			    	                </td>
			    	                <td class="align-middle text-right">
			                            <div class="btn-group btn-group-sm" role="group">
			                                <div class="btn text-danger" data-tooltip title="Supprimer cette ligne" onclick="import_mapping_delete(this);"><i class="far fa-trash-alt"></i></div>
			                            </div>
			    	                </td>
			                    </tr>
			               </tbody>
			                
			                <thead>
			                    <tr id="import-mapping-form" data-action="import_mapping_save" data-id="">
			                      <th colspan="5" class="text-center"><div onclick="import_mapping_add();" class="btn btn-success"><i class="fas fa-plus"></i></div></th>
			                    </tr>
			                </thead>
			              
			                
			            </table>



					</div>
					
					<br/>
			
				</div>

				<div class="row mt-3 update-only">
					<div class="col-md-12 shadow-sm bg-white p-3">
						<div class="btn btn-primary btn-import w-100" onclick="import_execute()"><i class="fas fa-file-import"></i> Importer</div>
					</div>
				</div>

				<div class="row mt-3 update-only import-excel-table-container hidden">
					<div class="col-md-12 p-3">
						<h3>Résultat d'import</h3>
						
						<ul class="import-stats-list">
							<template>
								<li class="shadow-sm" data-slug="{{slug}}"><i class="fas fa-times"></i> <span>{{value}}</span> {{label}}</li>
							</template>
						</ul>

						<table class="import-excel-table hidden shadow-sm bg-white mt-2">
							<thead class="title-head">
								<tr>
									<th colspan="4">Fichier.xlsx</th>
								</tr>
							</thead>
							<thead class="column-head">
								<tr>
									<th><img src="plugin/import/img/triangle.png"></th>
									<template>
									<th>{{.}}</th>
									</template>
								</tr>
							</thead>
							<tbody>
								<template>
									<tr class="state-{{state}}">
										<th>{{lineNumber}}</th>
									</tr>
								</template>
								<template>
								
									<td class="state-{{state}}">{{value}} {{#state}}<br><strong>{{stateLabel}}</strong>{{/state}}{{#message}}: {{message}}{{/message}}</td>
										
								</template>
							</tbody>
						</table>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>

