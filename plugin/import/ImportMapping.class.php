<?php
/**
 * Define a Line d\'import mappée
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class ImportMapping extends Entity{

	public $id;
	public $excelColumn; //Colonne Excel (Texte)
	public $excelColumnFixedValue; //Colonne Value Excel fixe (Texte)
	public $excelColumnName; //Nom de Colonne Excel (Texte)
	public $modifier; //Code de modification (Texte Long)
	public $entityField; //Champ entitée (Texte)
	public $entitySubField; //Sous Champ entitée (Texte)
	public $entityRelated; //Entitée reliée (Texte Long)
	public $entityRelatedField; //Sous Champ entitée reliée (Texte)
	public $unique; //Unicité (Liste classique)

	public $import; //Import lié (int)
	
	public const UNIQUE_SKIP = 'skip';
	public const UNIQUE_ERASE = 'erase';

	protected $TABLE_NAME = 'import_mapping';
	public $entityLabel = 'Line d\'import mappée';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'excelColumn' => array('type'=>'text', 'label' => 'Numero de Colonne Excel'),
		'excelColumnFixedValue' => array('type'=>'text', 'label' => 'Colonne Value Excel fixe'),
		'excelColumnName' => array('type'=>'text', 'label' => 'Nom de Colonne Excel'),
		'modifier' => array('type'=>'textarea', 'label' => 'Code de modification'),
		'entityField' => array('type'=>'text', 'label' => 'Champ entitée'),
		'entitySubField' => array('type'=>'text', 'label' => 'Sous Champ entitée'),
		'entityRelated' => array('type'=>'text', 'label' => 'Entitée reliée'),
		'entityRelatedField' => array('type'=>'text', 'label' => 'Sous Champ entitée reliée'),
		'import' => array('type'=>'int', 'label' => 'Import lié','link'=>'plugin/import/Import.class.php'),
		'unique' => array('type'=>'list','label' => 'Unicité')
	);


	//Colonnes indexées
	public $indexes = array();

	//liste des Unicité possibles
	public static function uniqueness($key=null){
		$items = array(
			'' => array('label'=>'Importer quand même'),
			self::UNIQUE_SKIP => array('label'=>'Ligne excel ignorée'),
			self::UNIQUE_ERASE => array('label'=>'Ligne en base écrasée')
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non définis');
	}
	
}
?>