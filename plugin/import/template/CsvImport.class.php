<?php

class CsvImport{
	public $mime = 'application/csv';
	public $extension = 'csv';
	public $description = 'Fichier tableur générique';
	public $meta = array();
	/* convertis le fichier en tableau lignes / colonnes
    
	Le tableau final doit avoir la forme : 
	array(   <-- excel
		array(  <-- ligne
			array(  <-- cellule
				'value' => '' <-- valeur cellule
				'color' => '' <-- propriété optionelle "couleur" (ou autre)
			)
		)
	)
    */
	public  function toArray($file){
		$stream = file_get_contents($file);
		$stream = utf8_encode($stream);
		
		//met a plat les lignes comportant un saut de ligne car entre ""
		$stream = preg_replace_callback('/"(.*)"/Uis', function($match){
			return str_replace(array("\n","\r"),'<br>',$match[1]);
		}, $stream);

		
		$finalArray = array();
		$emptyLines = 0;

		foreach(explode(PHP_EOL,$stream) as $line){

			//si + de 10 lignes vides on arrette de lire (evite les excels sans fin)
			if($this->emptyLine($line,true)) $emptyLines++;
			if($emptyLines>10) break;

			$finalLine = array();
			foreach(explode(';',$line) as $cell) $finalLine[]= array('value'=>$cell);
			
			$finalArray[] = $finalLine;
		}
		
		return $finalArray;
	}

	//vérifie si une ligne est a vide ou non
	public function emptyLine($line,$raw = false){

		if($raw) return str_replace(array(';',' ',"\t","\n","\r"),'',$line) == '';
		$oneFilled = false;
		foreach($line as $i=>$cell){
			if( $this->emptyCell($cell)  ) continue;
			
			$oneFilled = true;
			break;
		}
		return !$oneFilled;
	}

	//vérifie si une cellule est a vide ou non
	public function emptyCell($cell){
		return str_replace(array(';',' ',"\t","\n","\r"),'',$cell['value']) == '';
	}

	//Déduction du type d'une celleule a partir de sa valeur
	public function cellType($headerCell,$valueCell){
		$columnName = isset($headerCell['label']) ? $headerCell['label'] : $headerCell['value'];

		$value = $valueCell['value'];
		$deductedType = 'text';
		
		if(strlen($value)>225) $deductedType = 'textarea';
		if(is_int($value)) $deductedType = 'number';
		if(is_float($value) || is_double($value)) $deductedType = 'decimal';
		if(	   strtolower($value) =='oui' 
			|| strtolower($value) =='non' 
			|| strtolower($value) =='o' 
			|| strtolower($value) =='x' 
			|| strtolower($value) =='n'
			|| stripos($columnName, 'VRAI/FAUX') !== false
			|| stripos($columnName, 'OUI/NON') !== false
			|| stripos($columnName, 'O/N') !== false
		) $deductedType = 'boolean';

		if(strpos($value,'€') !== false) $deductedType = 'price';

		if(stripos($columnName, 'prix ')===0 || stripos($columnName, 'montant ')===0 || stripos($columnName, 'tarif ')===0 )  $deductedType = 'price';
		if(stripos($columnName, 'date ')===0)  $deductedType = 'date';
		if(stripos($columnName, 'catégorie ')===0 || stripos($columnName, 'categorie ')===0 )  $deductedType = 'dictionary';
		if(stripos($columnName, 'type ')===0 )  $deductedType = 'dictionary';
		

		return $deductedType;
	}

	//Réglages spécifiques a ce modèle d'import
	public function settings(){
		return array();
	}

}
?>