<?php
require_once(__DIR__.SLASH.'CsvImport.class.php');

class XlsxImport extends CsvImport{
	public $mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
	public $extension = 'xlsx';
	public $description = 'Fichier tableur excel enrichis';
	public $meta = array();
	/* convertis le fichier en tableau lignes / colonnes
    
	Le tableau final doit avoir la forme : 
	array(   <-- excel
		array(  <-- ligne
			array(  <-- cellule
				'value' => '' <-- valeur cellule
				'color' => '' <-- propriété optionelle "couleur" (ou autre)
			)
		)
	)
    */
	public  function toArray($file){
		

		//on prend l'onglet 1 si aucun onglet configuré
		$sheetIndex = isset($this->meta) && isset($this->meta['worksheet']) && is_numeric($this->meta['worksheet']) ? $this->meta['worksheet']-1 : 0;
		$sheetIndex = $sheetIndex<0 ? 0 : $sheetIndex;

        $rows = Excel::parse($file,false,$sheetIndex,false); 
        

        $emptyLines = 0;
        $finalArray = array();
        if(!is_array($rows)) $rows = array();

        
		foreach($rows as $line){
			//si + de 10 lignes vides on arrette de lire (evite les excels sans fin)
			if($this->emptyLine($line,true)) $emptyLines++;
			if($emptyLines>10) break;

			$finalLine = array();

			foreach($line as $cell){
				$cell = str_replace("\n","<br>",$cell);
				$finalLine[]= array('value'=>$cell);
			}
			$finalArray[] = $finalLine;
		}
		
		return $finalArray;
	}

	//vérifie si une ligne est a vide ou non
	public function emptyLine($line,$raw = false){

		if($raw) return str_replace(array(';',' ',"\t","\n","\r"),'',$line) == '';
		$oneFilled = false;
		foreach($line as $cell){
			if( $this->emptyCell($cell)  ) continue;

			$oneFilled = true;
			break;
		}
		//if($oneFilled){
			
			//foreach($line as $cell){
			//	var_dump($cell['value'],$this->emptyCell($cell));
			//}
		//}
		return !$oneFilled;
	}

	//vérifie si une cellule est a vide ou non
	public function emptyCell($cell){
		$val = str_replace(array(';',' ',"\t","\n","\r"),'',$cell['value']);
		//1900-01-18 00:00:00 compoense le bug de SimpleXLSX qui detecte 1900-01-18 00:00:00 sur une cellule vide de type date
		return $val == '' || $val =='1970-01-01 00:00:00';
	}

	//Déduction du type d'une celleule a partir de sa valeur
	public function cellType($headerCell,$valueCell){
		return parent::cellType($headerCell,$valueCell);
	}

	//Réglages spécifiques a ce modèle d'import
	public function settings(){
		
		$fields = array();
		$fields = array(
			'worksheet' => array(
				'type' => 'integer',
				'legend' => 'Optionnel (defaut: 1)',
				'label' => 'Numéro de l\'onglet a importer'
			)
		);
		return $fields;
	}

}
?>