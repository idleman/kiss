<?php
global $myUser,$conf;
User::check_access('stripe','configure');
?>

<div class="row">
	<div class="col-md-12"><br>
		<?php if($myUser->can('stripe', 'edit')) : ?>
			<div onclick="stripe_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
		<?php endif; ?>
		<h3>Réglages Stripe</h3>
		<div class="clear"></div>
		<hr>
	</div>
</div>
<div class="row">
	<!-- search results -->
	<div class="col-xl-12 settings-stripe">
		<?php echo Configuration::html('stripe'); ?>
		<p>Pour obtenir les code publics et privés, <a href="https://dashboard.stripe.com/register" target="_blank">créez un compte Stripe</a> puis allez dans la section <a href="https://dashboard.stripe.com/account/apikeys" target="_blank">Developpers &gt; API Keys</a>.</p>
		<p>Lorsque vous êtes en mode sandbox, vous pouvez tester avec la carte virtuelle :<br/>
			<ul>
				<li>N° Carte: <strong>4242424242424242</strong></li>
				<li>CVV: <strong>123</strong></li>
				<li>mois:<strong>8</strong></li>
				<li>année: <strong>2019</strong></li>
			</ul>
			Ou via <a href="https://stripe.com/docs/testing#cards" target="_blank">les autres cartes</a> proposées par stripe.
		</p>
		<hr>
		<h3>Intégrer le plugin à un formulaire</h3>
		<h5>Coté client :</h4>
		<pre>
			<code class="">stripe_card({
	key : 'my public key...',
	card: '4242424242424242',
	cvv: '123',
	month:8,
	year: 2019
},function(){

},function(response){
	if(response.error) return $.message('error',response.error.code+' : '+response.error.message);
	$.action({
		action : 'example_stripe_pay',
		token : response.id
	},function(r){
		$.message('info','Paiemment effectué');
	});
});</code>
		</pre>

		<h5>Coté Action :</h4>
		<pre>
			<code class="">//paye la somme de 20 €
stripe_payment($_['token'],20,'Description paiement','Description acheteur');</code>
		</pre>
	</div>
</div>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/monokai-sublime.min.css">

<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/languages/php.min.js"></script>