<?php

	
	Action::register('stripe_setting_save',function(&$response){
	
			global $myUser,$_,$conf;
			User::check_access('stripe','configure');
			foreach(Configuration::setting('stripe') as $key=>$value){
				if(!is_array($value)) continue;
				$allowed[] = $key;
			}
			foreach ($_['fields'] as $key => $value) {
				if(in_array($key, $allowed))
					$conf->put($key,$value);
			}
		
	});

	Action::register('stripe_init_component',function(&$response){
	
			try {
				global $myUser,$myFirm,$_;
				User::check_access('stripe','read');

				if(!isset($_['process']) || empty($_['process'])) throw new Exception("Aucune action pour procéder au paiement");
				$process = trim($_['process']);
				
				ob_start();
				require_once(__DIR__.SLASH.'stripe.component.php');
				$stream = ob_get_clean();
				$response['content'] = $stream;
			} catch (Exception $e) {
				$response['content'] = '<div class="w-100 text-danger text-center"><small>ERREUR AU CHARGEMENT<br>'.$e->getMessage().' ('.$e->getCode().')</small></div>';
			}
		
	});

?>