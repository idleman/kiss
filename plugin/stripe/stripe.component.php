<div class="col1 col-md-8">
	<div class="card">
		<div class="front">
			<div class="type">
				<i class="bankid"></i>
			</div>
			<span class="chip"></span>
			<span class="card_number">&#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; </span>
			<div class="date"><span class="date_value">MM / YYYY</span></div>
			<span class="fullname">NOM</span>
		</div>
		<div class="back">
			<div class="magnetic"></div>
			<div class="bar"></div>
			<span class="seccode">&#x25CF;&#x25CF;&#x25CF;</span>
			<span class="chip"></span><span class="disclaimer">This card is property of Random Bank of Random corporation. <br> If found please return to Random Bank of Random corporation - 21968 Paris, Verdi Street, 34 </span>
		</div>
	</div>
</div>
<div class="col2 col-md-4">
	<div class="row">
		<div class="col-md-12">
			<label>N° Carte :</label>
			<input type="text" id="stripe-card-number" class="number" maxlength="19" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
		</div>
		<div class="col-md-12">
			<label>Nom :</label>
			<input type="text" id="stripe-card-owner" class="inputname" placeholder="NOM Prénom"/>
		</div>
		<div class="col-md-6">
			<label>Expire le :</label>
			<input type="text" id="stripe-card-expire" class="expire" placeholder="MM / YYYY"/>
		</div>
		<div class="col-md-6">
			<label>N° CVV :</label>
			<input type="text" id="stripe-card-cvv" class="cvv" title="Le numero de CVV est le numéro à 3 chiffre à l'arrière de votre carte bleue" placeholder="CVV" maxlength="3" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
		</div>
		<div id="shop-stripe-button" class="col-md-12 buy-button" onclick="stripe_process_payment('<?php echo stripe_public_key(); ?>', '<?php echo $process; ?>');">
			<span class="button-loading hidden"><i class="fas fa-cog fa-spin pink"></i> Paiement en cours....</span>
			<span class="button-active"><i class="far fa-credit-card"></i> Payer</span>
		</div>
	</div>
</div>
