<?php

//Fonction executée lors de l'activation du plugin
function stripe_install($id){
	if($id != 'fr.core.stripe') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function stripe_uninstall($id){
	if($id != 'fr.core.stripe') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('stripe',array('label'=>'Gestion des droits sur le plugin stripe'));


//Cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html

require_once(__DIR__.SLASH.'action.php');

//Déclaration du menu de réglages
function stripe_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('stripe','configure')) return;
		$settingMenu[]= array(
			'sort' =>1,
			'url' => 'setting.php?section=global.stripe',
			'icon' => 'fas fa-angle-right',
			'label' => 'Paiement Stripe'
		);
	
}

//Récupère la clé publique du
//compte Stripe (sandbox ou prod)
function stripe_public_key(){
	global $conf;
	return $conf->get('stripe_mode') == 'production' ? $conf->get('stripe_public_production') : $conf->get('stripe_public_sandbox');
}

//Permet de réaliser un paiement
//en lien avec les informations récupérées
function stripe_payment($token,$price,$description=PROGRAM_NAME,$customerDescription=null){
	global $conf;
	require_once(__DIR__.SLASH.'Stripe.class.php');
	
	$key = $conf->get('stripe_mode') == 'production' ? $conf->get('stripe_private_production') :  $conf->get('stripe_private_sandbox');

	$stripe = new Stripe($key);
	$stripe->currency = $conf->get('stripe_currency');

	$result = $stripe->pay($token,$price,$description,$customerDescription);

	$response = array(
		'status' => $result['status'],
		'price' => $result['amount']/100,
		'paid' => $result['paid'],
		'payment' => $result['id'],
		'details' => $result
	);
	return $response;
}

//Déclaration des pages de réglages
function stripe_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

Configuration::setting('stripe',array(
        "Général",

        'stripe_mode' => array("label"=>"Mode","type"=>"select","values"=> array('sandbox'=>'Bac à sable','production'=>'Production')),
        'stripe_currency' => array("label"=>"Devise","type"=>"text","legend"=>"pour euros, taper : eur"),
        
        "Identifiants de tests",
        
        'stripe_public_sandbox' => array("label"=>"Clé publique","type"=>"text","legend"=>"Publishable key","placeholder"=>"Publishable key"),
        'stripe_private_sandbox' => array("label"=>"Clé privée","type"=>"password","legend"=>"API key"),
        
        "Identifiants de production",

        'stripe_public_production' => array("label"=>"Clé publique","type"=>"text","legend"=>"Publishable key","placeholder"=>"Publishable key"),
        'stripe_private_production' => array("label"=>"Clé privée","type"=>"password","legend"=>"API key"),
));




//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "stripe_install");
Plugin::addHook("uninstall", "stripe_uninstall"); 

Plugin::addHook("menu_setting", "stripe_menu_setting");    
Plugin::addHook("content_setting", "stripe_content_setting");   
  
 

?>