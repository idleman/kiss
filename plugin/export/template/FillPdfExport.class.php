<?php

class FillPdfExport{
	public $mime = 'application/pdf';
	public $extension = 'pdf';
	public $description = 'Fichier informations formulaire PDF';

	public  function sample($dataset,$level = 0){
		return 'Aucun exemple disponible';
	}

	public function start($stream,&$datas){
		$datas = $this->encodeValue($datas);
	}

	public function encodeValue($datas){
		foreach ($datas as $key=>$data) {
			$datas[$key] = is_array($data) ? $this->encodeValue($data) : utf8_decode($data);
		}
		return $datas;
	}

	public function from_template($stream,$datas){
		$temp = str_replace('\\\\','\\',File::temp().rand(0,1000000));
		file_put_contents($temp, $stream );
		$stream = Pdf::fillData($temp,$datas);
		unlink($temp);
		return $stream;

	}
}
?>