<?php
require_once(__DIR__.SLASH.'TextExport.class.php');
class CsvExport extends TextExport{
	public $mime = 'application/csv';
	public $extension = 'csv';
	public $description = 'Fichier tableur générique';

	public  function from_template($stream, $datas){
		return parent::from_template($stream, $datas);
	}

	public function end($stream){
		return  utf8_decode($stream);
	}

	public  function sample($dataset,$level = 0,$parent=''){
		return utf8_decode(parent::sample($dataset,$level));
	}

	public  function formatValue($type,$value){
		if($type == 'image') return '';
		return $value;
	}
}
?>