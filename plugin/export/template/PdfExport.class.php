<?php
require_once(__DIR__.SLASH.'HtmlExport.class.php');
class PdfExport extends HtmlExport{
	public $mime = 'application/pdf';
	public $extension = 'pdf';
	public $description = 'Fichier informations PDF';

	public function sample($dataset, $level=0, $parent=''){
		$stream = parent::sample($dataset);
		$pdf = new Pdf($stream);
		$stream = $pdf->generate();
		return $stream;
	}

	public function end($stream, $datas, $options){
		$margin = isset($options['options']['margin']) ? $options['options']['margin'] : array();
		$format = isset($options['options']['format']) ? $options['options']['format'] : 'A4';
		$orientation = isset($options['options']['orientation']) ? $options['options']['orientation'] : 'Portrait';
	    $pdf = new Pdf($stream,$margin,$format,$orientation);
		
		return $pdf->generate();
	}
}
?>