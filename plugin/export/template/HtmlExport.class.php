<?php
require_once(__DIR__.SLASH.'TextExport.class.php');
class HtmlExport extends TextExport{
	public  $mime = 'text/html';
	public  $extension = 'html';
	public  $description = 'Fichier page web HTML';

	public  function sample($dataset,$level = 0,$parent=''){
		$stream = '<!DOCTYPE html><html lang="fr"><head><meta charset="utf-8"></head><body>';
		$stream .= self::recursive_sample($dataset, $level,$parent);
		$stream .= '</body></html>';
		return $stream;
	}
	public function recursive_sample($dataset,$level = 0,$parent=''){
		$stream = '';
		$parent = ($parent!=''?$parent.'.':'');
		$indentation = str_repeat("<div style='width:20px;display:inline-block;height:10px;'></div>", $level);
		foreach($dataset as $macro => $infos){
			$infos['type'] = isset($infos['type']) ? $infos['type'] : '';
			
			switch($infos['type']){
				case 'list':
					$stream .= $indentation.'<h3>-'.$parent.$macro.' '.(isset($infos['label'])?': '.$infos['label']:'').' (liste)'.'</h3>'.PHP_EOL;
					$stream .= $indentation.'{{#'.$parent.$macro.'}}'.'<br/>'.PHP_EOL;;
					if(is_array($infos['value']) && isset($infos['value'][0])) $stream .=  self::sample($infos['value'][0],$level+1);
					$stream .=$indentation.'<br>{{/'.$parent.$macro.'}}';
				break;
				case 'image':
					$stream .= $indentation.'<img src="{{'.$parent.$macro.'::image}}">'.$infos['label'];
				break;
				case 'object':
					$stream .= '<h3>'.$indentation.'-'.$parent.$macro.' '.(isset($infos['label'])?': '.$infos['label']:'').'</h3>'.PHP_EOL;;
					$stream .= self::sample($infos['value'],$level+1,$parent.$macro);
				break;
				default : 
					$stream .= $indentation.'{{'.$parent.$macro.'}} : '.( !isset($infos['label']) ? '': $infos['label']).'<br/>'.PHP_EOL;
				break;
			}
		}
		return $stream;
	}
	
	public  function formatValue($type,$value){
		if($type == 'image') return 'data:image/png;base64,'.base64_encode($value);
		return $value;
	}
}
?>