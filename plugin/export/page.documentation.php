<?php
User::check_access('export','configure');
require_once(__DIR__.SLASH.'ExportModel.class.php');

?>
<div class="row justify-content-md-center">
	<div class="col-md-8">
		<legend>Documentation :</legend>
		<section class="export-model-documentation">
			<div>
				<span class="mb-2 block">Vous pouvez créer de nouveaux modèles d'exports avec l'une des extensions suivantes :</span>
				<ul>
					<?php foreach(ExportModel::templates() as $exportModel): ?>
					<li><code>.<?php echo $exportModel['extension']; ?> </code> <small>(<?php echo $exportModel['description']; ?>)</small></li>
					<?php endforeach; ?>
				</ul>
				Des fichiers d'exemple à télécharger vous sont proposés dans chacun de ces formats.
			</div>
			<div>
				À l'intérieur de ces documents vous pouvez utiliser des <strong>macros</strong> qui représenteront les <i>informations à exporter</i>.<br>
				Par exemple, <code>{{utilisateur.identifiant}}</code> sera remplacé lors de l'export par votre identifiant de connexion.
			</div>
			<div>
				<span class="mb-2 block">Les macros peuvent retourner trois types d'informations :</span>
				<ul>
					<li>
						Le type <span class="badge macro-value">Valeur</span>:<br>
						<i>ex :</i> <code>{{utilisateur.prénom}}</code> retourne <strong>John</strong>
					</li>
					<li>Le type <span class="badge macro-list">Liste</span>:<br>
						<i>ex :</i> <code><strong>{{#liste.utilisateurs}}</strong>{{utilisateur.prénom}} {{utilisateur.nom}},<strong>{{/liste.utilisateurs}}</strong></code>
						retourne la liste des prénoms et noms des utilisateurs du logiciel séparés par virgules.
					</li>
					<li>
						Le type <span class="badge macro-image">Photo</span>:<br>
						<i>ex :</i> <code>{{utilisateur.photo}}</code> retourne la photo <img src="img/default-avatar.png" alt="Image de profil par défaut" class="doc-picture avatar-rounded">
					</li>
				</ul>
			</div>
			<div>
				<span class="mb-2 block">
					Il y a également la possibilité de faire des conditions sur certains champs.<br>
					Ces conditions se présentent sous la forme d'un <b>Si</b> <code>&lt;condition&gt;</code> <b>Alors</b> <i>&lt;résultat&gt;</i> <b>Sinon</b> <i>&lt;autre-résultat&gt;</i><br><br>
					Pour utiliser les conditions vous devez :
				</span>
				<ol>
					<li>
						Ouvrir la macro de condition <b>Si</b> : <code>{{#condition}}</code>
					</li>
					<li>
						Placer le <i>contenu</i> <code></code>
					</li>
					<li>
						Fermer la macro de condition : <code>{{/condition}}</code>
					</li>
					<li>
						Ouvrir la macro de condition <b>Sinon</b> : <code>{{^condition}}</code>
					</li>
					<li>
						Placer le <i>contenu</i> <code></code>
					</li>
					<li>
						Fermer la macro de condition : <code>{{/condition}}</code>
					</li>
				</ol>
				Par ailleurs, il est aussi possible de n'utiliser que la condition <b>Si</b> comme ceci : <code>{{#condition}}</code> <i>contenu</i> <code>{{/condition}}</code>
			</div>
		</section>			
	</div>
</div>