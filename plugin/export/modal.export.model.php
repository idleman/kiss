<div class="modal fade export-modal" id="export-modal" tabindex="-1" role="dialog" aria-labelledby="export-modal-label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-secondary text-light">
				<h4 class="modal-title" id="export-modal-label">Export modèle :</h4>
				<button type="button" class="close text-light" data-dismiss="modal" aria-label="Fermer">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body row">
				<div id="exportmodel-form" class="col-md-12 exportmodel-form" data-action="export_model_export" data-id="">
					<legend>Choisissez un modèle d'export :</legend>
					<div class="input-group mb-3">
						<select name="exportModel" id="exportModel" class="form-control">
							<option value="none"> - </option>
						</select>
						<div class="input-group-append">
						    <a class="btn btn-outline-success" href="index.php?module=export&page=sheet" target="_blank" data-tooltip title="Créer un nouvel export modèle"><i class="fas fa-plus"></i></a>
						  </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="btn btn-small btn-light" data-dismiss="modal">Fermer</div>
				<div onclick="export_model_export();" class="btn btn-small btn-primary" id="export-button"><i class="fas fa-file-export"></i> Exporter</div>
			</div>
		</div>
	</div>
</div>