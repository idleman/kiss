<?php 
User::check_access('export','configure');
require_once(__DIR__.SLASH.'ExportModel.class.php');
$exportmodel = ExportModel::provide();

$datasets = array();
Plugin::callHook('export_model_data', array(&$datasets, array() ));

$dataSetPicker = array();
foreach ($datasets as $slug=>$dataset) {
	if(!isset($dataSetPicker[$dataset['plugin']])) $dataSetPicker[$dataset['plugin']] = array();
	$dataSetPicker[$dataset['plugin']][] = $dataset;
} 
?>
<br>
<div class="export">
	<div id="exportmodel-form" class="row exportmodel-form" data-action="export_model_save" data-id="<?php echo $exportmodel->id; ?>">
		<div class="col-md-12">
			<h3 class="d-inline-block">Fiche export modèle</h3>
			<div onclick="export_model_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
			<a href="setting.php?section=export" class="btn btn-info float-right mr-2"><i class="fas fa-arrow-left"></i> Retour</a>
			<hr>
		</div>
		<div class="col-md-8">
			<div class="row mb-2">
				<div class="col-md-8">
					<label for="label">Libellé : </label>
					<input required id="label" name="label" class="form-control" placeholder="Nom export modèle" value="<?php echo html_decode_utf8($exportmodel->label); ?>" type="text">
				</div>
				<div class="col-md-4 position-relative">
					<div class="export-privacy-button text-center">
						<label for="privacy" class="pointer"><i class="fas fa-user-secret"></i>&nbsp;Export modèle privé :</label>
						<input type="checkbox" data-type="checkbox" id="privacy" name="privacy" <?php echo $exportmodel->privacy==ExportModel::PRIVACY_PRIVATE?'checked':''; ?>>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="<?php echo $myUser->superadmin ? 'col-md-8' : 'col-md-12'; ?>">
					<label for="dataset">Jeu de données associé au modèle d'export : </label>
					<select required name="dataset" id="dataset" class="form-control" onchange="export_model_get_dataset(this);">
						<option value="" class="template"> - </option>
						<?php foreach ($dataSetPicker as $plugin=>$datasets): ?>
							<optgroup label="<?php echo $plugin; ?>">
								<?php foreach ($datasets as $dataset): ?>
									<option <?php echo $exportmodel->dataset==$dataset['dataset'] ? 'selected="selected"':'' ?> data-plugin="<?php echo $plugin; ?>" value="<?php echo $dataset['dataset']; ?>"><?php echo $dataset['label']; ?></option>
								<?php endforeach; ?>
							</optgroup>
						<?php endforeach; ?>
					</select>
				</div>
				<?php if($myUser->superadmin): ?>
					<div class="col-md-4">
						<label for="label">Slug : </label>
						<input required id="slug" name="slug" class="form-control" placeholder="Slug export modèle" value="<?php echo html_decode_utf8($exportmodel->slug); ?>" type="text">
					</div>
				<?php endif; ?>
				<div class="col-md-12 mt-2">
					<label for="description">Description : </label>
					<input id="description" name="description" class="form-control" placeholder="Description export modèle" value="<?php echo html_decode_utf8($exportmodel->description); ?>" type="text">
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div>
				<label for="document">Fichier modèle :</label>
				<div data-type="dropzone" data-max-files="1" data-allowed="<?php echo implode(',',array_keys(ExportModel::templates())); ?>" data-label="Faites glisser le modèle d'export ici..." data-delete="export_model_delete_document" data-save="export_model_add_document" class="form-control" id="document" name="document"><?php echo json_encode($exportmodel->documents()); ?></div>
			</div>
	
			<div class="options-box">
				<!-- format box -->
				<label for="export_format" class="pointer">&nbsp;Export au format :</label>
				<select name="export_format" data-value="<?php echo $exportmodel->export_format; ?>" id="export_format" class="form-control"></select>
			</div>
		</div>
		<div class="clear"></div>
		<hr class="col-md-12">
		<div id="dataset-container" class="col-md-12 dataset-container p-0 hidden">
			<div class="row">
				<div class="col-md-8 dataset-showcase">
					<h5 class="mb-2 d-inline-block">Liste des macros disponibles pour ce jeu de données :</h5>
					<a class="d-inline-block float-right" href="index.php?module=export&page=documentation"><i class="far fa-question-circle"></i> Voir la documentation générale</a>
					<div class="clear"></div>
					<ul>
						<li onclick="export_copy_macro(this,event)" class="hidden template li-{{type}}" data-type="{{type}}" data-macro="{{macro}}">
							<span class="badge badge-pill macro-{{type}}">{{badge}}</span>
							<code data-code="{{absolutemacro}}">{{macro}}</code> : {{label}}
						</li>
					</ul>
				</div>
				<div class="col-md-4 dataset-example-files">
					<span class="mb-2 block"><i class="fas fa-cloud-download-alt"></i> Fichiers d'exemple à télécharger :</span>
					<ul>
						<li class="hidden template" title="Cliquer pour copier dans le presse-papier"><a href="{{path}}" class="font-weight-bold"><i class="fas {{icon}}"></i> {{label}}</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div><br>
