<?php
/**
 * Define a exportmodel.
 * @author Valentin MORREEL
 * @category Plugin
 * @license MIT
 */
class ExportModel extends Entity{
	public $id,$label,$description,$plugin,$dataset,$slug,$filename,$privacy,$export_format;
	protected $TABLE_NAME = 'export_model';
	public $fields =
	array(
		'id' => 'key',
		'label' => 'string',
		'description' => 'longstring',
		'plugin' => 'string',
		'dataset' => 'string',
		'slug' => 'string',
		'filename' => 'string',
		'privacy' => 'string',
		'export_format' => 'string'
	);

	const PRIVACY_PRIVATE = 'private';
	const PRIVACY_PUBLIC = 'public';

	public function privacy(){
		return $this->privacy==self::PRIVACY_PRIVATE?'Privé':'Public';
	}

	//Renvoie les infos des documents placés en dropzone
	public function documents(){
		$documents = array();
		foreach(glob(__ROOT__.FILE_PATH.'export'.SLASH.'documents'.SLASH.$this->plugin.SLASH.$this->id.SLASH.'*.*') as $file){
			if(is_dir($file)) continue;
			$filenameDisk = $file;

			if(get_OS() === 'WIN') $file = utf8_encode($file);
			$ext = getExt($file);
			$documents[] = array(
				'path' => 'export'.SLASH.'documents'.SLASH.$this->plugin.SLASH.$this->id.SLASH.basename($file),
				'url' => 'action.php?action=export_model_download_document&path='.$this->plugin.SLASH.$this->id.SLASH.rawurlencode(basename($file)),
				'name' => basename($file),
				'ext' => $ext,
				'icon' => getExtIcon($ext),
				'lastModification' => ' - '.date('d/m/Y', filemtime($filenameDisk))
			);
		}
		return $documents;
	}

	public static function get_standard_dataset($parameters){
		global $myUser, $myFirm;
		setlocale (LC_TIME, 'fr_FR.utf8','fra');
		
		//Common DS
		$data['programme']  = array('type'=>'object','value'=>array(
			'date' => array(
				'label'=>'Date courante',
				'type'=>'object',
				'value' => array(
					'compact' => array('label'=>'Date courante (format "dd/mm/YYYY")','value'=>date('d/m/Y')),
					'littéral' => array('label'=>'Date courante (format "01 Janvier 1970")','value'=>strftime('%d %B %Y'))
				)
			),
			'heure' => array('label'=>'Heure courante (format hh:mm)','value'=> date('H:i')),
			'adresse-racine' => array('label'=>'Adresse racine','value'=> ROOT_URL),
		));

		$data['utilisateur'] = array('type'=>'object','value'=>array(
			'identifiant' => array('label'=>'Identifiant de l\'utilisateur courant','value'=>$myUser->login),
			'prénom' => array('label'=>'Prénom de l\'utilisateur courant','value'=>$myUser->firstname),
			'nom' => array('label'=>'Nom de l\'utilisateur courant','value'=>$myUser->name),
			'fonction' => array('label'=>'Fonction occupée par l\'utilisateur courant','value'=>$myUser->function),
			'email' => array('label'=>'Adresse e-mail de l\'utilisateur courant','value'=>$myUser->mail),
			'photo' => array(
				'label'=>'Photo de profil de l\'utilisateur courant', 
				'type'=>'image',
				'value' => file_get_contents($myUser->getAvatar(true))
			)
		));

		$data['établissement'] = array('type'=>'object','value'=>array(
			'logo' => array(
				'label'=>'Le logo de l\'établissement courant', 
				'type'=>'image',
				'value' => file_get_contents($myFirm->logo('path')),
			),
			'logo_64' => array(
				'label'=>'Le logo de l\'établissement courant en base64', 
				'type'=>'image',
				'value' => base64_encode(file_get_contents($myFirm->logo('path'))),
			),
			'logo_mime' => array(
				'label'=>'Le type mime du logo', 
				'value' => mime_content_type($myFirm->logo('path')),
			),
			'libellé' => array('label'=>'Le libellé de l\'établissement courant','value'=>$myFirm->label),
			'téléphone' => array('label'=>'Le n° de téléphone de l\'établissement courant','value'=>$myFirm->phone),
			'fax' => array('label'=>'Le n° de fax de l\'établissement courant','value'=>$myFirm->fax),
			'email' => array('label'=>'L\'adresse mail de l\'établissement courant','value'=>$myFirm->mail),
			'siret' => array('label'=>'Le n° de SIRET de l\'établissement courant','value'=>$myFirm->siret),
		));

		$data['établissement']['value']['adresse'] = array('type'=>'object','value'=>array(
			'rue' => array('label'=>'La rue de l\'établissement courant','value'=>$myFirm->street),
			'complément' => array('label'=>'Le complément d\'adresse de l\'établissement courant','value'=>$myFirm->street2),
			'ville' => array('label'=>'La ville de l\'établissement courant','value'=>$myFirm->city),
			'cp' =>  array('label'=>'Le code postal de l\'établissement courant','value'=>$myFirm->zipcode),
		));

		return $data;
	}

	//Renvoie les différents type de templates pris en compte
	public static function templates($key=null){

		$templates = array();
		foreach (glob(__DIR__.SLASH.'template'.SLASH.'*.class.php') as $templatePath) {
			require_once($templatePath);
			$className = str_replace('.class.php','',basename($templatePath));
			$infos = get_class_vars($className);
			$templates[$infos['extension']] = array('handler' => $className,'mime'=>$infos['mime'],'extension'=>$infos['extension'], 'description'=>$infos['description']);
		}

		if(!isset($key)) return $templates;
		return isset($templates[$key]) ? $templates[$key] : $templates['txt'];	
	}

	//Permet d'ajouter un jeu de données pour un plugin donné
	public static function add($plugin, $dataset, $label, $func){
		Plugin::addHook('export_model_data', function(&$datasets, $params) use ($plugin, $dataset, $label, $func){
			global $myUser;
			if(isset($params['plugin']) && $params['plugin'] != $plugin) return;
			if(isset($params['dataset']) && $params['dataset'] != $dataset) return;
			$datasets[$dataset] = array(
				'plugin' => $plugin,
				'dataset' => $dataset,
				'label' => $label,
				'function' => $func
			);
		});
	}

	//Récuperation de toutes les données (ou description) d'un dataset
	public static function dataset($plugin,$dataset,$parameters=array(),$description = null){
		$datasets = array();
	
		Plugin::callHook('export_model_data', array(
			&$datasets, array(
			'plugin' => $plugin,
			'dataset' => $dataset,
			'description' => true
		)));

		$current = reset($datasets);
		$data = $current['function']($parameters);
		$current['values'] = array();
		//Merge des données génériques erp et des données du dataset
		$allDataset = array_merge_recursive(
			ExportModel::get_standard_dataset($parameters),
			(!$data?array():$data)
		);
		
		$current['values'] = self::recursive_dataset_empty($allDataset);
		return $current;
	}

	public static function rawData($dataset){
		foreach ($dataset as $key => $value) {

			if(is_array($value) && array_key_exists('value', $value)){
			 	if(is_array($value['value'])) $value['value'] = self::rawData($value['value']);
				$dataset[$key] = (empty($value['value']) && (!is_array($value['value']) && !strlen($value['value'])) ) ? '' : $value['value'];
			}else{
				if(is_array($value)) $dataset[$key] = self::rawData($value);
			}
		}
		return $dataset;
	}


	public static function recursive_dataset_empty($set){
		$format = array();
		foreach ($set as $macro => $row) {

			if(is_array($row)){
				if(!empty($row['value']) &&  empty($row['type'])) $row['type'] = 'value';
				$row = self::recursive_dataset_empty($row);
			}
			if(isset($row['value']) && is_string($row['value'])){
				$row['value'] = '';
			}
		
			$format[$macro] = $row;
		}
		return $format;
	}

	//Export d'un set de donnée $datas dans le template $modelStream en fonction du type de document $type
	// ex : ExportModel::export('CsvExport', 'col1;{{name}}',array('name'=> array('value' => 'toto') ));
	public static function export($type, $modelStream, $datas, $parameters=array()){
		require_once(__DIR__.SLASH.'template'.SLASH.$type.'.class.php');
		$instance = new $type();
		if(method_exists($instance,'start')) $instance->start($modelStream,$datas, $parameters);
		$stream = $instance->from_template($modelStream, $datas);
		if(method_exists($instance,'end')) $stream = $instance->end($stream, $datas, $parameters);
		return $stream;
	}

	//convertis une entité en dataset
	public static function fromEntity($entity){
		$class = get_class($entity);
		$fields = $class::fields(false);
		$data = array('label'=>$class::entityLabel(), 'type'=>'object','value' => array());
		foreach($fields  as $key=>$field){
			$value = $entity->$key;
			$type = FieldType::available($field['type']);
			if(property_exists($type, 'onLoad')){
				$method = $type->onLoad;
				$value = $method($value);
			}

			$data['value'][$key] = array('label' =>( isset($field['label'])?$field['label'] : $key),'value' => $value);
		}
		return $data;
	}

}
?>