<?php 
global $myUser;
User::check_access('export','configure');
require_once(__DIR__.SLASH.'ExportModel.class.php');

$plugins = Plugin::getAll(true);
foreach ($plugins as $plugin)
	$allPlugs[$plugin->folder] = $plugin->name;

?>
<div class="row mb-2">
	<div class="col-md-12">
		<br>
		<?php if($myUser->can('export', 'edit')) : ?>
		<a href="index.php?module=export&page=sheet" class="btn btn-success float-right"><i class="fas fa-plus"></i> Créer un modèle d'export</a>
		<?php endif; ?>
		<h3>Réglages Export Modèle</h3>
		<hr>
	</div>

    <div class="col-md-12">
        <select id="filters" data-slug="exportmodel-search" data-type="filter" data-label="Recherche" data-function="export_model_search">
            <option value="description" data-filter-type="text">Description</option>
            <option value="filename" data-filter-type="text">Nom de fichier</option>
            <option value="plugin" data-filter-type="list" data-values='<?php echo json_encode($allPlugs); ?>'>Plugin</option>
            <option value="privacy" data-filter-type="list" data-values='<?php echo json_encode(array(ExportModel::PRIVACY_PUBLIC=>'Public', ExportModel::PRIVACY_PRIVATE=>'Privé')); ?>'>Visibilité</option>
        </select>
    </div>
</div>
<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		<table id="exportmodels" class="table table-striped table-exportmodels " data-entity-search="export_model_search">
            <thead>
                <tr>
                    <th></th>
                    <th data-sortable="label">Libellé</th>
                    <th data-sortable="description">Description</th>
                    <th data-sortable="plugin">Plugin</th>
                    <th style="width: 150px;">Jeu de données</th>
                   
                	<th></th>
                </tr>
            </thead>
            <tbody>
                <tr data-id="{{id}}" class="hidden">
                    <td class="align-middle p-2"><h2 class="text-center m-0"><i class="{{icon}}"></i></h2></td>
	                <td class="exportmodel-{{class}} align-middle p-2" title="Export modèle {{privacy}} ({{slug}})">
                        <a class="text-primary" href="index.php?module=export&page=sheet&id={{id}}">{{label}}</a>
                        <div class="text-muted"><small>Modèle : {{filename}}</small></div>
                    </td>
	                <td class="align-middle p-2">{{description}}</td>
	                <td class="align-middle p-2">{{pluginName}}</td>
	                <td class="align-middle p-2">{{datasetName}}</td>
	                <td class="text-right align-middle p-2">
	                    <a class="btn btn-info btn-squarred btn-mini" href="index.php?module=export&page=sheet&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
	                    <div class="btn btn-danger btn-squarred btn-mini" onclick="export_model_delete(this);"><i class="fas fa-times"></i></div>
	                </td>
                </tr>
           </tbody>
        </table>

        <!-- Pagination -->
        <ul class="pagination justify-content-center">
            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');export_model_search()">
                <span class="page-link">{{label}}</span>
            </li>
        </ul>
	</div>
</div>