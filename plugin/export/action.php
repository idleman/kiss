<?php

	/** TEMPLATE **/
	
	//Récuperation d'une liste de exportmodel
	Action::register('export_model_search',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','read');
			require_once(__DIR__.SLASH.'ExportModel.class.php');

			if(isset($_['params']) && !empty($_['params'])){
				//Récupération export modèle avec paramètres 
				//du plugin et jeu de données depuis le composant
				//d'export modèle
				$params = $_['params'];
				if(!isset($params['plugin']) || empty($params['plugin'])) throw new Exception("Il faut préciser le plugin ciblé");
				if(!isset($params['dataset']) || empty($params['dataset'])) throw new Exception("Il faut préciser le jeu de données à récupérer");

				$query = "SELECT * FROM {{table}} WHERE plugin = ? AND dataset = ? AND (privacy = ? OR (privacy = ? AND creator = ?)) ORDER BY label ASC";
				$data = array($params['plugin'], $params['dataset'], ExportModel::PRIVACY_PUBLIC, ExportModel::PRIVACY_PRIVATE,$myUser->login);

				foreach(ExportModel::staticQuery($query, $data,true) as $exportmodel)
					$response['rows'][] = $exportmodel;
			} else {
				//Suppression de tous les exports modèles non conformes
				foreach(ExportModel::staticQuery('SELECT * FROM {{table}} WHERE dataset IS NULL', array(), true) as $exMdl) {
					ExportModel::deleteById($exMdl->id);
					foreach ($exMdl->documents() as $doc) {
						$path = (get_OS() === 'WIN') ? utf8_decode($doc['path']) : $doc['path'];
						unlink(File::dir().$path);
					}
				}

				//Récupération des exports modèles pour
				//vue liste des exports modèle dans les 
				//paramètres.

				//Récupération liste des plugins
				foreach (Plugin::getAll() as $plugin)
					$allPlug[$plugin->folder] = $plugin;

				//Récupération jeux de données
				$datasets = array();
				Plugin::callHook('export_model_data', array(&$datasets, array('description'=>true)));

				$query = 'SELECT * 
				FROM {{table}} 
				WHERE (privacy = ? OR (privacy = ? AND creator = ?)) ';
				$data = array(ExportModel::PRIVACY_PUBLIC,ExportModel::PRIVACY_PRIVATE,$myUser->login);

				//Recherche simple
				if(!empty($_['filters']['keyword'])){
					$query .= ' AND (';
					foreach (array('filename', 'label', 'description') as $column) {
						$query .= $column.' LIKE ? OR ';
						$data[] = '%'.$_['filters']['keyword'].'%';
					}
					$query = rtrim($query, 'OR ');
					$query .= ')';
				}

				//Recherche avancée
				if(isset($_['filters']['advanced'])) filter_secure_query($_['filters']['advanced'],array('label','description','slug','filename','plugin','privacy'),$query,$data);

				//Tri des colonnes
				if(isset($_['sort']))
					sort_secure_query($_['sort'],array('label','description','slug','filename','plugin'),$query,$data);
				else 
					$query .= ' ORDER BY plugin ASC, created DESC';

				//Pagination
				$response['pagination'] = ExportModel::paginate(20,(!empty($_['page'])?$_['page']:0),$query,$data);
				
				//Mise en forme des résultats
				foreach(ExportModel::staticQuery($query,$data,true) as $exportmodel){
					$row = $exportmodel->toArray(true);
					$row['pluginName'] = $allPlug[$exportmodel->plugin]->name;
					$row['datasetName'] = isset($datasets[$exportmodel->dataset]) ? $datasets[$exportmodel->dataset]['label'] : $exportmodel->dataset;
					$row['class'] = $exportmodel->privacy==ExportModel::PRIVACY_PRIVATE ? 'private' : 'public';
					$row['privacy'] = $exportmodel->privacy();
					$row['icon'] = !empty($exportmodel->export_format) ? getExtIcon($exportmodel->export_format) : getExtIcon(getExt($exportmodel->filename));
					$response['rows'][] = $row;
				}
			}
		
	});
	
	Action::register('export_model_format_refresh',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','edit');
			require_once(__DIR__.SLASH.'ExportModel.class.php');
			$response['rows'] = array();
			
			switch($_['ext']){
				case 'html':
					$response['rows'][] = array('uid' => 'html', 'label' => 'HTML');
					$response['rows'][] = array('uid' => 'pdf', 'label' => 'PDF');
				break;
				default:
				break;
			}
		
	});

	//Ajout ou modification d'élément exportmodel
	Action::register('export_model_save',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','edit');
			require_once(__DIR__.SLASH.'ExportModel.class.php');

			if(!isset($_['label']) || empty($_['label'])) throw new Exception('Champ "Libellé" obligatoire');
			if(!isset($_['plugin']) || empty($_['plugin'])) throw new Exception('Champ "Plugin" obligatoire');
			if(!isset($_['dataset']) || empty($_['dataset'])) throw new Exception('Champ "Jeu de données" obligatoire');

			$item = ExportModel::provide();
			$_['slug'] = !empty($_['slug']) ? slugify($_['slug']) : (!empty($item->slug) ? $item->slug : slugify($_['label']));
			if(!empty($_['slug']) && ExportModel::load(array('slug'=>$_['slug'], 'id:!='=>$item->id))) throw new Exception("Le slug (".$_['slug'].") est déjà utilisé pour un autre export modèle");
			$newItem = isset($item->id) && !empty($item->id) ? false : true;
			$docs = $item->documents();
			//Ajout des fichiers joints
			if((!isset($_['document_temporary']) || empty($_['document_temporary'])) && empty($docs)) throw new Exception("Un fichier de modèle est requis");

			$item->label = $_['label'];
			$item->description = $_['description'];
			$item->plugin = $_['plugin'];
			$item->dataset = $_['dataset'];
			$item->slug = $_['slug'];
			$item->privacy = $_['privacy']==1 ? ExportModel::PRIVACY_PRIVATE : ExportModel::PRIVACY_PUBLIC;
			$item->save();

			if(!empty($_['document_temporary'])){
				$files = json_decode($_['document_temporary'],true);

				if(count($files)>1) {
					if($newItem) ExportModel::deleteById($item->id);
					throw new Exception("1 seul fichier est nécessaire par export modèle");
				}
				if(count($docs)==1) {
					if($newItem) ExportModel::deleteById($item->id);
					throw new Exception("Un fichier est déjà présent pour ce modèle d'export");
				}

				foreach($files as $file){
					$item->filename = $file['name'];
					$from = (get_OS() === 'WIN') ? File::temp().utf8_decode($file['path']) : File::temp().$file['path'];
					$to = (get_OS() === 'WIN') ? utf8_decode($file['name']) : $file['name'];
					$fileReturn = File::move($from, 'export'.SLASH.'documents'.SLASH.$item->plugin.SLASH.$item->id.SLASH.$to);
					$response['relativePath'] = $fileReturn['relative'];
					$response['filePath'] = 'action.php?action=export_model_download_document&path='.rawurlencode($item->plugin.SLASH.$item->id.SLASH.$to);
				}
			}
			if(count($docs)==1) {
				if(empty($item->export_format)) {
					$ext = getExt($docs[0]['path']);
					$item->export_format = $ext;
				}
				$item->filename = $docs[0]['name'];
			}
			$item->export_format = !isset($_['export_format']) || empty($_['export_format']) || $_['export_format']=='none' ? $item->export_format : $_['export_format'];
			$item->save();
			$response['id'] = $item->id;

			Log::put("Création/Modification d'export modèle ".$item->toText(), "Export Modèle");
		
	});
	
	//Récuperation ou edition d'élément exportmodel
	Action::register('export_model_edit',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','edit');
			require_once(__DIR__.SLASH.'ExportModel.class.php');
			$response = ExportModel::getById($_['id']);
		
	});

	//Suppression d'élement exportmodel
	Action::register('export_model_delete',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','delete');
			require_once(__DIR__.SLASH.'ExportModel.class.php');
			
			$item = ExportModel::getById($_['id']);
			ExportModel::deleteById($item->id);

			$docsProps = $item->documents();
			foreach ($docsProps as $doc) {
				$path = (get_OS() === 'WIN') ? utf8_decode($doc['path']) : $doc['path'];
				unlink(File::dir().$path);
			}

			// Décommenter pour une supression logique
			// $item = ExportModel::getById($_['id']);
			// $item->state = ExportModel::INACTIVE;
			// $item->save();
			Log::put("Suppression d'export modèle ".$item->toText(), "Export Modèle");
		
	});

	//Ajout document automatique à l'upload
	Action::register('export_model_add_document',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','edit');
			require_once(__DIR__.SLASH.'ExportModel.class.php');

			$exportmodel = ExportModel::provide();
			if((!isset($_['plugin']) || empty($_['plugin'])) && (!isset($exportmodel->plugin) || empty($exportmodel->plugin))) throw new Exception("Vous devez d'abord définir un plugin pour importer un fichier modèle");

			$newItem = isset($exportmodel->id) && !empty($exportmodel->id) ? false : true;
			$exportmodel->plugin = $_['plugin'];
			$exportmodel->save();

			if(count($_['files'])>1) {
				if($newItem) ExportModel::deleteById($exportmodel->id);
				throw new Exception("1 seul fichier est nécessaire par export modèle");
			}
			if(count($exportmodel->documents())==1) {
				if($newItem) ExportModel::deleteById($exportmodel->id);
				throw new Exception("Un fichier est déjà présent pour ce modèle d'export");
			}
			foreach ($_['files'] as $file) {
				$ext = getExt($file['name']);
				$exportmodel->export_format = $ext;

				$name = (get_OS() === 'WIN') ? utf8_decode($file['name']) : $file['name'];
				$exportmodel->filename = $file['name'];
				$row = File::move(File::temp().$file['path'],'export'.SLASH.'documents'.SLASH.$exportmodel->plugin.SLASH.$exportmodel->id.SLASH.$name);
				$row['url'] = 'action.php?action=export_model_download_document&path='.SLASH.$exportmodel->plugin.SLASH.$exportmodel->id.SLASH.rawurlencode($file['name']);
				$row['oldPath'] = $file['path'];
				$row['ext'] = $file['ext'];
				$response['files'][] = $row;
			}
			$exportmodel->save();

			$response['ext'] = $ext;
			$response['id'] = $exportmodel->id;
		
	});

	//Suppression document
	Action::register('export_model_delete_document',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','delete');
			require_once(__DIR__.SLASH.'ExportModel.class.php');
			if(!isset($_['path']) ) throw new Exception("Chemin non spécifié ou non numerique");
			$path = (get_OS() === 'WIN') ? utf8_decode($_['path']) : $_['path'];
			//Le premier argument est un namespace de sécurité 
			//et assure que le fichier sera toujours cloisoné dans un contexte file/export/documents
			File::delete('export'.SLASH.'documents',$path);

			$regex = "@\\".SLASH."(\d+)\\".SLASH."@i";
			preg_match($regex, $path, $matches);
			if(isset($matches[1])) ExportModel::change(array('export_format'=>NULL, 'filename'=>NULL), array('id'=>$matches[1]));

			Log::put("Suppression de fichier modèle : ".$_['path'], "Export Modèle");
		
	});

	//Téléchargement des documents
	Action::register('export_model_download_document',function(&$response){
		global $myUser,$_;
		User::check_access('export','read');
		$path = (get_OS() === 'WIN') ? utf8_decode($_['path']) : $_['path'];
		File::downloadFile(File::dir().'export'.SLASH.'documents'.SLASH.$path);

		Log::put("Téléchargement du fichier modèle : ".$path, "Export Modèle");
		exit();
	});

	//Téléchargement des templates d'exemple
	Action::register('export_model_download_template',function(&$response){
		global $myUser,$_;
		User::check_access('export','read');
		if(!isset($_['extension']) || empty($_['extension'])) throw new Exception("Extension non précisée");
		
		$template = ExportModel::templates($_['extension']);
		$dataset = ExportModel::dataset($_['plugin'],$_['dataset']);
		$className =  $template['handler'];
		$instance = new $className();

		$stream = $instance->sample($dataset['values']);
		
		File::downloadStream($stream, $_['filename'].'.'.$template['extension'],$template['mime']);

		Log::put("Téléchargement d'un fichier modèle d'exemple : ".$_['filename'].'.'.$template['extension'], "Export Modèle");
		exit();
	});

	//Récupération du détail d'un jeu de donnée
	Action::register('export_model_get_dataset',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','read');
			require_once(__DIR__.SLASH.'ExportModel.class.php');
	
			$params = isset($_['params']) ? $_['params'] : array();
			if(empty($params)) throw new Exception("Aucun paramètre spécifié pour récupérer le informations du jeu de données");

			$plugin = $_['params']['plugin'];
			$dataset = $_['params']['dataset'];
			$params['description'] = true;
			$set = ExportModel::dataset($plugin, $dataset, $params);
			$response['dataset'] = $set['values'];
			
			foreach(ExportModel::templates() as $extension => $template){
				$response['files'][] = array(
					'path'=>'action.php?action=export_model_download_template&plugin='.rawurlencode($plugin).'&dataset='.rawurlencode($dataset).'&extension='.$template['extension'].'&filename='.$set['label'],
					'icon'=>getExtIcon($template['extension']),
					'ext'=>$template['extension'],
					'label'=> $set['label'].'.'.$template['extension']
				);
			}
		
	});

	//Choix du exportmodel pour export modèle
	Action::register('export_model_export',function(&$response){
	
			global $myUser,$_;
			User::check_access('export','read');
			require_once(__DIR__.SLASH.'ExportModel.class.php');
				
			$parameters = isset($_['parameters']) ? $_['parameters'] : array();
			if(!isset($parameters['model']) || empty($parameters['model'])) throw new Exception("Il faut choisir un modèle d'export");
			$response['parameters'] = $parameters;
			$parameters['description'] = false;
		
			//Récuperation du fichier modèle sélectionné
			$model = ExportModel::getById($parameters['model']);
			if(!$model) throw new Exception("Impossible de récupérer le modèle d'export");

			//Récuperation des données brutes tout dataset confondus
			$datasets = array();
			Plugin::callHook('export_model_data', array(&$datasets, $parameters));
			
			//On  récupère uniquement le dataset qui nous intéresse et on execute la fonction associée
			if(!isset($datasets[$model->dataset])) throw new Exception("Jeu de données spécifié inexistant");
			$dataset = $datasets[$model->dataset];
			
			//On ajoute les données standard (comptes connecté etc..) aux set de donnée à associer au template
			$dataset = array_merge_recursive(ExportModel::get_standard_dataset($parameters), $dataset['function']($parameters));
	
			//On génère le fichier à exporter en utilisant le jeu de données ($datas)
			//+ le fichier associé à l'exportmodel (le fichier mis en dropzone)
			$templates = $model->documents();
			if(empty($templates)) throw new Exception("Aucun fichier modèle pour le modèle d'export : ".$model->label);

			foreach ($templates as $document) {
				$ext = getExt($document['name']);
				//Type d'export (csv, excel, html, word ...)
				$type = ExportModel::templates($model->export_format);
				$filename = basename($document['name'], $ext).$model->export_format;

				$filePath = (get_OS() === 'WIN') ? utf8_decode($document['path']) : $document['path'];
				
				//Les types d'export prennent toujours de l'utf8 en entrée
				$stream = file_get_contents(File::dir().$filePath);
				if(mb_detect_encoding($stream, 'UTF-8', true) == false) $stream = utf8_encode($stream);

				$datas = ExportModel::rawData($dataset);

				//Gestion macros dans le titre
				$filename = ExportModel::export('TextExport', $filename, $datas, $parameters);
				
				//On exporte le jeux de donnée pour le flux modèle séelctionné
				$stream = ExportModel::export($type['handler'], $stream, $datas, $parameters);

				Log::put("Exportation de modèle, plugin : ".$model->plugin.", jeu de données : ".$model->dataset.".", "Export Modèle");
				if(empty($parameters['destination']) || $parameters['destination'] == 'stream') {
					$response['filename'] = $filename;
					$response['type'] = $type['mime'];
					File::downloadStream($stream, utf8_decode($filename), $type['mime']);
					exit();
				} else {	
					//@TODO: Gérer le / et \ à replace avec SLASH pour compatibilité serveur en fct de l'OS
					$filename = preg_replace('#[\<\>\?\|\*\:\\\/\"\'\,]#i', '-', $filename);
					$filename = (get_OS() === 'WIN') ? utf8_decode($filename) : $filename;
					$filename = autoincrement_filename($model->export_format, $parameters['destination'], $filename);
					$response['filename'] = (get_OS() === 'WIN') ? utf8_encode($filename) : $filename;

					$finalPath = File::dir().$parameters['destination'];
					if(!file_exists($finalPath)) mkdir($finalPath,0755,true);
					$fileInfo = file_put_contents($finalPath.$filename, $stream);
				}
			}
		
	});

?>