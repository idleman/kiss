//CHARGEMENT DE LA PAGE
var isProcessing = false;
function init_plugin_export(){
	switch($.urlParam('page')){
		case 'sheet':
			export_model_get_dataset($('#dataset'));
			export_model_format_refresh();
		break;
		default:
		break;
	}
}

//Init des settings
function init_setting_export(parameter){
	switch(parameter.section){
		case 'export':
		break;
	}
	$('#exportmodels').sortable_table({
		onSort : export_model_search
	});
}

/* EXPORTMODELE */
//Récuperation d'une liste de exportmodel dans le tableau #exportmodels
function export_model_search(callback){
	var box = new FilterBox('#filters');

	$('#exportmodels').fill({
		action:'export_model_search',
	    filters : box.filters(),
		sort : $('#exportmodels').sortable_table('get')
	}, function(){
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément exportmodel
function export_model_save(){
	var data = $('#exportmodel-form').toJson();
	data.plugin = $('#dataset option:selected').attr('data-plugin');
	$.action(data,function(r){
		$('#exportmodel-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		
		$('#document_temporary').val('');
		var modelFile = $('#exportmodel-form [data-type="dropzone"] ul > li');
		modelFile.attr('data-path', r.relativePath);
		$('> a',modelFile).attr('href', r.filePath);
		$('> i.fa-times',modelFile).attr('onclick', 'export_model_delete_document(this)');
		
		$.message('success','Export modèle enregistré');
	});
}

function export_model_format_refresh(){
	var li = $('#document > ul > li:visible');
	var tpl = '<option  value="{{uid}}">{{label}}</option>';
	$('#export_format').html('');
	$('.options-box').addClass('hidden');
	if(li.length==0 || li.attr('data-ext')=='') return;

	$.action({
		action: 'export_model_format_refresh',
		ext: li.attr('data-ext')
	},function(r){
		for(var key in r.rows)
			$('#export_format').append(Mustache.render(tpl,r.rows[key]));
		
		$('#export_format').val( ($('#export_format').attr('data-value').length ? $('#export_format').attr('data-value') : li.attr('data-ext')) );
		if(r.rows.length>0) $('.options-box').removeClass('hidden');
	});
}

//Récuperation ou edition d'élément exportmodel
function export_model_edit(element){
	var line = $(element).closest('tr');
	$.action({
		action:'export_model_edit',
		id:line.attr('data-id')
	},function(r){
		$.setForm('#exportmodel-form',r);
		$('#exportmodel-form').attr('data-id',r.id);
	});
}

//Suppression d'élement exportmodel
function export_model_delete(element){
	if(!confirm('Êtes-vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	$.action({
		action : 'export_model_delete',
		id : line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Export modèle supprimé');
	});
}

//Ajout de document
function export_model_add_document(files){
	var form = $('#exportmodel-form');
	var exportmodelId = form.attr('data-id');

	$.action({
		action : 'export_model_add_document',
		id: exportmodelId,
		files : files,
		plugin: $('#dataset option:selected').attr('data-plugin')
	}, function(r){
		form.attr('data-id', r.id);

		$.each(r.files, function(i, file){
			var line = $('li[data-path="'+file.oldPath+'"]', form);
			line.attr('data-path', file.relative);
			line.find('a').attr('href', file.url);
			line.find('i.pointer').attr('onclick', 'export_model_delete_document(this)');

			$('[data-type="dropzone"] input:not(:visible)', form).val('');
			
			$.message('success', 'Fichier "'+file.name+'" sauvegardé');
		});
		export_model_format_refresh();
	});
}

//Suppression de document
function export_model_delete_document(element){
	if(!confirm("Êtes-vous sûr de vouloir supprimer ce fichier ?")) return;
	var line = $(element).closest('li');
	$.action({
		action : 'export_model_delete_document',
		path : line.attr('data-path')
	},function(r){
		line.remove();
		export_model_format_refresh();
		$.message('info','Élement supprimé');
	});
}

//Récupération du jeu de données 
//associé à l'élément exportmodel
function export_model_get_dataset(element){
	var container = $('#dataset-container');
	container.addClass('hidden');
	$('#empty-files').removeClass('hidden');
	$('li:not(.template)', container).remove();

	var datasetSelect = $(element);
	var curVal = datasetSelect.val();
	var pluginVal = $('option:selected',datasetSelect).attr('data-plugin');

	if(!curVal || !pluginVal) return;

	var label = $('#label');
	if(!label.length) label.val(pluginVal+': '+$('option:selected',datasetSelect).text());

	if($('#slug').length && !$('#slug').val().length){
		var slug = label.val().toString().toLowerCase().replace(/[^a-z0-9]/g, '-').replace(/\-\-+/g, '-');
		$('#slug').val(slug);
	}

	$.action({
		action: 'export_model_get_dataset',
		params: {
			dataset: curVal,
			plugin: pluginVal
		}
	}, function(r){
		container.removeClass('hidden');
		if(r.dataset){
			var showcase = $('.dataset-showcase');
			var tpl = $('li.hidden',showcase).get(0).outerHTML;
			export_model_recursive_dataset($('> ul', showcase),tpl,r.dataset,'');
		}

		if(r.files && r.files.length)
			$('.dataset-example-files > ul').addLine(r.files);
	});
}

function export_copy_macro(element,event){
	event.stopPropagation();
	var element = $(element);

	value = export_get_macro_recursive(element, []); 
	value.reverse();
	if(element.attr('data-type')!='list') value = '{{'+value.join('.').replace(/[}{]/ig,'')+'}}';
	
	copy_string(value,element.find('>code'));
}

function export_get_macro_recursive(element,path){
	var parent = element.parent().parent();
	if(parent.attr('data-macro')!=null && parent.attr('data-macro')!='' && parent.attr('data-type')!='list'){
		path.push(element.attr('data-macro'));
		path.concat(export_get_macro_recursive(parent,path));
	}else{
		path.push(element.attr('data-macro'));
	}
	return path;
}

function export_model_recursive_dataset(container,tpl,dataset){
	var typeLabel = {
		'list' : 'Liste',
		'image' : 'Image',
		'value': 'Valeur',
		'object': 'Objet'
	};

	$.each(dataset, function(macro, data){
		data.macro = data.type == 'list' ?'{{#'+macro+'}}{{/'+macro+'}}':'{{'+macro+'}}';
		data.type = data.type && data.type!=''  ?  data.type  : 'value';
		data.badge = typeLabel[data.type];
		var li = $(Mustache.render(tpl, data));

		li.removeClass('hidden template');
		container.append(li);

		if(data.type=='object' || data.type=='list'){
			var ul = $('<ul class="subitems"></ul>');
			li.append(ul);
			if(data.type=='list'){
				data.value = data.value[0];
			}
			export_model_recursive_dataset(ul,tpl,data.value);
		}

	});
}


//Export des données en fonction du modèle
//sélectionné dans la modal d'export
function export_model_export(callback){
	if(isProcessing) return;
	var modal = $('#export-modal');
	var model = $('#exportModel').val();
	if(model=='none') return $.message('error', 'Vous devez choisir un modèle d\'export');
	callback = callback!=null ? callback : modal.attr('data-post-callback');

	var parameters = JSON.parse($('#exportmodel-form').attr('data-parameters'));
	parameters.model = model;
	parameters.previousUrl = window.location.href.substring(window.location.href.lastIndexOf('/')+1);

	isProcessing = true;
	$('#export-button', modal).addClass('btn-preloader');
	$.action({
		action: 'export_model_export',
		parameters: parameters,
		downloadResponse: !("destination" in parameters)
	}, function(r){
		isProcessing = false;
		$.message('success', "Export modèle correctement généré");
		modal.modal('hide');

		if(callback!=null && callback.length) window[callback](r);
	}, function(r){
		isProcessing = false;
	});
}
