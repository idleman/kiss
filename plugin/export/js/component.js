/**
 * Initialisation du composant export modèle
 *
 * data-force 		: (facultatif) slug du modèle d'export à utiliser obligatoirement, si renseigné et que le modèle existe on exporte obligatoirement avec ce modèle
 * data-default 	: (facultatif) slug du modèle d'export à mettre par défaut
 * data-parameters  : les paramètres à renseigner pour l'export modèle
 * 		=> doit contenir les clés suivantes :
 * 			- plugin  (obligatoire) : le plugin ciblé
 * 			- dataset (obligatoire) : le jeu de données ciblé
 * 			- destination (facultatif) : chemin où enregistrer le fichier, ne pas mettre si on veut télécharger le fichier généré à la volée
 * 			- autres paramètres personnalisés...
 * data-pre-callback  : string de la fonction appelée avant export (juste après affichage de modal)
 * data-post-callback : string de la fonction appelée après export
 * 
 * @param  jQuery object input l'input qui servira de support pour le composant
 * @return rien       Affiche le modal avec les modèles trouvés pour le plugin et dataset défini
 */
function init_components_export_model(input){
	var preCallback = input.attr('data-pre-callback') ? input.attr('data-pre-callback') : null;
	var postCallback = input.attr('data-post-callback') ? input.attr('data-post-callback') : null;

	$(document).ready(function(e){
		input.off('click').on('click', function(e){
			$.ajax({
				type: 'GET',
				url: 'plugin/export/modal.export.model.php',
				async: true,
				success : function(modal){
					if(preCallback!=null && window[preCallback]) window[preCallback](input);
				}
			}).done(function(modalContent){
				var parameters = JSON.parse(input.attr('data-parameters'));
				if(!$('#export-modal').length)
					$('body').append(modalContent);
				var modal = $('#export-modal');
				reset_inputs(modal);

				$.action({
					action: 'export_model_search',
					params: parameters
				}, function(r){
					var selectExport = $('#exportModel', modal);
					if(r.rows){
						var forceExport = input.attr('data-force');
						var defaultExport = input.attr('data-default');
						selectExport.find('option').remove();

						$.each(r.rows,function(i, option){
							var opt = $('<option value="'+option.id+'">'+option.label+(option.description.length?' - '+option.description:'')+'</option>');
							if(forceExport && forceExport.length && option.slug === forceExport) opt.addClass('force');
							if(defaultExport && defaultExport.length && option.slug === defaultExport) opt.attr('selected', true);
							selectExport.append(opt);
						});
					}
					$('#exportmodel-form').attr('data-parameters', JSON.stringify(parameters));

					if($('> option',selectExport).length == 1 || $('> option.force',selectExport).length == 1){
						selectExport.val($('>option'+($('> option.force',selectExport).length==1?'.force':''),selectExport).val());
						if(selectExport.val()=='none'){
							$.message('warning', "Aucun modèle d'export existant");
							return;
						}
						input.find('.btn').addClass('btn-preloader');
						export_model_export(postCallback);
						return;
					}
					init_components($('#export-modal'));
					modal.modal('show');
					if(postCallback!=null) modal.attr('data-post-callback', postCallback);
				});
			});
		});
	});
}