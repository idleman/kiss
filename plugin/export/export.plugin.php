<?php

//Cette fonction va generer une page quand on clique sur export dans menu
function export_page(){
	global $_,$myUser;
	if(!isset($_['module']) || $_['module'] !='export') return;
	$page = !isset($_['page']) ? 'list' : $_['page'];
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function export_install($id){
	if($id != 'fr.core.export') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function export_uninstall($id){
	if($id != 'fr.core.export') return;
	if(is_dir(File::dir().'export')) delete_folder_tree(File::dir().'export');
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('export',array('label'=>'Gestion des droits sur le plugin export'));


//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');

//Déclaration du menu de réglages
function export_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('export','configure')) return;
	$settingMenu[]= array(
		'sort' =>30,
		'url' => 'setting.php?section=export',
		'icon' => 'fas fa-angle-right',
		'label' => 'Export modèle'
	);
}

//Déclaration des pages de réglages
function export_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 
Plugin::addJs("/js/component.js",true); 

//Mapping hook / fonctions
Plugin::addHook("install", "export_install");
Plugin::addHook("uninstall", "export_uninstall"); 


Plugin::addHook("page", "export_page");  
Plugin::addHook("menu_setting", "export_menu_setting");    
Plugin::addHook("content_setting", "export_content_setting");
?>