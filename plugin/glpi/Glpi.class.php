
<?php

/**
 * Permet la communication avec l'API REST de glpi
 * @author Valentin CARRUESCO
 * @category Rest/External
 * @license MIT
 */

class GLPI{
	public $url,$token = '';
	
	function __construct($url){
		$this->url = $url;
	}

	//Connexion login/mdp et récuperaiton du token
	public function connect($login,$password){
		$response = $this->rest('/initSession/',$login,$password);		
		$this->token = isset($response['session_token']) && !is_null($response['session_token']) ? $response['session_token'] : '';

	}

	//Récuperation d'un utilisateur GLPI par son login
	public function userByLogin($login){
		$criteria = 'criteria[0][field]=2&criteria[0][searchtype]=2&criteria[0][value]=&criteria[1][link]=AND&criteria[1][field]=1&criteria[1][searchtype]=2&criteria[1][value]='.$login;
		$response = $this->rest('/search/User/?'.$criteria,$this->token);
		
		$user = array();
		if(isset($response['data'][0])){
			$user =  array(
				'id' => $response['data'][0][2],
				'name' => $response['data'][0][34],
				'firstname' => $response['data'][0][9],
				'mail' => $response['data'][0][5],
				'phone' => $response['data'][0][6],
				'mobile' => $response['data'][0][11]
			);
		}

		return $user;
	}

	//Récuperation d'un ticket glpi par son id
	public function ticketById($id){

		$response = $this->rest('/Ticket/'.$id.'?expand_dropdowns=true',$this->token);
		return $response;
	}

	//Récuperation des tickets par criteres
	public function searchTickets($criteria){
	
		$response = $this->rest('/search/Ticket/?'.$criteria,$this->token);
		$tickets = array();
		if(isset($response['data'])){
			foreach ($response['data'] as $ticket) {
				$tickets[] = array(
					'id' => $ticket[2],
					'url' => $this->url.'/front/ticket.form.php?id='.$ticket[2],
					'subject' => $ticket[1],
					'date' => $ticket[15],
					'user' => $ticket[64],
					'firm' => $ticket[80],
			
				);
			}
		}
		return $tickets;
	}

	//Décrit les champs d'un item de glpi
	public function searchOptions($itemType = 'Ticket'){
		return $this->rest('/listSearchOptions/'.$itemType,$this->token);
	}
	

	//Récuperation des tickets assigné à un utilisateur
	public function userTickets($id,$criteria = null){
		$criteria = isset($criteria) ? $criteria:  'criteria[0][field]=5&criteria[0][searchtype]=equals&criteria[0][value]='.$id.'&criteria[1][link]=AND&criteria[1][field]=15&criteria[1][searchtype]=equals&criteria[1][value]=TODAY';
		$response = $this->rest('/search/Ticket/?'.$criteria,$this->token);
		$tickets = array();
		if(isset($response['data'])){
			foreach ($response['data'] as $ticket) {
				$tickets[] = array(
					'id' => $ticket[2],
					'url' => 'https://glpi.kiss.fr/front/ticket.form.php?id='.$ticket[2],
					'subject' => $ticket[1],
					'date' => $ticket[15],
					'firm' => $ticket[80],
				);
			}
		}
		return $tickets;
	}

	//Requete rest
	public function rest($action,$login,$password=null,$headers=array()){
		$url = $this->url.'/apirest.php'.$action;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if($password!=null) curl_setopt($ch, CURLOPT_USERPWD, $login . ":" . $password);  

		$headers = array();
		if($password==null) $headers[] = 'Session-Token: '.$login;
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);
		return json_decode($response,true);
	}
}
?>