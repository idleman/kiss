<?php 
require_once(__DIR__.SLASH.'Glpi.class.php');
global $conf,$myUser;


$service = urldecode($widget->data('service'));
$newHours = $widget->data('newHours');
$newHours = empty($newHours) || !is_numeric($newHours) ? 24: $newHours; 


$login =  $conf->get('glpi_login');
$password = $conf->get('glpi_password');
$glpi = new GLPI($conf->get('glpi_host'));
$glpi->connect($login,$password);




?>
<div class="widget-glpi">
	<?php if(empty($service)) : ?>
	<div class="text-center mt-5"><h2 class="text-muted"><i class="fa fas fa-headset"></i> Aucun service spécifié</h2><p>Veuillez configurer ce widget</p></div>
	<?php else: 
		$url = parse_url($service);
		$tickets = array();
		if(isset($url['query'])){

		parse_str($url['query'], $parameters);

		$query = '';
		if(!empty($url['query'])){
			if(!isset($parameters['sort'])){
				$parameters['sort'] = 15;
				$parameters['order'] = 'DESC';
			}
			$query = http_build_query($parameters);
		}

		$glpi->searchOptions();

		$tickets =  $glpi->searchTickets($query) ;
		}
		?>
	<ul>
	<?php 

		$newTreshold = strtotime('-'.$newHours.'hour');
		foreach($tickets as $ticket): 
		$date = strtotime($ticket['date']);

		?>
		<li class="glpi-ticket <?php echo ($date>$newTreshold)?'glpi-ticket-new':'' ?>">
			<i class="fas fa-fire-alt right text-warning glpi-new"></i>
			<a href="<?php echo $conf->get('glpi_host'); ?>/front/ticket.form.php?id=<?php echo  $ticket['id']; ?>">
			<small class="text-muted"><i class="far fa-clock"></i> <?php echo  complete_date($date).' '.date('H:i',$date); ?></small><br>
			<small class="glpi-firm"><i class="fas fa-user-tie"></i> <?php echo  str_replace(array('- > ',' > '),' <i class="fas fa-angle-right text-black-50"></i> ',$ticket['firm']); ?></small><br>
			<p class="text-dark"><?php echo  $ticket['subject']; ?></p>
			</a>
		</li>
	<?php endforeach; ?>
	
	</ul>
	<?php endif; ?>
</div>