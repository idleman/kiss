<?php


//Cette fonction va generer une page quand on clique sur glpi dans menu
function glpi_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='glpi') return;
	$page = !isset($_['page']) ? 'list.ticket' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function glpi_install($id){
	if($id != 'fr.core.glpi') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function glpi_uninstall($id){
	if($id != 'fr.core.glpi') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("glpi",array('label'=>"Gestion des droits sur le plugin glpi"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html

require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function glpi_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('glpi','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.glpi',
		'icon' => 'fas fa-angle-right',
		'label' => 'Glpi'
	);
}

//Déclaration des pages de réglages
function glpi_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('glpi',array(
    "Général",
    'glpi_host' => array("label"=>"Hôte GLPI","type"=>"text"),
    'glpi_login' => array("label"=>"Identifiant GLPI","type"=>"text"),
    'glpi_password' => array("label"=>"Mot de passe GLPI","type"=>"password"),
));


//Affichage du/des widget(s)
function glpi_widget(&$widgets){
	
	require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'glpi';
	$modelWidget->title = 'Glpi';
	$modelWidget->icon = 'fas fa-headset';
	$modelWidget->background = '#1f72b8';
	$modelWidget->load = 'action.php?action=glpi_widget_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js'];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v='.time()];

	$modelWidget->configure = 'action.php?action=glpi_widget_glpi_configure';
	$modelWidget->configure_callback = 'glpi_widget_glpi_configure_save';

	$modelWidget->description = "Récupere les tickets glpi en cours par service ou par utilisateur";
	$widgets[] = $modelWidget;
}
  



//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "glpi_install");
Plugin::addHook("uninstall", "glpi_uninstall"); 


Plugin::addHook("page", "glpi_page");    
Plugin::addHook("menu_setting", "glpi_menu_setting");    
Plugin::addHook("content_setting", "glpi_content_setting");   
Plugin::addHook("widget", "glpi_widget");    

?>