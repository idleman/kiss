<?php
$service = empty( $widget->data('service')) ? '':  $widget->data('service');

$newHours = $widget->data('newHours');
$newHours = empty($newHours) || !is_numeric($newHours) ? 24: $newHours; 

?>

<div id="glpi-widget-glpi-form">
	<div>
		<label class="m-0">Url</label><br>
		
		<input class="form-control" type="text"  id="widget-glpi-service" value="<?php echo $service; ?>"/>
		<small class="text-muted" style="word-break: break-all;">Faites une recherche sur glpi et copier l'url dans ce champ,ex :<br> 
			<?php echo $conf->get('glpi_host'); ?>/front/ticket.php?is_deleted=0&as_map=0&criteria%5B0%5D%5Blink%5D=AND&criteria%5B0%5D%5Bfield%5D=76665&criteria%5B0%5D%5Bsearchtype%5D=equals&criteria%5B0%5D%5Bvalue%5D=5&search=Rechercher&itemtype=Ticket&start=0&_glpi_csrf_token=79beba10aa8cba3a03ea608f957b6a99</small>
		

		<label class="m-0">Ticket marqué comme nouveau si datant de moins de x heures</label><br>
		<input class="form-control" type="text"  id="widget-glpi-new-hours" value="<?php echo $newHours; ?>"/>
	</div>
</div> 