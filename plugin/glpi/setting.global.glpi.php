<?php
global $myUser,$conf;
User::check_access('glpi','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	<div onclick="glpi_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Glpi</h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('glpi'); ?>
    </div>
</div>
