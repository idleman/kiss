//CHARGEMENT DE LA PAGE
function init_plugin_glpi(){
	switch($.urlParam('page')){
		default:
		break;
	}
	
	
	$('#tickets').sortable_table({
		onSort : glpi_ticket_search
	});
}


//Enregistrement des configurations
function glpi_setting_save(){
	$.action({ 
		action: 'glpi_setting_save', 
		fields:  $('#glpi-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}


/** TICKET **/
//Récuperation d'une liste de ticket dans le tableau #tickets
function glpi_ticket_search(callback){
	var box = new FilterBox('#filters');
	$('#tickets').fill({
		action:'glpi_ticket_search',
		filters: box.filters(),
		sort: $('#tickets').sortable_table('get')
	},function(response){
		$('.results-count span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément ticket
function glpi_ticket_save(){
	var data = $('#ticket-form').toJson();
	$.action(data,function(r){
		
		$.message('success','Enregistré');
	});
}


//Suppression d'élement ticket
function glpi_ticket_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	
	$.action({
		action: 'glpi_ticket_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

