/* HTML */
function glpi_widget_glpi_configure_save(widget,modal){
	var data = $('#glpi-widget-glpi-form').toJson();
	data.action = 'glpi_widget_glpi_configure_save';
	data.id = modal.attr('data-widget');

	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}