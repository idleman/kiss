<?php

	//Sauvegarde des configurations de glpi
	Action::register('glpi_setting_save',function(&$response){
		global $myUser,$_,$conf;
		User::check_access('glpi','configure');

		//Si input file "multiple", possibilité de normlaiser le
		//tableau $_FILES récupéré avec la fonction => normalize_php_files();
		
		foreach(Configuration::setting('glpi') as $key=>$value){
			if(!is_array($value)) continue;
			$allowed[] = $key;
		}
		foreach ($_['fields'] as $key => $value) {
			if(in_array($key, $allowed))
				$conf->put($key,$value);
		}
	});
	

	Action::register('glpi_widget_load',function(&$response){
		global $myUser;
		require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');

		User::check_access('glpi','read');
		$widget = DashboardWidget::current();
		$widget->title = 'Widget Glpi';
		ob_start();
	
		require_once(__DIR__.SLASH.'widget.php');
		$widget->content = ob_get_clean();

		echo json_encode($widget);
		exit();
	});
	
	Action::register('glpi_widget_glpi_configure',function(&$response){
		global $myUser;
		User::check_access('glpi','configure');
		require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
		$widget = DashboardWidget::current();
		ob_start();
		require_once(__DIR__.SLASH.'widget.configure.php');
		$content = ob_get_clean();
		echo $content ;
		exit();
	});

	Action::register('glpi_widget_glpi_configure_save',function(&$response){
			global $myUser,$_;
			User::check_access('glpi','configure');
			require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
			$widget = DashboardWidget::getById($_['id']);
			$widget->data('service',$_['widget-glpi-service']);
			$widget->data('newHours',$_['widget-glpi-new-hours']);
			$widget->save();
	});

?>