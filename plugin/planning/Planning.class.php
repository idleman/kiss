<?php
/**
 * Define a planning.
 * @author Julien NARBONI
 * @category Plugin
 * @license MIT
 */
class Planning extends Entity{
	public $id,$owner,$type,$label,$slug,$color,$default;
	protected $TABLE_NAME = 'planning';
	const TEAMMATE_SLUG = 'teammate';
	public $fields =
	array(
		'id' => 'key',
		'label' => 'string',
		'slug' => 'string',
		'color' => 'string',
		'owner' => 'string',
		'default' => 'int',
		'type' => 'string'
	);

	

}
?>