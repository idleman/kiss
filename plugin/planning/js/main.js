var majpushed = false;
var selectedDay;
window.planning = null;



//CHARGEMENT DE LA PAGE
function init_plugin_planning(){
	switch($.urlParam('page')){
		default:
		break;
	}

	$(window).resize(function() {
		if(window.planning) window.planning.setOption('height', $(window).height()*0.83);
	});

	planning_set_type();

	if($('.planning-page').attr('data-event-editable')=='0')
		$('#planning-event-form').find('input,select,textarea').attr('readonly','readonly');
	

	$( "#planning-datepicker" ).datepicker({
		dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
		dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
		dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
		changeMonth: true,
        changeYear: true,
		monthNames: ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Décembre"],
		monthNamesShort: ["Jan","Fév","Mars","Avr","Mai","Juin","Juil","Aout","Sept","Oct","Nov","Déc"],
		onSelect : function(date,element){
			date = date.split('/');
			window.planning.gotoDate(date[2]+'-'+date[0]+'-'+date[1]);
		}
	});

	$('#planning-event-form #label').enter(function(){
		planning_event_save();
	});
	
	planning_search(function(){
		planning_calendar_init();
	});
	$(document).keydown(function(e){
		switch(e.keyCode){
			//Appuis sur maj
			case 16:
				majpushed = true;
			break;
			//Appuis sur suppr
			case 46:

				var group = $('.event-selected').attr('data-group');
				//si le group est vide on supprime uniquement l'event sélectionné
				if(!group) return planning_event_delete($('.prime-event-selected'));

				//Si un groupe existe on verifie que d'autres evenements sont liés
				$.action({
					action : 'planning_group_events_search',
					group : group
				},function(response){
					//Si d'autres évenements sont liés, on propose a l'utilisateur de les supprimer
					if(response.rows.length > 1){
						planning_instance(function(target){
							if(target == 'group'){
								planning_event_delete($('.event-selected'));
							}else{
								planning_event_delete($('.prime-event-selected'));
							}
						});
					//Si aucun évenement est lié, on supprime uniquement l'evenement courant
					}else{
						planning_event_delete($('.prime-event-selected'));
					}
				});

				
			break;
		}
	});
	//relachage du maj
	$(document).keyup(function(e){
		if(e.keyCode!=16) return;
		majpushed = false;
	});


}

function planning_instance(callback){
	$('#event-group-modal').modal('show');
	$('#event-group-modal .event-group-option li').off('click').click(function(){
		$('#event-group-modal').modal('hide');
		callback($(this).attr('data-target'));
	});
}

function init_setting_planning(){
	planning_event_type_search();


	window.onbeforeunload = function(e){
	  if(!planningMenuEdited) return e = null;
	  return "Certaines modifications sur le menu n'ont pas été sauvgardées.\nÊtes-vous sûr de vouloir quitter la page ?";
	};


	$( ".planning-types" ).sortable({
			handle: ".planning-type-handler",
			cancel: ".not-editable",
			helper: "clone",
	    	opacity: 0.8,
			distance : 15,
			grid: [ 50, 1 ],
	      	update : function(){
	      		planningMenuEdited = true;
	      	},
	      	start: function(e,ui) {
	      		draggedItem = $(ui.item[0]);
	      		if(draggedItem.hasClass('planning-line-child')) return;
	      		
	      		children = [];
	      		var borderReached = false;
	      		$.each(draggedItem.nextAll('li.planning-line'), function(i, childLine){
	      			var line = $(childLine);

	      			if(borderReached || !line.hasClass('planning-line-child')) {
	      				borderReached = true;
	      				return;
	      			}
	      			children.push(line);
					line.addClass('hidden');
	      		});
	      	},
			sort: function(event,ui){
				var leftLimit = ui.item.hasClass('planning-line-child') ? -20 : 15;
				if(ui.position.left < leftLimit){
					ui.helper.css('left',leftLimit+'px');
				}
				if(ui.position.left > 65){
					left = 65;
					ui.helper.css('left',left+'px');
				}
				if(ui.item.index()==1){
					ui.helper.css('left',leftLimit+'px');
				}
			},
			stop: function(event,ui){
				if(ui.position.left>15){
					if(!ui.item.hasClass('planning-line-child')) planningMenuEdited = true;
					ui.item.addClass('planning-line-child');
				}else{
					if(ui.item.hasClass('planning-line-child')) planningMenuEdited = true;
					ui.item.removeClass('planning-line-child');
				}

				if(!children.length) return;
				$.each(children.reverse(), function(i, row){
					$(row).insertAfter(draggedItem).removeClass('hidden');
				});
			},
		
			placeholder: {
				element: function(clone, ui) {
					var placeholderRow =  $('<li class="planning-item-placeholder"></li>');

					/** Lié à la feature de drag&drop de plusieurs lignes à la fois **/
					if(!clone.hasClass('planning-line-child')){
						var rowNb = 1;
						var borderReached = false;

			      		$.each(clone.nextAll('li.planning-line'), function(i, childLine){
			      			var line = $(childLine);

			      			if(borderReached || !line.hasClass('planning-line-child')) {
			      				borderReached = true;
			      				return;
			      			}
			      			rowNb += 1
			      		});

						placeholderRow.css({
							height: 'calc(44px * '+rowNb+')',
						});
					}
	                return placeholderRow;
	            },
	            update: function() {
	                return;
	            }
			}
	    }).disableSelection();




}


/** PLANNING **/

//Récuperation d'une liste de planning dans le tableau #plannings
function planning_search(callback){

	$.action({
		action:'planning_search'
	},function(r){
		var planning = $('#plannings');
		var sharedPlanning = $('#shared-plannings');

		planning.find('li:visible').remove();
		sharedPlanning.find('li:visible').remove();

		var tpl = $('li.hidden',planning).get(0).outerHTML;
		var options = '';
		for(var k in r.rows){
			var line = r.rows[k];
			options +='<option value="'+line.id+'">'+line.label+'</option>';

			var li = $(Mustache.render(tpl,line));
			if(line.selected) li.find('input').prop('checked',true);

			if(line.shared){
				sharedPlanning.append(li);
			}else{
				planning.append(li);
			}
			
			li.removeClass('hidden');
		}
		$('#event-planning').html(options);

		if(callback!=null) callback();
	});


}

//Ajout ou modification d'élément planning
function planning_save(){
	var data = $('#planning-form').toJson();
	data.action = 'planning_save';
	$.action(data,function(r){
		$.message('success','Enregistré');
		$('#planningModal').modal('hide');
		planning_search();
	});
}

//Récuperation ou edition d'élément planning
function planning_edit(element){
	var data = {};
	if(element)
		data.id = $(element).closest('li').attr('data-id');
	
	$('#planningModal .modal-content').load('action.php?action=planning_edit',data,function(){
		init_components('#planningModal .modal-content');
		planning_share_search();
	});
	$('#planningModal').modal('show');

}

function planning_set_type(){
	var type = $('#planning-event-type option:selected').val()
	var subInput = $('#planning-event-subtype');
	$.action({
		action : 'planning_subtype_search',
		type : type
	},function(r){
		$('option',subInput).remove();
		for(var k in r.rows){
			var line = r.rows[k];
			subInput.append('<option style="background-color:'+line.color+'" data-icon="'+line.icon+'" value="'+line.id+'">'+line.label+'</option>');
		}
		init_components(subInput.parent());
	});
}

//Suppression d'élement planning
function planning_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('li');
	$.action({
		action : 'planning_delete',
		id : line.attr('data-id')
	},function(r){
		line.remove();
		window.planning.refetchEvents();
		$.message('info','Élement supprimé');
	});
}

function planning_select(){
	window.planning.refetchEvents();
}



//Récuperation d'une liste de planning dans le tableau #plannings
function planning_calendar_init(callback){
	var plannings = [];
	var planningMenuEdited = false;
	var clickCnt = 0;
	var data = $('#planning').data();

   window.planning = new FullCalendar.Calendar($('#planning').get(0),{
   	locale:'fr',
      initialView: data.view,
      plugins: [ TeammateViewPlugin ],
      nextDayThreshold: '00:00:00',
      height: $(window).height()*0.83,
	  	navLinks: true, // can click day/week names to navigate views
	  	editable: false,
	  	businessHours : true,
	  	dayMaxEvents: true, // autorise le lien + d'evenement si trop d'evenements
	  	selectable: true,
	  	buttonText : {
			today:    'Aujourd\'hui',
		  	month:    'Mois',
		  	week:     'Semaine',
		  	day:      'Jour',
		  	list:     'Liste'
		},
	  	customButtons: {
		   refreshButton: {
		   	text: '',
		      click: function() {
					window.planning.refetchEvents();
		      }
		   },
		   teammateButton: {
	         text: 'Equipe',
	         click: function () {
	             window.planning.changeView('teammate');
	         }
	      }
	  	},
      headerToolbar: {
      	left: 'prev,next today refreshButton',
        	center: 'title',
        	//right: 'teammateButton,dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        	right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
      },

     	//lancé lorsque qu'une nouvelle vue est chargée
      viewDidMount: function(data) { 
			//force fullcalendar a recharger les evenements lors du changement de la vue (ie: la vue equipe n'affiche pas les même event que la vue mois)
			window.planning.refetchEvents();

			//ajout l'icone de refresh au boutton rafraichir
			$('.fc-refreshButton-button').html('<i class="fas fa-redo"></i>');		
	  	},

	 	//custom d'evenemment en virtual DOM
	 	eventContent: function(arg, createElement) {
	  		/*var innerText
	  		if (arg.event.extendedProps.isUrgent) {
	    		innerText = 'urgent event'
	  		} else {
	    		innerText = 'normal event'
	  		}

	  		return createElement('div', {}, innerText);*/
		},
	
		//modif pour chaque event après chargement
	 	eventDidMount: function(data) {
	      if(data.event.end == null || data.event.start == null) return;
			var event = data.event;
			var start = moment(event.start);
			var end = moment(event.end);
			var startDay = start.format("DD/MM/YYYY");
			var endDay = end.format("DD/MM/YYYY");
			var startSchedule = start.format("HH:mm");
			var endSchedule = end.format("HH:mm");
			var location = event.extendedProps.location == null ? '' : event.extendedProps.location;
			var description = event.extendedProps.description == null ? '' : event.extendedProps.description;
			var info = "Du "+startDay+" au "+endDay+"\r"+event.title+"\r"+'De '+startSchedule+' à '+endSchedule+"\r"+description+"\r"+location;
			
			var element = $(data.el);
			element.attr("title", info).attr('data-tooltip',event.id).attr('data-id',event.id);

			if(event.display=='background') return;
			element.find('.fc-event-title').append('<span class="fc-icon right"><i class="'+event.extendedProps.icon+'"></i></span>');
		
			if(event.extendedProps.underlineColor) element.css({
				'border-left':'4px solid '+event.extendedProps.underlineColor,
				'border-bottom-left-radius':'0',
				'border-top-left-radius':'0'
			});

			//selection de l'event dont l'id est en parametre event si existant
			if($.urlParam('event')){
				if(event.id==$.urlParam('event')) $('.fc-event[data-id="'+$.urlParam('event')+'"]').addClass('event-selected');
				$('#planning').addClass('event-selected-mode');
			}
			init_tooltips(element.parent());
      },

      //options spéciales pour chaques vues
	  	views : {
	  		//vue teammate
			teammate : {
				addTeammate : function(teammates){
					$.action({
						action : 'planning_teammate_save',
						teammates : teammates
					},function(response){
						window.planning.view.renderTeammates(response.teammates);

						window.planning.refetchEvents();
					});
				},
				sortTeammate : function(sort){
					$.action({
						action : 'planning_teammate_sort',
						sort : sort
					},function(response){
						
					});
				},
				highlight: '.fc-teammate-line[data-teammate="'+$('#planning').attr('data-user')+'"]',
				
				dateIncrement: { days: 7 },
				visibleRange: function(currentDate) {
					currentDate = moment(currentDate);
			        var startOfMonth = currentDate.clone().startOf('week');
			        var endOfMonth = currentDate.clone().endOf('week');
			      
			        return {
			          start: startOfMonth.clone(),
			          end: endOfMonth.clone()
			        };
				},
				teammates : JSON.parse($('.planning-teammates').html()),
			},
	 	},
     
     	//Récuperation ajax des évenements
     	events: {
      	url: 'action.php?action=planning_event_search',
      	//parametre ajax dynamic (planning sélectionné, equipiers etc...)
      	extraParams: function(){
        		parameters = { plannings : [] };

				var teammates = JSON.parse($('.planning-teammates').html());
				parameters.teammates = [];
				for(var i in teammates){
					parameters.teammates.push(teammates[i].login);
				}
			
				parameters.view = $('.fc-view-harness > div').attr('class');
				if(!parameters.view) return;
				parameters.view = $('.fc-view-harness > div').attr('class').match(/fc-([^-]*)-view/i);
				parameters.view = parameters.view.length>1 ? parameters.view[1] : '';

				$('.planning-list li:visible input:checked').each(function(i,input){
					parameters.plannings.push($(input).closest('li').attr('data-id'))
				});
            
            return parameters;
      	},
			failure: function() {
      		$('#script-warning').removeClass('hidden');
    		}
      },
	   
	   //Appellé lors début/fin de chargement du planning
	   loading: function(isLoading){
			if(isLoading){
				$('.fc-refreshButton-button').html($('.planning-loader').html());
			}else{
				$('.fc-refreshButton-button').html('<i class="fas fa-redo"></i>');
			}
		},
		
		dateClick: function(data) {
			$('prime-event-selected').removeClass('prime-event-selected');
			$('.event-selected').removeClass('event-selected');
			selectedDay = {
				element : $(this),
				view : data.view.type,
				date : moment(data.date)
			};

			if($('.planning-page').attr('data-event-editable')=='0') return;
			
			var calculatedEnd =  ''+parseInt(selectedDay.date.format("HH"))+1;

			var newEvent = {
				startDate : selectedDay.date.format("DD/MM/YYYY"),
				endDate : selectedDay.date.format("DD/MM/YYYY"),
				startHour : selectedDay.date.format("HH"),
				endHour :  "00".substring(0, 2 - calculatedEnd.length) + ''+ calculatedEnd,
				startMinut : selectedDay.date.format("mm"),
				endMinut : selectedDay.date.format("mm")
			};
			switch(selectedDay.view){
				case 'teammate':
					newEvent.user = selectedDay.element.closest('tr').attr('data-teammate');
					if(selectedDay.date.format("HH") == '00'){
						var startTime = $('#planning').attr('data-start-hour') ? $('#planning').attr('data-start-hour') : '09:00';
					}else{
						var startTime =  '12:00';
					}
					startTime = startTime.split(':');
					newEvent.startHour = startTime[0];
					newEvent.startMinut = startTime[1];
				break;
				case 'dayGridMonth':
					newEvent.startHour = '08';
					newEvent.endHour = '12';
					newEvent.startMinut = '00';
					newEvent.endMinut = '00';

					var planningConf = $('#planning').data();
					var defaultStart = planningConf.startHour.split(':');
					var defaultEnd = planningConf.endHour.split(':');
					

					if(defaultStart.length==2){
						newEvent.startHour = defaultStart[0];
						newEvent.startMinut = defaultStart[1];
					}
					if(defaultEnd.length==2){
						newEvent.endHour = defaultEnd[0];
						newEvent.endMinut = defaultEnd[1];
					}

				break;
			}
			var end = moment(newEvent.startDate+' '+newEvent.startHour+':'+newEvent.startMinut, 'DD/MM/YYYY HH:mm').add(2,'hour');
			newEvent.endHour = end.format('HH');
			newEvent.endMinut = end.format('mm');
			planning_event_edit(newEvent,$(data.dayEl));
			data.jsEvent.stopPropagation();

		},
		
		eventClick: function(data) {
			clickCnt++;
			//Simple clic, on sélectionne l'event
			if(clickCnt === 1){
			   oneClickTimer = setTimeout(function() {
			      clickCnt = 0;
			      if(!majpushed) $('.event-selected').removeClass('event-selected');
			      var target = $(data.el);

			      var group = target.attr('data-group');
			      target.addClass('prime-event-selected');
			      if(group != null && group != ''){
			      	target = $('[data-group="'+group+'"]');
			      }else{
			      	target = $('[data-id="'+target.attr('data-id')+'"]');
			      }

			      if(!$(data.el).hasClass('event-selected')){
			      	$('#planning').addClass('event-selected-mode');
			      	target.addClass('event-selected');
			      	target.addClass('prime-event-selected');
			      }else{
			      	$('#planning').removeClass('event-selected-mode');
			      	target.removeClass('event-selected');
			      	target.removeClass('prime-event-selected');
			      }			      			      
			   }, 400);
			//Double clic, on ouvre la modale
			}else if(clickCnt === 2){
			   clearTimeout(oneClickTimer);
			   clickCnt = 0;
			   $('prime-event-selected').removeClass('prime-event-selected');
			   $('.event-selected').removeClass('event-selected');
			   var start = moment(data.event.start);
			   var end = moment(data.event.end);

			   
			   //obligé de copier l'objet car définit comme non extensible par fullcalendar
			   var props = Object.assign({}, data.event.extendedProps, {selected:false});

			   var event = $.extend(props,{
			   	id : data.event.id,
			   	label : data.event.title,
			   	startDate : start.format("DD/MM/YYYY"),
			   	endDate : end.format("DD/MM/YYYY"),
			   	startHour : start.format("HH"),
			   	endHour :  end.format("HH"),
			   	startMinut : start.format("mm"),
			   	endMinut : end.format("mm")
			   });



			   if(data.view.type == 'teammate') event.user = $(this).closest('tr').attr("data-teammate");
			  	planning_event_edit(event,$(data.el).closest('.fc-day'));
			  	data.jsEvent.stopPropagation();
			}
		},

		/* eventMouseEnter: function(options){}, */
		eventDrop: function(data) {

			if($('.planning-page').attr('data-event-editable')=='0') return;
			if(data.event == null) return;
			if(data.event.end == null || data.event.start == null) return null;

			var event = planning_convert_event(data.event);
	
			$.action({
				action : 'planning_event_move',
				view : window.planning.view.type,
				event : event
			});
		},
		eventResize: function(data ) {
			if($('.planning-page').attr('data-event-editable')=='0') return;
			if(data.event == null) return;
			var event = planning_convert_event(data.event);
			planning_event_save(event,function(r){
				if(r.error )revertFunc();
			});
		}

    });

    window.planning.render();
  
	if($.urlParam('start'))
		window.planning.gotoDate($.urlParam('start'));

	$('#planning').click(function(){
		planning_panel_show(false);

		if(!majpushed){
			$('.event-selected').removeClass('event-selected');
			$('#planning').removeClass('event-selected-mode');
		}
	});

}

//verifie la coherence date/heure début/fin et corrige au besoin 
function planning_checkstart_end(){
	if(!$('#startDate').val()) return;
	var start = moment($('#startDate').val()+' '+$('#startTime').val(),'DD/MM/YYYY HH:mm');
	if(!$('#endDate').val()) $('#endDate').val(start.format('DD/MM/YYYY'));
	if(!$('#endTime').val()) $('#endTime').val( moment(start).add(1,'hour').format('HH:mm') );

	var end = moment($('#endDate').val()+' '+$('#endTime').val(),'DD/MM/YYYY HH:mm');
	if(end<start){
		var newEnd = moment(start).add(1,'hour');
		$('#endDate').val( newEnd.format('DD/MM/YYYY') );
		$('#endTime').val( newEnd.format('HH:mm') );
	}
}

//Convertis un evenements fc Calendar en evenement erp
function planning_convert_event(fcEvent){
	if(fcEvent.end == null || fcEvent.start == null) return null;
	var event = {};

	event.id = fcEvent.id;
	event.label = fcEvent.title;

	var start = moment(fcEvent.start);
	var end = moment(fcEvent.end);

	event.startDate = start.format("DD/MM/YYYY");
	event.endDate = end.format("DD/MM/YYYY");
	event.startHour = start.format("HH");
	event.endHour = end.format("HH");
	event.startMinut = start.format("mm");
	event.endMinut = end.format("mm");
	$.extend(event,fcEvent.extendedProps);
	return event;
}





/** PLANNING EVENT **/
	
//Ajout ou modification d'élément planningevent
function planning_event_save(data,callback){
	if($('.planning-page').attr('data-event-editable')=='0') return;
	if(!data){
		data = $('#planning-event-form').toJson();
		data.type = $('#planning-event-type').val();
		data.id = $('#planning-event-form').attr('data-event');
		data.planning = $('#event-planning').val();
		data.dailyFrequency = $('[name="dailyRepeatFrequency"]:checked').attr('data-value');
		data.repeatEndType = $('[name="repeatEndType"]:checked').attr('data-value');

		data.resources = planning_event_resource_get();

		var startTime = data.startTime.split(':');
		var endTime = data.endTime.split(':');
		data.startHour = startTime[0];
		data.startMinut = startTime[1];

		data.endHour = endTime[0];
		data.endMinut = endTime[1];

		data.weeklyDay = [];
		$('[data-type-form="weekly"] input[type="checkbox"]:checked').each(function(){
			data.weeklyDay.push($(this).attr('data-value'));
		});
	}
	data.view = window.planning.view.type;
	data.action = 'planning_event_save';


	$.action(data,function(r){
		$('#planning-event-form').attr('data-event','');

		

		planning_panel_show(false);

		

		window.planning.refetchEvents();

		if(callback) callback(r);
	},function(r){
		if(callback) callback(r);
	});
}

//gestion ouverture panel avec effet
function planning_panel_show(open){
	var panel = $('#planning-event-panel');
	if(window.panelTimeout) clearTimeout(window.panelTimeout); 
	if(window.panelChangeTimeout) clearTimeout(window.panelChangeTimeout); 
	if(open){
		window.panelChangeTimeout = setTimeout(function(){
			panel.removeClass('hidden');
			window.panelTimeout = setTimeout(function(){
				panel.removeClass('fold');
			},200);
		},50);
	}else{
		window.panelChangeTimeout = setTimeout(function(){
			panel.addClass('fold');
			window.panelTimeout = setTimeout(function(){
				$('#planning-event-panel').addClass('hidden');
			},200);
		},50);
	}
}

//Récuperation ou edition d'élément planningevent
function planning_event_edit(event,dayElement){
	var eventPanel = $('#planning-event-panel');
	
	eventPanel.draggable({ 
		handle: ".planning-event-panel-head",
		containment: ".module-planning", 
		scroll: false
	});
	

	var top = 0;
	var left = 0;

	//positionnement de la modale en fonction de la taille du viewport et de l'élement cliqué
	if(dayElement){
		var offset = dayElement.offset();
		var top = offset.top +1;
		var left = offset.left +1;
		
		var viewportWidth = $(window).outerWidth();
		var viewportHeight = $(window).outerHeight();

		var eventPanelHeight = eventPanel.outerHeight();
		var eventPanelWidth = eventPanel.outerWidth();

		if(top+eventPanelHeight > viewportHeight)
			top-= eventPanelHeight;
		
		if(left+eventPanelWidth > viewportWidth)
			left-= eventPanelWidth;
	}

	eventPanel.css({
		top : top+'px',
		left : left+'px'
	});


	eventPanel.removeClass('event-readonly');
	//supprime toutes les classes débutant par view-*
	eventPanel.removeClass (function (index, className) {
	    return (className.match (/(^|\s)view-\S+/g) || []).join(' ');
	});

	if(event.editable===false) eventPanel.addClass('event-readonly');

	eventPanel.addClass('view-'+window.planning.view.type);
	


	planning_panel_show(true);
	eventPanel.find('#label').focus();

	$('#event-advanced').addClass('hidden');

	if(event.editable===false){
		$('#planning-event-panel').find('input,textarea,select').prop('readonly',true).attr('readonly','readonly');
		$('#planning-event-panel select option').attr('disabled', true);
	}else{
		$('#planning-event-panel').find('input,textarea,select').prop('readonly',false).removeAttr('readonly');
		$('#planning-event-panel select option').removeAttr('disabled');
	}


	$('#planning-event-form').find('input,textarea').val('');
	$('#planning-event-form').fromJson(event);

	$('#planning-event-form #startTime').val(event.startHour+':'+event.startMinut);
	$('#planning-event-form #endTime').val(event.endHour+':'+event.endMinut);


	$('#event-repeat-type').val(event.repeatType).trigger('change');
	$('#event-repeat-occurences').val(event.repeatOccurence);
	$('#event-repeat-until').val(event.repeatUntil)
	$('#event-repeat-daily-number').val(event.repeatDailyNumber);
	$('#event-repeat-yearly-number').val(event.repeatYearlyNumber);
	$('#event-repeat-monthly-number').val(event.repeatMonthlyNumber);
	$('#event-repeat-yearly-month').val(event.repeatYearlyMonth);
	$('#event-repeat-monthly-day').val(event.repeatMonthlyDay);

	$('[data-type-form="weekly"] input[type="checkbox"]').each(function(){
		$(this).prop('checked',$.inArray($(this).attr('data-value'),event.repeatWeeklyDay) != -1);
	});

	$('[name="repeatEndType"][data-value="'+event.repeatEndType+'"]').prop('checked',true);
	



	$('#planning-event-type').val(event.type);


	var planning = '';
	var checkedPlannings = $('.planning-list li:visible input:checked');
		

	//déduction du planning par défaut ou du planning correspondant a l'event
	if(event.planning){
		planning = event.planning ;
	}else{

		$('#planning-event-type .dropdown-menu > a:eq(0)').trigger('click');

		//si un planning est apr defaut on le prends de base
		planning = $('.planning-list li[data-default="1"]').attr('data-id');
		//Si pas de planning par defaut on prend le premier
		if(!planning) planning = $('.planning-list li:eq(1)').attr('data-id');
		//si un seul planning est coché on prend celui la
		if(checkedPlannings.length==1) planning = checkedPlannings.closest('li').attr('data-id');
		
	}

	$('#event-planning').val(planning);
	
	$('#planning-event-form').attr('data-event',!event.id? '': event.id);


	init_components('#planning-event-form');
	if(!$('#event-repeat-type').val()) $('#event-repeat-type').val('never');
	planning_event_repeat_change();
	if(event.resources) planning_event_resource_load(event.resources);
}

//Suppression d'élement planningevent
function planning_event_delete (elements){
	if($('.planning-page').attr('data-event-editable')=='0') return;
	var events = [];
	elements.each(function(i,event){
		if($(event).attr('data-id') != null)
			events.push($(event).attr('data-id'));
	});
	if(events.length == 0) return;
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	$.action({
		action : 'planning_event_delete',
		events : events
	},function(r){
		window.planning.refetchEvents();
	});
}



/** PLANNING EVENT TYPE **/
	
//Récuperation d'une liste de planningeventtype dans le tableau #planningeventtypes
function planning_event_type_search(callback){
	
	$('.planning-types').fill({
		action:'planning_event_type_search'
	},function(){
		$('.planning-types .type-editable').each(function(){
			$(this).prop('checked',$(this).val()=="true");
		});
		init_components('.planning-types');
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément event_type
function planning_event_type_save(){
	var items = [];
	var lastParentIndex = 0;

	$('.planning-line').each(function(i,element){
		var line = $(element);
		if(isNaN(line.attr('data-id'))) return;
		var item = {};
		item.id = line.attr('data-id');
		item.label = line.find('.type-label').val();
		item.color = line.find('.type-color').val();
		item.icon = line.find('.type-icon').val();
		item.editable = line.find('.type-editable').prop('checked') ? 1 :0;


		if(line.hasClass('planning-line-child')){
			items[lastParentIndex].childs.push(item);
		}else{
			lastParentIndex = items.length;
			item.childs = [];
			items.push(item);
		}
	});

	$.action({ 
		action : 'planning_event_type_save', 
		items : items
	},function(){
		planningnMenuEdited = false;
		planning_event_type_search();
		$.message('success','Enregistré');
	});
}




function planning_event_type_add(){
	var tpl = $('.planning-types li:eq(0)').get(0).outerHTML;
	var defaultData = {
		label : 'Nouveau type',
		color : '#17a2b8',
		editable : true,
		icon : 'far fa-bookmark'
	};
	var line = $(Mustache.render(tpl,defaultData));
	$('.planning-types').append(line.removeClass('hidden'));

}
//Suppression d'élement event_type
function planning_event_type_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('li');
	line.remove();
}


function planning_event_repeat_change(){

	var repeatEventType = $('#event-repeat-type').val();
	var eventOption = $('#event-repeat-options');
	if(repeatEventType=='never'){
		eventOption.addClass('hidden');
	}else{
		$('[data-type-form]',eventOption).addClass('hidden');
		$('[data-type-form="'+repeatEventType+'"]',eventOption).removeClass('hidden');

		eventOption.removeClass('hidden');
	}

}

/** PLANNING EVENT RESOURCES **/
function planning_event_resource_get(){
	var resources = [];
	$('.event-resources li:not(:eq(0))').each(function(){
		resources.push($(this).find('select').val());
	});
	return resources;
}
function planning_event_resource_clear(){
	$('.event-resources li:not(:eq(0))').remove();
}
function planning_event_resource_load(rows){

	planning_event_resource_clear();
	for(var k in rows){
		var data = rows[k];

		planning_event_resource_add(data,true);
	}
	planning_event_resource_states();
}

function planning_event_resource_add(data,noState){
	if(!data) data = {};
	var tpl = $('.event-resources li:eq(0)').get(0).outerHTML;
	var line = $(tpl);
	
	$('.event-resources').append(line);
	

	line.removeClass('hidden').fromJson(data);

	line.find('select').change(function(){
		planning_event_resource_states();
	});

	if(data.resource && !noState) planning_event_resource_states();
}

function planning_event_resource_states(){
	var resources = [];
	$('#planning-event-form [data-id="resource"]:not(:eq(0))').each(function(){
		resources.push($(this).val());
	});

	var startTime = $('#startTime').val();
	var endTime = $('#endTime').val();
	


	if(resources.length==0 || !startTime || !endTime) return;

	

	startTime = startTime.split(':');
	endTime =  endTime.split(':');

	$('.event-resources li:not(:eq(0)) .resource-state i').attr('class',"fas fa-circle-notch fa-spin text-muted")
	$('.event-resources li:not(:eq(0)) .resource-state').attr('title','Vérification de la disponibilité');

	$.action({
		action : 'planning_event_resource_state',
		event : $('#planning-event-form').attr('data-event'),
		resources : resources,
		startDate : $('#startDate').val(),
		endDate : $('#endDate').val(),
		startHour : startTime[0],
		startMinut : startTime[1],
		endHour : endTime[0],
		endMinut : endTime[1],
	},function(response){


		$('#planning-event-form [data-id="resource"]:not(:eq(0))').each(function(){
			var select = $(this);
			var line = select.closest('li');

			var className = 'far fa-check-circle text-success';
			var label = 'Ressource disponible pour cette periode';
		
			if(response.rows[select.val()] && response.rows[select.val()].length>0){
				className = 'far fa-times-circle text-danger';
				label = 'Ressource réservée :';

				for(var k in response.rows[select.val()]){
					var busy = response.rows[select.val()][k];
					
					label += "<br> <i class='fas fa-user-circle'></i> "+busy.creator+" du "+moment.unix(busy.startDate).format("DD/MM/YYYY HH:mm")+" au "+moment.unix(busy.endDate).format("DD/MM/YYYY HH:mm");
				}
			}
			$('.resource-state i',line).attr('class',className);
			$('.resource-state',line).attr('title',label);
		});
		init_components('.event-resources .resource-state');
		
		
	});
}

/** PLANNINGSHARE **/
	
//Récuperation d'une liste de planningshare dans le tableau #planningshares
function planning_share_search(callback){
	
	var planning = $('#planning-form').attr('data-id');
	if(planning=='') return;
	
	$('#planning-shares').fill({
		action:'planning_share_search',
		planning : planning,
		
	},function(){
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément planningshare
function planning_share_save(){
	var data = $('#planning-share-form').toJson();
	data.action='planning_share_save';
	data.planning = $('#planning-form').attr('data-id');
	data.recipientEntity = $('#recipient').data('values')[0].entity;
	if(data.planning=='') return $.message('warning','Veuillez enregistrer une fois le planning avant de créer des partages');
	$.action(data,function(r){
		planning_share_search();
	});
}


//Suppression d'élement planningshare
function planning_share_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	$.action({
		action : 'planning_share_delete',
		id : line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Élement supprimé');
	});
}



//Enregistrement des configurations
function planning_setting_save(){
	$.action({ 
		action : 'planning_setting_save', 
		fields :  $('#planning-setting-form').toJson() 
	},function(){ $.message('info','Configuration enregistrée'); });
}
