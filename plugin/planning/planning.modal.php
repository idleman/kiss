<?php 

global $myUser;

require_once(__DIR__.SLASH.'Planning.class.php');
$planning = Planning::provide();
$planning->color = $planning->color!='' ? $planning->color : '#8bc34a';

$canEdit = $myUser->login == $planning->owner || $planning->id == 0 || $myUser->superadmin;
?>
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Réglages de calendrier</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
</div>

<div class="modal-body" id="planning-form" data-id="<?php echo  $planning->id; ?>">

	<div class="row">
		<div class="input-group  col-md-12">
			
			<div class="input-group-prepend">
				<span class="input-group-text">Titre</span>
			</div>
			<input type="text" id="planning-label"   value="<?php echo htmlspecialchars($planning->label); ?>" class="form-control" placeholder="Titre de mon calendrier...">
			<div class="input-group-prepend">
				<span class="input-group-text">Couleur</span>
			</div>
			
			<input type="text" data-type="color" id="planning-color" class="form-control" value="<?php echo $planning->color; ?>">
			
		</div>
	</div>
	
	
	<div class="row planning-share-block">
		<div class="col-md-12">
			<div onclick="$('#planning-advanced').toggleClass('hidden')" class="btn btn-link pl-0 text-uppercase"><i class="fas fa-plus"></i> Plus d'options</div>

			<div id="planning-advanced" class="hidden mt-3">
				<h6 class="text-uppercase"><i class="fas fa-share-square"></i> Partages</h6>
				<p>Partagez votre calendrier avec vos collaborateurs.</p>

				<div class="input-group <?php echo $canEdit?'':'hidden' ?>" id="planning-share-form">
					<div class="input-group-prepend">
						<span class="input-group-text">Partage avec</span>
					</div>
					<input type="text" data-type="user" data-types="user,rank" id="recipient"  class="form-control">
					<div class="input-group-prepend">
						<span class="input-group-text">
							<label class="mb-0 pointer user-select-none"><input id="read" type="checkbox" data-type="checkbox">Lecture</label>
						</span>
					</div>
					<div class="input-group-prepend">
						<span class="input-group-text">
							<label class="mb-0 pointer user-select-none"><input id="edit" type="checkbox" data-type="checkbox">Écriture</label>
						</span>
					</div>
					<div class="input-group-append">
						<span class="input-group-text btn" onclick="planning_share_save();">
							<i class="far fa-plus-square"></i>
						</span>
					</div>
				</div>

				<table id="planning-shares" class="table mt-2">
					<thead>
						<tr>
							<th scope="col">Avec</th>
							<th scope="col">Lecture</th>
							<th scope="col">Ecriture</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<tr data-id="{{id}}" class="hidden">
							<td>{{recipient}} <small class="text-muted">- {{recipientType}}</small></td>
							<td>{{#read}}<i class="fas fa-check text-success"></i>{{/read}}{{^read}}<i class="fas fa-ban text-danger"></i>{{/read}}</td>
							<td>{{#edit}}<i class="fas fa-check text-success"></i>{{/edit}}{{^edit}}<i class="fas fa-ban text-danger"></i>{{/edit}}</td>
							<td><i class="far fa-trash-alt pointer <?php echo $canEdit?'':'hidden' ?>" onclick="planning_share_delete(this);"></i></td>
						</tr>
					</tbody>
				</table>

			

				<h6 class=" mt-3"><i class="far fa-calendar text-uppercase"></i> Synchronisation</h6>
				<p>Pour synchroniser ce calendrier sur votre téléphone, entrez l'addresse suivante dans les paramètres CalDav.</p>
				<code><?php echo ROOT_URL; ?>/dav/planning/<?php echo $planning->owner ?>/<?php echo $planning->slug; ?>?u=<?php echo $myUser->login; ?>&p=VOTRE-MOT-DE-PASSE</code>

			</div>
		</div>
	</div>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
	<?php if($canEdit): ?>
	<button type="button" class="btn btn-primary" onclick="planning_save();"><i class="far fa-calendar-check"></i> Enregistrer</button>
	<?php endif; ?>
</div>