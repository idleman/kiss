<?php
/**
 * Define a Ressource planning
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class PlanningEventResource extends Entity{

	public $id;
	public $resource; //Ressource (Liste configurable)
	public $event; //Evenement (Nombre Entier)
	
	protected $TABLE_NAME = 'planning_event_resource';
	public $entityLabel = 'Ressource planning';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'resource' => array('type'=>'dictionary', 'label' => 'Ressource'),
		'event' => array('type'=>'integer', 'label' => 'Evenement','link'=>'plugin/planning/PlanningEvent.class.php')
	);



	//Colonnes indexées
	public $indexes = array();
	
}
?>