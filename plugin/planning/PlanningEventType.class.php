<?php
/**
 * Définit un type d'evenement planning
 * @author Valentin CARRUESCO
 * @category Plugin
 * @license MIT
 */
class PlanningEventType extends Entity{
	public $id,$label,$icon,$editable,$parent,$color,$slug,$sort,$state;
	protected $TABLE_NAME = 'planning_event_type';
	public $fields =
	array(
		'id' => 'key',
		'label' => 'string',
		'icon' => 'string',
		'editable' => 'int',
		'parent' => 'int',
		'color' => 'string',
		'state' => 'string',
		'sort' => 'int',
		'slug' => 'string'
	);

	function __construct(){
		parent::__construct();
		$this->state = self::ACTIVE;
		$this->parent = 0;
	}

	public static function getAll($columns = array(), $order = null, $limit = null, $selColumn = array('*'), $joins = 0){
		$types = PlanningEventType::loadAll($columns,$order,$limit,$selColumn,$joins);
		Plugin::callHook('planning_types',array(&$types,$columns,$order,$limit,$selColumn,$joins));
		
		return $types;
	}
}
?>