<div id="planning-event-panel" class="planning-event-panel fold hidden">
		<div id="planning-event-form" data-planning="">
			

			<div class="planning-event-panel-head">
				<i onclick="$('.planning-event-panel').addClass('fold');" class="fas fa-times pointer right"></i>
				<div class="planning-agenda-block">
					<span>Agenda</span>		
					<select  id="event-planning"></select>
				</div>


				<h1>Édition événement</h1>
			</div>

			<div class="planning-event-panel-body">
				<div class="row mb-2">
					<div class="col-md-12">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<span class="input-group-text">Type</span>
							</div>
							<div class="input-group-append">
								<select data-type="dropdown-select" id="planning-event-type" onchange="planning_set_type()">
										<?php foreach(PlanningEventType::getAll(array('parent'=>0,'state'=>PlanningEventType::ACTIVE),array('sort')) as $type): ?>
										<option style="background-color:<?php echo $type->color; ?>" data-icon="<?php echo $type->icon; ?>" value="<?php echo $type->id; ?>" ><?php echo $type->label; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="input-group-prepend planning-subtype">
								<span class="input-group-text">Sous Type</span>
							</div>
							<div class="input-group-append planning-subtype">
								<select data-type="dropdown-select" id="planning-event-subtype"></select>
							</div>
						</div>
					</div>

					
				</div>

				<div class="row  mb-2">
					<div class="input-group  input-group-sm col-md-12">
						<div class="input-group-prepend">
							<span class="input-group-text">Libellé</span>
						</div>
						<input type="text" id="label" autocomplete="new-password" class="form-control">
					</div>
				</div>

				<!-- start / end -->
				<div class="row  mb-2">
				
					<div class="col-md-6 startTime">
					
						
						<input type="text" id="startDate" class="form-control form-control-sm col-sm-6 d-inline-block "  onchange="planning_checkstart_end()" data-type="date" value="" maxlength="10" placeholder="jj/mm/aaaa" pattern="[0-9/]{10}">
						
						<input type="text"  id="startTime" data-step="15" data-type="hour" onchange="planning_checkstart_end()" class="form-control  form-control-sm col-sm-5 d-inline-block" value="13">
					</div>
					
					<div class="col-md-6">
						<input type="text"  id="endDate" class="form-control form-control-sm col-sm-6 d-inline-block col-sm-5 d-inline-block"  onchange="planning_checkstart_end()" data-type="date" maxlength="10" placeholder="jj/mm/aaaa" pattern="[0-9/]{10}" title="Format jj/mm/aaaa">
						
						<input type="text" id="endTime" data-step="15"  onchange="planning_checkstart_end()" data-type="hour"  class="form-control  form-control-sm col-sm-5 d-inline-block" value="18">
					</div>
					
				</div>

				
				<!-- notifications -->
				<div class="row mt-2">
					<div class="input-group  input-group-sm col-md-12">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="far fa-bell mr-1"></i> Rappel</span>
						</div>

						<input type="number" id="notificationNumber"  class="form-control">
						<select type="text" id="notificationUnity" class="form-control">
							<option value="minut">Minute</option>
							<option value="hour">Heure</option>
							<option value="day">Jour</option>
						</select>
						<div class="input-group-prepend">
							<span class="input-group-text"> Avant l'évenènement.</span>
						</div>
					</div>
				</div>

				<div class="row mt-2">
					<div class="col-md-12">
						<textarea  id="description" placeholder="Commentaire..." class="form-control w-100"></textarea>
					</div>
				</div>

				<div id="event-advanced" class="row hidden mt-2">
					<div class="col-md-12">
						<div class="row">
							<div class="input-group input-group-sm col-md-12">
								<div class="input-group-prepend">
									<span class="input-group-text">Lieu</span>
								</div>
								<div class="input-group-prepend">
									<input type="text" data-type="location" id="street" placeholder="Rue" class="form-control">
								</div>
								<input type="text" id="zip" placeholder="Code postal" class="form-control">
								<input type="text" id="city" placeholder="Ville" class="form-control">
							</div>
						</div>

						

						<div class="row  mt-2 teammate-row">
							<div class="input-group  input-group-sm col-md-12">
								<div class="input-group-prepend">
									<span class="input-group-text">Équipiers</span>
								</div>
								<input type="text" data-type="user" data-multiple id="user" class="form-control">
							</div>
						</div>

						<div class="row  mt-2 resources-row">
							<div class="input-group  input-group-sm col-md-12 ">

								<ul class="event-resources w-100 d-block">
									<li class="hidden mb-1">
										<div class="input-group input-group-sm">
											<div class="input-group-prepend"  style="position: relative;">
												<div class="btn resource-state"  title="Veuillez sélectionner une ressource" data-tooltip>
													<i class="far fa-question-circle text-muted"></i>
												</div>
											</div>
											<select class="form-control select-control" data-id="resource"  type="text" data-type="dictionary" data-slug="planning_event_resource" data-depth="1" key=data-disable-label data-value="" class="resource"></select>
											<div class="input-group-append">
												<div class="btn text-muted"><i onclick="$(this).closest('li').remove()" class="fas fa-times"></i></div>
											</div>
										</div>

									</li>
								</ul>

								<div class="btn text-muted pl-0" onclick="planning_event_resource_add()"><i class="fas fa-video mr-2"></i> <span class="font-weight-bold">Ajouter une ressource</span></div>
							</div>
						</div>

						<!-- récurrence a terminer coté fullcalendar / dav et a decommenter-->
						 <div  class="row mt-2">
							<div class="col-md-12">
								<div class="input-group  input-group-sm">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-redo-alt mr-1"></i> Répéter l'évenement </span>
									</div>						
									<select type="text" onchange="planning_event_repeat_change();" id="event-repeat-type" class="form-control">
										<option value="never" selected="selected">Jamais</option>
										<option value="daily">Quotidiennement</option>
										<option value="weekly">Hebdomadairement</option>
										<option value="monthly">Mensuelement</option>
										<option value="yearly">Annuellement</option>
									</select>
									
								</div>
							</div>
						</div> 
						
						<div id="event-repeat-options" class="hidden">
							<div class="row mt-2 hidden justify-content-md-center" data-type-form="daily">
								<div class="col-md-3 form-inline">
									<label><input type="radio" data-value="allDays" checked="checked" name="dailyRepeatFrequency" onclick="$('#event-repeat-daily-number').val('')" class="mr-1"> Tous les jours</label>
								</div>
								<div class="col-md-2 text-muted pt-2 font-weight-bold text-uppercase">Ou</div>
								<div class="col-md-4 input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><label class="m-0"><input  name="dailyRepeatFrequency" onclick="$('#event-repeat-daily-number').focus()"  data-value="selectDays" type="radio">  Tous les </label></span>
									</div>
									<input type="text"  onclick="$('input[data-value=\'selectDays\']').click()"  id="event-repeat-daily-number"  class="form-control">
									<div class="input-group-append">
										<span class="input-group-text"> Jours </span>
									</div>
								</div>
							</div>

							<div class="row mt-2 hidden justify-content-md-center" data-type-form="weekly">
								<div class="col-md-12 form-inline">
									<label>Jour de la semaine</label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="1" checked="checked">  Lundi </label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="2" checked="checked">  Mardi </label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="3" checked="checked">  Mercredi </label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="4" checked="checked">  Jeudi </label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="5" checked="checked">  Vendredi </label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="6">  Samedi </label>
									<label class="m-1"><input data-type="checkbox" type="checkbox" data-value="7">  Dimanche </label>
								</div>
							</div>

							<div class="row mt-2 hidden justify-content-md-center" data-type-form="monthly">
								<div class="col-md-6 input-group  input-group-sm">
									<div class="input-group-prepend">
										<span class="input-group-text">Le</span>
									</div>
									<input type="number" id="event-repeat-monthly-number" placeholder="1,2,3,4..." class="form-control">
									<select class="form-control" id="event-repeat-monthly-day">
										<option value="1">Lundi</option>
										<option value="2">Mardi</option>
										<option value="3">Mercredi</option>
										<option value="4">Jeudi</option>
										<option value="5">Vendredi</option>
										<option value="6">Samedi</option>
										<option value="7">Dimanche</option>
									</select>
									<div class="input-group-append">
										<span class="input-group-text">du mois</span>
									</div>
								</div>
							</div>

							<div class="row mt-2 hidden justify-content-md-center" data-type-form="yearly">
								<div class="col-md-6 input-group  input-group-sm">
									<div class="input-group-prepend">
										<span class="input-group-text">Le</span>
									</div>
									<input type="number" placeholder="1,2,3,4..." id="event-repeat-yearly-number" class="form-control">
									<select class="form-control" id="event-repeat-yearly-month">
										<option value="1">Janvier</option>
										<option value="2">Février</option>
										<option value="3">Mars</option>
										<option value="4">Avril</option>
										<option value="5">Mai</option>
										<option value="6">Juin</option>
										<option value="7">Juillet</option>
										<option value="8">Aout</option>
										<option value="9">Septembre</option>
										<option value="10">Octobre</option>
										<option value="11">Novembre</option>
										<option value="12">Décembre</option>
									</select>
								</div>
							</div>

							<div class="row mt-2">
								<div class="col-md-4 input-group  input-group-sm">
									<span class="input-group-text w-100"><label class="m-0 w-100"><input data-value="allTime" checked="checked" name="repeatEndType" onclick="$('#event-repeat-until,#event-repeat-occurences').val('')" type="radio"> Tout le temps </label></span>
								</div>

								<div class="col-md-4 input-group  input-group-sm">
									<div class="input-group-prepend">
										<span class="input-group-text"> <label class="m-0"><input data-value="until" name="repeatEndType" type="radio" onclick="$('#event-repeat-occurences').val('')">  Jusqu'au </label></span>
										<input type="text"  onclick="$('input[data-value=\'until\']').click()"  id="event-repeat-until" data-type="date"  class="form-control">
									</div>
								</div>

								<div class="col-md-4 input-group  input-group-sm">
									<div class="input-group-prepend">
										<span class="input-group-text"> <label class="m-0"><input data-value="occurences" name="repeatEndType" onclick="$('#event-repeat-until').val('')" type="radio"> Répéter </label></span>
										<input type="number" onclick="$('input[data-value=\'occurences\']').click()"  id="event-repeat-occurences" class="form-control">
										<span class="input-group-text"> fois </span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				
			
			</div>
			<div class="planning-event-panel-footer">
				<div onclick="$('#event-advanced').toggleClass('hidden')" class="btn btn-light text-muted font-weight-bold btn-small">Autres options</div>
				<div class="btn btn-primary  btn-save btn-save-event btn-small" onclick="planning_event_save()"><i class="fa fa-check"></i> Enregistrer</div>
			</div>


		</div>
	</div>