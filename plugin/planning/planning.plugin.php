<?php
/*
@name Planning
@author Julien NARBONI <julien.narboni@core.fr>

@licence Copyright
@version 1.0.0
@description Gestion du planning
*/

//Déclaration d'un item de menu dans le menu principal
function planning_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('planning','read')) return;
	$menuItems[] = array(
		'sort'=>5,
		'url'=>'index.php?module=planning',
		'label'=>'Planning',
		'icon'=> 'fas fa-calendar-alt',
		'color'=> '#9b59b6'
	);
}

//Cette fonction va generer une page quand on clique sur planning dans menu
function planning_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='planning') return;
	User::check_access('planning','read');
	$page = !isset($_['page']) ? 'list' : $_['page'];
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function planning_install($id){
	if($id != 'fr.core.planning') return;
	Entity::install(__DIR__);

	$type  = new PlanningEventType();
	$type->editable = 0;
	$type->label = 'Rendez vous';
	$type->slug = 'rendez-vous';
	$type->color = '#8bc34a';
	$type->icon = 'far fa-calendar-check';
	$type->save();

	global $conf;
	$conf->put('planning_day_start','09:00');
	$conf->put('planning_day_end','17:00');
	$conf->put('planning_show_default',true);
	$conf->put('planning_allow_event_edit',true);
	$conf->put('planning_allow_share',true);

	$dictionary = new Dictionary();
	$dictionary->slug = 'planning_event_resource';
	if(Dictionary::rowCount(array('slug'=>$dictionary->slug)) ==0){
    	$dictionary->label = 'Planning : Ressource';
    	$dictionary->parent = 0;
    	$dictionary->state = Dictionary::ACTIVE;
    	$dictionary->save();
    }

}

//Fonction executée lors de la désactivation du plugin
function planning_uninstall($id){
	if($id != 'fr.core.planning') return;
	Entity::uninstall(__DIR__);
	$dictionary = Dictionary::bySlug('planning_event_resource');
	if($dictionary!= false && $dictionary->id!=0){
		Dictionary::delete(array('parent'=>$dictionary->id));
		Dictionary::delete(array('id'=>$dictionary->id));
	}
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('planning',array('label'=>'Gestion des droits sur le plugin planning'));


//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function planning_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('planning','configure'))  return;
		$settingMenu[]= array(
			'sort' =>1,
			'url' => 'setting.php?section=planning',
			'icon' => 'fas fa-angle-right',
			'label' => 'Planning'
		);
	
}

//Déclaration des pages de réglages
function planning_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

function planning_save_event(&$data){
	require_once(__DIR__.SLASH.'PlanningEvent.class.php');
	
	$event = is_null($data['id']) ? new PlanningEvent() : PlanningEvent::getById($data['id']);
	foreach($data as $key=>$value)
		$event->$key = $value;
	
	$event->save();

	$data['id'] = $event->id;
}

function planning_delete_event($id){
	require_once(__DIR__.SLASH.'PlanningEvent.class.php');
	PlanningEvent::deleteById($id);
}

function planning_dav($command){
	
	if(substr($command, 0,12)!='dav/planning') return;
	
	require_once('common.php');

	global $conf,$myUser,$_;

	if(!$myUser || $myUser->login==''){
		foreach(explode('&',parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY)) as $parameter){
			$infos = explode('=',$parameter);
			$_[$infos[0]] = $infos[1];
		}

			if(!isset($_['u']) ){
				header('HTTP/1.0 401 Unauthorized');
			    exit;
			}
			$myUser = User::check($_['u'],$_['p']);
		
			if(!$myUser->connected()){
			    header('HTTP/1.0 403 Unauthorized');
			    echo 'Bad login';
			    exit;
			}
	
			global $myFirm;
			if($myUser->superadmin == 1){
				foreach(Firm::loadAll() as $firm)
					$firms[$firm->id] = $firm;
				$myUser->setFirms($firms);
			}
	
			if(is_numeric($myUser->preference('default_firm')) && $myUser->haveFirm($myUser->preference('default_firm'))){
				$_SESSION['firm'] = serialize(Firm::getById($myUser->preference('default_firm')));
			} else if(count($myUser->firms)!=0){
				$_SESSION['firm'] = serialize(reset($myUser->firms));
			} else {
				throw new Exception('Ce compte n\'est actif sur aucun établissement, veuillez contacter l\'administrateur');
			}
			$myFirm = isset($_SESSION['firm']) ? unserialize($_SESSION['firm']) : new Firm();
			$_SESSION['currentUser'] = serialize($myUser);
	}

	require_once(__DIR__.SLASH.'CalDavServer.class.php');

	$projectPath = preg_replace('|https?\:\/\/'.$_SERVER['REMOTE_ADDR'].'|i', '', ROOT_URL);
	$server = new CalDavServer();
	$server->root = $projectPath.'/dav/planning/';
	

	$server->calendarLastUpdate = function($user,$calendar){
		global $myUser,$myFirm;
		require_once(__DIR__.SLASH.'Planning.class.php');
		require_once(__DIR__.SLASH.'PlanningEvent.class.php');
		require_once(__DIR__.SLASH.'PlanningEventType.class.php');
		$planning = Planning::load(array('slug'=>$calendar,'owner'=>$user));
		$lastUpdated = PlanningEvent::load(array(),array('updated DESC'));
		return !$lastUpdated ? 0 : $lastUpdated->updated;
	};
	
	$server->searchEvents = function($user,$calendar){
		global $myUser,$myFirm;
		require_once(__DIR__.SLASH.'Planning.class.php');
		require_once(__DIR__.SLASH.'PlanningEvent.class.php');
		require_once(__DIR__.SLASH.'PlanningEventType.class.php');
		$events = array();
		$planning = Planning::load(array('slug'=>$calendar,'owner'=>$user));
		if(!$planning) return $events;
		$userObject = User::byLogin($user);
		
		foreach(PlanningEvent::getAll($user,$userObject->ranks[$myFirm->id],array($planning->id),null,null) as $event){
			$row = $event->toArray();
			$row['type'] = $row['type']->label;
			$row['label'] = htmlspecialchars(html_entity_decode($row['label'],ENT_QUOTES,'UTF-8'));
			$row['description'] =htmlspecialchars(html_entity_decode($row['description'],ENT_QUOTES,'UTF-8'));
			$row['type'] =htmlspecialchars(html_entity_decode($row['type'],ENT_QUOTES,'UTF-8'));
			$row['street'] =htmlspecialchars(html_entity_decode($row['street'],ENT_QUOTES,'UTF-8'));
			$row['city'] =htmlspecialchars(html_entity_decode($row['city'],ENT_QUOTES,'UTF-8'));


			$events[] = $row;
		}
		return $events;
	};
	
	$server->deleteEvent = function($user,$calendar,$event = null){
		global $myUser;
		require_once(__DIR__.SLASH.'PlanningEvent.class.php');
		require_once(__DIR__.SLASH.'PlanningEventType.class.php');
		$errors = PlanningEvent::removeAll($myUser->login,array($event));
		if(count($errors)>0) throw new Exception("Error Processing Request");
		
	};

	$server->saveEvent = function($user,$calendar,$event = null,$infos){
		global $myUser;
		require_once(__DIR__.SLASH.'Planning.class.php');
		require_once(__DIR__.SLASH.'PlanningEvent.class.php');
		require_once(__DIR__.SLASH.'PlanningEventType.class.php');
		$event = new PlanningEvent();
		if(strpos($infos->uid, '-')===false){
			$event = PlanningEvent::getById($infos->uid);
			$event = !$event ? new PlanningEvent():$event;
		}
		$planning = Planning::load(array('slug'=>$calendar,'owner'=>$user));
		if($planning->owner != $myUser->login){
			if(PlanningShare::rowCount(array('planning'=>$planning->id,'recipient'=>$myUser->login,'edit'=>1))==0) 
			throw new Exception("Vous n'avez pas la permission d'éditer cet évenement");
		}

		$event->label = $infos->title;
		$event->planning = $planning->id;
		$event->startDate = $infos->start;
		$event->endDate = $infos->end;
		$event->street = $infos->location;
		$event->description = nl2br($infos->description);
		$event->save();
	};



	if($conf->get('planning_dav_log')=='1'){
		$logDirectory = File::dir().SLASH.'planning'.SLASH.'logs';
		if(!file_exists($logDirectory)) mkdir($logDirectory,0755,true);
		$server->log = function($type,$message,$isVerbose = false){
			
			file_put_contents($logDirectory.SLASH.'logs.txt','['.$type.'] : '.$message.PHP_EOL,FILE_APPEND);
			if(!$isVerbose) Log::put($message,'[CALDAV]['.$type.']');
			
		};
	}
		

	$server->start();
}



function planning_cron(){

	require_once(__DIR__.SLASH.'Planning.class.php');
	require_once(__DIR__.SLASH.'PlanningEvent.class.php');
	require_once(__DIR__.SLASH.'PlanningEventType.class.php');

	$query  = 'SELECT e.*,t.label AS '.PlanningEventType::tableName().'_join_label FROM {{table}} e LEFT JOIN '.PlanningEventType::tableName().' t ON e.type=t.id WHERE notificationNumber!=? AND notificationState!=?';
	$data = array(0,PlanningEvent::NOTIFIED);
	$now = new DateTime();

	foreach(PlanningEvent::staticQuery($query,$data,true,1) as $event){
		$unity = 60 ;

		if($event->notificationUnity=='hour') $unity = 3600;
		if($event->notificationUnity=='day') $unity = 86400;
		$notificationDelay = $event->notificationNumber * $unity;
		
		$notificationDate = new DateTime();

		$notificationDate->setTimestamp($event->startDate - $notificationDelay);

		if($notificationDate > $now) continue; 

		$type = $event->join('type');
		
		$html= 'Du <strong>'.date('d/m/Y H:i',$event->startDate).'</strong> au <strong>'.date('d/m/Y H:i',$event->endDate).'</strong>';
		if($event->street!='') $html.= ' au '.$event->street;	
		$html.= '<br/><a class="btn btn-primary" href="index.php?module=planning&start='.date('Ymd',$event->startDate).'&event='.$event->id.'">Voir le rendez vous</a>';	
		
		$recipients = array($event->creator);
		if($event->creator!=$event->updater) $recipients[] = $event->updater;

		// GESTION ENVOI NOTIFICATION
		Plugin::callHook('emit_notification',array(array(
				'label' => $type->label.' : '.$event->label.' '.strtolower(relative_time($event->startDate)),
				'html' => $html,
				'type' => 'notice',
				'meta' => array('link' => ROOT_URL.'/index.php?module=planning&start='.date('Ymd',$event->startDate).'&event='.$event->id),
				'end' => strtotime('+2day'),
				'recipients' => array_unique($recipients) // recipients contient login
			)
		));

		$event->notificationState = PlanningEvent::NOTIFIED;
		$event->save();

		
	}

}

function planning_widget(&$widgets){
	require_once(__DIR__.SLASH.'..'.SLASH.'dashboard'.SLASH.'DashboardWidget.class.php');
	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'planning';
	$modelWidget->title = 'Planning';
	$modelWidget->icon = 'far fa-calendar-alt';
	$modelWidget->background = '#ff7979';
	$modelWidget->load = 'action.php?action=planning_widget_load';
	$modelWidget->css = [Plugin::url().'/css/widget.css?v=2'];
	$modelWidget->description = "Affiche vos 10 prochains rendez vous";
	$widgets[] = $modelWidget;
}



//Déclaration des settings de base
Configuration::setting('planning',array(
    "Configuration générale",
    'planning_day_start' => array("label"=>"Heure de début de journée","legend"=>"Heure ouvrée pour votre établissement au format HH:mm","type"=>"hour", "placeholder"=>"08:00"),
    'planning_day_end' => array("label"=>"Heure de fin de journée","legend"=>"Heure ouvrée pour votre établissement au format HH:mm","type"=>"hour", "placeholder"=>"17:00"),
    'planning_default_view' => array("label"=>"Vue par défaut","type"=>"list", "values"=>array(
    	'teammate'=>'Vue equipe',
    	'dayGridMonth'=>'Vue mois',
    	'timeGridWeek'=>'Vue semaine',
    	'timeGridDay'=>'Vue jour',
    	'listMonth'=>'Vue liste'
    ) 
	),
    'planning_allow_event_edit' => array("label"=>"Autoriser l'édition d'évenement","type"=>"boolean"),
    'planning_show_default' => array("label"=>"Afficher le planning par défaut (général)","type"=>"boolean"),
    'planning_allow_share' => array("label"=>"Autoriser le partage de calendrier","type"=>"boolean"),
     "CalDAV",
    'planning_dav_log' => array("label"=>"Activer les logs caldav","legend"=>"Les logs caldav peuvent consommer de la performance","type"=>"boolean")
));




//Déclation des assets
Plugin::addCss("/css/fullcalendar.min.css?v=1");


Plugin::addJs("/js/rrule.min.js?v=1");
Plugin::addJs("/js/fullcalendar.min.js?v=1");
Plugin::addJs("/js/fullcalendar.kiss.js?v=1");
Plugin::addJs("/js/rrule.connector.min.js?v=1");
Plugin::addJs("/js/locale-all.min.js");
Plugin::addJs("/js/main.js?v=2");

Plugin::addCss("/css/fullcalendar.kiss.css?v=2");
Plugin::addCss("/css/main.css?v=2");

//Mapping hook / fonctions
Plugin::addHook("install", "planning_install");
Plugin::addHook("uninstall", "planning_uninstall");


Plugin::addHook("menu_main", "planning_menu");
Plugin::addHook("page", "planning_page");   
Plugin::addHook("menu_setting", "planning_menu_setting");    
Plugin::addHook("content_setting", "planning_content_setting");
Plugin::addHook("save_planning_event", "planning_save_event");
Plugin::addHook("delete_planning_event", "planning_delete_event");
Plugin::addHook("rewrite", "planning_dav");
Plugin::addHook("cron", "planning_cron");
Plugin::addHook("widget", "planning_widget");

/* EXEMPLE EVENT CUSTOM HOOK
Plugin::addHook("planning_event_search", function(&$events){
	$events[]  = array(
					'id'						=> '148',
					'title' 					=> 'hello world',
					'type' 					=> 2,
					'planning' 				=> '',
					'planningOwner' 				=> 'admin',
					'planningDefault' 				=> 1,
					'allDay' 				=> false,
					'street' 				=> '04 pl de la rotonde',
					'group' 				=> '123456',
					'city' 					=> 'Pessac',
					'zip' 					=> '33600',
					'notificationNumber' => 0,
					'notificationUnity' 	=> 0,
					'start' 					=> date('Y-m-d\TH:i:s',time()),
					'end' 					=> date('Y-m-d\TH:i:s',time()+3600),
					'backgroundColor'		=> 'lime',
					'typeColor'				=> 'lime',
					'borderColor'			=> 'transparent',
					'editable'				=> false,
					'icon'					=> 'fas fa-user',
					'textColor'				=> 'white',
					'description'			=> 'test',
					'location'				=> 'pessac'
	);
});
*/
?>