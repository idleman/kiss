<?php 
global $myUser;
require_once(__DIR__.SLASH.'Planning.class.php');
require_once(__DIR__.SLASH.'PlanningEvent.class.php');
require_once(__DIR__.SLASH.'PlanningShare.class.php');
require_once(__DIR__.SLASH.'PlanningEventType.class.php');

$query ='SELECT * FROM {{table}} WHERE planning IN(SELECT id FROM '.Planning::tableName().' p WHERE p.owner=? AND p.default=1 ) and startDate >=? ORDER BY startDate LIMIT 10';
$results = PlanningEvent::staticQuery($query,array($myUser->login, mktime(0,0,0) ),true);

$days = array();

foreach ($results as $result) {
	$days[date('dmY',$result->startDate)][] = $result;
}
?>
<div class="planningContainer">
	<div class="planningHeader"></div>
	<ul class="planning-widget-list">
		<?php 
		if(count($days)!=0):
			foreach($days as $day=>$events): 
				if(count($events)==0) continue;
				?>
				<li class="day"><i class="far fa-calendar-alt"></i> <?php echo day_name(date('N',$events[0]->startDate)).' <span>'.date('d',$events[0]->startDate).'</span> '.month_name(date('m',$events[0]->startDate)).' '.date('Y',$events[0]->startDate); ?></li>
				<?php foreach($events as $event): 

					$excerpt = '';
					if($event->description!='') $excerpt =  '<i class="far fa-comment"></i> '.$event->description;
					if($event->address()!='') $excerpt =  '<i class="fas fa-location-arrow"></i> '.$event->address();
					?>
					<li class="event"><a href="index.php?module=planning&start=<?php echo date('Ymd',$event->startDate); ?>&event=<?php echo $event->id; ?>"><span><?php echo date('H:i',$event->startDate); ?></span> <?php echo $event->label; ?></a> <small class="text-muted ml-2"><?php echo $excerpt; ?></small></li>
				<?php endforeach; ?>
			<?php endforeach; 
		else: ?>
			<li class="day">Aucun rendez vous à venir</li>
  <?php endif; ?>
		</ul>
		<a href="index.php?module=planning" class="btn btn-light block m-2" ><i class="far fa-calendar"></i> Voir le planning</a>

	</div>