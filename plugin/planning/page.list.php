<?php
global $myUser, $conf;
User::check_access('planning','read');
require_once(__DIR__.SLASH.'Planning.class.php');
require_once(__DIR__.SLASH.'PlanningEventType.class.php');

?>
<div class="planning-page" 
	data-planning-share="<?php echo $conf->get('planning_allow_share')==true ? 1:0; ?>" 
	data-event-editable="<?php echo $conf->get('planning_allow_event_edit')==true ? 1:0; ?>"
>
	<div class="row " >
		<!-- search results -->
		<div class="col-md-12"><div class="planning-loader hidden"><i class="fas fa-circle-notch fa-spin"></i></div></div>
		<div class='planning-title' data-id='1'></div>
		<div class="col-xl-12 clo-md-12 planning-view">    	
			<div id='planning-calendars'>
				<div id="planning-datepicker" class="shadow-sm"></div>
				<?php if($myUser->can('planning', 'edit')): ?>
					<div class="btn btn-primary mt-3 w-100" onclick="planning_edit();"><i class="far fa-calendar-plus"></i> Ajouter un agenda</div>
				<?php endif; ?>
				<hr>
				<h4 class="pointer user-select-none" onclick="$('#plannings').slideToggle(150); $(this).find('>i').toggleClass('fa-angle-right')">Mes agendas <i class="right pointer fas fa-angle-down"></i></h4>
				<ul id="plannings" class="planning-list user-select-none">
					<li class="hidden" data-id="{{id}}" data-default="{{default}}">
						<label>
							<div class="planning-box" style="background-color:{{color}}"><input type="checkbox"  onclick="planning_select()" data-type="checkbox"></div> 
							<div class="d-inline-block">
								<div>{{{label}}}</div>
								<div class="planning-owner text-muted">{{ownerName}}</div>
							</div>
						</label>
						<div class="planning-option-btn">
							<?php if($myUser->can('planning', 'edit')): ?>
								<i onclick="planning_edit(this)" title="Configurer" class="fas fa-cog"></i>
							<?php endif; ?>
							<?php if($myUser->can('planning', 'delete')): ?>
								{{#editable}}
								<i onclick="planning_delete(this)" title="Supprimer" class="far fa-trash-alt"></i>
								{{/editable}}
							<?php endif; ?>
						</div>
					</li>
				</ul>
				<div class="shared-plannings-block">
					<h4 class="pointer user-select-none" onclick="$('#shared-plannings').slideToggle(150); $(this).find('>i').toggleClass('fa-angle-right')">Agendas partagés <i class="right pointer fas fa-angle-down"></i></h4>
					<ul id="shared-plannings" class="planning-list"></ul>
				</div>

			</div>
			<div id='planning' style="width:100%!important"
				data-user="<?php echo $myUser->login; ?>" 
				data-start-hour="<?php echo $conf->get('planning_day_start'); ?>" 
				data-end-hour="<?php echo $conf->get('planning_day_end'); ?>"
				data-view="<?php echo empty($conf->get('planning_default_view'))? 'dayGridMonth': $conf->get('planning_default_view'); ?>"
			></div>

		</div>
	</div>

	<?php require_once(__DIR__.SLASH.'event.modal.php'); ?>


	<!-- Edition de calendrier -->
	<div class="modal fade" id="planningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">

			</div>
		</div>
	</div>

	<!-- teammate view -->
	<div class="planning-teammate-view hidden">
		<table class="fc-teammate">
			<thead class="fc-head fc-widget-header fc-head-container">
				<tr>
					<th>Equipe</th>
				</tr>
				
			
			</thead>
			<tbody class="fc-body fc-widget-content">
				
			</tbody>
			<tfoot class="fc-footer">
				<tr>
					<td><div class="btn btn-light btn-teammate-add w-100"><i class="fas fa-user-plus"></i> Equipiers</div></td>
					<td colspan="7"></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<!-- Modal equipiers -->
	<div class="modal fade" id="teammate-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Affichage des équipiers</h5>

				</div>
				<div class="modal-body">
					<div class="input-group  col-md-12">
						<label><input type="checkbox" data-type="checkbox" onclick="$('.teammate-picker input').trigger('click');"><small class="text-muted pointer">Tout (dé)cocher</small></label>


						<div class="teammate-picker">
							<ul>
								<?php foreach (User::getAll() as $user): 
									$user->password= '';
									?>
									<li><label><input type="checkbox" data-type="checkbox" data-login="<?php echo $user->login; ?>" data-name="<?php echo $user->name; ?>" data-firstname="<?php echo $user->firstname; ?>" data-avatar="<?php echo $user->getAvatar(); ?>"><?php echo $user->fullName(); ?></label></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					<button type="button" class="btn btn-primary btn-save">Enregistrer</button>
				</div>
			</div>
		</div>
	</div>



	<!-- Modal choix de groupe evenement -->
	<div class="modal fade" id="event-group-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Effectuer cette action pour...</h5>

				</div>
				<div class="modal-body">
					<div class="input-group  col-md-12">
						<div class="event-group-option">
							<ul>
								<li data-target="group"><i class="far fa-calendar-alt"></i>
									La série
									<small class="text-muted">Concernera tous les évenements lié à cet évenement</small>
								</li>
								<li data-target="event">
									<i class="far fa-calendar-check"></i>
									Cet évenement
									<small class="text-muted">Ne concernera que cet évenement précis</small>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>

	<!-- /teammate view -->



	<div class="planning-teammates hidden"><?php

		$response = array();
		$teammates = $myUser->preference('planning_teammate')!='' ? explode(',',$myUser->preference('planning_teammate')) : array();
		foreach ($teammates as $login) {
			$user = User::byLogin($login);
			$user->password = '';
			$row = $user->toArray();
			$row['avatar'] = $user->getAvatar();
			$response[] = $row;
		}
		echo json_encode($response);

	?></div>
</div>
