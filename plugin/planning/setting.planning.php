<?php 
global $myUser;
User::check_access('planning','configure');
require_once(__DIR__.SLASH.'PlanningEventType.class.php');
?>

<div class="row">
    <div class="col-md-12">
        <br>
        <?php if($myUser->can('planning', 'edit')) : ?>
        <div onclick="planning_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <?php endif; ?>
        <h3>Réglages planning</h3>
        <div class="clear"></div>
        <hr>
    </div>
</div>

<div class="row">
    <!-- search results -->
    <div class="col-md-12">
        <div class="tab-container noPrint">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#tab-types" aria-controls="tab-types" aria-selected="false">Types d'événements</a></li>
                <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-settings" aria-controls="tab-settings" aria-selected="false">Paramètres</a></li>
            </ul>
        </div>

        <div class="tab-content">
            <!-- Onglet Types d'événement -->
            <div class="tab-pane show active in" id="tab-types" role="tabpanel" aria-labelledby="tab-types"><br>
                <div class="row">
                    <div class="col-md-9">
                        <h3>Types d'évenements</h3>
                    </div>
                    <div class="col-md-3 text-right ">
                        <div class="btn btn-light" onclick="planning_event_type_add()"><i class="fas fa-plus"></i> Ajouter</div>
                        <div class="btn btn-success"  onclick="planning_event_type_save()"> Enregistrer</div>
                    </div>
                </div>

                <ul class="planning-types mt-2">
                    <li data-id="{{id}}" class="hidden planning-line {{class}}">
                        <span class="planning-type-handler">
                            <div class="planning-type-chip" style="background:{{color}};color:{{textcolor}}"><i class="{{icon}}"></i></div> {{label}}
                        </span>
                        <div class="planning-type-options">
                            {{#editableOptions}}
                            <div class="btn"  onclick="$(this).parent().parent().find('.planning-type-properties').toggleClass('hidden');event.stopPropagation();">
                                <i class="fas fa-cog text-muted "></i>
                            </div>
                            {{/editableOptions}}
                        </div>
                        <div class="planning-type-properties hidden">
                            <div class="input-group mt-3 mb-3">
                                <input data-type="icon" value="fas fa-info-circle" class="type-icon form-control input-group-prepend" type="text"  value="{{icon}}">
                                <input placeholder="Ajoutez un type d'évenement, ex : Congès" class="type-label form-control"type="text" value="{{label}}">
                                <input  type="text" value="{{color}}" class="type-color" data-type="color">
                            </div>

                            <label><input class="type-editable" data-type="checkbox" value="{{editable}}" type="checkbox">Editable</label>
                            <small class="text-muted pl-2">Éditable par les utilisateurs n'ayants pas de droit superadmin</small>
                            
                            <div class="right mt-2" ><i onclick="planning_event_type_delete(this);" class="far fa-trash-alt pointer"></i></div>
                            <div class="clear"></div>
                        </div>
                    </li>
                </ul>
              
            </div>

            <!-- Onglet Paramètres -->
            <div class="tab-pane" id="tab-settings" role="tabpanel" aria-labelledby="tab-settings"><br>
                <?php echo Configuration::html('planning'); ?>
            </div>
        </div>
    </div>
</div>

