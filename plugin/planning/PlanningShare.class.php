<?php
/**
 * Define a planningshare.
 * @author Valentin CARRUESCO
 * @category Plugin
 * @license MIT
 */
class PlanningShare extends Entity{
	public $id,$planning,$recipient,$recipientEntity,$read,$edit;
	protected $TABLE_NAME = 'planning_share';
	public $fields =
	array(
		'id' => 'key',
		'planning' => 'int',
		'recipient' => 'string',
		'recipientEntity' => 'string',
		'read' => 'boolean',
		'edit' => 'boolean'
	);
}
?>