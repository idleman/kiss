<?php
global $myUser,$conf;
User::check_access('client','configure');
?>

<div class="row" id="client-map-setting-form">
    <div class="col-xl-12"><br>
    	<div onclick="client_map_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Client</h3>
        <?php echo Configuration::html('clientmap'); ?>
        <div class="clear"></div>
		<hr>
    </div>
    
    <p>Pour ajouter des champs personnalisés à la fiche client, créez une formulaire dynamique ayant pour slug <code>client-sheet-custom</code></p>
    <p>Pour intégrer la base client à une autre application, vous pouvez utiliser le code suivant :</p>
    

    <code>
         &lt;iframe frameborder="0" width="100%" align="center" height="600px" src="<?php echo ROOT_URL ?>/index.php?module=clientmap&p=1&embedded=1"&gt;&lt;/iframe&gt;
    </code>

    <p><strong>nb :</strong> n'oubliez pas de définir les permissions pour les utilisateurs non connectés</p>
</div>
