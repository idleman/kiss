
# Bugs restants :
En lecture seule (anonyme):
- les cases a cocher + copier mailsur la liste des clients rafraichissent la page
- sur la carte, la pagination passe par dessus la fiche dépliée pour certaines fiche trop longue
- les champs customs ne sont pas visibles 

# Feature manquantes :
- case a cocher "publier pour les anonyme" qui créé le droit sur cette fiche pour les anonymes


# a verifier : 
- intégraiton iframe en mode déconnécté pour map
- intégraiton iframe en mode déconnécté pour liste client + fiche