<?php
global $myUser,$myFirm;


if(!$myUser->can('clientmap','read')) throw new Exception("Permission non accordée");

Plugin::need('client/Client');




$filters = array();
Plugin::callHook('client_filter',array(&$filters));
        



Plugin::callHook('client_search_view',array(&$columns)); 

//Filtres && colonne tableau en fonction des champs dynamiques
if($myFirm->has_plugin('fr.core.dynamicform') || ($myFirm->id==-1 && Plugin::is_active('fr.core.dynamicform')) ){
    Plugin::need('dynamicform/DynamicForm');
    $options = array();
    if(!empty($_['firm']) && is_numeric($_['firm'])) $options['firm'] = $_['firm'];
   
    //Récuperation des champs custom
    $fields = Dynamicform::list('client-sheet-custom',$options);
    //Ajout des champs custom en filtres
    $filters = array_merge($filters,Dynamicform::get_filters($fields));
    //Ajout des champs custom en colonnes dynamiques
    $columns = array_merge($columns,Dynamicform::get_dynamic_columns($fields));
}

?>
<div class="plugin-clientmap">
    <div class="map-panel shadow-sm">
       
        
        <div class="col-md-12 map-panel-box noPrint pt-3" style="height: inherit;">
            <select id="filters" data-type="filter" data-label="Recherche" data-autosearch="false" data-function="client_map_search">
               
                <?php foreach ($filters as $filter): echo $filter; endforeach; ?>
            </select>

           
            <div class="btn btn-link btn-return hidden w-100 my-3" onclick="client_map_edit_cancel()">RETOUR RECHERCHE</div>
            <div class="client-list-container">
            <ul id="clients"

            data-type="search-table" 
            data-slug="client-search-table"  
            data-entity-search="client_map_search"
            data-checkbox-action=".btn-checkbox-action"
            class="" >
            
               
                    <li data-id="{{id}}" class="hidden client-state-{{state}} client-item" onclick="client_map_edit(this)">
                       
                        <div>
                            <img class="client-logo-mini avatar-rounded left mr-2 mt-2 map-client-avatar" data-src="{{logo}}">
                            <div class="map-client-label"  title="{{{label}}} ({{type.label}})">
                                {{{label}}} ({{type.label}}) {{#pseudonym}}<small class="text-muted">({{{pseudonym}}})</small>{{/pseudonym}}
                               
                                
                                {{#job}}<br/><div class="text-muted client-job" title="{{{job}}}">{{{job}}}</div>{{/job}}
                            </div>
                            <div class="clear"></div>
                        </div>

                        {{#address}}
                        {{#address.city}}
                        <div class="my-2">
                            <i class="fas fa-map-marker-alt text-icon"></i> {{address.street}} {{address.complement}}  {{#address.zip}}
                            <small class="text-muted d-block">
                                {{address.city}} ({{address.zip}}) 
                            </small>
                            {{/address.zip}}
                        </div>
                        {{/address.city}}
                        {{/address}}

                        <div class="client-details">
                            <hr/>
                            {{#mail}}<div class="my-2"><i class="far fa-envelope-open text-icon"></i> <a href="mailto:{{mail}}">{{mail}}</a></div>{{/mail}}
                            {{#phone}}<div class="my-2"><i class="fas fa-mobile-alt text-icon"></i> {{phone}}</div>{{/phone}}

                            {{#client-sheet-custom_site-internet}}<div class="my-2"><a href="{{meta.website}}" target="_blank"><i class="fas fa-globe-europe text-icon"></i> {{{client-sheet-custom_site-internet}}}</a></div>{{/client-sheet-custom_site-internet}}
                            
                             <a class="btn btn-primary w-100 text-white my-3" title="Afficher sur Google Map" target="_blank" href="{{address.mapurl}}"><i class="fas fa-map-marked-alt ml-2 pointer"></i> VOIR SUR GOOGLE</a>
                        </div>

                        <a class="external-link" href="index.php?module=client&page=sheet.client&id={{id}}" target="_blank">
                            <i class="fas fa-external-link-alt"></i>
                        </a>
                    </li>
              
            </ul>
            </div><br>
           
             <ul class="pagination justify-content-center noPrint"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');client_map_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
        </div>

    </div>
    <div id="map-panel-button" class="d-sm-flex d-md-none rounded-right"  onclick="collapseMapPanel()">
        <i class="fas fa-caret-right"></i>
    </div>
    
    
    <!-- search results -->
    <div class="map-container"></div>
    
    <div id="client-map-no-points" class="client-map-no-points p-3 bg-white shadow-sm">Aucun résultat(s) pour ce(s) critère(s)</div>

    <!-- marker tooltip template -->        
    <template id="map-tooltip"><img class="client-logo-mini avatar-rounded left mr-2" src="{{client.logo}}"> 
        <a href="index.php?module=client&page=sheet.client&id={{client.id}}" data-id="{{client.id}}"  class="pointer text-info font-weight-bold mb-1 d-block">{{client.label}}</a><small class="text-muted">{{client.job}}</small>
        <div class="text-center mt-2">{{client.address.street}} {{client.address.zip}} {{client.address.city}}</div>
    </template>
     <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
        
    <div class="hidden" id="customFields"><?php 
    global $conf;
    echo $conf->get('clientmap_custom_fields'); ?></div>
</div>