//CHARGEMENT DE LA PAGE
function init_plugin_clientmap(){
	switch($.urlParam('page')){
		default:
		break;
	}


}



/** CLIENT **/

//Enregistrement des configurations
function client_map_setting_save(){
	$.action({ 
		action: 'client_map_setting_save', 
		fields:  $('#client-map-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}


function client_map_search(options){

	options = !options ? {} : options;

	var box = new FilterBox('#filters');
	if(options && options.exportMode) $('.btn-export').addClass('btn-preloader');

	var searchTable = $('#clients').data('searchTable');
	searchTable.resetGlobalCheckbox(); 
	if(!options.keepChecked) searchTable.resetCheckbox();

	var data = {
		action:'client_client_search',
		filters: box.filters(),
		sort: $('#clients').sortable_table('get'),
		firm: $.urlParam('firm'),
		export:  !options.exportMode ? false : options.exportMode,
		selected : searchTable.checkboxes()
	};

	var customFields = $('#customFields').text().split("\n").filter(n => n);
	console.log(customFields);
	if(customFields.length!=0){
		data.columns = { 
			added : customFields, 
			deleted : []
		}
	}

	$('#clients').fill(data,function(response){
		if(!options.exportMode) $('.results-count > span').text(response.pagination.total);

		searchTable.fillCheckbox();

		var tooltipTpl = $('#map-tooltip').html();

		var jsonPoints = [];

		for (var k in response.rows) {
			var client = response.rows[k];
			
			if(!client.meta) continue;
			
			
			if(!client.meta.longitude || !client.meta.latitude) continue;
			var clientPoint = {};
			clientPoint.latitude = client.meta.latitude;
			clientPoint.longitude = client.meta.longitude;
			clientPoint.id = client.id;
			clientPoint.label = Mustache.render(tooltipTpl,{client:client}); 
			jsonPoints.push(clientPoint);
		}
     
		

		$('.map-container').html('<div data-type="map" data-zoom-position="topright" id="client-map" style="height:100%;width:80%;"></div>');
		
		$('#client-map').get(0).innerHtml = JSON.stringify(jsonPoints);
		if(jsonPoints.length==0){
			$('#client-map-no-points').removeClass('hidden');
		}else{
			$('#client-map-no-points').addClass('hidden');
		}


		init_components();
	});


}

function client_map_edit(element){
	var li;

	if($(element).hasClass('client-item')) {
		li = $(element)
	} else {
		var id = $(element).attr('data-id');
		li = $('.plugin-clientmap .map-panel #clients').find('li[data-id="'+id+'"]');
	}

	$('.plugin-clientmap .map-panel #clients li').addClass('hidden');
	li.removeClass('hidden').addClass('selected');
	$('.plugin-clientmap .map-panel .btn-return').removeClass('hidden');

	var points = $('#client-map').data('points');
	var id = li.attr('data-id');
	var marker = points[id];

	if(marker) marker.fire('click');
}

function client_map_edit_cancel(){
	$('.plugin-clientmap .map-panel #clients li:not(:eq(0))').removeClass('hidden selected');
	$('.plugin-clientmap .map-panel .btn-return').addClass('hidden');
}

function collapseMapPanel(){
	$('.map-panel').toggleClass('visible');
	$('#map-panel-button').toggleClass('visible');
}