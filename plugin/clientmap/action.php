<?php
	
	//Sauvegarde des configurations de client
Action::register('client_map_setting_save',function(&$response){
			global $myUser,$_,$conf;
			User::check_access('clientmap','configure');

			//Si input file "multiple", possibilité de normlaiser le
			//tableau $_FILES récupéré avec la fonction => normalize_php_files();
			
			foreach(Configuration::setting('clientmap') as $key=>$value){
				if(!is_array($value)) continue;
				$allowed[] = $key;
			}
			foreach ($_['fields'] as $key => $value) {
				
				if(in_array($key, $allowed))
					$conf->put($key,$value);
			}
		});
	
?>