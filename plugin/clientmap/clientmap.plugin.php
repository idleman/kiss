<?php



//Déclaration d'un item de menu dans le menu principal
function clientmap_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('clientmap','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=clientmap',
		'label'=>'Carte des clients',
		'icon'=> 'fas fa-map-pin',
		'color'=> '#9b59b6'
	);
}

//Cette fonction va generer une page quand on clique sur Carte des Clients dans menu
function clientmap_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='clientmap') return;
	$page = !isset($_['page']) ? 'list.client' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function clientmap_install($id){
	if($id != 'fr.core.clientmap') return;
	Entity::install(__DIR__);

	
}

//Fonction executée lors de la désactivation du plugin
function clientmap_uninstall($id){
	if($id != 'fr.core.clientmap') return;
	Entity::uninstall(__DIR__);
	
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('clientmap',array('label'=>'Gestion des droits sur le plugin Carte des Clients'));


require_once(__DIR__.SLASH.'action.php');

/*

*/

//Déclaration du menu de réglages
function clientmap_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('client','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.clientmap',
		'icon' => 'fas fa-angle-right',
		'label' => 'Carte Client'
	);
}

//Déclaration des pages de réglages
function clientmap_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

Configuration::setting('clientmap',array(
    "Général",
    
    'clientmap_custom_fields' => array("label"=>"Ajouter champs custom suivants","legend"=>"slug des champs séparés par saut de ligne (ex: mon-champ-1 <saut de ligne> mon-champ-2","type"=>"textarea"),
    
));


//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "clientmap_install");
Plugin::addHook("uninstall", "clientmap_uninstall"); 


Plugin::addHook("menu_main", "clientmap_menu"); 
Plugin::addHook("page", "clientmap_page"); 
Plugin::addHook("menu_setting", "clientmap_menu_setting");    
Plugin::addHook("content_setting", "clientmap_content_setting");   

?>