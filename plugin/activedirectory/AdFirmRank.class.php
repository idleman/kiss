<?php
/**
 * Define a Liaison groupe rang
 * @author Jean CARRUESCO
 * @category Plugin
 * @license MIT
 */
class AdFirmRank extends Entity{

	public $id;
	public $firm; //Établissement (Etablissement)
	public $rank; //Rang ERP (Rang)
	public $group; //Groupe AD (Liste classique)
	public $server; //Serveur (Nombre Entier)
	
	protected $TABLE_NAME = 'activedirectory_ad_firm_rank';
	public $entityLabel = 'Liaison groupe rang';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'firm' => array('type'=>'firm','label' => 'Établissement','link'=>'class/Firm.class.php'),
		'rank' => array('type'=>'rank','label' => 'Rang ERP','link'=>'class/Rank.class.php'),
		'group' => array('type'=>'list','label' => 'Groupe AD'),
		'server' => array('type'=>'integer','label' => 'Serveur','link'=>'plugin/activedirectory/AdServer.class.php')
	);

	//Colonnes indexées
	public $indexes = array();
	
}
?>