<?php

//Fonction executée lors de l'activation du plugin
function activedirectory_install($id){
	if($id != 'fr.core.activedirectory') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function activedirectory_uninstall($id){
	if($id != 'fr.core.activedirectory') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register('activedirectory',array('label'=>'Gestion des droits sur le plugin Active directory'));

//cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html
function activedirectory_action(){
	require_once(__DIR__.SLASH.'action.php');
}

//Déclaration du menu de réglages
function activedirectory_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('activedirectory','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=list.adserver',
		'icon' => 'fas fa-angle-right',
		'label' => 'Active directory'
	);
}

//Déclaration des pages de réglages
function activedirectory_content_setting(){
	global $_;

	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

require_once(__DIR__.SLASH.'action.php');



function activedirectory_directory_list(&$usermapping){
	foreach ($usermapping as $login => $infos) {
		$user = $infos['object'];
		//todo à dynamiser en fct de plugin_activedirectory_metafields
		if(isset($user->meta['personalPhone'])) $usermapping[$login]['values']['Portable (perso)'] = '<a href="tel: '.$user->meta['personalPhone'].'">'.$user->meta['personalPhone'].'</a>';
	}
}

function activedirectory_account_global(){
	global $myUser; 
	require_once(__DIR__.SLASH.'AdServer.class.php');
	$metas = array();
	//todo récuperer les meta pour tous les serveurs ad
	foreach(AdServer::loadAll() as $server){
		$serverMapping = json_decode($server->mapping,true);
		if(!is_array($serverMapping)) $serverMapping = array();
		$metas = array_merge($metas,$serverMapping);
	}
	$userMeta = $myUser->meta;
	if(!is_array($userMeta)) $userMeta  = json_decode($userMeta,true);
	?>

	<div class="row">
	<?php foreach($metas as $slug=>$data): ?>
		<div class="col-md-6">
			<label for="<?php echo $data['field']; ?>"><?php echo $data['label'] ?> :</label>
			<input id="<?php echo $data['field']; ?>" name="<?php echo $data['field']; ?>" class="form-control-plaintext" readonly="readonly" type="text" value="<?php echo isset($userMeta[$data['field']])?$userMeta[$data['field']]:''; ?>">
		</div>
	<?php endforeach; ?>

	</div>
	<?php
}

function activedirectory_login(&$user,$login,$password,$loadRight,$loadManager=true,$noPassword=false){
	global $_,$conf;
	require_once(__DIR__.SLASH.'AdServer.class.php');
	if($user != false) return;

	foreach(AdServer::loadAll() as $server){

		try{			
			
			$options = array();
			if(!$noPassword) $options = array('mode'=>'login','login'=>$login.$server->domain,'password'=> $password);

			$server->login($options);

			$users = $server->users(array(
				$server->authenticationMode => $login.($server->authenticationMode=='userprincipalname'?  $ldap->domain : '')
			),array(
				'limit' => 1, // limit de users (0= pas de limite)
				'group' => true, // charger les groupes ad
				'rank' => true, // charger les rank et les firms
				'rights' => true, // charger les rank et les firms
				'manager' => true, // charger les managers en tant qu'objet user
				'activeOnly' => true //n'afficher que les comptes non expirés
			));


			if(empty($users)) return;

			if(!$conf->get('activedirectory_case_sensitive_login')){
				$users = array_change_key_case($users, CASE_LOWER);
				$login = strtolower($login);
			}

			if(!isset($users[$login])) return;

			$user = $users[$login];
			
			$avatarPath = __ROOT__.FILE_PATH.AVATAR_PATH.$user->login.'.jpg';
			if(isset($user->meta['ldap_avatar']) && !file_exists(__ROOT__.FILE_PATH.AVATAR_PATH.$user->login.'.gif')){
				if(!file_exists(__ROOT__.FILE_PATH.AVATAR_PATH)) mkdir(__ROOT__.FILE_PATH.AVATAR_PATH,0755,true);
				file_put_contents($avatarPath,base64_decode($user->meta['ldap_avatar']));
			}
			
			
			$server->logout();
			break;
		}catch(Exception $e){
			continue;
		}

	}
}

function activedirectory_user_get_all(&$users,$options){

	require_once(__DIR__.SLASH.'AdServer.class.php');
	
	global $conf;

	foreach (AdServer::loadAll() as $server) {
		try{
			$server->login();
			$users = array_merge($users,$server->users(array(),$options));
			$server->logout();
		}catch(Exception $e){
			$server->logout();
			continue;
		}
	}
}

function activedirectory_user_save(&$user,$userForm,&$response){
	if($user->origin != 'active_directory') return;
	if($user->login != $userForm->login) throw new Exception("L'identifiant n'est pas modifiable");

	global $_,$conf;
	require_once(__DIR__.SLASH.'AdServer.class.php');

	//Régles de définition de mot de passe
	if(!empty($userForm->password)){
		if(strlen($userForm->password)<7) throw new Exception("Le mot de passe doit être supérieur à 7 caractères");
		if(!preg_match('|[0-9]|i', $userForm->password)) throw new Exception("Le mot de passe doit contenir au moins un chiffre");
		if(!preg_match('|[a-z]|i', $userForm->password)) throw new Exception("Le mot de passe doit contenir au moins une lettre");
		if(!preg_match('|[a-z]|', $userForm->password)) throw new Exception("Le mot de passe doit contenir au moins une lettre Minuscule");
		if(!preg_match('|[A-Z]|', $userForm->password)) throw new Exception("Le mot de passe doit contenir au moins une lettre Majuscule");
	}

	$meta = $user->meta;
	$meta = !is_array($meta) ?  json_decode($meta,true): $meta;
	if(!isset($meta['activedirectory_server'])){
		$response['warning'] = 'Le compte n\'est lié a aucun active directory, aucune modification ne sera effectuée';
		return;
	}

	$server = AdServer::getById($meta['activedirectory_server']);
   
    if($server->adminLogin=='') throw new Exception("Le compte AD admin n'est pas configuré, veuillez contacter un administrateur");
	$server->login(array('mode'=>'admin'));

	$user->phone = $userForm->phone;
	$user->mobile = $userForm->mobile;

	$changes = array(
		'telephonenumber' => $userForm->phone,
		'mobile' => $userForm->mobile,
	);

	$avatarPath = glob(__ROOT__.FILE_PATH.AVATAR_PATH.$user->login.'.*');
	if(!empty($avatarPath)){
		$avatarPath = $avatarPath[0];
		$temp = File::temp().rand(0,10000).basename($avatarPath);
		copy($avatarPath,$temp);
		Image::toJpg($temp);
		$temp = explode('.', $temp);
		array_pop($temp);
		$temp = implode('.',$temp).'.jpg';
		
		if(file_exists($temp)){
			$changes['jpegphoto'] = file_get_contents($temp);
			unlink($temp);
		}
	}

	if(!empty($userForm->password)){
		$changes['password'] = $userForm->password;
		$user->preference('passwordTime',time());
	}

	$server->userChange($user->login,$changes);
	$response['warning'] = 'Vous êtes sur un compte de société, seules les informations suivantes ont été modifiées :<br/>
		- Téléphone<br/>
		- Mobile<br/>
		- Mot de passe (7 caracteres minimum : Majuscules, minucules et chiffres)<br/>
		- Avatar (JPG uniquement)<br/>';

	$server->logout();
}


Configuration::setting('activedirectory',array(
    "Utilisateurs de l'AD",
    'activedirectory_default_rank' => array("label"=>"Rang par défaut","legend"=>"Utilisé si aucun groupe AD n'a été défini pour le rang \"Utilisateur\" standard","type"=>"rank"),
    'activedirectory_case_sensitive_login' => array("label"=>"Identifiant sensible à la casse","type"=>"boolean"),
));

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "activedirectory_install");
Plugin::addHook("uninstall", "activedirectory_uninstall"); 
Plugin::addHook("menu_setting", "activedirectory_menu_setting");
Plugin::addHook("content_setting", "activedirectory_content_setting");

Plugin::addHook('directory_list',"activedirectory_directory_list");
Plugin::addHook("account_global", "activedirectory_account_global");
Plugin::addHook("user_login", "activedirectory_login");
//utilisé dans le user get all
Plugin::addHook("user_get_all", "activedirectory_user_get_all");
//Utilisé dans le user save
Plugin::addHook("user_save","activedirectory_user_save");


?>