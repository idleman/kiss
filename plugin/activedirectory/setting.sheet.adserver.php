<?php 
User::check_access('activedirectory','read');
require_once(__DIR__.SLASH.'AdServer.class.php');
$adserver = AdServer::provide();


?>
<div class="plugin-activedirectory">
	<div id="adserver-form" class="row justify-content-md-center adserver-form" data-action="activedirectory_adserver_save" data-id="<?php echo $adserver->id; ?>">
		<div class="col-md-12">
			<h3>SERVEUR 
			<div onclick="activedirectory_adserver_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<div onclick="activedirectory_adserver_test();" class="btn btn-small btn-test btn-primary right  mr-2"><i class="fas fa-vial"></i> Tester</div>
			<a href="setting.php?section=list.adserver" class="btn btn-small btn-dark right  mr-2">Retour</a></h3>
			

			<div id="activedirectory-test" class="activedirectory-test">
				<template>
					<div class="shadow-sm bg-white p-3 mb-3">
						<h7 class="text-muted font-weight-bold">TESTS DE CONNEXION</h7>
						
						<div class="row">
							<div class="col-md-4 activedirectory-test-block">
								<div class="mb-3">
								{{#data_validity}}<i class="far fa-check-circle text-success"></i>{{/data_validity}}
								{{^data_validity}}<i class="far fa-times-circle text-danger"></i>{{/data_validity}}
								Validité des données configurées
								</div>
							</div>
							<div class="col-md-4 activedirectory-test-block">
								<div class="mb-3">
								{{#server_reachable}}<i class="far fa-check-circle text-success"></i>{{/server_reachable}}
								{{^server_reachable}}<i class="far fa-times-circle text-danger"></i>{{/server_reachable}}
								IP/PORT du serveur atteignables
								</div>
							</div>
							<div class="col-md-4 activedirectory-test-block">
								<div class="mb-3">
								{{#authentication_readonly}}<i class="far fa-check-circle text-success"></i>{{/authentication_readonly}}
								{{^authentication_readonly}}<i class="far fa-times-circle text-danger"></i>{{/authentication_readonly}}
								Authentification de l'utilisateur readonly
								</div>
							</div>
							<div class="col-md-4 activedirectory-test-block">
								<div class="">
								{{#authentication_readonly}}<i class="far fa-check-circle text-success"></i>{{/authentication_readonly}}
								{{^authentication_readonly}}<i class="far fa-times-circle text-danger"></i>{{/authentication_readonly}}
								Authentification de l'utilisateur admin
								</div>
							</div>
							<div class="col-md-4 activedirectory-test-block">
								<div class="">
								{{#users}}<i class="far fa-check-circle text-success"></i>{{/users}}
								{{^users}}<i class="far fa-times-circle text-danger"></i>{{/users}}
								Détection des comptes ({{users}} comptes)
								</div>
							</div>
							<div class="col-md-4 activedirectory-test-block">
								<div class="">
								{{#groups}}<i class="far fa-check-circle text-success"></i>{{/groups}}
								{{^groups}}<i class="far fa-times-circle text-danger"></i>{{/groups}}
								Détection des groupes ({{groups}} groupes)
								</div>
							</div>
						</div>

					</div>
				</template>
			</div>

			<div class="shadow-sm bg-white p-3">
				<h7 class="text-muted font-weight-bold">CONNEXION</h7>
				<div class="input-group mb-3">
					  <div class="input-group-prepend">
					    <label class="input-group-text" for="ip">IP</label>
					  </div>
			
					<input  value="<?php echo $adserver->ip; ?>" class="form-control"  type="text"  id="ip" >
					<div class="input-group-prepend">
					    <label class="input-group-text" for="port">Port</label>
					</div>
					<input  value="<?php echo $adserver->port; ?>" class="form-control"  type="number"  id="port" >
					<div class="input-group-prepend">
					    <label class="input-group-text" for="sslPort">Port SSL</label>
					</div>
					<input  value="<?php echo $adserver->sslPort; ?>" class="form-control"  type="number"  id="sslPort" >
					<div class="input-group-prepend">
					    <label class="input-group-text" for="protocolVersion">Version LDAP</label>
					</div>
					<input  value="<?php echo $adserver->protocolVersion; ?>" class="form-control"  type="number"  id="protocolVersion" >
				</div>

				
				<div class="input-group mb-3">
					  <div class="input-group-prepend">
					    <label class="input-group-text" for="ip">Domaine</label>
					  </div>
					<input  value="<?php echo $adserver->domain; ?>" placeholder="@domaine.local" class="form-control"  type="text"  id="domain" >
				</div>

			</div>

			<div class="shadow-sm bg-white p-3 mt-3">
				<h7 class="text-muted font-weight-bold">ARBORESCENCE</h7>

				<div class="row">
					<div class="col-md-6">
						<label for="userRoot">Racine des comptes utilisateurs
							<small class="text-muted">Une "OU" par ligne</small></label>
						<textarea  class="form-control" type="text" id="userRoot"><?php echo $adserver->userRoot; ?></textarea>
					</div>
					<div class="col-md-6">
						<label for="groupRoot">Racine des groupes utilisateurs
							<small class="text-muted">Une "OU" par ligne</small></label>
						<textarea  class="form-control" type="text" id="groupRoot"><?php echo $adserver->groupRoot; ?></textarea>
					</div>
				</div>

			</div>

			<div class="shadow-sm bg-white p-3 mt-3">
				<h7 class="text-muted font-weight-bold">AUTHENTIFICATION</h7>


				<label for="authenticationMode" class="d-block">Mode d'authentification</label>
				
				<?php 
					$modes = array();
					foreach(AdServer::authenticationModes() as $key=>$mode){
						$modes[$key] = $mode['label'];
					}
				?>
				<label class="d-block"><input value="<?php echo  $adserver->authenticationMode ?>" data-values='<?php echo json_encode($modes) ?>' data-type="choice" id="authenticationMode" type="text" name="authenticationMode"></label>

				<div class="row">
					<div class="col-md-6">
						<label for="readonlyLogin">Identifiant du compte lecture seule</label>
						<input  value="<?php echo $adserver->readonlyLogin; ?>" class="form-control"  type="text"  id="readonlyLogin" >
						<label for="readonlyPassword">Mot de passe du compte lecture seule</label>
						<input  value="<?php echo decrypt($adserver->readonlyPassword); ?>" class="form-control"  type="text"  data-type="password"  autocomplete="new-password"  id="readonlyPassword" >
					</div>
					<div class="col-md-6">
						<label for="adminLogin">Identifiant compte administrateur</label>
						<input  value="<?php echo $adserver->adminLogin; ?>" class="form-control"  type="text"  id="adminLogin" >
						<label for="adminPassword">Mot de passe compte administrateur</label>
						<input  value="<?php echo decrypt($adserver->adminPassword); ?>" class="form-control"  type="text"  data-type="password"  autocomplete="new-password"  id="adminPassword" >
					</div>
				</div>

			</div>

			<div class="shadow-sm bg-white p-3 mt-3">
				<h7 class="text-muted font-weight-bold">GROUPES & RANGS</h7>
				<?php require_once(__DIR__.SLASH.'setting.list.ad.firm.rank.php'); ?>
		
			</div>

			<div class="shadow-sm bg-white p-3 mt-3">
				<h7 class="text-muted font-weight-bold">LIAISON DES CHAMPS</h7>
				<br>
				<label for="mapping">Plan de liaisons des champs Champ AD : Champ de la classe utilisateur</label>
				<input  data-format="multiple-values" data-columns='{"slug":"Champ AD","label":"Libellé","field":"Attribut Utilisateur"}' value='<?php echo $adserver->mapping; ?>'  data-type="jsontable" id="mapping">
				<div class="clear"></div>
			</div>
			
		</div>
	</div>
</div>