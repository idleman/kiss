<?php
global $myUser;
User::check_access('activedirectory','read');
require_once(__DIR__.SLASH.'AdFirmRank.class.php');





?>
<div class="plugin-activedirectory">
    <div class="row">
   
        
        <div class="col-md-12">
            <select id="activedirectory_ad_firm_rank-filters" data-type="filter" data-label="Recherche" data-function="activedirectory_ad_firm_rank_search">
                <option value="main.label" data-filter-type="text">Libellé</option>
                <option value="main.firm" data-filter-type="firm">Établissement</option>
                <option value="main.rank" data-filter-type="rank">Rang ERP</option>
                <option value="main.group" data-filter-type="list" data-values='<?php echo json_encode($groups); ?>' >Groupe AD</option>
            </select>
        </div>
        
    </div>
    <h5 class="results-count my-2"><span></span> Résultat(s)
        <!-- bloc de preference de pagination -->
        <small class="text-muted right text-muted text-small"><div class="d-inline-block mr-1" data-type="pagination-preference" data-table="#ad-firm-ranks" data-value="20" data-max-item="100"></div></small><div class="clear"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">

            

    		
            <!-- présentation tableau -->
            <table id="ad-firm-ranks" class="table table-striped mb-0" data-entity-search="activedirectory_ad_firm_rank_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->
                        <th data-sortable="group">Groupe AD</th>
                        <th data-sortable="rank">Rang ERP</th>
                        <th data-sortable="firm">Établissement</th>
                        <th></th>
                    </tr>
                </thead>
                
                <thead>
                    <tr id="ad-firm-rank-form" data-action="activedirectory_ad_firm_rank_save" data-id="">
                    
                        <th>
                            <select class="form-control select-control" type="text" value id="group" ></select>
                        </th>
                        <th class="position-relative"><input value="" class="form-control"  type="text"  data-type="user"  data-types="rank"  value  id="rank" ></th>
                        <th class="position-relative"><input value="" class="form-control"  type="text"  data-type="firm"  value  id="firm" ></th>
                        <th class="text-right"><div onclick="activedirectory_ad_firm_rank_save();" class="btn btn-success" title="Enregistrer" data-tooltip><i class="fas fa-check"></i></div></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	                <!--<td class="align-middle">{{id}}</td>-->
    	                <td class="align-middle">{{group}}</td>
                        <td class="align-middle"><a href="setting.php?section=right&rank={{rank.id}}">{{rank.label}}</a></td>
                        <td class="align-middle"><a href="firm.php?id={{firm.id}}">{{firm.label}}</a></td>
    	                
    	                
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                
                                <div class="btn text-info" title="Éditer ad_firm_rank" onclick="activedirectory_ad_firm_rank_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                                <div class="btn text-danger" title="Supprimer ad_firm_rank" onclick="activedirectory_ad_firm_rank_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
            

            
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');activedirectory_ad_firm_rank_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>