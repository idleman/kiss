<?php
global $myUser;
User::check_access('activedirectory','read');
require_once(__DIR__.SLASH.'AdServer.class.php');




?>

<div class="plugin-activedirectory">
    <div class="row">
    <div class="col-md-12"><br>
            <div class="btn btn btn-success ml-2 float-right" onclick="activedirectory_setting_save();"><i class="fas fa-check"></i> Enregistrer</div>
            <h3>Configuration générales</h3>
            <div class="clear"></div>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php echo Configuration::html('activedirectory'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">

                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des serveur active directorys</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="activedirectory_adserver_search(null,true);" id="export-activedirectorys-btn" class="btn btn-info rounded-0 btn-squarred ml-1 btn-export" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                
                
                <div class="my-auto ml-auto mr-0 noPrint">
                    <?php if($myUser->can('activedirectory', 'edit')) : ?>
                    <a href="setting.php?section=sheet.adserver" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                    <?php endif; ?>
                </div>
                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
            <select id="activedirectory_adserver-filters" data-type="filter" data-label="Recherche" data-function="activedirectory_adserver_search">
                <option value="main.label" data-filter-type="text">Libellé</option>
                <option value="main.ip" data-filter-type="text">IP</option>
                <option value="main.port" data-filter-type="integer">Port</option>
                <option value="main.sslPort" data-filter-type="integer">Port SSL</option>
                <option value="main.protocolVersion" data-filter-type="text">Version de protocole ldap</option>
                <option value="main.domain" data-filter-type="text">Domaine</option>
                <option value="main.userRoot" data-filter-type="textarea">Racine des comptes utilisateurs</option>
                <option value="main.groupRoot" data-filter-type="textarea">Racine des groupes utilisateurs</option>
                <option value="main.readonlyLogin" data-filter-type="text">Identifiant du compte lecture seule</option>
                <option value="main.adminLogin" data-filter-type="text">Identifiant compte administrateur</option>
                <option value="main.authenticationMode" data-filter-type="text">Authentication mode</option>
                <option value="main.defaultRank" data-filter-type="rank">Rang par défaut</option>
            </select>
        </div>
        
    </div>
    <h5 class="results-count my-2"><span></span> Résultat(s)
        <!-- bloc de preference de pagination -->
        <small class="text-muted right text-muted text-small"><div class="d-inline-block mr-1" data-type="pagination-preference" data-table="#adservers" data-value="20" data-max-item="100"></div></small><div class="clear"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">

            

    		
            <!-- présentation tableau -->
            <table id="adservers" class="table table-striped " data-entity-search="activedirectory_adserver_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->
                        <th data-sortable="ip">IP</th>
                        <th data-sortable="port">Port</th>
                       
                        <th data-sortable="domain">Domaine</th>
                        <th data-available="userRoot" data-sortable="protocolVersion">Version</th>
                        
                        <th data-available="userRoot" data-sortable="userRoot">Racines</th>
  
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	                <!--<td class="align-middle">{{id}}</td>-->
    	                <td class="align-middle">{{ip}}</td>
    	                <td class="align-middle">{{port}} / {{sslPort}}</td>
    	             
                        <td class="align-middle">{{domain}}</td>
    	                <td class="align-middle">{{protocolVersion}}</td>
    	                <td class="align-middle"><small class="text-muted">{{userRoot}}<br>{{groupRoot}}</small></td>

    	              
    
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                <a class="btn text-info" title="Éditer adserver" href="setting.php?section=sheet.adserver&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <div class="btn text-danger" title="Supprimer adserver" onclick="activedirectory_adserver_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
            

            <br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');activedirectory_adserver_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>