//CHARGEMENT DE LA PAGE
function init_setting_activedirectory(){
	

	$('#adservers').sortable_table({
		onSort : activedirectory_adserver_search
	});
	$('#ad-firm-ranks').sortable_table({
		onSort : activedirectory_ad_firm_rank_search
	});
}

function init_setting_sheet_adserver(){
	activedirectory_fields_search();
}


//Enregistrement des configurations
function activedirectory_setting_save(){
	$.action({ 
		action: 'activedirectory_setting_save', 
		fields:  $('#activedirectory-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}

/** SERVEUR ACTIVE DIRECTORY **/
//Récuperation d'une liste  serveur active directory dans le tableau #adservers
function activedirectory_adserver_search(callback,exportMode){
	var box = new FilterBox('#activedirectory_adserver-filters');
	if(exportMode) $('.btn-export').addClass('btn-preloader');

	$('#adservers').fill({
		action:'activedirectory_adserver_search',
		filters: box.filters(),
		sort: $('#adservers').sortable_table('get'),
		export:  !exportMode ? false : exportMode
	},function(response){
		if(!exportMode) $('.results-count > span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification serveur active directory
function activedirectory_adserver_save(){
	var data = $('#adserver-form').toJson();
	$.action(data,function(r){
		$('#adserver-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		activedirectory_fields_search();
		$.message('success','Enregistré');
	});
}


//Suppression serveur active directory
function activedirectory_adserver_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'activedirectory_adserver_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}


function activedirectory_adserver_test(){
	var id = $.urlParam('id');
	if(!id) return $.message('warning','Merci d\'enregistrer le serveur avant de le tester');
	$('.btn-test').addClass('btn-preloader');
	$('#activedirectory-test :not(template)').remove();
	$.action({
		action: 'activedirectory_adserver_test',
		id: id
	},function(r){
		var tpl = $('#activedirectory-test template').html();
		var block = Mustache.render(tpl,r);
		$('#activedirectory-test').append(block);
	});
}

function activedirectory_fields_search(){
	var id = $.urlParam('id');
	if(!id) return;
	var data = {action : 'activedirectory_fields_search', id:id};
	$.action(data,function(r){
		var options = '';
		for (var k in r.groups) {
			var group = r.groups[k];
			options+="<option>"+group+"</option>";
		}
		$('#group').html(options);
	});
}

/** LIAISON GROUPE RANG **/
//Récuperation d'une liste  liaison groupe rang dans le tableau #AdFirmRanks
function activedirectory_ad_firm_rank_search(callback,exportMode){
	var box = new FilterBox('#activedirectory_ad_firm_rank-filters');
	if(exportMode) $('.btn-export').addClass('btn-preloader');

	$('#ad-firm-ranks').fill({
		action:'activedirectory_ad_firm_rank_search',
		filters: box.filters(),
		sort: $('#ad-firm-ranks').sortable_table('get'),
		export:  !exportMode ? false : exportMode
	},function(response){
		if(!exportMode) $('.results-count > span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification liaison groupe rang
function activedirectory_ad_firm_rank_save(){
	var data = $('#ad-firm-rank-form').toJson();
	data.server = $.urlParam('id');
	$.action(data,function(r){
		$('#ad-firm-rank-form').attr('data-id','');
		activedirectory_ad_firm_rank_search();
	});
}

//Récuperation ou edition liaison groupe rang
function activedirectory_ad_firm_rank_edit(element){
	var line = $(element).closest('.item-line');

	$.action({
		action: 'activedirectory_ad_firm_rank_edit',
		id: line.attr('data-id')
	},function(r){
		$('#ad-firm-rank-form').fromJson(r);
		init_components('#ad-firm-rank-form');
		$('#ad-firm-rank-form').attr('data-id',r.id);
	});
}

//Suppression liaison groupe rang
function activedirectory_ad_firm_rank_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'activedirectory_ad_firm_rank_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}
