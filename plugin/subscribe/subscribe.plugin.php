<?php

//Cette fonction va generer une page quand on clique sur subscribe dans menu
function subscribe_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='subscribe') return;
	$page = !isset($_['page']) ? 'subscribe' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");

	require_once($file);
}

//captcha simple
function subscribe_captcha($code = null){
	//réponse captcha
	if(isset($code)){
		if($code == 'off'){
			unset($_SESSION['subscribe-captcha']);
			return;
		}
		if(!isset($_SESSION['subscribe-captcha'])) throw new Exception("Question anti-robot non initialisée");
		if($code != $_SESSION['subscribe-captcha']) throw new Exception("Réponse anti-robot invalide");
		return;
	}
	//question captcha
	$operators = array('+','-','*');
	$cssChars = array(
		'1'=>'<i class="boell"></i>',
		'un'=>'<i class="boell"></i>',
		'+'=>'<i class="lecoq"></i>',
		'plus'=>'<i class="lecoq"></i>',
		'-'=>'<i class="narboni"></i>',
		'moins'=>'<i class="narboni"></i>',
		'5'=>'<i class="carruesco"></i>'
	);
	$literalOperators = array('plus','moins','fois');
	$literalNumbers = array('zero','un','deux','trois','quatre','cinq','six','sept','huit','neuf','dix');
	$questionBefore = array('Quel est le résultat du calcul suivant : ','Combien font ','Calculer ', 'Trouvez le résultat de ');

	$first = mt_rand(0,10);
	$second = mt_rand(0,10);

	$html = $questionBefore[mt_rand(0,count($questionBefore)-1)];

	$html .= mt_rand(0,1) ? $literalNumbers[$first] : $first;
	$operator = $operators[mt_rand(0,2)];
	$literalOperator = $operator;
	$literalOperator = mt_rand(0,1) ? str_replace($operators,$literalOperators,$literalOperator) : $literalOperator;
	$html .= ' '.$literalOperator.' ';
	$html .= mt_rand(0,1) ? $literalNumbers[$second] : $second;

	$_SESSION['subscribe-captcha'] = $first;
	switch ($operator) {
		case '+': $_SESSION['subscribe-captcha']+= $second; break;
		case '-': $_SESSION['subscribe-captcha']-= $second; break;
		case '*': $_SESSION['subscribe-captcha']*= $second; break;
	}

	$htmlParts = array();
	foreach (explode(' ',$html) as $key => $word) {
		if(is_numeric($word) || strlen($word)<=1) {
			$htmlParts[] = $word;
			continue;
		}

		$wordParts = str_split($word,strlen($word)/(mt_rand(1,2)));
		foreach ($wordParts as $key=>$part) {
			if(mt_rand(0,10) > 7){
				$wordParts[$key] = str_replace(array_keys($cssChars), array_values($cssChars), $wordParts[$key]);
				continue;
			}
			$wordParts[$key] = subscribe_text_image($wordParts[$key],mt_rand(0,1));
		}
		$htmlParts[] = implode($wordParts);
	}
	return '<div class="subscribe-captcha-block">'.implode(' ',$htmlParts).'</div>';
}

function subscribe_text_image($text,$svg = false){
	if($svg){
		$width = mb_strlen($text)*8;
		return '<svg height="20" style="margin-top: 1.5px;" width="'.$width.'">
		  <text id="15 + 5 - 6 * 9" style="margin-top:5px;font-variant:normal;font-weight:normal;font-size:16px;font-family: \'subscribe-captcha\';-inkscape-font-specification:CourierNewPSMT;writing-mode:lr-tb;fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none" x="0" y="15">'.htmlentities($text).'</text>
		</svg>';
	}
	$width = mb_strlen($text)*8.5;

	ob_start();
	// Création de l'image
	$im = imagecreatetruecolor($width, 30);

	// Création de quelques couleurs
	$white = imagecolorallocate($im, 255, 255, 255);
	$black = imagecolorallocate($im, 33, 37, 41);
	imagefilledrectangle($im, 0, 0, 399, 29, $white);
	$font = __DIR__.SLASH.'font'.SLASH.'captcha.ttf';

	// Ajout du texte
	imagettftext($im, 13, 0, 0, 17, $black, $font, $text);
	imagepng($im);
	imagedestroy($im);

	$imgstream = ob_get_clean();

	return '<img style="width:'.$width .'px;height:30px;" src="data:image/png;base64,'.base64_encode($imgstream).'"/>';
}

//Fonction executée lors de l'activation du plugin
function subscribe_install($id){
	if($id != 'fr.core.subscribe') return;
	Entity::install(__DIR__);

	/* LISTE DE VALEUR : Domaines blacklistés */
	if(!$list = Dictionary::bySlug('subscribe_domain_blacklist')){
		$list =  new Dictionary;
		$list->slug = 'subscribe_domain_blacklist';
		$list->label = "Domaines blacklistés à l'inscription";
		$list->parent = 0;
		$list->state = Dictionary::ACTIVE;
		$list->save();
	}
	/* LISTE DE VALEUR : Domaines whitelistés */
	if(!$list = Dictionary::bySlug('subscribe_domain_whitelist')){
		$list =  new Dictionary;
		$list->slug = 'subscribe_domain_whitelist';
		$list->label = "Domaines whitelistés à l'inscription";
		$list->parent = 0;
		$list->state = Dictionary::ACTIVE;
		$list->save();
	}
}

//Fonction executée lors de la désactivation du plugin
function subscribe_uninstall($id){
	if($id != 'fr.core.subscribe') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("subscribe",array('label'=>"Gestion des droits sur le plugin subscribe"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


function subscribe_menu_login(&$loginMenu){
	global $conf;
	if(!$conf->get('subscribe_login_link')) return;
	$loginMenu[] = array(
		'sort' => 1,
		'icon' => 'fas fa-sign-in-alt',
		'label' => "S'inscrire",
		'url' => 'index.php?module=subscribe',
		'custom' => '<a title="Inscription" class="btn btn-secondary btn-sm" href="index.php?module=subscribe">S\'inscrire</a>'
	);
}

//Déclaration du menu de réglages
function subscribe_menu_setting(&$settingMenu){
	global $myUser;

	if(!$myUser->can('subscribe','configure')) return;
	$settingMenu[] = array(
		'sort' =>1,
		'url' => 'setting.php?section=global.subscribe',
		'icon' => 'fas fa-angle-right',
		'label' => 'Inscription'
	);
}

//Déclaration des pages de réglages
function subscribe_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Cron réalisé tous les jours pour supprimer
//les comptes n'ayant validé leur inscription
function subscribe_cron($time){
	global $_,$conf;
	if(date('H:i', $time)!='00:00' || empty($conf->get('subscribe_registered_expire'))) return;

	foreach(User::loadAll(array('state'=>'registered')) as $user){
		if($user->state!='registered' || (time()-$user->created) <= strtotime(intval($conf->get('subscribe_registered_expire')).' days', 0)) continue;
		User::deleteById($user->id);
	}
}

//Déclaration des settings de base
Configuration::setting('subscribe',array(
    "Général",
    'subscribe_login_link' => array("label"=>"Lien d'inscription dans l'encart de login", "legend"=>"Afficher le lien d'inscription dans le menu déroulant de connexion","type"=>"boolean"),
    'subscribe_short_login' => array("label"=>"Utiliser les identifiants courts","legend"=>"Tronquer l'email renseigné sans la partie @ pour générer l'identifiant","type"=>"boolean"),
    'subscribe_firm' => array("label"=>"Établissement attribué par défaut","legend"=>"Établissement attribué automatiquement aux utilisateurs validés après inscription","type"=>"firm"),
    'subscribe_rank' => array("label"=>"Rang attribué par défaut","legend"=>"Rang attribué automatiquement aux utilisateurs validés après inscription","type"=>"rank"),
    'subscribe_registered_expire' => array("label"=>"Expiration comptes inscrits non-validés","legend"=>"Délai en jours avant suppression des comptes n'ayant pas confirmé leur inscription","type"=>"integer", "placeholder"=>"eg. 28"),
    "Vérification",
    'subscribe_enable_captcha' => array("label"=>"Activer le captcha","legend"=>"Oblige les utilisateurs à renseigner la validation anti-robot pour s'inscrire","type"=>"boolean"),
    'subscribe_disable_paste' => array("label"=>"Désactiver le copier/coller","legend"=>"Si coché, oblige les utilisateurs à re-taper leur adresse mail pour la confirmation","type"=>"boolean"),
    'subscribe_mail_expire' => array("label"=>"Expiration lien d'inscription","legend"=>"Délai en jours avant expiration du lien d'inscription envoyé par mail (0 pour aucune expiration)","type"=>"integer", "placeholder"=>"eg. 06"),
    'subscribe_reply_mail' => array("label"=>"Adresse mail expéditeur","legend"=>"L'adresse mail indiqué comme expéditeur du mail d'inscription","type"=>"mail", "placeholder"=>"eg. no-reply@core.fr"),
    'subscribe_disable_mail' => array("label"=>"Désactiver la vérification par e-mail","legend"=>"<strong class=\"text-danger\">(Déconseillé)</strong> Si coché, aucun mail de confirmation d'inscription ne sera envoyé","type"=>"boolean"),
    "Gestion des domaines",
    'subscribe_enable_blacklist' => array("label"=>"Activer la liste noire des domaines","legend"=>"Empêche les domaines renseignés à l'inscription","type"=>"boolean"),
    'subscribe_enable_whitelist' => array("label"=>"Activer la liste blanche des domaines","legend"=>"Autorise uniquement les domaines renseignés à l'inscription","type"=>"boolean"),
));



//Déclation des assets
Plugin::addCss("/css/main.css");
Plugin::addJs("/js/main.js");

//Mapping hook / fonctions
Plugin::addHook("install", "subscribe_install");
Plugin::addHook("uninstall", "subscribe_uninstall");


Plugin::addHook("page", "subscribe_page");
Plugin::addHook("menu_setting", "subscribe_menu_setting");
Plugin::addHook("content_setting", "subscribe_content_setting");
Plugin::addHook("menu_login", "subscribe_menu_login");
Plugin::addHook("content_setting", "subscribe_content_setting");
Plugin::addHook("cron", "subscribe_cron");

?>