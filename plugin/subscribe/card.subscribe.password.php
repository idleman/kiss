<div class="subscribe-password-card font-">
	<div class="card-top">
		<h5>Règles de définition du mot de passe :</h5>
	</div>
	<div class="card-content">
		<small class="text-muted">
			<ul class="list-unstyled pl-2 pr-1 mb-2">
			<?php foreach($selectedFormats as $pattern):
				if(!isset($formats[$pattern])) continue;
				$label = str_replace(array("Le mot de passe doit", "(norme ANSSI)"), array("Doit", ""), $formats[$pattern]['label']); ?>
				<li class="d-flex">
					<i class="fas fa-angle-right mr-1 my-auto text-muted"></i>
				<?php if(preg_match($pattern, $passwordInput)): ?>
					<span class="text-success"><?php echo $label ?></span>
					<i class="far fa-check-circle text-success my-auto ml-auto mr-0"></i>
				<?php else: ?>
					<span class="text-danger"><?php echo $label ?></span>
					<i class="far fa-times-circle text-danger my-auto ml-auto mr-0"></i>
				<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>
		</small>
	</div>
</div>