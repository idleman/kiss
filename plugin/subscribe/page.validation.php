<?php global $_,$myUser,$conf; ?>
<div class="row justify-content-md-center">
	<div class="col-md-4 text-center subscribe-form">
		<?php try{
			if($myUser->connected()) throw new Exception("Inscription impossible en étant déjà connecté");
			
			if(!isset($_['token']) || empty($_['token'])) throw new Exception("Lien incorrect, code de confirmation manquant");
			$parameters = (array)json_decode(base64_decode($_['token']));

			if(!$user = User::load(array('token'=>$parameters['token']))) throw new Exception("Code de confirmation incorrect ou expiré");
			if(!empty($conf->get('subscribe_mail_expire')) && (time()-$user->created) > strtotime(intval($conf->get('subscribe_mail_expire')).' days', 0)) throw new Exception("Lien d'inscription expiré");

			foreach(User::getAll(array('right'=>false, 'force'=>true)) as $existingUser){
				if($existingUser->token==$user->token || $existingUser->state!=User::ACTIVE) continue;
				if($existingUser->mail == $user->mail) throw new Exception("Un utilisateur existe déjà avec cette adresse email, ".(!empty($conf->get('password_allow_lost'))?"veuillez utiliser le système de mot de passe oublié":"veuillez contacter un administrateur"));
			}

			if($user->state != 'registered') throw new Exception("Lien d'inscription expiré");

			switch ($parameters['step']) {
				case '1':
					$parameters['step'] = 2;
				?>
					<h5 class="text-muted text-uppercase">Validation d'inscription</h5>
					<img src="plugin/subscribe/img/subscribe_head.svg">
					<p class="mt-2 px-3">Bonjour, <?php echo $user->fullName(); ?>, pour valider votre inscription et récupérer vos identifiants, veuillez cliquer sur le bouton ci-dessous :</p>
				    <a class="btn btn-primary mt-2 w-100" href="<?php echo ROOT_URL.'/index.php?module=subscribe&page=validation&token='.base64_encode(json_encode($parameters)); ?>"><i class="fas fa-fw fa-check"></i> Valider</a>
				    <?php 
				break;
				
				case '2':
					//save du rang / établissement par défaut
					$userfirmRank = new UserFirmRank();
					$userfirmRank->rank = $conf->get('subscribe_rank');
					$userfirmRank->firm = $conf->get('subscribe_firm');
					$userfirmRank->user = $user->login;
					$userfirmRank->creator = $user->login;
					$userfirmRank->updater = $user->login;
					$userfirmRank->save();

					$user->state = User::ACTIVE;
					$user->save(); ?>
					<h5 class="text-muted text-uppercase">Inscription terminée</h5>
					<img src="plugin/subscribe/img/subscribe_head.svg">
					<p class="mt-2 px-3">Bonjour, <?php echo $user->fullName(); ?>, vous pouvez dès à présent vous connecter avec les éléments suivants :</p>
					<ul class="list-unstyled px-5 text-left">
				        <li><strong>Identifiant :</strong> <code class="font-weight-bold bg-light p-2 pointer" onclick="select_text(this, event);copy_string($(this).text(), this);"><?php echo $user->login; ?></code></li>
				        <li><strong>Mot de passe : </strong><small>Le mot de passe renseigné lors de votre inscription</small></li>
				    </ul>
				    <a href="index.php" class="btn btn-success mt-2 w-100" ><i class="fas fa-home"></i> Retour à l'accueil</a>
				    <?php 
				break;

				default:
					throw new Exception("Lien d'inscription expiré");
				break;
			} ?>
		<?php }catch(Exception $e){ ?>
			<div class="alert alert-danger" role="alert">
				<strong>Erreur</strong> <?php echo $e->getMessage(); ?>
			</div>
			<a href="index.php">Revenir à l'accueil</a>
		<?php } ?>
	</div>
</div>