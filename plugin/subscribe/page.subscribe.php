<?php global $conf; ?>
<div class="row justify-content-md-center">
	<div id="subscribe-form" class="col-lg-7 col-xl-5 text-center subscribe-form <?php if(!empty($conf->get('subscribe_disable_paste'))) echo "subscribe-disable-paste"; ?>" data-action="subscribe_save">
		<h5 class="text-muted text-uppercase">Inscription</h5>
		<img src="plugin/subscribe/img/subscribe_head.svg">
		<p class="mt-2 mb-3">Veuillez remplir le formulaire ci-dessous pour vous inscrire</p>
		<div class="row">
		<?php
		$passwordLegend = '';
		$forbiddenChars = htmlentities($conf->get('password_forbidden_char'));
		if(!empty($forbiddenChars)) $passwordLegend .= '<span class="text-danger pointer d-inline-block ml-1" data-tooltip title="'.$forbiddenChars.' interdits"><i class="far fa-question-circle"></i></span>';

		$selectedFormats = json_decode($conf->get('password_format'),true);
		if(is_array($selectedFormats) && !empty($selectedFormats)) $passwordLegend .= '<span class="font-italic text-primary pointer d-inline-block mt-1 float-right user-select-none subscribe-password-rules" data-type="card" data-action="subscribe_password_card" data-parameters=""><u>Voir les règles</u></span>';

		$fields = array(
			'firstname' => array(
				'label' => '<i class="far fa-user"></i>',
				'type' => 'text',
				'placeholder' => 'ex: John',
				'legend' => 'Prénom',
				'default' => '',
				'required' => true,
				'attributes' => array(
					'class' => '"text-capitalize form-control"'
				),
			),
			'name' => array(
				'label' => '<i class="fas fa-user"></i>',
				'type' => 'text',
				'placeholder' => 'ex: DOE',
				'legend' => 'Nom de famille',
				'default' => '',
				'required' => true,
				'attributes' => array(
					'class' => '"text-uppercase form-control"'
				),
			),
			'mail' => array(
				'label' => '<i class="far fa-envelope"></i>',
				'type' => 'mail',
				'placeholder' => 'john@doe.com',
				'legend' => 'Votre email',
				'default' => '',
				'required' => true,
			),
			'mail_confirm' => array(
				'label' => '<i class="far fa-envelope"></i>',
				'type' => 'mail',
				'placeholder' => 'john@doe.com',
				'legend' => 'Confirmation de votre email',
				'default' => '',
				'required' => true,
			),
			'password_initialisation' => array(
				'label' => '<i class="fas fa-lock"></i>',
				'type' => 'password',
				'placeholder' => '************',
				'attributes' => array(
					'autocomplete'=>'"new-password"',
					'data-generator' => true,
					'data-show-strength' => true,
					'data-forbidden' => $forbiddenChars,
					'data-length' => '"6"',
					'onkeyup' => '"subscribe_password_update(this)"',
					'onchange' => '"subscribe_password_update(this)"',
				),
				'legend' => 'Mot de passe '.$passwordLegend,
				'default' => '',
				'required' => true
			),
			'password_confirm' => array(
				'label' => '<i class="fas fa-lock"></i>',
				'type' => 'password',
				'placeholder' => '************',
				'legend' => 'Confirmation de mot de passe',
				'default' => '',
				'required' => true
			)
		);

		Plugin::callHook('subscribe_form',array(&$fields));
		foreach(FieldType::toForm($fields) as $field): ?>
			<div class="col-lg-12 col-xl-6 mb-3 text-left">
				<span class="pl-1 mb-1 d-inline-block"><?php echo $field['label']; ?></span>
				<?php if(!empty($field['legend'])): ?>
				<small class="text-muted"> - <?php echo $field['legend']; ?></small>
				<?php endif; 
				echo $field['input'] ?>
			</div>
		<?php endforeach; ?>
		
		<?php if($conf->get('subscribe_enable_captcha')): ?>
			<div class="col-md-12 subscribe-captcha-container">
				<hr>
				<legend class="mb-3">Question anti-robot<div class="btn btn-small" title="Renouveler le captcha" onclick="subscribe_reload_captcha(this)"><i class="fas fa-fw fa-sync-alt"></i></div></legend>
				<?php echo subscribe_captcha(); ?>
				<input class="form-control form-anti-robot text-center" type="text">
			</div>
		<?php endif; ?>
		</div>
		<div class="btn btn-success mt-3 w-100" title="Valider l'inscription" onclick="subscribe_save()"><i class="fas fa-fw fa-check"></i> Valider</div>
	</div>

	<!-- page de validation -->
	<div class="col-md-4 text-center subscribe-validation hidden">
	<?php if(!$conf->get('subscribe_disable_mail')): ?>
		<h5 class="text-muted text-uppercase">Inscription validée</h5>
		<img class="my-3" src="plugin/subscribe/img/subscribe_check.svg">
		<p class="mt-2">Vous allez recevoir un e-mail <br>de confirmation dans peu de temps.</p>
		<p class="mt-2">Si vous ne voyez pas l'email dans votre boîte de réception, <br>merci de regarder dans vos courriers indésirables.</p>
	<?php else: ?>
		<h5 class="text-muted text-uppercase">Inscription terminée</h5>
		<img src="plugin/subscribe/img/subscribe_head.svg">
		<p class="mt-2 px-3">Vous pouvez dès à présent vous connecter avec les éléments suivants :</p>
		<ul class="list-unstyled px-5 text-left">
			<li><strong>Identifiant :</strong> <code class="font-weight-bold bg-light p-2 pointer" onclick="select_text(this, event);copy_string($(this).text(), this);"></code></li>
			<li><strong>Mot de passe : </strong><small>Le mot de passe renseigné lors de votre inscription</small></li>
		</ul>
	<?php endif; ?>
		<a href="index.php">Revenir à l'accueil</a>
	</div>
</div>