<?php
global $myUser,$conf;
User::check_access('subscribe','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	<div onclick="subscribe_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Inscription</h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('subscribe'); ?>
    	<div data-type="dictionary-table" data-dictionary="subscribe_domain_blacklist" class="my-4"></div>
        <div data-type="dictionary-table" data-dictionary="subscribe_domain_whitelist" class="my-4"></div>
    </div>
</div>
