//CHARGEMENT DE LA PAGE
function init_plugin_subscribe(){
	switch($.urlParam('page')){
		default:
			$('#subscribe-form.subscribe-disable-paste input[type="mail"]').on('cut copy paste', function(e){
				e.preventDefault();
				return false;
			})
		break;
	}
	subscribe_search();
}


//Enregistrement des configurations
function subscribe_setting_save(){
	$.action({ 
		action: 'subscribe_setting_save', 
		fields: $('#subscribe-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}


/** SUBSCRIBE **/
//Permet de renouveler le captcha
function subscribe_reload_captcha(element){
	if(isProcessing) return;
	var button = $(element);

	isProcessing = true;
	$('i', button).addClass('fa-spin');
	setTimeout(function(){
		var form = $('#subscribe-form');
		$('.subscribe-captcha-container').load(document.URL +  ' .subscribe-captcha-container>*');
		$('i', button).removeClass('fa-spin');
		isProcessing = false;
	}, 1000);
}

function subscribe_password_update(element){
	var input = $(element);
	$('.subscribe-password-rules').attr({
		'data-parameters' : JSON.stringify({password:input.val()})
	});
}

//Récuperation d'une liste de  dans le tableau #s
function subscribe_search(callback){
	$('#s').fill({
		action:'subscribe_search'
	},function(response){
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément 
function subscribe_save(){
	var form = $('#subscribe-form')
	var data = form.toJson();
	data.captcha = $('.form-anti-robot').val();

	$('.btn-success', form).addClass('btn-preloader');
	$.action(data,function(r){
		var validation = $('.subscribe-validation');
		if(r.login!=null) $('code',validation).text(r.login);

		form.addClass('hidden');
		validation.removeClass('hidden');

		setTimeout(function(){
			validation.addClass('go-in')
			$('> img', validation).addClass('animate');
		},200);
	});
}


//Suppression d'élement 
function subscribe_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	
	$.action({
		action: 'subscribe_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

