<?php 
User::check_access('contact','read');

$contacts = array();
$contactperson = ContactPerson::provide();
if($contactperson->id==''){
	$contactperson->scope="contact_person";
	$contactperson->uid="";
}else{
	$contactperson->uid = empty($contactperson->uid) ? $contactperson->id: $contactperson->uid;
	foreach (Contact::loadAll(array('scope'=>'contact_person','uid'=>$contactperson->id)) as $key => $contact) {
		$contacts[]= array(
			'type'=>$contact->type,
			'value'=>$contact->value,
		);
	}
}

?>
<div class="plugin-contact">
	<div id="contact-person-form" class="row justify-content-md-center contact-person-form" data-action="contact_contact_person_save" data-id="<?php echo $contactperson->id; ?>">
		<div class="col-md-4 shadow-sm bg-white p-3">
			<h3>Contact 
			<div onclick="contact_contact_person_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<div class="btn btn-small btn-info mb-2 btn-dark mr-2 right" data-scope="contactperson" data-uid="<?php echo $contactperson->id; ?>" data-show-important="true" data-type="history" data-tooltip title="Ouvrir l'historique"> <i class="far fa-comment-dots"></i></div>
			<a href="index.php?module=contact&page=list.contact.person" class="btn btn-small btn-dark right  mr-2">Retour</a>
		</h3>
			

			<label for="name">Nom</label>
			<input  value="<?php echo $contactperson->name; ?>" class="form-control"  type="text"  id="name" >
			<label for="firstname">Prénom</label>
			<input  value="<?php echo $contactperson->firstname; ?>" class="form-control"  type="text"  id="firstname" >
			<label for="job">Métier</label>
			<input  value="<?php echo $contactperson->job; ?>" class="form-control"  type="text"  id="job" >
			<label for="civility">Civilité</label>
			<select class="form-control select-control" type="text" id="civility">
			<?php foreach(ContactPerson::civilities() as $slug=>$item): ?>
				<option <?php echo $contactperson->civility == $slug ? 'selected="selected"' : '' ?> value="<?php echo $slug ?>" ><?php echo $item['label']; ?></option>
			<?php endforeach; ?>
			</select>

			<label for="tag">Etiquettes</label>
			<input  value="<?php echo $contactperson->tag; ?>" class="form-control"  type="text"  data-type="tag"  data-multiple=true  id="tag" >
			
			<hr/>
			<label for="phones" class="text-muted font-weight-bold">TELEPHONES(S) & EMAILS(S)</label>
			<ul id="contact-list" class="contact-list">
				<li>
					<div class="input-group mb-3">
					  	<div class="input-group-prepend">
					    	<span class="input-group-text input-group-sm  p-0">
					    		<select class="form-control type form-control-select input-sm p-0 bg-transparent border-0" onchange="contact_contact_save()">
					    			<?php foreach (Contact::types() as $key => $type): ?>
					    				<option value="<?php echo $key;  ?>"><?php echo $type['label'];  ?></option>
					    			<?php endforeach ?>
					    		</select>
					    	</span>
					  	</div>
						<input class="form-control value" type="text" value="" onchange="contact_contact_save()">
						<div class="input-group-append">
							<div class="btn pointer" onclick="contact_contact_remove(this)"><i class="fa fa-trash-alt text-muted"></i></div>
							<div class="btn pointer" onclick="contact_contact_add(this)"><i class="fa fa-plus text-muted"></i></div>
						</div>
					</div>
				</li>
			</ul>
			

			
			<input  value="<?php echo base64_encode(json_encode($contacts)); ?>" class="form-control"  type="hidden"  id="contacts" >
			<input  value="<?php echo $contactperson->scope; ?>" class="form-control"  type="hidden"  id="scope" >
			<input  value="<?php echo $contactperson->uid; ?>" class="form-control"  type="hidden"  id="uid" >

		
	
			<br/>
			
		</div>

		<?php
		//Champs dynamiques
		global $myFirm;
			if($myFirm->has_plugin('fr.core.dynamicform')):
				Plugin::need('dynamicform/DynamicForm');
				$form = Dynamicform::show('contact-form',array(
					'scope'=>'contact_person',
					'uid'=>$contactperson->id
				));
				if(!empty($form)):
				
		?>

		<div class="col-md-4 shadow-sm bg-white p-3 ml-3">
			<h3>Informations complémentaires</h3>
			<?php 
			echo $form;
			?>
		</div>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</div>

