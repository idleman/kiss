<?php
global $myUser,$conf;
User::check_access('contact','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	
        <h3 class="mb-0"><i class="far fa-address-card"></i> Réglages Annuaire de contacts <div onclick="contact_setting_save();" class="btn btn-sm btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div></h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('contact'); ?>
    </div>

    <p>Pour ajouter des champs personnalisés à la fiche contact, créez une formulaire dynamique ayant pour slug <code>contact-form</code></p>
</div>
