<?php
global $myUser;
User::check_access('contact','read');






$civilities = array();
foreach (ContactPerson::civilities() as $key => $civility) {
    $civilities[$key] = $civility['label'];
}

?>
<div class="plugin-contact">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des contacts</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="contact_contact_person_search(null,true);" id="export-contacts-btn" class="btn btn-info rounded-0 btn-squarred ml-1" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                
                <div class="my-auto ml-auto mr-0 noPrint">
                    <?php if($myUser->can('contact', 'edit')) : ?>
                    <a href="index.php?module=contact&page=sheet.contact.person" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                    <?php endif; ?>
                </div>
                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
           
            <select id="filters" data-type="filter" data-label="Recherche" data-function="contact_contact_person_search">
                <option value="label" data-filter-type="text">Libellé</option>
                <option value="name" data-filter-type="text">Nom</option>
                <option value="firstname" data-filter-type="text">Prénom</option>
                <option value="job" data-filter-type="text">Métier</option>
                <option value="civility" data-operator-delete='["in","not in"]' data-filter-type="list" data-values='<?php echo json_encode($civilities); ?>' >Civilité</option>
                <option value="state" data-operator-delete='["in","not in"]' data-filter-type="list" data-values='{"<?php echo ContactPerson::ACTIVE; ?>":"Actif","<?php echo ContactPerson::INACTIVE; ?>":"Inactif"}' >Etat</option>
                
                <option value="scope" data-filter-type="text">Module</option>
                <option value="tag" data-operator-delete='["in","not in"]' data-filter-type="tag">Etiquettes</option>
        
                <option value="comment" data-filter-type="text">Commentaire</option>
                <?php
                    //Champs dynamiques
                    global $myFirm;
                        /*if($myFirm->has_plugin('fr.core.dynamicform')):
                            Plugin::need('dynamicform/DynamicForm');
                            $fields = Dynamicform::get_fields(array('slug'=>'contact-form'));
                            foreach ($fields as $k => $field): ?>
                                <option value="<?php echo $field['slug'] ?>" data-filter-type="text"><?php echo $field['label'] ?></option>
                           <?php 
                          endforeach;
                    endif; */
                    //todo a décommenter et implémenter avec la feature de JN pour les recherches sur dynamic field
                    //lorsqu'elle sera terminée.          
                ?>
            </select>
        </div>
        
    </div>
    <h5 class="results-count"><span></span> Résultat(s)</h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">
    		
            <table id="contact-persons" class="table table-striped " data-entity-search="contact_contact_person_search">
                <thead>
                    <tr>
               
                        <th >Contact</th>
 
                        <th data-sortable="job">Métier</th>
                        <th data-sortable="civility">Civilité</th>
                        <th data-sortable="phone">Téléphone</th>
                        <th data-sortable="mail">Email</th>
                        <th>Commentaire</th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    

                    <tr data-id="{{id}}" class="hidden item-line">
    	                <td class="align-middle"><div class="contact-avatar left mr-2"><img class="avatar-rounded" src="{{avatar}}"></div> {{{fullname}}}</td>
    	                <td class="align-middle">{{{job}}}
                            {{#hasTag}}
                            <div>
                                {{#tag}}
                                    <span class="badge badge-secondary">{{.}}</span>
                                {{/tag}}
                            </div>
                            {{/hasTag}}
                        </td>
    	                <td class="align-middle">{{civility.label}}</td>
    	                <td class="align-middle"><ul class="contact-phones">{{#phones}}
                        <li>{{label}} {{value}}</li>
                        {{/phones}}</ul></td>
    	                <td class="align-middle"><ul class="contact-mails">{{#mails}}
                        <li>{{value}}</li>
                        {{/mails}}</ul></td>
                        <td class="align-middle">{{comment}}{{^comment}}-{{/comment}}</td>
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm noPrint" role="group">
                                <div class="btn" title="Envoyer par mail" onclick='sendmail_preview({"origin" : "fr.core.contact","subject" : "Information contact {{fullname}}","message" : "<h3>{{civility.label}} {{fullname}}</h3><h5>{{job}}</h5><br>Tel: {{phone}}<br>Email: {{mail}}"});'><i class="fas fa-paper-plane"></i></div>
                                <a class="btn text-info" title="Éditer contact_person" href="index.php?module=contact&page=sheet.contact.person&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <div class="btn text-danger" title="Supprimer contact_person" onclick="contact_contact_person_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table><br>

             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center noPrint"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');contact_contact_person_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>