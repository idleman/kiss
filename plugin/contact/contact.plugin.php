<?php



//Déclaration d'un item de menu dans le menu principal
function contact_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('contact','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=contact',
		'label'=>'Annuaire de contacts',
		'icon'=> 'far fa-address-card',
		'color'=> '#9b59b6'
	);
}

//Cette fonction va generer une page quand on clique sur Annuaire de contacts dans menu
function contact_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='contact') return;
	$page = !isset($_['page']) ? 'list.contact.person' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function contact_install($id){
	if($id != 'fr.core.contact') return;
	Entity::install(__DIR__);

	global $myFirm;
		if($myFirm->has_plugin('fr.core.dynamicform')){
		Plugin::need('dynamicform/DynamicForm');
		
		$form = new DynamicForm();
		$form->slug = 'contact-form';
		$form->color = '#cecece';
		$form->state = DynamicForm::ACTIVE;
		$form->label = 'Contact : Informations complémentaires';
		$form->save();

		ContactPerson::staticQuery("INSERT INTO `dynamicform_field` ( `slug`, `type`, `label`, `mandatory`, `readonly`, `default`, `description`, `meta`, `state`, `form`, `row`, `sort`, `column`, `created`, `updated`, `updater`, `creator`) VALUES
			( 'firm', 'text', 'Société', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 0, '0', 1603286969, 1603286969, 'admin', 'admin'),
			( 'address', 'address', 'Adresse professionnelle', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 1, '0', 1603286969, 1603286969, 'admin', 'admin'),
			( 'url', 'text', 'Site Web', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 0, '1', 1603286969, 1603286969, 'admin', 'admin'),
			( 'category', 'list', 'Catégorie', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 1, '1', 1603286969, 1603286969, 'admin', 'admin'),
			( 'contact-form_service', 'text', 'Service', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 2, '0', 1603286969, 1603286969, 'admin', 'admin'),
			( 'contact-form_responsable', 'text', 'Responsable', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 2, '1', 1603286969, 1603286969, 'admin', 'admin'),
			( 'contact-form_commentaire', 'textarea', 'Commentaire', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 3, '0', 1603286969, 1603286969, 'admin', 'admin'),
			( 'contact-form_assistant(e)', 'text', 'Assistant(e)', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 3, '1', 1603286969, 1603286969, 'admin', 'admin'),
			( 'contact-form_tel.-assistant(e)', 'phone', 'Tel. Assistant(e)', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 4, '1', 1603286969, 1603286969, 'admin', 'admin'),
			( 'contact-form_tel.-standard', 'phone', 'Tel. standard', 0, 0, '', '', NULL, 'published', '".$form->id."', '0', 5, '1', 1603286969, 1603286969, 'admin', 'admin');");
	}

}

//Fonction executée lors de la désactivation du plugin
function contact_uninstall($id){
	if($id != 'fr.core.contact') return;
	Entity::uninstall(__DIR__);
	global $myFirm;
	if($myFirm->has_plugin('fr.core.dynamicform')){
		Plugin::need('dynamicform/DynamicForm,dynamicform/DynamicField');
		$form = DynamicForm::load(array('slug'=>'contact-form'));
		DynamicForm::delete(array('slug'=>'contact-form'));
		if($form)
			DynamicField::delete(array('form'=>$form->id));

	}
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('contact',array('label'=>'Gestion des droits sur le plugin Annuaire de contacts'));


//cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html
function contact_action(){
	require_once(__DIR__.SLASH.'action.php');
}
//Déclaration du menu de réglages
function contact_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('contact','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.contact',
		'icon' => 'fas fa-angle-right',
		'label' => 'Annuaire de contacts'
	);
}

//Déclaration des pages de réglages
function contact_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


require_once(__DIR__.SLASH.'action.php');


//Déclaration des settings de base
//Types possibles : voir FieldType.class.php. Un simple string définit une catégorie.
Configuration::setting('contact',array(
    "Général",
    //'contact_enable' => array("label"=>"Activer","type"=>"boolean"),
));
  



//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "contact_install");
Plugin::addHook("uninstall", "contact_uninstall"); 


Plugin::addHook("menu_main", "contact_menu"); 
Plugin::addHook("page", "contact_page"); 
Plugin::addHook("menu_setting", "contact_menu_setting");
Plugin::addHook("content_setting", "contact_content_setting");
?>