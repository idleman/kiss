<?php 
global $myUser;

try{
	User::check_access('statistic','read');
	require_once(__DIR__.SLASH.'Widget.class.php');



	if($widget->data('stats')!='') $statsWidget = Widget::getById($widget->data('stats'));
	
?>
<div class="widgetStatsContainer">
<?php if(isset($statsWidget) && $statsWidget && $statsWidget->id!=0): 
	$widget->title = $statsWidget->label;
	$item = array();

	try {

		$html = Widget::show($widget->data('stats'));
	} catch(Exception $e) {
		$html = '<div class="alert alert-danger"> <strong>WIDGET EN ERREUR</strong><p>Erreur de paramétrage du widget</p> <br>'.$e->getMessage().'</div>';
	}
	echo $html;
	else: ?>
	<h4 class="noContent"><i class="far fa-compass"></i> Aucune statistique spécifiée</h4>
<?php endif; ?>
</div>
<?php }catch(Exception $e){ ?>
<div class="alert alert-warning text-center m-0" role="alert">
	<?php echo  $e->getMessage(); ?>
</div>
<?php } ?>