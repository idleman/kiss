<?php
class Connection extends Entity{
	public $id,$label,$meta,$handler;
	public $TABLE_NAME = 'statistic_connection';
	public $fields = array( 
		'id'=>'key',
		'label'=>'longstring',
		'meta'=>'longstring',
		'handler'=>'string'
	);

	public function realString(){

		$meta = json_decode($this->meta,true);
		require_once(__ROOT__.SLASH.'connector'.SLASH.$this->handler.'.class.php');
		
		$connection = $this->handler::connection;
		foreach ($meta as $key => $value) {
			$connection = str_replace('{{'.$key.'}}',$value,$connection);
		}
		$connection = str_replace('{{ROOT}}',__ROOT__,$connection);
		return  $connection;
	}

	public function test(){
		require_once(__ROOT__.SLASH.'connector'.SLASH.$this->handler.'.class.php');

		$pdo = $this->getPdo();
		if(!isset($pdo)) throw new Exception("Impossible de se connecter à la base");
		$statment = $pdo->prepare($this->handler::show_tables());
		$statment->execute(array());
		$rows = $statment->fetchAll();
		if(!is_array($rows))  throw new Exception("Impossible de requeter la base");
		return count($rows);
	}

	public function getPdo(){

		$fileHandler = __ROOT__.SLASH.'connector'.SLASH.$this->handler.'.class.php';
		if(!file_exists($fileHandler)) return null;
		require_once($fileHandler);
		$string = $this->realString();

		$meta = json_decode($this->meta,true);
		$login = isset($meta['login']) ? $meta['login'] : null;
		$password = isset($meta['password']) ? $meta['password'] : null;
		try{
			$connection = new PDO($string, $login,$password,$this->handler::pdo_attributes());

			if(method_exists ( $this->handler , 'beforeTransaction' ))
         	$this->handler::beforeTransaction($connection);

			return $connection;
		}catch(Exception $e){
	      return null;
	   }    	
	}

	public function readOnly(){
		$pdo = $this->getPdo();
		if(strrpos($this->realString(), 'mysql:')!==false){
			$query = $pdo->query('SHOW GRANTS');
			$response = $query->fetchAll();
			if(isset($response[0])){
				if(!preg_match("/GRANT SELECT ON .* TO /i",$response[0][0])) return false;
			}
		}
		return true;
	}

	public function tables(){
		$fileHandler = __ROOT__.SLASH.'connector'.SLASH.$this->handler.'.class.php';
		if(!file_exists($fileHandler)) return array();
		require_once($fileHandler);
		$pdo = $this->getPDO();
		$tables = array();
		if(is_null($pdo)) return $tables;
		$lines = array();

		$query = $pdo->query($this->handler::show_tables());
		$lines = $query->fetchAll();
		foreach($lines as $line)
			$tables[] = $line[0];
		
		return $tables;
	}

	public function columns($table){
		$fileHandler = __ROOT__.SLASH.'connector'.SLASH.$this->handler.'.class.php';
		if(!file_exists($fileHandler)) return array();
		require_once($fileHandler);
		$pdo = $this->getPDO();
		$columns = array();
		if(is_null($pdo)) return array();
		$lines = array();

		$query = str_replace('{{table}}',$table,$this->handler::show_columns());
		$query = $pdo->query($query);
		$lines = $query->fetchAll();
		foreach($lines as $line){
			if(!isset($line['column'])){
				if(isset($line['name'])) $line['column'] = $line['name'];
			}
			$columns[] = $line;
		}
		
		return $columns;
	}

}
?>
