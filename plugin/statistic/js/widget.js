



function stats_widget_configure_init(){
	var widgetTitle = $('#widget-stats');
	widgetTitle.autocomplete({
		action : 'stats_widget_configure_autocomplete',
		skin : function(item){
			var html = '';
			var re = new RegExp(widgetTitle.val(),"gi");
			name = item.name.replace(re, function (x) {
				return '<strong>'+x+'</strong>';
			});
			html += '<h5 class="mt-1"><i class="'+item.icon+'"></i> <span>'+name+'</span></h5>'; 
			html +='<small>'+item.report+'</small></div>';
			html += '<div class="clear"></div>';
			return html;
		},
		highlight : function(item){
			return item;
		},
		onClick : function(selected,element){
			widgetTitle.val(selected.name);
			widgetTitle.attr('data-id',selected.id)
			init_components();
		}
	});
}



function widget_stats_init(){

		init_components();
		$('.widgetStatsContainer canvas').css('height','280px');
		$('.widgetStatsContainer canvas[data-type="doughnut"]').css('width','280px').css('margin','auto');
	
}

function stats_widget_configure_save(widget,modal){
	var data = $('#stats-widget-form').toJson();
	data.action = 'stats_widget_configure_save';
	data.id = modal.attr('data-widget');
	data.stats = $('#widget-stats').attr('data-id');
	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}