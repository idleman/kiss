var editor = null;

function init_plugin_statistic(){
	if($.urlParam('page')=='report'){
		stats_search_widget();
		$('.grid-stack').gridstack({
		    resizable: {
		        handles: 'e, se, s, sw, w'
		    },
		    animate: true,
		});
	}
	if($.urlParam('page')=='edit_report')
		stats_report_search_filter(function(){});

	$('.module-statistic.section-report').click(function(){
		$('.grid-stack-item-content').removeClass('active');
	});
}

function stats_select_widget(event,element){
	$('.grid-stack-item-content').removeClass('active');
	$(element).addClass('active');
	event.stopPropagation();
}

function stats_select_properties_tab(){
	$("#widgetTab .nav-item").addClass('hidden');
	$("#widgetTab .nav-link").removeClass('active show');
	$("#widgetTabContent .tab-pane").removeClass('active show');

	$("#widgetPropertiesTab").removeClass('hidden');
	$("#properties-tab").addClass('active show');
	$("#properties-panel").addClass('active show');
}

function stats_print_report(){
	$('.widgetBlock').addClass('widgetBlock-print');
	$('.widgetBlock').each(function(i,element){
		var widget = $(element);
		var canvas = $('canvas',widget).get(0);
		if(!canvas) return;
		var url = canvas.toDataURL('image/png');
		$('.grid-stack-item-content',widget).append('<img src="'+url+'">');
	});
	$('.widgetBlock canvas').addClass('hidden');
	$('#gridtack').removeClass('grid-stack');
	$('.widgetBlock').removeClass('grid-stack-item');
	
	setTimeout(function(){
		window.print();
		$('#gridtack').addClass('grid-stack');
		$('.widgetBlock').addClass('grid-stack-item');
		$('.widgetBlock').each(function(i,element){
			var widget = $(element);
			
			$('.widgetBlock canvas').removeClass('hidden');
			$('.grid-stack-item-content img',widget).remove();
			$('.widgetBlock').removeClass('widgetBlock-print');
		});
	},500);
}

function stats_export_view_table(element){
	var widget = $(element).closest('.widgetBlock');
	var id = widget.attr('data-gs-id');
	//si appellé depuis la modale d'édition du widget
	if(!id) id = $('#widgetModal').attr('data-widget');
	var filters = stats_get_filters();
	var filterString = '';
	for(var key in filters){
		filterString +="&filters%5B"+key+"%5D="+filters[key];
	}
	window.location = 'action.php?action=stats_export_view_table'+filterString+'&id='+id;
}

function stats_save_widget_element(){
	var data = {};
	data.action = 'stats_save_widget_element';
	data.widget = $('#widget').data('id');
	data.label = $('#label').val();
	data.type = $('#type').val();
	data.id = $('#elements thead tr').attr('data-id');
	


	$.action(data,function(r){
		$('#label').val('');
		$('#elements thead tr').removeAttr('data-id');
		stats_search_widget_element();
	});
}

function stats_search_widget_element(callback){
	$('#elements').fill({
		action:'stats_search_widget_element',
		id: $('#widget').data('id'),
	},function(){
		
		if(callback != null)
			$('#elements tbody').find('td:visible:first').click();		

		$('#elements tbody').sortable({
		  axis: "y",
		  helper: function (e, ui) {
		    ui.children().each(function() {
		        $(this).width($(this).width());
		    });
		    return ui;
			},
			 handle: ".btn-element-sort",
			//
		  update: function( event, ui ){

		  	var sort = [];
		  	$('#elements tbody tr:visible').each(function(){
		  		sort.push({
		  			id : $(this).attr('data-id'),
		  			type : $(this).attr('data-type'),
		  		});
		  	});

		  	$.action({
		  		action:'stats_widget_element_sort',
				sort: sort
			});
		  }
		});


	});	
}

function stats_select_widget_line(){
	var line = $('#elements tbody tr').eq(1);
	line.addClass('active');
	stats_edit_widget_element(line);
}

function stats_save_widget(){
	var data = {};
	data.action = 'stats_update_widget';
	data.id = $('#widget').data('id');
	data.label = $('#widgetLabel').val();
	$.action(data,function(r){
		$('#widgetPropertiesTitle').html('Widget : ');
		$('#widgetProperties').html(r.label);
	});
}


//filter

function stats_get_filters(){
	var data = {};

	$('#filter-form input,select:visible,textarea').each(function(i,input){
		var slug = $(input).attr('data-slug');
		if($(input).hasClass('data-type-user') )  return ;
		if (slug=='' || !slug) return;
		data[slug] = $(input).val();
	});
	return data;
}

function stats_report_import(){
	
	$.action({
		action : 'stats_report_import',
		file : $('#import').get(0).files
	},function(r){
		document.location.reload(true);
	});
}

function stats_report_search_filter(){
	$('#filterTable').fill({
		action:'stats_report_search_filter',
		report:$('#report').attr('data-id')
		
	},function(){

			$('#filterTable tbody tr:visible').each(function(i,element){
				$(element).find('[data-tpl-type="right"]').attr('data-type',"right");
			});
			init_components();
			$('#filterTable tbody').sortable({
				/*items: 'tr',*/
				handle : '.btn-move',
				placeholder: "navigation-sortable-placeholder",
				axis: "y",
				cursor: "move",
				stop : function(e, ui){
					var sort = [];
					$('#filterTable tr:visible').each(function(i,element){
						if(!$(this).attr('data-id') || $(this).attr('data-id')=='') return;
						sort.push($(this).attr('data-id'));
					});
					$.action({
						action : 'stats_report_move_filter',
						sort : sort
					});
				},
				//handle: ".navigation-template-item"
			});
		});
}

function stats_add_filter(){
	var data = $.getForm('#filter-form');
	data.id = $('#filter-form').attr('data-id');
	data.action = 'stats_save_filter';
	data.report = $('#report').attr('data-id');
	data.label = data.filterLabel;
	data.datasource = data.filterDataSource;
	data.type = data.filterType;
	data.default = data.filterDefault;
	//data.id = data.filterDefault;

	if(data.report==''){
		alert('Veuillez enregistrer le rapport avant d\'ajouter des filtres');
		return;
	}

	$.action(data,function(r){
		$('#filter-form').removeAttr('data-id').find('input').val('');
		$('#filter-form select').prop('selectedIndex',0);
		stats_report_search_filter();
	});
}
function stats_delete_filter(element, event){
	if(!confirm('Êtes-vous sûr de vouloir supprimer ce filtre ?')) return;
	event.stopPropagation();
	var data = {};
	var line = $(element).closest('tr');
	data.action = 'stats_delete_filter';
	data.id = line.attr('data-id');
		
	$.action(data,function(r){
		line.remove();
	});
}

function stats_change_filter_type(){
	$('.filter-source-container').addClass('hidden');
	if($('#filterType option:selected').attr('data-source'))
		$('.filter-source-container').removeClass('hidden');
	
	$('#filterDefault').attr('placeholder',$('#filterType option:selected').attr('data-placeholder'));
}

function stats_edit_filter(element){
	var data = {};
	var line = $(element).closest('tr');
	data.action = 'stats_edit_filter';
	data.id = line.attr('data-id');
	
	$.action(data,function(r){
		$.setForm('#filter-form',{ filterLabel : r.label ,filterType : r.type ,filterDefault : r.default,filterDataSource : r.datasource });
		$('#filter-form').attr('data-id',r.id);

		stats_change_filter_type();
	});
}

//report
function stats_save_report(){

	$.action({
		action : 'stats_save_report',
		label : $('#label').val(),
		report : $('#report').attr('data-id'),
		row : $('#row').val(),
		column : $('#column').val(),
		icon : $('#icon').val(),
		color : $('#color').val()
	},function(r){
		$.urlParam('id',r.id);
		$('.btn-open').attr('href',"index.php?module=statistic&page=report&id="+r.id);
		$('#report').attr('data-id',r.id);
	});
}


function stats_search_widget(){
	var report = $('#report').attr('data-id');

	$.action({
		action : 'stats_search_widget',
		report : report
	},function(r){
		var grid = $('.grid-stack').data('gridstack');
		grid.removeAll();

		if(r.widgets.length==0){
			$('#filter-form').addClass('hidden');
			$('.no-data').removeClass('hidden');

		}else{
			$('.no-data').addClass('hidden');
		}

		for(var key in r.widgets){
			var widget = r.widgets[key];
			
			var grid = $('.grid-stack').data('gridstack');
		    var tpl = $($('#widgetTemplate').html());

		    if(widget.content) $('.grid-stack-item-content',tpl).html(widget.content);
		    if(widget.background) $('.grid-stack-item-content',tpl).css('background',widget.background);
		  

		    if(!widget.edit){
		    	$('.btn-widget-edit',tpl).addClass('hidden');
		    	$('.grid-stack-item-content',tpl).removeAttr('ondblclick').removeAttr('onclick');
		    }
		    if(!widget.delete) $('.btn-widget-delete',tpl).addClass('hidden');

		    if(!widget.edit) $(tpl).attr('data-gs-no-move','yes').attr('data-gs-no-resize','yes');
		    
		    var element = grid.addWidget(tpl, widget.x, widget.y, widget.width, widget.height, false,3,12,3,12,widget.id);
		
		  
		}
		stats_refresh_widget_content();

		$('.grid-stack').on('change', function(event, items) {
	       var data = {
	       		action : 'stats_save_widget',
	       		report : $('#report').attr('data-id'),
	       		widgets : []
	       };

	       for(var i in items){
	       		data.widgets.push({
	       			id : $(items[i].el[0]).attr('data-gs-id'),
	       			x : items[i].x,
	       			y : items[i].y,
	       			width : items[i].width,
	       			height : items[i].height
	       		});
	       }

	       $.action(data,function(response){
	       		for(var i in items){
	       			if(!items[i].id)
	       				$(items[i].el[0]).attr('data-gs-id',response.lastId);
	       		}
	       });
		});

		$(document).keyup(function(e){
			if($('body').hasClass('modal-open')) return;
			if(e.keyCode==46 && $('.module-statistic .grid-stack-item-content.active').length)
				stats_delete_widget();
		});

		$('#add-widget').click(function() {
			$('.no-data').addClass('hidden');
		    var grid = $('.grid-stack').data('gridstack');
		    var widget = $($('#widgetTemplate').html());
		    grid.addWidget(widget, 0, 0, 3, 3, true,3,12,3,12);
		});
	});
}

function stats_refresh_widget_content(){
	var report = $('#report').attr('data-id');
	$('.btn-process').addClass('btn-preloader');
	$.action({
		action : 'stats_refresh_widget_content',
		report : report,
		filters :  stats_get_filters(),
		defaultSuccess : false
	},function(r){
		for(var key in r.widgets){
			var widget = r.widgets[key];
			//var container = $('.widgetBlock[data-gs-x="'+widget.x+'"][data-gs-y="'+widget.y+'"]');
			var container = $('.widgetBlock[data-gs-id="'+widget.id+'"]');

			container.find('.grid-stack-item-content').css('background-color','#ffffff').html(widget.html);
		}
		$('.dataTables_info').addClass('hidden');
		init_components();
	});
}


function stats_select_widget_view(element){
	if(typeof(element) !== 'undefined'){
		$('#view li').removeClass('selected');
		$(element).addClass('selected');
	}
	
	$.action({
		action : 'stats_update_widget',
		id :  $('#widgetModal').attr('data-widget'),
		view : $('#view li.selected').attr('data-uid')
	},function(r){
		$.action({
			action : 'stats_preview_view',
			filters :  stats_get_filters(),
			widget :  $('#widgetModal').attr('data-widget'),
			view : $('#view li.selected').attr('data-uid'),
			defaultSuccess : false
		},function(r){
	
			$("#widgetTab .nav-link").removeClass('active show');
			$("#widgetTabContent .tab-pane:not('.workspace .tab-pane')").removeClass('active show');

			$("#widgetTab .nav-item").not("#widgetPropertiesTab").removeClass('hidden');
			$("#preview-tab").addClass('active show');
			$("#preview-panel").addClass('active show');

			$('#preview-panel').html(r.html);
			
			$('.dataTables_info').addClass('hidden');
			init_components();

		},function(r){

			$("#widgetTab .nav-link").removeClass('active show');
			$("#widgetTabContent .tab-pane:not('.workspace .tab-pane')").removeClass('active show');

			$("#widgetTab .nav-item").not("#widgetPropertiesTab").removeClass('hidden');
			$("#preview-tab").addClass('active show');
			$("#preview-panel").addClass('active show');
			
			$('#preview-panel').html('<code class="error-code">'+r.error+'</code><hr><pre class="error-trace">'+r.trace+'</pre>');
		});
	});
}

function stats_properties_load(){
	
	$.action({
		action : 'stats_properties_load',
		id : $('#widget').attr('data-id')
	},function(r){
		$('.properties-block').html(r.html);
		init_components('.properties-block');
	});
}

function stats_properties_save(){
	var data = $('#properties-form').toJson();
	data.action = 'stats_properties_save';
	data.id = $('#widget').attr('data-id');
	$.action(data,function(r){
		$('.properties-block').html(r.html);
		$.message('success','Enregistré');
	});
}


function stats_rename_widget_element(element,event){
	event.stopPropagation();
	var line = $(element).closest('tr');
	$.action({
		action : 'stats_rename_widget_element',
		type : line.attr('data-type'),
		id : line.attr('data-id')
	},function(r){
		$.setForm('#elements thead tr',r);
		$('#elements thead tr').attr('data-id',r.id);
		
	});
}
function stats_delete_widget_element(element,event){
	event.stopPropagation();
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	$.action({
		action : 'stats_delete_widget_element',
		type : line.attr('data-type'),
		id : line.attr('data-id')
	},function(r){
		$('#elements tbody').find('td:visible:first').click();
		line.remove();		
	});
}

function stats_edit_widget_element(element){
	var line = $(element);
	$('#elements tr').removeClass('active');
	line.addClass('active');
	var id = line.attr('data-id');
	var type = line.attr('data-type');
	var editorOutput = $('#editorOutput');

	$.action({
		action : 'stats_edit_widget_element',
		type : type,
		defaultSuccess : false,
		id : id,
		filters :  stats_get_filters()
	},function(r){
		var editPanel = $("#edit-panel");
		var widgetTabContainer = $("#widgetTab");


		$(".nav-link", widgetTabContainer).removeClass('active show');
		$("#widgetTabContent .tab-pane").removeClass('active show');
		$(".nav-item", widgetTabContainer).not("#widgetPropertiesTab").removeClass('hidden');
		$("#edit-tab").addClass('active show');

		editPanel.addClass('active show');	

		editPanel.html(r.edit);

		$('#workspace').attr('data-type',type).attr('data-id',id);
		
		if(r.script){
			eval('var script = '+r.script);
			script();
		}
		editorOutput.removeAttr('style').html('<code class="success-code">Requête exécutée avec succès</code><hr><pre>'+r.data['data']+'</pre>');
		
		if(r.meta){
			for(var k in r.meta){
				editorOutput.append('<h5 class="text-warning">'+k+'</h5><pre>'+r.meta[k]+'</pre>');
			}
		}

		$('#output-panel').removeAttr('style').html('<code class="success-code">Requête exécutée avec succès</code><hr><div><label><i class="far fa-eye"></i> Données en sortie</label></div><pre>'+r.data+'</pre>');
	},function(r){
		var editPanel = $("#edit-panel");
		var widgetTabContainer = $("#widgetTab");


		$(".nav-link", widgetTabContainer).removeClass('active show');
		$("#widgetTabContent .tab-pane").removeClass('active show');
		$(".nav-item", widgetTabContainer).not("#widgetPropertiesTab").removeClass('hidden');
		$("#edit-tab").addClass('active show');

		editPanel.addClass('active show');
		editPanel.html(r.edit);

		$('#workspace').attr('data-type',type).attr('data-id',id);
		if(window[r.script]!=null) window[r.script]();
		$('#editorOutput').html('<code class="error-code">'+r.error+'</code><hr><pre>'+r.trace+'</pre>');
		$('#output-panel').html('<code class="error-code">'+r.error+'</code><hr><pre>'+r.trace+'</pre>');
	});
}

function stats_edit_widget_content(element){
	var cell = $(element).closest('.widgetBlock');
	var report = $('#report').attr('data-id');
	var modal = $('#widgetModal');
	
	var id = cell.attr('data-gs-id');
	$.action({
		action : 'stats_edit_widget_content',
		id : cell.attr('data-gs-id')
	},function(r){
		modal.modal('show');
		modal.attr('data-widget',cell.attr('data-gs-id'));
		$('.modal-body', modal).html(r.content);

		modal.on('shown.bs.modal', function () {
		  $('.stats-element-panel').panelResize({
		  	minWidth : 300
		  });
		});

		

		stats_search_widget_element(true);
		
		$('#preview-tab').on('click', function(){
			stats_select_widget_view();
		});
	});
}




function synonyms(cm, option) {
    return new Promise(function(accept) {

      setTimeout(function() {
        var cursor = cm.getCursor(), line = cm.getLine(cursor.line);
        var start = cursor.ch, end = cursor.ch;

        while (start && /\w/.test(line.charAt(start - 1))) --start;
        while (end < line.length && /\w/.test(line.charAt(end))) ++end;

        var word = line.slice(start, end).toLowerCase();

        for (var i = 0; i < comp.length; i++) if (comp[i].indexOf(word) != -1)
          	return accept({
          		list: comp[i],
                from: CodeMirror.Pos(cursor.line, start),
                to: CodeMirror.Pos(cursor.line, end)
            });

        	return accept(null);
    	}, 100)
    });
}

function stats_element_preview(element,callback){
	var editorOutput = $('#editorOutput');
	var output = $('#output-panel');
	editorOutput.css('text-align', 'center').html('<i class="fas fa-sync-alt fa-spin"></i>');
	output.css('text-align', 'center').html('<i class="fas fa-sync-alt fa-spin"></i>');

	if(element) $(element).addClass('btn-preloader');
	$.action({
		action : 'stats_element_preview',
		defaultSuccess : false,
		type : $('#workspace').attr('data-type'),
		filters :  stats_get_filters(),
		id : $('#workspace').attr('data-id'),
	},function(r){
		$('#tab-tables').removeClass('active show');
		$('#tab-results').addClass('active show');
		$('.tools-area').find('a').removeClass('active');
		$('.tools-area').find('a:first').addClass('active');
		editorOutput.removeAttr('style').html('<code class="success-code">Requête exécutée avec succès</code><hr><pre>'+r.data+'</pre>');

		if(r.meta){
			for(var k in r.meta){
				editorOutput.append('<h5 class="text-warning">'+k+'</h5><pre>'+r.meta[k]+'</pre>');
			}
		}


		output.removeAttr('style').html('<code class="success-code">Requête exécutée avec succès</code><hr><div><label><i class="far fa-eye"></i> Données en sortie</label></div><pre>'+r.data+'</pre>');
		if(callback) callback();
	},function(r){
		$('#tab-tables').removeClass('active show');
		$('#tab-results').addClass('active show');
		$('.tools-area').find('a').removeClass('active');
		$('.tools-area').find('a:first').addClass('active');
		editorOutput.removeAttr('style').html('<code class="error-code">'+r.error+'</code><hr><pre class="mt-3">'+r.trace+'</pre>');
		output.removeAttr('style').html('<code class="error-code">'+r.error+'</code><hr><pre class="mt-3">'+r.trace+'</pre>');
	});
}

function stats_element_query_table_filter(input){
	var keyword = $(input).val().toLowerCase();
	clearInterval(window.queryTableFilter);
	window.queryTableFilter = setTimeout(function(){
		$('#stats-query-tables .stats-widget-element > li:not(:first)').each(function(){
			var line = $(this);
			if( line.attr('data-table').toLowerCase().indexOf(keyword) === -1 ){
				line.addClass('hidden');
			}else{
				line.removeClass('hidden');
			}
		});
	},500);
	
}

function stats_element_init(type){
	$(function(){
		var object = type=='query' ? '#query' : '#source'
		window.editor = CodeMirror.fromTextArea($(object).get(0), {
		    mode: type=='query' ? 'text/x-mysql' : 'text/x-php',
		    theme: 'monokai',
		    lineWrapping: true,
		    gutter: true,
		    fixedGutter : true,
		    lineNumbers: true,
		    matchBrackets : true
		});
		editor.on("blur", function(){
			stats_element_content_save();
		});
		if(type=='query') stats_save_widget_connection();
	});
}

function stats_element_content_save(){
	var workspace = $('#workspace');
	var status = $('#server-status');
	if(window.editor.getValue()=='') return;
	$('#editor div.CodeMirror:visible').prepend($('<div id="save-overlay"><i class="fas fa-sync fa-spin fa-3x"></i></div>'));
	var overlay = $('#save-overlay');

	$.action({
		action :'stats_save_widget_element',
		type : workspace.attr('data-type'),
		id : workspace.attr('data-id'),
		sql : editor.getValue(),
		source : editor.getValue(),
		connection : $('#connection').val(),
	},function(r){
		overlay.remove();
		status.html('<code class="success-code"><i class="fas fa-check"></i> Modifications enregistrées</code>');
		setTimeout(function() { $('#server-status>*').addClass('hidden'); }, 3000);
	},function(r){
		overlay.remove();
		status.html('<code class="error-code"><i class="fas fa-times"></i> Modifications non enregistrées</code>');
	});
}




function stats_save_widget_connection(){
	$('.stats-table-tab > a').click();
	$.action({
		action: 'stats_save_widget_connection',
		id: $('#elements').find('tr.active[data-type="Query"]').attr('data-id'),
		connection: $('#connection').val()
	},function(){
		$('#stats-query-tables > ul').fill({
			action:'stats_search_query_tables',
			connection: $('#connection').val(),
		},function(){


		});
	});
	
}

function stats_search_query_columns(element,event){
	event.stopPropagation();
	var line = $(element).closest('li');
	var fieldBloc = $('ul',line);
	fieldBloc.toggleClass('hidden');

	if(!fieldBloc.hasClass('hidden')){
		$.action({
			action:'stats_search_query_columns',
			table:line.attr('data-table'),
			connection: $('#connection').val(),
		},function(r){
			fieldBloc.removeClass('hidden');
			var tpl = '<li data-column="{{column}}" onclick="stats_query_add_column(this,event)">{{column}} <small class="text-muted">{{type}}</small></li>';
			var html = '';
			for(var k in r.rows){
				var item = Mustache.render(tpl,r.rows[k]);
				html+=item;
			}
			fieldBloc.html(html);
		});
	}
	
}

function stats_query_add_table(element){
	var table = $('span',element).text();
	window.editor.setValue(window.editor.getValue()+table);
	stats_element_content_save();
	
}
function stats_query_add_column(element,event){
	event.stopPropagation();
	
	var column = $(element).attr('data-column');
	window.editor.setValue(window.editor.getValue()+column);
	stats_element_content_save();
}

function stats_delete_widget(element){
	if(!confirm('Êtes-vous sûr de vouloir supprimer ce widget ?')) return;
	if(!element) element = $('.grid-stack-item-content.active');
		var cell = $(element).closest('.widgetBlock');
	
	$.action({
		action : 'stats_delete_widget',
		id : cell.attr('data-gs-id'),
	},function(){
		var grid = $('.grid-stack').data('gridstack');
		grid.removeWidget(cell);
		if($('.widgetBlock:visible').length==0) $('.no-data').removeClass('hidden');
	});
}


function stats_export_report(){
	window.location = 'action.php?action=stats_export_report&id='+$('#report').attr('data-id');
}

function stats_delete_report(element){
	if(!confirm('Êtes-vous sûr de vouloir supprimer ce rapport ?')) return;

	window.location = 'action.php?action=stats_delete_report&id='+$(element).closest('.report-bloc').attr('data-id');
}

/** CONNECTION **/

function init_setting_statistic(){
	search_connection();
}

// SEARCH
function search_connection(callback){
	$('#connections').fill({
		action:'stats_search_connection'
	},function(){
		if(callback!=null) callback();
	});
}

// SAVE
function stats_save_connection(account){
	var data = $('#connectionForm').toJson();
	data.account = account == true;
	data.id = $('#connectionForm').attr('data-id');
	data.meta = $('.stats-connection-info').toJson();

	$.action(data,function(r){
		$.message('success','Enregistré');
		if(account) return;
		
		$('#connectionForm input').val('');
		$('#connectionForm').attr('data-id','');
		search_connection();
	});
}

//change handler
function stats_connection_handler_change(callback){
	
	$.action({action:'stats_connection_handler_search',
		handler:$('#handler').val()},function(r){
		$('.stats-connection-info').html(r.html);
		if(callback) callback();
	});
}


function stats_connection_test(element){

	var line = $(element).closest('tr');
	$('.btn-test',line).addClass('btn-preloader');
	$.action({action:'stats_connection_test',id:line.attr('data-id')},function(r){
		$.message('success',r.message);
	});
}

// EDIT
function stats_edit_connection(element){
	var line = $(element).closest('tr');
	$.action({action:'stats_edit_connection',id:line.attr('data-id')},function(r){
		$.setForm('#connectionForm',r);
		stats_connection_handler_change(function(){
			$.setForm('#connectionForm',r.meta);
		});
		$('#connectionForm').attr('data-id',r.id);
	});
}

// DELETE
function stats_delete_connection(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item?')) return;
	var line = $(element).closest('tr');
	$.action({
		action : 'stats_delete_connection',
		id : line.attr('data-id')
	},function(r){
		line.remove();
	});
}
