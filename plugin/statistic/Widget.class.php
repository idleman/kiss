<?php
require_once(__DIR__.DIRECTORY_SEPARATOR.'WidgetElement.class.php');
class Widget extends Entity{

	public $id,$report,$width,$height,$x,$y,$data,$label,$view,$background,$meta;
	public $TABLE_NAME = 'statistic_widget';
	public $fields = array( 
		'id'=>'key',
		'report'=>'int',
		'width' => 'int',
		'height' => 'int',
		'x' => 'int',
		'y' => 'int',
		'label'=>'string',
		'view' => 'string',
		'meta' => 'text',
		'background' => 'string'
	);

	public function elements(){
		require_once(__DIR__.SLASH.'WidgetElement.class.php');
		return WidgetElement::getElementsByWidget($this->id);
	}

	//Vérification des acces sur le widget courant ou sur le rapport du widget courant
	public function check_access($crud,$user = null){
		global $myUser;
		if(!isset($user)) $user = $myUser;
		//pour avoir 'lacces, l'utilisateur doit être le créateur OU avoir access au rapport OU avoir acces au widget
		return $user->login == $this->creator  ||
		 $user->can('statistic_report',$crud,$this->report) ||
		 $user->can('statistic_widget',$crud,$this->id);
	}

	public function remove(){
		require_once(__DIR__.SLASH.'element'.SLASH.'Query.class.php');
		require_once(__DIR__.SLASH.'element'.SLASH.'Treatment.class.php');
		self::deleteById($this->id);

		Query::delete(array('widget'=>$this->id));
		Treatment::delete(array('widget'=>$this->id));
	}

	//affiche un widget a partir de son id
	public static function show($id,$options = array(),$checkPermission = true){
		require_once(__DIR__.SLASH.'WidgetElement.class.php');
		global $myUser;


		$statsWidget = Widget::getById($id);
		if(!$statsWidget || $statsWidget->id == 0) throw new Exception("Widget #$id introuvable",404);


		if($myUser->login != $statsWidget->creator && !$myUser->can('statistic','read',$statsWidget->id) ) throw new Exception("Vous n'avez pas la permission pour afficher cette statistique");


		$item = array();
		$item = $statsWidget->toArray();
		$view = $statsWidget->view;
		if($view==null) throw new Exception("Vue introuvable",404);
		require_once(__DIR__.SLASH.'view'.SLASH.$view.'.class.php');
		$results = $statsWidget->cascadePreview(	 isset($options['filters']) ? $options['filters']: array()	);
		return  $view::toHtml(null,$results,$options);
	}


	public  function cascadePreview($filters = array()){
		$elements = WidgetElement::getElementsByWidget($this->id);
		$lastElement = end($elements);
		if(!$lastElement) throw new Exception("Veuillez spécifier au moins une requête ou un traitement pour afficher le bloc");
		return WidgetElement::cascadePreview(get_class($lastElement),$lastElement->id,$filters,$this);
	}
}
?>