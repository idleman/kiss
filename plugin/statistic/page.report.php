<?php
global $myUser;
User::check_access('statistic','read');
require_once(__DIR__.SLASH.'StatisticReport.class.php');
require_once(__DIR__.SLASH.'Filter.class.php');
$report = isset($_['id']) ? StatisticReport::getByid($_['id']) : new StatisticReport();
$filters = Filter::loadAll(array('report'=>$report->id),array('sort'));
require_once(__DIR__.SLASH.'element'.SLASH.'Treatment.class.php');


?>

<div class="row">
    <div class="col-xl-12">
        <div class="row">
            <div class="col-md-9">
                <h3 id="chart-title" class="chart-title"><i style="color:<?php echo $report->color; ?>" class="<?php  echo $report->icon; ?>"></i> 
                  <?php  echo $report->label; ?></h3>
            </div>
            <div class="col-md-3">
                <ul id="chart-menu" class="chart-menu noPrint">
                    <?php if($myUser->can('statistic','configure')): ?>
                        <li id="add-widget"><i title="Ajouter un widget au rapport" class="fas fa-plus text-success"></i></li>
                        <li  onclick="stats_export_report();" id="export-report"><i title="Exporter le rapport" class="far fa-file-archive"></i></li>
                        <li><a class="text-info" title="Éditer le rapport" href="index.php?module=statistic&page=edit_report&id=<?php echo $_['id']; ?>"><i class="far fa-edit"></i></a></li>
                        
                    <?php endif; ?>
                    <li onclick="stats_print_report()"><i  title="Imprimer le rapport" class="fas fa-print text-primary"></i></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<hr class="mb-2 mt-2">

<div id="filter-form" class="mb-3">
    <div class="row">
        <?php foreach($filters as $filter): ?>
            <div class="col-xl-2"><?php echo $filter->toHtml(); ?></div>
        <?php endforeach; ?>
        <div class="col-xl-2">
            <div class="btn btn-primary noLabel noPrint btn-process" onclick="stats_refresh_widget_content();"><i class="fas fa-chart-area"></i> Calculer</div>
        </div>
    </div>
</div>


<div id="report" class="mb-5 <?php echo $myUser->can('statistic','configure') ? 'statistic-configure': ''; ?>" data-id="<?php echo $report->id; ?>">
    <div id="gridtack" class="grid-stack"></div>

    <div id="widgetTemplate" class="hidden">
        <div class="widgetBlock">
            <div class="grid-stack-item-content" onclick="stats_select_widget(event,this)" ondblclick="stats_edit_widget_content(this);">
                <ul class="widgetMenu">
                    <li class="btn-widget-edit" onclick="stats_edit_widget_content(this);"><i class="far fa-edit text-info"></i></li>
                    <li class="btn-widget-delete" onclick="stats_delete_widget(this);"><i class="far fa-trash-alt text-danger"></i></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Modal creation widget -->
    <div class="modal fade" id="widgetModal" data-isadmin="<?php echo $myUser->superadmin; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edition du widget</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow: auto;"></div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</div>


<div class="alert alert-info no-data" role="alert">
  <strong>Ce rapport est vide !</strong>Aucune donnée n'est actuellement configurée pour ce rapport.<br>
  Vous pouvez ajouter des données a ce rapport en cliquant sur l'icone <i class="fas fa-plus"></i> en haut à droite de l'écran
</div>
