<?php 
global $myUser;
require_once(__DIR__.SLASH.'StatisticReport.class.php');
?>	

<h3>Rapports statistiques</h3>
<hr class="mt-2 mb-2">
<div class="row">
	<?php foreach(StatisticReport::loadAll(array(), array(' created ASC ')) as $report): ?>
		<div class="col-xs-3 report-bloc" data-id="<?php echo $report->id; ?>">
			<div class="report" style="background-color:<?php echo $report->color; ?>">
				<?php if($myUser->can('statistic','configure')): ?>
					<ul class="report-sheet-menu">
						<li>
							<a title="Éditer le rapport" href="index.php?module=statistic&page=edit_report&id=<?php echo $report->id; ?>">
								<i class="far fa-edit"></i>
							</a>
						</li>
						<li onclick="stats_delete_report(this);"><i title="Supprimer le rapport" class="far fa-trash-alt"></i></li>
					</ul>
				<?php endif; ?>
				<a href="index.php?module=statistic&page=report&id=<?php echo $report->id; ?>"><i class="<?php echo $report->icon; ?>"></i><?php echo $report->label; ?></a>
			</div>
		</div>
	<?php endforeach; ?>
	<?php if($myUser->can('statistic','configure')): ?>
		<div class="col-xs-3">
			<a class="report addReport" title="Créer un rapport" href="index.php?module=statistic&page=edit_report"><i class="fas fa-plus"></i></a>
		</div>
		<div class="col-xs-3">
			<div onclick="$('#import').click()" class="report importReport" title="Importer un rapport" href="index.php?module=statistic&page=edit_report"><i class="fas fa-file-upload"></i></div><input type="file" name="import" id="import" class="hidden" onchange="stats_report_import()" multiple accept=".zip">
		</div>
	<?php endif; ?>
</div>
