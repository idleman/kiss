<?php


?>
<h4>Widget</h4>
<label for="widgetLabel">Titre</label>
<input type="text" onblur="stats_save_widget()" id="widgetLabel" class="form-control" value="<?php echo html_decode_utf8($widget->label); ?>" placeholder="Titre du widget">

<div class="btn btn-dark mt-2" data-type="right" 
       				data-scope = 'statistic_widget' 
       				data-uid = '<?php echo $widget->id; ?>'
       				data-configure = "false" 
					data-recursive = "false"
       			><i class="fas fa-user-lock"></i> Permissions sur le widget</div>
<br/>
<hr/>

<?php if(!is_null($widget->view)){ ?>
<h4>Graphique</h4>
<div id="properties-form">
	<?php
	require_once(__DIR__.SLASH.'View.class.php');
	View::require_all();
	
	$widget->meta = json_decode($widget->meta,true);

	if(!is_array($widget->meta)) $widget->meta = array();
	if(!isset($widget->meta['properties'])) $widget->meta['properties'] = array();

	
	$fields = $widget->view::option();
	foreach($fields as $key=>$field):

		if(isset($widget->meta['properties'][$key])){
			$fields[$key]['value'] = $widget->meta['properties'][$key];
		}
	endforeach;

	$form = FieldType::toForm($fields);
	foreach($form as $element): ?>
		<div>
		<label><?php echo $element['label']; ?></label>
		<?php echo $element['input']; ?>
		</div>
	<?php endforeach; 
} ?>

	<br/>
	<div class="btn btn-dark" onclick="stats_properties_save()">Enregistrer</div>
</div>
