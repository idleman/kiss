<?php

//Cette fonction va generer un nouvel element dans le menu
function stats_plugin_menu(&$menuItems){
	global  $myUser;
	if(!$myUser->can('statistic','read')) return;
	$menuItems[] = array(
		'sort'=>100,
		'url'=>'index.php?module=statistic',
		'label'=>'Statistiques',
		'color'=>'#607d8b',
		'icon'=>'fas fa-chart-bar',
		'parent' => 'statistics'
	);
}

//Cette fonction va generer une page quand on clique sur Modele dans menu
function stats_plugin_page(){
	global $_;
	if(!isset($_['module']) || $_['module']!='statistic') return;

	User::check_access('statistic', 'read');

	$page = !isset($_['page']) ? 'reports' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	require_once($file);
		
}

function stats_plugin_install($id){
	if($id != 'fr.core.statistic') return;
	Entity::install(__DIR__);
	Entity::install(__DIR__.SLASH.'element');

	$default = new Connection();
	$default->label = 'Base locale';
	$default->handler = 'Sqlite';
	$default->meta = '{"name":".database"}';
	$default->save();

}
function stats_plugin_uninstall($id){
	if($id != 'fr.core.statistic') return;

	Entity::uninstall(__DIR__);
	Entity::uninstall(__DIR__.SLASH.'element');
}

//Droits global sur les statistiques
Right::register('statistic',array('label'=>'Gestion des droits sur le plugin Statistiques'));
//Droits ciblés sur un rapport
Right::register('statistic_report',array(
		'label'=>'Gestion des droits sur un rapport de statistiques (ciblé entité)',
		'global'=> false,
		'check' => function($action,$right){
			global $myUser;
			if($right->uid <= 0) throw new Exception('Id widget non spécifié');
			require_once(__DIR__.SLASH.'StatisticReport.class.php');
			$report = StatisticReport::getById($right->uid);
			if($myUser->login != $report->creator) throw new Exception('Seul le créateur de ce rapport ('.$report->creator.') peut définir des droits pour celui-çi');
		}
));
//Droits ciblés sur les widgets
Right::register('statistic_widget',array(
		'label'=>'Gestion des droits sur les widgets de statistiques (ciblé entité)',
		'global'=> false,
		'check' => function($action,$right){
			global $myUser;
			if($right->uid <= 0) throw new Exception('Id widget non spécifié');
			require_once(__DIR__.SLASH.'Widget.class.php');
			$widget = Widget::getById($right->uid);
			if($myUser->login != $widget->creator) throw new Exception('Seul le créateur de ce widget ('.$widget->creator.') peut définir des droits pour celui-çi');
		}
));
//Droits ciblés sur les filtres
Right::register('statistic_filter',array(
		'label'=>'Gestion des droits sur les filtres de statistiques (ciblé entité)',
		'global'=> false,
		'check' => function($action,$right){
			global $myUser;
			if($right->uid <= 0) throw new Exception('Id widget non spécifié');
			require_once(__DIR__.SLASH.'StatisticReport.class.php');
			$report = StatisticReport::getById($right->uid);
			if($myUser->login != $report->creator) throw new Exception('Seul le créateur de ce rapport ('.$report->creator.') peut définir des droits pour celui-çi');
		}
));


// comprend toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');

function stats_plugin_menu_setting(&$settingMenu){
	global $myUser;
	if($myUser->can('statistic', 'configure')){
		$settingMenu[]= array(
			'sort' =>0,
			'url' => 'setting.php?section=statistic',
			'icon' => 'fas fa-angle-right',
			'label' => 'Statistiques'
		);
	}
}

function stats_plugin_content_setting(){
	global $_;
	if(in_array($_['section'],array('statistic')) && file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
	require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


function stats_widget(&$widgets){
	
	require_once(__DIR__.SLASH.'..'.SLASH.'dashboard'.SLASH.'DashboardWidget.class.php');

	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'stats';
	$modelWidget->title = 'Statistiques';
	$modelWidget->icon = 'fas fa-chart-line';
	$modelWidget->background = '#a29bfe';
	$modelWidget->load = 'action.php?action=stats_widget_load';
	$modelWidget->js = [Plugin::url().'/js/main.js?v=1',Plugin::url().'/js/widget.js?v=3'];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v=0'];
	$modelWidget->configure = 'action.php?action=stats_widget_configure';
	$modelWidget->configure_callback = 'stats_widget_configure_save';
	$modelWidget->configure_init = 'stats_widget_configure_init';
	$modelWidget->description = "Affiche une graphique de rapport de statistiques";
	$widgets[] = $modelWidget;
}



Plugin::addCss("/css/main.css"); 
Plugin::addCss("/css/codemirror.css"); 
Plugin::addCss("/css/monokai.css"); 
Plugin::addCss("/css/gridstack.min.css"); 

Plugin::addJs("/js/main.js"); 
Plugin::addJs("/js/codemirror.js"); 

//Gestion du DataTables
Plugin::addCss("/css/datatables.min.css");
Plugin::addJs("/js/datatables.min.js"); 

foreach (glob(__DIR__.'/js/codemirror/*.js') as $path) {
	Plugin::addJs("/js/codemirror/".basename($path)); 
}

Plugin::addJs("/js/lodash.min.js");
Plugin::addJs("/js/gridstack.min.js"); 
Plugin::addJs("/js/gridstack.jQueryUI.min.js");

//Mapping hook / fonctions
Plugin::addHook("widget", "stats_widget");
Plugin::addHook("install", "stats_plugin_install");
Plugin::addHook("uninstall", "stats_plugin_uninstall"); 

Plugin::addHook("menu_main", "stats_plugin_menu"); 
Plugin::addHook("page", "stats_plugin_page");  
  

Plugin::addHook("menu_setting", "stats_plugin_menu_setting");    
Plugin::addHook("content_setting", "stats_plugin_content_setting");    


?>