<?php

class View{
	public static function views(){
		$views = array();
		foreach (glob(__DIR__.SLASH.'view/*.class.php') as $file) {
			require_once($file);
			$uid = str_replace('.class.php','',basename($file));
			$views[] =  $uid;
		}
		return $views;
	} 

	//@TODO: Possibilité de customiser les couleurs disponibles en
	//configuration et de laisser celles-ci par défaut si rien n'est défini
	public static function colorSet(){
		return array(
			'#1abc9c',
			'#9b59b6',
			'#3498db',
			'#34495e',
			'#f1c40f',
			'#e67e22',
			'#2ecc71',
			'#8e44ad',
			'#27ae60',
			'#e74c3c',
			'#d35400',
			'#c0392b',
			'#95a5a6',
			'#cd84f1',
			'#ffcccc',
			'#ff4d4d',
			'#ffaf40',
			'#ff9f1a',
			'#ff3838',
			'#ffb8b8',
			'#c56cf0',
			'#7efff5',
			'#18dcff',
			'#7d5fff',
			'#7158e2',
			'#17c0eb',
			'#3ae374',
			'#F8EFBA',
			'#3B3B98',
			'#FC427B',
			'#BDC581',
			'#55E6C1',
			'#B33771',
			'#FEA47F',
			'#6D214F',
			'#ffdd59',
			'#0fbcf9',
			'#34e7e4',
			'#d2dae2',
			'#808e9b',
			'#f53b57',
			'#474787',
			'#ffb142',
			'#34ace0',
			'#2c2c54',
			'#b33939',
			'#20bf6b',
			'#26de81',
			'#fd9644',
			'#fc5c65',
			'#FFEB3B',
			'#FFC107',
			'#CDDC39',
			'#795548',
			'#FF5722',
			'#E91E63',
			'#673AB7',			
			'#2196F3',
			'#8BC34A',
			'#607D8B',
			'#9E9E9E',
			'#e74c3c',
			'#f1c40f',
			'#d35400',
			'#c0392b',
			'#ff7675',
			'#00cec9',
			'#fd79a8',
			'#ffeaa7'
		);
	}


	public static function require_all(){
		foreach(glob(__DIR__.SLASH.'view'.SLASH.'*.class.php') as $view) require_once($view);
	}

	public static function array_depth($array) {
	    $max_depth = 1;

	    foreach ($array as $value) {
	        if (is_array($value)) {
	            $depth = self::array_depth($value) + 1;
	            if ($depth > $max_depth) $max_depth = $depth;
	        }
	    }
	    return $max_depth;
	}
}
?>