<?php
require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');
class Html{
	public static $LABEL = "Html";
	public static $DESCRIPTION = "Paragraphe de texte dynamique";
	public static $ICON = "fas fa-font";

	public static function option(){
		$options = array();
		return $options;
	}

	public static function toHtml($label,$data){
		$html = '';
		if(!isset($options['properties'])) $options['properties'] = array();
		try{
			$data = $data['data'];
			if(!isset($data) || !is_string($data) ) throw new Exception("Données d'entrées incorrectes.<br> Les données de ce paragraphe doivent être présentées sous la forme d'une chaine de caractère html");
			if(isset($label)) $html.='<h5 class="text-center">'.$label.'</h5>';
			$html .= $data;
		
		}catch(Exception $e){
			$html .= '<h5 class="text-center">'.(isset($label)?$label:'Affichage impossible :').'</h5><br>'.$e->getMessage();
		}
		return $html ;
	
	}

}
?>