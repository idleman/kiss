<?php
require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');
class Bar{
	public static $LABEL = "Histogramme";
	public static $DESCRIPTION = "Graphique en barres verticales";
	public static $ICON = "far fa-chart-bar";
	function __construct(){}

	public static function option(){
		$options = array();
		
		for($i=0;$i<12;$i++){
			$options['color-'.$i] = array('label'=>'Couleur '.$i,'type'=>'color');
		}
		return $options;
	}

	public static function toHtml($label,$data,$options){
		require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');
		if(!isset($options['properties'])) $options['properties'] = array();
		$html = '';
		try{
			$data = $data['data'];
			$colors = View::colorSet();
			shuffle($colors);

			$colorNumber = 0;
			foreach($options['properties'] as $key=>$value){
				if(strpos( $key,'color-')===false || empty($value)) continue;
				$colors[$colorNumber] = $value;
				$colorNumber++;
			}

			$background = implode('","',$colors);


			$keys = array_keys($data);
			$values = array_values($data);

			if(count($data)==0) throw new Exception("<h4 class='text-center'>Aucune donnée</h4>");
			if(!isset($data) || View::array_depth($data)>1) throw new Exception("Données d'entrées incorrectes.<br> Les données de ce graphique doivent être présentées sous la forme d'un simple tableau clé valeur<br>
<pre>
array(
	'Légende 1' => 12,
	'Légende 2' => 14,
	'Légende 3' => 15
);
</pre>");
			if(isset($label)) $html.='<h5 class="text-center">'.$label.'</h5>';
			/*
			$html .= '<canvas  data-background="[\''.$background.'\']" ';
			$html .= 'data-values="['.implode(',',$values).']" ';
			$html .= 'data-keys=\'["'.implode('","',$keys).'"]\' data-object="chart" data-type="bar"></canvas>';
			*/

			$hideLegend = (!isset($options['properties']['show-legend']) || $options['properties']['show-legend']!=='1');

			$html .= '<canvas  data-colors=\'["'.$background.'"]\' ';
			$html .= 'data-values="['.implode(',',$values).']" ';
			$html .= 'data-labels=\'["'.implode('","',$keys).'"]\' data-type="bar"></canvas>';
			



		}catch(Exception $e){
			$html .= '<h5 class="text-center">'.(isset($label)?$label:'Affichage impossible :').'</h5><br>'.$e->getMessage();
		}
		return $html;
	}
}
?>