<?php
require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');

class Gauge{
	public static $LABEL = "Graphique jauge";
	public static $DESCRIPTION = "Graphique en forme de jauge";
	public static $ICON = "fas fa-tachometer-alt";
	function __construct(){}

	public static function option(){
		$options = array();
	
		return $options;
	}

	public static function toHtml($label,$data,$options = array()){
		$html = '';
		if(!isset($options['properties'])) $options['properties'] = array();
		try{

			$data = $data['data'];

		

			$keys = array_keys($data);
			$values = array_values($data);

			if(count($data)==0) throw new Exception("<h4 class='text-center'>Aucune donnée</h4>");
			if(!isset($data) || View::array_depth($data)>1 || count($data)!=3 ) throw new Exception("Données d'entrées incorrectes.<br> Les données de ce graphique doivent être présentées sous la forme d'un simple tableau clé valeur<br>
<pre>
array(
	'Atteint' => 12,
	'Objectif' => 14,
	'Unité' => '€',
);
</pre>");
				if(isset($label)) $html.='<h5 class="text-center">'.$label.'</h5>';

				$html .= '<canvas ';
				if(isset($options['height'])) $html .= 'data-height="'.$options['height'].'"';
		
				$html .='  data-values="['.$values[0].','.$values[1].']" data-unity="'.$values[2].'" data-labels=\'["'.implode('","',$keys).'"]\'   data-type="gauge" id="myout" width="100" height="100"></canvas>';

		}catch(Exception $e){
			$html .= '<h5 class="text-center">'.(isset($label)?$label:'Affichage impossible :').'</h5><br>'.$e->getMessage();
		}
		return $html;
	}

}
?>