<?php
require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');
class Line{
	public static $LABEL = "Graphique Linéaire";
	public static $DESCRIPTION = "Graphique lineaire a deux axes";
	public static $ICON = "far fa-chart-line";
	function __construct(){}

	public static function option(){
		$options = array();
	
		$options['color-1'] = array('label'=>'Couleur','type'=>'color');
		
		return $options;
	}

	public static function toHtml($label,$data,$options){
		$html = '';
		if(!isset($options['properties'])) $options['properties'] = array();
		try{
			$data = $data['data'];
			$colors = View::colorSet();
			shuffle($colors);

			if(!empty($options['properties']['color-1'])){
				$background = $options['properties']['color-1'];
			}else{
				$background = array_pop($colors);
			}
			

			$keys = array_keys($data);
			$values = array_values($data);
			if(count($data)==0) throw new Exception("<h4 class='text-center'>Aucune donnée</h4>");
			if(!isset($data) || View::array_depth($data)>1 ) throw new Exception("Données d'entrées incorrectes.<br> Les données de ce graphique doivent être présentées sous la forme d'un simple tableau clé valeur<br>
				<pre>
array(
	'Légende 1' => 12,
	'Légende 2' => 14,
	'Légende 3' => 15
);
				</pre>");
			if(isset($label)) $html.='<h5 class="text-center">'.$label.'</h5>';
			
			$html .= '<canvas data-color=\''.$background.'\' ';

			$html .= 'data-values="['.implode(',',$values).']" data-labels=\'["'.implode('","',$keys).'"]\'  data-type="line" id="myout"></canvas>';

		}catch(Exception $e){
			$html .= '<h5 class="text-center">'.(isset($label)?$label:'Affichage impossible :').'</h5><br>'.$e->getMessage();
		}
		return $html;
	}

}
?>