<?php
require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');

class Doughnut{
	public static $LABEL = "Graphique Donuts";
	public static $DESCRIPTION = "Graphique en camembert creux";
	public static $ICON = "far fa-chart-pie";
	function __construct(){}

	public static function option(){
		$options = array();
		$options['show-legend'] = array('label'=>'Afficher les légendes','type'=>'boolean');
		for($i=0;$i<10;$i++){
			$options['color-'.$i] = array('label'=>'Couleur '.$i,'type'=>'color');
		}
		return $options;
	}

	public static function toHtml($label,$data,$options = array()){
		$html = '';
		if(!isset($options['properties'])) $options['properties'] = array();
		try{

			$data = $data['data'];
			$colors = View::colorSet();
			shuffle($colors);

			$colorNumber = 0;
			foreach($options['properties'] as $key=>$value){
				if(strpos( $key,'color-')===false || empty($value)) continue;
				$colors[$colorNumber] = $value;
				$colorNumber++;
			}

			$background = implode('","',$colors);

			$keys = array_keys($data);
			$values = array_values($data);

			if(count($data)==0) throw new Exception("<h4 class='text-center'>Aucune donnée</h4>");
			if(!isset($data) || View::array_depth($data)>1 ) throw new Exception("Données d'entrées incorrectes.<br> Les données de ce graphique doivent être présentées sous la forme d'un simple tableau clé valeur<br>
<pre>
array(
	'Légende 1' => 12,
	'Légende 2' => 14,
	'Légende 3' => 15
);
</pre>");
				if(isset($label)) $html.='<h5 class="text-center">'.$label.'</h5>';

				$html .= '<canvas ';
				if(isset($options['height'])) $html .= 'data-height="'.$options['height'].'"';
				if(isset($options['properties']['show-legend']) && $options['properties']['show-legend']=='1') $html .= 'data-legend="true" ';
				$html .=' data-colors=\'["'.$background.'"]\' data-values="['.implode(',',$values).']" data-labels=\'["'.implode('","',$keys).'"]\'  data-type="doughnut" id="myout" width="100" height="100"></canvas>';

		}catch(Exception $e){
			$html .= '<h5 class="text-center">'.(isset($label)?$label:'Affichage impossible :').'</h5><br>'.$e->getMessage();
		}
		return $html;
	}

}
?>