<?php
require_once(__DIR__.SLASH.'..'.SLASH.'View.class.php');
class Table{

	public static $LABEL = "Tableau";
	public static $DESCRIPTION = "tableau de données";
	public static $ICON = "fas fa-table";

	public static function toArray($data){
		$array = array(array(
			'Erreur' => 'Données d\'entrées incorrectes.Les données de ce graphique doivent être présentées sous la forme d\'un tableau de tableau clé/valeurs '
		));
		$data = $data['data'];
		if(isset($data) && is_array($data) && ((isset($data[0]) && is_array($data[0]))))
			$array = $data;

		return $array ;
	}

	public static function option(){
		$options = array();
		$options['allow-html'] = array(
				'label'=>'Autoriser le HTML dans la table',
				'type' => 'boolean'
		);
		$options['pageLength'] = array(
				'label'=>'Nombre de résultats par page',
				'type' => 'list',
				'values'=> array('10'=>'10','20'=>'20','40'=>'40','-1'=>'Tous')
		);
		return $options;
	}

	public static function toHtml($label,$data,$options = array()){
		$html = "Données d'entrées incorrectes.<br> Les données de ce graphique doivent être présentées sous la forme d'un tableau de tableau clé/valeurs <br>
<pre>
array(
	array(
		'Col 1' => 12,
		'Col 2' => 14,
		'Col 3' => 15
	),
	array(
		'Col 1' => 'Hello',
		'Col 2' => 'World',
		'Col 3' => '!!!'
	)
);
</pre>";

		if(!isset($options['properties'])) $options['properties'] = array();

		$data = $data['data'];

		if(isset($data) && is_array($data) && (((isset($data[0]) && is_array($data[0]) || count($data)==0)))) {

			$html = '';

			if(isset($label)) $html.='<h5 class="text-center p-4">'.$label.' <i onclick="stats_export_view_table(this)" title="Exporter au format Excel" class="far fa-file-excel pointer text-success"></i></h5>';
			$html .= '
				<table data-type="data-table" data-pageLength="'.(isset($options['properties']['pageLength']) && is_numeric($options['properties']['pageLength']) ? $options['properties']['pageLength'] : 20).'" class="table table-stripped table-hover table-bordered">';
			$html .= '<thead class="thead-inverse-blue">';
			$html .= '<tr>';

			if(isset($data[0])){
				foreach($data[0] as $column=>$value)
					$html .= '<th>'.htmlspecialchars($column).'</th>';
			}else{
				$html .= '<th class="text-center">Aucune donnée</th>';
			}
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';

			foreach($data as $line){
				$html .= '<tr>';
				foreach($line as $column=>$value){
					if(!isset($options['properties']['allow-html']) || $options['properties']['allow-html']!=='1') $value = htmlspecialchars($value);
					$html .= '<td>'.$value.'</td>';
				}
				$html .= '</tr>';
			}
			$html .= '<tbody>';
			$html .= '</table>';
		}
		return $html ;
	}

}
?>