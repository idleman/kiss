<?php 

require_once(__DIR__.SLASH.'Filter.class.php');

?>

<h2>Documentation sur les filtres</h2>


<h5 class="pt-2">Assigner une valeur par défaut pour un filtre</h5>
<div id="filterMeta">
    <h5>Macros :</h5> Il est possible d'utiliser les mots clés suivants dans le champ <code>Valeur par défaut</code>  : 
    <ul>
        <?php foreach(Filter::getMetaTerms() as $key=>$filter): ?>
        <li><strong>{{<?php echo $key ?>}}</strong> : <?php echo $filter['description']; ?> ex : <code><?php echo $filter['value']; ?></code></li>
        <?php endforeach; ?>
    </ul>
</div>
nb : pour les valeurs par defaut de filtres numeriques, il est possible d'ajouter ou enlever un nombre avec la syntaxe <code>{{date.month-1}}</code>

<h5 class="pt-2">Utilisation des filtres dans une requête</h5>
<p>Pour utiliser un filtre dans une requete SQL, Récuperez le <code>slug</code> du filtre et inserez le dans le code SQL délimité par des doubles
accolades,ex : pour le filtre <code>Date début</code> ayant pour slug <code>date-debut</code> ma requete sera <br>
<code>SELECT * FROM matable WHERE date = {{date-debut}}</code>
</p>


<h5 class="pt-2">Utilisation des conditions de filtres dans une requête</h5>
<p>Dans le cas ou le filtre n'est pas remplis par l'utilisateur, il est possible de conditionner la requete avec la syntaxe 
	<code>{{?monslug}} sql a executer si le filtre est définis {{/monslug}}</code> exemple :
	<code>SELECT * FROM matable {{?monslug}}WHERE date = {{date-debut}}{{/monslug}}</code> dans cet exemple la partie <code>WHERE date = {{date-debut}}</code> n'est executée que si le filtre contient au minimum un caractère.
</p>

<h5 class="pt-2">Utilisation des filtres dans un traitement PHP</h5>
Si vous souhaitez utiliser les filtres dans un traitement php, ils sont disponible via la variable <code>$filters</code>.
ex: pour utiliser le filtre <code>Date début</code> ayant pour slug <code>date-debut</code> il faut executer <code>$filters['date-debut']</code>



