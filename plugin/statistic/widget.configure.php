<?php 

try{
	User::check_access('statistic','read');
	require_once(__DIR__.SLASH.'Widget.class.php');
	$label = $id = '';
	if($widget->data('stats')!=''){
		$statsWidget = Widget::getById($widget->data('stats'));
		if(!$statsWidget) $statsWidget = new Widget();
		$label = $statsWidget->label;
		$id = $statsWidget->id;
	}
?>
<div id="stats-widget-form">
	<label><i class="fas fa-chart-bar"></i> Statistique à afficher :</label>
	<input class="form-control" type="text" value="<?php echo $label; ?>" data-id="<?php echo $id; ?>" id="widget-stats" placeholder="Titre de la statistique à afficher">
</div> 
<?php }catch(Exception $e){ ?>
<div class="alert alert-warning text-center m-0" role="alert">
	<?php echo  $e->getMessage(); ?>
</div>
<?php } ?>