<?php
global $myUser;
User::check_access('statistic','edit');

require_once(__DIR__.SLASH.'Widget.class.php');
require_once(__DIR__.SLASH.'View.class.php');

$widget =  Widget::provide();

if($myUser->login != $widget->creator  && !$myUser->can('statistic','edit',$widget->id) ) throw new Exception("Edition non permise");


$view = $widget->view;
$widgetLabel = $widget->label == '' ? 'Widget' : $widget->label;

?>

		<div class="row stats-widget-board" id="widget" data-id="<?php echo $widget->id; ?>">
			

			<!-- panel elements -->
			<div class="stats-element-panel">
				<div class="widget-tree">
					<h5>
						<span id="widgetPropertiesTitle"><?php echo ($widget->label == '' ? '' : 'Widget : '); ?></span>
						<a id="widgetProperties" onclick="stats_select_properties_tab();"><?php echo $widgetLabel; ?></a>
					</h5>
					<table class="table table-dark stats-widget-element" id="elements">
						<thead>
							<tr>
								<th>
									<div class="form-row">
				  						<div class="form-group col-md-12">
											<label for="label">Libellé</label>
											<input class="form-control-sm" id="label" name="label" type="text">
										</div>
									</div>
								</th>
								<th>
									<div class="form-row">
				  						<div class="form-group col-md-6">
				  							<label for="type">Type</label>
				  							<select class="form-control-sm" id="type" name="type">
				  								<?php foreach(glob(__DIR__.SLASH.'element'.SLASH.'*.class.php') as $file): 
				  									require_once($file);
				  									$className = str_replace('.class.php','',basename($file));
				  									$instance = new $className();
				  									?>
				  									<option value="<?php echo $className; ?>"><?php echo $instance->typeLabel; ?></option>
				  								<?php endforeach; ?>

											</select>
				  						</div>
				  					</div>
								</th>
								<th>
									<div class="widget-btn widget-btn-sm" onclick="stats_save_widget_element();">
										<i class="fas fa-check no-margin"></i>
									</div>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr data-id="{{id}}" data-type="{{type}}" class="hidden" onclick="stats_edit_widget_element(this)">
								<td><i class="{{icon}}" title="{{typeLabel}}"></i>{{label}}<br><code>id: {{slug}}</code></td>
								<td colspan="2">
									<div class="right">
										<div class="widget-btn widget-btn-sm d-inline-block btn-element-sort" title="Déplacer">
											<i class="fas fa-arrows-alt-v"></i>
										</div>
										<div class="widget-btn widget-btn-sm d-inline-block" title="Modifier" onclick="stats_rename_widget_element(this,event);"><i class="fas fa-pencil-alt no-margin"></i></i></div>
										<div class="widget-btn widget-btn-sm d-inline-block" title="Supprimer"  onclick="stats_delete_widget_element(this,event);"><i class="fas fa-times no-margin"></i>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
					<div class="view-block">
						<h5 class="text-muted text-bold">AFFICHAGE</h5>
						<ul id="view" class="stats-widget-element">
							<?php foreach (View::views() as $item) { ?>
								<li onclick="stats_select_widget_view(this);"  title="<?php echo $item::$LABEL.' : '.$item::$DESCRIPTION; ?>" class="<?php echo $view==$item ? 'selected':'' ?>" data-uid="<?php echo $item; ?>">
									<i class="fa <?php echo $item::$ICON; ?>"></i>
									
								</li>
							<?php } ?>
						</ul>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<!-- editeur -->
			<div class="stats-workspace-panel" id="workspace">
				<ul class="nav nav-tabs" id="widgetTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="edit-tab" data-toggle="tab" href="#edit-panel" role="tab" aria-controls="edit-panel" aria-selected="true">Editeur</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="output-tab" data-toggle="tab" href="#output-panel" role="tab" aria-controls="output-panel" aria-selected="false">Données</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="preview-tab" data-toggle="tab" href="#preview-panel" role="tab" aria-controls="preview-panel" aria-selected="false">Rendu</a>
					</li>
					<li  class="nav-item" id="widgetPropertiesTab" onclick="stats_properties_load();">
						<a class="nav-link" id="properties-tab" data-toggle="tab" href="#properties-panel" role="tab" aria-controls="properties-panel" aria-selected="false">Propriétés</a>
					</li>
				</ul>

				<button id="widgetCloseBtn" type="button" onclick="stats_refresh_widget_content();" class="close widget-btn" data-dismiss="modal" aria-label="Close">
          			<span>&times;</span>
        		</button>

				<div class="tab-content" id="widgetTabContent">
					<div class="tab-pane fade show active" id="edit-panel" role="tabpanel" aria-labelledby="edit-tab"></div>
					<div class="tab-pane fade" id="output-panel" role="tabpanel" aria-labelledby="output-tab"></div>
					<div class="tab-pane fade" id="preview-panel" role="tabpanel" aria-labelledby="preview-tab"></div>
					<div class="tab-pane fade" id="properties-panel" role="tabpanel" aria-labelledby="properties-tab">
						<div class="form-row">
							<div class="form-group col-md-12 properties-block"></div>
						</div>		
						<div class="element-properties"></div>

					</div>
				</div>
			</div>
			<!--<div id="scripts"></div>-->
		</div>
