<?php 
global $myUser;
User::check_access('statistic','configure');

$handlers = array();

foreach(glob(__ROOT__.SLASH.'connector'.SLASH.'*.class.php') as $classFile){
	require_once($classFile);
	$className = str_replace('.class.php','',basename($classFile));
	$handlers[$className] = $className::label;
}

?>
<div class="row">
	<div class="col-md-12">
		<br>
		<div onclick="stats_save_connection();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
		<h3>Réglages Statistiques</h3>
		<hr/>
		<h5 class="panel-heading">Configuration de connexion</h5>
		  
		<small class="text-muted">L'utilisateur doit avoir les droits "SELECT" sur les tables concernées pas les statistiques </small>
		    
		
		<div id="connectionForm" class="row" data-action="stats_save_connection" data-id="">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<label for="label">Libellé  </label>
						<input id="label" class="form-control" placeholder="Ma connexion 1" type="text"><br>
					</div>
					<div class="col-md-6">
						<label for="database">Type de base  </label>
						<select class="form-control" id="handler" name="handler" onchange="stats_connection_handler_change()">
							<option value="">-</option>
							<?php foreach($handlers as $class=>$label): ?>
								<option value="<?php echo $class ?>"><?php echo $label; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 stats-connection-info"></div>
				</div>
				
				
			</div>
		
		</div>
		<hr>
		<div class="panel panel-default">
			<legend class="panel-heading">Connexions existantes </legend>
			<table id="connections" class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Libellé</th>
						<th>Type</th>
					
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr data-id="{{id}}" class="hidden">
						<td>{{id}}</td>
						<td>{{label}}</td>
						<td>{{handler}}</td>
						
						<td class="text-right">
							<div onclick="stats_connection_test(this)" title="Tester la connexion" class="btn btn-dark btn-squarred btn-mini btn-test"><i class="fas fa-vial"></i></div>
							<div onclick="stats_edit_connection(this)" class="btn btn-info btn-squarred btn-mini"><i class="fas fa-pencil-alt"></i></div>
							<div onclick="stats_delete_connection(this)" class="btn btn-danger btn-squarred btn-mini"><i class="fas fa-times"></i></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

