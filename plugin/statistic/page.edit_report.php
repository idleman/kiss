<?php
global $myUser;
User::check_access('statistic','configure');

require_once(__DIR__.SLASH.'StatisticReport.class.php');
require_once(__DIR__.SLASH.'Connection.class.php');
require_once(__DIR__.SLASH.'Filter.class.php');
require_once(__DIR__.SLASH.'View.class.php');
$report = StatisticReport::provide();
if($report->id==0){
    $report->label = 'Nouveau rapport';
    $report->color = '#8bc34a';
    $report->icon = 'fas fa-chart-bar';
}

?>
<div class="row" id="report" data-id="<?php echo empty($report->id)?'0':$report->id; ?>">
    <div class="col-xl-12">
        
        <div class="tab-content" id="statsTabContent">
            <div class="tab-pane show active" id="global" role="tabpanel" aria-labelledby="global-tab">            
                <div class="row">
                    <div class="col-xl-10">
                       
                        
                        <div class="input-group">
                                <span class="icon-chooser" id="basic-addon-icon-statistic">
                                    <input id="icon" data-type="icon" name="icon" class="form-control" placeholder="" value="<?php echo $report->icon==''? 'fas fa-question': $report->icon; ?>" type="text">
                                </span>
                                <input type="text"  class="form-control" id="label" value="<?php echo $report->label; ?>">
                                
                                <input type="text" data-type="color" class="form-control" value="<?php echo $report->color; ?>" id="color">

                                 <div class="input-group-append">
                                <div class="btn btn-small btn-dark right"  title="Permissions sur le rapport" data-tooltip data-placement='bottom' data-type="right" 
                                    data-scope = 'statistic_report',
                                    data-uid = <?php echo $report->id; ?>
                                ><i class="fas fa-user-lock"></i></div>
                                </div>
                            <div class="input-group-append">
                                <div class="btn btn-success" onclick="stats_save_report()"><i class="fas fa-check"></i> Enregistrer</div>
                            </div>
                        </div>

                    </div>
                    <div class="col-xl-2">
                         <a class="btn btn-dark m-auto right edit-only btn-open"  href="index.php?module=statistic&page=report&id=<?php echo $report->id; ?>" role="tab" aria-controls="filters" aria-selected="false"><i class="fas fa-book-reader"></i> Ouvrir le rapport</a>
                    </div>
                </div>
            </div>
            <div class="edit-only">
 

            <hr/>

            <div id="filters">
                <legend><i class="fas fa-filter"></i> Filtres du rapport</legend>
                <div class="row" id="filter-form" class="filter-form">
                    <div class="col-xl-4">
                        <label>Libellé :</label>
                        <input class="form-control" id="filterLabel" type="text">
                    </div>

                    <div class="col-xl-4">
                        <label>Type :</label> 
                        <select class="form-control" onchange="stats_change_filter_type()"  id="filterType">
                            <?php foreach(Filter::types() as $uid=>$type): ?>
                            <option data-placeholder="<?php echo isset($type['placeholder'])?$type['placeholder']:''; ?>" value="<?php echo $uid; ?>" <?php echo $type['datasource']?'data-source="true"':''; ?>><?php echo $type['label']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-xl-4">
                        <label>Valeur par défaut <a href="index.php?module=statistic&page=documentation">(?)</a> :</label> 
                        <div class="input-group">
                            <input class="form-control" type="text"  id="filterDefault">
                            <div class="btn btn-success ml-2" onclick="stats_add_filter();"><i class="fas fa-check"></i></div>
                        </div>
                    </div>

                    <div class="col-xl-12 filter-source-container hidden"><br>
                        <label>Source de données :</label> 
                        <textarea id="filterDataSource" class="form-control" placeholder="id:<<id>> 
                            query:<<query>> 
                            plain:
                            key1:value1
                            key2:value2"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12"><br>
                        <table class="table table-stripped" id="filterTable">
                            <thead>
                                <tr>
                                    <th>Libellé <small>(affiché dans l'interface)</small></th>
                                    <th>Slug <small>(à utiliser dans la requête)</small></th>
                                    <th>Type</th>
                                    <th>Valeur par défaut</th>
                                    <th class="text-right"></th>
                              </tr>
                          </thead>
                          <tbody>
                                <tr class="hidden" data-id="{{id}}">
                                    <td>{{label}}</td>
                                    <td><input type="text" readonly="readonly" class="form-control" value="{{slug}}" onclick="$(this).select();"></td>
                                    <td>{{type}}</td>
                                    <td>{{default}}</td>
                                    <td class="text-right">

                                        <div class="btn btn-mini btn-dark"  title="Permissions sur le filtre" data-tooltip data-tpl-type="right" 
                                    data-scope = 'statistic_filter' 
                                    data-uid = "{{id}}"
                                    data-firm = "0"
                                    data-read = "false"
                                    data-delete = "false"
                                ><i class="fas fa-user-lock"></i></div>

                                        <div class="btn btn-mini pointer text-muted btn-move"><i class="fas fa-arrows-alt"></i></div>
                                        <div class="btn btn-info btn-squarred btn-mini pointer" onclick="stats_edit_filter(this);"><i class="fas fa-pencil-alt"></i></div>
                                        <div class="btn btn-danger btn-squarred btn-mini pointer" onclick="stats_delete_filter(this, event);"><i class="fas fa-times"></i></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                Consultez <a href="index.php?module=statistic&page=documentation">la documentation</a> pour savoir comment utiliser les filtres.
            </div>

            </div>
           
        </div>
    </div>
</div>
