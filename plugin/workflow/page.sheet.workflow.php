<?php 
User::check_access('workflow','read');
require_once(__DIR__.SLASH.'Workflow.class.php');
require_once(__DIR__.SLASH.'WorkflowEvent.class.php');
require_once(__DIR__.SLASH.'WorkflowEffect.class.php');
$workflow = Workflow::provide();
global $conf;
if(!$workflow) throw new Exception("Ce workflow n'existe pas ou a été supprimé");


?>
<div class="workflow">
	<div id="workflow-form" class="row workflow-form" data-action="workflow_workflow_save" data-id="<?php echo $workflow->id; ?>">
		<div class="col-md-12">
			
			<div class="row">
				<div class="col-md-9">
					<h3>Workflow</h3>
				</div>
				<div class="col-md-3 text-right">
					<a class="btn px-1 visible-on-update" title="Historique" data-scope="workflow" data-uid="<?php echo $workflow->id; ?>" data-type="history"><i class="far fa-comment-dots"></i></a>
					<a href="index.php?module=workflow" class="btn btn-dark btn-small mr-1">Retour</a>
					<div onclick="workflow_workflow_save();" class="btn btn-success  btn-small"><i class="fas fa-check"></i> Enregistrer</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-7">
					<!-- Nom, couleur, icone du workflow -->
					<div class="input-group"> 
						<input id="icon" name="icon" data-type="icon" class="form-control input-group-prepend" placeholder="" value="<?php echo $workflow->icon; ?>" type="text">
						<input id="label" name="label" class="form-control" placeholder="Libellé" value="<?php echo $workflow->label; ?>" type="text">
						<input id="color" name="color" data-type="color" title="Couleur" class="form-control" placeholder="" value="<?php echo $workflow->color; ?>" type="text">
					</div>
					
					<!-- Type de workflow -->
					<div class="input-group mt-1"> 
						<div class="input-group-prepend">
							<div class="input-group-text">Type</div>
						</div>
						<select id="type" name="label" class="form-control create-only" onchange="workflow_workflow_type_change()">
								<?php foreach(Workflow::types() as $slug=>$type): 
									if($slug == Workflow::TYPE_ALL) continue;

									?>
									<option <?php echo $slug==$workflow->type?'selected="selected"':''; ?> value="<?php echo $slug; ?>">
										<?php echo $type['label']; ?>	
									</option>
								<?php endforeach; ?>
						</select>
					</div>

					<!-- Entité de workflow -->
					<div class="input-group mt-1"> 
						<div class="input-group-prepend entity-group">
							<div class="input-group-text">Entité</div>
						</div>
						<select id="entity" name="label" class="form-control entity-group create-only" >
							<option value="">-</option>
							<?php foreach(WorkflowEvent::events(null,Workflow::TYPE_ENTITY) as $event): ?>
								<option <?php echo $event['entity']['slug']==$workflow->entity?'selected="selected"':''; ?> value="<?php echo $event['entity']['slug']; ?>">
									<?php echo $event['entity']['label']; ?>	
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-md-5">

					<!-- Evenements -->
					<div class="workflow-event-bloc visible-on-update">
						 <div id="workflow-event-form" data-action="workflow_workflow_event_save" data-id="">
			                <div class="input-group input-group-sm">
			                   <div class="input-group-prepend">
				                   	<div class="input-group-text">
				                   		Lancer le workflow
				                   	</div>
			                   </div>
			                    <select id="event" name="label" class="form-control" onchange="workflow_workflow_event_save()"></select>
								
		                	</div>
		                </div>

						<ul id="workflow-events" class="table table-striped workflow-events" data-entity-search="workflow_event_search">
			                <li data-id="{{id}}" data-slug="{{slug}}" class="hidden">
				               

				                 {{label}} 
								<div class="right">
									<i title="Supprimer l'événement" onclick="workflow_workflow_event_delete(this);" class="far fa-trash-alt text-danger mr-1 pointer"></i>
									<i title="Executer le workflow" onclick="workflow_workflow_run(this);" class="fas fa-cog pointer text-muted"></i> 
				                </div>


			                </li>
				        </ul>
			    	</div>
				</div>
			</div>
			
			
			<hr>


			

			<div class="row visible-on-update">


				<!-- causes -->
				 <div class="col-md-7 workflow-causes">
					<h6>CAUSES</h6>
					<select id="cause-filters-template"  class="hidden" data-only-advanced="true" data-default='{{enabled}}'>{{{filters}}}</select>
				</div>

				<!-- effets -->
				<div class="col-md-5 workflow-effects">
					<h6>EFFETS</h6>
						<table id="workflow-effects" class="table table-striped " data-entity-search="workflow_cause_effect_search">
				            <thead>
				                <tr id="workflow-effect-form"  data-action="workflow_workflow_effect_save" data-id="">
				                    <th colspan="2">Ajouter un effet :   
										<select data-type="dropdown-select" class="d-inline-block" id="effect-type">
							            	<?php 
							        		foreach(WorkflowEffect::types() as $effect): ?>
							            	<option selected="selected" value="<?php echo $effect::manifest('slug'); ?>" style="background-color:#333;color:#fff;" data-icon="<?php echo $effect::manifest('icon'); ?>"><?php echo $effect::manifest('label'); ?></option>
							            	<?php endforeach; ?>
							            </select>
							            <div onclick="workflow_workflow_effect_save();" class="btn btn-success btn-small right"><i class="fas fa-plus"></i></div>
							            <div class="clear"></div>
				                    </th>
				                </tr>
				            </thead>
				            <tbody>
				                <tr data-id="{{id}}" data-slug="{{type.slug}}" class="hidden">
					                <td class="align-middle"><i class="{{type.icon}}"></i> {{type.label}}
				                        <div class="btn-group btn-group-sm right" role="group">
				                     		<div class="btn btn-edit" title="Éditer workflow_effect" onclick="workflow_workflow_effect_edit(this);"><i class="fas fa-pencil-alt text-muted"></i></div>
				                            <div class="btn" title="Déplacer"><i class="fas fa-arrows-alt text-muted"></i></div>
				                            <div class="btn text-danger" title="Supprimer workflow_effect" onclick="workflow_workflow_effect_delete(this);"><i class="far fa-trash-alt"></i></div>
				                        </div>
										<div class="workflow-effect-form">
											{{{form}}}
										</div>
					                </td>
				                </tr>
				           </tbody>
        			</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Les filtres custom de causes sont contenus ici -->
<div class="workflow-cause-filters"></div>