<?php 
/*
	Effet de workflow
	Définit une variable (scopée workflow) à la valeur indiquée
*/
class VariableEffect{

	//Descriptif du type d'effet
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'var',
			'label' => 'Définir une variable',
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'fas fa-dollar-sign',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}

	//méthode d'affichage de l'effet
	public static function form($item){
		$html  = '';
		$class = get_called_class();
		
		//$class::manifest('icon')
		ob_start();
		?>
		
		<div class="input-group">
			<div class="input-group-text input-group-prepend">
				Variable  : 
			</div>
			<input type="text" data-id="variable-slug" class="form-control" value="<?php echo isset($item['variable-slug']) ? $item['variable-slug'] : '' ?>" placeholder="Ma variable" required>
		</div>
		<div class="input-group mt-1">
			<div class="input-group-text input-group-prepend">
				Valeur  : 
			</div>
			<input type="text" data-id="variable-value" class="form-control" value="<?php echo isset($item['variable-value']) ? $item['variable-value'] : '' ?>" placeholder="Valeur de la variable">
		</div>
			

		
		<?php
		$html = ob_get_clean();
		return $html;
	}

	public static function run($effect,$parameters = array()){
		global $conf;
		$logs = '';
		
		if( in_array($parameters['workflow']['type'] , array( Workflow::TYPE_ENTITY, Workflow::TYPE_LIST))  ){
			if(isset($parameters['current'])) $parameters['current'] = $parameters['current']->toArray();
			if(isset($parameters['old'])) $parameters['old'] = $parameters['old']->toArray();
		}

		if(!isset($effect->values['variable-slug'])) throw new Exception("Nom de la variable non défini");

		$effect->values['variable-slug'] = template($effect->values['variable-slug'],$parameters,true);
		$effect->values['variable-value'] = isset($effect->values['variable-value']) ? template($effect->values['variable-value'],$parameters,true) : '';
		

		$workflowVars = json_decode($conf->get('workflow-var'),true);
		$workflowVars[$effect->values['variable-slug']] = $effect->values['variable-value'];

		$conf->put('workflow-var', json_encode($workflowVars));



		$logs .= '<i class="fas fa-dollar-sign"></i> '.$effect->values['variable-slug'].' passée à '.$effect->values['variable-value'];
		
		return $logs;
	}
}

?>