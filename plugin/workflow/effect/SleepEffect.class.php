<?php 
/*
	Effet de workflow
	Fait une pause de x secondes
*/
class SleepEffect{

	//Descriptif du type d'effet
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'sleep',
			'label' => 'Faire une pause',
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'fas fa-stopwatch',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}

	//méthode d'affichage de l'effet
	public static function form($item){
		$html  = '';
		$class = get_called_class();
		ob_start();
		?>
		<div class="input-group">
			<div class="input-group-text input-group-prepend">
			Pause de : 
			</div>
			<input type="text" data-id="time" class="form-control" value="<?php echo isset($item['time']) ? $item['time'] : '' ?>" placeholder="10">
			<div class="input-group-text input-group-prepend">
			secondes  
			</div>
		</div>
		<?php
		$html = ob_get_clean();
		return $html;
	}

	public static function run($effect,$parameters = array()){
		global $conf;
		try{
			sleep($effect->values['time']);
			$logs = 'Attente pendant : "'.$effect->values['time'].'" secondes ';
		}catch(Exception $e){
			$logs .= '<span  class="text-danger"><br>Erreur : '.$e->getMessage().'</span>'; 
		}
		return $logs;
	}
}

?>