<?php 
/*
	Effet de workflow
	Execute une requete POST,GET... sur une url et récupere le résultat (json ou html) dans une variable de conf workflow
*/
class CurlEffect{

	//Descriptif du type d'effet
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'curl',
			'label' => 'Lancer une requête',
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'fas fa-atlas',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}

	//méthode d'affichage de l'effet
	public static function form($item){
		$html  = '';
		$class = get_called_class();
		ob_start();
		?>
		
		<div class="input-group">
			<div class="input-group-text input-group-prepend">
			Méthode 
			</div>
			<input type="text" data-id="curl-method" class="form-control" value="<?php echo isset($item['curl-method']) ? $item['curl-method'] : 'GET' ?>" placeholder="POST,GET..." required>
			<div class="input-group-text input-group-prepend">
			URL  
			</div>
			<input type="text" data-id="curl-url" class="form-control" value="<?php echo isset($item['curl-url']) ? $item['curl-url'] : 'http://' ?>" placeholder="http://..." required>
		</div>

		En-têtes  
		<textarea data-id="curl-header" class="form-control"  placeholder="Une ligne par en-tête ex :
Content-type:'application/json'"><?php echo isset($item['curl-header']) ? $item['curl-header'] : '' ?></textarea>
		
		Corps
		<textarea data-id="curl-body" class="form-control"  placeholder="{...}"><?php echo isset($item['curl-body']) ? $item['curl-url'] : '' ?></textarea>
		
		<hr>
		<div class="input-group">
			<div class="input-group-text input-group-prepend">
				Format de résultat    
			</div>
			<select data-id="curl-result-type" class="form-control">
				<option <?php echo isset($item['curl-result-type']) && $item['curl-result-type']=='json' ? "selected='selected'" : '' ?> value="">Aucun</option>
				<option <?php echo isset($item['curl-result-type']) && $item['curl-result-type']=='html' ? "selected='selected'" : '' ?> value="html">HTML</option>
				<option <?php echo isset($item['curl-result-type']) && $item['curl-result-type']=='json' ? "selected='selected'" : '' ?> value="json">JSON</option>
			</select>
		</div>
		<div class="input-group mt-1">
			<div class="input-group-text input-group-prepend">
				Variable de résultat    
			</div>
			<input type="text" data-id="curl-var" class="form-control" value="<?php echo isset($item['curl-var']) ? $item['curl-var'] : '' ?>" placeholder="Ma variable">
			<div class="input-group-text input-group-prepend">
				Prendre le chemin JSON    
			</div>
			<input type="text" data-id="curl-var-path" class="form-control" value="<?php echo isset($item['curl-var-path']) ? $item['curl-var-path'] : '' ?>" placeholder="Ma variable">
		</div>
		<?php
		$html = ob_get_clean();
		return $html;
	}

	public static function run($effect,$parameters = array()){
		global $conf;
		$logs = '';
		
		if( in_array($parameters['workflow']['type'] , array( Workflow::TYPE_ENTITY, Workflow::TYPE_LIST))  ){
			if(isset($parameters['current'])) $parameters['current'] = $parameters['current']->toArray();
			if(isset($parameters['old'])) $parameters['old'] = $parameters['old']->toArray();
		}
		$effect->values['curl-method'] = template($effect->values['curl-method'],$parameters,true);
		$effect->values['curl-url'] = template($effect->values['curl-url'],$parameters,true);
		$effect->values['curl-header'] = template($effect->values['curl-header'],$parameters,true);
		$effect->values['curl-body'] = template($effect->values['curl-body'],$parameters,true);
	
		$ch = curl_init();
		$options = array(
			CURLOPT_URL => $effect->values['curl-url'],
		    CURLOPT_HEADER => false,
		    CURLOPT_SSL_VERIFYHOST => false,
   			CURLOPT_SSL_VERIFYPEER => false,
   			CURLOPT_RETURNTRANSFER => true,
   			CURLOPT_CONNECTTIMEOUT=> 10000
		);

		$options[CURLOPT_HTTPHEADER] = explode(PHP_EOL,$effect->values['curl-header']);
		$options[CURLOPT_POSTFIELDS] = $effect->values['curl-body'] ;
		curl_setopt_array($ch, $options);
		$logs .= '<i class="'.self::manifest('icon').'"></i> Lancement de la requete '.$effect->values['curl-method'].' '.$effect->values['curl-url'].'<br>';
		$results = curl_exec($ch);
		
		$logs .= 'Résultat '.$results.'<br>';

		curl_close($ch);
		
		if(isset($effect->values['curl-var'])){
			$logs .= 'Stockage résultat en variable '.$effect->values['curl-var'].'<br>';
			if($effect->values['curl-result-type'] == 'json'){
				$results = json_decode($results,true);
				$logs .= 'Décodage json :'.($results==false?'JSON incorrect':'JSON correct').'<br>';

				if(!empty($effect->values['curl-var-path']) && $results!=false){
					foreach (explode('.',$effect->values['curl-var-path']) as $key => $value) {
						if(!isset($results[$value])) break;
						$results = $results[$value];
					}
				}
			}
			$logs .= 'Stokage résultat '.$effect->values['curl-var'].' = '.$results.'<br>';
			$workflowVars = json_decode($conf->get('workflow-var'),true);
			$workflowVars[$effect->values['curl-var']] = $results;
			$conf->put('workflow-var', json_encode($workflowVars));
		}
		
		return $logs;
	}
}

?>