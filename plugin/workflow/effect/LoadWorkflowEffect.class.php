<?php 
/*
	Effet de workflow
	Execute un autre workflow, nb: le workflow executé doit être du même type que le workflow executant
*/
class LoadWorkflowEffect{

	//Descriptif du type d'effet
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'workflow',
			'label' => 'Lancer un workflow',
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'fas fa-network-wired',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}

	//méthode d'affichage de l'effet
	public static function form($item){
		$html  = '';
		$class = get_called_class();
		
		//$class::manifest('icon')
		ob_start();
		?>
		
		<div class="input-group">
			<div class="input-group-text input-group-prepend">
			Workflow  : 
			</div>
			<input type="text" data-id="workflow-id" class="form-control" value="<?php echo isset($item['workflow-id']) ? $item['workflow-id'] : '' ?>" placeholder="Id du workflow">
		</div>
		
		<?php
		$html = ob_get_clean();
		return $html;
	}

	public static function run($effect,$parameters = array()){
		global $conf;
		$logs = '';
		
		if( in_array($parameters['workflow']['type'] , array( Workflow::TYPE_ENTITY, Workflow::TYPE_LIST))  ){
			if(isset($parameters['current'])) $parameters['current'] = $parameters['current']->toArray();
			if(isset($parameters['old'])) $parameters['old'] = $parameters['old']->toArray();
		}
		$effect->values['workflow-id'] = template($effect->values['workflow-id'],$parameters,true);
	
		$toLoad = Workflow::getById($effect->values['workflow-id']);
		if(!$toLoad){
			$logs.=' Workflow inexistant';
			return $logs;
		}
	
		if( $parameters['workflow']['type'] != $toLoad->type ){
			$logs.=" Workflow d'origine d'un type différent du workflow de destination";
			return $logs;
		}

		$logs .= '<i class="'.self::manifest('icon').'"></i> Lancement de '.$toLoad->label;
		

		$logs .= implode('<br>',$toLoad->run(null,$parameters));

		return $logs;
	}
}

?>