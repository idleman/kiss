<?php
/**
 * Define a workfloweffect.
 * Un effet est la conséquence d'une ou plusieurs causes
 * @author Administrateur
 * @category Plugin
 * @license MIT
 */

class WorkflowEffect extends Entity{

	public $id;
	public $type; //Type (Liste configurable)
	public $workflow; //Workflow (Entier)
	public $values; //Valeurs (Texte Long)
	public $sort; //Ordre (Entier)
	
	protected $TABLE_NAME = 'workflow_effect';
	public $fields = array(
		'id' => 'key',
		'type' => 'string',
		'workflow' => 'int',
		'values' => 'longstring',
		'sort' => 'int'
	);

	//Colonnes indexées
	public $indexes = array();

	//Méthode d'execution d'un effet
	public function run($parameters = array()){
		
		//On récupere la calsse du type d'effet pour l'effet courant
		$type = self::types($this->type);
		// On met les variables de workflow a disposition de l'effet
		// On utilise pas $conf->get volontairement pour eviter le session cache conflictuel avec les workflows
		$theConf = Configuration::load(array('key'=>'workflow-var'));
		$confValue = !$theConf ? '{}' : decrypt($theConf->value);
		$parameters['workflow-var'] = json_decode($confValue,true);
		//On execute la méthode run de la classe du type d'effet ciblée en lui fournissant les données de l'effet et les données workflow (entité, item courant etc...)
		$results = $type::run($this,$parameters);
		$logs[] = '<i class="'.$type::manifest('icon').'"></i> '.$type::manifest('label').'<br><div class="pl-3">'.$results.'</div>';
		return is_array($logs) ? $logs : array();
	}

	//Récuperation des ttyes d'effets possibles
	public static function types($slug = null){
		$types = array();
		//Récuperations des types d'effet par defaut définit dans le dossier effect
		foreach (glob(__DIR__.SLASH.'effect'.SLASH.'*.class.php') as $file) {
			require_once($file);
			$class = str_replace('.class.php','',basename($file));
			$infos = $class::manifest();
			$types[$infos['slug']] = $class;
		}
		//Récuperation des effets fournis par d'autres plugins
		Plugin::callHook('workflow_effect',array(&$types));
		if(!isset($slug)) return $types;
		return isset($types[$slug]) ? $types[$slug] : $types['mail'];
	}
}
?>