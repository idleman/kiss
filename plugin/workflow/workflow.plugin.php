<?php

//Déclaration d'un item de menu dans le menu principal
function workflow_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('workflow','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=workflow',
		'label'=>'Workflow',
		'icon'=> 'fas fa-network-wired',
		'color'=> '#3498db'
	);
}

//Cette fonction va generer une page quand on clique sur workflow dans menu
function workflow_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='workflow') return;
	$page = !isset($_['page']) ? 'list.workflow' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function workflow_install($id){
	if($id != 'fr.core.workflow') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function workflow_uninstall($id){
	if($id != 'fr.core.workflow') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("workflow",array('label'=>"Gestion des droits sur le plugin workflow"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html

require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function workflow_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('workflow','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.workflow',
		'icon' => 'fas fa-angle-right',
		'label' => 'Workflow'
	);
}

//Déclaration des pages de réglages
function workflow_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Cron executant les workflow globaux ayant pour évenement "Toutes les minutes"
function workflow_cron(){
	require_once(__DIR__.SLASH.'WorkflowEvent.class.php');
	WorkflowEvent::trigger(WorkflowEvent::EVENT_CRON);
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('workflow',array(
    "Général",
    'workflow_mail_from' => array("label"=>"Envoyeur des e-mails","type"=>"text"),
    'workflow_mail_reply' => array("label"=>"Envoyeur des e-mails : mail de réponse","type"=>"text"),
));


Plugin::addHook("workflow_event", function(&$events){
	//Déclaration de l'évenement global "Toutes les minutes" pour utilisation sur les workflow globaux
	$events[] = array(
		'type' => Workflow::TYPE_GLOBAL,
		'events' => array(
			WorkflowEvent::EVENT_CRON => 'Toutes les minutes'
		)
	);
});



//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "workflow_install");
Plugin::addHook("uninstall", "workflow_uninstall"); 


Plugin::addHook("menu_main", "workflow_menu"); 
Plugin::addHook("page", "workflow_page");  
Plugin::addHook("menu_setting", "workflow_menu_setting");    
Plugin::addHook("content_setting", "workflow_content_setting");   
Plugin::addHook("cron", "workflow_cron");   
    

?>