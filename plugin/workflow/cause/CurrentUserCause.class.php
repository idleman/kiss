<?php 
/*
	Cause de workflow
	Compare l'utilisateur courant à la valeur définie dans la cause
*/
class CurrentUserCause{

	//Descriptif de la cause
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'current-user',
			'label' => 'Utilisateur Connecté',
			'type' => array(Workflow::TYPE_ALL),
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'far fa-users',
			'attributes' => ' data-filter-type="user" ',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}
	
	//Méthode de vérification de la cause
	public static function check($filter,$parameters = array()){
		global $myUser;
		switch($filter['operator']){
			case '=':
				if($myUser->login != $filter['value'][0] ) return false;
			break;
			case '!=':
				if($myUser->login == $filter['value'][0] ) return false;
			break;
			case 'is null':
				if(!empty($myUser->id)) return false;
			break;
			case 'is not null':
				if(empty($myUser->id)) return false;
			break;
		}
		return true;
	}

	
}

?>