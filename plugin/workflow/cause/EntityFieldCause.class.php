<?php 
/*
	Cause de workflow
	Compare un champ de l'entité ciblé à la valeur définie dans la cause
	Ne s'applique qu'aux workflows de type entité et liste
*/
class EntityFieldCause{

	//Descriptif de la cause
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'entity-field',
			'label' => 'Entité',
			'type' => array(Workflow::TYPE_ENTITY,Workflow::TYPE_LIST),
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'fas fa-leaf',
			'attributes' => ' data-filter-type="entity-field" ',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}

	//Type de filtre de cause custom pour cette cause (champs additionnels)
	public static function filter($entity = array()){
		
		$stream = '<div class="filter-value-block" data-value-type="entity-field" data-value-selector=".filter-value:last-child">
			<div class="filter-subfield  d-inline-block">
				'.$entity['label'].'.
				<select data-id="entity-field" class="d-inline-block mt-2 border-0 font-weight-bold text-primary" value="Ma colonne">
					<option value="">- Attribut - </option>';
					 if(isset($entity['fields'] )):
					 foreach($entity['fields'] as $slug=>$attribute): 
						$stream .=' <option data-attribute-type="'.$attribute['type'].'" value="'.$slug.'">'.$attribute['label'].'</option>';
					 endforeach; 
					endif;
			$stream .='</select>
			</div>
			<select class="form-control filter-operator border-0 text-primary">
				<option data-default-view="'.base64_encode(json_encode(array('view' => "text"))).'" value="=">Égal</option>
				<option data-default-view="'.base64_encode(json_encode(array('view' => "text"))).'" value="!=">Différent</option>
				<option data-default-view="'.base64_encode(json_encode(array('view' => "text"))).'" value=">">Inférieur</option>
				<option data-default-view="'.base64_encode(json_encode(array('view' => "text"))).'" value="<">Supérieur</option>
				<option data-default-view="'.base64_encode(json_encode(array('view' => "text"))).'" value="like">Contient</option>
			</select>
			
		</div>';
		
		return $stream;;
	}

	//Méthode de vérification de la cause
	public static function check($filter,$parameters = array()){
		if(!isset($parameters['current'])) throw new Exception("Aucun item entité n'est présent");
		$column = $filter['subcolumn']['entity-field'];
		$class = get_class($parameters['current']);
		
		$columnValue = null;
		
		//la colonne évoquée peut être un attribut de classe ou une méthode de classe
		if( property_exists ( $class , $column ) ) $columnValue = $parameters['current']->$column;
		if( method_exists ( $class , $column ) ) $columnValue = $parameters['current']->$column();
				

		switch($filter['operator']){
			case '=':
				if(!isset($columnValue) ) return false;
				if($columnValue != $filter['value'][0]  ) return false;
			break;
			case '!=':
				if(isset($columnValue) && $columnValue == $filter['value'][0]  ) return false;
			break;
			case '<':
				if($columnValue > $filter['value'][0]) return false;
			break;
			case '>':
				if($columnValue < $filter['value'][0]) return false;
			break;
			case 'like':
				if( strpos(  mb_strtolower($columnValue), mb_strtolower($filter['value'][0]) ) === false  ) return false;
			break;
		}
		
		return true;
	}

	
}

?>