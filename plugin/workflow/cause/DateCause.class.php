<?php 
/*
	Cause de workflow
	Compare la date courante à la valeur définie dans la cause
*/
class DateCause{

	//Descriptif de la cause
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'date',
			'label' => 'Date',
			'type' => array(Workflow::TYPE_GLOBAL,Workflow::TYPE_ALL),
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'far fa-calendar-alt',
			'attributes' => ' data-filter-type="date" ',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}
	
	//Méthode de vérification de la cause
	public static function check($filter,$parameters = array()){
		$compareIn = mktime(0,0,0);
		switch($filter['operator']){
			case 'between':
				$compareTo = timestamp_date($filter['value'][0]);
				$compareTo2 = timestamp_date($filter['value'][1]);
				if( $compareIn < $compareTo || $compareIn > $compareTo2 ) return false;
			break;
			case '<':
				$compareTo = timestamp_date($filter['value'][0]);
				if($compareIn >= $compareTo) return false;
			break;
			case '>':
				$compareTo = timestamp_date($filter['value'][0]);
				if($compareIn <= $compareTo) return false;
			break;
			case '=':
				$compareTo = timestamp_date($filter['value'][0]);
				if($compareIn != $compareTo) return false;
			break;
			case 'is null':
				if(!empty($compareIn)) return false;
			break;
			case 'is not null':
				if(empty($compareIn)) return false;
			break;
		}
		return true;
	}

	
}

?>