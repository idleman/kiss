<?php 
/*
	Cause de workflow
	Compare l'heure courante à la valeur définie dans la cause
*/
class HourCause{

	//Descriptif de la cause
	public static function manifest($key = null){
		$manifest = array(
			'slug' => 'hour',
			'label' => 'Heure',
			'type' => array(Workflow::TYPE_GLOBAL,Workflow::TYPE_ALL),
			'class' => get_called_class(),
			'path' => __FILE__,
			'icon' => 'far fa-clock',
			'attributes' => ' data-filter-type="hour" ',
			'color' => '#ff9f43',
		);
		if(!isset($key)) return $manifest;
		return isset($manifest[$key]) ? $manifest[$key] : '' ;
	}

	//Méthode de vérification de la cause
	public static function check($filter,$parameters = array()){
		$hour = date('H');
		$minuts = date('i');

		if(isset($filter['value'][0])){
			$compareTo = explode(':',$filter['value'][0]);
			$compareHourTo = $compareTo[0];
			$compareMinutTo = $compareTo[1];
		}
		switch($filter['operator']){
			case 'between':
				$compareTo2 = explode(':',$filter['value'][1]);
				$compareHourTo2 = $compareTo2[0];
				$compareMinutTo2 = $compareTo2[1];
				if($compareHourTo > $hour ) return false;
				if($compareHourTo == $hour && $compareMinutTo > $minuts ) return false;
				if($compareHourTo2 < $hour ) return false;
				if($compareHourTo2 == $hour && $compareMinutTo2 < $minuts ) return false;	
			break;
			case '<':
				if($compareHourTo < $hour ) return false;
				if($compareHourTo == $hour && $compareMinutTo < $minuts ) return false;
			break;
			case '>':
				if($compareHourTo > $hour ) return false;
				if($compareHourTo == $hour && $compareMinutTo > $minuts ) return false;
			break;
			case '=':
				if($compareHourTo != $hour || $compareMinutTo != $minuts) return false;
			break;
			case 'is null':
				return false;
			break;
		}
		return true;
	}

	
}

?>