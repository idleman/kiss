<?php
global $myUser;
User::check_access('workflow','read');
require_once(__DIR__.SLASH.'Workflow.class.php');
?>

<div class="row">
    <div class="col-md-12">
        <div class="d-flex my-2 w-100">
            <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des workflows</h4>
            <div class="my-auto ml-auto mr-0 noPrint">
                <?php if($myUser->can('workflow', 'edit')) : ?>
                <a href="index.php?module=workflow&page=sheet.workflow" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="clear noPrint"></div>
    </div>
    <!-- Moteur de recherche -->
    <div class="col-md-8">
        <select id="filters" data-type="filter" data-label="Recherche" data-function="workflow_workflow_search"></select>
    </div>
</div>

<hr>

<h5 class="results-count"><span></span> Résultat(s)</h5>
<div class="row">
	<!-- Liste des workflow créés -->
	<div class="col-xl-12">
		<ul id="workflows" class="workflows-list" data-entity-search="workflow_workflow_search">
                <li data-id="{{id}}" class="hidden" style="border-color:{{color}};">
                    <a class="btn text-info" title="Éditer workflow" href="index.php?module=workflow&page=sheet.workflow&id={{id}}">
	                <i class="{{icon}}" ></i><br>
                        <h6>#{{id}} - {{label}}</h6>
                        <small>{{type.label}} {{#entity}}({{entity}}){{/entity}}</small>
                        <div class="workflow-menu">
                            <div class="btn-group btn-group-sm" role="group">
                                <div class="btn "><i class="fas fa-pencil-alt"></i></div>
                                <div class="btn text-danger" title="Supprimer workflow" onclick="workflow_workflow_delete(this,event);"><i class="far fa-trash-alt"></i></div>
                            </div>
                        </div>
	                </a>
                </li>
        
        </ul><br>
         <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
        <ul class="pagination justify-content-center"  data-range="5">
            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');workflow_workflow_search();">
                <span class="page-link">{{label}}</span>
            </li>
        </ul>
	</div>
</div>
