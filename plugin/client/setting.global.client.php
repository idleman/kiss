<?php
global $myUser,$conf,$myFirm;
User::check_access('client','configure');
?>

<div class="row client-setting-form">
    <div class="col-xl-12"><br>
    	<div onclick="client_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Client</h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('client'); ?>


        <h3>Import excel</h3>
        <p>Pour importer des clients, téléchargez la trame <a href="action.php?action=client_import_model">d'export ici</a> puis envoyez la trame renseignée dans le champs ci dessous</p>
        <input 
            type="text"
            data-type="file"
            data-extension="csv" 
            data-action="client_import_component"
            data-id="attachments" 
            class="component-file-default bg-white shadow-sm">

        <div class="btn" onclick="client_import();">Importer</div>
        <hr/>
        
        <p>Pour ajouter des champs personnalisés à la fiche client, créez une formulaire dynamique ayant pour slug <code>client-sheet-custom</code></p>
        <p>Pour intégrer la base client à une autre application, vous pouvez utiliser le code suivant :</p>
        

        <code>
             &lt;iframe frameborder="0" width="100%" align="center" height="600px" src="<?php echo ROOT_URL ?>/index.php?module=client&firm=<?php echo $myFirm->id ?>&embedded=1"&gt;&lt;/iframe&gt;
        </code>

        <p><strong>nb :</strong> n'oubliez pas de définir les permissions pour les utilisateurs non connectés</p>

    </div>


    
</div>
