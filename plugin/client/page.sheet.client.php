<?php 
global $conf,$myUser,$myFirm;

require_once(__DIR__.SLASH.'Client.class.php');
$client = Client::provide();



if(!$myUser->can('client','read') && $client->id!=0 && !$myUser->can('client_sheet','read',$client->id)) throw new Exception("Permission non accordée sur cette fiche");


if(isset($_['id']) && !$client) throw new Exception("Aucun client existant avec cet identifiant");

$addresses = $client->addresses('global');
$mainAddress = !empty($addresses) ? $addresses[0]: new Address(); 

$contacts = $client->contacts();

$phone = isset($contacts['phone']) ? $contacts['phone'] : new Contact();
$phoneValue = $phone->value;
if(!empty($phone->value)) $phoneValue = implode(' ',str_split($phone->value,2));

$mail = isset($contacts['mail']) ? $contacts['mail'] : new Contact();

$clientMenu = array();

if($client->creator == '') $client->creator = $myUser->login;
if($client->updater == '') $client->updater = $myUser->login;

$creator = User::byLogin($client->creator);
$updater = User::byLogin($client->updater);

$subsites = $client->subsites();
$holding = isset($_['parent']) ? Client::getById($_['parent']) : $client->holding();

$holdingPhone = '';
$holdingAddress = new Address();

if($holding->id!=0){
	$holdingPhone =  $holding->contacts('phone')->value;
	$holdingAddress = $holding->addresses('global');
	$holdingAddress = !empty($holdingAddress) ? $holdingAddress[0] : new Address();
}

Plugin::callHook('client_menu',array(&$clientMenu,$client));

usort($clientMenu,function($a,$b){
	if($a->sort < $b->sort) return -1;
	if($a->sort > $b->sort) return 1;
	if($a->sort == $b->sort) return 0;
});

$editable = $myUser->can('client','edit') && ($client->id==0 || $client->firm==$myFirm->id || $myUser->can('client_sheet','edit',$client->id));


?>
<div class="client-container" data-client-type="<?php echo !empty($client->type) ? $client->type : $conf->get('client_type'); ?>" data-id="<?php echo $client->id; ?>" data-editable="<?php echo $editable === true ?'1':'0' ?>">
	<div class="client-summary-panel">
		<div class="client-thumbnail type-firm" style="background-image:url(action.php?action=client_assets_load&type=cover&client=<?php echo $client->id; ?>)"></div>
		<div class="client-summary-sheet p-3">
			<h1 class="client-label m-0" data-live-update="#label"><?php echo $client->label; ?></h1>
			<small class="text-muted d-inline-block mb-2 w-100" data-live-update="#code">N° <?php echo $client->code; ?></small>
			<div class="button right d-block d-sm-none" title="Contacts et Adresses" onclick="client_contact_mobile_toggle();"><i class="far fa-address-book"></i></div>
			<h2 class="client-job type-firm" data-live-update="#job"></h2>
			<span class="client-city" data-city="<?php echo $mainAddress->city; ?>" data-zip="<?php echo $mainAddress->zip; ?>"><i class="fas fa-map-marker-alt"></i> <span><?php echo $mainAddress->city.(!empty($mainAddress->zip)?' ('.$mainAddress->zip.')':''); ?></span> </span>
			<ul class="client-contact">
				<li class="<?php echo empty($phone->value)?'hidden':''; ?>"><i class="fas fa-mobile-alt mr-1"></i> <span data-type="phone-text" data-live-update="#phone"><?php echo $phoneValue; ?></span></li>
				<li class="<?php echo empty($mail->value)?'hidden':''; ?>"><i class="fas fa-envelope-open-text"></i> <span data-live-update="#mail"><?php echo $mail->value; ?></span></li>
			</ul>
			<div class="client-created" title="Modifié le <?php echo complete_date($client->updated); ?> par <?php echo $updater->fullName(); ?>">
				<i class="far fa-calendar-alt"></i> Créé le <span><?php echo complete_date($client->created); ?></span><br>
				<i class="fas fa-user-tie"></i> Créé par <span><?php echo $creator->fullName(); ?></span>
			</div>
			<?php 
				//hook panel de gauche après créé par...
				Plugin::callHook('client_panel',array($client));
			?>
		</div>

		<div class="client-summary-sites type-firm">
			<div >
				<h3 class="text-uppercase mb-0"><?php echo mb_strtoupper($conf->get('client_holding_singular')); ?> 
					<a class="right pointer client-edition editable-only" data-tooltip title="Lier cette entreprise à un client existant" onclick="client_relation_add(true);"><i class="fas fa-link" ></i></a>
				</h3>
				<ul id="holding" class="p-0 m-0"></ul>
				
			</div>

				<h3 class="text-uppercase"><span class="client-subsite-count"></span>Sous-établissements <a class="right pointer client-edition editable-only" data-tooltip title="Ajouter un sous-établissement" onclick="window.location = 'index.php?module=client&page=sheet.client&parent='+$.urlParam('id');"><i class="far fa-plus-square"  ></i></a></h3>
				<div class="clear"></div>
				<ul id="subsites" class="m-0" >
					<li class="hidden site-block" data-id="{{id}}" data-level="{{level}}">
						<a href="index.php?module=client&page=sheet.client&id={{id}}">
							<h4 class="my-2"><i class="far fa-building"></i> {{label}}</h4>
							<p>{{#address.id}}{{address.street}} {{address.complement}}<br>
							{{address.zip}} {{address.city}}{{/address.id}}{{#phone}} - {{phone}}{{/phone}}</p>
						</a>
						<i class="fas fa-times text-muted btn-relation-delete" title="Supprimer la relation" onclick="client_relation_delete(this)"></i>
						<div class="clear"></div>
					</li>
				</ul>
			</div>

			<div class="client-summary-memberof type-individual">
				<h3 class="text-uppercase pb-0 mb-0">LIAISONS</h3>
				<ul id="memberof" class="m-0"></ul>
			</div>


		</div>

		<div class="client-content-panel">
			<div class="client-menu-bar">
				<ul>
					<?php foreach ($clientMenu as $menu): ?>
						<li data-slug="<?php echo $menu->slug; ?>" ><a href="<?php echo $menu->url; ?>"><?php echo $menu->label; ?></a></li>
					<?php endforeach; ?>
				</ul>
				<div class="button right contact-btn d-none d-sm-block" title="Contacts et Adresses" onclick="client_contact_mobile_toggle();"><i class="far fa-address-book"></i></div>
			</div>
			<div class="client-content-page">
				<!-- TAB CONTENT HERE -->
			</div>
		</div>

		<!-- CONTACTS & ADRESSES-->
		<div class="client-contact-panel">
			<div onclick="client_contact_mobile_toggle();" class="btn w-100 text-center text-uppercase d-block"><i class="fas fa-times"></i> Fermer</div>
			<!-- CONTACTS -->
			<div class="type-firm pb-2">
			    <div onclick="client_contact_edit();" class="btn btn-info client-edition text-uppercase editable-only"><i class="fas fa-user-edit"></i> Ajouter</div>
			    <h4 class="mt-3 mb-1 contacts-title text-uppercase">Contacts (<span>0</span>)</h4>
			    <ul id="contacts" class="contacts-list">
			        <li class="hidden contact-template" data-id="{{id}}" >

						<div class="dropdown btn-options editable-only">
							<div class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-ellipsis-v text-muted"></i>
							</div>
							<div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
							    <a class="dropdown-item px-3" title="Éditer ce contact" onclick="client_contact_edit(this)"><i class="fas fa-pencil-alt"></i> Éditer</a>
							    <a class="dropdown-item px-3 text-danger" title="Supprimer ce contact" onclick="client_contact_delete(this)"><i class="fas fa-trash-alt"></i> Supprimer</a>
							</div>
						</div>
						<div class="contact-avatar" onclick="{{#link}}window.location='{{link}}';{{/link}}"><img class="avatar-rounded" src="{{avatar}}"></div>
						<div class="d-inline-block ml-2" onclick="{{#link}}window.location='{{link}}';{{/link}}">
						    <h2>{{fullName}}</h2>
						    <span class="text-muted">{{job}}</span>
						    {{#mobile}}<span class="text-muted d-block" data-type="phone-text">{{mobile}}</span>{{/mobile}}
						    {{^mobile}}{{#phone}}<span class="text-muted d-block" data-type="phone-text">{{phone}}</span>{{/phone}}{{/mobile}}
						    {{#mail}}<span class="text-muted  d-block"><a href="mailto:{{mail}}">{{mail}}</a></span>{{/mail}}
						    {{#hasTag}}
                            <div>
                                {{#tag}}
                                    <span class="badge badge-secondary">{{.}}</span>
                                {{/tag}}
                            </div>
                            {{/hasTag}}
						</div>
					</li>
				</ul>
				<h4 class="mt-3 mb-1 internals-title text-uppercase">Référents internes(<span>0</span>)</h4>
				<ul id="internals" class="internals-list"></ul>
				<hr class="mb-0">
			</div>
			<!-- ADRESSES -->
			<div class="pb-2">
				<div onclick="client_address_edit();" class="btn btn-info client-edition text-uppercase editable-only"><i class="fas fa-map-marked-alt"></i> Ajouter</div>
				<h4 class="mt-3 mb-1 address-title text-uppercase">Adresses (<span>0</span>)</h4>
				<ul id="addresses" class="addresses-list">
					<li class="hidden address d-flex" onclick="client_address_change(this)" data-id="{{id}}" data-street="{{street}}" data-complement="{{complement}}" data-zip="{{zip}}" data-city="{{city}}" data-country="{{country}}" data-type="{{type.slug}}">
						<div class="dropdown btn-options">
							<div class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-ellipsis-v text-muted"></i>
							</div>
							<div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item px-3" title="Copier cette adresse" onclick="client_address_copy(this)"><i class="fas fa-clipboard"></i> Copier adresse</a>
								<a class="dropdown-item px-3 editable-only" title="Éditer cette adresse client" onclick="client_address_edit(this)"><i class="fas fa-pencil-alt"></i> Éditer</a>
								<a class="dropdown-item px-3 text-danger editable-only" title="Supprimer cette adresse client" onclick="client_address_delete(this)"><i class="fas fa-trash-alt"></i> Supprimer</a>
							</div>
						</div>
						<div class="address-avatar my-auto"><i class="{{type.icon}}"></i></div>
						<div class="adress-content d-inline-block ml-3">
							<h2>{{type.label}}</h2>
							<p class="text-muted mb-0">
								{{street}} {{complement}}, 
								<br>{{#zip}}{{zip}}{{/zip}}{{^zip}}-{{/zip}} {{#city}}{{city}}{{/city}}{{^city}}-{{/city}} 
								<br>{{#country}}{{country}}{{/country}}
							</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Modale contact -->
	<!-- Modal -->
	<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-secondary">
					<h5 class="modal-title" id="contact-modal">Contact</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<h6  class="text-muted text-uppercase">Type de contact</h6>
							<label class="px-2 pl-0 pointer" onclick="client_contact_type()"><input type="radio" checked="checked" name="contact-type" data-value="external" data-type="radio"> Contact externe</label>
							<label class="px-2 pr-0 pointer" onclick="client_contact_type()"><input type="radio" name="contact-type" data-value="client" data-type="radio">  <?php echo ucFirst($conf->get('client_label_singular')); ?> contact</label>
							<label class="px-2 pr-0 pointer" onclick="client_contact_type()"><input type="radio" name="contact-type" data-value="user" data-type="radio"> Référent interne</label>
						</div>
					</div>
					<hr class="my-2">
					<div class="contact-type-block external-type-block">
						<div class="row mb-2">
							<div class="col-md-6">
								<label class="mb-1" for="firstname">Prénom :</label>
								<div class="input-group">
							    	<select class="form-control form-control-sm" id="civility">
						            	<option value="m">M</option>
						            	<option value="mme">Mme</option>
						            </select>
									<input id="firstname" name="firstname" class="form-control form-control-sm" placeholder="eg. John" value="" type="text">
								</div>
							</div>
							<div class="col-md-6">
								<label class="mb-1" for="name">Nom :</label>
								<input id="name" name="name" class="form-control form-control-sm" placeholder="eg. Doe" value="" type="text">
							</div>
						</div>
						<div class="row mb-2">
							<div class="col-md-4">
								<label class="mb-1" for="contact-mail">Mail :</label>
								<input id="contact-mail" class="form-control form-control-sm" placeholder="eg. john.doe@exemple.com" value="" type="text">
							</div>
							<div class="col-md-4">
								<label class="mb-1" for="contact-phone">Téléphone :</label>
								<input id="contact-phone" class="form-control form-control-sm" placeholder="eg. 01 23 45 67 89" value="" type="text">
							</div>
							<div class="col-md-4">
								<label class="mb-1" for="contact-phone">Mobile :</label>
								<input id="contact-mobile" class="form-control form-control-sm" placeholder="eg. 07 22 44 66 55" value="" type="text">
							</div>
						</div>
					</div>
					<div class="contact-type-block client-type-block mb-2 hidden">
						<label class="mb-1" for="account">Nom du contact :</label>
						<input required data-id="account" data-type="client" class="form-control form-control-sm" placeholder="" value="" type="text">
					</div>
					<div class="contact-type-block user-type-block mb-2 hidden">
						<label class="mb-1" for="account">Nom du compte :</label>
						<input required data-id="account" data-type="user" class="form-control form-control-sm" placeholder="" value="" type="text">
					</div>
					<label for="tag">Etiquettes</label>
					<input  value="" class="form-control" placeholder="facturation, livraison..." type="text"  data-type="tag"  data-multiple=true  id="tag" >
					
					<label class="mb-1" for="contact-job">Fonction :</label>
					<input id="contact-job" class="form-control form-control-sm" placeholder="PDG, Directeur commercial..." value="" type="text">
					<label class="mb-1" for="contact-comment">Commentaire :</label>
					<input id="contact-comment" class="form-control form-control-sm"  value="" type="text">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal" title="Fermer sans enregistrer">Fermer</button>
					
					<button type="button" onclick="client_contact_save();" class="btn btn-primary" title="Enregistrer la fiche contact"><i class="fas fa-check"></i> Enregistrer</button>
				</div>
			</div>
		</div>
	</div>



	<!-- Modale addresse -->
	<!-- Modal -->
	<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-secondary">
					<h5 class="modal-title" id="address-modal">Adresse</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<h6  class="text-muted text-uppercase">Type d'adresse</h6>
							<select id="address-type" class="form-control form-control-sm">
								<?php $i=0;
								foreach(Client::addressType() as $key=>$type): ?>
									<option value="<?php echo $key; ?>" <?php echo $i==0?'selected="selected"':'' ?> ><?php echo $type['label'] ?></option>
								<?php $i++; endforeach; ?>
							</select>
						</div>
					</div>
					<hr class="my-2">
					<div class="address-type-block">
						<div class="row">
							<div class="col-md-6">
								<label class="mb-1" for="address-street">Rue</label>
								<input class="form-control form-control-sm" id="address-street" type="text" data-type="location" data-select-callback="client_address_fill">
							</div>
							<div class="col-md-6">
								<label class="mb-1" for="address-complement">Complément</label>
								<input class="form-control form-control-sm" id="address-complement">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label class="mb-1" for="city">Ville</label>
								<input id="address-city"  class="form-control form-control-sm" placeholder="" type="text" value="" type="text">
							</div>
							<div class="col-md-6">
								<label class="mb-1" for="address-zip">Code postal</label>
								<input id="address-zip" class="form-control form-control-sm" placeholder=""  type="text" value="" type="text">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label class="mb-1" for="address-country">Pays</label>
								<input id="address-country"  class="form-control form-control-sm" placeholder=""  type="text" value="France" type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal" title="Fermer sans enregistrer">Fermer</button>
					<button type="button" onclick="client_address_save();" class="btn btn-primary" title="Enregistrer la fiche adresse"><i class="fas fa-check"></i> Enregistrer</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal liaison de clients -->
	<div class="modal fade" id="client-holding-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Lier à</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<span class="text-muted mr-2">Lier le client</span> <input type="text" data-type="client" class="form-control d-inline-block w-25" id="relation-link"> <span class="text-muted mx-2">en tant que </span>
					<label><input type="radio"  name="relation-level" value="holding"> <?php echo ucFirst($conf->get('client_holding_singular')); ?></label>
					<label><input type="radio"  name="relation-level" value="subsite"> Sous établissement</label>
					<span class="ml-2 text-muted">du client actuellement ouvert</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal" title="Fermer sans enregistrer">Fermer</button>
					<button type="button" onclick="client_relation_add();" class="btn btn-primary btn-merge"><i class="fas fa-check"></i> Lier</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal fusion de clients -->
	<div class="modal fade" id="client-merge-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-secondary">
					<h5 class="modal-title" id="address-modal">Fusion</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<h6  class="text-muted text-uppercase">Fusionner le client ci dessous avec cette fiche</h6>
							<input type="text" class="form-control" id="client-merge-right" data-type="client">
						</div>
					</div>
					<hr class="my-2">
					
					
						<div class="row">
							<div class="col-md-12">
								<p>Dans le cas ou les deux clients comportent des informations, choisissez qui l'emporte</p>
								
								<table class="table">
									<thead>
									<tr>
										<th>Information</th>
										<th>Client courant</th>
										<th>Client fusionné</th>
									</tr>
									</thead>
									<tbody>
								<?php foreach(Client::fields(false) as $field): 
									if($field['column']=='id' || $field['column']=='parent') continue;
									?>
									<tr>
										<td><?php echo $field['label']; ?></td>
										<td><input type="radio" data-type="radio" data-key="<?php echo $field['column']; ?>" name="merge_<?php echo $field['column']; ?>" checked="checked" value="left"></td>
										<td><input type="radio" data-type="radio" data-key="<?php echo $field['column']; ?>" name="merge_<?php echo $field['column']; ?>" value="right"></td>
									</tr>
								<?php endforeach; ?>
								</tbody>
								</table>

								<div class="merge-logs"></div>

							</div>
							
						</div>
						
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal" title="Fermer sans enregistrer">Fermer</button>
					<button type="button" onclick="client_client_merge();" class="btn btn-primary btn-merge"><i class="fas fa-check"></i> Fusionner</button>
				</div>
			</div>
		</div>
	</div>