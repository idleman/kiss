<?php

//Déclaration d'un item de menu dans le menu principal
function client_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('client','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=client',
		'label'=>'Clients',
		'icon'=> 'fas fa-user-tie',
		'color'=> '#3498db'
	);
}

//Cette fonction va generer une page quand on clique sur client dans menu
function client_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='client') return;
	$page = !isset($_['page']) ? 'list.client' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function client_install($id){
	if($id != 'fr.core.client') return;
	global $conf;
	Entity::install(__DIR__);
	$conf->put('client_code_mask','{{label[2]}}-{{Y}}{{M}}-{{id}}');

    $conf->put('client_enable_map', 1);
    $conf->put('client_sheet_max_field', 8);
    $conf->put('client_type', "both"); 
    //Doc sur : https://entreprise.data.gouv.fr/api_doc/sirene
    $conf->put('client_api_url','https://entreprise.data.gouv.fr/api/sirene/v1/full_text/{{label}}');
    $conf->put('client_api_sheet_url','https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/?siren={{siret}}');

    $conf->put('client_label_plural','clients');
    $conf->put('client_label_singular','client');
    $conf->put('client_holding_singular','holding');

    $dictionary = new Dictionary();
    $dictionary->slug = 'client_category';
    $dictionary->label = 'Categorie de client';
    $dictionary->parent = 0;
    $dictionary->state = Dictionary::ACTIVE;
    $dictionary->save();

    $item = new Dictionary();
    $item->slug = 'client_category_none';
    $item->label = 'Aucune';
    $item->parent = $dictionary->id;
    $item->state = Dictionary::ACTIVE;
    $item->save();

    global $myFirm;
	if($myFirm->has_plugin('fr.core.export')){
		Plugin::need('export/ExportModel');
		$model = new ExportModel();
		$model->label =  'Export fiche client';
		$model->privacy = 'public';
		$model->dataset = 'client-sheet';
		$model->slug = 'client-sheet';
		$model->description= "Modèle d'export PDF utilisé depuis la fiche d'un client"; 
		$model->document_temporary = '';
		$model->export_format =  'pdf';
		$model->plugin = 'client';
		$model->filename = 'Fiche clients.html';
		$model->save();
		copy(__DIR__.SLASH.'export.html',File::dir().$model->filename);
		File::move(File::dir().$model->filename, 'export'.SLASH.'documents'.SLASH.'client'.SLASH.$model->id.SLASH.$model->filename);
	}
}

//Fonction executée lors de la désactivation du plugin
function client_uninstall($id){
	if($id != 'fr.core.client') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("client",array('label'=>"Gestion des droits sur le plugin client"));
Right::register("client_history",array('label'=>"Gestion des droits sur historique du plugin client"));

//Droits ciblés sur les fiches client
Right::register('client_sheet',array(
	'label'=>'Gestion des droits sur une fiche client particuliere',
	'global'=> false,
	'check' => function($action,$right){
		global $myUser;
		if($right->uid <= 0) throw new Exception('Id non spécifié');
		
		
		if(!$myUser->can('client','edit')) throw new Exception('Seul le un edtieur de  client peut définir des droits pour cette fiche');
	}
));

require_once(__DIR__.SLASH.'action.php');

//Déclaration du menu de réglages
function client_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('client','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.client',
		'icon' => 'fas fa-angle-right',
		'label' => 'Client'
	);
}

//Déclaration des pages de réglages
function client_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('client',array(
    "Général",
    'client_code_mask' => array("label"=>"Masque de generation de code",
    	"legend"=>"Macro possibles : <br>
    	{{id}} : Identifiant unique client<br>
    	{{label}} : libellé client, possible {{label[x]}} ou x est le nombre de lettres a conserver<br>
    	{{M}} : Mois sur 2 digits<br>
    	{{Y}} : Années sur 4 digits<br>
    	{{y}} : Années sur 2 digits<br>
    	{{rand(x,y)}} : Chiffre aléatoire entre x et y<br>
    	",
    	"placeholder"=>"ex: {{label[2]}}-{{Y}}{{M}}-{{id}}","type"=>"text"),
   
    'client_enable_map' => array("label"=>"Afficher la mini-map","type"=>"boolean"),
    'client_sheet_max_field' => array("label"=>"Nombre de champs avant résumé","type"=>"number"),
    'client_type' => array("label"=>"Type de client","type"=>"list",'values' => array('individual'=>'Particulier','firm'=>'Entreprise','both'=>'Les deux'), 'default'=>'both'),
    'client_condition' => array("label"=>"Statut de client","type"=>"list",'values' => array('prospect'=>'Prospect','client'=>'Client','both'=>'Les deux'), 'default'=>'both'),
    'client_hide_fields' => array("label"=>"Masquer les uid champs suivants","legend"=>"Uid des champs séparés par saut de ligne (ex: condition <saut de ligne> siret","type"=>"textarea"),
    'Api',
     'client_api_search_url' => array("label"=>"Api de recherche client","placeholder"=>"ex: https://entreprise.data.gouv.fr/api/sirene/v1/full_text/{{label}}","type"=>"text"),
     'client_api_sheet_url' => array("label"=>"Api de fiche client","placeholder"=>"ex: https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/?siren={{siret}}","type"=>"text"),
      "Terminologie",
    'client_holding_singular' => array("label"=>"Libellé des holdings (singulier)",'placeholder'=>'holding',"type"=>"text"),
    'client_label_plural' => array("label"=>"Libellé des clients (pluriel)",'placeholder'=>'clients',"type"=>"text"),
    'client_label_singular' => array("label"=>"Libellé des clients (singulier)",'placeholder'=>'client',"type"=>"text"),
    
));



//Affichage du/des widget(s)
function client_widget(&$widgets){
	global $myUser;
	require_once(PLUGIN_PATH.'dashboard'.SLASH.'DashboardWidget.class.php');
	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'client';
	$modelWidget->title = 'Client';
	$modelWidget->icon = 'fas fa-user-tie';
	$modelWidget->background = '#6B5B95';
	$modelWidget->load = 'action.php?action=client_widget_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js?v=2'];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v=2'];
	$modelWidget->description = "Module de gestion des clients et prospects";
	$widgets[] = $modelWidget;
}
  

function client_tab_menu(&$clientMenu,$client){
	$menu = new MenuItem();
	$menu->label = 'Accueil';
	$menu->url = '#tab=home';
	$menu->slug = 'home';
	$menu->sort = -1;
	$clientMenu[] = $menu;
}

function client_tab_page($slug){
	if($slug != 'home') return;
	require_once(__DIR__.SLASH.'tab.home.php');
}

function client_merge($clientBase,$clientToMerge,&$logs){
	require_once(__DIR__.SLASH.'Client.class.php');
	$logs[] = ' Migration des sous clients attachés ';
	Client::change(array('parent'=>$clientBase->id),array('parent'=>$clientToMerge->id));
	$logs[] = ' Migration des contacts attachés ';
	ContactPerson::change(array('uid'=>$clientBase->id),array('scope'=>'client','uid'=>$clientToMerge->id));
	$logs[] = ' Migration des adresses attachées ';
	Address::change(array('uid'=>$clientBase->id),array('scope'=>'client','uid'=>$clientToMerge->id));
}

//Declaration des évenements et entités workflow
Plugin::addHook("workflow_event", function(&$events){
	//Evenement entité
	$events[] = WorkflowEvent::registerEntity(__DIR__.SLASH.'Client.class.php');
	//Evenement liste d'entité
	$events[] = WorkflowEvent::registerList(__DIR__.SLASH.'Client.class.php');
});




global $myFirm;
if( $myFirm->has_plugin('fr.core.export')){
	require_once(__ROOT__.PLUGIN_PATH.'export'.SLASH.'ExportModel.class.php');

	ExportModel::add('client','client-sheet', 'Fiche client', function($parameters){
		global $myUser,$myFirm;
		
		require_once(__DIR__.SLASH.'Client.class.php');

		$client = new Client();

		if(isset($parameters['description']) && $parameters['description']!=true && !empty($parameters['id']))
			$client =  Client::getById($parameters['id']);
		
		$data['client'] = ExportModel::fromEntity($client);
		


		unset($data['client']['value']['parent']);

		

		if(empty($client->comment)) unset($data['client']['value']['comment']['value']);



		$data['client']['value']['category']['value'] = 
		isset($data['client']['value']['category']) && $data['client']['value']['category']['value']!=0 ? Dictionary::getById($data['client']['value']['category']['value'])->label: 'Aucune';
		
		if(!empty($data['client']['value']['type']['value'])) 
			$data['client']['value']['type']['value'] = Client::types($data['client']['value']['type']['value'])['label'];
		

		if(!empty($data['client']['value']['condition']['value']))
			$data['client']['value']['condition']['value'] = Client::conditions($data['client']['value']['condition']['value'])['label'];
		$data['client']['value']['created']['value'] = $data['client']['value']['created']['value'];
		$data['client']['value']['updated']['value'] = $data['client']['value']['updated']['value'];
		$data['client']['value']['updater']['value'] = User::byLogin($data['client']['value']['updater']['value'])->fullName();
		$data['client']['value']['creator']['value'] = User::byLogin($data['client']['value']['creator']['value'])->fullName();

		//export des contacts liés
		$data['client']['value']['contacts'] = array('label'=>'Liste des contacts', 'type'=>'list','value' => array());
		$contacts = $client->id == 0 ? array(new ContactPerson()) :ContactPerson::loadAll(array('scope'=>'client','uid'=>$client->id));
		foreach($contacts as $contact){
			$contact->meta();

			$contactRow = ExportModel::fromEntity($contact)['value'];

			$contactRow['phone']['value'] = $contact->phone;
			$contactRow['mobile']['value'] = $contact->mobile;
			$contactRow['mail']['value'] = $contact->mail;

			$data['client']['value']['contacts']['value'][] = $contactRow;
			
		}

		//export des adresses liées
		$data['client']['value']['addresses'] = array('label'=>'Liste des adresses liées', 'type'=>'list','value' => array());
		$addresses = $client->id == 0 ? array(new Address()) :$client->addresses();
		foreach($addresses as $address){
			$data['client']['value']['addresses']['value'][] = ExportModel::fromEntity($address)['value'];
		}
		
		//export des champs custom 
		if($myFirm->has_plugin('fr.core.dynamicform')){
			Plugin::need('dynamicform/DynamicForm');
			$data['client']['value']['dynamicFields'] = array('label'=>'Champs personnalisés liés', 'type'=>'list','value' => array());
			$dynamicFields = DynamicForm::show('client-sheet-custom',array(
				'arrayOutput' => true,
				'firm' => $myFirm->id,
				'uid' => $client->id,
				'scope' => 'client'
			));

			$fieldTypes = FieldType::available();
			if(is_array($dynamicFields)){
				foreach($dynamicFields as $dynamicField){
						$value = $dynamicField['value'];
						if(!isset(  $fieldTypes[$dynamicField['type']->slug])   ) continue;
						$type = $fieldTypes[$dynamicField['type']->slug];
						
						if(!empty($value) && property_exists($type,"onRawDisplay")){
							$method = $type->onRawDisplay;
							$value = $method($value,array('type'=>$type));
						}
					$data['client']['value']['dynamicFields']['value'][] = array(
						'value' => array('label' => $dynamicField['label']  , 'value' => $value )
					);
				}
			}
		}


		

		$data['client']['value']['logo'] = array(
			'label'=>'Logo du client',
			'type'=>'image',
			'value' => file_get_contents($client->logo(true))
		);

		if(empty($client->comment)) $data['client']['value']['comment']['value'] = false;
		
		$parent = new Client();
		$parent->label = 'Aucun';

		if(!empty($client->parent)) $parent = Client::getById($client->parent);
			
		$data['client']['value']['parent'] = ExportModel::fromEntity($parent);
		// print_r($data['client']['value']['parent']);
		// print_r($client->parent);
		// exit();
		return $data;
	});



}

function client_list_action(){
?>
 <a class="dropdown-item" onclick="client_client_search({exportMode:true,keepChecked:true});" href="#"><i class="fa fa-file-export"></i> Exporter en excel</a>
 <a class="dropdown-item" onclick="client_client_mail_copy();" href="#"><i class="far fa-envelope-open"></i> Copier les e-mails</a>
 <?php
}

function client_filter(&$filters){
	global $conf;
	require_once(__DIR__.SLASH.'Client.class.php');
	$types = array();
	foreach (Client::types() as $key => $value) 
	   $types[$key] = $value['label'];
	
	$conditions = array();
	foreach (Client::conditions() as $key => $value) 
   		$conditions[$key] = $value['label'];


   	$hideFields = explode("\n",$conf->get('client_hide_fields'));

   	$currentFilters = array(
    'id' => '<option value="main.id" data-filter-type="number">Identifiant unique</option>',
    'type' => '<option value="main.type" data-filter-type="list" data-values=\''.json_encode($types).'\'>Type</option>',
    'condition' => '<option value="main.condition" data-filter-type="list" data-values=\''.json_encode($conditions).'\'>Statut</option>',
    'creator' => '<option value="main.creator" data-filter-type="user">Auteur</option>',
    'label' => '<option value="main.label" data-filter-type="text">Libellé</option>',
    'firm' => '<option value="fi.id" data-filter-type="firm">Etablissement</option>',
    'created' => '<option value="main.created" data-filter-type="date">Création</option>',
    'job' => '<option value="main.job" data-filter-type="text">Métier</option>',
    'code' => '<option value="main.code" data-filter-type="text">Code '.$conf->get('client_label_singular').'</option>',
    'siret' => '<option value="main.siret" data-filter-type="text">N° SIRET</option>',
    'address.city' => '<option value="address.city" data-filter-type="text">Ville</option>',
    'address.zip' => '<option value="address.zip" data-filter-type="text">Code postal</option>',
    'mail.value' => '<option value="mail.value" data-filter-type="text">Adresse E-mail</option>',
    'phone.value' => '<option value="phone.value" data-filter-type="text">N° Téléphone</option>',
    'comment' => '<option value="main.comment" data-filter-type="text">Commentaire</option>',
    'internal_contact' => '<option value="internal_contact" data-filter-type="user">Client contact</option>'
	);

	foreach($currentFilters as $key=>$filter){
		if(in_array($key,$hideFields)) unset($currentFilters[$key]);
	}

	$filters = array_merge($filters, $currentFilters);
}


function client_search_view(&$columns){
	global $conf;
	$hideFields = explode("\n",$conf->get('client_hide_fields'));
	$mycolumns = array(
            'logo' => 
                array(
                    'head' => '<th></th>',
                    'body' => '<td class="align-middle text-center"><img class="client-logo-mini avatar-rounded" data-src="{{logo}}"></td>',
                ),
             'label' => 
                array(
                    'head' => '<th data-sortable="main.label">Libellé</th>',
                    'body' => '<td class="align-middle">
                        <a href="index.php?module=client&page=sheet.client&id={{id}}" data-type="card" data-show-delay="800" data-action="client_card" data-parameters=\'{"id":"{{id}}"}\'>{{{label}}}</a> {{#pseudonym}}<small class="text-muted">({{{pseudonym}}})</small>{{/pseudonym}}
                        {{#holding}}<br><small class="text-muted">Sous etablissement de <a href="index.php?module=client&page=sheet.client&id={{holding.id}}">{{{holding.label}}}</a></small>{{/holding}}
                        {{#affiliate}}<br><small class="text-muted">'.$conf->get('client_holding_singular').' de <span class="text-info" title="{{#affiliate.rows}}{{label}}{{/affiliate.rows}}">{{affiliate.count}} Etablissement(s)</span></small>{{/affiliate}}
                        {{#job}}<br/><small class="text-muted">{{{job}}}</small>{{/job}}
                    </td>',
                ),
            'code' => 
                array(
                    'head' => '<th data-sortable="code" data-available="created">Code '.($conf->get('client_label_singular')).'</th>',
                    'body' => '<td class="align-middle">{{code}}</td>',
                ),
            'city' => 
                array(
                    'head' => '<th data-sortable="address.city">Ville</th>',
                    'body' => '<td class="align-middle text-warning" title="{{address.street}} {{address.complement}} {{address.zip}} {{address.city}}">
                        {{address.city}} {{#address.zip}}
                        <small class="text-muted">
                            ({{address.zip}}) 
                            <a title="Afficher sur Google Map" target="_blank" href="{{address.mapurl}}"><i class="fas fa-map-marked-alt text-muted ml-2 pointer"></i></a>
                        </small>
                        {{/address.zip}}
                    </td>',
                ),
            'type' => 
                array(
                    'head' => '<th data-sortable="main.type" data-available="main.type">Type</th>',
                    'body' => '<td class="align-middle">{{type.label}}</td>',
                ),
            'category' => 
                array(
                    'head' => '<th data-sortable="category.label" data-available="main.category">Categorie</th>',
                    'body' => '<td class="align-middle">{{category.label}}</td>',
                ),

            'mail' => 
                array(
                    'head' => '<th data-sortable="mail.value" data-available="mail">E-mail</th>',
                    'body' => '<td class="align-middle"><a href="mailto:{{mail}}">{{mail}}</a></td>',
                ),
            'phone' => 
                array(
                    'head' => '<th data-sortable="phone.value" data-available="main.phone">Téléphone</th>',
                    'body' => '<td class="align-middle">{{phone}}</td>',
                ),
            'comment' => 
                array(
                    'head' => '<th data-sortable="main.comment" data-available="main.comment">Commentaire</th>',
                    'body' => '<td class="align-middle" ><small style="display:block;max-width:300px;max-height:40px;overflow:auto;">{{comment}}</small></td>',
                ),
            'condition' => 
                array(
                    'head' => '<th data-sortable="main.condition" data-available="main.created">Statut</th>',
                    'body' => '<td class="align-middle">{{condition.label}}</td>',
                ),
            'firm' => 
                array(
                    'head' => '<th data-sortable="fi.label" data-available="fi.label">Etablissement</th>',
                    'body' => '<td class="align-middle">{{firm.label}}</td>',
                )
        );
	foreach($mycolumns as $key=>$column){
		if(in_array($key,$hideFields)) continue;
		$columns[$key]= $column;
	}
}

function client_sheet(&$sheet,$client){
global $conf;
$contacts = $client->contacts();

$phone = isset($contacts[Contact::MOBILE]) ? $contacts[Contact::MOBILE] : new Contact();
$mail = isset($contacts[Contact::PROFESSIONAL_MAIL]) ? $contacts[Contact::PROFESSIONAL_MAIL] : new Contact();

$sheet = array();

	$hideFields = explode("\n",$conf->get('client_hide_fields'));

	$sheet['label'] = array(
		'client-type' => 'firm',
		'label' => 'Libellé <small>'.( !empty($client->code) ? '(N°'.$client->code.')':'').'</small>',
		'before' => '<div class="input-group">',
		'after' => '
		<div class="input-group-append editable-only"> '.(!empty($conf->get('client_api_search_url')) ? '<span class="btn text-muted"  onclick="client_internet_modal()"><i class="fas fa-globe pointer" data-tooltip title="Récuperer les informations sur internet"></i></span>':'').'
		</div>
		<div class="input-group-append"> <span class="btn text-muted"  onclick="client_switch_pseudonym(true)"><i class="fas fa-sort" data-tooltip title="Renseigner un pseudonyme"></i></span>
		</div>
		</div>',
		'sort' => 10,
		'block-class' => 'type-both block-label',
		'attributes' => array(
			'onchange' => '"client_client_check_duplicate(this)"'
		),
		'class' => 'text-uppercase',
		'value' => html_entity_decode($client->label)
	);
	$sheet['pseudonym'] = array(
		'client-type' => 'firm',
		'label' => 'Pseudonyme',
		'before' => '<div class="input-group">',
		'after' => '
		<div class="input-group-append"> <span class="btn text-muted"  onclick="client_switch_pseudonym(false)"><i class="fas fa-sort" data-tooltip title="Renseigner le libellé"></i></span>
		</div>
		</div>',
		'sort' => 11,
		'block-class' => 'type-both block-pseudonym hidden',
		'class' => 'text-uppercase',
		'value' => html_entity_decode($client->pseudonym)
	);


	if(!in_array('job',$hideFields))
	$sheet['job'] = array(
		'client-type' => 'firm',
		'label' => 'Métier',
		'sort' => 20,
		'block-class' => 'type-both',
		'value' => html_entity_decode($client->job)
	);

	
	$sheet['firstname'] = array(
		'client-type' => 'individual',
		'label' => 'Prénom',
		'sort' => 8,
		'block-class' => 'type-individual',
		'attributes' => array(
			'onkeyup' => '"client_name_change()"'
		),
		'value' => html_entity_decode($client->firstname)
	);
	$sheet['name'] = array(
		'client-type' => 'individual',
		'label' => 'Nom',
		'sort' => 9,
		'block-class' => 'type-individual',
		'class' => 'text-uppercase',
		'attributes' => array(
			'onkeyup' => '"client_name_change()"'
		),
		'value' => html_entity_decode($client->name)
	);

	if($conf->get('client_condition') == 'both' || empty($conf->get('client_condition')) ){
		$sheet['condition'] = array(
			'sort' => 30,
			'label' => 'Statut',
			'type' => 'choice',
			'block-class' => 'condition-block',
			'values' => array(
				'prospect' => 'Prospect',
				'client' => 'Client',
			),
			'value' => $client->condition == '' ? 'prospect' : $client->condition,
		);
	}

	if(!in_array('category',$hideFields))
	$sheet['category'] = array(
		'sort' => 40,
		'label' => 'Catégorie',
		'block-class' => 'field-block',
		'type' => 'dictionary',
		'attributes' => array(
			'data-slug' => '"client_category"',
			'data-value' => '"'.$client->category.'"'
		)
	);

	if(!in_array('mail',$hideFields))
	$sheet['mail'] = array(
		'sort' => 60,
		'label' => 'E-Mail',
		'before' => '<div class="input-group">',
		'after' => '</div>',
		'attributes' => array(
			'onchange' => '"client_client_check_duplicate(this)"'
		),
		'value' => html_entity_decode($mail->value)
	);

	if(!in_array('phone',$hideFields))
	$sheet['phone'] = array(
		'sort' => 70,
		'label' => 'Téléphone',
		'before' => '<div class="input-group">',
		'after' => '</div>',
		'attributes' => array(
			'onchange' => '"client_client_check_duplicate(this)"'
		),
		'value' => normalize_phone_number($phone->value)
	);

	if(!in_array('siret',$hideFields))
	$sheet['siret'] = array(
		'sort' => 80,
		'client-type' => 'firm',
		'label' => 'N° SIRET',
		'block-class' => 'type-firm',
		'before' => '<div class="input-group">',
		'after' => '</div>',
		'attributes' => array(
			'onchange' => '"client_client_check_duplicate(this)"'
		),
		'value' => $client->siret
	);

	if(!in_array('comment',$hideFields))
	$sheet['comment'] = array(
		'type' => 'textarea',
		'label' => 'Commentaire',
		'value' => html_entity_decode($client->comment)
	);
}

function client_field_type(&$types){
	//User
	$type = new FieldType();
	$type->slug = 'client';
	$type->label = 'Client';
	$type->sqlType = 'integer';
	$type->default_attributes = 
	array(
		'class'=>'"form-control"',
		'data-type'=>'"client"',
		'type'=>'"text"'
	);

	$type->filter = array(
			'operators' => array (
				'=' =>array('view'=>'client'),
				'!='  =>array('view'=>'client'),
				'null' =>array(),
				'not null' =>array()
			)
	);

	$type->onInput  = $types['text']->onInput;


	$type->onRawDisplay = function($value,$options = array()){
		if(is_null($value) || empty($value)) return;
		require_once(__DIR__.SLASH.'Client.class.php');
		if(empty($value)) return '';
		$client = Client::getById($value);
		return !$client ? '' : $client->label;
	};

	$type->onHtmlDisplay = function($value,$options){
		if(is_null($value) || empty($value)) return;
		require_once(__DIR__.SLASH.'Client.class.php');
		$client = Client::getById($value);
		return '<a href="index.php?module=client&page=sheet.client&id='.$value.'">'.$client->label.'</a>';
	};

	
	$type->icon = 'fab fa-black-tie';
	$type->description = 'Autocompletion sur les clients';
	$type->default = '';
	$types[$type->slug] = $type; 
}




function client_sheet_option(&$options){
		global $myUser,$_,$myFirm;
		if($myUser->can('client','configure')): 
			$options[] = '<div class="dropdown-item pointer user-select-none px-3" title="Fusionner ce client" onclick="client_client_merge_edit()"><i class="fas fa-object-group text-warning"></i> Fusionner</div>';
		endif; 
		if($myUser->can('export', 'read') && $myFirm->has_plugin('fr.core.export')) : 
			$options[] = '<div class="d-inline-block" data-type="export-model" data-parameters=\''.stripslashes(json_encode(array("plugin"=>"client","dataset"=>"client-sheet","id"=>$_['id']))).'\'>
					<div class="dropdown-item pointer user-select-none px-3" title="Exporter ce client"><i class="fas fa-download"></i> Exporter</div>
				</div>';
	    endif; 
	    if($myUser->can('client','edit') || ($_['id']!=0 && $myUser->can('client_sheet','edit',$_['id']))): 
	       	$options[] = '<div class="dropdown-item pointer user-select-none px-3" data-type="right" 
       				data-scope = "client_sheet" data-uid = "'.$_['id'].'"><i class="fas fa-user-lock"></i>  Permissions sur la fiche</div>';
       	endif; 
       	if($myUser->can('client','delete') || ($_['id']!=0 && $myUser->can('client_sheet','delete',$_['id']))): 
				$options[] = '<div class="dropdown-item text-danger pointer user-select-none px-3" title="Supprimer ce client" onclick="client_client_delete()"><i class="fa fa-trash-alt"></i> Supprimer</div>';
		endif;
}

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 
Plugin::addCss('/css/component.css');
Plugin::addJs('/js/component.js');

//Mapping hook / fonctions
Plugin::addHook("install", "client_install");
Plugin::addHook("uninstall", "client_uninstall"); 


Plugin::addHook("menu_main", "client_menu"); 
Plugin::addHook("page", "client_page");  

Plugin::addHook("menu_setting", "client_menu_setting");    
Plugin::addHook("content_setting", "client_content_setting");   
Plugin::addHook("widget", "client_widget"); 
Plugin::addHook("client_merge", "client_merge");

Plugin::addHook("field_types", "client_field_type");
Plugin::addHook("client_sheet_option", "client_sheet_option");

Plugin::addHook("client_list_action","client_list_action");
Plugin::addHook("client_menu", "client_tab_menu");    
Plugin::addHook("client_page", "client_tab_page");    
Plugin::addHook("client_filter", "client_filter");
Plugin::addHook("client_sheet", "client_sheet");
Plugin::addHook("client_search_view","client_search_view");

?>