<?php 
	require_once(__DIR__.SLASH.'Client.class.php');
	$max = $widget->data('client_number');
	$max = $max =="" ? 5 : $max ;
	$last5 = Client::staticQuery('SELECT * FROM {{table}} WHERE state = ? ORDER BY `created` DESC LIMIT '.$max,array(Client::ACTIVE),true);
	$number = Client::staticQuery('SELECT count(id) results FROM {{table}} WHERE state=? AND parent IS NOT NULL',array(Client::ACTIVE))->fetch();
?>
<div class="client-container">
	<a href="index.php?module=client&title=Client" class="pt-1 pb-1 pl-2 pr-2 d-block text-center">
		<h2><span class="text-white"><?php echo $number['results']; ?></span> Clients</h2>
	</a>
	<div class="client-search m-2">Accès rapide <input type="text" onchange="widget_client_search(this);" class="form-control" data-type="client"></div>
	<hr class="mt-0">
	<h5 class="ml-2 mb-3 mt-3"><?php echo $max>0 ? $max.' dernier'.($max>1?'s':'').' client'.($max>1?'s':'') : 'Aucun nouveau client'; ?></h5>

	<div class="last-clients">
		
		<ul class="list-unstyled clients text-center mb-0 pb-4">
		<?php foreach($last5 as $client): ?>
			<!-- @TODO: Mettre le logo client en background de chaque li -->
			<li class="m-3">
				<a href="index.php?module=client&page=sheet.client&id=<?php echo $client->id; ?>" target="_blank" class="text-center">
					<div>
						<i class="far fa-user fa-2x d-block pt-2 pb-1"></i>
						<span class="d-block text-center" title="<?php echo htmlentities($client->label); ?>"><?php echo $client->label; ?></span>
					</div>
				</a>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>
</div>