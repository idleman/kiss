
function widget_client_init(){
	init_components('.client-container');
}

function widget_client_search(input){
	var id = $(input).val();
	if(!id) return;
	window.location = 'index.php?module=client&page=sheet.client&id='+id;
}

function client_widget_configure_save(widget,modal){
	var data = $('#client-widget-form').toJson();
	data.action = 'client_widget_configure_save';
	data.id = modal.attr('data-widget');

	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}