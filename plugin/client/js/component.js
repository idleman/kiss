
/*
Pour un client : 
<input type="text" data-type="client" class="form-control" id="my_client">
Pour un contact lié au client #7
<input type="text" data-type="contact" data-scope="client" data-uid="7" class="form-control" id="my_contact">
*/

function init_components_client(input){
	var data = {
		editData: function(){
			return {before : input.attr('data-before')}
		},
		data: function(){
			return {
				before: input.attr('data-before'),
				firm: input.attr('data-firm'),
				parent: input.attr('data-parent')
			}
		}
	};
	
	input.component_autocomplete('client',$.extend(data,{
		skin: function(item){
			var html = '';
			var re = new RegExp(input.val(),"gi");

			label = item.label.replace(re, function (x) {
				return '<strong>'+x+'</strong>';
			});

			html += '<div class="media">';
			if(item.logo) html += '<img src="'+item.logo+'" class="avatar-mini avatar-rounded align-self-center mr-2" alt="Logo client">';
			html += '<div class="my-auto media-body client-autocomplete user-infos"><small class="d-flex flex-column">'
			if(item.parentLabel) html+='<small class="text-muted">'+item.parentLabel+'</small>' ;
			html+='<span>'+label ;
			if(input.attr('data-extra') && input.attr('data-extra').split(',').indexOf('id')!=-1) html += '<small> #' + item.id+'</small>';
			html +='</span>'; 
			html += '</small></div>'
			html += '</div>'
			return html;
		},
		onClick: function(selected,element){
			container = input.data("data-component");
			input.val(selected.id);
			var label = selected.label;
			if(selected.parentLabel) label +=' ('+selected.parentLabel+')';
			container.val(label);
			input.trigger('click').trigger('change');
		},
		onLoad: function(component,item){
			var label = item.label;
		
			if(item.parent) label+=' ('+item.parent.label+')';
			component.container.val(label);
		}
	}));
	
}