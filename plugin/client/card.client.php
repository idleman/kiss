<div class="client-card">
	<div class="top-part">
		<div class="header-top">
			<div class="icon">
				<img class="avatar-rounded logo" src="<?php echo $client->logo(); ?>" alt="Logo du client">
			</div>
			<div>
				<h3 class="label"><a title="Plus de détails" href="index.php?module=client&page=client.detail&id=<?php echo $client->id; ?>" target="_blank">
					<?php echo $client->label; ?></a>
				</h3>
				
				<small class="mail text-muted">Créé par <?php echo $client->creator; ?> le <?php echo date('d-m-Y',$client->created); ?></small>
				<?php if($client->siret!=''): ?>
					<br><small class="text-box"> N° SIRET <a href="https://www.societe.com/cgi-bin/search?champs=<?php echo $client->label; ?>"><?php echo $client->siret; ?></a></small>
				<?php endif; ?>
			</div>
		</div>
		<div class="content-block">
			<?php 
			$addresses = $client->addresses();
			if(count($addresses)!=0): 
				foreach ($addresses as $address) : 
			?>
				<div class="mt-2">
				<div class="font-weight-bold">Adresse [<?php echo Address::types($address->type)['label']; ?>]: </div>
				<div class="text-box"><?php echo $address->fullName(); ?></div>
			</div>
			<?php	endforeach; ?>
			
			<?php endif; ?>
			
		 
			<div class="clear"></div>
		</div>
	</div>

	<?php $contacts = ContactPerson::staticQuery('SELECT * FROM {{table}} co WHERE scope="client" AND uid = ? LIMIT 5',array($client->id),true);
	if(count($contacts)>0): ?>
	<hr>
	<div class="bottom-part pb-2 ">

			<small class="font-weight-bold text-uppercase text-muted">Contacts : </small>
			<ul class="client-card-contacts">
			<?php 
			foreach($contacts as $contact): ?>
				<li>
				 <?php  echo $contact->fullName(); ?><br>
				 <?php  if($contact->mail!=''): ?>
				 <a class="contact-mail" href="mailto:<?php  echo $contact->mail; ?>"><i class="far fa-envelope-open"></i> <?php  echo $contact->mail; ?></a>
				 <?php endif; ?>
				 <?php  if($contact->phone!=''): ?>
				 <a class="contact-phone" href="tel:<?php  echo $contact->phone; ?>"><i class="fas fa-mobile-alt"></i> <?php  echo $contact->phone; ?></a>
				<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>
	</div>
	<?php endif; ?>
</div>