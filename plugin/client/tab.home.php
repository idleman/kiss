<?php 
global $myUser,$_,$conf,$myFirm;


if(!$myUser->can('client','read') && $_['id']!=0 && !$myUser->can('client_sheet','read',$_['id'])) throw new Exception("Permission non accordée sur cette fiche");

require_once(__DIR__.SLASH.'Client.class.php');
$client = Client::provide();
$clientType = $conf->get('client_type');




//hook champs de fiche client
Plugin::callHook('client_sheet',array(&$sheet,$client));

$index = 1;
$moreFields = array();
$maxSheetFields = !empty($conf->get('client_sheet_max_field')) ? $conf->get('client_sheet_max_field') : 6;

$sheetOptions = array();
Plugin::callHook('client_sheet_option',array(&$sheetOptions));


?>
<div id="client-form"  class="client-form <?php if($conf->get('client_enable_map')) echo 'client-form-map' ?>" data-action="client_client_save" data-id="<?php echo $client->id; ?>">
	<div class="client-map">
		<div class="gmap_canvas">
			<iframe width="430" height="500" id="gmap_canvas" src="" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
		</div>

	</div>
	
	
	<div class="client-info">
		<div class="client-thumbnail" title="Editer le logo" style="background-image:url(action.php?action=client_assets_load&type=logo&client=<?php echo $client->id; ?>)"></div>
		<?php if(count($sheetOptions) > 0): ?>
		<div class="dropdown btn-options  btn-delete-client">
			<div class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-ellipsis-v text-muted"></i>
			</div>
			<div class="dropdown-menu py-1" aria-labelledby="dropdownMenuButton">
				<?php 
				foreach($sheetOptions as $sheetOption) echo $sheetOption;
				?>
			</div>
		</div>
		<?php endif; ?>

		<?php $client->type = empty($client->type) ? 'firm' : $client->type;  ?>
		<label>Type</label>
		<div class="type-choice">
			<?php foreach(Client::types() as $slug=>$type): ?>
			<label>
				<input type="radio" onclick="client_type_change()" data-type="data-client-type" <?php echo ($client->type==$slug?'checked="checked"':'') ?> name="type" value="<?php echo $slug ?>"> <?php echo $type['label'] ?>
			</label>
			<?php endforeach; ?>
		</div>
	<?php 

		$types = FieldType::available();
		$fields = FieldType::toForm($sheet);

		foreach($fields as $key=>$field): ?>
			<span class="<?php echo isset($field['data']['block-class'])?$field['data']['block-class']:'';?>"> 
			<?php echo $field['label']; ?>
			<?php echo isset($field['data']['before'])?$field['data']['before']:''; ?>
			<?php echo $field['input']; ?>
			<?php echo isset($field['data']['after'])?$field['data']['after']:''; ?>
			</span>
		<?php endforeach;

		?>
	<?php if(!empty($moreFields)): ?>
		<section id="more-fields-container" class="more-fields-container hidden">
		<?php foreach ($moreFields as $field)
			echo $field; ?>
		</section>
		<a class="btn btn-link w-100 text-center text-muted btn-more-fields pt-1" title="Afficher plus de champs" id="btn-more-fields" onclick="client_toggle_more_fields(this);"><i class="position-relative fas fa-chevron-down"></i></a>
	<?php endif; ?>


	<?php
	global $myFirm;
		if($myFirm->has_plugin('fr.core.dynamicform')){
			Plugin::need('dynamicform/DynamicForm');
			?>
			<div class="client-dynamic-fields">
			<?php echo Dynamicform::show('client-sheet-custom',array(
				'scope'=>'client',
				'uid'=>$client->id
			));
			?></div><?php
		}
	?>
		<div onclick="client_client_save();" class="btn btn-info w-100 text-uppercase editable-only">
			<i class="fas fa-check"></i> Enregistrer
		</div>
	
	</div>
	<div class="clear"></div>
</div>

<?php if($myUser->can('client_history','read')): ?>
<div class="client-history">
	<h4 class="d-inline-block">Historique</h4>
	<div class="text-left ml-3 d-inline-block noPrint text-muted">
	   <span onclick="window.print();" class="ml-1 pointer"  title="Imprimer la page"><i class="fas fa-print"></i></span>
	   <span onclick="client_core_history_search(null,true);" id="export-clients-btn" class="ml-1 pointer" title="Exporter les résultats"><i class="fas fa-file-export"></i></span>
	 </div>

	<div class="row">
	    <div class="col-md-12">


	    		<textarea data-type="wysiwyg" id="history-comment"></textarea>
	    		<div class="btn btn-success mb-2" onclick="client_core_history_save();"><i class="fas fa-check"></i>  Enregistrer</div>

	        <select id="client-history-filters" data-type="filter" data-label="Recherche" data-function="client_core_history_search">
	            <option value="comment" data-filter-type="text">Libellé</option>
	            <option value="created" data-filter-type="date">Date</option>
	            <option value="creator" data-filter-type="user">Auteur</option>
	        </select>
	    </div>
	</div>
	
	<div class="row">
		<!-- search results -->
		<div class="col-xl-12 client-history">
			<ul id="historys" data-entity-search="client_core_history_search">
				<li class="hidden item-line" data-id="{{id}}">
					<div class="d-inline-block client-history-date">
						{{created.dayShortName}}
						<i class="far fa-calendar"><span>{{created.day}}</span></i>
						<small>{{created.monthName}}</small>
					</div>
					<div class="d-inline-block client-history-content">
						<div class="w-100 text-right client-history-author">
							 <img src="action.php?action=core_account_avatar_download&amp;user={{creator.login}}&amp;extension=jpg" class="avatar-mini avatar-rounded avatar-login" title="{{creator.fullName}}"> 
							<small>{{created.time}}</small>
						</div>
						{{{comment}}}
						{{#editable}}
						<ul class="client-history-options">
							<li><i class="fas fa-pen" onclick="client_history_edit(this)"></i></li>
							<li><i class="fas fa-trash" onclick="client_core_history_delete(this)"></i></li>
						</ul>
						{{/editable}}
					</div>
				</li>
			</ul>
	        <br>
	        <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
	        <ul class="pagination justify-content-center"  data-range="5">
	            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');client_core_history_search();">
	                <span class="page-link">{{label}}</span>
	            </li>
	        </ul>
		</div>
	</div>
</div>
<?php endif; ?>

<!-- Modal -->
<div class="modal fade" id="client-internet-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Recherche du client sur le net</h5><div class="modal-preloader" ><i></i></div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <ul class="list-group choices">
        	<li class="list-group-item pointer hidden" onclick="client_api_load(this);">
				<h5 class="font-weight-bold">{{label}}</h5>
			 	{{#job}}<div class="text-muted"><i class="fas fa-atom"></i> {{job}}</div>{{/job}}
				{{#street}}<div class="text-dark"><i class="fas fa-map-marker-alt"></i> {{street}} {{city}} ({{zip}})</div>{{/street}}
				{{#size}}<div class="text-muted"><i class="fas fa-sitemap"></i> {{size}}  ~ {{employees}} employé</div>{{/size}}
			</li>
        </ul>

    

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>