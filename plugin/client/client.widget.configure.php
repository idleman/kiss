<?php 

$max = $widget->data('client_number');
$max = $max == "" ? 5 : $max;
?>
<div id="client-widget-form">
	<label for="client-max">Nombre de nouveaux clients à afficher :</label>
	<input type="number" class="form-control mb-2" value="<?php echo $max; ?>" name="client-max" id="client-max">
</div> 