<?php
global $myUser,$myFirm,$conf;



if(!$myUser->can('client','read') && !$myUser->can('client_sheet','read',0,array('contains'=>true))) 
    throw new Exception("Permission non accordée");

require_once(__DIR__.SLASH.'Client.class.php');


$filters = array();
Plugin::callHook('client_filter',array(&$filters));

//Colonnes possibles pour la table
$columns = array();
Plugin::callHook('client_search_view',array(&$columns));

//Filtres && colonne tableau en fonction des champs dynamiques
if($myFirm->has_plugin('fr.core.dynamicform') || ($myFirm->id==-1 && Plugin::is_active('fr.core.dynamicform')) ){
    Plugin::need('dynamicform/DynamicForm');
    $options = array();
    if(!empty($_['firm']) && is_numeric($_['firm'])) $options['firm'] = $_['firm'];
   
    //Récuperation des champs custom
    $fields = Dynamicform::list('client-sheet-custom',$options);
    //Ajout des champs custom en filtres
    $filters = array_merge($filters,Dynamicform::get_filters($fields));
    //Ajout des champs custom en colonnes dynamiques
    $columns = array_merge($columns,Dynamicform::get_dynamic_columns($fields));
}


?>

<div class="row">
    <div class="col-md-12">
        <div class="d-flex my-2 w-100">
            <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des <?php echo $conf->get('client_label_plural'); ?></h4>
            <div class="text-left ml-3 d-inline-block noPrint">
                <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred noPrint" title="Imprimer la page"><i class="fas fa-print"></i></div>
                <div onclick="client_client_search({exportMode:true,keepChecked:true});" id="export-clients-btn" class="btn btn-info rounded-0 btn-squarred ml-1 noPrint btn-export" title="Exporter tous les résultats recherché"><i class="fas fa-file-export"></i></div>
                <?php Plugin::callHook('client_buttons'); ?>
            </div>
            <div class="my-auto ml-auto mr-0 noPrint">
                <?php if($myUser->can('client', 'edit')) : ?>
                <a href="index.php?module=client&page=sheet.client" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="clear noPrint"></div>
    </div>
    
    <div class="col-md-8 noPrint search-container">
        <select id="filters" <?php echo $myUser->connected()?'data-slug="client-search"':''; ?> data-global-shortcut="#clients-shortcuts" data-user-shortcut="#clients-shortcuts" data-type="filter" data-label="Recherche" data-autosearch="false" data-function="client_client_search">
            <?php foreach ($filters as $filter): echo $filter; endforeach; ?>
        </select>

        <!-- bouttons de recherche rapide pré-enregistrées -->
        <ul id="clients-shortcuts" class="list-unstyled p-0 m-0 mb-2">
            <li class="hidden d-inline-block">
                <div class="py-0 btn btn-small btn-link" onclick="$('#filters').data('componentObject').filter_search_execute('{{uid}}');">
                    <i class="fas fa-search"></i> {{label}}
                </div>
            </li>
        </ul>

    </div>
    <div class="col-md-4">
        
    </div>
</div>
<h5 class="results-count"><span></span> Résultat(s) 
    <!-- selection mutliples lignes par checkbox -->
    <div class="btn-group btn-checkbox-action" data-count="0">
      <button type="button" class="btn btn-small btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="count font-weight-bold"></span> Selectionnés
      </button>
      <div class="dropdown-menu">
       
        <?php echo Plugin::callHook('client_list_action',array()); 

       ?>
      </div>
    </div>
</h5>
<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
    
		<table id="clients" 
            data-type="search-table" 
            data-slug="client-search-table"  
            data-entity-search="client_client_search"
            data-checkbox-action=".btn-checkbox-action"
            class="table table-striped table-clients" 
            >
            <thead>
                <tr>
                    <?php  foreach($columns as $row): echo $row['head']; endforeach; ?>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr data-id="{{id}}" class="hidden client-state-{{state}}">
                   
                    <?php foreach($columns as $row): echo $row['body']; endforeach;  ?>
	                <td class="align-middle text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            <a class="btn text-info noPrint" title="Consulter le client" href="index.php?module=client&page=sheet.client&id={{id}}<?php echo isset($_['embedded'])?'&embedded':'' ?>"><i class="<?php echo $myUser->can('client','edit')?'fas fa-pencil-alt':'fas fa-eye'; ?>"></i></a>
                               
                            {{#deletable}}
                            <div class="btn text-danger noPrint" title="Supprimer le client" onclick="client_client_delete(this);">
                                <i class="far fa-trash-alt"></i>
                            </div>
                            {{/deletable}} 
                        </div>
                    </td>
                </tr>
           </tbody>
        </table><br>
         <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
        <ul class="pagination justify-content-center noPrint"  data-range="5">
            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');client_client_search({keepChecked:true});">
                <span class="page-link">{{label}}</span>
            </li>
        </ul>
	</div>
</div>
