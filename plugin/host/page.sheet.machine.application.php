<?php 
User::check_access('host','read');
require_once(__DIR__.SLASH.'MachineApplication.class.php');
$machineapplication = MachineApplication::provide();

if($machineapplication->id==0){
	$machineapplication->version = '1.0.0';
}
?>

<div id="machine-application-form" class="machine-application-form" data-action="host_machine_application_save" data-id="<?php echo $machineapplication->id; ?>">

	<div class="input-group mt-2">
		<div class="input-group-prepend">
			<div class="input-group-text">Libellé</div>
		</div>
		<input  value="<?php echo $machineapplication->label; ?>"  onchange="host_machine_application_save();" class="form-control"  type="text"  data-id="label" >
	</div>
	<div class="input-group mt-2">
		<div class="input-group-prepend">
			<div class="input-group-text">Référent</div>
		</div>
		<input  value="<?php echo $machineapplication->referent; ?>"  data-type="user" onchange="host_machine_application_save();" class="form-control"  type="text"  data-id="referent" >
		<div class="input-group-prepend">
			<div class="input-group-text">Version</div>
		</div>
		<input  value="<?php echo $machineapplication->version; ?>" onchange="host_machine_application_save();" class="form-control"  type="text"  data-id="version" >
	</div>

	
	<div class="input-group  mt-2">
		<div class="input-group-prepend">
			<div class="input-group-text">Url</div>
		</div>
		<input  class="form-control" type="text" data-type="url" onchange="host_machine_application_save();" data-id="url" value="<?php echo $machineapplication->url; ?>">
		<div class="input-group-prepend">
			<div class="input-group-text">
				
					<input <?php echo $machineapplication->monitored == 1 ? 'checked="checked"' : '' ?>  data-type="checkbox" type="checkbox" data-id="monitored" onchange="host_machine_application_save();"> A surveiller

				</div>
		</div>
	</div>
	


	<!-- <div class="input-group input-group-sm mt-2">
		<div class="input-group-prepend">
			<div class="input-group-text">Machine liée</div>
		</div>
	<input  value="<?php echo $machineapplication->machine; ?>" onchange="host_machine_application_save();" class="form-control"  type="number"  id="machine" >
	</div> -->
</div>


