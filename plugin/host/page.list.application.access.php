<?php
global $myUser;
User::check_access('host','read');
require_once(__DIR__.SLASH.'ApplicationAccess.class.php');



    $defaultAccessFilters = array();
    if($application->id != 0){ 

        $defaultAccessFilters = filters_default(array(
        "",
        array('main.application' => $application->id)
        ));            
    }
  

?>

<div class="plugin-host plugin-host-access-list <?php echo ($application->id == 0?'':'embedded') ?>">
    <div class="row ">
        <div class="col-md-12">
            <div class="d-flex mb-2 w-100">
                <h6 class="d-inline-block my-auto mx-0 text-uppercase"> <span class="results-count"><span></span></span> Accès</h6>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-small px-1" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="host_application_access_search(null,true);" id="export-hosts-btn" class="btn btn-small px-1 btn-export" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                
                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
            <select id="application_access_filters" data-urlsearch data-type="filter" <?php echo $application->id!=0 ? 'data-hide-filters' : ''; ?> data-default='<?php echo json_encode($defaultAccessFilters); ?>' data-label="Recherche" data-function="host_application_access_search">
                <option value="main.application" data-filter-type="integer">Application</option>

            </select>
        </div>
        
    </div>

    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">
    		
            <table id="application-accesss" class="table table-striped " data-entity-search="host_application_access_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->
                        <th data-sortable="label">Libellé</th>
                        <th data-sortable="login">Identifiant</th>
                        <th data-sortable="password">Mot de passe</th>
                        <th class="no-embedded" data-sortable="application">Application liée</th>
                       
                        <th></th>
                    </tr>
                </thead>
                
                <thead class="noReadOnly">
                    <tr id="application-access-form" data-action="host_application_access_save" data-id="">
                     
                        <th><input  value="" class="form-control form-control-sm machine-application"  type="text"  value  id="label" ></th>
                        <th><input  value="" class="form-control form-control-sm machine-application"  type="text"  value  id="login" ></th>
                        <th><input  value="" class="form-control form-control-sm machine-application"  type="text"  data-type="password" data-generator  autocomplete="new-password"  value  id="password" ></th>
                        <th class="no-embedded" ><input  value="<?php echo $application->id; ?>" class="form-control"  type="number"  value  id="application" ></th>
                        
                        <th class="text-right align-top"><div onclick="host_application_access_save();" class="btn btn-success btn-mini"><i class="fas fa-check"></i></div></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	               
    	                <td class="align-middle">{{label}}</td>
    	                <td class="align-middle"><div class="pointer" onclick="copy_string($(this).text());$.message('info','Copié!')">{{login}}</div></td>
    	                <td class="align-middle"><code class="pointer" onclick="copy_string($(this).text());$.message('info','Copié!')">{{password}}</code></td>
    	                <?php if($application->id == 0): ?> <td class="align-middle">{{application}}</td> <?php endif; ?>
    	                
                        <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                
                                <div class="btn text-info noReadOnly" title="Éditer application_access" onclick="host_application_access_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                                <div class="btn text-danger noReadOnly" title="Supprimer application_access" onclick="host_application_access_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');host_application_access_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>