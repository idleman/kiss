<?php 
global $_,$myUser;
User::check_access('host','read');
require_once(__DIR__.SLASH.'Machine.class.php');
$machine = Machine::provide();


if($machine->id==0){
	$machine->manager = $myUser->login;
	$machine->label = 'Nouvel hébergement';
	$machine->ram = 2;
	$machine->cpu = 2;
	$machine->ip = '10.172.20.';
}
?>
<div class="plugin-host">
	<div id="machine-form" class="row justify-content-md-center machine-form" data-action="host_machine_save" data-id="<?php echo $machine->id; ?>">
		<div class="col-md-12 shadow-sm bg-white p-3">
			

			<div class="row">
				<div class="col-md-12">

				<div class="left py-2 host-preloader hidden"><small class="text-muted"><i class="fas fa-circle-notch fa-spin"></i> Enregistrement</small></div>

				<div class="btn btn-small btn-info mb-2 btn-success right" onclick="window.location = 'action.php?action=host_machine_open_with&tool=winscp&id='+$('#machine-form').attr('data-id');"><i class="fas fa-external-link-alt"></i> WinSCP</div>
				<div class="btn btn-small btn-info mb-2 btn-dark mx-2 right" onclick="window.location = 'action.php?action=host_machine_open_with&tool=putty&id='+$('#machine-form').attr('data-id');"><i class="fas fa-external-link-alt"></i> Putty</div>

				<!-- on ne specifie pas d'ui, le composant prendra $.urlParam('id') par defaut -->
				<div class="btn btn-small btn-light mb-2 right noReadOnly no-embedded" data-type="right" 
       				data-scope = "host_sheet"><i class="fas fa-user-lock"></i> Permissions</div>	

				<div class="btn btn-small btn-info mb-2 btn-dark mx-2 right no-embedded" data-scope="host_machine" data-uid="<?php echo $machine->id; ?>" data-show-important="true" data-type="history" data-tooltip title="Ouvrir l'historique de la vm"> <i class="far fa-comment-dots"></i></div>
				

				<div class="clear"></div>
				</div>
			</div>
			<div class="row mb-2">
				<div class="col-md-6 host-sheet">
				<?php
				global $myFirm;
				if($myFirm->has_plugin('fr.core.dynamicform')){
					Plugin::need('dynamicform/DynamicForm');
					echo Dynamicform::show('host-sheet',array(
						'scope'=>'host',
						'uid'=>$machine->id
					));
				}
				?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="input-group input-group-sm">
						<div class="input-group-prepend"><div class="input-group-text">Type</div></div>
						<select class="form-control select-control form-control" type="text" id="type">
						<?php foreach(Machine::types() as $slug=>$item): ?>
							<option <?php echo $machine->type == $slug ? 'selected="selected"' : '' ?> value="<?php echo $slug ?>" ><?php echo $item['label']; ?></option>
						<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group input-group-sm">
						<div class="input-group-prepend"><div class="input-group-text">Responsable</div></div>
						<input  value="<?php echo $machine->manager; ?>"  class="form-control"  type="text"  data-type="user"  id="manager" >
					</div>


				</div>
			</div>

			
			<div class="row">
				<div class="col-md-6">

					<div class="input-group input-group-sm mt-2">
						<div class="input-group-prepend"><div class="input-group-text">Libellé</div></div>
						<input  value="<?php echo $machine->label; ?>"  class="form-control"  type="text"  id="label" >
						
					</div>

				
				</div>
				<div class="col-md-6">
					<div class="p-2">
					<span class="text-muted">Créé le</span> <?php echo date('d/m/Y',$machine->created); ?> <span class="text-muted">par</span> <?php echo $machine->creator; ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="input-group input-group-sm mt-2">
						<div class="input-group-prepend"><div class="input-group-text">IP LAN</div></div>
						<input  value="<?php echo $machine->ip; ?>"  class="form-control"  type="text"  id="ip" >
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group input-group-sm mt-2">
						<div class="input-group-prepend"><div class="input-group-text">Os</div></div>
						<select class="form-control form-control-sm" type="text" data-type="dictionary" data-slug="host_machine_os" data-depth="1" key=data-disable-label data-value="<?php echo $machine->os; ?>" id="os" ></select>
					</div>
				</div>
			</div>
		

			<div class="row mb-3">
				<div class="col-md-4">
					<div class="input-group input-group-sm mt-2">
						<div class="input-group-prepend"><div class="input-group-text">MAC</div></div>
						<input  value="<?php echo $machine->mac; ?>"  class="form-control"  type="text"  id="mac" >
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group input-group-sm mt-2">
						<div class="input-group-prepend"><div class="input-group-text">RAM</div></div>
						<input  value="<?php echo $machine->ram; ?>"  class="form-control"  type="number"  id="ram" >
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group input-group-sm mt-2">
						<div class="input-group-prepend"><div class="input-group-text">Nb. CPU</div></div>
						<input  value="<?php echo $machine->cpu; ?>"  class="form-control"  type="number"  id="cpu" >
					</div>
				</div>
			</div>

		

			<div class="row bg-light machine-application-container">
				<div class="col-md-6">
					<?php require_once(__DIR__.SLASH.'page.list.machine.application.php'); ?>
				</div>
				<div class="col-md-6  host-application-container p-2">
					
				</div>
			</div>

			<h5>Commentaires</h5>
			<textarea  class="pt-0" type="text" data-type="wysiwyg" data-simple id="comment"><?php echo $machine->comment; ?></textarea>
		</div>
	</div>
</div>

