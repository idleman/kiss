<?php
require_once(__DIR__.SLASH.'Machine.class.php');
require_once(__DIR__.SLASH.'MachineApplication.class.php');

global $myUser;
User::check_access('host','read');
?>
<div class="col-md-12">
	<h3>Plan d'adressage</h3>
	<hr/>
	<select id="root" class="form-control mb-2" onchange="host_plan_search()">
		<?php 
		$roots = array();
		foreach(Machine::loadAll() as $machine){
			$root = explode('.',$machine->ip); 
			if(count($root)<4) continue;
			array_pop($root);
			$root = implode('.', $root );
			$roots[$root] = 1;
		}
		foreach($roots as $root=>$useless): ?>
			<option value="<?php echo $root; ?>"><?php echo $root; ?></option>
		<?php endforeach; ?>
	</select>
	
	<table class="table table-borderless table-hover table-sm table-plan" id="table-plan">
	<?php 
		for($i =0; $i<32;$i++):
	?>
		<tr>
			<?php 
				for($u = 0; $u<8;$u++):
				$final = ( $u * 32) +$i ;
				
			?>
				<th class="bg-dark text-light text-center" style="width:20px;" ><?php echo $final; ?></th><td class="p-0" style="width:10%" data-ip="<?php echo $final; ?>"></td>
			<?php endfor; ?>
		</tr>
	<?php endfor; ?>
	</table>

	<template id="table-plan-template">
		<a href="index.php?module=host&id={{id}}" class="d-block w-100 h-100 bg-light text-center"><small><i title="{{type.label}}" class="{{type.icon}}"></i> {{label}}</small></a>
	</template>
</div> 
