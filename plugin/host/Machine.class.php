<?php
/**
 * Define a Machine / Environnement
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class Machine extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $manager; //Responsable (Utilisateur)
	public $ip; //IP Lan (Texte)
	public $client; //Client (int)
	public $os; //Système d'exploitation (Liste configurable)
	public $mac; //Adresse MAC (Texte)
	public $ram; //Mémoire Vive (RAM) (Décimal)
	public $cpu; //Nb Cœur CPU (Décimal)
	public $comment; //Commentaire (Texte enrichis)
	public $type; //Type (Liste classique)

	const TYPE_VM = 'virtual-machine';
	const TYPE_DOCKER = 'docker-container';

	protected $TABLE_NAME = 'host_machine';
	public $entityLabel = 'Machine / Environnement';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text', 'label' => 'Libellé'),
		'manager' => array('type'=>'user', 'label' => 'Responsable'),
		'ip' => array('type'=>'text', 'label' => 'IP Lan'),
		'client' => array('type'=>'int', 'label' => 'Client'),
		'os' => array('type'=>'dictionary', 'label' => 'Système d\'exploitation'),
		'mac' => array('type'=>'text', 'label' => 'Adresse MAC'),
		'ram' => array('type'=>'decimal', 'label' => 'Mémoire Vive (RAM)'),
		'cpu' => array('type'=>'decimal', 'label' => 'Nb Cœur CPU'),
		'comment' => array('type'=>'wysiwyg', 'label' => 'Commentaire'),
		'type' => array('type'=>'list', 'label' => 'Type')
	);

	public $links = array(
		'client' => 'Client',
	);

	//Colonnes indexées
	public $indexes = array();
	
	//liste des Type possibles
	public static function types($key=null){
		$items = array(
			self::TYPE_VM => array('label'=>'Machine virtuelle','icon'=>'fas fa-laptop'), 
			self::TYPE_DOCKER => array('label'=>'Conteneur Docker','icon'=>'fab fa-docker'), 
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non définis');
	}
	//Vérifie un droit sur une machine
	public function can($right){
		global $myUser;
		return ($myUser->can('host','configure') || $myUser->can('host_sheet',$right,$this->id) || $myUser->login == $this->creator);
	}

	public function remove(){
		require_once(__DIR__.SLASH.'MachineApplication.class.php');
		foreach(MachineApplication::loadAll(array('machine'=>$this->id)) as $application){
			$application->remove();
		}
		History::delete(array('scope'=>'host_machine','uid'=>$this->id));
		Machine::deleteById($this->id);
	}
}
?>