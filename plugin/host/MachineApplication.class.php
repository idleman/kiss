<?php
/**
 * Define a Application sur la machine
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class MachineApplication extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $version; //Version (Décimal)
	public $url; //Url (Adresse web)
	public $monitored; //A surveiller (Vrai ou Faux)
	public $machine; //Machine liée (int)
	public $referent; //Référent de l'application (user)
	
	const TYPE_UP = 'up';
	const TYPE_DOWN = 'down';

	protected $TABLE_NAME = 'host_machine_application';
	public $entityLabel = 'Application sur la machine';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text', 'label' => 'Libellé'),
		'version' => array('type'=>'decimal', 'label' => 'Version'),
		'url' => array('type'=>'url', 'label' => 'Url'),
		'monitored' => array('type'=>'boolean', 'label' => 'A surveiller'),
		'machine' => array('type'=>'int', 'label' => 'Machine liée'),
		'referent' => array('type'=>'user', 'label' => 'Référent de l\'application')
	);

	public $links = array(
		'machine' => 'Machine'
	);

	//Colonnes indexées
	public $indexes = array();
	
	public function remove(){
		require_once(__DIR__.SLASH.'ApplicationAccess.class.php');
		ApplicationAccess::delete(array('application'=>$this->id));
		MachineApplication::deleteById($this->id);
		History::delete(array('scope'=>'host_application','uid'=>$this->id));
	}

}
?>