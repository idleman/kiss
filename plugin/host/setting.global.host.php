<?php
global $myUser,$conf;
User::check_access('host','configure');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	
        <h3 class="mb-0"><i class="fab fa-ubuntu"></i> Réglages Hébergement <div onclick="host_setting_save();" class="btn btn-sm btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div></h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('host'); ?>


	    
    <h4>Systèmes d'exploitation</h4>
    <div data-type="dictionary-table" data-dictionary="host_machine_os"></div>
	    
    </div>


</div>
