<?php
global $myUser;
User::check_access('host','read');
require_once(__DIR__.SLASH.'Machine.class.php');

$types = array();
foreach (Machine::types() as $key => $type) {
    $types[$key] = $type['label'];
}


?>
<div class="plugin-host">

   
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-3">
             <div class="row">
                <div class="col-md-12 no-embedded">
                    <div class="d-flex my-2 w-100">
                        <h5 class="d-inline-block my-auto mx-0 text-uppercase results-count"><span></span> Machine(s)</h5>
	                    
                        <div class="my-auto ml-auto mr-0 noPrint">
                            <a class="btn btn-small px-1" data-tooltip data-placement="bottom" href="index.php?module=host&page=plan" target="_BLANK" title="Plan d'adressage">
                             <i class="fas fa-sitemap"></i>
                            </a>
                            <a class="btn btn-small" data-tooltip data-placement="bottom" href="index.php?module=host&page=ping" target="_BLANK" title="Ping des ndd">
                                <i class="fas fa-table-tennis"></i>
                            </a>
                    
                           
                            <div onclick="window.print();" class="btn btn-small px-1" data-tooltip data-placement="bottom" title="Imprimer la page"><i class="fas fa-print"></i></div>
                            <div onclick="host_machine_search(null,true);" id="export-hosts-btn" data-tooltip data-placement="bottom" class="btn btn-export px-1" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                            <?php if($myUser->can('host', 'edit')) : ?>
                            <div onclick="host_machine_edit();" class="btn btn-small btn-success rounded-0 btn-squarred" data-tooltip data-placement="bottom" title="Ajouter une machine"><i class="fas fa-plus"></i></div>
                            <?php endif; ?>
                        </div>
                        
                    </div>
                    <div class="clear noPrint"></div>
                </div>       
            </div>

            <select id="filters" data-type="filter" data-autosearch="false" data-label="Rech." data-function="host_machine_search"></select>
            <div class="machines-preloader hidden  p-2"><small class="text-muted"><i class="fas fa-circle-notch fa-spin-fast"></i> <div class="mt-2">Chargement...</div></small></div>
            <ul id="machines" class="list-group mt-2" data-entity-search="host_machine_search">
               
                    <li data-id="{{id}}" class="hidden {{class}} item-line list-group-item p-2" onclick="host_machine_edit(this);">
    	                <i class="{{type.icon}}"></i> <span class="font-weight-bold">{{label}}</span> {{#client}}- {{client.label}}{{/client}}  <i  title="Supprimer machine" onclick="event.stopPropagation();host_machine_delete(this);" class="far fa-trash-alt right text-muted noReadOnly"></i>
                        <br/><span class="text-muted">{{ip}} {{#os}}({{os.label}}){{/os}}</span>
                    </li>
          
            </ul><br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');host_machine_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
        <div class="col-xl-9 host-sheet-container">
            <!-- fiche -->
        </div>
    </div>
</div>