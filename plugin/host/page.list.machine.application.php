<?php
global $myUser;
User::check_access('host','read');
require_once(__DIR__.SLASH.'MachineApplication.class.php');




?>
<div class="plugin-host">
    <div class="row">
       
        
         <?php 
            //var_dump($machine->id);
            $defaultFilters = array();
            if($machine->id != 0){ 

                $defaultFilters = filters_default(array(
                "",
                array('main.machine' => $machine->id)
                ));            
            }
        
            ?>
        <div class="col-md-12">
            <select id="host_machine_application-filters" data-urlsearch <?php echo $machine->id!=0 ? 'data-hide-filters' : ''; ?> data-default='<?php echo json_encode($defaultFilters); ?>' data-type="filter" data-label="Recherche" data-function="host_machine_application_search">
                <option value="main.label" data-filter-type="text">Libellé</option>
                <option value="main.version" data-filter-type="integer">Version</option>
                <option value="main.url" data-filter-type="text">Url</option>
                <option value="main.monitored" data-filter-type="boolean">A surveiller</option>
                <option value="main.machine" data-filter-type="integer">Machine liée</option>
            </select>
        </div>
       
        
    </div>
    <h5 class="results-count my-2"><span></span> Application(s)
        <?php if($myUser->can('host', 'edit')) : ?>
              <a onclick="host_machine_application_edit();" class="btn btn-mini btn-success right noReadOnly text-light"><i class="fas fa-plus"></i></a>
        <?php endif; ?>
         <div class="clear noPrint"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">

            
            <!-- présentation liste -->
            <ul id="machine-applications" class="list-group mb-3" data-entity-search="host_machine_application_search">
                <li data-id="{{id}}" class="hidden item-line p-2 list-group-item pointer" onclick="host_machine_application_edit(this);">
                        
                        <div class="align-middle">
                            
                            <div class="d-inline-block mt-1"><span class="font-weight-bold">{{label}}</span> - {{#version}}<span class="text-muted">(v{{version}})</span>{{/version}}  <a href="{{url}}">{{url}}</a></div>


                        <div class="btn-group btn-group-sm right" role="group">
                            <div class="p-1">{{#monitored}} <i class="far fa-eye text-success" data-tooltip title="Application surveillée"></i>{{/monitored}}{{^monitored}}<i class="far fa-eye-slash text-muted" data-tooltip title="Application non surveillée"></i>{{/monitored}}</div>

                            <div class="btn btn-mini" data-scope="host_application" data-uid="{{id}}" data-show-important="true" data-type="history" data-tooltip title="Ouvrir l'historique de l'application"> <i class="far fa-comment-dots"></i></div>

                            <div class="btn text-dark" title="Voir les accès" data-tooltip onclick="host_application_access_page_search(this,event);"><i class="fas fa-user-lock"></i></div>

                            <div class="btn text-danger noReadOnly" title="Supprimer" onclick="host_machine_application_delete(this,event);"><i class="far fa-trash-alt"></i></div>
                        </div>
                        </div>
                        
                        <?php if($machine->id == 0): ?>
                            <div class="align-middle">{{machine.label}}</div>
                        <?php endif; ?>
                    
                        
                   
                </li>
            </ul>
            
          
          
    	</div>
    </div>
</div>