<?php 
User::check_access('host','read');
require_once(__DIR__.SLASH.'ApplicationAccess.class.php');
$applicationaccess = ApplicationAccess::provide();
?>
<div class="plugin-host">
	<div id="application-access-form" class="row justify-content-md-center application-access-form" data-action="host_application_access_save" data-id="<?php echo $applicationaccess->id; ?>">
		<div class="col-md-6 shadow-sm bg-white p-3">
			<h3>Accès à l\'Application  
			<div onclick="host_application_access_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<a href="index.php?module=host&page=list.application.access" class="btn btn-small btn-dark right  mr-2">Retour</a></h3>
			<label for="label">Libellé</label>
			<input  value="<?php echo $applicationaccess->label; ?>" class="form-control"  type="text"  id="label" >
			<label for="login">Identifiant</label>
			<input  value="<?php echo $applicationaccess->login; ?>" class="form-control"  type="text"  id="login" >
			<label for="password">Mot de passe</label>
			<input  value="<?php echo $applicationaccess->password; ?>" class="form-control"  type="text"  data-type="password"  autocomplete="new-password"  id="password" >
			<label for="application">Application liée</label>
			<input  value="<?php echo $applicationaccess->application; ?>" class="form-control"  type="number"  id="application" >
			<br/>
			
		</div>
	</div>
</div>

