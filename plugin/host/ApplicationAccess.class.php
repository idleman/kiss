<?php
/**
 * Define a Accès à l\'Application 
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class ApplicationAccess extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $login; //Identifiant (Texte)
	public $password; //Mot de passe (Mot de passe)
	public $application; //Application liée (Nombre Entier)
	
	protected $TABLE_NAME = 'host_application_access';
	public $entityLabel = 'Accès à l\'Application ';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text', 'label' => 'Libellé'),
		'login' => array('type'=>'text', 'label' => 'Identifiant'),
		'password' => array('type'=>'password', 'label' => 'Mot de passe'),
		'application' => array('type'=>'integer', 'label' => 'Application liée')
	);

	public $links = array(
		'application' => 'MachineApplication'
	);

	//Colonnes indexées
	public $indexes = array();
	
}
?>