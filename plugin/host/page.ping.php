<?php
require_once(__DIR__.SLASH.'Machine.class.php');
require_once(__DIR__.SLASH.'MachineApplication.class.php');

global $myUser;
User::check_access('host','read');
?>
<div class="col-md-12">
	<h3>Page de ping</h3>
	<hr/>
	<div class="btn btn-info" onclick="host_application_url_ping()"><i class="fas fa-table-tennis"></i> Check des url</div>
	<ul id="vm-ping-list" class="m-0 p-0">
	<li data-id="{{id}}" class="hidden bg-white p-3 my-2 mr-2 d-inline-block shadow-sm align-top w-20">
		
		<h5><i class="{{type.icon}}"></i> {{label}}</h5>
		<a href="index.php?module=client&page=sheet.client&id={{client.id}}" class="text-muted">{{client.label}}</a>
		<div>
			<small class="text-muted">{{ip}}</small><br/>
			<ul class="applications">
			{{#applications}}
				<li class="application" data-application="{{id}}">
					<a href="{{url}}">{{label}} - <small class="text-muted">{{url}}</small></a>
				<span class="ping-state pointer" title="Afficher les détails" onclick="$(this).next().toggleClass('hidden')"></span>
				<div class="ping-message hidden">Tout vas bien :)</div>
			</li>
			{{/applications}}
			</ul>
		</div>
    </li>
	</ul>

</div> 
<br/>