<?php 
global $myFirm,$myUser;
User::check_access('example','read');
require_once(__DIR__.SLASH.'ContactExample.class.php');
$contact = ContactExample::provide();


$data = array(
	'origin' => 'fr.core.example',
	'subject' => (!empty($contact->label) ? 'Envoi de la fiche contact de '.$contact->label : 'Envoi fiche contact'),
	'message' => "Contenu du message envoyé",
	'recipients' => array('to'=>array('claude@claude.fr', 'testouille@test.fr'), 'cc'=>array('carboncopy@test.net'), 'cci'=>array('another@email.fr')),
	'attachments' => $contact->documents()
);


?>
<div class="plugin-example">
	<div id="contact-form" class="row justify-content-md-center contact-form" data-action="example_contact_save" data-id="<?php echo $contact->id; ?>">
		<div class="col-md-6 shadow-sm bg-white p-3">
			<h3>ContactExample 
			<div onclick="example_contact_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<a href="index.php?module=example&page=list.contact" class="btn btn-small btn-dark right  mr-2">Retour</a>
			<div class="btn btn-small btn-info mb-2 btn-dark mx-2 right" data-scope="contact" data-uid="<?php echo $contact->id; ?>" data-show-important="true" data-type="history" data-tooltip title="Ouvrir l'historique"> <i class="far fa-comment-dots"></i></div>


			<?php if($contact->id>0): ?>

       			<!--
					data-scope : section / partie de code ou de module concerné par le droit (defaut : core)
					data-uid : Id spécifique d'entité concerné par le droit (defaut : $.urlParam('id'))
					data-edit : Afficher la case edition (defaut : true)
					data-read : Afficher la case lecture (defaut : true)
					data-recursive : Afficher la case récursif (defaut : false)
					data-configure : Afficher la case configuration (defaut : false)
					data-firm : Scoper sur un établissement, cache la selection d'établissement si définit (defaut : null)
					data-saveAction : Définir une action de save custom du droit (defaut : core_right_save)
					data-deleteAction :  Définir une action de delete custom du droit (defaut : core_right_delete)
       			-->
       			<div class="btn btn-small btn-dark right"  title="Permissions sur la fiche" data-tooltip data-type="right" 
       				data-scope = 'example_contact',
					data-uid = <?php echo $contact->id; ?>
       			><i class="fas fa-user-lock"></i></div>

       	
			<?php endif; ?>

			<div class="btn btn-small btn-dark right mr-2" data-tooltip title=" Envoi mail" onclick='sendmail_preview(<?php echo json_encode($data); ?>);'><i class="fas fa-paper-plane"></i></div>

			<span data-type="card" data-show-delay="200" data-action="example_contact_card" data-parameters='<?php echo json_encode(array('id'=>$contact->id)); ?>'><?php echo html_decode_utf8($contact->label); ?></span>
		</h3>

			
				
			<label for="label">Libellé</label>
			<input  value="<?php echo $contact->label; ?>" class="form-control"  type="text"  id="label" >
			<label for="birth">Date de naissance</label>
			<small class="text-muted"> (Input data-type="date")</small>
			<input  value="<?php echo empty($contact->birth)?'':date('d/m/Y',$contact->birth); ?>" class="form-control"  type="text"  data-type="date"  title="format jj/mm/aaaa"  placeholder="JJ/MM/AAAA"  id="birth" >
			<label for="hour">Heure de naissance</label>
			<small class="text-muted"> (Input data-type="hour")</small>
			<input  value="<?php echo $contact->hour; ?>" class="form-control"  type="text"  data-type="hour"  title="format hh:mm"  placeholder="13:37"  id="hour" >
			<label for="size">Taille</label>
			<input  value="<?php echo $contact->size; ?>" data-type="decimal" class="form-control"  type="text"  id="size" >
			
		
			<label for="phone">Téléphone</label>
			<small class="text-muted" data-tooltip title="* data-regex : regex de vérification (defaut : ^[0-9]{10}$, les espaces sont trimés pour vérification)<br>
			 * data-spacing : espace automatiquement les chiffre si collés (défaut true)<br>
			 * data-empty-field : vide le champs si invalide (défaut false)<br>
			 * data-invalid-class : définit la classe a ajouter si invalide (défaut .invalid-value)<br>
			 * data-blur : si définis, trigger le blur indiqué après validation du champs (le onblur classique trigger avant)<br>
			 * data-type-only : empeche l'utilisateur de taper autre caractere que définis dans cette express (defaut : [0-9\+\s])<br>"> (Input data-type="phone")</small>
			<input  value="<?php echo $contact->phone; ?>" class="form-control"  type="text"  data-type="phone"  id="phone" >
			<label for="website">Site perso</label>
			<small class="text-muted" data-tooltip title="* data-no-lock : ne pas afficher le cadenas sécurisé <br>
			* data-no-link : ne pas afficher le globe de lien <br>
			*data-default-scheme : [https/http] ajoute le scheme spcéifié si inexistant lors du blur"> (Input data-type="url")</small>
			<input  value="<?php echo $contact->website; ?>" class="form-control"  type="text"  data-type="url"  id="website" >
			<label for="mail">Email</label>
			<input  value="<?php echo $contact->mail; ?>" class="form-control"  type="mail"  data-type="mail"  pattern=".+@.+"  id="mail" >
			<label for="firm">Etablissement</label>
			<small class="text-muted">(Input data-type="firm")</small>
			<input  value="<?php echo $contact->firm; ?>" class="form-control"  type="text"  data-type="firm"  id="firm" >
				
			
			
			
			
			
			<label for="manager">Manager</label>
			<small class="text-muted" data-placement="right" data-tooltip title="* data-multiple			:	Autorise l'utilisateur a sélectionner  des entité multiples 
			* data-types (default: user) : définis les entité sélectionnables (ex: user,rank)	"> (Input data-type="user")</small>
			* data-types (default: user) : définis les entité sélectionnables (ex: user,rank)	
			* data-scope= (default : $myFirm->id) scope a la firme ciblée (0 pour toutes les firms)  "> (Input data-type="user")</small>

			<input  value="<?php echo $contact->manager; ?>" class="form-control"  type="text"  data-type="user"  id="manager" >
			<label for="address">Adresse</label>
			<small class="text-muted"> (Input data-type="location")</small>
			<input  value="<?php echo $contact->address; ?>" class="form-control"  type="text"  data-type="location"  id="address" >
			<label for="state">Localisation</label>
			<small data-tooltip title="* data-click : fonction js appellée lors d'un click sur la map
* data-latitude : latitude de la vue (si non renseigné, se base sur les marqueurs)
* data-longitude : longitude de la vue(si non renseigné, se base sur les marqueurs)
* data-zoom : zoom de la map" class="text-muted">(div data-type="map")</small><br/>
	<div data-type="map" data-zoom="16" data-click="example_map_click" style="height:300px;">
		[
			{
				"label" : "Les bikers insupportables",
				"latitude" : 44.81183294100969,
				"longitude" : -0.6146047155672256
			},
			{
				"label" : "Sys<strong>Dev</strong>",
				"latitude" : 44.81180753731187, 
				"longitude" : -0.6150445275851699
			},
			{
				"type" : "circle",
				"label" : "Leclerc",
				"radius" : 100,
				"latitude" : 44.810301604975486, 
				"longitude" : -0.6192602267205941
			},
			{
				"type" : "polygon",
				"label" : "Chinois",
				"backgroundColor" : "red",
				"borderColor" : "red",
				"points" : [
					[44.81238216817115, -0.6150852464139218],
				    [44.8124115648384, -0.6150025683594396],
				    [44.81236052554113, -0.6149765582247242],
				    [44.812335762552145, -0.6150543470901866]
				 ]
					
			}
		]
	</div>

			
			<label for="vehicle">Véhicule</label>
			<small class="text-muted" data-placement="right" data-tooltip title="* data-depth 			:	nb de profondeur de liste
* data-slug 			:	le slug de la liste mère à afficher
* data-value 			:	la valeur de l'entité  à récup en base
* data-output 			:	type de valeur de retour slug ou id de la liste (id par defaut)
* data-disable-label 	:	cache le label de sous-liste si mentionné">(Select data-type="dictionary")</small>
			<select class="form-control select-control" type="text" data-type="dictionary" data-slug="example_contact_vehicle" data-depth="2" key=data-disable-label data-value="<?php echo $contact->vehicle; ?>" id="vehicle" ></select>			
			<label for="storyshort">Histoire résumée</label>
			<textarea  class="form-control" type="text" id="storyshort"><?php echo $contact->storyshort; ?></textarea>
			<label for="story">Histoire riche</label>
			<small class="text-muted" data-placement="right" data-tooltip title="* data-label 			:	le label affiché dans la zone
* data-mention-user='user' || data-mention-user='rank' : activer les mentions d'utilisateurs interne par défaut user et rank
* data-mention-object='action' :activer les mentions # sur une action
* data-script-allow : désactive la supression automatique des tags <scripts> et <link> (anti xss)
"> (Textarea data-type="wysiwyg")</small>
			<textarea  class="" type="text" data-type="wysiwyg" id="story"><?php echo $contact->story; ?></textarea>
			<label for="password">Mot de passe</label>
			<small class="text-muted"> (Input data-type="password")</small>
			<input  value="<?php echo $contact->password; ?>" class="form-control"  type="text"  data-type="password"  autocomplete="new-password"  id="password" >
			<label for="icon">Icone préférée</label>
			<small class="text-muted"> (Input data-type="icon")</small>
			<input  value="<?php echo $contact->icon; ?>" class="form-control"  type="text"  data-type="icon"  id="icon" >
			<br/><label for="available">
				<input <?php echo $contact->available == 1 ? 'checked="checked"' : '' ?>  data-type="checkbox" type="checkbox" id="available"> Disponible  <small class="text-muted"> (Input data-type="checkbox")</small>
				</label>
			<br/>
			<label for="cv">CV</label>
			<small class="text-muted" title="
				Options possibles
				data-action : nom de l'action de gestion du composant (obligatoire)
				data-all-start : callback quand l'upload des fichiers débute - default : function(files){return true},
				data-all-complete : callback quand tous les fichiers sont terminés  - default : function(){},
				data-file-start : callback sur départ upload d'un fichier  - default : function(file){return true},
				data-file-complete : callback sur upload terminé d'un fichier  - default : function(file){},
				data-file-error : callback sur erreur d'un fichier  - default : function(file){},
				data-file-progress : callback sur progression d'un fichier - default : function(file){},
				data-over : classe sur le dragover - default : 'file-hover',
				data-label : Libellé à afficher sur la zone de drop - default : 'Faites glisser vos fichiers ici',
				data-data : extra données au format json (ex: {'id':12}) - default : {},
				data-limit : limit de fichiers  - default : 0,
				data-readonly : lecture seule - default : false
				data-extension: extensions autorisées -  default extensions dispo dans la classe File
				data-buttons : selecteur du coneteneur contenant des extra bouttons default : ''

				<!> : la classe component-file-default affiche en mode liste, la classe component-file-cover en mode image (cf exemple avatar juste après)
			">(Input data-type="file")</small>
			<input  value="" class="component-file-default bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-extension="jpg,png,bmp,jpeg,gif,svg,webp,docx,xlsx,pptx,msg,eml,pdf,zip,doc,xls,ppt,txt,csv,mp3,wav,mp4,avi,flv"  data-action="example_contact_cv"  data-id="cv"  data-data='{"id":"<?php echo $contact->id; ?>"}'  id="cv" >
			<label for="avatar">Avatar</label>
			<small class="text-muted">(Input data-type="file")</small>
			<input  value="" class="component-file-cover bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-limit="1"  data-extension="jpg,png,bmp,jpeg,gif,svg,webp"  data-action="example_contact_avatar"  data-id="avatar"  data-data='{"id":"<?php echo $contact->id; ?>"}'  id="avatar" >
			<label for="solvability">Solvabilité</label>
			<select class="form-control select-control" type="text" id="solvability">
			<?php foreach(ContactExample::solvabilities() as $slug=>$item): ?>
				<option <?php echo $contact->solvability == $slug ? 'selected="selected"' : '' ?> value="<?php echo $slug ?>" ><?php echo $item['label']; ?></option>
			<?php endforeach; ?>
			</select>
			<label for="handicap">Handicaps</label>
			<small class="text-muted" data-placement="right" data-tooltip title="* data-display			:	dropdown ou vide (vide=déplié)
			* data-button			:	libellé/html du button si diaply en dropdown<br>
			* data-values :valeurs possibles en json<br>
			* data-slug : s'ajoute à data-values par les valeurs du dictionary dont le slug est spécifié <br>
			* data-depth : spécifie la profondeur si un slug de distionnary est fournis <br>
			"> (Input data-type="checkbox-list")</small><br>
			<span class="text-warning">Mode plié, à choix multi niveau</span>
			<input  value="<?php echo $contact->handicap; ?>" class="form-control"  type="text"  data-type="checkbox-list"  data-depth="5" data-multi-level-select="true" data-slug="example_contact_handicap"  data-display="dropdown"  value="<?php echo $contact->handicap; ?>"  id="handicap" >

			<span class="text-warning">Mode plié, à choix multi niveau, avec JSON</span>
			<input  value="<?php echo $contact->handicap; ?>" class="form-control"  type="text"  data-type="checkbox-list"  data-depth="5" data-multi-level-select="true" data-values='{"cat-1":{"id":"cat-1","label":"Catégorie custom 1"},"cat-2":{"id":"cat-2","label":"Catégorie custom 2"},"blind":{"id":"blind","label":"Aveugle","childs":{"one":{"id":"blind_one","label":"Dun oeil"},"two":{"id":"blind_two","label":"Des deux"}}},"lvl1":{"id":"lvl1","label":"Level 1","childs":{"lvl2":{"id":"lvl2","label":"Level 2","childs":{"lvl3":{"id":"lvl3","label":"Level 3","childs":{"lvl4":{"id":"lvl4","label":"Level 4","childs":{"lvl5":{"id":"lvl5","label":"Level 5"}}},"lvl4_1":{"id":"lvl4_1","label":"Level 4.1"},"lvl4_2":{"id":"lvl4_2","label":"Level 4.2"}}}}}}}}'  data-display="dropdown"  value="<?php echo $contact->handicap; ?>"  id="handicap" >


			<span class="text-warning">Mode plié, à choix multi niveau, avec JSON simple</span>
			<input  value="<?php echo $contact->handicap; ?>" class="form-control"  type="text"  data-type="checkbox-list"  data-depth="5" data-multi-level-select="true" data-values='{"cat-1":"Catégorie custom 1", "cat-2":"Catégorie custom 2"}'  data-display="dropdown"  value="<?php echo $contact->handicap; ?>"  id="handicap" >
			
			<span class="text-warning">Mode plié, à choix simple niveau</span>
			<input  value="<?php echo $contact->handicap; ?>" class="form-control"  type="text"  data-type="checkbox-list"  data-depth="3" data-exclusive-depth="true" data-multi-level-select="false" data-slug="example_contact_handicap"  data-display="dropdown"  value="<?php echo $contact->handicap; ?>"  id="handicap" >
			
			<span class="text-warning">Mode déplié</span>
			<input data-type="checkbox-list"  type="text"  value="<?php echo $contact->handicap; ?>" data-slug="example_contact_handicap" data-depth="1" class="form-control" id="category2" data-values='{"cat-1":"Catégorie custom"}' />

			<label for="childs">Nb. Enfants</label>
			<input  value="<?php echo $contact->childs; ?>" class="form-control"  type="number"  id="childs" >
			


			<label for="properties">Tags</label>
			<small class="text-muted" data-placement="right" data-tooltip title="* data-multiple			:	Autorise l'utilisateur a sélectionner  des tag multiples
			* data-autocomplete			:	Propose une autocompletion sur les mots clés
			* data-dictionary-slug : a spécifier si l'autocomplete est lié a un dictionary">(Select data-type="tag")</small>
			<input  value="<?php echo $contact->properties; ?>" class="form-control"  type="text"  data-type="tag"  data-multiple=true  id="properties" >
			<label for="color">Couleur préférée</label>
			<small class="text-muted"> (Input data-type="color")</small>
			<input onchange="$('#mainMenu').css('backgroundColor',$(this).val())" value="<?php echo $contact->color; ?>" class="form-control"  type="text"  data-type="color"  id="color" >
			<label for="salary">Salaire brut</label>
			<input  value="<?php echo $contact->salary; ?>" class="form-control"  type=number  data-type="price"  step="0.01"  id="salary" >
			<label for="orientation">Orientation</label>
			<small class="text-muted" data-placement="right" data-tooltip title='* data-values	:	Valeurs possibles au format json ({"clé1":"valeur1","clé2":"valeur2"})'> (Input data-type="choice")</small><br/>
			<?php 
				$orientations = array();
				foreach(ContactExample::orientations() as $slug=>$item):
					$orientations[$slug] = $item['label'];
				endforeach;
			?>
			<label><input value="<?php echo  $contact->orientation ?>" data-values='<?php echo json_encode($orientations) ?>' data-type="choice" id="orientation" type="text" name="orientation"></label>
			


			
			<br/>

			<?php
		//Champs dynamiques
		global $myFirm;
			if($myFirm->has_plugin('fr.core.dynamicform')){
				Plugin::need('dynamicform/DynamicForm');
				echo Dynamicform::show('fiche-example',array(
					'scope'=>'contact',
					'uid'=>$contact->id
				));
			}
		?>

			<hr>
			<h4>Autres composants</h4>
			<small class="text-muted">Non lié a cette fiche contact</small>

			<label for="state">Etat</label>
			<small data-tooltip title="* data-icon : l'icone affichée près du label (optionnel)
* data-value : valeur à l'affichage de la page (optionnel)
* data-no-toggle : Supprime la fleche de dropdown nb: a mettre sur le select (optionnel)" class="text-muted">(Select data-type="dropdown-select")</small><br/>
			<select data-type="dropdown-select" class="item-state" id="state" data-value="active">
            	<option value="active" style="background-color:#2cbe4e;color:#ffffff;" data-icon="far fa-check-circle">Actif</option>
            	<option value="inactive" style="background-color:#c90000;color:#ffffff;" data-icon="far fa-times-circle">Inactif</option>
            </select><br/>
			
            <label for="tutor">Nom du tuteur :</label>
			<small class="text-muted"> (Div data-type="quickform")</small>
			<div class="quickform d-inline-block" data-type="quickform" data-title="Ajout rapide de tuteur :" data-loaded="example_quickform_buttons"  data-action="contact_quick_create"><i title="Ajoutez rapidement un tuteur." class="fas fa-user-plus"></i></div>
			<input type="text" value="" placeholder="Nom du tuteur" class="form-control" id="tutor" name="tutor"/>
			
			<br>
			<label for="doughnut">Donuts</label>
			<small class="text-muted"> (canvas data-type="doughnut")</small>
			<canvas data-type="doughnut" data-values="[1,3,4,7]" data-labels='["fib","on","acci"]' data-colors='["#f39c12","#2980b9","#27ae60"]'></canvas>
			<br>
			<label for="line">Line</label>
			<small class="text-muted"> (canvas data-type="line")</small>
			<canvas data-type="line" data-values="[1,3,4,7]" data-labels='["fib","on","acci"]' data-color='#8e44ad'>Graphique example</canvas>
			<br>
			<label for="bar">Bar</label>
			<small class="text-muted"> (canvas data-type="bar")</small>
			<canvas data-type="bar" data-values="[2,3,4,7]" data-labels='["fib","on","acci"]' data-colors='["#f39c12","#2980b9","#27ae60"]'>Graphique example</canvas>

			<label for="bar">Gauge</label>
			<small class="text-muted"> (canvas data-type="gauge")</small>
			<canvas data-type="gauge" data-values="[75,100]"  data-unity="€" data-legend="true" data-labels='["Atteint","Objectif"]'>Graphique example</canvas>

			<br>
			<label for="label">Widget de stats</label>
			<div>
			<?php if($myFirm->has_plugin('fr.core.statistic')) : ?>
				<?php
				Plugin::need('statistic/Widget');
				$firstWidget = Widget::loadAll(array(),array('id'));
				try{
					echo count($firstWidget)>0 ? Widget::show($firstWidget[0]->id) : 'Aucun widget existant';
				} catch(Exception $e){
					echo '<div class="alert alert-danger"><strong>Erreur : </strong>'.$e->getMessage().'</div>';
				} ?>
			<?php else: ?>
				Le plugin statistique doit être activé et au moins un widget créé pour voir cet exemple.
			<?php endif; ?>
		 	</div><br>

		 	<label for="dictionary-table">Tableau d'une liste</label>
			<small class="text-muted"> (div data-type="dictionary-table")</small>
			<!-- <div data-type="dictionary-table" data-dictionary="example_contact_vehicle"></div> -->
			<br>

			<?php if($myFirm->has_plugin('fr.core.stripe')) : ?>
				<label for="checkbox">Formulaire de paiement en ligne (via API Stripe)</label>
				<small class="text-muted"> (Div data-type="payment")</small><br>
				<small class="text-muted">Il faut configurer les clés API en config avant de pouvoir réaliser un paiement. (Voir <a href="https://stripe.com/docs/testing#cards" target="_blank">cette page</a> pour des cartes bleues de tests)</small>

				<div data-type="payment" data-action="example_stripe_pay"></div>
			<?php endif; ?>

			<?php if($myUser->can('export', 'read') && $myFirm->has_plugin('fr.core.export')) : ?>
			<div class="d-inline-block" data-type="export-model" data-pre-callback="contact_export_pre_callback" data-post-callback="contact_export_post_callback" data-parameters='<?php echo stripslashes(json_encode(array("plugin"=>"example","dataset"=>"contact-sheet","id"=>$contact->id,"destination"=>addslashes('contact'.SLASH.'documents'.SLASH.$contact->id.SLASH)))); ?>'>
				<div class="btn btn-primary"><i class="far fa-file"></i> Export modèle</div>
			</div>
       		<?php endif; ?>

			<br><label for="checkbox">Sélection de dossier picker</label>
			<small class="text-muted"  data-tooltip title="* data-editable : si spécifié, le chemin devient éditable aprés sélection  (optionnel)
			* data-root : pour scoper un dossier particulier de file (ex: data-root='documents' pour scoper la ged)"> (Div data-type="filepicker")</small><br>
			<input type="text" id="personalFolder" data-type="filepicker" value=""   class="form-control"/>
       		
       		<label>Radio button</label>
			<small class="text-muted"> (Input data-type="radio")</small><br/>
			<label><input checked="checked" value="key1" data-type="radio" type="radio" name="myName"> Radio boutton</label>
				
			<label for="jsontable">JSON Table</label>
			<small class="text-muted"> (Input data-type="jsontable")</small><br/>
			<input data-format="key-value" value='{"clé1":"valeur1","clé2":"valeur2"}' data-type="jsontable" type="text">

			<label for="jsontable2">JSON Table multiple champs</label>
			<small class="text-muted"> (Input data-type="jsontable")</small><br/>
			<input data-format="multiple-values" data-columns='{"slug":"valeur1","label":"valeur2","color":"valeur3"}' value='[{"slug":"valeur1","label":"valeur2","color":"valeur3"}]' data-type="jsontable" type="text">

			<label for="entitypicker">Entitée ciblée</label>
			<small class="text-muted"> (Input data-type="entitypicker")</small><br/>
			<input   data-type="entitypicker" type="text">

       		
		</div>
	</div>
</div>

