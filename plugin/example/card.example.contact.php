<div class="contact-card">
	<div class="top-part">
		<div class="header-top">
			<div class="icon">
				<img class="avatar-rounded" src="<?php echo $contact->picture(); ?>" alt="Image de profil">
				<!-- <i class="<?php echo $contact->icon; ?>"></i> -->
			</div>
			<div>
				<h3 class="label"><a href="index.php?module=example&page=sheet&id=<?php echo $contact->id; ?>" target="_blank"><?php echo $contact->label; ?></a></h3>
				<small class="mail text-muted"><?php echo $contact->creator; ?></small>
			</div>
		</div>
		<div class="content-block">
			<div>
				<div class="label-box">Anniversaire : </div>
				<div class="text-box"><span class="reference"><?php echo date('d/m/Y',$contact->birth); ?></span></div>
			</div>

			<div>
				<div class="label-box">Téléphone : </div>
				<div class="text-box"><span class="phone"><?php echo $contact->phone; ?></span></div>
			</div>
			<div>
				<div class="label-box">Adresse : </div>
				<div class="text-box"><span class="fax"><?php echo $contact->address; ?></span></div>
			</div>
		
			<div class="clear"></div>
		</div>
	</div>
	<hr>
	<div class="bottom-part row">
		<a class="btn btn-primary btn-sm ml-auto" href="index.php?module=example&page=sheet.contact&id=<?php echo $contact->id; ?>" target="_blank" title="Voir la fiche"><i class="fas fa-search"></i></a>
		<a href="mailto:<?php echo $contact->mail; ?>" class="btn btn-info btn-sm" title="Contacter par mail"><i class="fas fa-envelope"></i></a>
	</div>
</div>