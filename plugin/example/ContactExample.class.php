<?php
/**
 * Define a ContactExample
 * @author Valentin CARRUESCO
 * @category Plugin
 * @license MIT
 */

class ContactExample extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $phone; //Téléphone (Téléphone)
	public $birth; //Date de naissance (Date)
	public $hour; //Heure de naissance (Heure)
	public $firm; //Etablissement (Etablissement)
	public $manager; //Manager (Utilisateur)
	public $address; //Adresse (Adresse)
	public $properties; //Tags (Etiquettes)
	public $vehicle; //Véhicule (Liste configurable)
	public $storyshort; //Histoire résumée (Texte Long)
	public $story; //Histoire riche (Texte enrichi)
	public $password; //Mot de passe (Mot de passe)
	public $icon; //Icone préférée (Icône)
	public $available; //Disponible (Vrai ou Faux)
	public $cv; //CV (Fichier)
	public $avatar; //Avatar (Image)
	public $solvability; //Solvabilité (Liste classique)
	public $handicap; //Handicaps (Liste de cases à cocher)
	public $childs; //Nb. Enfants (Nombre Entier)
	public $size; //Taille (Décimal)
	public $color; //Couleur préférée (Couleur)
	public $salary; //Salaire brut (Prix)
	public $orientation; //Orientation (Choix)
	public $website; //Site perso (Adresse web)
	public $mail; //Email (E-mail)

	protected $TABLE_NAME = 'example_contact';
	public $entityLabel = 'Contact Example';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text', 'label' => 'Libellé'),
		'phone' => array('type'=>'phone', 'label' => 'Téléphone'),
		'birth' => array('type'=>'date', 'label' => 'Date de naissance'),
		'hour' => array('type'=>'hour', 'label' => 'Heure de naissance'),
		'firm' => array('type'=>'firm', 'label' => 'Etablissement','link'=>'class/Firm.class.php'),
		'manager' => array('type'=>'user', 'label' => 'Manager'),
		'address' => array('type'=>'address', 'label' => 'Adresse'),
		'properties' => array('type'=>'tag', 'label' => 'Tags'),
		'vehicle' => array('type'=>'dictionary', 'label' => 'Véhicule'),
		'storyshort' => array('type'=>'textarea', 'label' => 'Histoire résumée'),
		'story' => array('type'=>'wysiwyg', 'label' => 'Histoire riche'),
		'password' => array('type'=>'password', 'label' => 'Mot de passe'),
		'icon' => array('type'=>'icon', 'label' => 'Icone préférée'),
		'available' => array('type'=>'boolean', 'label' => 'Disponible'),
		'solvability' => array('type'=>'list', 'label' => 'Solvabilité'),
		'handicap' => array('type'=>'checkbox-list', 'label' => 'Handicaps'),
		'childs' => array('type'=>'integer', 'label' => 'Nb. Enfants'),
		'size' => array('type'=>'decimal', 'label' => 'Taille'),
		'color' => array('type'=>'color', 'label' => 'Couleur préférée'),
		'salary' => array('type'=>'price', 'label' => 'Salaire brut'),
		'orientation' => array('type'=>'choice', 'label' => 'Orientation'),
		'website' => array('type'=>'url', 'label' => 'Site perso'),
		'mail' => array('type'=>'mail', 'label' => 'Email')
	);
	

	//Colonnes indexées
	public $indexes = array();


	//liste des solvabilité possibles
	public static function solvabilities($key=null){
		$items = array(
			'solvable' => array('label'=>'Solvable') ,
			'unsolvable' => array('label'=>'Non solvable') ,
			'tocheck' => array('label'=>'A verifier') ,
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non défini');
	}
	//liste des orientations possibles
	public static function orientations($key=null){
		$items = array(
			'machoman' => array('label'=>'macho convaincu'), 
			'almostgay' => array('label'=>'Metrosexuel'), 
			'binary' => array('label'=>'Ethero sexuel'), 
			'villagepeople' => array('label'=>'100% Gay'), 
			'chucknorris' => array('label'=>'Un peu de tout ça')
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non défini');
	}

	public function dir(){
		return File::dir().'contact'.SLASH.$this->id.SLASH;
	}

	public function picture($returnPath = false){
		return 'action.php?action=example_contact_avatar&type=download&path='.base64_encode('example/contact/'.$this->id.'/avatar.*');
	}

	function documents(){
		$documents = array();
		foreach(glob(__ROOT__.FILE_PATH.'contact'.SLASH.'documents'.SLASH.$this->id.SLASH.'*.*') as $file){
			if(get_OS() === 'WIN') $file = utf8_encode($file);
			$documents[] = array(
				'path' => 'contact'.SLASH.'documents'.SLASH.$this->id.SLASH.basename($file),
				'url' => 'action.php?action=contact_download_document&path='.$this->id.SLASH.rawurlencode(basename($file)),
				'name' => basename($file),
				'icon' => getExtIcon(getExt($file))
			);
		}
		return $documents;
	}
}
?>
