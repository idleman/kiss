<?php
global $myFirm,$myUser;
User::check_access('example','read');
require_once(__DIR__.SLASH.'ContactExample.class.php');

$solvabilities = array();
foreach (ContactExample::solvabilities() as $key => $solvability) {
    $solvabilities[$key] = $solvability['label'];
}

$orientations =  array();
foreach (ContactExample::orientations() as $key => $orientation) {
    $orientations[$key] = $orientation['label'];
}

$defaultFilters = filters_default(array(
    "jean",
    array(
        'birth' => "17/09/1998",
        'join' => 'or'
    ),
    array(
        'phone:like' => "9754"
    )
));

$filters = array();
$columns = array();
 //Filtres && colonne tableau en fonction des champs dynamiques
if($myFirm->has_plugin('fr.core.dynamicform')){
    Plugin::need('dynamicform/DynamicForm');
    //Récuperation des champs custom
    $fields = Dynamicform::list('fiche-example');
    //Ajout des champs custom en filtres
    $filters = array_merge($filters,Dynamicform::get_filters($fields));
    //Ajout des champs custom en colonnes dynamiques
    $columns = array_merge($columns,Dynamicform::get_dynamic_columns($fields));
}


?>

<div class="plugin-example">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des contacts</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                   
                    <!-- impression de la page courante -->
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" data-tootltip title="Imprimer la page"><i class="fas fa-print"></i></div>
                    
                    <!-- export classique des résultats -->
                    <div onclick="example_contact_search(null,true);" id="export-examples-btn" class="btn btn-info rounded-0 btn-squarred ml-1 btn-export" data-tootltip title="Exporter les résultats"><i class="fas fa-file-export"></i></div>

                    <!-- exports modèles si disponibles -->
                    <?php if($myUser->can('export', 'read') && $myFirm->has_plugin('fr.core.export')) : ?>
                        <div class="right mr-2 d-inline-block" data-type="export-model" data-default="xlsx2" data-pre-callback="contact_export_pre_callback" data-post-callback="contact_export_post_callback" data-parameters='<?php echo stripslashes(json_encode(array("plugin"=>"example","dataset"=>"contact-list"))); ?>'>
                            <div data-tootltip title="Exporter selon modèle" class="btn btn-info rounded-0 btn-squarred ml-2"><i class="far fa-file"></i></div>
                        </div>
                    <?php endif; ?>
                </div>
                
                
                <div class="my-auto ml-auto mr-0 noPrint">
                    <?php if($myUser->can('example', 'edit')) : ?>
                    <a href="index.php?module=example&page=sheet.contact" title="Ajouter" data-tooltip class="btn btn-success right"><i class="fas fa-plus"></i></a>
                    <?php endif; ?>
                </div>


                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
              <!-- 
            voir filter.component.js pour la documentation
            data-default='<?php echo json_encode($defaultFilters); ?>' 
             -->
            <select 
                id="example_contact-filters" 
                data-type="filter" 
                data-function="example_contact_search"
                data-slug="contact-search"  
                data-label="Trouver un contact" 
                data-global-shortcut="#contact-shortcuts" 
                data-user-shortcut="#contact-shortcuts" 
                data-show-url-advanced="false"
                data-configure="<?php echo $myUser->can('example','configure') ? 1:0 ?>" 
            >
              <!-- pour une recherche simple, ne pas spécifier d'options dans cette liste -->
                <option value="main.label" data-filter-type="text">Libellé</option>
                <option value="main.phone" data-filter-type="phone">Téléphone</option>
                <option value="main.birth" data-filter-type="date">Date de naissance</option>
                <option value="main.hour" data-filter-type="hour">Heure de naissance</option>
                <option value="main.firm" data-filter-type="firm">Etablissement</option>
                <option value="main.manager" data-filter-type="user">Manager</option>
                <option value="main.address" data-filter-type="address">Adresse</option>
                
                <option value="main.properties" data-operator-delete='["in","not in"]' data-filter-type="tag">Tags</option>
                <option value="main.vehicle" data-slug="example_contact_vehicle" data-depth="1" data-filter-type="dictionary">Véhicule</option>
                <option value="main.handicap" data-slug="example_contact_handicap"  data-filter-type="list" data-depth="3" data-multi-level-select="true">Handicaps</option>
                <option value="main.solvability" data-filter-type="list"  data-values='<?php echo json_encode($solvabilities); ?>' >Solvabilité</option>
                <option value="main.orientation"  data-filter-source='<?php echo json_encode($orientations); ?>' data-filter-type="choice">Orientation</option>

                <option value="main.storyshort" data-filter-type="textarea">Histoire résumée</option>
                <option value="main.story" data-filter-type="wysiwyg">Histoire riche</option>
                <option value="main.password" data-filter-type="password">Mot de passe</option>
                <option value="main.icon" data-filter-type="icon">Icone préférée</option>
                <option value="main.available" data-filter-type="boolean">Disponible</option>
                <option value="main.childs" data-operator-view='{"in":{"view":"tag"}}' data-filter-type="integer">Nb. Enfants</option>
                <option value="main.size" data-filter-type="decimal">Taille</option>
                <option value="main.color" data-filter-type="color">Couleur préférée</option>
                <option value="main.salary" data-filter-type="price">Salaire brut</option>
                <option value="main.website" data-filter-type="url">Site perso</option>
                <option value="main.mail" data-filter-type="mail">Email</option>
                <option value="main.mobile" data-filter-type="phone">Téléphone portable</option>
                <?php foreach($filters as $filter):
                    echo $filter; 
                endforeach; ?>
            </select>

            <!-- Exemple de templates de bouttons de recherche rapide pré-enregistrées -->
            <div id="contact-shortcuts" class="p-0 m-0 mb-2">
                <h4 class="has-shortcut mb-0 text-uppercase user-select-all">Mes recherches</h4>
                <ul class="pl-0 list-unstyled">
                    <li class="hidden d-inline-block">
                        <div class="btn btn-small btn-link pl-0" onclick="$('#filters').data('componentObject').filter_search_execute('{{uid}}');"><i class="fas fa-search"></i> {{label}}</div>
                    </li>
                </ul>
            </div>
        </div>
        
    </div>
    <h5 class="results-count my-2"><span></span> Résultat(s)
        <!-- bloc de preference de pagination -->
        <small class="text-muted right text-muted text-small"><div class="d-inline-block mr-1" data-type="pagination-preference" data-table="#contacts" data-value="20" data-max-item="100"></div></small><div class="clear"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">
		
            <!-- présentation tableau -->
            <table 
                id="contacts" 
                class="table table-striped " 
                data-slug="contact-search-table" 
                data-entity-search="example_contact_search"
                data-column-search="example_contact_search"
            >
                <thead>
                    <tr>

                        <th data-sortable="label">Libellé</th>
                        <th data-sortable="phone">Téléphone</th>
                        <th data-available="birth" data-sortable="birth">Date de naissance</th>
                        <th data-available="hour" data-sortable="hour">Heure de naissance</th>
                        <th data-sortable="firm">Etablissement</th>
                        <th data-sortable="manager">Manager</th>
                        <th data-available="address" data-sortable="address">Adresse</th>
                        <th data-sortable="properties">Tags</th>
                        <th data-sortable="vehicle">Véhicule</th>
                        <th data-available="storyshort" data-sortable="storyshort">Histoire résumée</th>
                        <th data-available="story" data-sortable="story">Histoire riche</th>
                        <th data-available="password" data-sortable="password">Mot de passe</th>
                        <th data-sortable="icon">Icone</th>
                        <th data-sortable="available">Disponible</th>
                        <th data-sortable="cv">CV</th>
                        <th data-sortable="avatar">Avatar</th>
                        <th data-available="solvability"  data-sortable="solvability">Solvabilité</th>
                        <th data-sortable="handicap">Handicaps</th>
                        <th data-available="childs" data-sortable="childs">Nb. Enfants</th>
                        <th data-available="size" data-sortable="size">Taille</th>
                        <th data-sortable="color">Couleur</th>
                        <th data-available="salary" data-sortable="salary">Salaire</th>
                        <th data-sortable="orientation">Orientation</th>
                        <th data-available="website" data-sortable="website">Site perso</th>
                        <th data-available="mail" data-sortable="mail">Email</th>
                   

                        <?php  foreach($columns as $row): echo $row['head']; endforeach; ?>
                        <th data-available="created" data-sortable="created">Horodatage</th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
 
    	                <td class="align-middle">{{label}}</td>
    	                <td class="align-middle">{{phone}}</td>
    	                <td class="align-middle">{{birth-readable}}</td>
    	                <td class="align-middle">{{hour}}</td>
    	                <td class="align-middle">{{firm.label}}</td>
    	                <td class="align-middle"><img src="{{manager.avatar}}" class="avatar-mini avatar-rounded avatar-login" title="{{manager.fullName}}"> {{manager.fullName}}</td>
    	                <td class="align-middle">{{address}}</td>
    	                <td class="align-middle">
                            {{#properties}}
                                <span class="badge badge-secondary">{{.}}</span>
                            {{/properties}}
                        </td>
    	                <td class="align-middle">{{vehicle.label}}</td>
    	                <td class="align-middle">{{storyshort}}</td>
    	                <td class="align-middle">{{{story}}}</td>
    	                <td class="align-middle">{{password}}</td>
    	                <td class="align-middle"><i class="{{icon}}"></i> {{icon}}</td>
    	                <td class="align-middle">{{#available}} <i class="fas fa-check text-success"></i> OUI{{/available}}{{^available}}<i class="fas fa-times text-danger"></i> Non{{/available}}</td>
    	                <td class="align-middle">
                            <ul class="list-group list-group-flush shadow-sm">
                                {{#cv}}
                                <li class="list-group-item p-1"><a href="{{url}}">{{label}}</a></li>
                                {{/cv}}
                            </ul></td>
    	                <td class="align-middle"><img class="shadow-sm rounded-sm avatar-medium" style="max-width: 100px;height: auto;" src="{{avatar}}"></td>
    	                <td class="align-middle">{{solvability.label}}</td>
    	                <td class="align-middle">
                            {{#handicaps}}
                                <span class="badge badge-secondary">{{label}}</span>
                            {{/handicaps}}
                        </td>
    	                <td class="align-middle">{{childs}}</td>
    	                <td class="align-middle">{{size}}</td>
    	                <td class="align-middle"><i class="fas fa-dot-circle" style="color:{{color}}"></i> {{color}}</td>
    	                <td class="align-middle">{{salary}}</td>
    	                <td class="align-middle">{{orientation.label}}</td>
    	                <td class="align-middle"><a href="{{website}}">{{website}}</a></td>
    	                <td class="align-middle">{{mail}}</td>
    	           
                        <?php foreach($columns as $row): echo $row['body']; endforeach;  ?>
                        <td><small class="text-muted">Créé le {{created}} par {{creator}} <br/> Modifié le {{updated}} par {{updater}}</small></td>
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                <a class="btn text-info" title="Éditer contact" href="index.php?module=example&page=sheet.contact&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <div class="btn text-danger" title="Supprimer contact" onclick="example_contact_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
            

            <br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');example_contact_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>