//CHARGEMENT DE LA PAGE
function init_plugin_example(){
	switch($.urlParam('page')){
		default:
		break;
	}
	$('#contacts').sortable_table({
		onSort : example_contact_search
	});
}

//Enregistrement des configurations
function example_setting_save(){
	$.action({ 
		action: 'example_setting_save', 
		fields:  $('#example-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}

/** CONTACT **/
//Récuperation d'une liste  contact dans le tableau #contacts
function example_contact_search(callback,exportMode){
	var box = new FilterBox('#example_contact-filters');
	if(exportMode) $('.btn-export').addClass('btn-preloader');

	$('#contacts').fill({
		action:'example_contact_search',
		filters: box.filters(),
		//Gestion du tri par colonnes de tableau (optionnel)
		sort: $('#contacts').sortable_table('get'),
		//Activation de l'export excel (optionnel)
		export:  !exportMode ? false : exportMode,
		//exemple d'affichage de ligne personnalisé (optionnel)
		showing : function(item,i){
			item.css({
				transform:'translateX(-200px)',
				transition:'all 0.2s ease-in-out',
				opacity : 0
			}).removeClass('hidden');
			setTimeout(function(){
				item.css({
					transform:'translateX(0px)',
					opacity : 1
				})
			},(i+1)*5);
		}
	},function(response){
		if(!exportMode) $('.results-count > span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification contact
function example_contact_save(){
	var data = $('#contact-form').toJson();
	$('input[data-type="radio"]:checked', '#contact-form').each(function(){
		data[$(this).attr('name')] = $(this).val();
	});
	$.action(data,function(r){
		$('#contact-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		$.message('success','Enregistré');
	});
}


//Suppression contact
function example_contact_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'example_contact_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}


/* QUICKFORM */
function example_quickform_buttons(){
	$('.quickform-modal .modal-footer').append('<div class="btn btn-success" onclick="contact_save(contact_submit_quickform);"><i class="fas fa-check"></i> Ajouter</div>');
}
// Callback du quickform on save
function contact_submit_quickform(){
	$('#quickform-modal').modal('hide');
}


/* EXPORT MODELE */
function contact_export_pre_callback(){
	console.log('Callback custom après chargement du modal');

	setTimeout(function(){
		$('#export-modal .cb-custom-btn').remove();
		$('#export-modal .modal-footer').prepend('<div class="btn btn-primary mr-auto cb-custom-btn"><i class="fas fa-check"></i> Bouton ajouté avec le callback custom</div>');
	},0);
}

function contact_export_post_callback(){
	alert('Callback custom après export');
}

/* LOCATION CALLBACK */
function example_location_select(location){
	var attributes = {};
	for(var key in location){
	    attributes['data-'+key] = location[key];
	    if($('#'+key)!='') $('#'+key).val(location[key]).text(location[key]);
	}
}

function example_location_geocode(infos){
	alert("Latitude: "+infos.Response.View[0].Result[0].Location.DisplayPosition.Latitude+"\nLongitude: "+infos.Response.View[0].Result[0].Location.DisplayPosition.Longitude);
}


function example_map_click(args){
	console.log('click on map with args',args);
}