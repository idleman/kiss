<?php
global $myUser;
User::check_access('docker','read');
require_once(__DIR__.SLASH.'DockerEnvironment.class.php');
$states = array();
foreach (Dockerenvironment::states() as $key => $state) {
    $states[$key] = $state['label'];
}
?>

<div class="row">
    
    <div class="col-md-8">
        <select id="filters" data-type="filter" data-label="Recherche" data-function="docker_environment_search">
            <option value="label"   data-filter-type="text">Libellé</option>
            <option value="machine" data-filter-type="number">Machine hôte</option>
            <option value="client" data-filter-type="client">Client</option>
            <option value="domain" data-filter-type="text">Nom de domaine</option>
            <option value="port" data-filter-type="number">Port docker</option>
            <option value="git" data-filter-type="text">Dépot git</option>
            <option value="memory" data-filter-type="number">Mémoire max.</option>
            <option value="cpu" data-filter-type="number">Cpu max.</option>
            <option value="ssl" data-filter-type="boolean">SSL</option>
            <option value="state" data-filter-type="list" data-filter-source='<?php echo json_encode($states); ?>' >Etat</option>
                
        </select>
    </div>
    
	<div class="col-md-4">
		<?php if($myUser->can('docker', 'edit')) : ?>
		<a href="index.php?module=docker&page=list.docker.machine" class="btn btn-dark right ml-2"><i class="fas fa-eye"></i> Machines</a>
        <a href="index.php?module=docker&page=sheet.docker.environment" class="btn btn-success right"><i class="fas fa-plus"></i> Environnement</a>
        
		<?php endif; ?>
	</div>
</div>
<br/>
<h4 class="results-count"><span></span> Résultat(s) <div class="btn btn-dark btn-small" onclick="docker_environment_search(null,true)"><i class="far fa-file-excel"></i> Exporter</div></h4>
<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		<table id="docker-environments" class="table table-striped " data-entity-search="docker_environment_search">
            <thead>
                <tr>
                    <th data-sortable="domain">Domaine</th>
                    <th data-sortable="client">Client</th>
                    <th data-sortable="state">Etat</th>
                    <th data-sortable="cpu">Cpu</th>
                    <th data-sortable="memory">Ram</th>
                    <th data-sortable="git">Git</th>
                    <th data-sortable="port">Port interne</th>
                    <th></th>
                </tr>
            </thead>
            
            <tbody>
                <tr data-id="{{id}}" class="hidden">
                    <td>{{domain}}</td>
	                <td><a href="index.php?module=client&page=sheet.client&id={{client.id}}">{{client.label}}</a></td>
	                <td>{{state.label}}</td>
	                <td>{{cpu}}</td>
	                <td>{{memory}} Mo</td>
	                <td>{{git}}</td>
	                <td>{{port}}</td>
	                <td>
	                    <div class="btn-group btn-group-sm right" role="group">
                            
                            <a class="btn btn-info" href="index.php?module=docker&page=sheet.docker.environment&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                            
                            
                            <div class="btn btn-danger " onclick="docker_environment_delete(this);"><i class="far fa-trash-alt"></i></div>
	                    </div>
	                </td>
                </tr>
           </tbody>
        </table>

         <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
       
        <ul class="pagination justify-content-center"  data-range="5">
            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');docker_environment_search();">
                <span class="page-link">{{label}}</span>
            </li>
        </ul>

	</div>
</div>
