<?php
class SophosUtm{
	public $login,$password,$host,$port;
	
	public function getVHost($ref = null){
		$hosts = $this->rest('GET','/objects/reverse_proxy/frontend');
		if(!isset($ref)) return $hosts;
		foreach ($hosts as $host) {
			if($host['_ref']==$ref) return $host;
		}
		return false;
	}

	public function setVHost($ref = 'REF_RevFroDockeWordpMulti',$vhost = array()){
		//On supprime les attributs readonly (débutant par _)
		foreach ($vhost as $key => $value) 
			if(substr($key, 0,1)=='_') unset($vhost[$key]);
	
		$body = json_encode($vhost,JSON_PRETTY_PRINT);
		return $this->rest('PUT','/objects/reverse_proxy/frontend/'.$ref,$body);
	}

	public function getCertificates(){
		return $this->rest('GET','/objects/ca/host_key_cert/');
	}

	public function createLocation($label,$realWebServer){


	 	$attributes = array (
	        "access_control"=> "0",
		    "allowed_networks"=> array(
		      "REF_NetworkAny"
		    ),
		    "auth_profile"=> "",
		    "backend"=> array(
		      $realWebServer
		    ),
		    "be_path"=> "",
		    "comment"=> "",
		    "denied_networks"=> array(),
		    "hot_standby"=> false,
		    "name"=> "/ (".sha1(time().'doc').")",
		    "path"=> "/",
		    "status"=> true,
		    "stickysession_id"=> "ROUTEID",
		    "stickysession_status"=> false,
		    "websocket_passthrough"=> false
  	   );

	
		$body = json_encode($attributes,JSON_PRETTY_PRINT);
	
		return $this->rest('POST','/objects/reverse_proxy/location/',$body);

	}

	public function createVHost($label,$domains=array(),$port=80,$serverRef = '',$certificateRef = ''){

	 $location = $this->createLocation($domains[0],$serverRef);
	
	 if(empty($location['_ref'])) throw new Exception("Impossible de créer le chemin pour ce domaine, peut être existe t-il déjà dans l'utm ?");
	 

	 $attributes = array (
	      'add_content_type_header' => true,
	      'address' =>  'REF_NetIntLiveb21223Addre3',
	      'allowed_networks' => array('REF_NetworkAny'),
	      'blockpagetheme' => '',
	      'certificate' => $certificateRef,
	      'comment' =>  '',
	      'disable_compression' => false,
	      'domain' => $domains,
	      'exceptions' => array(),
	      'htmlrewrite' => false,
	      'htmlrewrite_cookies' => true,
	      'implicitredirect' => true,
	      'lbmethod' => 'bybusyness',
	      'locations' =>  array ($location['_ref']),
	      'name' =>  $label,
	      'port' =>  $port,
	      'preservehost' =>  true,
	      'profile' =>  '',
	      'status' => true,
	      'type' =>  'http'.($port==443?'s':''),
	      'waflocalpath' => '/.waf',
	      'xheaders' =>  false
  	   );


	
		$body = json_encode($attributes,JSON_PRETTY_PRINT);
		return $this->rest('POST','/objects/reverse_proxy/frontend/',$body);
	}

	//Requete rest
	public function rest($method,$action,$body='',$headers=array()){
		$url = $this->host.':'.$this->port.'/api'.$action;
		$ch = curl_init();
		$options[CURLOPT_URL] =  $url;
		$options[CURLOPT_RETURNTRANSFER] =  true;
		$options[CURLOPT_SSL_VERIFYPEER] =  false;
		$options[CURLOPT_FOLLOWLOCATION] = true;
		$options[CURLOPT_SSL_VERIFYPEER] = false;
		$options[CURLOPT_USERAGENT] = 'Awesome erp';
		$options[CURLOPT_USERPWD] =  $this->login . ":" . $this->password;
		$options[CURLOPT_CUSTOMREQUEST] = $method;

		if(!empty($body)) $options[CURLOPT_POSTFIELDS] = $body;
	
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Accept: application/json';

		$options[CURLOPT_HTTPHEADER] = $headers;

		
		curl_setopt_array($ch,$options);

		$response = curl_exec($ch);

		if($response === false) throw new Exception(curl_error($ch));

		curl_close($ch);
		return json_decode($response,true);
	}	
}
?>