<?php 
User::check_access('docker','read');
require_once(__DIR__.SLASH.'DockerMachine.class.php');
$machine = DockerMachine::provide();
if($machine->id==0){
	$machine->ip = '10.172.20.';
	$machine->port = 22;
}
?>
<div class="plugin-docker">
	<div id="machine-form" class="row justify-content-md-center machine-form" data-action="docker_machine_save" data-id="<?php echo $machine->id; ?>">
		<div class="col-md-6 shadow-sm bg-white p-3">
			<h3>Machine 
			<div onclick="docker_machine_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<a href="index.php?module=docker&page=list.docker.machine" class="btn btn-small btn-dark right  mr-2">Retour</a></h3>
			<label for="label">Libellé</label>
			<input  value="<?php echo $machine->label; ?>" class="form-control"  type="text"  id="label" >
			<label for="ip">Ip SSH</label>
			<input  class="form-control" type="text" value="<?php echo $machine->ip; ?>" id="ip">
			<label for="port">Port SSH</label>
			<input  value="<?php echo $machine->port; ?>" class="form-control"  type="number"  id="port" >
			<label for="domain">Nom de domaine</label>
			<input type="text"  class="form-control" type="text" id="domain" value="<?php echo $machine->domain; ?>">
			<label for="sophosReference">Référence sophos</label>
			<input  value="<?php echo $machine->sophosReference; ?>" class="form-control"  type="text"  id="sophosReference" >
			<label for="description">Description</label>
			<textarea  class="" type="text" data-type="wysiwyg" id="description"><?php echo $machine->description; ?></textarea>
			<label for="private_key">Clé privée</label>
			<small class="text-muted">Nom id_rsa uniquement</small>
			<input  value="" class="component-file-default bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-extension="ppk,id_rsa"  data-action="docker_machine_private_key"  data-id="private_key"  data-data='{"id":"<?php echo $machine->id; ?>"}'  id="private_key" >
			<label for="public_key">Clé publique</label>
			<small class="text-muted">Extension .pub uniquement</small>
			<input  value="" class="component-file-default bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-extension="ppk,pub,id_rsa"  data-action="docker_machine_public_key"  data-id="public_key"  data-data='{"id":"<?php echo $machine->id; ?>"}'  id="public_key" >
			<br/>
			
		</div>
	</div>
</div>

