
		
			  
			  <!-- NDD -->
			  <div  id="domain-tab" >
			  

			  	
					<label for="domain">Domaine (sans http/https)</label>
					<input id="domain" name="domain" class="form-control" placeholder="" value="<?php echo $environment->domain; ?>" type="text" required>
				

			<div class="row">
				<div class="col-md-6">

					<label for="ssl"><br>
					<input id="ssl" name="ssl" data-type="checkbox" onclick="docker_environment_change_ssl()" placeholder="" type="checkbox" <?php echo $environment->ssl?'checked="checked"':''; ?>>Certificat SSL ?</label>
					</div>
						<div class="col-md-6">
							<div id="certificate-block" class="hidden">
								<label for="certificate">Certificat</label>
								<select id="certificate" name="certificate" class="form-control" placeholder="" data-value="<?php echo $environment->certificate; ?>"></select>
							</div>
						</div>
					</div>


			  	<?php if(!empty($environment->port)): 
					$externalUrl = 'http://'.$environment->domain;
				?>

				<br>Url externe : <a href="<?php echo $externalUrl; ?>"><?php echo $externalUrl; ?></a>
			
				<?php endif; ?>
				<hr>
					<a class="btn btn-dark btn-domain-create  my-2" onclick="docker_environment_domain_create(event);" href="#"><i class="fas fa-plus"></i> Relier le DNS OVH</a>
			  </div>
			