<?php
class Ovh{
    public $applicationKey,$applicationSecret,$consumerKey,$host = 'https://eu.api.ovh.com';
    
  

    public function consumerKey(){
      
        $body = '{
    "accessRules": [
        {
            "method": "GET",
            "path": "/*"
        },
        {
            "method": "POST",
            "path": "/*"
        }
    ],
    "redirection":"https://www.idleman.fr/"
}';
        $headers = array();
       
        $headers['Accept'] = 'application/json';

        return $this->rest('POST','/1.0/auth/credential',$body,$headers);
    }

    public function domain(){
       return $this->rest('GET','/1.0/domain/zone/idleman.fr/record/5136453749','');
    }

    public function getDomain($zone = ''){
       if(!empty($zone)) $zone = '/'.$zone;
       return $this->rest('GET','/1.0/domain/zone'.$zone,'');
    }

     public function setDomain($zone,$subdomain,$target){
         
         $body = array(
            'fieldType' =>  'CNAME',
            'subDomain' =>  $subdomain,
            'target' =>  'idleman.fr.',
            'ttl' =>  '0'
         );
        

        $response =  $this->rest('POST','/1.0/domain/zone/idleman.fr/record',json_encode($body));
        $this->rest('POST','/1.0/domain/zone/idleman.fr/refresh');
       return $response;
    }

    //Requete rest
    public function rest($method,$action,$body='',$headers = null){
        $url = $this->host.$action;
        $ch = curl_init();
        
        $options[CURLOPT_URL] =  $url;
        $options[CURLOPT_RETURNTRANSFER] =  true;
        $options[CURLOPT_SSL_VERIFYPEER] =  false;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_USERAGENT] = 'Awesome erp';
        $options[CURLOPT_HEADER] = true;
        $options[CURLOPT_CUSTOMREQUEST] = $method;

        if(!empty($body)) $options[CURLOPT_POSTFIELDS] = $body;

        if(empty($headers)) $headers = array();
        
        $options[CURLOPT_HTTPHEADER] = $headers;

        curl_setopt_array($ch,$options);

        $headers['Content-type'] = 'application/json; charset=utf-8';
        $headers['X-Ovh-Application']=$this->applicationKey;

        if($action!='/1.0/auth/credential'){ 
            $time = file_get_contents('https://eu.api.ovh.com/1.0/auth/time');
            $signature = $this->applicationSecret."+".$this->consumerKey."+".$method."+".$url."+".$body."+".$time;
            $signature = "$1$" . sha1($signature);
            $headers['X-Ovh-Signature'] = $signature;
            $headers['X-Ovh-Consumer'] = $this->consumerKey;
            $headers['X-Ovh-Timestamp'] = $time;
        }
     
        $headers['cache-control'] = 'no-cache';
        
        $cmd = "curl ".$url." ";
        foreach ($headers as $key => $value) {
            $cmd .= " -H '".$key.": ".$value."' ";
        }

      
       
        if(!empty($body)) $cmd .= " -d '".$body."' ";
    
        $response = exec($cmd);
  
        if($response === false) throw new Exception(curl_error($ch));
     
        curl_close($ch);
        return json_decode($response,true);
    }   
}
?>