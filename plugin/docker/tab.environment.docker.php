<?php



?>
			  <!-- DOCKER -->
			  <div class="" id="docker-tab" >

			  	<label for="memory">Mémoire (RAM en Mo)</label>
				<input id="memory" name="memory" class="form-control" placeholder="" value="<?php echo $environment->memory; ?>" type="text">
			
				<label for="port">Port docker</label>
				<input  value="<?php echo $environment->port; ?>" class="form-control"  type="number"  id="port" >


				<div class="row">
					<div class="col-md-6">
						<label for="mysqlRootPassword">Pass. Mysql Root</label>
						<small class="text-muted"> - Identifiant : root</small>
						<input  value="<?php echo decrypt($environment->mysqlRootPassword); ?>" <?php echo $environment->id!=0?'readonly="readonly"':''; ?> class="form-control"  type="text"  data-type="password"  data-generator   autocomplete="new-password"  id="mysqlRootPassword" >
					</div>
					<div class="col-md-6">
						<label for="mysqlPassword">Pass. Mysql App</label>
						<small class="text-muted"> - Identifiant : <?php echo $environment->domain; ?></small>
						<input  value="<?php echo decrypt($environment->mysqlPassword); ?>" <?php echo $environment->id!=0?'readonly="readonly"':''; ?> class="form-control"  type="text"  data-type="password"  data-generator   autocomplete="new-password"  id="mysqlPassword" >
					</div>
				</div>


				
				


				<hr/>
				<a class="btn btn-dark btn-docker-create  my-2" onclick="docker_environment_docker_create(event);" href="#"><i class="fas fa-plus"></i> Créer l'hébergement</a>
			  	<a class="btn btn-danger btn-docker-remove  my-2 mx-2" onclick="docker_environment_docker_remove(event);" href="#"><i class="fas fa-times"></i> Detruire l'hébergement</a>
			  	<?php if(!empty($environment->port)): 
					$internalUrl = 'http://'.$machine->ip.':'.$environment->port;
				?>
				<br>Url interne (si créé) : <a href="<?php echo $internalUrl; ?>"><?php echo $internalUrl; ?></a>
				<?php endif; ?>
			  	<hr/>
			  	<a class="btn btn-primary btn-pma-create   my-2 " onclick="docker_environment_pma_create(event);" href="#"><i class="fas fa-plus"></i> Ouvrir un Accès PHPMyAdmin</a>
			  	<a class="btn btn-warning btn-pma-remove   my-2 " onclick="docker_environment_pma_remove(event);" href="#"><i class="fas fa-times"></i> Fermer l'Accès PHPMyAdmin</a>
			  	<br>
				Url PMA (si créé) : <a href="http://10.172.20.109:8011">http://10.172.20.109:8011</a>
			  

			  </div>
