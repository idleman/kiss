<?php
global $myUser;
User::check_access('docker','read');
require_once(__DIR__.SLASH.'DockerMachine.class.php');




?>
<div class="plugin-docker">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des machines</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="docker_machine_search(null,true);" id="export-dockers-btn" class="btn btn-info rounded-0 btn-squarred ml-1" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                
                
                <div class="my-auto ml-auto mr-0 noPrint">
                    <?php if($myUser->can('docker', 'edit')) : ?>
                    <a href="index.php?module=docker&page=sheet.docker.machine" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                    <?php endif; ?>
                </div>
                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
            <select id="filters" data-type="filter" data-label="Recherche" data-function="docker_machine_search">
                <option value="label" data-filter-type="text">Libellé</option>
                <option value="ip" data-filter-type="text">Ip SSH</option>
                <option value="port" data-filter-type="number">Port SSH</option>
                <option value="domain" data-filter-type="text">Nom de domaine</option>
                <option value="sophosReference" data-filter-type="text">Référence sophos</option>
                <option value="description" data-filter-type="text">Description</option>
            </select>
        </div>
        
    </div>
    <h5 class="results-count"><span></span> Résultat(s)</h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">
    		
            <table id="machines" class="table table-striped " data-entity-search="docker_machine_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->
                        <th data-sortable="label">Libellé</th>
                        <th data-sortable="ip">Ip SSH</th>
                        <th data-sortable="public_key">Clé publique</th>
                        <th data-sortable="domain">Nom de domaine</th>
                        <th data-sortable="sophosReference">Référence sophos</th>
                       
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	                <!--<td class="align-middle">{{id}}</td>-->
    	                <td class="align-middle">{{label}} <small class="text-muted">{{{description}}}</small></td>
    	                <td class="align-middle">{{ip}} (Port {{port}})</td>
    	               
    	            
    	                <td class="align-middle">
                            <textarea class="machine-public-key" onclick="$(this).select()">{{public_key}}</textarea>  
                        </td>
    	                <td class="align-middle">{{domain}}</td>
    	                <td class="align-middle">{{sophosReference}}</td>
    	              
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                <a class="btn text-info" title="Éditer machine" href="index.php?module=docker&page=sheet.docker.machine&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <div class="btn text-danger" title="Supprimer machine" onclick="docker_machine_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table><br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');docker_machine_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>