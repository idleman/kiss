<?php
global $myUser,$conf;
User::check_access('docker','configure');
?>

<div class="row">
	<div class="col-md-12">
          <h3><i class="fas fa-wrench"></i> Réglages Docker
                <?php if($myUser->can('docker', 'edit')) : ?>
        <div onclick="docker_setting_save();" class="btn btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
        <?php endif; ?>
          </h3>
        <hr/>
	</div>
</div>
<p>Veuillez remplir les informations ci dessous.</p>

<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		  <?php echo Configuration::html('docker'); ?>
	</div>
</div>

<p>
	Pour récuperer l'app key et l'app secret OVH, cliquez ici : <a href="https://eu.api.ovh.com/createApp/">https://eu.api.ovh.com/createApp/</a>
	Puis remplissez les settings avec les infos app key et app secret, enregistrez et lancez l'action suivante : <a href="<?php echo ROOT_URL ?>/action.php?action=docker_ovh_consumerKey_generate"><?php echo ROOT_URL ?>/action.php?action=docker_ovh_consumerKey_generate</a>, sur l'url unique qui s'affiche, se logguer avec les identifiants OVH et saisir "unlinmited" puis cliquer sur ok
</p>
