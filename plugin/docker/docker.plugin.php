<?php


//Déclaration d'un item de menu dans le menu principal
function docker_menu(&$menuItems){
	global $_,$myUser;
	if(!$myUser->can('docker','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=docker',
		'label'=>'Docker',
		'icon'=> 'fab fa-docker',
		'color'=> '#3498db'
	);
}

//Cette fonction va generer une page quand on clique sur docker dans menu
function docker_page(){
	global $_,$myUser;
	if(!isset($_['module']) || $_['module'] !='docker') return;
	$page = !isset($_['page']) ? 'list.docker.environment' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function docker_install($id){
	if($id != 'fr.core.docker') return;
	Entity::install(__DIR__);
	global $conf;
	
	$conf->get('docker_ovh_host','https://eu.api.ovh.com');
	$conf->get('docker_git_host','http://git.idleman.fr/api/v4');
	$conf->get('docker_sophos_host','https://firewall.idleman.fr');
	$conf->get('docker_sophos_port','4444');


}

//Fonction executée lors de la désactivation du plugin
function docker_uninstall($id){
	if($id != 'fr.core.docker') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("docker",array('label'=>"Gestion des droits sur le plugin docker"));

//cette fonction comprends toutes les actions du plugin qui ne nécessitent pas de vue html
function docker_action(){
	require_once(__DIR__.SLASH.'action.php');
}

//Déclaration du menu de réglages
function docker_menu_setting(&$settingMenu){
	global $_, $myUser;
	
	if(!$myUser->can('docker','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.docker',
		'icon' => 'fas fa-angle-right',
		'label' => 'Docker'
	);
}

//Déclaration des pages de réglages
function docker_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('docker',array(
        "Utm Sophos",
        'docker_sophos_host' => array("label"=>"Hôte","type"=>"string"),
        'docker_sophos_port' => array("label"=>"Port","type"=>"number"),
        'docker_sophos_login' => array("label"=>"Identifiant","type"=>"string"),
        'docker_sophos_password' => array("label"=>"Mot de passe","type"=>"string"),
        //'docker_sophos_vhost_ref' => array("label"=>"Référence du real webserver vers lequel pointer","placeholder"=>'ex : REF_RevBacLindockerw',"type"=>"string"), //REF_RevFroDockeWordpMulti
        "Ovh",
        'docker_ovh_host' => array("label"=>"Endpoint ovh",'placeholder'=>'ex : https://eu.api.ovh.com',"type"=>"string"),
        'docker_ovh_application_key' => array("label"=>"Clé publique d'application","type"=>"string"),
        'docker_ovh_application_secret' => array("label"=>"Clé privée d'application","type"=>"password"),
        'docker_ovh_consumer_key' => array("label"=>"Clé consommateur","type"=>"password"),
        "GitLab",
        'docker_git_host' => array("label"=>"Hote gitlab","placeholder"=>'ex: http://git.idleman.fr/api/v4',"type"=>"string"),
        'docker_git_token' => array("label"=>"Clé privée d'application","type"=>"password"),
));


 require_once(__DIR__.SLASH.'action.php');


//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "docker_install");
Plugin::addHook("uninstall", "docker_uninstall"); 


Plugin::addHook("menu_main", "docker_menu"); 
Plugin::addHook("page", "docker_page");  
Plugin::addHook("action", "docker_action");  
Plugin::addHook("menu_setting", "docker_menu_setting");    
Plugin::addHook("content_setting", "docker_content_setting");   
    

?>