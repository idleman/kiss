//CHARGEMENT DE LA PAGE
function init_plugin_docker(){
	switch($.urlParam('page')){
		case 'sheet.docker.environment':
			docker_environment_change_ssl();

			$('#docker-environment-tab li a').click(function(){
		    
		        var tab = $(this).attr("href").substring(1);
		        $("#docker-environment-tab-content").load("action.php?action=docker_environment_tab&id="+$.urlParam('id')+"&tab="+tab,function(){
		        	init_components("#docker-environment-tab-content");
		        });
		    });
		    if($.urlParam('id')){
		    	$('.docker .environment-detail').removeClass('hidden');
		    	$('#docker-environment-tab li:eq(0) a').click();
		    }
		break;
		default:
		break;
	}
	
	
}


//Enregistrement des configurations
function docker_setting_save(){
	$.action({ 
		action : 'docker_setting_save', 
		fields :  $('#docker-setting-form').toJson() 
	},function(){ $.message('info','Configuration enregistrée'); });
}


/** DOCKERENVIRONMENT **/
	
//Récuperation d'une liste de dockerenvironment dans le tableau #dockerenvironments
function docker_environment_search(callback,exportMode){
	
	var box = new FilterBox('#filters');	
	$('#docker-environments').fill({
		action:'docker_environment_search',
		filters : box.filters(),
		sort : $('#docker-environments').sortable_table('get'),
		export:  !exportMode ? false : exportMode
	},function(response){
		$('.results-count span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification d'élément dockerenvironment
function docker_environment_save(){
	var data = $('#docker-environment-form').toJson();
	$.action(data,function(r){
		$('#docker-environment-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		$.message('success','Enregistré');
		$('.docker .environment-detail').removeClass('hidden');
	});
}

function docker_environment_docker_create(event){
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_docker_create';
	$('.btn-docker-create').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		$.message('success','Créé');
	});
}

function docker_environment_docker_remove(event){
	if(!confirm('Êtes vous sûr de vouloir supprimer ce conteneur ? Toutes les données seront supprimées')) return;
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_docker_remove';
	$('.btn-docker-remove').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		$.message('success','Supprimé');
	});
}

function docker_environment_pma_create(event){
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_pma_create';
	$('.btn-pma-create').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		$.message('success','Créé');
	});
}

function docker_environment_pma_remove(event){
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_pma_remove';
	$('.btn-pma-remove').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		$.message('success','Créé');
	});
}

function docker_environment_utm_create(event){
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_utm_create';
	$('.btn-utm-create').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		$.message('success','Créé');
	});
}
function docker_environment_domain_create(event){
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_domain_create';
	$('.btn-domain-create').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		$.message('success','Créé');
	});
}
function docker_environment_git_create(event){
	event.stopPropagation();
	var data = $('#docker-environment-form').toJson();
	data.action='docker_environment_git_create';
	$('.btn-git-create').addClass('btn-preloader');
	$.action(data,function(r){
		$('#logs').html(r.logs);
		document.location.reload(true);
		$.message('success','Créé');
	});
}

function docker_environment_migrate_path(){
	var tpl = $('template.migrationPath').html();
	var data = {};

	data.login = $('#login').val();
	data.password = $('#password').val();
	data.ip = $('#ip').val();
	data.root = $('#root').val();

	data.relative_destination = data.root.split('/');
	data.relative_destination = data.relative_destination.splice(-1,1);
	data.relative_destination = data.relative_destination[0];

	var chain = Mustache.render(tpl,data);
	$('code.migrationPath').html(chain);
}

function docker_environment_change_ssl(){
	var ssl = $('#ssl').prop('checked');
	if(!ssl){
		$('#certificate-block').addClass('hidden');
		return;
	}
	$('#certificate-block').removeClass('hidden');
	$('#certificate').html('<option>Chargement...</option>');
	$.action({
		action : 'docker_environment_certificate_search',
	},function(r){
		$('#certificate').html('');
		var preset = $('#certificate').attr('data-value');
		for(var k in r.rows){
			$('#certificate').append('<option value="'+r.rows[k].ref+'" '+(preset==r.rows[k].ref?' selected="selected" ':'')+'>'+r.rows[k].label+'</option>');
		}

	});
}

//Suppression d'élement dockerenvironment
function docker_environment_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('tr');
	$.action({
		action : 'docker_environment_delete',
		id : line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Élement supprimé');
	});
}

/** MACHINE **/
//Récuperation d'une liste  machine dans le tableau #machines
function docker_machine_search(callback,exportMode){
	var box = new FilterBox('#filters');
	$('#machines').fill({
		action:'docker_machine_search',
		filters: box.filters(),
		sort: $('#machines').sortable_table('get'),
		export:  !exportMode ? false : exportMode
	},function(response){
		$('.results-count span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification machine
function docker_machine_save(){
	var data = $('#machine-form').toJson();
	$.action(data,function(r){
		$('#machine-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		$.message('success','Enregistré');
	});
}


//Suppression machine
function docker_machine_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'docker_machine_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}