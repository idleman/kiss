<?php
/**
 * Define a Machine
 * @author Administrateur PRINCIPAL
 * @category Plugin
 * @license MIT
 */
class DockerMachine extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $ip; //Ip SSH (Texte Long)
	public $port; //Port SSH (Nombre Entier)
	public $private_key; //Clé privée (Fichier)
	public $public_key; //Clé publique (Fichier)
	public $domain; //Nom de domaine (Texte Long)
	public $password; //Mot de passe (Password)
	public $sophosReference; //Référence sophos (Texte)
	public $description; //Description (Texte enrichis)
	
	protected $TABLE_NAME = 'docker_machine';
	public $entityLabel = 'Machine';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text', 'label' => 'Libellé'),
		'ip' => array('type'=>'textarea', 'label' => 'Ip SSH'),
		'port' => array('type'=>'integer', 'label' => 'Port SSH'),
		'domain' => array('type'=>'textarea', 'label' => 'Nom de domaine'),
		'sophosReference' => array('type'=>'text', 'label' => 'Référence sophos'),
		'password' => array('type'=>'password', 'label' => 'Mot de passe de la clé'),
		'description' => array('type'=>'wysiwyg', 'label' => 'Description')
	);

	
	//Colonnes indexées
	public $indexes = array();
	
}
?>