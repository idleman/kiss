<?php 
global $conf;
User::check_access('docker','read');
require_once(__DIR__.SLASH.'DockerEnvironment.class.php');
require_once(__DIR__.SLASH.'DockerMachine.class.php');


$environment = DockerEnvironment::provide();
$machine = new DockerMachine();
if($environment->id == 0){
	$environment->memory = '256';
	$environment->cpu = '0.5';
	$environment->port = DockerEnvironment::lastPort()+2;
}else{
	$machine = DockerMachine::getById($environment->machine);
}


?>
<div class="docker">
	<div id="docker-environment-form" class="row docker-environment-form justify-content-md-center" data-action="docker_environment_save" data-id="<?php echo $environment->id; ?>">
		<div class="col-md-5 shadow-sm bg-white p-3">
			<h3>Docker Environment <a class="btn px-1" data-scope="docker" data-uid="<?php echo $environment->id; ?>" data-show-important="true" data-type="history"><i class="far fa-comment-dots"></i></a>

			<div onclick="docker_environment_save();" class="btn btn-success right ml-2"><i class="fas fa-check"></i> Enregistrer</div>
			<a class="btn btn-dark right" href="index.php?module=docker">Retour</a>
			</h3>
			<label for="machine">Machine</label>
			<select id="machine" name="machine" class="form-control" placeholder="" value="<?php echo $environment->machine; ?>" required>
				<?php foreach(DockerMachine::loadAll() as $machine): ?>
					<option <?php echo $machine->id==$environment->machine?'selected="selected"':'' ?> value="<?php echo $machine->id; ?>"><?php echo $machine->label; ?></option>
				<?php endforeach; ?>
			</select>
		
			
				
				
			<label for="client">Client</label>
			<input id="client" name="client" data-type="client" class="form-control" placeholder="" type="text" value="<?php echo $environment->client; ?>">

			<label for="comment">Commentaire</label>
			<textarea id="comment" data-type="wysiwyg"><?php echo $environment->comment; ?></textarea>


			


		</div>
		<div class="col-md-5 shadow-sm bg-white p-3 ml-2 environment-detail hidden">
			<ul class="nav nav-tabs" id="docker-environment-tab" role="tablist">
			
			  <li class="nav-item" role="presentation">
			    <a class="nav-link" data-toggle="tab" href="#docker" role="tab"  aria-selected="false"><i class="fab fa-docker"></i> Environnement</a>
			  </li>
			  <li class="nav-item" role="presentation">
			    <a class="nav-link" data-toggle="tab" href="#domain" role="tab" aria-selected="false"><i class="fas fa-globe"></i> Nom de domaine</a>
			  </li>
			  <li class="nav-item" role="presentation">
			    <a class="nav-link" data-toggle="tab" href="#proxy" role="tab"  aria-selected="false"><i class="fas fa-shield-alt"></i> Reverse Proxy</a>
			  </li>
			  <li class="nav-item" role="presentation">
			    <a class="nav-link" data-toggle="tab" href="#git" role="tab" aria-selected="false"><i class="fab fa-git"></i> Git</a>
			  </li>
			  <li class="nav-item" role="presentation">
			    <a class="nav-link" data-toggle="tab" href="#migration" role="tab"  aria-selected="false"><i class="fas fa-dove"></i> Migration</a>
			  </li>
			</ul>
			<div class="tab-content" id="docker-environment-tab-content"></div>

		

			<hr>
			<div id="logs"></div>

		</div>
	</div>
</div>

