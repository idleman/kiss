
		
			  
			  
			  <div  id="migration-tab" >
			  
			  	<label for="login">Login SSH de la vm d'origine</label>
				<input id="login" onkeyup="docker_environment_migrate_path()" name="login" class="form-control" placeholder="" value="" type="text" required>
			  	
			  	<label for="password">Mot de passe SSH de la vm d'origine</label>
				<input id="password" onkeyup="docker_environment_migrate_path()" name="password" class="form-control" placeholder="" value="" type="text" required>
			  	

				<label for="ip">Ip de la VM d'origine (sans http/https)</label>
				<input id="ip" onkeyup="docker_environment_migrate_path()" name="ip" class="form-control" placeholder="" value="10.172.20..." type="text" required>
			
				<label for="root">Chemin vers la racine de wordpress</label>
				<input id="root" onkeyup="docker_environment_migrate_path()" name="root" class="form-control" placeholder="" value="/var/www/html/domain.tld" type="text" required>

				<p>passer la commande : </p>
				<template class="migrationPath">
					Penser à ajouter le user au groupe www-data sur la vm d'ortigine ex : usermod -a -G www-data {{login}}
					<br>
					rsync -aurvz --progress --delete -e 'ssh -p 22' {{login}}@{{ip}}:{{root}} /home/clients/<?php echo $environment->domain; ?><br>
					{{password}}<br>
					mv /home/clients/<?php echo $environment->domain; ?>/wordpress/ /home/clients/<?php echo $environment->domain; ?>/_wordpress<br>
					mv /home/clients/<?php echo $environment->domain; ?>/{{relative_destination}} /home/clients/<?php echo $environment->domain; ?>/wordpress<br>
					chmod 755 -R /home/clients/<?php echo $environment->domain; ?>/wordpress && chown -R www-data:www-data /home/clients/<?php echo $environment->domain; ?>/wordpress
					<br>
					Import de la base sql
					<br>
					Modifier le fichier /home/clients/<?php echo $environment->domain; ?>/wordpress/wp-config.php avec les infos de /home/clients/<?php echo $environment->domain; ?>/_wordpress/wp-config.php<br>
					<strong>Important : relancer le conteneur wordpress sinon le dossier volume ne sera plus mappé</strong>

					Changer le host
					<strong>Important2 : si la page est blanche, aller sur /wp-admin et autoriser la migration de db</strong>
					<strong>Important3 : Penser a installer le plugin wp-mail-smtp réglé comme suivant :<br>
					 Host: 10.172.20.2 <br>
					 Cryptage: Aucun <br>
					 Port: 25 <br>
					 Authentification: Aucun <br>
					 Supprimer le suser kiss si plus besoin
				</strong>
					</template>
				<code class="migrationPath"></code>


			
			  </div>
			