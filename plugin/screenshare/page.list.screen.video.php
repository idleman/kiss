<?php
global $myUser;
User::check_access('screenshare','read');
require_once(__DIR__.SLASH.'ScreenVideo.class.php');
?> 

<h3>Capture d'écran</h3>

<div class="row screenshare">
	<!-- search results -->


     
    <div class="col-xl-6 screenshare-preview-block">
          <video id="screenshare-preview" poster="plugin/screenshare/img/poster.jpg" controls class="screenshare-preview" autoplay></video>
        <?php if($myUser->can('screenshare', 'edit')) : ?>
        <div class="screenshare-button-bar ">

            <!--<div onclick="$.message('warning','désolé, pas encore implémenté...')" class="btn"><i class="fas fa-crop-alt text-muted"></i></div>-->
            <div onclick="screenshare_screen_mouse_toggle()" title="Dés/Activer l'affichage souris" class="btn mouse-button" data-mode="active"><i class="fas fa-mouse-pointer text-muted"></i></div>

            <div onclick="screenshare_screen_video_record();" class="btn record-button"><i class="text-danger fas fa-circle"></i></div>

            <div onclick="screenshare_screen_sound_toggle()" title="Dés/Activer le son" class="btn sound-button" data-mode="inactive"><i class="fas fa-volume-up text-muted"></i></div>
        </div>
        <?php endif; ?>
    </div>
	<div class="col-xl-6">

        <select id="filters" data-type="filter" data-label="Recherche" data-function="screenshare_screen_video_search">
            <option value="label"   data-filter-type="text">Libellé</option>
        </select>

        <h4 class="results-count"><span></span> Capture(s)</h4>
		<ul id="screen-videos" class="screen-videos"  data-entity-search="screenshare_screen_video_search">
            
            
            
                <li data-id="{{id}}"   class="hidden">
	                <span class="text-muted"><i class="fas fa-hashtag"></i>{{id}} </span>
	          
                    <input type="text" class="mr-2 mb-2 label" onblur="screenshare_screen_video_save(this)"  value="{{label}}"> 

                     <div class="btn-group btn-group-sm right text-muted" role="group">
                            <a class="mx-2 pointer"  onclick="copy_string('{{url}}');$.message('info','Lien copié')" title="lien"><i class="fas fa-link"></i></a>
                            <a class="mx-2 pointer text-muted"  href="action.php?action=screenshare_screen_video_preview&download&slug={{slug}}" title="Télécharger"><i class="fas fa-file-download"></i></a>
                            <div class="mx-2 pointer" title="Prévisualiser" onclick="screenshare_screen_video_preview(this);"><i class="far fa-play-circle"></i></div>
                            <div class="mx-2 pointer" title="Supprimer" onclick="screenshare_screen_video_delete(this);"><i class="fas fa-trash"></i></div>
                        </div>
                    <br>
                    <small class="text-muted">
                     Expire le <input type="text" class="expireDate text-muted" onblur="screenshare_screen_video_save(this)" data-type="date"  value="{{expireDate}}">
                     à <input type="text" class="expireHour text-muted" onblur="screenshare_screen_video_save(this)" data-type="hour"  value="{{expireHour}}">
	               </small>
	                   
	                
                </li>
         
        </ul>

         <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
       
        <ul class="pagination justify-content-center"  data-range="5">
            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');screenshare_screen_video_search();">
                <span class="page-link">{{label}}</span>
            </li>
        </ul>

	</div>
</div>

<div id="screenshare-progress" class="screenshare-progress hidden">
    <div class="progress">
        <div class="progress-bar bg-dark" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <i class="text-danger fas fa-circle"></i> Enregistrement de la vidéo en cours <span></span> ...
</div>
