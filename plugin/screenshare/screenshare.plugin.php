<?php

 
//Déclaration d'un item de menu dans le menu principal
function screenshare_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('screenshare','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=screenshare',
		'label'=>'Screen Share',
		'icon'=> 'fas fa-camera',
		'color'=> '#3498db'
	);
}

//Cette fonction va generer une page quand on clique sur screenshare dans menu
function screenshare_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='screenshare') return;
	$page = !isset($_['page']) ? 'list.screen.video' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function screenshare_install($id){
	if($id != 'fr.core.screenshare') return;
	Entity::install(__DIR__);
	global $conf;
	$conf->put('screenshare_default_expire',48);
}

//Fonction executée lors de la désactivation du plugin
function screenshare_uninstall($id){
	if($id != 'fr.core.screenshare') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("screenshare",array('label'=>"Gestion des droits sur le plugin screenshare"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html

require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function screenshare_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('screenshare','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.screenshare',
		'icon' => 'fas fa-angle-right',
		'label' => 'Screenshare'
	);
}

//Déclaration des pages de réglages
function screenshare_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('screenshare',array(
        "Général",
        'screenshare_default_expire' => array("label"=>"Nombre d'heures par défaut avant expiration ","type"=>"number"),

));


function screenshare_wiki_page_pre_html(&$rawText){
	 $rawText = preg_replace_callback('|(https?://.*index\.php)\?(module=screenshare&page=screen\.play&slug=)([a-z0-9]*)|i',function($m){
	return '{{screenshare:'.$m[3].'}}';
        },$rawText);
}
function screenshare_wiki_page_post_html(&$html){
	    $html = preg_replace_callback('|{{screenshare:(.*)}}|i',function($m){
            return '<video id="screenshare-preview" src="action.php?action=screenshare_screen_video_preview&slug='.$m[1].'" loop controls class="screenshare-preview" muted autoplay></video>';
        }, $html );
}

    

//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "screenshare_install");
Plugin::addHook("uninstall", "screenshare_uninstall"); 


Plugin::addHook("wiki_page_pre_html", "screenshare_wiki_page_pre_html"); 
Plugin::addHook("wiki_page_post_html", "screenshare_wiki_page_post_html"); 

Plugin::addHook("menu_main", "screenshare_menu"); 
Plugin::addHook("page", "screenshare_page");  
Plugin::addHook("menu_setting", "screenshare_menu_setting");    
Plugin::addHook("content_setting", "screenshare_content_setting");   
    

?>