<?php

	/** SCREENVIDEO **/
	//Récuperation d'une liste de screenvideo
	Action::register('screenshare_screen_video_search',function(&$response){
		global $myUser,$_;
		User::check_access('screenshare','read');
		require_once(__DIR__.SLASH.'ScreenVideo.class.php');
		
		// OPTIONS DE RECHERCHE, A ACTIVER POUR UNE RECHERCHE AVANCEE
		$query = 'SELECT * FROM '.ScreenVideo::tableName().' WHERE 1';
		$data = array();
		//Recherche simple
		if(!empty($_['filters']['keyword'])){
			$query .= ' AND label LIKE ?';
			$data[] = '%'.$_['filters']['keyword'].'%';
		}

		$query .= ' AND creator = ?';
		$data[] = $myUser->login;

		//Recherche avancée
		if(isset($_['filters']['advanced'])) filter_secure_query($_['filters']['advanced'],array('label'),$query,$data);

		//Tri des colonnes
		$query.=' ORDER BY created ';

		//Pagination
		$response['pagination'] = ScreenVideo::paginate(20,(!empty($_['page'])?$_['page']:0),$query,$data);

		$screenvideos = ScreenVideo::staticQuery($query,$data,true,0);
		
		foreach($screenvideos as $screenvideo){
			$row = $screenvideo->toArray();
			if($row['expire']==0){
				unset($row['expire']);
			}else{
				$row['expireDate'] = date('d/m/Y',$row['expire']);
				$row['expireHour'] = date('H:i',$row['expire']);
			}
			$row['url'] = ROOT_URL.'/index.php?module=screenshare&page=screen.play&slug='.$row['slug'];

			$response['rows'][] = $row;
		}
	});
	
	Action::register('screenshare_screen_video_preview',function(&$response){
		global $myUser,$_;
		require_once(__DIR__.SLASH.'ScreenVideo.class.php');
		if(isset($_['id'])){
			User::check_access('screenshare','read');
			$video = ScreenVideo::getById($_['id']);
			if($video->creator!=$myUser->login) throw new Exception("Vous n'avez pas la permission");
			
		}else if(isset($_['slug'])){
			$video = ScreenVideo::load(array('slug'=>$_['slug']));
		}
		File::downloadFile($video->file(),null,null,isset($_['download']));
	});

	//Ajout ou modification d'élément screenvideo
	Action::register('screenshare_screen_video_save',function(&$response){
	
	   global $myUser,$_,$conf;
		User::check_access('screenshare','edit');
		require_once(__DIR__.SLASH.'ScreenVideo.class.php');
	   $item = ScreenVideo::provide();
	   $defaultExpire = $conf->get('screenshare_default_expire') == '' ? 24: $conf->get('screenshare_default_expire');
	   if($item->id==0){
	   	$item->slug = sha1(md5('9523145'.mt_rand(0,1000).'dev'.time()));
	   	$item->label = 'Capture du '.date('d-m-Y H:i:s');
	   	$item->expire = time() + (3600*$defaultExpire) ;
	   }
	   if(isset($_['label']))$item->label = $_['label'];
	   $item->expire =  0;

	   if(!empty($_['expireDate'])){
	   	$date = explode('/',$_['expireDate']);
	   	$hour = !empty($_['expireHour']) ? explode(':',$_['expireHour']) : array(0,0);
	   	$time = mktime($hour[0],$hour[1],0,$date[1],$date[0],$date[2]);
	   	$item->expire = $time;
	   }
	   
	   if(isset($_['comment'])) $item->comment = $_['comment'];
	   $item->save();

	   if(isset($_FILES['data'])) move_uploaded_file($_FILES['data']['tmp_name'],  $item->file());
	   
	   $response['item'] = $item->toArray();
	
	});

	//Suppression d'élement screenvideo
	Action::register('screenshare_screen_video_delete',function(&$response){
	
		global $myUser,$_;
		User::check_access('screenshare','delete');
		require_once(__DIR__.SLASH.'ScreenVideo.class.php');
		$capture = ScreenVideo::getById($_['id']);
		if($myUser->login!=$capture->creator && !$myUser->superAdmin) throw new Exception("Vous devez être le créateur de cette capture pour la supprimer");
		
		$capture->remove();
	});

	
	//Sauvegarde des configurations de screenshare
	Action::register('screenshare_setting_save',function(&$response){
		global $myUser,$_,$conf;
		User::check_access('screenshare','configure');

		//Si input file "multiple", possibilité de normlaiser le
		//tableau $_FILES récupéré avec la fonction => normalize_php_files();
		
		foreach(Configuration::setting('screenshare') as $key=>$value){
			if(!is_array($value)) continue;
			$allowed[] = $key;
		}
		foreach ($_['fields'] as $key => $value) {
			if(in_array($key, $allowed))
				$conf->put($key,$value);
		}
	});

?>