<?php 
global $myUser;
require_once(__DIR__.SLASH.'ScreenVideo.class.php');
 
if(!isset($_['slug'])) throw new Exception("Identifiant de sécurité non spécifié");

$video = ScreenVideo::load(array('slug'=>$_['slug']));

if(!$video || $video->id==0) throw new Exception("Identifiant de sécurité incorrect");
if($video->expire!=0 && time()>$video->expire){
	throw new Exception("Vidéo expirée depuis le ".date('d/m/Y',$video->expire));
}
$creator = User::byLogin($video->creator);
?>
<h3>Capture Video</h3>
<p>Vidéo partagée par <img class="avatar-mini avatar-rounded" src="<?php echo $creator->getAvatar(); ?>"> <?php echo $creator->fullName(); ?> <span class="pl-5 text-muted">Expiration : <?php echo $video->expire==0 ? 'Jamais': complete_date($video->expire).' à '.date('H:i',$video->expire); ?></span></p>
<video id="screenshare-preview" src="action.php?action=screenshare_screen_video_preview&slug=<?php echo $video->slug; ?>" loop controls class="screenshare-preview" muted autoplay></video>