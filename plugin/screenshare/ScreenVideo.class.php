<?php
/**
 * Define a screenvideo. 
 * @author Administrateur
 * @category Plugin
 * @license MIT
 */
class ScreenVideo extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $expire; //Expiration (Date)
	public $slug; //Slug (Texte)
	public $comment; //Commentaire (Texte Long)
	
	protected $TABLE_NAME = 'screenshare_screen_video';
	public $fields =
	array(
		'id' => 'key',
		'label' => 'string',
		'expire' => 'date',
		'slug' => 'string',
		'comment' => 'longstring'
	);

	//Colonnes indexées
	public $indexes = array();

	public static function dir(){
		return File::dir().SLASH.'screenshare';
	}

	public function file(){
	  	if(!file_exists(self::dir())) mkdir(self::dir(),0755);
	    return self::dir().SLASH.$this->id.'.webm';
	}

	public function remove(){
		self::deleteById($this->id);
		if(file_exists($this->file())) unlink($this->file());
	}

}
?>