<?php
global $myUser,$conf;
User::check_access('screenshare','configure');
?>

<div class="row"> 
	<div class="col-md-12">
          <h3><i class="fas fa-wrench"></i> Réglages Screenshare
                <?php if($myUser->can('screenshare', 'edit')) : ?>
        <div onclick="screenshare_setting_save();" class="btn btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
        <?php endif; ?>
          </h3>
        <hr/>
	</div>
</div>
<p>Veuillez remplir les informations ci dessous.</p>

<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		  <?php echo Configuration::html('screenshare'); ?>
	</div>
</div>
