<?php
global $myUser,$conf;
require_once(__DIR__.SLASH.'Sms.class.php');
User::check_access('sms','configure');
?>

<div class="row">
	<div class="col-md-12">
          <h3><i class="fas fa-wrench"></i> Réglages Sms
                <?php if($myUser->can('sms', 'edit')) : ?>
        <div onclick="sms_setting_save();" class="btn btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
        <?php endif; ?>
          </h3>
        <hr/>
	</div>
</div>
<p>Veuillez remplir les informations ci dessous.</p>

<div class="row">
	<!-- search results -->
	<div class="col-xl-12">
		  <?php echo Configuration::html('sms'); ?>

		  <div class="alert alert-primary" role="alert">
	Si vous n'avez pas les informations ci-dessus, vous pouvez les créer a partir de votre compte ovh à l'adresse :  <a href="https://api.ovh.com/createToken">https://api.ovh.com/createToken</a> 
</div>
		  
	</div>
</div>

<?php if(
	!empty($conf->get('sms_api_key')) && 
	!empty($conf->get('sms_api_secret')) && 
	!empty($conf->get('sms_api_consumer')) &&
	!empty($conf->get('sms_api_service')) 
): ?>
<div class="row">

	<div class="col-xl-12">


	 <?php 
		$sms = new Sms();
		$sms->key = $conf->get('sms_api_key');
		$sms->secret = $conf->get('sms_api_secret');
		$sms->consumer = $conf->get('sms_api_consumer');
		$sms->service = $conf->get('sms_api_service');
		$infos = $sms->info($conf->get('sms_api_service'));
	?>
	<ul class="list-group">
		<li class="list-group-item">Crédit restant : <span class="font-weight-bold"><?php echo $infos['creditsLeft']; ?></span>&euro;</li>
		<li class="list-group-item">Nom du service : <span class="font-weight-bold"><?php echo $infos['name']; ?></span></li>
		<li class="list-group-item">Status: <span class="font-weight-bold"><?php echo $infos['status']; ?></span></li>
	</ul>
	</div>
</div>
<?php endif; ?>
<pre>

