<?php


/**
 * Define a sms.
 * @author Valentin CARRUESCO
 * @category API
 * @license MIT
 * pour ajouter des api  : https://api.ovh.com/console/#/sms
 */
class Sms {

	const ENDPOINT = "ovh-eu";
	public $consumer,$key,$secret,$service,$phone,$message,$token;

	public function send(){
		if(empty($this->key)) throw new Exception("Merci de renseigner le champ  API key");
		if(empty($this->secret)) throw new Exception("Merci de renseigner le champ  API secret");
		if(empty($this->consumer)) throw new Exception("Merci de renseigner le champ API Consumer");
		if(empty($this->phone)) throw new Exception("Merci de renseigner le champ n°téléphone ");

		if(empty($this->message)) throw new Exception("Merci de renseigner le champ message");
		if(substr($this->phone, 0,1)=='0') $this->phone = '+33'.substr($this->phone, 1);

	
		require __DIR__ . '/vendor/autoload.php';
		

		$ovh = new \Ovh\Api( $this->key ,
		                $this->secret,
		                self::ENDPOINT,
		                $this->consumer);

		$content = (object) array(
		    "charset"=> "UTF-8",
		    "class"=> "phoneDisplay",
		    "coding"=> "7bit",
		    "message"=> $this->message,
		    "noStopClause"=> false,
		    "priority"=> "high",
		    "receivers"=> array( $this->phone  ),
		    "senderForResponse"=> true,
		    "validityPeriod"=> 2880
		);
		
		$response = $ovh->post('/sms/'. $this->service . '/jobs', $content);

		if(isset($response['invalidReceivers']) && count($response['invalidReceivers'])>0) throw new Exception("Les numéros suivants sont invalides : ".implode(', ',$response['invalidReceivers']));

		
	}

	 public function info($service){
	 	require __DIR__ . '/vendor/autoload.php';
		$ovh = new \Ovh\Api( $this->key ,
		                $this->secret,
		                self::ENDPOINT,
		                $this->consumer);

	 	return $ovh->get('/sms/'.$service);
	 }

	public function services(){

	 	require __DIR__ . '/vendor/autoload.php';
		$ovh = new \Ovh\Api( $this->key ,
		                $this->secret,
		                self::ENDPOINT,
		                $this->consumer);

	 	return $ovh->get('/sms/');

	 }

	
}
?>