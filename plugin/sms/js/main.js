//CHARGEMENT DE LA PAGE
function init_plugin_sms(){
	switch($.urlParam('page')){
		default:
			sms_histories_search();
		break;
	}
}


//Enregistrement des configurations
function sms_setting_save(){
	$.action({ 
		action : 'sms_setting_save', 
		fields :  $('#sms-setting-form').toJson() 
	},function(){ $.message('info','Configuration enregistrée'); });
}




//Ajout ou modification d'élément sms
function sms_send(){
	$('.btn-send').addClass('btn-preloader');
	var data = $('#sms-form').toJson();
	$.action(data,function(r){
		$.message('success','SMS envoyé');
		sms_histories_search();
	});
}

/** HISTORY **/
	
//Récuperation d'une liste de history dans le tableau #historys
function sms_histories_search(callback){
	
	var box = new FilterBox('#filters');	
	$('#histories').fill({
		action:'sms_histories_search',
		filters : box.filters(),
		sort : $('#histories').sortable_table('get')
	},function(response){
		$('.results-count span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}