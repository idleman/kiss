<?php



//Déclaration d'un item de menu dans le menu principal
function sms_menu(&$menuItems){
	global $myUser;
	if(!$myUser->can('sms','read')) return;
	$menuItems[] = array(
		'sort'=>3,
		'url'=>'index.php?module=sms',
		'label'=>'SMS',
		'icon'=> 'fas fa-sms',
		'color'=> '#3498db'
	);
}

//Cette fonction va generer une page quand on clique sur sms dans menu
function sms_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='sms') return;
	$page = !isset($_['page']) ? 'sheet' : $_['page'];
	$page = str_replace('..','',$page);
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}


//Fonction executée lors de l'activation du plugin
function sms_install($id){
	if($id != 'fr.core.sms') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function sms_uninstall($id){
	if($id != 'fr.core.sms') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
Right::register("sms",array('label'=>"Gestion des droits sur le plugin sms"));

//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function sms_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('sms','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.sms',
		'icon' => 'fas fa-angle-right',
		'label' => 'SMS'
	);


}

//Déclaration des pages de réglages
function sms_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}


function sms_notification_type(&$types){

	$types['sms'] = array(
		'label' =>'SMS',
		'color' =>'#1abc9c',
		'icon'  =>'fas fa-sms',
		'description'  =>"Notifications de type SMS",
		'default_methods' => array(
			'interface' => false,
			'mail' =>  false,
			'sms' =>  true
		)
	);
}


function sms_notification_methods(&$sendTypes){
	Plugin::need('notification/Notification, notification/UserNotification');
	Plugin::need('sms/Sms');

	$sendTypes[] = array(
		'slug' => 'sms',
		'label' => 'SMS',
		'explain' => 'Envoi par SMS',
		'icon' => 'fas fa-sms',
		'callback' => function($recipient, $infos, $notification){
			if($infos['pinned'] == 1) return;

			if(!isset($recipient))
				throw new Exception("Aucun destinataires n\'a été renseignés pour l\'envoi de sms à travers l\'ERP");
			
			$user = User::byLogin($recipient);
			if(empty($user->phone)) return;

			

		

			$phone = str_replace(' ','',$user->phone);
		

			

			Log::put('Envois à '.$phone.' : '.$sms->message,'Sms');
			try{
				$response = sms_send($phone,$infos['label']);
				Log::put('Envois à '.$phone.' : '.json_encode($response),'Sms');
			}catch(Exception $e){
				Log::put('Envois à '.$phone.' ERREUR : '.$e->getMessage(),'Sms');
			}


		}
	);
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('sms',array(
        "Général",
        'sms_api_key' => array("label"=>"Clé de l'api","placeholder"=>"","type"=>"text"),
        'sms_api_secret' => array("label"=>"Clé de Secrete de l'api","placeholder"=>"","type"=>"text"),
        'sms_api_consumer' => array("label"=>"Identifiant consomateur","placeholder"=>"","type"=>"text"),
        'sms_api_service' => array("label"=>"Service l'api","placeholder"=>"sms-ry3016-1","type"=>"text"),

));


function sms_send($phone,$message){
	global $conf;
	require_once(__DIR__.SLASH.'Sms.class.php');
	
	$sms = new Sms();

	$sms->key = $conf->get('sms_api_key');
	$sms->secret = $conf->get('sms_api_secret');
	$sms->consumer = $conf->get('sms_api_consumer');
	$sms->service = $conf->get('sms_api_service');

	$sms->phone = $phone;
	$sms->message = $message;

	$sms->send();
}




//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "sms_install");
Plugin::addHook("uninstall", "sms_uninstall"); 



Plugin::addHook("menu_setting", "sms_menu_setting");    
Plugin::addHook("content_setting", "sms_content_setting");   
Plugin::addHook('notification_type',"sms_notification_type");   
Plugin::addHook("notification_methods", "sms_notification_methods");
Plugin::addHook("menu_main", "sms_menu"); 
Plugin::addHook("page", "sms_page");  
Plugin::addHook("sms_send", "sms_send");  

?>