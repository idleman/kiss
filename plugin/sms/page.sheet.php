<?php 
User::check_access('sms','read');


?>
<div class="sms">
	<div id="sms-form" class="row sms-form justify-content-md-center" data-action="sms_send">
		<div class="col-md-4">
			<h3>Sms</h3>
			<p>Envois de SMS aux clients &amp; collaborateurs</p>
			<small class="text-muted">nb: les caractères suivants sont contre induqés '²','ç' et '°'</small><br>
			<label for="phone">N°Téléphone</label>
			<input id="phone" name="phone" class="form-control" data-type="phone" placeholder="" value="" type="tel" 
       required>
			<label for="message">Message</label>
			<textarea id="message" name="message" class="form-control"></textarea>
			<br/>
			<div onclick="sms_send();" class="btn btn-success btn-send"><i class="fas fa-check"></i> Enregistrer</div>
		</div>
		<div class="col-md-4">
			<select id="filters" data-type="filter" data-label="Recherche" data-function="sms_histories_search">
	            <option value="uid"   data-filter-type="text">Téléphone</option>
	            <option value="creator"   data-filter-type="user">Auteur</option>
	            <option value="created"   data-filter-type="date">Envoyé le</option>
        	</select>
			<h4 class="results-count"><span></span> sms envoyé(s)</h4>
			<ul class="list-group" id="histories" data-entity-search="sms_histories_search">
				<li class="list-group-item hidden">
					<span class="text-muted right" title="{{created}}"><i class="far fa-clock"></i> {{createdReadable}}</span>
					<img src="action.php?action=core_account_avatar_download&amp;user={{creator.login}}&amp;extension=jpg" class="avatar-mini avatar-rounded avatar-login" title="{{creator.login}}"> {{fullname}}<br> Envois sur <span class="font-weight-bold"><i class="fas fa-mobile-alt"></i> {{phone}}</span> 
				</li>
			</ul>
			 <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
       
	        <ul class="pagination justify-content-center mt-2"  data-range="5">
	            <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');sms_histories_search();">
	                <span class="page-link">{{label}}</span>
	            </li>
	        </ul>
		</div>
	</div>
</div>

