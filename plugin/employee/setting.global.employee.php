<?php
global $myUser,$conf;
User::check_access('employee','configure');
require_once(__DIR__.SLASH.'EmployeeWorkTime.class.php');
?>

<div class="row">
    <div class="col-xl-12"><br>
    	
        <h3 class="mb-0"><i class="fas fa-user-tie"></i> Réglages Fiche employé <div onclick="employee_setting_save();" class="btn btn-sm btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div></h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('employee'); ?>

	    <h4>Type de poste</h4>
	    <div data-type="dictionnary-table" data-dictionnary="employee_employee_job"></div>
	    <h4>Status</h4>
	    <div data-type="dictionnary-table" data-dictionnary="employee_employee_statute"></div>
	    <h4>Types de contrats</h4>
	    <div data-type="dictionnary-table" data-dictionnary="employee_employee_contractType"></div>
	    <h4>Temps de travail</h4>
	    <div data-type="dictionnary-table" data-dictionnary="employee_employee_workTime"></div>
	    <h4>Matériel alloué</h4>
	    <div data-type="dictionnary-table" data-dictionnary="employee_employee_hardware"></div>
	    <h4>Temps de travail</h4>
	     <table id="employee-work-times" class="table table-striped " data-entity-search="employee_employee_work_time_search">
            <thead>
                <tr>
                    <th>#</th>
                    <th data-sortable="label">Libellé</th>
                    <th data-sortable="hourByWeek">Heures par semaine</th>
                    <th data-sortable="dayByYear">Jours par an</th>
                    <th data-sortable="recovertype">Récuperation</th>
                    <th data-sortable="hourByWeek">Heures par jour <small class="text-muted">[Heures lundi, Heures mardi ...]</small></th>
                    <th></th>
                </tr>
            </thead>
            
                <thead>
                    <tr id="employee-work-time-form" data-action="employee_employee_work_time_save" data-id="">
                        <th>#</th>
                        <th><input  value="" class="form-control"  type="text"  id="label" ></th>
                        <th><input  value="" class="form-control"  type="text"  data-type="decimal"  id="hourByWeek" ></th>
                        <th><input  value="" class="form-control"  type="text"  data-type="decimal"  id="dayByYear" ></th>
                        <th>
                            <select class="form-control select-control" type="text" id="recovertype" >
                               <?php foreach(EmployeeWorkTime::recovertypes() as $slug=>$recovertype): ?>
                                <option  value="<?php echo $slug; ?>"><?php echo $recovertype['label']; ?></option>
                                <?php endforeach ?>
                            </select>
                        </th>
                         <th><textarea  class="form-control" type="text" id="hourByDay"></textarea></th>
                        
                        <th><div onclick="employee_employee_work_time_save();" class="btn btn-success"><i class="fas fa-check"></i> Enregistrer</div></th>
                    </tr>
                </thead>
            
            <tbody>
                <tr data-id="{{id}}" class="hidden item-line">
                    <td></td>
                    <td>{{label}}</td>
                    <td>{{hourByWeek}}</td>
                    <td>{{dayByYear}}</td>
                    <td>{{recovertype.label}}</td>
                    <td><ul>{{#hourByDay}}<li>{{day}} : <strong>{{hours}} H</strong></li>{{/hourByDay}}</ul></td>
                    <td class="text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            
                            <div class="btn btn-info " onclick="employee_employee_work_time_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                            <div class="btn btn-danger " onclick="employee_employee_work_time_delete(this);"><i class="far fa-trash-alt"></i></div>
                        </div>
                    </td>
                </tr>
           </tbody>
        </table>
    </div>
</div>
