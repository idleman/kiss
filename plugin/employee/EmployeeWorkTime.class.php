<?php
/**
 * Define a Temps de travail empoyé
 * @author Kiss team
 * @category Plugin
 * @license MIT
 */
class EmployeeWorkTime extends Entity{

	public $id;
	public $label; //Libellé (Texte)
	public $hourByWeek; //Heures par semaine (Décimal)
	public $hourByDay; //Heures par jour (Text long)
	public $dayByYear; //Jours par an (Décimal)
	public $recovertype; //Récuperation (Liste classique)
	
	protected $TABLE_NAME = 'employee_work_time';
	public $entityLabel = 'Temps de travail employé';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'label' => array('type'=>'text','label' => 'Libellé'),
		'hourByWeek' => array('type'=>'decimal','label' => 'Heures par semaine'),
		'hourByDay' => array('type'=>'textarea','label' => 'Heures par jour'),
		'dayByYear' => array('type'=>'decimal','label' => 'Jours par an'),
		'recovertype' => array('type'=>'list','label' => 'Récuperation')
	);

	//Colonnes indexées
	public $indexes = array();
	
	//liste des Récuperation possibles
	public static function recovertypes($key=null){
		$items = array(
			'none' => array('label'=>'Aucune'), 
			'rtt' => array('label'=>'RTT'), 
		);
		if(!isset($key)) return $items;
		return isset($items[$key]) ? $items[$key] : array('label'=>'Non définis');
	}
}
?>