
function init_components_employee(input){
	var data = {};
	input.component_autocomplete('employee',$.extend(data,{
		skin  : function(item){
			var html = '';
			var re = new RegExp(input.val(),"gi");
			label = item.label.replace(re, function (x) {
				return '<strong>'+x+'</strong>';
			});
			html += '<div class="media">';
			if(item.logo) html += '';
			html += '<div class="my-auto media-body employee-autocomplete employee-infos"><small class="d-flex flex-column">'
			html+='<span>'+label+'</span>'; 
			html += '</small></div></div>'
			return html;
		},
		onClick : function(selected,element){
			container = input.data("data-component");
			input.val(selected.id);
			var label = selected.label;
			if(selected.parentLabel) label+=' ('+selected.parentLabel+')';
			container.val(label);
			input.trigger('click').trigger('change');
		},
		onLoad : function(component,item){
			var label = item.label;
			component.container.val(label);
		}
	}));
}