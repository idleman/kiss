//CHARGEMENT DE LA PAGE
function init_plugin_employee(){
	switch($.urlParam('page')){
		case 'sheet.employee':
			employee_employee_contract_search();
		break;
		default:
		break;
	}
	$('#employees').sortable_table({
		onSort : employee_search
	});
}

function init_setting_global_employee(){
	employee_employee_work_time_search()
}

//Enregistrement des configurations
function employee_setting_save(){
	$.action({ 
		action: 'employee_setting_save', 
		fields:  $('#employee-setting-form').toJson() 
	},function(){
		$.message('success','Enregistré');
	});
}

/** FICHE EMPLOYé **/
//Récuperation d'une liste  fiche employé dans le tableau #employees
function employee_search(callback,exportMode){
	var box = new FilterBox('#filters');

	if(exportMode) $('.btn-export').addClass('btn-preloader');

	$('#employees').fill({
		action:'employee_search',
		filters: box.filters(),
		sort: $('#employees').sortable_table('get'),
		export:  !exportMode ? false : exportMode
	},function(response){
		$('.results-count span').text(response.pagination.total);
		if(callback!=null) callback();
	});
}

//Ajout ou modification fiche employé
function employee_save(callback,noMessage){
	var data = $('#employee-form').toJson();
	data.hardware = [];
	$('.btn-save').addClass('btn-preloader');
	$('.hardware:checked').each(function(){
		data.hardware.push($(this).attr('data-id'));
	});
	$.action(data,function(r){
		$('#employee-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		if(noMessage===false || !noMessage) $.message('success','Enregistré');
		if(callback) callback();
	});
}


//Suppression fiche employé
function employee_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'employee_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

/** TEMPS DE TRAVAIL EMPLOYé **/
//Récuperation d'une liste  temps de travail employé dans le tableau #employeeworktimes
function employee_employee_work_time_search(callback){

	$('#employee-work-times').fill({
		action:'employee_employee_work_time_search'
	},function(response){
		if(callback!=null) callback();
	});
}

//Ajout ou modification temps de travail employé
function employee_employee_work_time_save(){
	var data = $('#employee-work-time-form').toJson();
	$.action(data,function(r){
		$('#employee-work-time-form').attr('data-id','');
		employee_employee_work_time_search();
		$('#employee-work-time-form').attr('data-id',r.id);
		$.urlParam('id',r.id);
		$.message('success','Enregistré');
	});
}

//Récuperation ou edition temps de travail employé
function employee_employee_work_time_edit(element){
	var line = $(element).closest('.item-line');

	$.action({
		action: 'employee_employee_work_time_edit',
		id: line.attr('data-id')
	},function(r){
		$('#employee-work-time-form').fromJson(r);
		init_components('#employee-work-time-form');
		$('#employee-work-time-form').attr('data-id',r.id);
	});
}

//Suppression temps de travail employé
function employee_employee_work_time_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'employee_employee_work_time_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}

/** CONTRAT EMPLOYé **/
//Récuperation d'une liste  contrat employé dans le tableau #employeecontracts
function employee_employee_contract_search(callback){
	var id = $.urlParam('id');
	if(!id) return;

	$('#employee-contracts').fill({
		action:'employee_employee_contract_search',
		employee : id
	},function(response){
		if(callback!=null) callback();
	});
}

function employee_employee_contract_edit(element){
	
	$('#contractModal').clear().modal('show');
	var data = {action : 'employee_employee_contract_edit'};
	if(element) data.id = $(element).closest('tr').attr('data-id');
	$.action(data,function(r){
		$('#contractModal').fromJson(r);
		$('#contractModal').attr('data-id',r.id);
	});
	
}

//Ajout ou modification contrat employé
function employee_employee_contract_save(){
	
	var saveContract = function(){
		var data = $('#contractModal').toJson();
		data.action='employee_employee_contract_save';
		data.employee = $.urlParam('id');
		data.id = $('#contractModal').attr('data-id');

		$.action(data,function(r){
			employee_employee_contract_search();
			$('#contractModal').modal('hide');
			$.message('success','Enregistré');
		});
	}

	if(!$.urlParam('id')){
		employee_save(function(){saveContract()},true);
	}else{
		saveContract();
	}

	
}


//Suppression contrat employé
function employee_employee_contract_delete(element){
	if(!confirm('Êtes vous sûr de vouloir supprimer cet item ?')) return;
	var line = $(element).closest('.item-line');
	$.action({
		action: 'employee_employee_contract_delete',
		id: line.attr('data-id')
	},function(r){
		line.remove();
		$.message('info','Item supprimé');
	});
}
