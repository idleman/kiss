<?php
global $myUser;
User::check_access('employee','configure');
require_once(__DIR__.SLASH.'EmployeeWorkTime.class.php');
?>
<br>
<h3>Réglages Fiche employé</h3>
<hr/>
<div class="row">
    
    <div class="col-md-4">
        <?php if($myUser->can('employee', 'edit')) : ?>
        <a href="index.php?module=employee&page=sheet.employee.work.time" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
        <?php endif; ?>
    </div>
</div>
<br/>
<div class="row">
    <!-- search results -->
    <div class="col-xl-12">
        <table id="employee-work-times" class="table table-striped " data-entity-search="employee_employee_work_time_search">
            <thead>
                <tr>
                    <th>#</th>
                    <th data-sortable="label">Libellé</th>
                    <th data-sortable="hourByWeek">Heures par semaine</th>
                    <th data-sortable="dayByYear">Jours par an</th>
                    <th data-sortable="recovertype">Récuperation</th>
                    <th></th>
                </tr>
            </thead>
            
                <thead>
                    <tr id="employee-work-time-form" data-action="employee_employee_work_time_save" data-id="">
                        <th>#</th>
                        <th><input  value="<?php echo $employeeworktime->label; ?>" class="form-control"  type="text"  id="label" ></th>
                        <th><input  value="<?php echo $employeeworktime->hourByWeek; ?>" class="form-control"  type="text"  data-type="decimal"  id="hourByWeek" ></th>
                        <th><input  value="<?php echo $employeeworktime->dayByYear; ?>" class="form-control"  type="text"  data-type="decimal"  id="dayByYear" ></th>
                        <th><select class="form-control select-control" type="text" id="recovertype" ><option  value="recovertype-1">Récuperation</option></select></th>
                        <th><div onclick="employee_employee_work_time_save();" class="btn btn-success"><i class="fas fa-check"></i> Enregistrer</div></th>
                    </tr>
                </thead>
            
            <tbody>
                <tr data-id="{{id}}" class="hidden">
                    <td></td>s
                    <td>{{label}}</td>s
                    <td>{{hourByWeek}}</td>s
                    <td>{{dayByYear}}</td>s
                    <td>{{recovertype}}</td>
                    <td class="text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            
                            <div class="btn btn-info " onclick="employee_employee_work_time_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                            <div class="btn btn-danger " onclick="employee_employee_work_time_delete(this);"><i class="far fa-trash-alt"></i></div>
                        </div>
                    </td>
                </tr>
           </tbody>
        </table>

         

    </div>
</div>
