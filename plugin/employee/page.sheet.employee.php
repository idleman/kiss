<?php 
global $conf,$myUser,$myFirm;
User::check_access('employee','read');
require_once(__DIR__.SLASH.'Employee.class.php');
require_once(__DIR__.SLASH.'EmployeeWorkTime.class.php');
$employee = Employee::provide();


$myEmployee = Employee::load(array('account'=>$myUser->login));

if($employee->id == 0){
	$employee->date = time();
	if(is_object($myEmployee)) $employee->manager = $myEmployee->id;
}else{
	if(!$myUser->can('employee','configure')){
		
		if(!$myEmployee) throw new Exception('Vous n\'avez pas la permission de voir cette fiche');
		if(!$myEmployee->can($employee,'read')) throw new Exception('Vous n\'avez pas la permission de voir cette fiche');
	}
}


$hardwares = !empty($employee->hardware) ? json_decode($employee->hardware,true): array();



?>
<div class="plugin-employee">
	<div id="employee-form" class="row justify-content-md-center employee-form" data-action="employee_save" data-id="<?php echo $employee->id; ?>">
		<div class="col-md-6 shadow-sm bg-white p-3">
			<h3><i class="fas fa-user-tie"></i> Fiche employé 
			<div onclick="employee_save();" class="btn btn-small btn-save btn-success right"><i class="fas fa-check"></i> Enregistrer</div>

			<?php if($myUser->can('export', 'read') && $myFirm->has_plugin('fr.core.export')) : ?>
		
				<div class="btn btn-primary btn-small right mx-2" data-type="export-model"  data-parameters='<?php echo stripslashes(json_encode(array("plugin"=>"employee","dataset"=>"employee-sheet","id"=>$employee->id))); ?>'><i class="far fa-file"></i> Exporter en word</div>
		
       		<?php endif; ?>

			<a href="index.php?module=employee&page=list.employee" class="btn btn-small btn-dark right ">Retour</a></h3>
			
			<div class="row">
				<div class="col-md-3">
					<input  value="" class="component-file-cover bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-limit="1"  data-extension="jpg,png,bmp,jpeg,gif"  data-action="employee_employee_photo"  data-id="photo"  data-data='{"id":"<?php echo $employee->id; ?>"}'  id="photo" >
				</div>
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-6">
							<label for="birthName">Nom de naissance</label>
							<input  value="<?php echo $employee->birthName; ?>" onblur="if(!$('#name').val()) $('#name').val($(this).val())" class="form-control"  type="text"  id="birthName" >
						</div>
						<div class="col-md-6">
							<label for="name">Nom marital</label>
							<input  value="<?php echo $employee->name; ?>" class="form-control"  type="text"  id="name" >
						</div>
					</div>
					<label for="firstname">Prénom</label>
					<input  value="<?php echo $employee->firstname; ?>" class="form-control"  type="text"  id="firstname" >
				</div>
			</div>

			
			<label for="job">Poste</label>
			<select class="form-control select-control" type="text" data-type="dictionnary" data-slug="employee_employee_job" data-depth="1" key=data-disable-label data-value="<?php echo $employee->job; ?>" id="job" ></select>
			<label for="jobDescription">Rôle/Missions</label>
			<textarea  class="" type="text" data-type="wysiwyg" id="jobDescription"><?php echo $employee->jobDescription; ?></textarea>
			
			<label for="manager">Responsable</label>
			<input  value="<?php echo $employee->manager; ?>" class="form-control" data-type="employee"  type="text"   id="manager" >
			
			<label for="account">Compte lié</label>
			<input  value="<?php echo $employee->account; ?>" class="form-control"  type="text"  data-type="user"  id="account" >
			
			<label for="workplace">Lieu de travail</label>
			<input  value="<?php echo $employee->workplace; ?>" class="form-control"  type="text"  data-type="location"  id="workplace" >
			

			
			<label for="hardware">Mise a disposition de matériel</label><br>
		
			<?php  foreach(Dictionnary::bySlug('employee_employee_hardware',true) as $item): ?>
				<label><input type="checkbox" <?php echo (in_array($item->id, $hardwares) ? 'checked="checked"':'' ) ?> data-id="<?php echo $item->id; ?>" class="hardware" data-type="checkbox"> <?php echo $item->label; ?></label>
			<?php  endforeach; ?>
			<br/>
			<label for="date">Date</label>
			<input  value="<?php  if(!empty($employee->date)) echo date('d/m/Y',$employee->date); ?>" class="form-control"  type="text"  data-type="date"  title="format jj/mm/aaaa"  placeholder="JJ/MM/AAAA"  id="date" >
			<br/>
			<label for="comment">Information sur le recrutement</label>
			<textarea  class="" type="text" data-type="wysiwyg" id="comment"><?php echo $employee->comment; ?></textarea>
			<br/>
			<label for="attachments">Pièces jointes</label>
			<input  value="" class="component-file-default bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-extension="jpg,png,bmp,jpeg,gif,docx,xlsx,pptx,msg,eml,pdf,zip,doc,xls,ppt,txt,csv"  data-action="employee_employee_attachments"  data-id="attachments"  data-data='{"id":"<?php echo $employee->id; ?>"}'  id="attachments" >
			
		</div>

		<div class="col-md-3 ml-3">
			

			<div class="row">
				<div class="col-md-12 shadow-sm bg-white p-3">
					<h5><i class="fas fa-file-signature"></i> HISTORIQUE CONTRATS</h5>
					 <!-- présentation tableau -->
		            <table id="employee-contracts" class="table table-striped " data-entity-search="employee_employee_contract_search">
		                <thead>
		                    <tr>
		                        <th data-sortable="start">Contrat</th>
		                        <th data-sortable="worktime">Tps travail</th>
		                        <th><div onclick="employee_employee_contract_edit(this);" class="btn btn-mini btn-success btn-squarred right" title="Ajouter un contrat"><i class="fas fa-plus"></i></div></th>
		                    </tr>
		                </thead>
		                
		                <tbody>
		                    <tr data-id="{{id}}" class="hidden item-line">
		    	                <td class="align-middle"><strong>{{start-readable}} - {{#end-readable}} {{end-readable}}{{/end-readable}}{{^end-readable}}<span class="success text-info">Aujourd'hui</span>{{/end-readable}}</strong><br><small class="text-muted">{{type.label}} - {{statute.label}}</small></td>
		    	                <td class="align-middle">{{worktime.label}}</td>
		
		    	                <td class="align-middle text-right">
		                            <div class="btn-group btn-group-sm" role="group">
		                                <div onclick="employee_employee_contract_edit(this);" class="btn text-info" title="Éditer employee_contract"><i class="fas fa-pencil-alt"></i></div>
		                                <div class="btn text-danger" title="Supprimer employee_contract" onclick="employee_employee_contract_delete(this);"><i class="far fa-trash-alt"></i></div>
		                            </div>
		    	                </td>
		                    </tr>
		               </tbody>
		            </table>
		        </div>
		    </div>

		</div>


	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="contractModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edition contrat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        	<label for="contractType">Type de contrat</label>
					<select class="form-control select-control" type="text" data-type="dictionnary" data-slug="employee_employeecontract_type" data-depth="1" key=data-disable-label data-value="" id="type" ></select>

					<label for="statute">Statut</label>
					<select class="form-control select-control" type="text" data-type="dictionnary" data-slug="employee_employeecontract_statute" data-depth="1" key=data-disable-label data-value="" id="statute" ></select>

					<label for="contractStart">Date de début de contrat</label>
					<input  value="" class="form-control"  type="text"  data-type="date"  title="format jj/mm/aaaa"  placeholder="JJ/MM/AAAA"  id="start" >
					
					<label for="contractEnd">Date de fin (si CDD)</label>
					<input  value="" class="form-control"  type="text"  data-type="date"  title="format jj/mm/aaaa"  placeholder="JJ/MM/AAAA"  id="end">
					

					<label for="salary">Rémunération annuelle brute</label>
					<input class="form-control" type="text" data-type="price" value="" id="salary" >

					<label for="workTime">Temps de travail</label>
						
					<select class="form-control select-control"  id="worktime" >
							<?php foreach(EmployeeWorkTime::loadAll() as $workTime): ?>
								<option value="<?php echo $workTime->id; ?>">
									<?php echo $workTime->label; ?>
								</option>
							<?php endforeach; ?>
					</select>

					<label for="cddComment">Commentaire</label>
					<textarea  class="form-control" type="text" id="comment"></textarea>
      </div>
      <div class="modal-footer">
        <div type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</div>
        <div onclick="employee_employee_contract_save()" type="button" class="btn btn-primary">Enregistrer</div>
      </div>
    </div>
  </div>
</div>