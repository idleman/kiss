<?php
global $myUser;
User::check_access('employee','read');
require_once(__DIR__.SLASH.'Employee.class.php');



?>
<div class="plugin-employee">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des fiche employés</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="employee_search(null,true);" id="export-employees-btn" class="btn btn-info btn-export rounded-0 btn-squarred ml-1" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                
                
                <div class="my-auto ml-auto mr-0 noPrint">
                    <?php if($myUser->can('employee', 'edit')) : ?>
                    <a href="index.php?module=employee&page=sheet.employee" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                    <?php endif; ?>
                </div>
                
            </div>
            <div class="clear noPrint"></div>
        </div>
        
        <div class="col-md-12">
            <select id="filters" data-type="filter" data-label="Recherche" data-slug="employee_search" data-function="employee_search"  data-user-shortcut="#employee-user-shortcuts">
                <option value="main.label" data-filter-type="text">Libellé</option>
                <option value="main.birthName" data-filter-type="text">Nom de naissance</option>
                <option value="main.name" data-filter-type="text">Nom marital</option>
                <option value="main.firstname" data-filter-type="text">Prénom</option>
                <option value="main.job" data-filter-type="dictionary">Poste</option>
                <?php if($myUser->can('employee','configure')): ?>
                <option value="man.account" data-filter-type="user">Responsable</option>
                <?php endif; ?>
                <option value="main.workplace" data-filter-type="address">Lieu de travail</option>
            </select>

            <div id="employee-user-shortcuts">
                <h5 class="has-shortcut">MES RECHERCHES</h5>
                <ul  class="no-chip p-0 m-0 mb-2">
                    <li class="hidden d-inline-block">
                        <div class="btn btn-square mr-1" 
                            title="Lancer la recherche"
                            onclick="$('#filters').data('componentObject').filter_search_execute('{{uid}}');" 
                            ><i class="fas fa-search"></i> {{label}}
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        
    </div>
    <h5 class="results-count"><span></span> Résultat(s)</h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">
    		
            <table id="employees" class="table table-striped " data-entity-search="employee_search">
                <thead>
                    <tr>
                        <!--<th>#</th>-->
                        <th data-sortable="main.photo">Photo</th>                  
                        <th data-sortable="main.name">Nom marital</th>
                        <th data-sortable="main.firstname">Prénom</th>
                        <th data-sortable="main.job">Poste</th>
                        <th data-sortable="main.manager">Responsable</th>
                        <th data-sortable="main.workplace">Lieu de travail</th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	                <!--<td class="align-middle">{{id}}</td>-->
    	                <td class="align-middle">{{#photo}}<img class="shadow-sm rounded-sm" style="max-width: 100px;height: auto;" src="{{photo}}">{{/photo}}</td>
    	             
    	                <td class="align-middle">{{name}} {{#birthName}}<br><span class="text-muted">Anc. ({{birthName}})</span>{{/birthName}}</td>
    	                <td class="align-middle">{{firstname}}</td>
    	                <td class="align-middle">{{job.label}}</td>
    	    
    	               
    	                <td class="align-middle text-center">
                            {{#manager}}
                            <img src="{{manager.photo}}" class="avatar-mini avatar-rounded avatar-login" data-tooltip title="{{manager.fullName}}"> <br><small class="text-muted">{{manager.fullName}}</small>
                            {{/manager}}
                        </td>
    	                <td class="align-middle">{{workplace}}</td>
    	               
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                <a class="btn text-info" title="Éditer employee" href="index.php?module=employee&page=sheet.employee&id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <div class="btn text-danger" title="Supprimer employee" onclick="employee_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table><br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');employee_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
          
    	</div>
    </div>
</div>
