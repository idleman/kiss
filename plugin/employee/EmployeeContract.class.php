<?php
/**
 * Define a Contrat employé
 * @author Kiss team
 * @category Plugin
 * @license MIT
 */
class EmployeeContract extends Entity{

	public $id;
	public $employee; //Employé (Nombre Entier)
	public $start; //Date de début (Date)
	public $end; //date de fin (Date)
	public $type; //Type de contrat (Liste configurable)
	public $statute; //Statut (Liste configurable)
	public $salary; //Rémunération annuelle brute (Prix)
	public $worktime; //Temps de travail (Nombre Entier)
	public $comment; //Commentaire (Texte enrichi)
	
	protected $TABLE_NAME = 'employee_contract';
	public $entityLabel = 'Contrat employé';
	public $fields = array(
		'id' => array('type'=>'key', 'label' => 'Identifiant'),
		'employee' => array('type'=>'integer','label' => 'Employé','link'=>'plugin/employee/Employee.class.php'),
		'start' => array('type'=>'date','label' => 'Date de début'),
		'end' => array('type'=>'date','label' => 'date de fin'),
		'type' => array('type'=>'dictionary','label' => 'Type de contrat','link'=>'class/Dictionary.class.php'),
		'statute' => array('type'=>'dictionary','label' => 'Statut'),
		'salary' => array('type'=>'price','label' => 'Rémunération annuelle brute'),
		'worktime' => array('type'=>'integer','label' => 'Temps de travail','link'=>'plugin/employee/EmployeeWorkTime.class.php'),
		'comment' => array('type'=>'wysiwyg','label' => 'Commentaire')
	);

	//Colonnes indexées
	public $indexes = array();
	
}
?>
