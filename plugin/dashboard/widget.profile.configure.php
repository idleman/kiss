<?php

?>
<div id="dashboard-widget-profile-form">
	<div class="input-group">
		<div class="input-group-prepend">
			<label for="widget-profile-background-color" class="input-group-text">Couleur de bannière :</label>
		</div>
		<input class="form-control" name="widget-profile-background-color" type="text" data-type="color" value="<?php echo $widget->data('background-color'); ?>" id="widget-profile-background-color">
	</div>
</div> 