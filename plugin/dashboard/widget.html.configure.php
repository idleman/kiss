<?php
$html = empty( $widget->data('html')) ? '<div class="p-3">Votre texte ici</div>':  $widget->data('html');

?>

<div id="dashboard-widget-html-form">
	<div>
		<label class="m-0">Titre / couleur de la fenetre :</label>
		<div class="input-group mb-2">
			<input class="form-control" type="text" value="<?php echo $widget->data('title'); ?>" id="widget-html-title">
			
			<input class="form-control" data-type="color" type="text" value="<?php echo $widget->data('color'); ?>" id="widget-html-color">
			
		</div>
	</div>
	<div>
		<label class="m-0">Code html à afficher :</label>
		<textarea data-type="wysiwyg" data-script-allow="true" class="mt-0 mb-2" id="widget-html-content" style="height:400px;"><?php echo $html; ?></textarea>
	</div>
</div> 