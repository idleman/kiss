<?php 
global $myUser,$conf; 
require_once(__DIR__.SLASH.'Dashboard.class.php');
require_once(__DIR__.SLASH.'DashboardWidget.class.php');

$mandatoryDashboard = Dashboard::load(array('mandatory'=>'1'));
$readonly = false;
if($mandatoryDashboard){
	$readonly = !$myUser->can('dashboard','configure');
	$dashboards =  array($mandatoryDashboard);
}else{
	$dashboards = Dashboard::loadAll(array('user'=>$myUser->login));
} ?>
<div class="dashboard-container <?php echo $readonly?'readonly':'' ?>" style="<?php echo count($dashboards)==1 ? 'padding-left: 0px;' : ''; ?>">
	<?php if($myUser->connected()): ?>
		<ul id="dashboard-view" class="<?php echo count($dashboards)>1?'':'hidden' ?>" >
			<li class="dashboard-view-title">DASH</li>
			<?php 
			
			if(count($dashboards)==0){
				$defaultDash = Dashboard::provide();
				$defaultDash->user = $myUser->login;
				$defaultDash->label = 'Général';
				$defaultDash->icon = 'far fa-bookmark';
				$defaultDash->default = true;
				$defaultDash->save();
				$dashboards[] = $defaultDash;

				foreach(array('profile','clock','log') as $i=>$widget){
					$item = new DashboardWidget();
					$item->model = $widget;
					$item->position = $i;
					$item->dashboard = $defaultDash->id;
					$item->save();
				}
			}
			foreach($dashboards as $dashboard): ?>
				<li class="dashboard-item" <?php echo $dashboard->default?'data-selected="1"':''; ?> data-id="<?php echo $dashboard->id; ?>"  title="<?php echo $dashboard->label; ?>"><span><?php echo $dashboard->label; ?></span><div><i class="<?php echo $dashboard->icon; ?>"></i></div></li>
			<?php endforeach; ?>
		</ul>
		<div class="clear"></div>

		<ul class="dashboard-widget-menu">
		<li data-toggle="modal" data-target="#addWidgetModal">
			<div title="Ajouter un widget"><i class="fas fa-plus"></i> <span>WIDGET</span></div>
		</li>
	</ul>
	
	<div class="row" id="dashboard">
		<!-- MODEL WIDGET -->
		<div class="widget hidden" data-width="4" data-id="" >
			<div class="widget_window">
				<div class="widget_header pl-2">
					<i class="fa fa-caret"></i> <span></span>
					<ul class="widget_options"></ul>
				</div>
				<div class="widget_content"></div>
				<div class="widget_footer"></div>
				<div class="widget_resize"></div>
			</div>
		</div>
		<!-- Add wiget modal -->
		<div class="modal fade" id="addWidgetModal" tabindex="-1" role="dialog" aria-labelledby="widget-add-label" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="widget-add-label">Ajout d'un widget</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					    	<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
							<?php
								$models = array();
								Plugin::callHook('widget',array(&$models));
							?>
								<select class="form-control" id="widget-list" class="left" size="8">
									<option data-description="Sélectionnez le widget que vous souhaitez ajouter" data-width="" data-background="" value="" data-icon="fas fa-plus"> - </option>
									<?php
									foreach($models as $model): ?>
									<option data-description="<?php echo $model->description; ?>" data-width="<?php echo !empty($model->width)?$model->width:1; ?>"  data-background="<?php echo $model->background; ?>" data-icon="<?php echo $model->icon; ?>" value="<?php echo $model->model; ?>"><?php echo $model->title; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
							<div class="widgetDescription col-md-6">
								<h3><i class='fas fa-plus'></i> <span>Sélectionnez un widget</span></h3>
								<p>Sélectionnez le widget que vous souhaitez ajouter</p> 
								<div class="widgetWidth"></div>
								<div class='widgetColor'></div>
							</div>
						</div>
				  	</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
					<button type="button" class="btn btn-primary" onclick="dashboard_dashboardwidget_add();"><i class="fas fa-plus"></i> Ajouter</button>
				</div>
				</div>
			</div>
		</div>
		<!-- Configure wiget modal -->
		<div class="modal fade" id="configureWidgetModal" tabindex="-1" role="dialog" aria-labelledby="widget-configure-label" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="widget-configure-label">Configuration d'un widget</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="pluginContent">
							<!-- Configuration plugin ici -->
						</div>
					</div>
					<div class="modal-footer">
					 	<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						<button type="button" class="btn btn-primary" onclick="dashboard_dashboardwidget_save_configuration();"><i class="fas fa-check"></i> Enregistrer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div> 
