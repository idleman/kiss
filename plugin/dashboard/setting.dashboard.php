<?php
global $myUser;
User::check_access('dashboard','configure');
require_once(__DIR__.SLASH.'DashboardWidgetShare.class.php');
require_once(__DIR__.SLASH.'DashboardWidget.class.php');
require_once(__DIR__.SLASH.'Dashboard.class.php');

$widgets = array();
foreach (Dashboard::loadAll(array('user'=>$myUser->login)) as $dashboard) {
    foreach (DashboardWidget::loadAll(array('dashboard'=>$dashboard->id)) as $widget) {
        $widgets[] = $widget;
    }
}
usort($widgets, function($a,$b){
    if($a > $b) return 1;
    if($a < $b) return -1;
    if($a == $b) return 0;
});
?>

<div class="row">
    <div class="col-md-12"><br>
        <h3>Réglages Dashboard</h3>
        <div class="clear"></div>
        <hr>
    </div>
</div>

<div class="row">
    <!-- search results -->
    <div class="col-xl-12">
        <div class="tab-container mb-0 noPrint">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#tab-dashboards" aria-controls="tab-dashboards" aria-selected="false">Liste Dashboards</a></li>
                <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-shared-widgets" aria-controls="tab-shared-widgets" aria-selected="false">Widgets communs</a></li>
            </ul>
        </div>

        <div class="tab-content">
            <!-- Onglet Dashboards -->
            <div class="tab-pane show active in" id="tab-dashboards" role="tabpanel" aria-labelledby="tab-dashboards">
                <div class="alert alert-info my-3"><strong>NB :</strong> Marquer une dashboard comme obligatoire imposera celle-ci à l'ensemble des utilisateurs (sans possibilité de modification)</div>
                <legend>Dashboard disponibles :</legend>
                <table id="dashboards" class="table table-striped table-dashboard" data-entity-search="dashboard_search">
                     <thead class="bg-secondary text-light">
                         <tr>
                             <th data-sortable="label">Libellé</th>
                             <?php if($myUser->can('dashboard','configure')): ?>
                             <th data-sortable="creator">Utilisateur</th>
                             <?php endif; ?>
                             <th data-sortable="icon">Icône</th>
                             <th data-sortable="default" class="text-center">Par défaut</th>
                             <th data-sortable="mandatory" class="text-center">Obligatoire</th>
                             <th></th>
                         </tr>
                         <tr id="dashboard-form" data-action="dashboard_dashboard_save" data-id="">
                             <th class="align-middle"><input id="label" name="label" class="form-control" placeholder="eg. Metrics..." type="text"></th>
                             <?php if($myUser->can('dashboard','configure')): ?>
                             <th class="align-middle position-relative"><input id="user" name="user" data-type="user" class="form-control" placeholder="eg. John Doe..."></th>
                             <?php endif; ?>
                             <th class="align-middle position-relative"><input id="icon" name="icon" data-type="icon" class="form-control w-100" value="far fa-bookmark" type="text"></th>
                             <th class="align-middle text-center"><input id="default" name="default" class="" data-type="checkbox" type="checkbox"></th>
                             <th class="align-middle text-center"><input id="mandatory" name="mandatory" class="" data-type="checkbox" type="checkbox"></th>
                             <th class="align-middle text-right"><div onclick="dashboard_dashboard_save();" class="btn btn-success"><i class="fas fa-check"></i></div></th>
                         </tr>
                     </thead>
                     
                     <tbody>
                         <tr data-id="{{id}}" class="hidden">
                             <td>{{label}}</td>
                             <?php if($myUser->can('dashboard','configure')): ?>
                             <td>{{user}}</td>
                             <?php endif; ?>
                             <td><i class="{{icon}}"></i> {{icon}}</td>
                             <td class="text-center">
                                 {{#default}}<i class="fas fa-check text-success"></i>{{/default}}
                                 {{^default}}<i class="fas fa-times text-danger"></i>{{/default}}
                             </td>
                             <td class="text-center">
                                 {{#mandatory}}<i class="fas fa-check text-success"></i>{{/mandatory}}
                                 {{^mandatory}}<i class="fas fa-times text-danger"></i>{{/mandatory}}
                             </td>
                             <td class="text-right">
                                 <div class="btn btn-info btn-mini btn-squarred" onclick="dashboard_dashboard_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                                 <div class="btn btn-danger btn-mini btn-squarred" onclick="dashboard_dashboard_delete(this);"><i class="far fa-trash-alt"></i></div>
                             </td>
                         </tr>
                    </tbody>
                </table>

                <!-- Pagination -->
                <ul class="pagination">
                    <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');dashboard_dashboard_search();">
                        <a class="page-link" href="#">{{label}}</a>
                    </li>
                </ul>
            </div>

            <div class="tab-pane" id="tab-shared-widgets" role="tabpanel" aria-labelledby="tab-shared-widgets"><br>
                <legend>Gestion des widgets communs :</legend>
                <table id="dashboard-widget-shares" class="table table-striped " data-entity-search="dashboard_widget_share_search">
                    <thead class="bg-secondary text-light">
                        <tr>
                            <th>ID Widget</th>
                            <th class="text-center">Obligatoire</th>
                            <th>Afficher pour <small class="text-muted">(Laisser vide pour tout le monde)</small></th>
                            <th>Ordre par défaut</th>
                            <th></th>
                        </tr>
                        <tr id="dashboard-widget-share-form" data-action="dashboard_widget_share_save" data-id="">
                            <th>
                                <select id="widget" name="widget" class="form-control">
                                    <option value="">-</option>
                                <?php foreach ($widgets as $widget) : ?>
                                   <option value="<?php echo $widget->id; ?>">Widget type <?php echo $widget->model ?> en position <?php echo $widget->position ?></option> 
                                <?php endforeach; ?>
                                </select>
                            </th>
                            <th class="text-center align-middle"><input id="mandatory" data-type="checkbox" name="mandatory" class="" placeholder="" value="" type="checkbox"></th>
                            <th style="position:relative;"><input id="uid" name="uid" class="form-control" placeholder="" value="" type="text" data-type="user"  data-types="user,rank"></th>
                            <th><input id="sort" name="sort" class="form-control" placeholder="" value="" type="text"></th>
                            <th class="text-center"><div onclick="dashboard_widget_share_save();" class="btn btn-success"><i class="fas fa-check"></i></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr data-id="{{id}}" class="hidden">
                            <td>{{widget.model}} (#{{widget.id}}) en position {{widget.position}}</td>
                            <td>{{mandatory}}</td>
                            <td>{{for}}</td>
                            <td>{{sort}}</td>
                            <td class="text-right">
                                <div class="btn btn-info btn-mini btn-squarred " onclick="dashboard_widget_share_edit(this);"><i class="fas fa-pencil-alt"></i></div>
                                <div class="btn btn-danger btn-mini btn-squarred " onclick="dashboard_widget_share_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </td>
                        </tr>
                   </tbody>
                </table>
            </div>
       </div>
    </div>
</div>
