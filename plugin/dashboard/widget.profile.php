<?php 
global $myUser;

$function = !empty($myUser->function) ? $myUser->function : 'Employé';
$phone = !empty($myUser->phone) ? $myUser->phone : '';
$mail = !empty($myUser->mail) ? $myUser->mail : '';
?>
<div class="profileContainer">
	<div class="profileHeader" style="<?php echo 'background-color:'.$widget->data('background-color'); ?>"></div>
	<div class="profileImage">
		<a href="account.php"><img class="avatar-mini" src="<?php echo $myUser->getAvatar(); ?>"><div title="Éditer mon profil" class="edit-overlay"></div></a>
	</div>
	<div class="profile-content">
		<h3 class="w-100"><?php echo $myUser->fullname(); ?></h3>
		<small><?php echo $function; ?></small><br>
		<small><?php echo $phone; ?> <a href="mailto:'.$mail.'"><?php echo $mail; ?></a></small>
	</div>
	<div class="profile-buttons">
		<a href="account.php" class="btn text-uppercase"><i class="far fa-meh-blank"></i> Profil</a>
		<a href="account.php?section=notification" class="btn text-uppercase"><i class="fas fa-bell"></i> Notifications</a>
		<a onclick="core_logout();" class="btn text-danger text-uppercase"><i class="fas fa-sign-out-alt"></i> Déconnexion</a>
		<div class="clear"></div>
	</div>
</div>