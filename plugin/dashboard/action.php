<?php


/** DASHBOARDWIDGET / BLOC DE TABLEAU DE BORD **/

Action::register('dashboard_widget_search',function(&$response){
	global $_;
	User::check_access('dashboard','read');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	$filters = array();
	if(empty($_['scope']))  throw new Exception('Portée non spécifiée');
	$filters['scope'] = $_['scope'];
	if(empty($_['uid'])) $filters['uid'] = $_['uid'];

	$dashboard = Dashboard::load($filters);
	$widgets = DashboardWidget::loadAll(array('dashboard'=>$dashboard->id));

	$response['widgets'] = array();
	foreach($widgets as $widget){
		$response['widgets'][] = $widget->toData();
	}
});

Action::register('dashboard_widget_by_id',function(&$response){
	global $_;
	User::check_access('dashboard','read');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	if(empty($_['id']))  throw new Exception('Id non spécifiée');
	$response['item'] = DashboardWidget::getById($_['id'])->toArray();
});

Action::register('dashboard_widget_add',function(&$response){
	global $_,$myUser;
	User::check_access('dashboard','edit');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	$filters = array();
	if(empty($_['scope'])) throw new Exception('Portée non sépcifiée');
	$filters = array('scope'=>$_['scope']);
	if(!empty($_['uid'])) $filters['uid'] = $_['uid'];

	$dashboard = Dashboard::load($filters);
	if(!$dashboard) throw new Exception("Tableau de bord introuvable");

	$widget = new DashboardWidget();
	$widget->dashboard = $dashboard->id;
	if(isset($_['row'])) $widget->row = $_['row'];
	if(isset($_['column'])) $widget->column = $_['column'];
	$widget->save();

	$response = $widget->toData();
});

Action::register('dashboard_widget_move',function(&$response){
	global $_;
	User::check_access('dashboard','edit');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	if(empty($_['widget']['id'])) throw new Exception('Aucun identifiant renseigné');
	if(!isset($_['widget']['column'])) throw new Exception('Colonne non spécifiée');
	if(!isset($_['widget']['row'])) throw new Exception('Ligne non spécifiée');

	$widget = DashboardWidget::getById($_['widget']['id'],1);
	if(!$widget) throw new Exception('Widget introuvable');

	$widget->column = $_['widget']['column'];
	$widget->row = $_['widget']['row'];
	$widget->save();
});

Action::register('dashboard_widget_resize',function(&$response){
	global $_;
	User::check_access('dashboard','edit');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	if(empty($_['widget']['id'])) throw new Exception('Aucun identifiant renseigné');
	if(!isset($_['widget']['width'])) throw new Exception('Largeur non spécifiée');
	if(!isset($_['widget']['height'])) throw new Exception('Hauteur non spécifiée');

	$widget = DashboardWidget::getById($_['widget']['id'],1);
	if(!$widget) throw new Exception('Widget introuvable');

	$widget->width = $_['widget']['width'];
	$widget->height = $_['widget']['height'];
	$widget->save();
});

Action::register('dashboard_widget_delete',function(&$response){
	global $_;
	User::check_access('dashboard','delete');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	if(empty($_['widget']['id'])) throw new Exception('Aucun identifiant renseigné');

	$widget = DashboardWidget::getById($_['widget']['id'],1);
	if(!$widget) throw new Exception('Widget introuvable');

	DashboardWidget::deleteById($widget->id);
});



Action::register('dashboard_model_search',function(&$response){
	global $_;
	User::check_access('dashboard','read');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');
	$response['rows'] = array();
	foreach(DashboardWidget::model() as $model){
		$row = $model->toArray();
		$row["excerpt"] = truncate($row["description"], $length = 100, array('html'=>true)) ;
		if($row["description"] == $row["excerpt"]) $row["description"] = '';
		$row['created'] = date('d-m-Y',$row['created']);
		$response['rows'][] = $row;
	}
	unset($response['rows'][DashboardWidget::MODEL_NEW]);
});


Action::register('dashboard_widget_refresh',function(&$response){
	global $_;
	User::check_access('dashboard','read');
	require_once(__DIR__.SLASH.'Dashboard.class.php');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');

	$response['rows'] = array();
	$widgets = array();

	$filters = array();
	if(empty($_['scope']))  throw new Exception('Portée non spécifiée');
	$filters['scope'] = $_['scope'];
	if(empty($_['uid'])) $filters['uid'] = $_['uid'];

	$dashboard = Dashboard::load($filters);


	foreach(DashboardWidget::loadAll(array('dashboard'=>$dashboard->id)) as $widget){
		if(!isset($widgets[$widget->model])) $widgets[$widget->model] = array();
		$widgets[$widget->model][] = $widget;
	}

	foreach(DashboardWidget::model() as $model){
		if(!isset($model->content) || is_string($model->content) || !isset($widgets[$model->model]) ) continue;
		
		$method = $model->content;
		foreach($widgets[$model->model] as $currentWidget)
			$method($currentWidget);
			$response['rows'][] = $currentWidget;
	}
});

Action::register('dashboard_widget_configure',function(&$response){
	global $_;
	User::check_access('dashboard','edit');
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');
	require_once(__DIR__.SLASH.'Dashboard.class.php');

	if(empty($_['id'])) throw new Exception('Aucun identifiant renseigné');

	$widget = DashboardWidget::getById($_['id'],1);
	if(!$widget) throw new Exception('Widget introuvable');

	$widget->fromArray($_);
	$widget->save();
});




