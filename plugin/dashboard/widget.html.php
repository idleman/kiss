<?php 
global $myUser;


$html = $widget->data('html');

if(empty($html)) $html = '<h4 class="noContent"><i class="fas fa-code"></i> Aucun code spécifié</h4>';
?>
<div class="widgetHtmlContainer">
	<?php echo  $html; ?>
</div>