 <?php
global $myUser,$conf;
if(!$myUser->can('dashboard','read')) return;
require_once(__DIR__.SLASH.'Dashboard.class.php');
require_once(__DIR__.SLASH.'DashboardWidget.class.php');

if(Dashboard::rowCount(array('scope'=>'home','uid'=>$myUser->login))==0){
    $dashboard = new Dashboard();
    $dashboard->uid = $myUser->login;
    $dashboard->scope = "home";
    $dashboard->save();
}

?>
<div class="plugin-dashboard">
    <div class="row">
        <div class="col-md-12">
            <div data-type="dashboard" id="home-dashboard" data-scope="home" data-uid="<?php echo $myUser->login ?>"></div>
        </div>
    </div>
</div>