<?php
/**
 * Define a dashboardwidgetshare.
 * @author Administrateur
 * @category Plugin
 * @license copyright
 */
class DashboardWidgetShare extends Entity{

	public $id;
	public $widget; //ID Widget (Entier)
	public $mandatory; //Obligatoire (Booléen)
	public $entity; //Entité de partage (Texte)
	public $uid; //Uid de partage (Texte)
	public $sort; //Ordre par défaut (Entier)
	
	protected $TABLE_NAME = 'dashboard_dashboard_widget_share';
	public $fields = array(
		'id' => 'key',
		'widget' => array('label'=>'Widget','type'=>'int','link'=>'plugin/dashboard/DashboardWidget.class.php'),
		'mandatory' => 'boolean',
		'entity' => 'string',
		'uid' => 'string',
		'sort' => 'int'
	);


	//Colonnes indexées
	public $indexes = array();
}
?>