<?php
/**
 * Define a dashboard.
 * @author Administrateur
 * @category Plugin
 * @license copyright
 */
class Dashboard extends Entity{
	public $id,$user,$label,$icon,$default,$mandatory;
	protected $TABLE_NAME = 'dashboard_dashboard';
	public $fields =
	array(
		'id' => 'key',
		'user' => 'string',
		'label' => 'string',
		'icon' => 'string',
		'mandatory' => 'boolean',
		'default' => 'boolean'
	);
}
?>