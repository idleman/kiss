<?php 
$logs = Log::loadAll(array(),array('created DESC'),array(30));
$lastDate = '';
?>

<ul class="dashboard-widget-log">
<?php foreach($logs as $log): ?>
	<li>
		<?php if($lastDate!=date('d-m-y',$log->created)): 
			$lastDate = date('d-m-y',$log->created); ?>
			<h2 class="text-muted"><i class="far fa-calendar-alt"></i> <?php echo day_name(date('N',$log->created)).' '.date('d ',$log->created).' '.month_name(date('m',$log->created)).' '.date('Y',$log->created); ?></h2>
		<?php endif; ?>
		<span class="font-weight-bold"><i class="far fa-clock"></i> <?php echo date('H:i:s',$log->created); ?></span> | <small class="text-primary pr-1"><i class="far fa-meh-blank"></i> <?php echo $log->creator; ?></small>
		<?php echo $log->label(); ?>
	</li>
<?php endforeach; ?>
</ul>