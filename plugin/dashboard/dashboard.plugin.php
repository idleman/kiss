<?php
//Cette fonction va generer une page quand on clique sur dashboard dans menu
function dashboard_page(){
	global $_,$myUser,$conf;
	if(isset($_['module']) || !$myUser->connected()) return;
	$page = !isset($_['page']) ? 'home' : $_['page'];
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	require_once($file);
}

//Fonction executée lors de l'activation du plugin
function dashboard_install($id){
	if($id != 'fr.core.dashboard') return;
	global $conf;
	Entity::install(__DIR__);
	$conf->put('dashboard_enable_sidebar',1);
}

//Fonction executée lors de la désactivation du plugin
function dashboard_uninstall($id){
	if($id != 'fr.core.dashboard') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('dashboard',array('label'=>'Gestion des droits sur le plugin dashboard'));


//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function dashboard_menu_setting(&$settingMenu){
	global  $myUser;
	
	if(!$myUser->can('dashboard','configure')) return;
	$settingMenu[] = array(
		'sort' =>1,
		'url' => 'setting.php?section=dashboard',
		'icon' => 'fas fa-angle-right',
		'label' => 'Dashboard'
	);
}

//Déclaration des pages de réglages
function dashboard_content_setting(){
	global $_;
	$_['section'] = str_replace('..', '', $_['section']);

	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}



function dashboard_default_widget(&$widgets){
	global $myUser;
	require_once(__DIR__.SLASH.'DashboardWidget.class.php');

	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'clock';
	$modelWidget->title = 'Horloge';
	$modelWidget->icon = 'far fa-clock';
	$modelWidget->background = '#212529';
	$modelWidget->load = 'action.php?action=dashboard_widget_clock_load';
	$modelWidget->js = [Plugin::url().'/js/progressbar.min.js?v=2',Plugin::url().'/js/widget.js?v=2'];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v='.time()];
	$modelWidget->description = "Affiche l'heure en temps réel";
	$widgets[] = $modelWidget;

	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'profile';
	$modelWidget->title = 'Profil';
	$modelWidget->minWidth = 2;
	$modelWidget->maxWidth = 4;
	$modelWidget->icon = 'far fa-user';
	$modelWidget->background = '#007bff';
	$modelWidget->load = 'action.php?action=dashboard_widget_profile_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js?v='.time()];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v='.time()];
	$modelWidget->configure = 'action.php?action=dashboard_widget_profile_configure';
	$modelWidget->configure_callback = 'dashboard_widget_profile_configure_save';
	$modelWidget->description = "Affiche les informations de profil";
	$widgets[] = $modelWidget;

	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'html';
	$modelWidget->title = 'Texte / Code HTML';
	$modelWidget->icon = 'fas fa-code';
	$modelWidget->background = '#686de0';
	$modelWidget->load = 'action.php?action=dashboard_widget_html_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js?v='.time()];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v='.time()];
	$modelWidget->configure = 'action.php?action=dashboard_widget_html_configure';
	$modelWidget->configure_callback = 'dashboard_widget_html_configure_save';
	$modelWidget->description = "Affiche un texte ou un morceau de code html intégré";
	$widgets[] = $modelWidget;

	$modelWidget = new DashboardWidget();
	$modelWidget->model = 'log';
	$modelWidget->title = 'Logs';
	$modelWidget->defaultWidth = 8;
	$modelWidget->options[] = array('function'=>'window.location = \'setting.php?section=log\';','icon'=>'fa-eye','title'=>'Voir tous les logs');
	$modelWidget->icon = 'far fa-comment-dots';
	$modelWidget->background = '#28a745';
	$modelWidget->load = 'action.php?action=dashboard_widget_log_load';
	$modelWidget->js = [Plugin::url().'/js/widget.js?v='.time()];
	$modelWidget->css = [Plugin::url().'/css/widget.css?v='.time()];
	$modelWidget->description = "Affiche les informations des 30 derniers logs";
	if($myUser->can('log','read')) 
		$widgets[] = $modelWidget;
}



//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addJs("/js/main.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "dashboard_install");
Plugin::addHook("uninstall", "dashboard_uninstall"); 


Plugin::addHook("page", "dashboard_page");  

Plugin::addHook("menu_setting", "dashboard_menu_setting");    
Plugin::addHook("content_setting", "dashboard_content_setting");   
Plugin::addHook("widget", "dashboard_default_widget");

?>