<?php
global $myUser;
User::check_access('issue','read');
require_once(__DIR__.SLASH.'IssueReport.class.php');
require_once(__DIR__.SLASH.'IssueReportTag.class.php');

$browsers = array(
    'opera' => 'Opera',
    'firefox' => 'Mozilla Firefox',
    'chrome' => 'Google Chrome',
    'safari' => 'Safari',
    'ie' => 'Internet Explorer',
    'edge' => 'Microsoft Edge',
    'blink' => 'Blink'
);
$states = array();
foreach(IssueReport::states() as $slug=>$state)
    $states[$slug] = $state['label'];

$tags = IssueReportTag::tags();

$defaultFilters = filters_default(array(
    "",
    array(
        'state' => "open"
    )
));

?>

<div class="row">
    <div class="col-xl-9 noPrint">
        <h3>Tickets</h3>
    </div>
    <div class="col-xl-3 noPrint">
         <div class="btn btn-primary right" onclick="issue_add();" title="Déclarer un nouveau bug ou faire une nouvelle demande"><i class="fas fa-plus"></i> Déclarer un ticket</div>
    </div>
</div>
<div class="row mt-2">
    <div class="tab-content col-xl-12">
        <!-- Onglet Listing des rapports -->
        <div class="tab-pane show active in" id="tab-reports" role="tabpanel" aria-labelledby="tab-reports">
            <div class="issue-reports-search mb-2">
                
                <select id="filters" 
                    data-user-shortcut="#issue-shortcuts" 
                    data-slug="issue_search" 
                    data-label="Trouver un ticket" 
                    data-type="filter"
                    data-show-url-advanced="false"
                    data-default='<?php echo json_encode($defaultFilters); ?>' 
                    data-function="issue_issuereport_search">
                    <option value="{{table}}.id" data-filter-type="number">Numéro ticket</option>
                    <option value="state" data-filter-type="list" data-values='<?php echo json_encode($states); ?>'>État</option>
                    <option value="from" data-filter-type="text">Url</option>
                    <option value="comment" data-filter-type="text">Commentaire</option>
                    <option value="{{table}}.creator" data-filter-type="user">Auteur</option>
                    <option value="assign" data-filter-type="user">Assigné</option>
                    <option value="{{table}}.created" data-filter-type="date">Date</option>
                    <option value="browser" data-filter-type="list" data-values='<?php echo json_encode($browsers); ?>'>Navigateur</option>
                    <option value="ip" data-filter-type="text">Adresse IP</option>
                </select>
                <div id="issue-shortcuts" class="p-0 m-0">
                    <ul class="pl-0 mb-0 list-unstyled">
                        <li class="hidden d-inline-block">
                            <div class="btn btn-small btn-link pl-0" onclick="$('#filters').data('componentObject').filter_search_execute('{{uid}}');"><i class="fas fa-search"></i> {{label}}</div>
                        </li>
                    </ul>
                </div>
                <div>
                    <label class="d-inline-block font-weight-bold text-muted text-uppercase">Tags :</label>
                    <div class="d-inline-block">
                        <input type="text" onchange="issue_issuereport_search()" data-tags='<?php echo json_encode($tags); ?>' id="report-tags" data-type="tagcloud">
                    </div>
                    
                </div>
            </div>
          
            <ul id="issuereports" class="list-issue-reports" data-entity-search="issue_issuereport_search">
                <li data-id="{{id}}" class="hidden">
                    <a class="issue-title" title="Voir le rapport détaillé" href="index.php?module=issue&page=sheet.report&id={{id}}">
                        <div class="issue-state">
                            <i class="{{state.icon}}" style="color: {{state.color}};" title="{{state.label}}"></i>
                        </div>
                        <div class="infos-cell text-left">
                            <span class="font-weight-bold d-inline-block mb-1">{{{excerpt}}}</span>
                            
                            {{#assign}}<span class="assign"> Assigné à <i class="ml-1 far fa-meh-blank"></i> {{assign}}</span>{{/assign}}
                            <div class="mb-1 mt-2 text-muted">
                                <span class="pointer font-weight-bold mr-2" onclick="event.preventDefault(); $(this).parent().next('div.issue-details').slideToggle(150)" title="Afficher plus de détails">#{{id}}</span><i class="far fa-calendar"></i> {{date}} <i class="far fa-clock ml-2"></i> {{hour}} par <span class=""><i class="far fa-meh-blank"></i> {{#fullName}}{{fullName}}{{/fullName}}{{^fullName}}{{creator}}{{/fullName}}</span> 
                                <ul class="report-tag ml-2">
                                    {{#tags}}
                                    <li><span class="badge badge-tag user-select-none" style="background-color:{{color}}; border:1px solid {{color}};color:#ffffff;"><i class="{{icon}}"></i> {{label}}</span></li>
                                    {{/tags}}
                                </ul>
                            </div>
                            <div class="issue-details">
                                <span> Page : <code title="Page concernée par le ticket">{{relativefrom}}</code></span><br>
                                <small class="text-muted mb-2 d-block">IP: {{ip}}</small>
                                <div>
                                    <span class="mr-1"><i class="{{browserIcon}}"></i> {{browser}}</span>|
                                    <span class="ml-1 mr-1"><i class="{{osIcon}}"></i> {{os}}</span>|
                                    <span class="ml-1"><i class="fas fa-tv"></i> {{width}}px * {{height}}px</span>
                                </div>
                                <small class="text-muted">{{browserVersion}}</small>
                            </div>
                        </div>
                        <div class="report-buttons">
                            {{#comments}}<i title="{{comments}} Commentaire(s)" class="fas fa-comment text-muted btn-comments"></i> {{comments}}{{/comments}}
                            {{#belongUser}}
                            <i class="far fa-trash-alt pointer btn-delete text-danger" title="Supprimer le ticket" onclick="event.preventDefault(); issue_issuereport_delete(this);"></i>
                            {{/belongUser}}
                        </div>
                    </a>
                </li>
            </ul>

             <!-- Pagination -->
            <ul class="pagination justify-content-center">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');issue_issuereport_search();">
                    <a class="page-link" href="#">{{label}}</a>
                </li>
            </ul>
        </div>
    </div>
</div>
