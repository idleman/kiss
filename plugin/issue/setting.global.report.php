<?php
global $myUser;
User::check_access('issue','configure');

?>

<div class="row">
    <div class="col-xl-12"><br>
    	<div onclick="issue_setting_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
        <h3>Réglages Tickets</h3>
        <div class="clear"></div>
		<hr>
    </div>
    <div class="col-xl-12">
	    <?php echo Configuration::html('issue'); ?>
    </div>
</div>
