<?php
/**
 * Define a issueevent.
 * @author Administrateur
 * @category Plugin
 * @license MIT
 */
class IssueEvent extends Entity{

	public $id;
	public $type; //Type (Texte)
	public $content; //Contenu (Texte Long)
	public $issue; //Issue (Entier)

	const TYPE_COMMENT = 'comment';
	const TYPE_STATE = 'state';
	const TYPE_ASSIGNATION = 'assignation';
	const TYPE_TAG = 'tag';

	protected $TABLE_NAME = 'issue_issue_event';
	public $fields =
	array(
		'id' => 'key',
		'type' => 'string',
		'content' => 'longstring',
		'issue' => array('type'=> 'int','label'=>'Ticket','link'=>'plugin/issue/IssueReport.class.php')
	);

	

	public $indexes = array('issue');

	public static function types($key=null){
		$types = array(
			TYPE_COMMENT => array('label' => 'Commentaire','icon' => 'fas fa-comment','color' => '#cecece'),
			TYPE_STATE => array('label' => 'Changement d\'état','icon' => 'fas fa-comment','color' => '#cecece'),
			TYPE_ASSIGNATION => array('label' => 'Nouvelle assignation','icon' => 'fas fa-comment','color' => '#cecece'),
			TYPE_TAG => array('label' => 'Modification tags','icon' => 'fas fa-comment','color' => '#cecece'),
		);
		$default =  array('label' => ' - ','icon' => 'fas fa-comment','color' => '#cecece');
		if(!isset($key)) return $types;

		return isset($types[$key]) ? $types[$key] : $default;
	}

	public function dir($relativePath = false){
		return ($relativePath ? '' : File::dir()).'issue'.SLASH.'attachments'.SLASH.$this->id;
	}

	public function remove(){
		self::deleteById($this->id);

		//supression des pieces jointes
		if(file_exists($this->dir())) delete_folder_tree($this->dir(),true);

	}
}
?>