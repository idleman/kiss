<?php
/**
 * Define a issuereport.
 * @author Administrateur
 * @category Plugin
 * @license MIT
 */
class IssueReport extends Entity{
	public $id,$browser,$browserVersion,$online,$os,$from,$width,$height,$ip,$history,$state,$assign,$followers;
	protected $TABLE_NAME = 'issue_report';
	public $fields =
	array(
		'id' => 'key',
		'browser' => 'string',
		'browserVersion' => 'string',
		'online' => 'boolean',
		'state' => 'string',
		'assign' => 'string',
		'os' => 'string',
		'from' => 'longstring',
		'width' => 'int',
		'height' => 'int',
		'history' => 'longstring',
		'followers' => 'longstring',
		'ip' => 'string'
	);

	public static function states($key=null){
		$states = array(
			'open' => array('icon'=>'far fa-clock','label'=>'Ouvert','color'=>'#3c3c3c'),
			'closed' => array('icon'=>'fas fa-check','label'=>'Résolu','color'=>'#2cbe4e'),
			'canceled' => array('icon'=>'fas fa-ban','label'=>'Annulé','color'=>'#cb2431'),
		);

		if(!isset($key)) return $states;
		return isset($states[$key]) ? $states[$key] : array(); 
	}


	public function remove($cascading = true){
		self::deleteById($this->id);
		if($cascading){
			require_once(__DIR__.SLASH.'IssueEvent.class.php');
			foreach(IssueEvent::loadAll(array('issue'=>$this->id)) as $event){
				$event->remove();
			}
		}
	}



	public function followers($followersArray = null){
		if(!isset($followersArray)) return explode(',', $this->followers);
		$this->followers = implode(',',$followersArray);
	}

	public function addFollower($login){
		$followers = $this->followers();
		$followers[] = $login;
		$followers = array_filter(array_unique($followers));
		$this->followers($followers);	
	}




}
?>