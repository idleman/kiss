<?php
//Cette fonction va generer une page quand on clique sur issue dans menu
function issue_page(){
	global $_;
	if(!isset($_['module']) || $_['module'] !='issue') return;
	$page = !isset($_['page']) ? 'sheet.report' : $_['page'];
	$file = __DIR__.SLASH.'page.'.$page.'.php';
	if(!file_exists($file)) throw new Exception("Page ".$page." inexistante");
	
	require_once($file);
}

function issue_modal(){
	global $myUser;
	if($myUser->connected()) require_once(__DIR__.SLASH.'modal.issue.report.php');
}

//Fonction executée lors de l'activation du plugin
function issue_install($id){
	if($id != 'fr.core.issue') return;
	Entity::install(__DIR__);
}

//Fonction executée lors de la désactivation du plugin
function issue_uninstall($id){
	if($id != 'fr.core.issue') return;
	Entity::uninstall(__DIR__);
}

//Déclaration des sections de droits du plugin
//Déclaration des sections de droits du plugin
Right::register('issue',array('label'=>'Gestion des droits sur le plugin issue'));


//Comprends toutes les actions du plugin qui ne nécessitent pas de vue html
require_once(__DIR__.SLASH.'action.php');


//Déclaration du menu de réglages
function issue_menu_setting(&$settingMenu){
	global $myUser;
	
	if(!$myUser->can('issue','configure')) return;
	$settingMenu[]= array(
		'sort' =>1,
		'url' => 'setting.php?section=global.report',
		'icon' => 'fas fa-angle-right',
		'label' => 'Issues'
	);
}

//Déclaration categorie de notification
function issue_notification_type(&$types){
	global $conf;
	$types['issue'] = array(
		'category' =>'Développement',
		'label' =>'Déclaration de tickets',
		'color' =>'#dc3545',
		'icon'  =>'fas fa-ticket-alt',
		'description' => "Notification lorsqu'un ticket est ouvert par un utilisateur",
		'default_methods' => array(
			'interface' => !empty($conf->get('issue_interface')) ? $conf->get('issue_interface') : true,
			'mail' => !empty($conf->get('issue_mail')) ? $conf->get('issue_mail') : true
		)
	);
}

//Déclaration des pages de réglages
function issue_content_setting(){
	global $_;
	if(file_exists(__DIR__.SLASH.'setting.'.$_['section'].'.php'))
		require_once(__DIR__.SLASH.'setting.'.$_['section'].'.php');
}

//Déclaration des settings de base
//Types possibles : text,select ( + "values"=> array('1'=>'Val 1'),password,checkbox. Un simple string définit une catégorie.
Configuration::setting('issue',array(
	"Général",
    'issue_report_mails' => array(
    	"label"=>"Destinataires des ouvertures de tickets",
    	"legend"=>"Utilisateurs ou rangs qui recevront la notification d'ouverture de nouveaux tickets",
    	"type"=>"user",
    	"placeholder"=>"eg. Administrateur",
    	"attributes"=>array(
    		"data-multiple"=>true,
    		"data-types"=>"user,rank"
    	)
    ),
));

function issue_menu_user(&$userMenu){
	global $myUser;
	if(!$myUser->can('issue','read')) return;

	$filters = filters_default(array(
		array('{{table}}.creator' => $myUser->login)
	));

	$userMenu[] = array(
		'sort' => 0.6,
		'icon' => 'fas fa-ticket-alt',
		'label' => 'Mes tickets',
		'url' => 'index.php?module=issue&page=list.issue&filters='.base64_encode(json_encode($filters))
	);
};


//Déclation des assets
Plugin::addCss("/css/main.css"); 
Plugin::addCss("/css/component.css"); 
Plugin::addJs("/js/main.js"); 
Plugin::addJs("/js/component.js"); 
Plugin::addJs("/js/html2canvas.min.js"); 

//Mapping hook / fonctions
Plugin::addHook("install", "issue_install");
Plugin::addHook("uninstall", "issue_uninstall"); 

Plugin::addHook("page", "issue_page");   
Plugin::addHook("application_bottom", "issue_modal");   
  
Plugin::addHook("menu_setting", "issue_menu_setting");    
Plugin::addHook("content_setting", "issue_content_setting");   
Plugin::addHook("menu_user", "issue_menu_user");
Plugin::addHook("notification_type", "issue_notification_type"); 
?>