<?php 
global $myUser;
User::check_access('issue','read');
require_once(__DIR__.SLASH.'IssueReport.class.php');
require_once(__DIR__.SLASH.'IssueReportTag.class.php');

$issuereport = IssueReport::provide();
if(!isset($issuereport->id)) throw new Exception("Impossible d'accéder à un rapport vierge");
$tags = IssueReportTag::tags();
$state = IssueReport::states($issuereport->state);

$osIcon = $browserIcon = 'fas fa-question-circle';
if(strlen($issuereport->os)>=3 && substr(strtolower($issuereport->os),0,3) == 'win') $osIcon = 'fab fa-windows text-primary';
if(strlen($issuereport->os)>=5 && substr(strtolower($issuereport->os),0,5) == 'linux') $osIcon = 'fab fa-linux text-danger';
if(strlen($issuereport->os)>=3 && substr(strtolower($issuereport->os),0,3) == 'mac') $osIcon = 'fab fa-apple text-secondary';

switch($issuereport->browser){
	case 'firefox': $browserIcon = 'fab fa-firefox text-warning'; break;
	case 'ie': $browserIcon = 'fab fa-internet-explorer text-danger'; break;
	case 'edge': $browserIcon = 'fab fa-edge text-primary'; break;
	case 'chrome': $browserIcon = 'fab fa-chrome text-success'; break;
}

$issueTags = array();
foreach(IssueReportTag::loadAll(array('report'=>$issuereport->id)) as $tag):
	$issueTags[] = $tag->tag;
endforeach;

?>
<div class="issue <?php echo !$myUser->can('issue','edit')?'readonly':''; ?>">
	<div id="issuereport-form" class="row issuereport-form  justify-content-md-center"  data-action="issue_issuereport_save" data-id="<?php echo $issuereport->id; ?>">
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-9 p-3">
					<h2 class="d-inline-block mb-0">Ticket #<?php echo $issuereport->id; ?></h2>
				</div>
				<div class="col-md-3 p-3">
					<a href="index.php?module=issue&amp;page=list.issue" class="btn btn-light w-100"><i class="fas fa-reply"></i> Revenir aux tickets</a>
				</div>
			</div>
			<div class="row">
				<!-- events -->
				<div class="col-md-9 issue-event-bar">
					<ul class="issue-events">
						<li class="issue-event issue-comment hidden {{classes}}" id="issue-comment-{{id}}" data-id="{{id}}">
							<div class="comment-header">
								<img class="avatar-medium avatar-rounded" src="{{avatar}}"> 
								<div class="header-infos">
									<span class="d-inline-block  font-weight-bold"> {{#fullName}}{{fullName}}{{/fullName}}{{^fullName}}{{creator}}{{/fullName}}</span> <br>
									<small class="text-muted">{{createdRelative}}</small> 	
								</div>
								<ul class="only-editable right">
									{{#edit_rights}}
									<li class="text-muted edit-issue-comment" onclick='issue_issue_event_edit(this);' title="Éditer le commentaire"><i class="fas fa-pencil-alt"></i></li>
									<li class="text-danger delete-issue-comment" onclick='issue_issue_event_delete(this);' title="Supprimer le commentaire"><i class="far fa-trash-alt"></i></li>
									{{/edit_rights}}
								</ul>
							</div>
							<div class="comment-message">
								{{{comment}}}
							</div>
							{{#hasfiles}}
							<div class="comment-attachments">
								<h5 class="text-uppercase">Pièces jointes : </h5>
								<div class="attachments">
								{{#files}}
									<a href="{{url}}"  class="comment-attachment attachment-{{type}}" target="_blank">
										<img class="attachment-image-view"  src="{{url}}">
										<div class="attachment-file-view"><i class="{{icon}}"></i> <h6 title="{{label}}">{{labelExcerpt}}</h6></div>
									</a>
								{{/files}}
								</div>
							</div>
							{{/hasfiles}}
						</li>
						
						<li class="issue-event issue-state hidden" data-id="{{id}}">
							<img class="avatar-mini avatar-rounded" src="{{avatar}}"> <span class="d-inline-block  font-weight-bold"> {{fullName}}</span>
							<i> a changé l'état du ticket </i> 
							<i> de </i><span class="badge badge-secondary" style="background: {{oldstate.color}}"><i class="{{oldstate.icon}}"></i> {{oldstate.label}}</span> 
							<i> en </i><span class="badge badge-secondary" style="background: {{state.color}}"><i class="{{state.icon}}"></i> {{state.label}}</span>
							<small class="text-muted"> - {{createdRelative}}</small> 	
						</li>
						
						<li class="issue-event issue-assignation hidden" data-id="{{id}}">
							<img class="avatar-mini avatar-rounded" src="{{avatar}}"> <span class="d-inline-block  font-weight-bold"> {{fullName}}</span>
							<i> a assigné le ticket à </i><img class="avatar-mini avatar-rounded" src="{{assigned.avatar}}"> <span class="d-inline-block  font-weight-bold"> {{assigned.fullName}}</span>
							<small class="text-muted"> - {{createdRelative}}</small> 
						</li>
						<li class="issue-event issue-tag hidden" data-id="{{id}}">
							<img class="avatar-mini avatar-rounded" src="{{avatar}}"> <span class="d-inline-block  font-weight-bold"> {{fullName}}</span>
							<i> a {{action}} le tag </i><span class="badge badge-secondary ml-1" style="background: {{tag.color}}"><i class="{{tag.icon}}"></i> {{tag.label}}</span> 
							<small class="text-muted"> - {{createdRelative}}</small> 	
						</li>
					</ul>


					<div id="issue-event-form" class="issue-event-form pb-0" data-action="issue_issue_event_save" data-id="">
						<div class="post-header">
							<div onclick="issue_issue_event_save(this);" class="btn btn-primary right"><i class="far fa-paper-plane"></i> Envoyer</div>
							<img class="avatar-mini avatar-rounded mr-1" src="<?php echo $myUser->getAvatar(); ?>"> 
							<span class="font-weight-bold"> <?php echo $myUser->fullName(); ?></span> 
							<small class="text-muted"><?php echo date('d/m/y - H:i'); ?></small> 	
							<div class="clear"></div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-8">
								<textarea id="content" data-type="wysiwyg" data-mention-user="user,rank" data-mention-object='issue_autocomplete' name="content" class="mt-0 mb-3 shadow-sm" placeholder="Répondre au sujet..."></textarea>
							</div>
							<div class="col-md-4">
								<input 
								   id="event-files"
			                       type="text"
			                       data-type="file"
			                       data-data='{"id":-1}'
			                       data-action="issue_attachments"
			                       data-label="Faites glisser vos pièces jointes ici" data-extension="jpeg,jpg,bmp,gif,png,xlsx,docx,pdf" class="component-file-default bg-white shadow-sm"/>
							</div>
						</div>
						
						 
					</div>
				</div>


				<!-- sidebar -->
				<div class="col-md-3 issue-sidebar">
					<h5 class="text-muted text-uppercase">Tags :</h5>
					<input type="text" data-tags='<?php echo json_encode($tags); ?>' value="<?php echo implode(',',$issueTags); ?>" class="issue-modal-tags user-select-none" data-type="tagcloud"  id="tags" onchange="issue_issuereport_meta_save()">
					<hr>
					<select class="w-100" data-type="dropdown-select" id="reportState" onchange="issue_issuereport_meta_save()">
						<?php foreach(IssueReport::states() as $slug=>$state): ?>
							<option <?php echo $slug==$issuereport->state ? 'selected="selected"': ''; ?> value="<?php echo $slug; ?>" style="background-color:<?php echo $state['color']; ?>;color:#ffffff;" data-icon="<?php echo $state['icon']; ?>"><?php echo $state['label']; ?></option>
						<?php endforeach; ?>
					</select>
					<hr>
					<h5 class="text-muted text-uppercase">Assignation :</h5>
					<input type="text" class="form-control" data-type="user" id="assign" value="<?php echo $issuereport->assign; ?>" onchange="issue_issuereport_meta_save()">
					<hr>
					<h5 class="text-muted text-uppercase">Suivi par :</h5>
					
					<ul class="issue-followers">
					<?php foreach($issuereport->followers() as $login):
						if(empty($login)) continue;
						$user = User::byLogin($login);
						?>
						<li data-user="<?php echo $user->login; ?>" title="<?php echo !empty($user->fullName()) ? $user->fullName() : $user->login; ?>"><img class="avatar avatar-mini avatar-rounded d-inline-block" src="<?php echo $user->getAvatar(); ?>" title="<?php echo $user->fullName(); ?>"></li>
					<?php endforeach; ?>
					</ul>
					
					<div onclick="issue_follow_toggle(this)" class="btn btn-light w-100 btn-following mt-2 <?php echo in_array($myUser->login, $issuereport->followers()) ? 'followed' : '' ?>">
						<i class="far"></i> <span></span>
					</div>
					
					<hr/>
					<h5 class="text-muted text-uppercase">Environnement :</h5>
					<ul class="list-group">
						<li class="list-group-item">  
							<i class="far fa-file-code" title="Page"></i>
							<a href="<?php echo str_replace(ROOT_URL,'',$issuereport->from); ?>" title="Accéder à la page" class="text-break"><?php echo str_replace(ROOT_URL,'',$issuereport->from); ?></a><br>
						</li>
						<li class="list-group-item">
							<i class="<?php echo $browserIcon; ?>" title="Navigateur"></i>
							<strong><?php echo $issuereport->browser; ?></strong><br>
							<small class="text-muted"><?php echo $issuereport->browserVersion; ?></small>
						</li>
						<li class="list-group-item"> 
							<i class="<?php echo $osIcon; ?>" title="Os"></i>
							<strong><?php echo $issuereport->os; ?></strong>
						</li>
						<li class="list-group-item">
							<i class="fas fa-tv" title="Écran"></i>
							<strong><?php echo $issuereport->width; ?>px * <?php echo $issuereport->height; ?>px</strong>
						</li>
						<li class="list-group-item">IP : 
							<strong><?php echo $issuereport->ip; ?></strong>
						</li>
					</ul>
					<?php 
					$histories = json_decode($issuereport->history,true);
					if($histories!=false): 
					?>
					<hr>
					<h5 class="text-muted pointer text-uppercase" title="Déplier/plier" onclick="$(this).next('.list-group').toggleClass('hidden')"><i class="fas fa-chevron-right"></i> 5 Actions AJAX :</h5>
					<ul class="list-group hidden">
						<?php  foreach ($histories as $history): 
							preg_match("/(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}:\d{2})/i", $history['time'], $matches);
							$date = isset($matches[1]) ? date("d/m/Y", strtotime($matches[1])) : '-';
							?>
							<li class="list-group-item">
								<small class="text-muted"><?php echo $date.' - '.(isset($matches[2])? $matches[2]: '-'); ?></small><br>
								<pre><?php print_r($history['request']); ?></pre>
							</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div><br>

