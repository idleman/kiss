<?php
/**
 * Define a IssueReportTag.
 * @author Administrateur
 * @category Plugin
 * @license MIT
 */
class IssueReportTag extends Entity{
	public $id,$tag,$report;
	protected $TABLE_NAME = 'issue_report_tag';
	public $fields = array(
		'id' => 'key',
		'tag' => 'string',
		'report' => 'int'
	);

	public $indexes = array('report');

	public static function tags($key=null){
		$tags = array(
			'bug' => array('icon'=>'fas fa-bug','label'=>'Bug','color'=>'#cb2431'),
			'feature' => array('icon'=>'fas fa-check','label'=>'Demande','color'=>'#2cbe4e'),
			'question' => array('icon'=>'far fa-question-circle','label'=>'Question','color'=>'#00BCD4')
		);

		if(!isset($key)) return $tags;
		return isset($tags[$key]) ? $tags[$key] : array(); 
	}
}
?>