function init_components_tagcloud(input){

	if(!input.data("data-component")) {
		
		var html = '<ul class="data-type-tagcloud" data-source="'+input.attr('id')+'">';
		var tags = input.data('tags');
		
		for(var key in tags){
			var tag = tags[key];
			html += '<li><span class="badge badge-tag" data-slug="'+key+'" data-color="'+tag.color+'"><i class="'+tag.icon+'"></i> '+tag.label+'</span></li>';
		}
		html += '</ul>';
		var picker = $(html); 
		input.before(picker);
		input.data("data-component",picker);


		
		picker.find('.badge-tag').click(function(){

			if(input.attr("readonly") == "readonly") return;

			var selected = input.val().split(',');

			//Supprimer les valeurs vides
			selected = selected.filter(function(e){return e});
			var element = $(this);
			if(!element.hasClass('active')){
				$(this).addClass('active').css({
					background : element.attr('data-color'),
					color : '#ffffff',
					border : '1px solid '+ element.attr('data-color')
				});
				selected.push(element.attr('data-slug'));
			} else {
				$(this).removeClass('active').css({
					background : '',
					color : '',
					border : ''
				});
				var index = selected.indexOf(element.attr('data-slug'));
				if(index > -1 ) selected.splice(index, 1);
			}
			input.val(selected.join(','));
			input.trigger('change').trigger('click');
		});
	

		if(input.parent().hasClass("input-group")){
			input.parent().after(input.detach());
			input.before('<div id="business-anchor" class="dropdown-anchor"></div>');
		}
		input.addClass('hidden');
	}else{
		picker = input.data("data-component");
	}
	//Si le champ input est ré-rempli, on met a jour les tags
	if(input.val()!=''){
		var selected = input.val().split(',');
		for(var key in selected){
			var tag = selected[key];
			var tagElement = picker.find('.badge-tag[data-slug="'+tag+'"]');
			tagElement.addClass('active').css({
				background : tagElement.attr('data-color'),
				color : '#ffffff',
				border : '1px solid '+ tagElement.attr('data-color')
			});
		}
	}
}