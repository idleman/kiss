<?php 
require_once(__DIR__.SLASH.'IssueReportTag.class.php');
$tags = IssueReportTag::tags();
?>

<div class="issue-declare-button d-flex noPrint" onclick="issue_add();" title="Déclarer un ticket"><i class="fas fa-ticket-alt m-auto"></i><i class="fas fa-cog fa-spin m-auto hidden"></i></div>
<!-- Modal -->
<div class="modal fade issue-report-modal" id="issue-report-modal" tabindex="-1" role="dialog" aria-labelledby="modal-issue-report-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-muted" id="modal-issue-report-label">NOUVEAU TICKET</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row issue-form">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="mr-2 bold-title">CATEGORIE</span>
                                <div class="d-inline-block">
                                    <input type="text" data-tags='<?php echo json_encode($tags); ?>' class="issue-modal-tags user-select-none" data-type="tagcloud">
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <span class="mr-2 bold-title">COMMENTAIRE<small class="text-muted"> - (eg. votre demande, étapes pour arriver à l'erreur, etc...)</small></span>
                                <textarea data-type="wysiwyg" data-mention-user="user,rank" data-mention-object='issue_autocomplete' id="issue-comment" class="mt-0"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <span class="d-inline-block mr-2 w-auto bold-title">FICHIERS</span>
                        <div onclick="issue_screenshot()" class="btn d-inline-block p-0 float-right"><i class="fas fa-camera-retro"></i> <small class="text-muted">Prendre une capture d'écran</small></div>

                       <!--  <div data-type="dropzone" data-label="Faites glisser vos fichiers supplémentaires ici" data-allowed="jpeg,jpg,bmp,gif,png,xlsx,docx,pdf" class="form-control mt-2" id="document" name="document"></div> -->

                       <input 
                       type="text"
                       data-type="file"
                       data-action="issue_attachments"
                       data-label="Faites glisser vos fichiers supplémentaires ici" data-extension="jpeg,jpg,bmp,gif,png,xlsx,docx,pdf" class="component-file-default bg-white shadow-sm" data-id="document" name="document"/>

                        <div class="issue-screenshot hidden"></div>
                        <h5 class="d-inline-block mr-2 mt-2 w-auto bold-title">INFORMATIONS</h5>
                        <ul  class="list-group">
                            <li class="list-group-item">
                                <div class="mb-1 font-weight-bold">Page :</div>
                                <u class="text-info default"><span class="page-link-issue" data-uid="from"></span></u>
                            </li>
                            <li class="list-group-item">
                                <div class="mb-1 font-weight-bold">Navigateur : </div>
                                <span data-uid="browser"></span> <small class="text-muted" data-uid="browserVersion"></small>
                            </li>
                            <li class="list-group-item">
                                <div class="font-weight-bold d-inline-block">Os : </div>
                                <span class="badge badge-primary badge-pill" data-uid="os"></span>
                            </li>
                            <li class="list-group-item">
                                <div class="font-weight-bold d-inline-block">Internet : </div>
                                <span class="badge badge-primary badge-pill" data-uid="online"></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="close-button btn btn-light" data-dismiss="modal">Fermer</button>
                    <button type="button" id="issue-send" class="btn btn-primary" onclick="issue_send(this);"><i class="far fa-paper-plane"></i> Envoyer</button>
                </div>
            </div>
        </div>
    </div>
</div>
