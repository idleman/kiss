<?php
global $conf,$myUser;

User::check_access('document','read');
require_once(__DIR__.SLASH.'Element.class.php');
?>

<div id="document-container" data-right-edit="<?php echo $myUser->can('document', 'edit')?'true':'false' ?>" data-right-delete="<?php echo $myUser->can('document', 'delete')?'true':'false' ?>" data-right-permission="<?php echo $myUser->can('document', 'configure')?'true':'false' ?>"></div>