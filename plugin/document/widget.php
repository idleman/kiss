<?php 
global $myUser;
try{
	User::check_access('document','read');
?>
<div class="widgetDocumentContainer">
	<div data-type="library" 
	data-root-label="C:"
	data-root="<?php echo $widget->data('widget-document-root'); ?>"  
	data-tree-panel="<?php echo $widget->data('widget-document-tree')=='1'? 'true':'false' ?>"  
	data-search-panel="<?php echo $widget->data('widget-document-search')=='1'? 'true':'false' ?>"  
	data-detail-panel="<?php echo $widget->data('widget-document-detail')=='1'? 'true':'false' ?>" 
	></div>
</div>

<?php
}catch(Exception $e){ ?>
 
 <div class="alert alert-warning" role="alert">
  <?php echo $e->getMessage(); ?>
</div>

<?php } ?>
