<div class="document-container<?php echo isset($embedded)?' embedded':'' ?>" 

	<?php 
	if(isset($_['data']) && is_array($_['data'])):
		foreach($_['data'] as $key=>$value): ?>
			data-<?php echo $key; ?>="<?php echo $value; ?>" 
		<?php endforeach;
	endif; ?>
	>
	

	<div class="tree-panel d-none d-md-block">
		<ul>
			<li class="pointer root-folder"><i class="fas fa-home"></i> <span></span></li>
		</ul>
		<ul class="tree-folders folders-container">
			<li class="hidden folder {{className}}" data-label="{{label}}" data-path="{{path}}"><i class="far fa-folder"></i> <span>{{label}}</span><ul class="folders-container"></ul></li>
		</ul>
	</div>

	<div class="file-panel">
		<div class="drag-overlay hidden">
			<div class="overlay-text"><i class="far fa-file"></i>&nbsp;&nbsp;Déposez vos fichiers ici.</div>
			<div class="overlay-icon"><i class="fas fa-arrow-alt-circle-down"></i></div>
		</div>
		<div class="breadcrumb-module">
			<ul>
				<li class="hidden" data-path="{{path}}" title="Cliquer pour naviguer dans le dossier">{{{label}}}</li>
			</ul>
		</div>
		<div class="search-module form-inline">
			<div class="doc-search-container">	
				<input class="form-control form-control-sm label-search" type="text" placeholder="">
				<span class="search-clear fas fa-times"></span>
				<span class="text-muted btn-search" title="Cliquer pour rechercher dans le dossier courant"><i class="fas fa-search"></i></span>
			</div>
			

			<div class="dropdown document-add-element">
				<button class="btn btn-small btn-primary dropdown-menu-button" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus"></i> Ajouter</button>
				<div class="dropdown-menu document-create-dropdown" aria-labelledby="dropdownMenuButton" style="width: 250px;">
					<a class="dropdown-item {{lineClass}} hidden pointer" >
						<div class="{{buttonClass}}"><i class='{{icon}}'></i> {{label}}</div>
						{{{afterHtml}}}
					</a>
				</div>
			</div>

			<ul class="view-module">
				<li class="hidden"  data-view="{{uid}}"><i title="{{label}}" class="{{icon}} {{class}}"></i></li>
			</ul>

		</div>

		<div class="file-module">
			<div class="file-preloader"><i class="fas fa-circle-notch fa-spin"></i></div>

			<div class="file-editor hidden">
				<div class="file-editor-header">
					<i class="far fa-file-alt ml-3 my-auto"></i> <input type="text" class="file-editor-name" value="Nouveau fichier.txt">
					<div class="btn btn-light btn-editor-expand" title="Agrandir"><i class="fas fa-expand-arrows-alt"></i></div>
					<div class="btn btn-light btn-editor-cancel"><i class="fas fa-ban"></i> Annuler</div>
					<div class="btn btn-primary btn-editor-save"><i class="far fa-check-circle"></i> Enregistrer</div>
					<div class="clear"></div>
				</div>
				<div class="file-editor-content">
					<textarea class="file-editor-input" placeholder="Aucun contenu pour le moment..."></textarea>
				</div>
			</div>

			<!-- Vue liste -->
			<table class="file-elements-list file-view hidden" data-view="list">
				<thead>
					<tr>
						<th data-sortable="label" class="name-head">Nom</th>
						<th data-sortable="creator" class="creator-head">Créateur</th>
						<th data-sortable="size" class="size-head">Taille</th>
						<th data-sortable="updated" class="updated-head">Modifié le</th>
					</tr>
				</thead>
				<tbody>
					<tr class="hidden file-element {{#link}}element-thumbnail-symlink{{/link}}" data-path="{{path}}" data-type="{{type}}" data-extension="{{extension}}" data-id="{{id}}" >
						<td class="name-cell"><img class="element-thumbnail" data-src="{{icon}}"/> 
							<span>
								<input type="text" class="rename-input hidden" value="">
								<span>{{label}}</span>
							</span><i title="Renommer" class="fas fa-pencil-alt element-rename"></i>
						</td>
						<td class="creator-cell">{{creator}}</td>
						<td class="size-cell">{{sizeReadable}}</td>
						<td class="updated-cell" title="{{updatedRelative}}">{{updatedReadable}}</td>
					</tr>
				</tbody>
			</table>
			<!-- Vue grille -->
			<ul class="file-elements-grid file-view hidden" data-view="grid">
				<li class="file-element hidden  element-type-{{extension}}  {{#link}}element-thumbnail-symlink{{/link}}" data-path="{{path}}" data-type="{{type}}" data-extension="{{extension}}" data-id="{{id}}">
					<div class="grid-container">
						<div class="element-thumbnail" style="background-image:url({{thumbnail}});"></div>
						<div  class="element-infos">
							<div class="name-cell">
								<span>
									<input type="text" class="rename-input hidden" value="">
									<span>{{label}}</span>
								</span><i title="Renommer" class="fas fa-pencil-alt element-rename"></i>
							</div>
							
							<span class="size-cell">{{#childNumber}}{{childNumber}} éléments{{/childNumber}} {{sizeReadable}}</span> <span class="creator-cell">par {{creator}}</span><br>
							<span class="updated-cell" title="{{updatedRelative}}"><i class="far fa-calendar"></i> {{updatedReadable}}</span>
						</div>
					</div>
				</li>
			</ul>

		</div>
	</div>

	<div class="detail-panel">
		<div class="detail-thumbnail">
			<div class="thumbnail-preloader"><i class="fas fa-circle-notch fa-spin"></i></div>
		</div>
		<h1 class="text-center">Aucun fichier sélectionné</h1>
		<small class="informations"><i class="fas fa-star"></i> <span>-</span></small>
		
		<ul class="detail-buttons" class="hidden">
			<li class="hidden {{lineClass}}">{{{html}}}{{^html}}<div class="btn {{buttonClass}}"><i class="{{icon}}"></i> {{label}}</div>{{/html}}</li>
		</ul>
	</div>


	<!-- upload Preloader -->
	<div class="preloader-upload-container hidden">
		<i class="fas fa-times preloader-upload-close"></i>
		<div class="preloader-upload">
			<div class="preloader-upload-shadow"></div><div class="preloader-upload-box"></div>
			<div class="upload-state"><h5>Envois en cours...</h5><span>x/n fichiers</span></div>
			
			<ul class="upload-files">
				<li class="hidden" data-sort="{{sort}}">
					<div>
						<h5>{{label}}</h5> <small>{{size}}</small> <span class="upload-file-state"></span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: 20%;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>



<!-- Modal Propriété -->
<div class="modal fade document-properties-modal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Propriétés</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

			</div>
			<div class="hidden modal-body-template">
				Libellé : <input class="form-control-plaintext form-control-sm text-info" readonly="readonly" type="text" value="{{label}}">
				Chemin relatif : <input class="form-control-plaintext form-control-sm text-info" readonly="readonly" type="text" value="{{path}}">
				Date de modification : <input class="form-control-plaintext form-control-sm text-info" readonly="readonly" type="text" value="{{updatedLabel}}">
				Propriétaire : <input class="form-control-plaintext form-control-sm text-info" type="text" readonly="readonly" value="{{creator}}">
				Dernier changement par : <input class="form-control-plaintext form-control-sm text-info" readonly="readonly" type="text" value="{{updater}}">
				Identifiant du document : <input class="form-control-plaintext form-control-sm text-info" readonly="readonly" type="text" value="#{{id}}">
				Adresse racine : <input class="form-control-plaintext form-control-sm text-info" type="text" onclick="$(this).select()" readonly="readonly" value="{{rootUrl}}">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>


</div>




