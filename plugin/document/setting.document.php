<?php 
global $myUser,$conf;
User::check_access('document','configure');
?>
<div class="row" id="document-setting">
	<div class="col-md-12">
		<br>
		<div class="btn btn-success float-right" onclick="document_setting_save()"><i class="fas fa-check"></i> Enregistrer</div>
		<h3>Réglages Documents</h3>
		<hr/>
		<?php echo Configuration::html('document'); ?>
	</div>
</div>
