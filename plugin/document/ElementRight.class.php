<?php
/**
 * Define a elementright.
 * @author Valentin CARRUESCO
 * @category Plugin
 * @license MIT
 */
class ElementRight extends Entity{
	public $id,$element,$entity,$uid,$read,$edit,$recursive;
	protected $TABLE_NAME = 'document_right';

	public $fields =
	array(
		'id' => 'key',
		'element' => array('type'=>'int','label'=>'Element','link'=>'plugin/document/Element.class.php'),
		'entity' => 'string',
		'uid' => 'string',
		'read' => 'int',
		'edit' => 'int',
		'recursive' => 'int'
	);
	public $indexes = array('element');
}
?>