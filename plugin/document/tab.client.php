<?php
global $_,$myUser,$conf;
User::check_access('client','read');
Plugin::need('client/Client');
require_once(__DIR__.SLASH.'Element.class.php');
$client = Client::getById($_['id']);


$relativeDirectory = empty($conf->get('document_client_directory')) ? 'Clients'.SLASH.'{{slug}}' : $conf->get('document_client_directory'); 
$relativeDirectory = str_replace('{{slug}}',$client->slug,$relativeDirectory);

$clientDirectory = Element::root().$relativeDirectory;
if(!file_exists($clientDirectory)) mkdir($clientDirectory,0755,true);
$element = Element::fromPath($clientDirectory);
$element->author = $myUser->login;
$element->save();

?>

<div class="p-2">
	<div style="background-color: #ffffff;"  data-type="library" 
	data-root-label="C:"
	data-root="<?php echo $relativeDirectory;?>"  
	data-tree-panel="false"  
	data-search-panel="true"  
	data-detail-panel="true" 
	></div>
</div>