<?php 
User::check_access('document','configure');

$treepanel = $widget->data('widget-document-tree');
$treepanel = $treepanel =="" ? 0 : $treepanel ;

$detailpanel = $widget->data('widget-document-detail');
$detailpanel = $detailpanel =="" ? 0 : $detailpanel ;

$searchpanel = $widget->data('widget-document-search');
$searchpanel = $searchpanel =="" ? 0 : $searchpanel ;

?>
<div id="document-widget-form">
	<label><input type="checkbox" data-type="checkbox" <?php echo $treepanel==1 ? 'checked="checked"' : '' ; ?> id="widget-document-tree"> Afficher le panneau arborescence</label><br>
	<label><input type="checkbox" data-type="checkbox" <?php echo $detailpanel==1 ? 'checked="checked"' : '' ; ?> id="widget-document-detail"> Afficher le panneau détail</label><br>
	<label><input type="checkbox" data-type="checkbox" <?php echo $searchpanel==1? 'checked="checked"' : '' ; ?> id="widget-document-search"> Afficher le panneau recherche</label>
	<div>
		<label>Afficher le chemin relatif suivant<small class="text-muted">- eg : dossier1/sous dossier</small> :</label>
		<input type="text" class="form-control form-control-sm" value="<?php echo $widget->data('widget-document-root'); ?>" id="widget-document-root" placeholder="dossier1/sous dossier">
	</div>
</div> 