function init_components_library(input){
	var data =  input.data();
	var doc = new DocumentApi(input);

	doc.options.panels.tree.visible = data.treePanel;
	doc.options.panels.search.visible = data.searchPanel;
	doc.options.panels.detail.visible = data.detailPanel;
	doc.options.root = data.root;
	doc.options.rootLabel = data.rootLabel;

	doc.load();
}
