

function init_plugin_document(){
	switch($.urlParam('page')){
		default:
		break;
	}

	var urlOptions = $.urlParam('data');
	
	if(urlOptions && urlOptions!='')
		urlOptions = JSON.parse(atob(urlOptions));
	
	var doc = new DocumentApi('#document-container',urlOptions);

	var data = $('#document-container').data();
	if(!data.rightPermission) doc.options.rightPermission = false;
	if(!data.rightDelete) doc.options.rightDelete = false;
	if(!data.rightEdit) doc.options.rightEdit = false;

	doc.load();
}


/** SETTINGS */
//Enregistrement des configurations
function document_setting_save(){
	$.action({ 
		action : 'document_setting_save', 
		fields :  $('#document-setting-form').toJson() 
	}, function(r){
		$.message('success','Enregistré');
	});
}


