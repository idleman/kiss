


var DocumentApi = function(element,options) {
			
			this.onEvents = {};
			this.currentView = 'list';
			this.currentFolder = '.';
			this.views = [];
			this.selected = [];
			this.isProcessing = false;

			this.editorOptions = {
		 		btns:  [
			 		['strong', 'em', 'underline','del'],
			 		['foreColor', 'backColor'],
			 		['undo', 'redo'], // Only supported in Blink browsers
			 		['formatting'],
			 		['superscript', 'subscript'],
			 		['link'],
			 		['insertImage'],
			 		['table'],
			 		['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			 		['unorderedList', 'orderedList'],
			 		['horizontalRule'],
			 		['removeformat'],
			 		['viewHTML']
				],
		 		lang: 'fr',
		 		allowBigPaste : true,
		 		autogrow: true,
		 		semantic: false
		 	};
			this.dom = {
				container :  $(element),
				editor : null,
				panels : {
					tree : null,
					detail : null,
					search : null,
					breadcrumb : null,
					files : null,
					workspace : null
				}
			}

			this.options = {
				rootLabel : 'C:', //Libellé de la racine
				root : '', //Racine visible des fichiers, on ne peut pas aller au dessus
				path : null, // dossier ouvert au lancement
				rightEdit : true,
				rightDelete : true,
				rightPermission : true,
				panels : {
					detail : { 
						visible : true,
						buttons : []
					},
					view : { 
						visible : true,
						buttons : []
					},
					tree : { 
						visible : true
					},
					files : { 
						visible : true
					},
					workspace : { 
						visible : true
					},
					search : { 
						visible : true,
						buttons : []
					},
					breadcrumb : { 
						visible : true
					}
				},
				actions : {
					template : 'document_load_template',
					element_search : 'document_element_search',
					element_tree_search : 'document_element_tree_search',
					folder_create : 'document_folder_create',
					element_rename : 'document_element_rename',
					element_delete : 'document_element_delete',
					element_move : 'document_element_move',
					element_share_edit : 'document_element_share_edit',
					element_preview : 'document_element_preview',
					element_execute : 'document_element_execute',
					element_upload : 'document_element_upload',
					element_edit : 'document_element_edit',
					element_save : 'document_element_save',
					properties_show : 'document_properties_show',
					right_search : 'document_right_search',
					right_save : 'document_right_save',
					right_delete : 'document_right_delete',
				}
			};		
			if(options) this.options = $.extend(this.options,options);

	}
	DocumentApi.prototype.load = function(callbacks){
			
		this.dom.container.html('Chargement...');
		var element = this.element;
		var object = this;

		
		object.options.panels.detail.buttons.push({
			buttonClass : 'btn-primary btn-download',
			icon : 'fas fa-arrow-alt-circle-down',
			label : 'TÉLÉCHARGER',
			visibility : {
				'type': ['file','directory']
			}
		});

		object.options.panels.detail.buttons.push({
			lineClass : 'properties-button directory-button',
			buttonClass : 'btn',
			icon : 'fas fa-align-justify',
			label : 'PROPRIÉTÉS',
			visibility : {
				'type': ['file','directory']
			}
		});

		object.options.panels.search.buttons.push({
			lineClass : 'upload-button',
			buttonClass : '',
			icon : 'far fa-file-alt',
			label : 'Envoyer un fichier',
			afterHtml : '<form class="box" method="post" action="action.php?action='+object.options.actions.element_upload+'" enctype="multipart/form-data"><input type="file" name="file[]"  multiple /></form>'
		});
		object.options.panels.search.buttons.push({
			lineClass : '',
			buttonClass : 'add-new-folder',
			icon : 'far fa-folder',
			label : 'Créer un dossier',
			afterHtml : '<div  class="new-folder-block hidden"> \
							<div class="input-group input-group-sm">\
								<input type="text" class="form-control form-control-sm folder-name" placeholder="Nom du dossier">\
								<div class="input-group-append">\
									<span class="btn btn-success"><i class="fas fa-check"></i></span>\
								</div>\
							</div>\
						</div>'
		});

		object.options.panels.search.buttons.push({
			lineClass : 'create-txt-button',
			buttonClass : '',
			icon : 'fas fa-pen text-secondary',
			label : 'Créer un fichier'
		});

		if(object.options.rightPermission){
			object.options.panels.detail.buttons.push({
				lineClass : 'share-button directory-button',
				buttonClass : 'btn',
				icon : 'fas fa-share',
				label : 'PERMISSIONS',
				visibility : {
					'type': ['file','directory']
				}
			});

			object.options.panels.detail.buttons.push({
				html : '<div class="right-panel hidden"><hr/>\
						<h6>PARTAGES</h6>\
						<div class="element-right-form">\
							<input class="uid" class="form-control" type="text" data-type="user" data-types="user,rank" placeholder="Rang ou utilisateur..." /><hr/>\
							<label><input class="read" type="checkbox" data-type="checkbox"> Lecture</label>\
							<label><input class="edit" type="checkbox" data-type="checkbox"> Écriture</label>\
							<label><input class="recursive" type="checkbox" data-type="checkbox"> Récursif</label>\
							<div class="btn btn-add-share btn-light"><i class="fa fa-plus"></i> Ajouter</div>\
						</div>\
						<ul class="table element-rights" >\
							<li data-id="{{id}}" class="hidden p-2">\
								{{{uid}}} <br>\
								<small class="text-muted p-0">\
									<span class="p-2">{{#read}}<i class="fa fa-check text-success"></i>{{/read}}{{^read}}<i class="fa fa-times text-secondary"></i>{{/read}} Lecture </span>\
									<span class="p-2">{{#edit}}<i class="fa fa-check text-success"></i>{{/edit}}{{^edit}}<i class="fa fa-times text-secondary"></i>{{/edit}} Écriture </span>\
									<span class="p-2">{{#recursive}}<i class="fa fa-check text-success"></i>{{/recursive}}{{^recursive}}<i class="fa fa-times text-secondary"></i>{{/recursive}} Récursif </span>\
									<i title="Supprimer la permission" class="fa fa-trash pointer right btn-delete-right" ></i>\
								</small>\
							</li>\
						</ul>\
						<hr/></div>',
				visibility : {
					'type': ['file','directory']
				}
			});
		}

		if(object.options.rightDelete){
			object.options.panels.detail.buttons.push({
				buttonClass : 'btn text-danger btn-element-delete',
				icon : 'far fa-trash-alt',
				label : 'SUPPRIMER',
				visibility : {
					'type': ['file','directory']
				}
			});			
		}

		object.dom.container.load('action.php?action='+object.options.actions.template,function(){
			object.dom.panels =  {
				tree : $('.tree-panel',object.dom.container),
				detail : $('.detail-panel',object.dom.container),
				search : $('.search-module',object.dom.container),
				breadcrumb : $('.breadcrumb-module',object.dom.container),
				files : $('.file-module',object.dom.container),
				workspace : $('.file-panel',object.dom.container)
			};


			if(object.options.root) $('.document-container').attr('data-root',object.options.root);
			if(object.options.panels['tree'].visible === false) object.hidePanel('tree');
			if(object.options.panels['detail'].visible  === false) object.hidePanel('detail');
			if(object.options.panels['search'].visible  === false) object.hidePanel('search');
			if(object.options.panels['files'].visible  === false) object.hidePanel('files');
			if(object.options.panels['workspace'].visible  === false) object.hidePanel('workspace');
			if(object.options.panels['breadcrumb'].visible  === false) object.hidePanel('breadcrumb');
			
			$('.root-folder span',object.dom.panels.tree).text(object.options.rootLabel);
			
			object.views.push({
				uid : 'list',
				icon : 'fas fa-align-justify',
				label : 'Vue liste',
				html : $('.file-view[data-view="list"]').get(0).outerHTML
			});

			object.views.push({
				uid : 'grid',
				icon : 'fas fa-th-large',
				label : 'Vue grille',
				html : $('.file-view[data-view="grid"]').get(0).outerHTML
			});

			//Chargement des vues disponibles
			object.loadViews();
			
			//Rendu de la vue courante & Chargement des fichiers
			object.renderView(object.options.path);

			

			//Chargement des boutons ajouter (dossier, fichier..)
			$('.document-create-dropdown .dropdown-item:not(.hidden)').remove();
			var tpl = $('.document-create-dropdown .dropdown-item.hidden').get(0).outerHTML;
			for(var k in object.options.panels.search.buttons){
				var data = object.options.panels.search.buttons[k];
				var button = $(Mustache.render(tpl,data));
				button.removeClass('hidden');
				$('.document-create-dropdown').append(button);
			}

			$('.root-folder',object.dom.container).addClass('folder-focused');

			$(document).off('keyup').on('keyup', function(e){
				switch(e.keyCode){
					case 46:
						if(!$('.file-view tr').hasClass('element-focused') || !object.options.rightDelete) break;
						object.element_delete();
					break;
					case 113:
						if(!object.options.rightEdit) break;
						if($('.file-view tr').hasClass('element-focused'))
							object.element_rename_edit($('.element-rename',object.selected[0].element));
					break;
					case 13:
						if($('.label-search',object.dom.panels.search).is(":focus"))
							object.element_search();
					break;
				}
			});

			$('.file-view:visible').sortable_table({
				onSort : function(){object.element_search();}
			});

			//RESET KEYWORD
			$(".label-search").on('keyup',function(){
				if ($(this).val() != ''){
					$('.doc-search-container').addClass('typing');
				}else{
					$('.doc-search-container').removeClass('typing');
				}
			});
			
			$(".search-clear").click(function(){
				$('.doc-search-container').removeClass('typing');

				$(".label-search").val('');
				$(".label-search").focus();
				object.element_search();
			});
			$('div.file-panel').on('click',function(e){
				if(e.target == (this)) {
					$('.file-view:visible tr').removeClass('element-focused');
					object.reset_preview();
				}
			});

			$(object.dom.panels.tree).panelResize({
				minWidth : 100,
				stored : true
			});
			$(object.dom.panels.detail).panelResize({
				minWidth : 100,
				direction : 'left',
				stored : true
			});

			// Mapping des events DOM / objet
			$('.root-folder',object.dom.panels.tree).click(function(event){object.element_search('.');});
			$('.btn-search',object.dom.panels.search).click(function(){object.element_search();});
			$('.add-new-folder',object.dom.panels.search).click(function(event){  object.new_folder(event); });
			$('.new-folder-block .input-group-append',object.dom.panels.search).click(function(){object.folder_create()});

			$('.detail-buttons',object.dom.panels.detail).on('click','.btn-download',function(){object.element_download(null,true)});
			$('.detail-buttons',object.dom.panels.detail).on('click','.share-button',function(){object.right_toggle()});
			$('.detail-buttons',object.dom.panels.detail).on('click','.properties-button',function(){object.properties_show()});

			$('.detail-buttons',object.dom.panels.detail).on('click','.element-right-form .btn-add-share',function(){object.right_save()});
			$('.detail-buttons',object.dom.panels.detail).on('click','.btn-delete-right',function(){object.right_delete(this)});
			$('.detail-buttons',object.dom.panels.detail).on('click','.btn-element-delete',function(){object.element_delete();});

			$('.tree-folders',object.dom.panels.tree).on('click','li',function(event){object.folder_toggle(this,event);});
			
			//editeur
			$('.dropdown-menu-button',object.dom.panels.search).click(function(){$('.file-element').removeClass('element-focused');});
			$('.create-txt-button',object.dom.panels.search).click(function(){object.element_edit();});
			$('.btn-editor-cancel',object.dom.panels.file).click(function(){ $('.file-editor',object.dom.panels.file).addClass('hidden'); });
			$('.btn-editor-save',object.dom.panels.file).click(function(){object.element_edit_save();});
			$('.btn-editor-expand',object.dom.panels.file).click(function(){object.element_edit_expand();});

			if(!object.options.rightEdit){
				$('.document-add-element',object.dom.panels.file).addClass('hidden');
			}else{
				object.init_upload();
			}
			object.triggerEvent('loaded');
		});
	}

	DocumentApi.prototype.triggerEvent = function (event,options){
		if(!this.onEvents[event]) return;
		for(var key in this.onEvents[event]){
			this.onEvents[event][key](options);
		}
	}
	DocumentApi.prototype.hidePanel = function (panel){
		var object = this;
		object.options.panels[panel].visible = false;
		
		if(object.dom.panels[panel]!=null) object.dom.panels[panel].addClass('hidden');
	}
	DocumentApi.prototype.showPanel = function (panel){
		var object = this;
		object.options.panels[panel].visible = true;
		if(object.dom.panels[panel]!=null) object.dom.panels[panel].removeClass('hidden');
	}


	//Récuperation d'une liste de element dans le tableau elements
	DocumentApi.prototype.element_search = function (folderPath){
		
		var object = this;
	
		if(object.isProcessing) return;

		$('.file-preloader',object.dom.panels.files).removeClass('hidden');

		var keyword = $('.label-search',object.dom.panels.search).val();

		//tri
		var sort = {};
		var sortElement = $('.file-view:visible th[data-sortable][data-sort]',object.dom.panels.files);

		if(sortElement.length!=0 && sortElement.attr('data-sort')!=""){
			sort.sortable = sortElement.attr('data-sortable');
			sort.sort = sortElement.attr('data-sort');
		}

		var data = {
			/*async: false,*/
			action: object.options.actions.element_tree_search,
			keyword : keyword,
			sort : sort,
			root :object.options.root,
			folder : folderPath ? folderPath : object.currentFolder
		}
		
		data.folder = data.folder == '.' ? '': data.folder;


		
		//gestion de l'arborescence de gauche (tree)
		$.action(data,function(r){
			var tree = $('ul.tree-folders',object.dom.panels.tree);
	  		var tpl = $('li:not(:visible)',tree).get(0).outerHTML;
	  		tree.find('li:visible').remove();
	  		//On n'affiche l'arborescence que si on est pas en mode recherche
	  		if(!data.keyword || data.keyword ==''){
	  			var selected = folderPath ? folderPath : '' ;
	  			if(selected.length>=2 && selected.substring(0,2) == './') selected = selected.substring(2);
	  			for(var k in r.tree){
		  			var path = r.tree[k];
		  			var parts = path.split('/');
		  			var recipient =  tree.parent();
		  			for(var i in parts){
		  				var part = parts[i];
		  				var path = parts.slice(0,parseInt(i)+1).join('/');
		  				var element = $('> .folders-container > li[data-path="'+path+'"]',recipient);
		  				if(element.length ==0){
		  					var classes = '';
		  					if(selected==path) classes+= ' folder-focused ';
		  					if(selected && (selected.indexOf(path+'/')!==-1 || selected==path) ) classes+= ' folder-open ';
		  					var row = {
		  						path : path,
		  						label : part, 
		  						className : classes 
		  					};
		  					var element = $(Mustache.render(tpl,row));
		  					element.removeClass('hidden');
		  					recipient.find('> .folders-container').append(element);
		  				}
		  				recipient = element;
		  			}
		  		}
	  		}
			
		});
		

		data.action = object.options.actions.element_search;

		object.isProcessing = true;
		$('.file-view',object.dom.panels.files).fill(data,function(response){

			object.currentFolder = data.folder;
			$('.file-preloader',object.dom.panels.files).addClass('hidden');

			object.triggerEvent('searched',{data:data,response:response});

			object.isProcessing = false;


			var breadcrumb = null;
			if(data.folder) breadcrumb = data.folder.split('/');

			//Fil d'arianne
			object.breadcrumb_render(breadcrumb);

			//reset panel detail
			object.reset_preview();
		

			//EVENTS

			//Prévisualisation
			$('.file-view:visible .file-element',object.dom.panels.files).off('click').click(function(event){ object.element_preview(this, event);  });
			//Ouverture fichier / dossier
			$('.file-view:visible .file-element',object.dom.panels.files).off('dblclick').dblclick(function(event){ object.element_execute(this);  });
			

			//Renommage
			$('.file-view:visible .file-element .element-rename',object.dom.panels.files).click(function(event){ 
				event.stopPropagation();
	 			event.preventDefault();
				object.element_rename_edit(this);
			});
			//Validation du renommage
			$('.rename-input',object.dom.panels.files).bind('blur keyup', function(e){
				var input = $(this);
				var text = input.next('span');
				var label = text.text();
				if(e.type === 'keyup'){
					//adapte la largeur en temps réel de l'input à  la largeur du texte avec un max de 320 px et un min de 45px
			 		var width = Math.min(Math.max( input.val().length *5.5 ,45),320);
			 		$(this).width(width+15);
				}

				if (e.type !== 'blur' && e.keyCode !== 13) return;
				
				var tr = input.closest('.file-element');
				var button = tr.find('.element-rename');

				var newLabel = input.val().replace(/\s+$/, '');
			
				//Les dossiers ne peuvent finir par un .
				if(newLabel.slice(-1) == '.' && tr.attr('data-type') =='directory') newLabel = newLabel.slice(0,-1);

				//Check si le label a changé ou non
				if(newLabel == label){
					text.text(label).removeClass('hidden');
					input.addClass('hidden');
					button.removeClass('hidden');
					return;
				}

				//Attribution nouveau label
				text.text(newLabel).removeClass('hidden');
				input.addClass('hidden');
		 		
				$.action({
					action : object.options.actions.element_rename,
					path : tr.attr('data-path'),
					label : newLabel
				}, function(r){
					
					button.removeClass('hidden');
					$('.tree-folders li[data-path="'+ tr.attr('data-path')+'"]').attr('data-path',r.element.path).find('span').text(newLabel);
					tr.attr('data-path', r.element.path);
					object.triggerEvent('renamed',{
						path : tr.attr('data-path'),
						old_label : label,
						label : newLabel
					});

				}, function(r){
					text.text(label);
					button.removeClass('hidden');
				});
				
			});
			
			//Maj du placeholder de la recherche
	 		$('.label-search').attr('placeholder','Rechercher'+(breadcrumb ? ' dans '+breadcrumb[breadcrumb.length-1]:''));


	 		if(!object.options.rightEdit){
				$('.element-rename',object.dom.panels.file).addClass('hidden');
			}else{

		 		//Drag & Drop fichiers
		 		var viewElement = $(".file-view:visible");
		 		var droppableContainer = viewElement.get(0).tagName == 'TABLE' ?  ' tbody': '';
				$(".file-view:visible"+droppableContainer,object.dom.panels.files).sortable({
					distance: 5,
					opacity: 0.75,
					start: function(e, ui){

						var placeholder = ui.item.clone().addClass('original-placeholder').removeAttr('style');
						ui.item.after(placeholder);
					},
					stop: function(e, ui){
						$('.original-placeholder').remove();
						$(e.originalEvent.target).one('click', function(e){
							e.stopImmediatePropagation(); 
						});
					}
				}).disableSelection();

				$(".file-view:visible .file-element",object.dom.panels.files).droppable({
					tolerance: "pointer",
					hoverClass: "folder-receive-element",
					drop: function(event, ui) {
						var from = $(ui.draggable["0"]);
						var to = $(this);
						if(!from.attr('data-path')) return;
						if(to.attr('data-type') != 'directory') return;
						$.action({
							action : object.options.actions.element_move,
							from : from.attr('data-path'),
							to : to.attr('data-path'),
						}, function(response){
							if (response.element !== null && typeof response.element === 'object') {
								object.triggerEvent('moved',{from:from,to:to,response:response});
								from.remove();
								object.remove_treeline(from);
								object.reset_preview();
							} else {
								alert('Action impossible, un élément existe déjà avec ce nom dans le dossier.');
							}
						});
					}
				});

				//Drag & drop arborescence
				$("li",object.dom.panels.tree).droppable({
					tolerance: "pointer",
					greedy: true,
					over: function(e, ui){
						var to = $(this);
						var toPath = to.hasClass('root-folder') ? '.' : to.attr('data-path');
						to.addClass( to.hasClass('root-folder') ?'folder-focused':'folder-hover');
					},
					out: function(e, ui){
						var to = $(this);
						if( to.hasClass('root-folder') && to.hasClass('folder-focused')) 
							to.removeClass('folder-focused');
						if(to.hasClass('folder-hover')) 
							to.removeClass('folder-hover');
					},
					drop: function(event, ui) {
						var from = $(ui.draggable["0"]);
						var fromPath = from.attr('data-path');
						var to = $(this);

						var toPath = to.hasClass('root-folder') ? '.' : to.attr('data-path');

						if( to.hasClass('root-folder') && to.hasClass('folder-focused'))
							to.removeClass('folder-focused');
						if(to.hasClass('folder-hover'))
							to.removeClass('folder-hover');
						$('.tree-panel li').removeClass('folder-hover');
						if(fromPath == toPath) {
							$.message('error', 'Impossible de déplacer un dossier dans lui-même.');
							return;
						}
						$.action({
							action : object.options.actions.element_move,
							from : fromPath,
							to : toPath,
						}, function(response){
							if (response.element !== null && typeof response.element === 'object') {

								object.triggerEvent('moved',{from:from,to:to,response:response});

								// Suppression de la ligne dans le panneau arborescence
								object.remove_treeline(from);
								// Ajout d'une ligne dans le panneau de l'arborescence
								if(from.attr('data-type') == 'directory'){
									var fromLabel = $('span>span', from).text();
									var tpl = $('.tree-folders li:not(:visible)',object.dom.panels.tree).get(0).outerHTML;
									var toContainer = toPath == '.' ? $('ul.tree-folders',object.dom.panels.tree) : $('>ul.folders-container', to);
									toContainer.parent().addClass('folder-open');
									$('i.far.fa-folder',toContainer.parent()).removeClass('fa-folder').addClass('fa-folder-open');
									var li = $(Mustache.render(tpl,{label:fromLabel,path:toPath+'/'+fromLabel}));
									li.removeClass('hidden');
									toContainer.append(li);
								}
								// Suppression de la ligne dans le panneau d'élément
								from.remove();
								// Reset de la preview
								object.reset_preview();
							}
							else
								alert('Action impossible, un élément existe déjà avec ce nom dans le dossier.');
						});
					}
				});
			}
		});
	}


	//Draw du fil d'Ariane
	DocumentApi.prototype.breadcrumb_render = function (breadcrumb){

		var object = this;
		if(!object.dom.panels.breadcrumb) return;

		if(breadcrumb === null) breadcrumb = ['.'];
		
		if(breadcrumb[0]!='.') breadcrumb.unshift('.');
		
		$('ul li:visible',object.dom.panels.breadcrumb).remove();

		var tpl = $('li:not(:visible)',object.dom.panels.breadcrumb).get(0).outerHTML;

		for(var key in breadcrumb){
			var label = breadcrumb[key];
			if(label == '.') label = '<i class="fas fa-home"></i>';
			var currentpath = breadcrumb.slice(0,key);
			currentpath.push(breadcrumb[key]);
			currentpath = currentpath.join('/');
			var li = $(Mustache.render(tpl,{label:label,path:currentpath}));
			if(key == breadcrumb.length-1) li.attr('title', 'Dossier courant');
			$('ul',object.dom.panels.breadcrumb).append(li);
			li.removeClass('hidden');
		}
		$('ul > li',object.dom.panels.breadcrumb).click(function(){
			object.element_search( $(this).attr('data-path'));
		});
	}

	

	/** DOSSIER */
	DocumentApi.prototype.folder_toggle = function (element, event, force){
		var object = this;
		$('.label-search').val('').blur();
		var li = $(element).closest('li');
		if(li.hasClass('folder-focused')){
			var parent = li.parent().parent();
			object.element_search(parent.attr('data-path') == null ? '.' : parent.attr('data-path'));
		}else{
			object.element_search(li.attr('data-path'));
		}
		if(event) event.stopPropagation();
	}




	// Reset du panneau de preview de la GED
	DocumentApi.prototype.reset_preview = function(){
		var object = this;
		$('.detail-thumbnail',object.dom.panels.detail).removeAttr('style');
		$('> h1',object.dom.panels.detail).text('Aucun fichier sélectionné');
		$('> small > span',object.dom.panels.detail).text('-');
		$('.detail-buttons',object.dom.panels.detail).addClass('hidden');
	}


	

	// Preloader affiché durant l'upload
	DocumentApi.prototype.toggle_preloader_upload = function(){
		var object = this;
		var preloader = $('.preloader-upload-container');
		if(!preloader.is(':visible')) {
			preloader.removeClass('hidden');
			preloader.find('.preloader-upload-close').unbind('click').click(function(){
				object.toggle_preloader_upload();
			});
			$('body').css('overflow', 'hidden');
		} else {
			$('body').css('overflow', '');
			preloader.addClass('hidden');
		}
	}


	// Init de l'upload
	DocumentApi.prototype.init_upload = function(){
		var object = this;
		var form = $('.upload-button form',object.dom.container);
		var input = form.find('input[type="file"]');
		var zone = $('.file-panel');
		var droppedFiles = null;
		var noBlink = null;
		var fileQueue = [];

		//Upload des images dans le presse papier
		window.addEventListener("paste", function(event){
			var clipboardData = event.clipboardData || window.clipboardData;
			
		   if(clipboardData.getData('Text').length) return;

			if(event.clipboardData == false || event.clipboardData.items == undefined) return;
		   var items = event.clipboardData.items;

		   if(items.length == 0) return;
		   fileQueue = [];
			for(var i = 0; i < items.length; i++){
				var f = items[i];
				if (f.type.indexOf("image") == -1) return;
				var file = items[i].getAsFile();

				fileQueue.push({
					method : 'paste',
					file : file
				});
			}
			form.trigger('submit');		   
		}, false);


		input.addClass('hidden');
		$('.upload-button > div',object.dom.container).click(function(e){
			input.trigger('click');
			e.preventDefault();
			e.stopPropagation();
		});

		input.on('change', function (e) {
			fileQueue = [];
			for(var k = 0; k < this.files.length; k++){
				if(this.files[k].name == null) break;
				fileQueue.push({
					method : 'browse',
					file : this.files[k]
				});
			}
			form.trigger('submit');
		});

		zone.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
			e.preventDefault();
			e.stopPropagation();
		})
		.on('dragover dragenter', function (e) {
			clearTimeout(noBlink);
			$('.drag-overlay').removeClass('hidden');
			zone.addClass('drag-over');
			e.preventDefault();
			e.stopPropagation();
		})
		.on('dragleave dragend drop', function (e) {
			noBlink = setTimeout(function(){
				$('.drag-overlay').addClass('hidden');
				zone.removeClass('drag-over');
			},500);

			e.preventDefault();
			e.stopPropagation();
		})
		.on('drop', function (e) {
		   fileQueue = [];
			if(!e.originalEvent.dataTransfer) return;
			droppedFiles = e.originalEvent.dataTransfer.files;
		
			for (var i=0, f; f=droppedFiles[i]; ++i) {

				if(f.name.indexOf(".")==-1 && f.size%4096 == 0) {
					$.message('error', 'Impossible d\'envoyer un dossier.');
					return;
				}
				fileQueue.push({
					method : 'drop',
					file : droppedFiles[i]
				});
			}
			form.trigger('submit');			
		});

		form.on('submit', function (e) {
			$('.drag-overlay').addClass('hidden');
			e.preventDefault();  
			var tpl = $('.upload-files li.hidden').get(0).outerHTML;
			$('.upload-files li:not(.hidden)').remove();
		
			var fileProcessed = [];

			object.toggle_preloader_upload();
			for( var k in fileQueue){
				var file = fileQueue[k].file;
				
				var line = $(Mustache.render(tpl,{
					label : file.name,
					sort : k,
					size : readable_size(file.size)
				}));
				line.removeClass('hidden');
				$('.upload-files').append(line);

				
				var ajaxData = new FormData();

				ajaxData.append(input.attr('name'), file);
				ajaxData.append('method', fileQueue[k].method);

				var path = object.currentFolder =='.' ? '' : object.currentFolder;
				if(path== '' && object.options.root!='' ) path = object.options.root;

				ajaxData.append('path',  path);
				ajaxData.append('sort', k);

				$.ajax({
					url: form.attr('action'),
					type: form.attr('method'),
					data: ajaxData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					complete: function (data) {},
					success: function (response) {

						if(!response.error)
							$('.upload-files li[data-sort="'+response.sort+'"] .upload-file-state').addClass('success').html('<i class="far fa-check-circle"></i> Succès');
						else
							$('.upload-files li[data-sort="'+response.sort+'"] .upload-file-state').addClass('error').html('<i class="fas fa-exclamation-circle"></i> Erreur :'+response.error);
						
						fileProcessed.push(response);

						$('.preloader-upload-container .upload-state span').text(fileProcessed.length+'/'+fileQueue.length+' fichiers envoyés');

						if(fileQueue.length==fileProcessed.length){
							object.triggerEvent('uploaded',{data:fileProcessed});
							$('.document-create-dropdown').removeClass('show');
							object.element_search();
							//On cache automatiquement le gestionnaire d'upload si aucune erreure.
							if($('.upload-file-state.error').length == 0){
								setTimeout(function(){
									var preloader = $('.preloader-upload-container');
									$('body').css('overflow', '');
									preloader.addClass('hidden');
								},1500);
							}
						}
					},
					error: function (data) {
						$('.document-create-dropdown').removeClass('show');
						$.message('error',data);
					},
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						xhr.sort = k;
						
						xhr.upload.addEventListener("progress", function(evt){
							
							if (evt.lengthComputable) {
							var percentComplete = (evt.loaded / evt.total) * 100;
							percentComplete = Math.round(percentComplete * 100) / 100;

							var progressBar = $('.upload-files li[data-sort="'+xhr.sort+'"] .progress-bar')
								.css('width',percentComplete+'%')
								.text(percentComplete+'%')
								.attr('aria-valuenow',percentComplete);
					
					
							}
						}, false);
						return xhr;
					}
				});
			}
		});
	}


	 DocumentApi.prototype.folder_create = function(){
	 	var object = this;
	 	var path = object.currentFolder =='.' || object.currentFolder =='' ? '' : object.currentFolder+'/';
		if(path== '' && object.options.root!='' ) path = object.options.root+'/';
	 	path += $('.folder-name').val();
	 	var data = {
	 		action : object.options.actions.folder_create,
	 		path : path,
	 		folder : $('.folder-name',object.dom.panels.search).val()
	 	}
	 	$.action(data,function(response){
	 		object.triggerEvent('folder-created',{path:data.path,folder:data.folder,response:response});
	 		$('.folder-name',object.dom.panels.search).val('').focus();
	 		$('.new-folder-block',object.dom.panels.search).toggleClass('hidden');
	 		$('.dropdown-menu',object.dom.panels.search).trigger('click');
	 		object.element_search();
	 	});
	 }

	 DocumentApi.prototype.new_folder = function(event){
	 	var object = this;
	 	$('.new-folder-block',object.dom.panels.search).toggleClass('hidden');
	 	$('.new-folder-block input',object.dom.panels.search).focus().enter(function(){object.folder_create()});
	 	event.stopPropagation();
	 }


	 /** ELEMENT **/
	 DocumentApi.prototype.element_rename_edit = function(button){
	 	var object = this;
	 	var button = $(button);
	 	var span = button.prev('span');
	 	var text = span.find('span');
	 	var tr = span.closest('.file-element');
	 	var label = span.find('span').text();
	 	var input = $('.rename-input',tr);

	 	//adapte la largeur de l'input à  la largeur du texte avec un max de 320 px et un min de 45px
	 	var width = Math.min(Math.max(text.width(),45),320);
	 	
		button.addClass('hidden');
		text.addClass('hidden');

		input.removeClass('hidden')
			.focus()
			.val(label)
			.width(width+15)
			.click(function(event){
				event.stopPropagation();
			});

	 }



	// Met à jour l'arborescence (supprime la line dans l'arborescence sur move ou rename)
	DocumentApi.prototype.remove_treeline = function(from){
		var parent = $('.folder-focused ul');
		if(parent.length == 0) parent = $('.tree-panel > ul');
		$('li',parent).each(function(i,li){ 
			var li = $(li);
			if(li.attr('data-path') == from.attr('data-path')) {
				li.remove();
			}
		});
	}



	//Récuperation d'une liste de element dans le tableau elements
	DocumentApi.prototype.element_preview = function(element, event){
		var object = this;
		var line = $(element);

		console.log('preview');

		if(!event.ctrlKey) object.selected = [];

		object.selected.push({
			element : line,
			path : line.attr('data-path'),
			type : line.attr('data-type'),
			extension : line.attr('data-extension'),
			id : line.attr('data-id')
		});

		console.log(object.selected);

		$('.file-view:visible .file-element',object.dom.panels.files).removeClass('element-focused');
			
		for(var k in object.selected){
			object.selected[k].element.addClass('element-focused');
		}

		if(line.hasClass('element-focused')) return;

		if(object.selected.length==1){
			$('.right-panel',object.dom.panels.detail).addClass('hidden');
			$('.detail-buttons',object.dom.panels.detail).removeClass('hidden');
			

			if(object.selected.type =='directory') {
				$('.detail-buttons > .directory-button',object.dom.panels.detail).removeClass('hidden');
			}else{
				$('.detail-buttons > .directory-button',object.dom.panels.detail).addClass('hidden');
			}

			$('.detail-buttons > li:visible',object.dom.panels.detail).remove();
			var buttonTemplate = $('.detail-buttons > li:eq(0).hidden').get(0).outerHTML;
			if(object.options.panels.detail.buttons){
				for(var i in object.options.panels.detail.buttons){
					var data = object.options.panels.detail.buttons[i];
					if(data.visibility && data.visibility.type && data.visibility.type.indexOf(object.selected.type) == -1 ) continue;
					if(data.visibility && data.visibility.extension && data.visibility.extension.indexOf(object.selected.extension) == -1 ) continue;
					
					var button = $(Mustache.render(buttonTemplate,data));
					button.removeClass('hidden');
					$('.detail-buttons').append(button);
				}
			}

			//autoblur sur le rename d'un fichier en cours si existant
			var currentRename = $('.file-view:visible .file-element .rename-input:visible');
			if(currentRename.length>0) currentRename.trigger('blur');
			
	

			$('.detail-thumbnail .thumbnail-preloader',object.dom.panels.detail).addClass('show');

			$.action({
				action : object.options.actions.element_preview,
				path : line.attr('data-path'),
			},function(response){

				setTimeout(function(){
					$('.detail-thumbnail .thumbnail-preloader',object.dom.panels.detail).removeClass('show');
				},100);

				$('.detail-thumbnail',object.dom.panels.detail).css('background-image','url('+response.row.thumbnail+')');

				var substrings = ['.jpg','.jpeg','.bmp','.gif','.png','.jfif'];
				$('.detail-thumbnail',object.dom.panels.detail).css('background-size','');
				if (new RegExp(substrings.join("|")).test(response.row.thumbnail))
					$('.detail-thumbnail',object.dom.panels.detail).css('background-size','contain');

				$('> h1',object.dom.panels.detail).html(response.row.label).text();
				var size = response.row.sizeReadable;
				if(response.row.type == 'directory') size = response.row.childNumber+' élement'+(response.row.childNumber>1?'s':'');

				$('> small > span',object.dom.panels.detail).text(size+", "+response.row.updatedRelative);

				object.triggerEvent('selected',object.selected);

			});
		}
	}

	//Suppression d'un élément de la GED
	DocumentApi.prototype.element_delete = function(){

		var object = this;

		if(object.selected.length == 0) return $.message('info', 'Sélectionnez d\'abord un élément à supprimer');
		if(!confirm('Êtes-vous sûr de vouloir supprimer '+(object.selected.length)+' élément ?')) return;

		for(var k in object.selected){

			var select = object.selected[k];

			if($('span .rename-input:visible',select.element).length) continue;

			$.action({
				action : object.options.actions.element_delete,
				path : select.path,
			},function(response){

				object.triggerEvent('deleted',{
					element : select.element,
					response : response,
					path : select.path,
				});

				select.element.remove();
				object.reset_preview();
				object.remove_treeline(select.element);
			});
		}
	}

	DocumentApi.prototype.element_share_edit = function(){
		var object = this;
		if(object.selected.length==0) return $.message('info', 'Sélectionnez d\'abord un élément à partager');
		var selected = object.selected[0];

		if($('span .rename-input:visible',selected.element).length) return;
		
		$.action({
			action : object.options.actions.element_share_edit,
			path : selected.path,
		});
	}

	//edition d'un fichier texte en ligne
	DocumentApi.prototype.element_edit = function(path){
		var object = this;

		$('.file-editor-name',object.dom.panels.file).val('Nouveau Fichier.txt');

		//reinitialisation
		$('.file-editor-content iframe',object.dom.panels.file).remove();
		$('.file-editor-input',object.dom.panels.file).removeClass('hidden');

		$('.file-editor-input',object.dom.panels.file).val('');
		
		var saveShortcut = function(event){
			if(event.ctrlKey && event.key == 's'){
				object.element_edit_save(true);
				event.preventDefault();
				event.stopPropagation();
			}
		}


		if(path){
			$('.file-editor-input',object.dom.panels.file).val('Chargement en cours...');

			$.action({
				action : object.options.actions.element_edit,
				path : path,
			},function(r){

				$('.file-editor-name',object.dom.panels.file).val(r.label);
				$('.file-editor-input',object.dom.panels.file).val(r.content);

				$('.file-editor-content iframe').remove();

				//sauvegarde sur le ctrl+s
				$('.file-editor-input',object.dom.panels.file).unbind('keydown').keydown(saveShortcut);

				if(r.wysiwyg){
					
					$('.file-editor-input').addClass('hidden');
					var iframe = $('<iframe width="100%" height="100%" frameBorder="0">');


					iframe.on('load',function(){
						
						object.dom.editor =  $('<div id="editor">').html($('.file-editor-input').val());

						object.dom.editor.unbind('keydown').keydown(saveShortcut);

						var contextIframe = $(this).contents()[0];
				
		                // Set the style on the head of the iframe.
		                $('head', contextIframe).append('<link href="css/trumbowyg.min.css" rel="stylesheet"> \
		                	<style>html,body{padding:0!important;margin:0!important;}.trumbowyg-box{margin-top: 0;}.trumbowyg-button-pane{position:fixed;margin-top: -1px;}.trumbowyg-box .trumbowyg-editor{margin-top:20px;border:0px;}.trumbowyg-box{border:0px;}.trumbowyg-dropdown{position:fixed!important;width:300px!important;}</style>');

		                // Set the content to be editable.
		                $('body', contextIframe).append(object.dom.editor);

		               
						
		                object.dom.editor.trumbowyg(object.editorOptions);
					});

					$('.file-editor-content').append(iframe);
				}
			});
		}

		$('.file-editor',object.dom.panels.file).removeClass('hidden');
		$('.file-editor-input',object.dom.panels.file).focus();
	}

	//edition d'un fichier texte en ligne
	DocumentApi.prototype.element_edit_save = function(dontCloseEditor){
		var object = this;

		
		var content = $('.file-editor-input',object.dom.panels.file).val();

		if($('.file-editor-content iframe').length > 0){
			var contextIframe = $('.file-editor-content iframe').contents()[0];
			content = $('textarea[name="editor"]',contextIframe).val();
		}

		var path = object.currentFolder =='.' ? '' : object.currentFolder;
		if(path== '' && object.options.root!='' ) path = object.options.root;

		$.action({
			action : object.options.actions.element_save,
			label : $('.file-editor-name',object.dom.panels.file).val(),
			content : content,
			path : path,
		},function(r){
			//sauvegarde sans fermer
			if(dontCloseEditor){
				$.message('info','Fichier sauvegardé !');
				return;
			}
			//sauvegarde avec fermeture
			$('.file-editor',object.dom.panels.file).addClass('hidden');
			object.element_search();
		});
	}

	//Agrandissement de l'éditor en mode fullscreen
	DocumentApi.prototype.element_edit_expand= function(){
		var object = this;
		var editor = $('.file-editor',object.dom.panels.file);
		if(editor.hasClass('expanded')){
			editor.removeClass('expanded');
			$('.btn-editor-expand').attr('title','Agrandir');
			$('.btn-editor-expand i').attr('class','fas fa-expand-arrows-alt');
		}else{
			$('.btn-editor-expand i').attr('class','fas fa-compress-arrows-alt');
			$('.btn-editor-expand').attr('title','Réduire');
			editor.addClass('expanded');
		}
	}

	//Récuperation d'une liste de element dans le tableau elements
	DocumentApi.prototype.element_execute = function(element){
		var object = this;
		var inputSearch = $('.label-search',object.dom.panels.search);
		if(inputSearch.val().length) {
			inputSearch.val('').blur();
			$(".search-clear",object.dom.panels.search).removeAttr('style');
		}

		var line = $(element);

		if(line.attr('data-type') == 'directory'){
			object.element_search(line.attr('data-path'));
			object.triggerEvent('opened',{
				element : line,
				path : line.attr('data-path')
			});
			return;
		}
	
		object.triggerEvent('executed',{
				element : line,
				path : line.attr('data-path')
		});

		var selected = object.selected[0];

		var ext = selected.path.split('.').pop();
		switch(ext){
			case 'txt':
			case 'html':
			case 'htm':
				object.element_edit(line.attr('data-path'));
			break;
			default:
				object.element_download(line);
			break;
		}

		
	}

	// Téléchargement d'un élément de la GED
	DocumentApi.prototype.element_download = function(line,forceDownload){
		var object = this;
		var line = line ? line : object.selected[0].element;
		if(line.length == 0) return $.message('info', 'Sélectionnez d\'abord un élément à télécharger');
			
		object.triggerEvent('downloaded',{
				element : line,
				path : line.attr('data-path')
		});

		var url = 'action.php?action='+object.options.actions.element_execute+'&path='+encodeURIComponent(btoa(line.attr('data-path')));
		if(forceDownload) url+= '&download';
		window.open(url, '_blank');
	}


	/** PROPERTIES **/

	DocumentApi.prototype.properties_show = function(){
		
		var object = this;
		var modal = $('.document-properties-modal');
		var tpl = $('.modal-body-template',modal).html();
		modal.modal('show');

		$('.modal-body',modal).html('Chargement...');
		
		
		$.action({
			action : object.options.actions.properties_show,
			id : object.selected[0].id
		},function(r){
			$('.modal-body',modal).html(Mustache.render(tpl,r.row));
			init_components(modal);
			object.triggerEvent('properties-showed',{
				modal : modal,
				id : object.selected.id
			});
		});
	}

	/** ELEMENTRIGHT **/
		//Récuperation d'une liste de elementright dans le tableau elementrights
	DocumentApi.prototype.right_toggle = function(){
			var object = this;
			$('.right-panel',object.dom.panels.detail).toggleClass('hidden');
			init_components('.element-right-form');
			this.right_search();
	}
	//Récuperation d'une liste de elementright dans le tableau elementrights
	DocumentApi.prototype.right_search = function(callback){
		var object = this;
	
		if(object.selected.length == 0) return;

		$('.element-rights').fill({
			action:object.options.actions.right_search,
			id : object.selected[0].id
		},function(){
			if(callback!=null) callback();
		});
	}

	//Ajout ou modification d'élément elementright
	DocumentApi.prototype.right_save = function(){
		var object = this;
	
		var data = {
			uid : $('.element-right-form input.uid').val(),
			read : $('.element-right-form input.read').prop('checked') ? 1 :0,
			edit : $('.element-right-form input.edit').prop('checked') ? 1 :0,
			recursive : $('.element-right-form input.recursive').prop('checked') ? 1 :0
		}

		data.action = object.options.actions.right_save;
		data.element = object.selected[0].id;
		$.action(data,function(r){
			$.message('success','Enregistré');

			$('.uid').val('');
			$('.read,.edit,.recursive').prop('checked',false);
			init_components('.element-right-form');
			object.right_search();

			data.id = object.selected[0].id;
			data.element = object.selected[0].element;
			object.triggerEvent('right-saved',data);


		});
	}

	//Suppression d'élement elementright
	DocumentApi.prototype.right_delete = function(element){
		var object = this;
		if(!confirm('Êtes-vous sûr de vouloir supprimer ce droit ?')) return;
		var line = $(element).closest('li');
		$.action({
			action : object.options.actions.right_delete,
			id : line.attr('data-id')
		},function(r){
			object.triggerEvent('right-deleted',{id:line.attr('data-id'),element:line});
			line.remove();
		});
	}
	//Ajoute un callback sur un evenement de la ged particulier
	DocumentApi.prototype.on = function(event,callback){
		if(!this.onEvents[event])this.onEvents[event] = [];
		this.onEvents[event].push(callback);
	}

	/* Gestion des vues */

	//Change la vue courante
	DocumentApi.prototype.view = function(view){
		var object = this;
		object.currentView = view;
		object.renderView();
		object.triggerEvent('view-loaded',{view:view});
	}

	//Ajoute une vue custom
	DocumentApi.prototype.addView = function(data){
		var object = this;
		data.html = $(data.html);
		
		data.html.attr('data-view',data.uid);
		if(!data.html.hasClass('file-view')) data.html.addClass('file-view');
		
		object.views.push(data);
	}

	//affiche le rendu de la vue courante (object.currentView)
	DocumentApi.prototype.renderView = function(initPath){

		var object = this;
		if(!object.dom.panels.files) return;
		$('.file-view',object.dom.panels.files).addClass('hidden');
		var viewElement = $('.file-view[data-view="'+object.currentView+'"]');

		$(object.dom.panels.files).attr('data-view',object.currentView);
		$('.view-module > li',object.dom.panels.search).removeClass('selected');
		$('.view-module > li[data-view="'+object.currentView+'"]',object.dom.panels.search).addClass('selected');

		viewElement.removeClass('hidden');
		object.element_search(initPath);
		object.triggerEvent('rendered',{});
	}

	//charge toutes les vues disponibles dans le dom
	DocumentApi.prototype.loadViews = function(){
		var object = this;
		//Chargement des bouttons de vue
		$('.view-module > li:visible',object.dom.panels.search).remove();
		var tpl = $('.view-module > li.hidden',object.dom.panels.search).get(0).outerHTML;

		for(var key in object.views){
			var view = object.views[key];
			var button = $(Mustache.render(tpl, view));
			button.removeClass('hidden');
			$('.view-module',object.dom.panels.search).append(button);
			button.click(function(){
				object.view($(this).attr('data-view'));
			});

			
			if($('.file-view[data-view="'+view.uid+'"]',object.dom.panels.files).length==0)
		 		object.dom.panels.files.append(view.html);

		}

	}

