function widget_document_init(){
	init_components('.widgetDocumentContainer');
}


function document_widget_configure_save(widget,modal){

	var data = $('#document-widget-form').toJson();
	
	data.action = 'document_widget_configure_save';
	data.id = modal.attr('data-widget');

	$.action(data,function(){
		$.message('success','Configuration enregistrée');
		dashboard_dashboardwidget_search();
	});
}