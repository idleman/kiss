<?php 
global $myUser, $conf;
User::check_access('user','configure');
?>
<div class="row">
	<div class="col-md-12">
		<br>
		<div onclick="core_user_save();" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
		<h3>Utilisateurs</h3>
		<div class="clear"></div>
		<hr>
		<div id="userFormAdmin" data-action="core_user_save">
			<div class="row mb-3">
				<div class="col-md-6">
					<label class="mb-1" for="login">Identifiant :</label><small class="text-danger"> <?php echo ' (,'.$conf->get('login_forbidden_char').' interdits)'; ?> </small>
					<input id="login" required class="form-control" placeholder="Identifiant" type="text">
				</div>
				<div class="col-md-6">
					<label class="mb-1" for="mail">Mail :</label>
					<input id="mail" required class="form-control" placeholder="Mail" type="text">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-6">
					<label class="mb-1" for="password">Mot de passe : </label><small class="text-danger"><?php echo empty($conf->get('password_forbidden_char'))?'':' ('.$conf->get('password_forbidden_char').' interdits)'; ?></small>
					<input required type="password" data-show-strength data-generator data-type="password" placeholder="Mot de passe" class="form-control" id="password" autocomplete="new-password"/>
				</div>
				<div class="col-md-6">
					<label class="mb-1" for="password2">Mot de passe (confirmation) : </label><small class="text-danger"><?php echo empty($conf->get('password_forbidden_char'))?'':' ('.$conf->get('password_forbidden_char').' interdits)'; ?></small>
					<input required type="password" data-type="password" placeholder="Mot de passe (confirmation)" class="form-control" id="password2" autocomplete="new-password"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<label class="mb-1" for="firstname">Prénom :</label>
					<input id="firstname" class="form-control" placeholder="Prénom" type="text">
				</div>
				<div class="col-md-3">
					<label class="mb-1" for="name">Nom :</label>
					<input id="name" class="form-control" placeholder="Nom" type="text">
				</div>
				<div class="col-md-6">
					<label class="mb-1" for="manager">Manager :</label>
					<input id="manager" data-type="user" class="form-control" placeholder="Manager" type="text">
				</div>
			</div>
		</div>
		<hr class="my-3">
		<div class="panel panel-default">
			<legend class="panel-heading">Liste des utilisateurs existants :</legend>
			<table id="users" class="table table-sm table-users table-striped">
				<thead class="bg-secondary text-light">
					<tr>
						<th class="text-center">#</th>
						<th class="avatar-head"></th>
						<th>Nom</th>
						<th>Mail</th>
						<th>Identifiant</th>
						<th>Origine</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr data-user="{{login}}" class="hidden">
						<td class="align-middle text-center">{{id}}</td>
						<td class="text-center avatar-cell align-middle"><img class="avatar-mini avatar-rounded" data-src="{{avatar}}"/></td>
						<td class="align-middle">{{firstname}} {{name}}</td>
						<td class="align-middle">{{mail}}</td>
						<td class="align-middle"><code>{{login}}</code></td>
						<td class="align-middle">{{origin}}</td>
						<td class="text-right align-middle action-cell">
							<?php if($myUser->can('user','edit')): ?>
								{{#id}}<div onclick="core_user_edit(this)" class="btn btn-info btn-squarred btn-mini" title="Modifier"><i class="fas fa-pencil-alt"></i></div>{{/id}}
							<?php endif; ?>
							<?php if($myUser->superadmin): ?>
								<div onclick="window.location='action.php?action=core_core_user_impersonation&login={{formatedLogin}}';" title="Se connecter en tant que {{login}}" class="btn btn-warning btn-squarred btn-mini"><i class="far fa-meh-blank"></i></div>
							<?php endif; ?>
							<?php if($myUser->can('user','delete')): ?>
								{{#id}}<div onclick="core_user_delete(this)" class="btn btn-danger btn-squarred btn-mini"><i class="fas fa-times"></i></div>{{/id}}
							<?php endif; ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

