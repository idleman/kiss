<?php 
global $myUser;
User::check_access('user','configure');
//Refresh forcé des users pour avoir leurs établissements et rangs
$users = User::getAll(array('right'=>true, 'force'=>true));
$parameters = empty($myUser->superadmin) ? array('superadmin' => 0) : array();
?>
<div class="row">
	<div class="col-md-12">
		<br>
		<h3>Établissement / Utilisateur / Rang</h3>
		<hr/>

		<div id="userfirmrankForm" data-action="core_user_savefirmrank">
			<div class="form-row">
				<div class="form-group col-md-3">
			     	<label for="firm">Établissement :</label>
			     	<select class="form-control" id="firm" placeholder="Etablissement" onchange="core_userfirmrank_search();" required>
			     			<option value="0">Tous</option>
			     		<?php foreach(Firm::loadAll() as $firm) : ?>
							<option value="<?php echo $firm->id; ?>"><?php echo $firm->label; ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			    <div class="form-group col-md-3">
			     	<label for="user">Utilisateur :</label>
			     	<input type="text" data-type="user" placeholder="Nom, prénom..." data-scope="0" class="form-control" id="user" name="user" required/>
			    </div>
			    <div class="form-group col-md-3">
			     	<label for="rank">Rang :</label>
			     	<select class="form-control" id="rank" placeholder="Rang" required>
			     		<?php foreach(Rank::loadAll($parameters) as $rank):  ?>
							<option value="<?php echo $rank->id; ?>"><?php echo $rank->label; ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
				<div class="form-group col-md-3 text-right">
			     	<div class="btn btn-success noLabel " onclick="core_userfirmrank_save(this)"><i class="fas fa-check"></i> Enregistrer</div>
			    </div>
			</div>
		</div>
		<br/>
		<div class="panel panel-default">
			<legend class="panel-heading">Utilisateurs de l'établissement :</legend>
			<table id="userfirmranks" class="table table-sm table-striped">
				<thead class="bg-secondary text-light">
					<tr>
						<th></th>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Établissement</th>
						<th>Fonction</th>
						<th>Rang</th>
						<th class="text-right"></th>
					</tr>
				</thead>
				<tbody>
					<tr data-id="{{id}}" class="hidden">
						<td class="align-middle text-center">{{#avatar}}<img class="avatar-mini avatar-rounded" data-src="{{avatar}}"/>{{/avatar}}</td>
						<td class="align-middle">{{user.name}}</td>
						<td class="align-middle">{{user.firstname}}</td>
						<td class="align-middle">{{firm}}</td>
						<td class="align-middle">{{user.function}}</td>
						<td class="align-middle">{{rank}} {{#superadmin}}<code class="">(Super Admin)</code>{{/superadmin}}</td>
						<td class="align-middle text-right">
							<div onclick="core_userfirmrank_edit(this)" class="btn btn-info btn-squarred btn-mini"><i class="fas fa-pencil-alt"></i></div>
							<div onclick="core_userfirmrank_delete(this)" class="btn btn-danger btn-squarred btn-mini"><i class="fas fa-times"></i></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

