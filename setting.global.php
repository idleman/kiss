<?php 
global $myUser, $conf;
User::check_access('setting_global','read');
global $databases_credentials;
?>
<div class="row">
	<div class="col-md-12 general-settings" id="general-settings" data-action="core_general_settings_save">
		<br>
		<?php if($myUser->can('setting_global','configure')): ?>
		<div class="btn btn-success float-right" onclick="core_general_settings_save();"><i class="fas fa-check"></i> Enregistrer</div>
		<?php endif; ?>
		<h3>Réglages généraux</h3>
		<p>Bienvenue sur l'espace de configuration, cliquez sur le menu de gauche pour acceder aux différents réglages de l'application.</p>
		<hr/>

		<?php if($myUser->can('setting_global','configure')): ?>
		<div>
			<h5>Informations générales :</h5>
			<ul>
				<li>Nom du programme : <strong><?php echo PROGRAM_NAME; ?></strong></li>
				<li>Version : <strong><?php echo SOURCE_VERSION; ?></strong></li>
				<li>Version PHP : <strong><?php echo phpversion(); ?></strong></li>
				<li>Version Apache : <strong><?php echo apache_get_version(); ?></strong></li>
				<li>Version de la base: <strong><?php echo $databases_credentials['local']['connector'].' - '.Database::version(); ?></strong></li>
				<li>Adresse de la base : <strong><?php echo $databases_credentials['local']['host']; ?></strong></li>
				<li>Time Zone : <strong><?php echo TIME_ZONE; ?></strong></li>
				<li>OS : <strong><?php echo PHP_OS; ?></strong></li>
				<li>Horodatage serveur : <strong><?php echo date('d/m/Y H:i.s'); ?></strong></li>
			</ul>
		</div>
		<hr>
		<!-- Réglages généraux de l'application -->
		<div>
			<div>
				<h5 class="d-inline-block mb-2">Logs</h5><small class="text-muted"> - Définition de la durée de conservation des logs par catégorie en nombre de jours</small> :
				<small class="d-inline-block float-right text-danger">
					Laisser vide si vous souhaitez conserver les logs indéfiniment (<strong>déconseillé</strong>)
				</small>
				<div class="clear"></div>
			</div>
			<div class="row mt-2">
				<?php foreach(Log::staticQuery('SELECT DISTINCT category FROM {{table}}',array(),true) as $log): 
					$slug = slugify($log->category);
				?>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<div class="input-group-prepend w-75">
							<span class="input-group-text w-100"><code><?php echo $log->category ?></code></span>
						</div>
						<input type="number" onkeydown="return input_number_control(event);" class="form-inline form-control" id="log_retention_time_<?php echo $slug; ?>"  placeholder="ex. 30" value="<?php echo $conf->get('log_retention_time_'.$slug); ?>">
					</div>		
				</div>
				<?php endforeach; ?>
			</div>
			<hr>
        	<div class="row password-block">
				<div class="col-md-12">
					<h5 class="d-inline-block mb-2">Mots de passes</h5><small class="text-muted"> - Définition de la politique de format des mots de passes</small> :
					<div id="password-format-form">
						<?php 
							$selectedFormats = json_decode($conf->get('password_format'),true);
							if(!is_array($selectedFormats )) $selectedFormats  = array();
							foreach(User::password_formats() as $format):
						?>
						<label class="pl-3 pointer"><input type="checkbox" data-type="checkbox" <?php echo in_array($format['pattern'], $selectedFormats)?'checked="checked"':''; ?> data-label="<?php echo $format['label'] ?>" data-pattern="<?php echo $format['pattern'] ?>" onclick="password_settings_format()"> <?php echo $format['label'] ?></label><br/>
						<?php endforeach; ?>
						
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12 general-settings-block">
					<?php echo Configuration::html('configuration-global'); ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12 maintenance-block">
					<span class="float-right">
						<input onchange="toggle_maintenance();" <?php echo file_exists(File::dir().SLASH.'enabled.maintenance') ? 'checked="checked"' : '' ?> type="checkbox" autocomplete="off" name="maintenance" id="maintenance" data-type="checkbox">
						<label for="maintenance" class="pointer">Site en maintenance</label><br>
					</span>
					<h5>Maintenance :</h5>
					<div class="clear"></div>
					<textarea data-type="wysiwyg" name="maintenance-content" id="maintenance-content" cols="30" rows="3" class="m-0">
						<?php echo file_exists(File::dir().SLASH.'enabled.maintenance') ? file_get_contents(File::dir().SLASH.'enabled.maintenance') : (file_exists(File::dir().SLASH.'disabled.maintenance') ? file_get_contents(File::dir().SLASH.'disabled.maintenance') : ''); ?>
					</textarea>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div><br>
