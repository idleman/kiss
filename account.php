<?php require_once __DIR__.DIRECTORY_SEPARATOR.'header.php';  

User::check_access('account','read');

$accountMenu = array();
Plugin::callHook("menu_account", array(&$accountMenu));
usort($accountMenu, function($a,$b){
	if($a['sort'] > $b['sort']) return 1;
	if($a['sort'] < $b['sort']) return -1;
	if($a['sort'] == $b['sort']) return 0;
});
$_['section'] = !isset($_['section']) ? 'global': $_['section'];
$page = basename($_SERVER['PHP_SELF']);
?>

<div class="row">
	<div class="col-md-3">
		<div class="list-group">
			<span class="list-group-item bg-secondary text-light"><h5 class="mb-0">Préférences</h5></span>
			<?php foreach($accountMenu as $item): ?>
				<a href="<?php echo $item['url']; ?>" class="list-group-item list-group-item-action <?php echo $item['url'] == $page.'?section='.$_['section']?'active':'text-primary'; ?>"><i class="<?php echo $item['icon']; ?>"></i> <?php echo $item['label']; ?></a>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="col-md-9">
		<?php Plugin::callHook("content_account"); ?>
	</div>
</div>

<?php require_once __ROOT__.'footer.php' ?>
