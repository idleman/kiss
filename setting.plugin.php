<?php 
global $myUser;
User::check_access('plugin','configure');
?>
<div class="row">
	<div class="col-md-12">
		<h3>Plugins</h3>
		<hr/>
		<ul id="plugins" class="list-group plugins">
			<li class="list-group-item hidden item" data-id="{{id}}" style="{{#color}}border-left:2px solid {{color}};{{/color}}">
				<h5 class="list-group-item-heading">{{#icon}}<i class="{{icon}}"></i>{{/icon}} {{name}} ({{id}}) <span class="label label-info">v{{version}}</span></h5>
				<p class="list-group-item-text text-muted mb-0">{{description}} <a class="pointer btn btn-link btn-small" title="Afficher les informations du plugin" data-tooltip onclick="$(this).parent().next('ul').toggleClass('hidden');"><i class="fas fa-search"></i> + d'Infos</a></p>
				<ul class="hidden">
					<li>ID : {{id}}</li>
					<li>Auteur : <span class="label label label-default">{{author.name}}</span></li>
					<li>Licence : {{#licence.url}}<a href="{{licence.url}}">{{/licence.url}}{{licence.name}}{{#licence.url}}</a>{{/licence.url}}</li>
					<li>Version : <code>{{version}}</code></li>
					<li>Site web : <a href="{{url}}">{{url}}</a></li>
					<li>Dossier : {{folder.path}}</li>
					{{#require}}
					<li>Pré-requis : 
						<ul>{{#required}}
							<li>{{id}} - <code>{{version}}</code></li>
						{{/required}}</ul>
					</li>
					{{/require}}
				</ul>

				<div class="plugin-firm-block">
					Activer pour les établissements : 
					<ul class="list-style-type-none">
					{{#firms}}
						<li class="firm-item" data-id="{{id}}"><label class="pointer"><input type="checkbox" class="firm-toggle" onclick="core_firm_plugin_save(this)" data-value="{{value}}" data-type="checkbox"> {{label}}</label></li>
					{{/firms}}
					</ul>
				</div>
				

				<label class="btn activator {{#state}}text-success{{##state}}{{^state}}text-muted{{##state}}">
					<input {{#state}}checked{{##state}} type="checkbox" class="toggle" autocomplete="off" name="plugin-{{id}}" id="plugin-{{id}}" data-type="checkbox">
					{{#state}}Désactiver{{/state}}{{^state}}Activer{{/state}}
				</label>
			</li>
		</ul>
	</div>
</div>
<br>

