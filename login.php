<?php
global $conf,$myUser;
$loginMenu = array();
Plugin::callHook("menu_login", array(&$loginMenu));
uasort ($loginMenu , function($a,$b){return $a['sort']>$b['sort']?1:-1;}); 

?>
<div id="loginForm" class="form-inline mt-mb-0 login-form">
	<label for="login">Identifiant</label>
	<input name="login" id="login" maxlength="260" class="form-control form-control-sm mb-2" type="text" required autofocus="true" autocomplete="username">
	<label for="password">Mot de passe</label> 
	<input data-type="password" name="password" id="password" class="form-control form-control-sm " type="password" required autocomplete="current-password">
	<div class="form-check connexion-remember mb-1" title="Cocher la case pour enregistrer la session de connexion">
	    <label class="form-check-label pointer" onclick="event.stopPropagation();"><input data-type="checkbox" class="" type="checkbox" id="rememberMe" name="rememberMe"> Rester connecté</label>
	</div>
	<div class="btn btn-primary btn-sm btn-login w-100" title="Connexion" id="login-button" onclick="core_login(this)" tabindex="0">
		<i class="fas fa-fw fa-sign-in-alt"></i> Se connecter</div>
	
	<?php foreach($loginMenu as $item): ?>
		<?php if(isset($item['custom'])):
			echo $item['custom'];
		else: ?>
			<a class="login-menu-item" href="<?php echo $item['url']; ?>">
				<?php echo (isset($item['icon'])?'<i class="'.$item['icon'].'"></i> ':'').$item['label']; ?>
			</a>
		<?php endif; ?>
	<?php endforeach; ?>

	<?php if($conf->get('password_allow_lost')): ?>
	<a href="account.lost.php" title="Modifier son mot de passe grâce à un lien de récupération reçu par email" class="d-block w-100 text-center mt-1 lost-password">Mot de passe oublié ?</a>
	<?php endif; ?>
</div>