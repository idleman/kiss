<?php

$firms = Firm::loadAll();
$firms[] = Firm::anonymous_firm();

?>
<br>
<h3>Établissement / Plugins</h3>
<hr/>
<div class="row">
	<div class="col-md-12 firm-plugin-header">
		<label>Établissement concerné :</label>
		<select onchange="search_firm_plugin();" class="form-control" id="firm">
			<option value="0">Tous</option>
		<?php foreach ($firms as $firm) : ?>
			<option value="<?php echo $firm->id; ?>"><?php echo $firm->label; ?></option>
		<?php endforeach; ?>
		</select>
	</div>
</div>
<br/>
<table class="table table-striped table-bordered table-hover" id="firmplugins" data->
    <thead class="bg-secondary text-white">
		<tr>
			<th>Plugin</th>
			<th>Description</th>
			<th class="text-center">Activé</th>
		</tr>
    </thead>
	<tbody>			    
		<tr class="hidden" data-id="{{id}}">			    
			<td>{{name}} <br><small class="text-muted">{{id}}</small></td>
			<td>{{description}}</td>
			<td class="text-center">
				<input value="1" data-result={{state}} onchange="toggle_firm_plugin(this)"  type="checkbox" autocomplete="off" name="plugin-{{id}}" id="plugin-{{id}}" data-type="checkbox">
			</td>
		</tr>
	</tbody>
</table>