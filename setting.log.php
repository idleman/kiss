<?php 
global $myUser;
User::check_access('log','read');
$categories = array();
foreach(Log::staticQuery('SELECT DISTINCT category FROM {{table}} ORDER BY category',array(),true) as $category)
	$categories[$category->category] = $category->category; 
?>
<div class="row">
	<div class="col-md-12">
		<br>
		<div class="btn btn-primary float-right" title="Exporter les résultats au format Excel" onclick="search_log(true);" id="export-logs-btn"><i class="far fa-file-excel mr-1"></i>Exporter</div>
		<h3>Logs</h3>
		<div class="clear"></div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<select id="filters" data-slug="log-search" data-type="filter" data-label="Recherche" data-function="search_log">
				    <option value="label" data-filter-type="text">Libellé</option>
				    <option value="category" data-filter-type="list" data-values='<?php echo json_encode($categories); ?>'>Catégorie</option>
				    <option value="created" data-filter-type="date">Date</option>
				    <option value="ip" data-filter-type="text">IP</option>
				    <option value="creator" data-filter-type="user">Utilisateur</option>
				</select>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-12">
				<table id="logs" class="table table-striped">
					<thead class="bg-secondary text-light">
						<tr>
							<th data-sortable="created">Date</th>
							<th data-sortable="category">Catégorie</th>
							<th data-sortable="label" class="log-col">Libellé</th>
							<th data-sortable="creator">Utilisateur</th>
							<th data-sortable="ip">Ip</th>
						</tr>
					</thead>
					<tbody>
						<tr data-id="{{id}}" class="hidden">
							<td>{{created}}</td>
							<td>{{category}}</td>
							<td class="log-col">{{{label}}}</td>
							<td>{{creator}}</td>
							<td>{{ip}}</td>
						</tr>
					</tbody>
				</table>

				<ul class="pagination justify-content-center">
				    <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');search_log()">
				        <span class="page-link">{{label}}</span>
				    </li>
				</ul>
			</div>
		</div>
	</div>
</div>

