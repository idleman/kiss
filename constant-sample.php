<?php
define('SLASH',DIRECTORY_SEPARATOR);
define('__ROOT__',dirname(__FILE__).SLASH);
define('TIME_ZONE','Europe/Paris');
define('ENTITY_PREFIX', '');
define('LOG_PATH',__ROOT__.'log');
define('FILE_PATH','file'.SLASH);
define('PLUGIN_PATH','plugin'.SLASH);
define('LIB_PATH',__ROOT__.'lib'.SLASH);

define('AVATAR_PATH','avatar'.SLASH);

global $databases_credentials;
$databases_credentials = array(
	'local' => array(
		'connector' => '{{connector}}',
		'host' => '{{host}}',
		'name' => '{{name}}',
		'login' => '{{login}}',
		'password' => '{{password}}',
	),
	// 'db2' => array(
	// 	'connector' => 'Oracle',
	// 	'name' => 'my ODBC name',
	// 	'login' => 'login',
	// 	'password' => 'password',
	// )
);

define('ROOT_URL','{{root}}');
define('CRYPTKEY','{{cryptKey}}');
//logs toutes les requetes sans formattage
define('BASE_DEBUG',false);
define('COOKIE_NAME','kiss-cookie');
define('PROGRAM_NAME','Kiss');
define('PROGRAM_AUTHOR','Kiss Team');
define('SOURCE_VERSION','1.0');

define('ADMIN_MAIL','{{admin_mail}}');



?>