<?php 
global $myUser;
User::check_access('rank','configure');
?>
<div class="row">
	<div class="col-md-12">
		<br>
		<h3>Rangs & Accès</h3>
		<hr>
		<div class="panel panel-default">
			<table id="ranks" class="table table-striped">
				<thead class="bg-secondary text-white">
					<tr>
						<th>#</th>
						<th>Libellé</th>
						<th>Description</th>
						<th style="width: 100px;"></th>
					</tr>
					<tr id="rankForm" data-action="core_rank_save">
						<th class="py-1 align-middle"></th>
						<th class="py-1 align-middle"><input id="label" class="form-control form-control-sm" placeholder="Libellé" type="text"></th>
						<th class="py-1 align-middle"><input id="description" class="form-control form-control-sm" placeholder="Description" type="text"></th>
						<th class="py-1 align-middle"><div onclick="core_rank_save();" data-tooltip title="Enregistrer" class="btn btn-success w-100"><i class="fas fa-check"></i></div></th>
					</tr>
				</thead>
				<tbody>
					<tr data-id="{{id}}" class="hidden">
						<td>{{id}}</td>
						<td>{{label}}</td>
						<td>
							<span>{{description}}</span>
							<div class="rank-rights text-muted">
								<ul>
								{{#rights}}
									<li class="d-flex">
										<div class="mr-2">
											<span class="text-info font-weight-bold">{{firm}}</span>
											<code class="">{{scope}}</code>
										</div>
										<div>
											{{#read}}<i title="Lecture" class="text-dark far fa-eye"></i>{{/read}} 
											{{#edit}}<i title="Edition" class="text-info fas fa-pencil-alt"></i>{{/edit}} 
											{{#delete}}<i title="Suppression" class="text-danger far fa-trash-alt"></i>{{/delete}} 
											{{#configure}}<i title="Configuration" class="text-warning fas fa-wrench"></i>{{/configure}} 
										</div>
									</li>
								{{/rights}}
								</ul>
							</div>
						</td>
						<td class="option option-3 text-center">
							{{#configure}}<a href="setting.php?section=right&amp;rank={{id}}" class="btn btn-primary btn-squarred btn-mini" title="Voir les droits du rang"><i class="fas fa-lock"></i></a>{{/configure}}
							{{#edit}}<div onclick="core_rank_edit(this)" class="btn btn-info btn-squarred btn-mini" title="Éditer le rang"><i class="fas fa-pencil-alt"></i></div>{{/edit}}
							{{#delete}}<div onclick="core_rank_delete(this)" class="btn btn-danger btn-squarred btn-mini" title="Supprimer le rang"><i class="fas fa-times"></i></div>{{/delete}}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>