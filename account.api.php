<?php 
global $myUser, $myFirm, $conf;
if (!$myUser->connected()) throw new Exception("Vous devez être connecté pour accéder à la page",401);

$api_enabled = $myUser->preference('api_enabled');
$api_id = decrypt($myUser->preference('api_id'));
$api_secret = decrypt($myUser->preference('api_secret'));

if(empty($api_id)){
	$myUser->preference('api_id',encrypt(bin2hex(random_bytes(15))));
	$myUser->preference('api_secret',encrypt(bin2hex(random_bytes(15))));
	$api_id = decrypt($myUser->preference('api_id'));
	$api_secret = decrypt($myUser->preference('api_secret'));
	$myUser->loadPreferences();
	$_SESSION['currentUser'] = serialize($myUser);
}

?>
<div id="account-api-form" class="account-api-form">
	<br>
	<div onclick="core_account_api_save()" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
	<h3>API & TOKEN</h3>
	<div class="clear"></div>
	<hr>
	<div class="row">
		<div class="col-md-6 api_section" data-enabled="<?php echo $api_enabled?1:0 ?>">
			<label><input id="api-enabled" type="checkbox" <?php echo $api_enabled?"checked='checked'":"" ?> onclick="$(this).closest('.api_section').attr('data-enabled',$(this).prop('checked')?1:0)" data-type="checkbox"> Activer les API pour ce compte</label>
			<div class="api_credentials" >
				<div><strong>ID API : </strong><input type="text" id="api_id" value="<?php echo $api_id; ?>" class="form-control" readonly="readonly"></div>
				<div><strong>SECRET API : </strong><input type="text" data-type="password" id="api_secret" value="<?php echo $api_secret; ?>" class="form-control" readonly="readonly"></div>
			</div>
		</div>
	</div>
</div>
