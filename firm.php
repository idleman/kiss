<?php 
require_once('header.php');
User::check_access('firm','read');

$firm = Firm::provide();
?>
<div class="plugin-core">
	<div id="firm-form" class="row justify-content-md-center firm-form" data-action="core_firm_save" data-id="<?php echo $firm->id; ?>">
		<div class="col-md-6 shadow-sm bg-white p-3">
			<h3>Etablissement 
			<div onclick="core_firm_save();" class="btn btn-small btn-success right"><i class="fas fa-check"></i> Enregistrer</div>
			<a href="setting.php?section=firm" class="btn btn-small btn-dark right  mr-2">Retour</a></h3>
			<label for="label">Raison sociale</label>
			<input  value="<?php echo $firm->label; ?>" class="form-control"  type="text"  id="label" >
			<label for="description">Description</label>
			<textarea  class="mt-0" type="text" data-type="wysiwyg" id="description"><?php echo $firm->description; ?></textarea>
			<label for="logo">Logo</label>
			<input  value="" class="component-file-cover bg-light shadow-sm rounded-sm"  type="text"  data-type="file"  data-limit="1"  data-extension="jpg,png,bmp,jpeg,gif,svg,webp"  data-action="core_firm_logo"  data-id="logo"  data-data='{"id":"<?php echo $firm->id; ?>"}'  id="logo" >
			<label for="mail">E-mail</label>
			<input  value="<?php echo $firm->mail; ?>" class="form-control"  type="mail"  data-type="mail"  pattern=".+@.+"  id="mail" >
			<label for="phone">N° Téléphone</label>
			<input  value="<?php echo $firm->phone; ?>" class="form-control"  type="text"  data-type="phone"  id="phone" >
			<label for="fax">N° FAX</label>
			<input  value="<?php echo $firm->fax; ?>" class="form-control"  type="text"  data-type="phone"  id="fax" >
			<label for="street">Rue</label>
			<textarea  class="form-control" type="text" id="street"><?php echo $firm->street; ?></textarea>
			<label for="street2">Complément d'adresse</label>
			<textarea  class="form-control" type="text" id="street2"><?php echo $firm->street2; ?></textarea>
			<label for="city">Ville</label>
			<input  value="<?php echo $firm->city; ?>" class="form-control"  type="text"  id="city" >
			<label for="zipcode">Code postal</label>
			<input  value="<?php echo $firm->zipcode; ?>" class="form-control"  type="text"  id="zipcode" >
			<label for="siret">N° SIRET</label>
			<input  value="<?php echo $firm->siret; ?>" class="form-control"  type="text"  id="siret" >
			<label for="iban">N° IBAN</label>
			<input  value="<?php echo $firm->iban; ?>" class="form-control"  type="text"  id="iban" >
			<br/>
			
		</div>
	</div>
</div>
<?php 
require_once('footer.php');
?>