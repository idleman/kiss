<div class="welcome container login-request" >
	<div class="row justify-content-center">
		<div class="col-md-6 col-sm-10 p-5 shadow-sm bg-white">
			<h3><i class="fas fa-user-circle"></i> Veuillez vous identifier</h3>
			<p class="text-muted">Pour vous connecter, renseignez le formulaire ci dessous</p>
			<div class="login-form-block position-relative">
				<?php require(__ROOT__.'login.php'); ?>
			</div>
		</div>
	</div>
</div>