<?php
global $myUser;
User::check_access('firm','configure');
?>
<div class="plugin-core">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex my-2 w-100">
                <h4 class="d-inline-block my-auto mx-0 text-uppercase">Liste des etablissements</h4>
                <div class="text-left ml-3 d-inline-block noPrint">
                    <div onclick="window.print();" class="btn btn-info rounded-0 btn-squarred" title="Imprimer la page"><i class="fas fa-print"></i></div>
                    <div onclick="core_firm_search(null,true);" id="export-cores-btn" class="btn btn-info rounded-0 btn-squarred ml-1 btn-export" title="Exporter les résultats"><i class="fas fa-file-export"></i></div>
                </div>
                <?php if($myUser->can('firm', 'edit')) : ?>
                <div class="my-auto ml-auto mr-0 noPrint">
                    <a href="firm.php" class="btn btn-success right"><i class="fas fa-plus"></i> Ajouter</a>
                </div>
                <?php endif; ?>
            </div>
            <div class="clear noPrint"></div>
        </div>
        <div class="col-md-12">
            <select id="core_firm-filters" data-type="filter" data-label="Recherche" data-function="core_firm_search">
                <option value="main.label" data-filter-type="text">Libellé</option>
                <option value="main.description" data-filter-type="wysiwyg">Description</option>
                <option value="main.logo" data-filter-type="image">Logo</option>
                <option value="main.mail" data-filter-type="mail">E-mail</option>
                <option value="main.phone" data-filter-type="phone">N° Téléphone</option>
                <option value="main.fax" data-filter-type="phone">N° FAX</option>
                <option value="main.street" data-filter-type="textarea">Rue</option>
                <option value="main.street2" data-filter-type="textarea">Complément d'adresse</option>
                <option value="main.city" data-filter-type="text">Ville</option>
                <option value="main.zipcode" data-filter-type="text">Code postal</option>
                <option value="main.siret" data-filter-type="text">N° SIRET</option>
                <option value="main.iban" data-filter-type="text">N° IBAN</option>-->
            </select>
        </div>
    </div>
    <h5 class="results-count my-2"><span></span> Résultat(s)
        <!-- bloc de préférence de pagination -->
        <small class="text-muted right text-muted text-small"><div class="d-inline-block mr-1" data-type="pagination-preference" data-table="#firms" data-value="20" data-max-item="100"></div></small><div class="clear"></div>
    </h5>
    <div class="row">
    	<!-- search results -->
    	<div class="col-xl-12">
            <table id="firms" class="table table-striped " data-entity-search="core_firm_search">
                <thead>
                    <tr>
                        <th></th>
                        <th data-sortable="label">Libellé</th>
                        <th data-sortable="description">Description</th>
                        <th data-sortable="mail">E-mail</th>
                        <th data-sortable="phone">N° Téléphone</th>
                        <th data-sortable="fax">N° FAX</th>
                        <th data-sortable="street">Adresse</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-id="{{id}}" class="hidden item-line">
    	                <td class="align-middle text-center bg-dark"><img class="rounded-sm" style="max-width: 80px;height: auto;" src="{{logo}}"></td>
    	                <td class="align-middle">{{label}}</td>
    	                <td class="align-middle">{{{description}}}</td>
    	                <td class="align-middle">{{mail}}</td>
    	                <td class="align-middle">{{phone}}</td>
    	                <td class="align-middle">{{fax}}</td>
    	                <td class="align-middle">{{street}} {{street2}} {{zipcode}} {{city}}</td>
    	                <td class="align-middle text-right">
                            <div class="btn-group btn-group-sm" role="group">
                                <a class="btn text-info" title="Éditer firm" href="firm.php?id={{id}}"><i class="fas fa-pencil-alt"></i></a>
                                <div class="btn text-danger" title="Supprimer firm" onclick="core_firm_delete(this);"><i class="far fa-trash-alt"></i></div>
                            </div>
    	                </td>
                    </tr>
               </tbody>
            </table>
            <br>
             <!-- Pagination (data-range définit le nombre de pages max affichées avant et après la page courante) -->
            <ul class="pagination justify-content-center"  data-range="5">
                <li class="page-item hidden" data-value="{{value}}" title="Voir la page {{label}}" onclick="$(this).parent().find('li').removeClass('active');$(this).addClass('active');core_firm_search();">
                    <span class="page-link">{{label}}</span>
                </li>
            </ul>
    	</div>
    </div>
</div>