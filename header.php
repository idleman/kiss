<?php require_once __DIR__.DIRECTORY_SEPARATOR.'common.php'; 

$scheme = define_url_scheme();
$mediaRoot = define_media_root();

if(!empty($myUser->preference('passwordTime')) && !$myUser->superadmin){
    $basepage = explode('?',$page);
    $basepage = $basepage[0];
    if(is_numeric($conf->get('password_delay')) && $basepage!="account.php" && ((time() - ($conf->get('password_delay') * 86400) > $myUser->preference('passwordTime')))) 
    	header('location: account.php?error=Votre mot de passe est trop vieux, veuillez le renouveller ci dessous.');
}

//Redirige la home si définie dans les settings
if((strpos($page, 'index.php')!==false || $page==basename(ROOT_URL) || $page=='') && !isset($_['module']) && !empty($conf->get('home_page')) && (!isset($_['admin_login']) || empty($_['admin_login'])))
	header('location:'.$conf->get('home_page'));

$cssModule = array();
foreach(array('module','page','section') as $term){
	if(isset($_[$term])) $cssModule[] = preg_replace('/([^a-z])/i', '-',$term.'-'.$_[$term]);
}
if(empty($cssModule) && ($page=='index.php' || $page==basename(ROOT_URL) || $page=='' || preg_match("/index\.php\?admin_login=[0|1]/i", $page))) $cssModule[] = 'module-index';

if(isset($_['embedded'])) $cssModule[]='embedded';

if(!isset($_['admin_login']) && file_exists(File::dir().SLASH.'enabled.maintenance') && !$myUser->connected()){
    isset($_['error']) && !empty($_['error']) ? header('location: maintenance.php?error='.$_['error']) : header('Location: maintenance.php');
    exit();
} else {
	$mainMenu = array();
	Plugin::callHook("menu_main", array(&$mainMenu));
	uasort($mainMenu , function($a,$b){return $a['sort']-$b['sort'];});
	
	$userMenu = array();
	if($myUser->connected()){
		Plugin::callHook("menu_user", array(&$userMenu));
		uasort($userMenu , function($a,$b){return $a['sort']-$b['sort'];});
	} ?>
	<!DOCTYPE html>
	<html class="html <?php echo implode(' ',$cssModule); ?>" lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Blocage du référencement -->
		<?php if(empty($conf->get('core_public_seo'))): ?>
		<meta name="robots" content="noindex">
		<?php endif; ?>

		<!-- Favicon -->
    	<link rel="shortcut icon" type="image/png" href="media/core/public/favicon.png" />
	
		<?php $title = $myUser->connected() ? 'Accueil':'Connexion';
		foreach($mainMenu as $item)
			$title = (!empty($item['url']) && strpos($page, $item['url']) !== false) ? $item['label'] : $title;

		foreach($userMenu as $item)
			$title = (isset($item['url']) && strpos($page, $item['url']) !== false) ? $item['label'] : $title;
		?>
	
		<title><?php echo PROGRAM_NAME.' - '.$title ?></title>
	
		<!-- Bootstrap core CSS -->
		<link href="<?php echo $mediaRoot ?>/css/bootstrap.min.css" rel="stylesheet">
		<!-- jQuery UI -->
		<link rel="stylesheet" href="<?php echo $mediaRoot ?>/css/jquery-ui.min.css">
		<link rel="stylesheet" href="<?php echo $mediaRoot ?>/css/jquery.timepicker.min.css">
		<!-- Font awesome -->
		<link rel="stylesheet" href="<?php echo $mediaRoot ?>/css/fontawesome-all.min.css">
		<!-- Trumbowyg -->
		<link href="<?php echo $mediaRoot ?>/css/trumbowyg.min.css" rel="stylesheet">
		<link href="<?php echo $mediaRoot ?>/css/trumbowyg.table.css" rel="stylesheet">
		<link href="<?php echo $mediaRoot ?>/css/trumbowyg.colors.css" rel="stylesheet">
		<link href="<?php echo $mediaRoot ?>/css/leaflet.css" rel="stylesheet">
		<link href="<?php echo $mediaRoot ?>/css/MarkerCluster.css" rel="stylesheet">
		<link href="<?php echo $mediaRoot ?>/css/MarkerCluster.Default.css" rel="stylesheet">

		<?php if(!$conf->get('offline_mode')): ?>
		<!-- Lato font-->
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
		<?php endif; ?>

		<!-- Custom styles for this template -->
		<link href="<?php echo $mediaRoot ?>/css/component.css?v=<?php echo $cacheVersion; ?>" rel="stylesheet">
		<link href="<?php echo $mediaRoot ?>/css/main.css?v=<?php echo $cacheVersion; ?>" rel="stylesheet">
		<!-- Plugin css files -->
		<?php echo Plugin::callCss($mediaRoot,$cacheVersion); ?>
	</head>
	
	<body>

		<!-- Fixed navbar -->
		<nav id="mainMenu" class="navbar navbar-expand-md navbar-dark fixed-top noPrint">
			<?php if ($myUser->connected()): ?>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<div class="menu">
					<div class="menu-icon">
						<div class="menu-line menu-line-1"></div>
						<div class="menu-line menu-line-2"></div>
						<div class="menu-line menu-line-3"></div>
						<div class="menu-line menu-line-4"></div>
						<div class="menu-line menu-line-5"></div>
					</div>
				</div>
			</button>
			<?php endif; 

			$logo = 'media/core/public/logo.png';
			if($myFirm->id!=0  ){
				$firmLogo = $myFirm->logo('publicPath');
			
				if(!empty($firmLogo)) $logo = $firmLogo;
			}
			?>
			<a class="navbar-brand" style="background-image: url(<?php echo $logo; ?>)" <?php echo $conf->get('logo_website_header') ? 'target="_blank" href="'.$conf->get('logo_website_header').'"' : 'href="index.php"'; ?>><?php echo $conf->get('show_application_name') ? PROGRAM_NAME : ''; ?></a>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav navbar-main">
				<?php foreach($mainMenu as $item): ?>
					<?php if (isset($item['label']) && $item['label'] == 'Réglages') $page = basename($_SERVER['PHP_SELF']); 
					$classes = isset($item['classes'])? $item['classes']: '';
					if(isset($item['url']) 
						&& ((!empty($item['url']) 
						&& strpos($page, $item['url']) !== false 
						&& $item['url'] != 'index.php') || $page == $item['url'])
						|| (isset($item['active']) && $item['active'] ==true)
					){
						$classes .= ' active';
					} 
					?>
					<li data-id="<?php echo isset($item['id'])?$item['id']:''; ?>" class="nav-item <?php echo $classes; ?> ">
						<a class="nav-link" 
							title="<?php echo isset($item['title'])?$item['title']:''; ?>" 
							target="<?php echo isset($item['target'])?$item['target']:''; ?>" 
							onclick="<?php echo isset($item['onclick'])?$item['onclick']:''; ?>" 
							<?php echo isset($item['url'])? 'href="'.$item['url'].'"':''; ?>>
							<?php echo (isset($item['icon'])?'<i class="'.$item['icon'].'"></i> ':'').'<span>'.(isset($item['label'])?$item['label']:'').'</span>'; ?>
							<?php echo isset($item['html'])? $item['html']:''; ?>
						</a>
					</li>
				<?php endforeach; ?>
				</ul>
				<?php Plugin::callHook("header", array()); ?>

				<div id="loginHeader" class="ml-auto d-flex text-right" data-firm="<?php echo $myFirm->id; ?>" data-connected="<?php echo $myUser->login; ?>">
				<?php Plugin::callHook("login_header", array());		
				if(empty($conf->get('hide_header_login')) || $myUser->connected() || (!empty($conf->get('hide_header_login')) && ($page!='index.php' || ($page!='' && $page!='index.php')))): ?>
					<!-- User connecté -->
					<?php if($myUser->connected()): ?>
					<div class="dropdown user-dropdown-menu ml-1">
						<button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img src="<?php echo $myUser->getAvatar(); ?>" class="avatar-mini avatar-rounded avatar-login" title="<?php echo htmlentities($myUser->fullName()); ?>">
						</button>
						<div class="dropdown-menu dropdown-menu-right pb-1" aria-labelledby="dropdownMenuButton">
							<div class="font-weight-bold text-primary p-2 text-center user-fullname"><?php echo $myUser->fullName(); ?></div>
							<?php foreach($userMenu as $item): ?>
								<?php if(isset($item['custom'])):
									echo $item['custom'];
								else: ?>
									<a class="dropdown-item user-menu-item px-3" href="<?php echo $item['url']; ?>">
										<?php echo (isset($item['icon'])?'<i class="fa-fw '.$item['icon'].'"></i> ':'').$item['label']; ?>
									</a>
								<?php endif; ?>
							<?php endforeach; ?>
						</div>
					</div>
					<!-- User non connecté -->
					<?php else: ?>
					<div class="dropdown user-dropdown-menu ml-1" id="login-dropdown">
						<button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Connexion</button>
						<div class="dropdown-menu dropdown-menu-right pb-1" aria-labelledby="dropdownMenuButton">
							<div class="dropdown-item login-item"> 
								<?php require_once(__DIR__.SLASH.'login.php'); ?>
							</div>
						</div>
					</div>
					<?php endif; ?>
				<?php endif; ?>
				</div>
			</div>
		</nav>
	
		<!-- Begin page content -->
		<div class="container-fluid">
<?php } 
