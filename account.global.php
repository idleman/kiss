<?php 
global $myUser, $myFirm, $conf;
if (!$myUser->connected()) throw new Exception("Vous devez être connecté pour accéder à la page",401);
?>
<div id="user-form" class="user-form">
	<br>
	<div onclick="core_account_save()" class="btn btn-success float-right"><i class="fas fa-check"></i> Enregistrer</div>
	<h3><?php echo $myUser->fullName(); ?> <small class="text-muted">(<?php echo $myUser->login; ?>)</small></h3>
	<div class="clear"></div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<label for="avatar">Photo :</label><br/>
			<input id="avatar" data-type="image" name="avatar" class="form-control-file" value="<?php echo $myUser->getAvatar(); ?>" type="file" data-delete="core_account_avatar_delete(this)" data-default-src="img/default-avatar.png">
		</div>
		<div class="col-md-6">
			<div class="user-ranks">
				<label for="rank">Rangs :</label><br>
				<small class="text-muted">
					<?php 
					$ranks = array();
					if($myUser->superadmin == 1) {
						$ranks = array('Super Admin');
					} else {
						foreach($myUser->ranks[$myFirm->id] as $rank) 
							$ranks[] = $rank->label; 
					}

					foreach ($ranks as $i=>$rank): ?>
						<span class="badge badge-<?php echo $i%2==0 ? 'dark':'secondary'; ?>"><?php echo $rank; ?></span>
					<?php endforeach; ?>
				</small>
			</div>
		</div>
	</div><br>
	<div class="row">
		<div class="col-md-6">
			<label for="login">Identifiant :</label><small class="text-danger"> <?php echo ' (,'.$conf->get('login_forbidden_char').' interdits)'; ?> </small>
			<input required id="login" name="login" class="form-control" readonly="readonly" placeholder="Identifiant" value="<?php echo html_decode_utf8($myUser->login); ?>" type="text">
		</div>
		<div class="col-md-6">
			<label for="mail">Mail :</label>
			<input required id="mail" name="mail" class="form-control" placeholder="Mail" type="text" value="<?php echo $myUser->mail; ?>">
		</div>
	</div><br/>
	<div class="row">
		<div class="col-md-6">
			<label for="password">Mot de passe : </label><small class="text-danger"><?php echo empty($conf->get('password_forbidden_char'))?'':' ('.$conf->get('password_forbidden_char').' interdits)'; ?></small>
			<input required id="password" data-show-strength data-generator data-type="password"  name="password" class="form-control" placeholder="Mot de passe" type="password" autocomplete="new-password">
		</div>
		<div class="col-md-6">
			<label for="password2">Mot de passe (confirmation) : </label><small class="text-danger"><?php echo empty($conf->get('password_forbidden_char'))?'':'('.$conf->get('password_forbidden_char').' interdits)'; ?></small>
			<input required id="password2" data-type="password" name="password2" class="form-control" placeholder="Mot de passe (confirmation)" type="password" autocomplete="new-password">
		</div>
	</div><br>
	<div class="row">
		<div class="col-md-3">
			<label for="firstname">Prénom :</label>
			<input id="firstname" name="firstname" class="form-control" placeholder="Prénom" type="text" value="<?php echo html_decode_utf8($myUser->firstname()); ?>">
		</div>
		<div class="col-md-3">
			<label for="name">Nom :</label>
			<input id="name" name="name" class="form-control" placeholder="Nom" type="text" value="<?php echo html_decode_utf8($myUser->lastname()); ?>">
		</div>
		<div class="col-md-6">
			<label for="function">Fonction :</label>
			<input id="function" name="function" class="form-control" placeholder="ex: Responsable RH" type="text" value="<?php echo $myUser->function; ?>">
		</div>
	</div><br>
	<div class="row">
		<div class="col-md-6">
			<label for="phone">Téléphone fixe :</label>
			<input id="phone" name="phone" class="form-control" placeholder="N° Téléphone fixe" type="text" value="<?php echo $myUser->phone; ?>">
		</div><div class="col-md-6">
			<label for="mobile">Téléphone mobile professionnel :</label>
			<input id="mobile" name="mobile" class="form-control" placeholder="N° Téléphone mobile professionnel" type="text" value="<?php echo $myUser->mobile; ?>">
		</div>
		
	</div><br>
	<div class="row mb-2">
		<div class="col-md-6">
			<label for="">Compte créé le :</label>
			<input class="form-control-plaintext" readonly="readonly" type="text" value="<?php echo empty($myUser->created) ? '-' : date('d/m/Y H:i', $myUser->created); ?>">
		</div>
		<div class="col-md-6">
			<label for="manager">Manager :</label>
			<input id="manager" name="manager" readonly="readonly" class="form-control-plaintext" placeholder="Nom de manager" type="text" value="<?php echo is_object($myUser->manager) ? $myUser->manager->fullName():' - '; ?>">
		</div>
	</div><br>
	<?php Plugin::callHook('account_global'); ?>
</div>
