<?php 

global $myUser;
User::check_access('rank','configure');
$rank = Rank::getById($_['rank']);

$firms = Firm::loadAll();
$firms[] = Firm::anonymous_firm();
?>
<div class="row">
	<div class="col-md-12">
		<br>
		<h3 id="targetUid" data-rank="<?php echo $rank->id ?>">Droits pour le rang <span class="text-success"><?php echo $rank->label ?></span> :</h3>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<label class="mb-1">Établissement concerné :</label>
				<select onchange="core_right_search();" class="form-control" id="firm">
					<option value="0">Tous</option>
					<?php foreach($firms as $firm) : ?>
						<option value="<?php echo $firm->id; ?>"><?php echo $firm->label; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<br>		
		<div class="panel panel-default">
			<table id="rights" class="table table-striped">
				<thead class="bg-secondary text-white">
					<tr>
						<th>Libellé</th>
						<th class="text-center"></th>
						<th class="text-center">Consultation</th>
						<th class="text-center">Édition</th>
						<th class="text-center">Suppression</th>
						<th class="text-center">Configuration</th>
					</tr>
				</thead>
				<tbody>
					<tr class="hidden" data-scope="{{scope}}">
						<td class="align-middle">{{description}} <code>({{scope}})</code></td>
						<td class="align-middle text-center"><div class="btn btn-mini btn-warning" onclick="right_switch(this);"><i class="fas fa-redo"></i> Tout switcher</div></td>
						<td class="align-middle text-center">
							<input data-right="read" data-result="{{read}}" onchange="core_right_toggle(this)"  type="checkbox" autocomplete="off" name="read-{{scope}}" id="read-{{scope}}" data-type="checkbox">
						</td>
						<td class="align-middle text-center">
							<input data-right="edit" data-result="{{edit}}" onchange="core_right_toggle(this)" type="checkbox" autocomplete="off" name="edit-{{scope}}" id="edit-{{scope}}" data-type="checkbox">
						</td>
						<td class="align-middle text-center">
							<input data-right="delete" data-result="{{delete}}" onchange="core_right_toggle(this)" type="checkbox" autocomplete="off" name="delete-{{scope}}" id="delete-{{scope}}" data-type="checkbox">
						</td>
						<td class="align-middle text-center">
							<input data-right="configure" data-result="{{configure}}" onchange="core_right_toggle(this)" type="checkbox" autocomplete="off" name="configure-{{scope}}" id="configure-{{scope}}" data-type="checkbox">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>