<?php require_once __DIR__.DIRECTORY_SEPARATOR.'header.php'; ?>
<?php if(!$myUser->connected()) throw new Exception('Vous devez être connecté pour accéder à cette fonctionnalité', 401); 

$settingMenu = array();
$administrationMenu = false;
Plugin::callHook("menu_setting", array(&$settingMenu));

$_['section'] = !isset($_['section']) ? 'global': $_['section'];
$page = basename($_SERVER['PHP_SELF']);
?>

<div class="row">
	<div class="col-md-3">
		<div class="list-group">
			<span class="list-group-item"><h5 class="mb-0">Configuration</h5></span>
			<?php foreach($settingMenu as $item): 
				if(isset($item['category']) && $item['category']!='configuration') {
					$administrationMenu = true;
					continue;
				} ?>
				<a href="<?php echo isset($item['url'])?$item['url']:''; ?>" class="setting-menu-item list-group-item list-group-item-action <?php echo $item['url'] == $page.'?section='.$_['section']?'active':'text-primary'; ?>">
					<i class="<?php echo $item['icon']; ?>"></i> <span class="d-inline-block"><?php echo $item['label']; ?></span>
				</a>
			<?php endforeach; ?>
		</div>
		<br>
		<?php if($administrationMenu): ?>
		<div class="list-group">
			<span class="list-group-item"><h5 class="mb-0">Administration</h5></span>
			<?php foreach($settingMenu as $item):
				if(!isset($item['category']) || $item['category']!='administration') continue;
			?>
				<a href="<?php echo isset($item['url'])?$item['url']:''; ?>" class="setting-menu-item list-group-item list-group-item-action <?php echo $item['url'] == $page.'?section='.$_['section']?'active':'text-primary'; ?> ">
					<i class="<?php echo $item['icon']; ?>"></i> <span class="d-inline-block"><?php echo $item['label']; ?></span>
				</a>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="col-md-9"><?php Plugin::callHook("content_setting"); ?></div>
</div>
<br>
<?php require_once __ROOT__.'footer.php' ?>
