<?php 

global $myUser;
if($myUser->login=='') throw new Exception("Permissions insuffisantes", 403);
?>
<div class="row">
	<div class="col-md-12">
		<?php 

			if(!file_exists(__DIR__.SLASH.'.git')) throw new Exception("Fonctionnalité désactivée car le projet n'est pas versionné via GIT");
			//À voir pour le rendre dynamique au 
			//lieu d'avoir les 30 dernières modifs
			$stream = shell_exec('git log -30 --pretty=~~~%ct---%B~~~'); 
			$commits = explode('~~~',$stream); 
		?>
		<br>
		<h3>30 Dernières modifications :</h3>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<ul class="update-timeline">
				<?php
				$i=0;
				foreach($commits as $commit): 
				 	
					$infos = explode('---',$commit); 
					if(count($infos)<2) continue;
					$i++;
					list($time,$message) = $infos;

					$icons = array();
					$colors = array();
					$tags = array();
					
					if(preg_match('/Merge branch/i', $message)){
						$icons[]= 'fas fa-code-branch';
						$colors[]= 'info';
						$message = str_replace(array('Merge branch',' of ','Conflicts'),array('Fusion de branche',' de ','Conflits'),$message);
						$tags['merge'] = 1;
					}
					
					// Refactor de la regex pour accepter les syntaxes -Plugin: ou Plugin:
					$message = preg_replace('/(^-*|\n|\n-*)[\s\t]*([^:-]*)[\s\t]*:/iU', '<span class="update-commit-module mt-2"><i class="far fa-bookmark"></i> $2</span>', $message);
					
					$message = preg_replace_callback('/#([^\s]*)(\s|$)/iU', function($matches) use(&$tags){
						$tag = trim(strtolower($matches[1]));
						if($tag=='') return;

						if(!isset($tags[$tag])) $tags[$tag] = 0;
						$tags[$tag]++;
						return $matches[2];
					}, $message);

					if(isset($tags['feature'])){
						$icons[] = 'fas fa-plus-circle';
						$colors[] = 'success';
					}
					if(isset($tags['bug']) || isset($tags['fix'])){
						$icons[] = 'fas fa-bug';
						$colors[] = 'danger';
					}
					if(isset($tags['refactor']) || preg_match('/refactor|rework/i', $message)){
						$icons[] = 'fas fa-sync';
						$colors[] = 'warning';
					}
				
					if(count($icons)==0) $icons[] ='fas fa-pencil-alt';
					?>
				    <li <?php echo $i%2==0?'class="update-timeline-inverted"':'' ?>>
				    	<div class="update-timeline-badge <?php echo count($colors)==1?$colors[0]:''; ?>"  style="font-size: <?php echo 1.4/count($icons); ?>rem;<?php echo count($colors)>1?'background-color:#e83e8c;':''; ?>">
				    		<?php foreach($icons as $icon): ?>
				    			<i class="<?php echo $icon; ?>" style="font-size: <?php echo 1.4/count($icons); ?>rem"></i>
				    		<?php endforeach; ?>
				    	</div>
				    	
				    	<div class="update-timeline-panel">
				    		<div class="update-timeline-heading">
				    			<p><small class="text-muted"><i class="far fa-clock"></i> <?php echo relative_time($time); ?></small></p>
				    		</div>
				    		<div class="update-timeline-body">
				    			<p><?php echo nl2br($message); ?></p>
				    			<?php foreach($tags as $tag=>$count): ?>
				    				<span class="update-commit-tag"><i class="fas fa-tag"></i> <?php echo $tag ?><?php echo $count > 1 ? ' ('.$count.')':'' ?></span>
				    			<?php endforeach; ?>
				    		</div>
				    	</div>
				    </li>
			    <?php endforeach; ?>
			    </ul>
			</div>
		</div>
		<br>		
	</div>
</div>