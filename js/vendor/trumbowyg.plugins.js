
/* ===========================================================
 * trumbowyg.pasteImageKiss.js v1.0 basé sur trumbowyg.pasteImage.js v1.0
 * Basic base64 paste plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Valentin CARRUESCO depuis Alexandre Demode (Alex-D)
 */


 (function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        plugins: {
            pasteImageKiss: {
                init: function (trumbowyg) {
                    trumbowyg.pasteHandlers.push(function (pasteEvent) {
                        try {
                            var items = (pasteEvent.originalEvent || pasteEvent).clipboardData.items,
                            mustPreventDefault = false,
                            reader;

                            for (var i = items.length - 1; i >= 0; i -= 1) {
                                if (items[i].type.match(/^image\//)) {

                                    var file = items[i].getAsFile();
                                    if(file.size > 5000 && !trumbowyg.o.allowBigPaste){
                                        //mode upload, fichier lourd
                                        if(trumbowyg.o.uploadAction){

                                            
                                            $.action({
                                                action : trumbowyg.o.uploadAction,
                                                stream : file,
                                                name : Date.now().getTime()+(Math.floor(Math.random() * Math.floor(1000)))+'.png'
                                            },function(response){
                                                
                                                trumbowyg.execCmd('insertHtml',response.html, false, true);
                                            });
                                        }else{
                                            //Si aucune action d'upload prévue
                                            trumbowyg.execCmd('insertHtml','<span class="text-danger">Image Trop lourde ('+file.size+' octets) et upload non implémenté</span>', false, true);
                                        }
                                    }else{
                                        //mode base 64 fichier léger
                                        reader = new FileReader();
                                        reader.onloadend = function (event) {
                                            var base64 = event.target.result;
                                            trumbowyg.execCmd('insertImage',base64, false, true);
                                        };
                                        reader.readAsDataURL(file);
                                       
                                    }

                               

                                    mustPreventDefault = true;
                                }
                            }

                            if (mustPreventDefault) {
                                pasteEvent.stopPropagation();
                                pasteEvent.preventDefault();
                            }
                        } catch (c) {
                        }
                    });
                }
            }
        }
    });
})(jQuery);


/* ===========================================================
 * trumbowyg.core.cleanpaste.js v1.0
 * Clean les balises word
 * ===========================================================
 * Authors : Valentin CARRUESCO
 */

 (function ($) {
    'use strict';

    // clean editor
    // this will clean the inserted contents
    // it does a compare, before and after paste to determine the
    // pasted contents
    $.extend(true, $.trumbowyg, {
        plugins: {
            cleanPaste: {
                init: function (trumbowyg) {
                    trumbowyg.pasteHandlers.push(function () {
                        setTimeout(function () {
                          try {
                                trumbowyg.$ed.find("*").filter(function(){
                                    return /([^><]*):([^>]*)/i.test(this.nodeName);
                                }).remove();
                          } catch (c) {
                          }
                      }, 0);
                    });
                }
            }
        }
    });
})(jQuery);


/* ===========================================================
 * trumbowyg drag & drop
 * Permet l'upload en drag & drop
 * ===========================================================
 * Authors : Valentin CARRUESCO
 */

 (function ($) {
    'use strict';


    $.extend(true, $.trumbowyg, {
        plugins: {
            dragAndDrop: {
                init: function (trumbowyg) {
                    
                    //Attens que le dom soit chargé (sinon  trumbowyg.$ed n'est pas accessible)
                    trumbowyg.$c.on('tbwinit', function () {
           
                        //test if d&d is enabled n browser
                        var div = document.createElement('div');
                        var dragAndDropEnabled = (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
                        if (!dragAndDropEnabled) return;


                        trumbowyg.$ed
                        .off('click drag dragstart dragend dragenter dragleave drop dragover').on('click', function (e) {
                           e.preventDefault();
                           e.stopPropagation();
                        })
                        .on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                        })
                        .on('dragover dragenter', function () {
                            trumbowyg.$ed.addClass('wysiwyg-dragover');
                        })
                        .on('dragleave dragend drop', function () {
                            trumbowyg.$ed.removeClass('wysiwyg-dragover');
                        })
                        .on('drop', function (e) {
                            var droppedFiles = e.originalEvent.dataTransfer.files;
                            
                            if(trumbowyg.o.uploadAction){
                                $.each(droppedFiles, function (i, file) {
                                    var uid = btoa(file.name);
                                    trumbowyg.execCmd('insertHtml','<span class="text-muted wysiwyg-upload" data-uid="'+uid+'"><i class="fas fa-circle-notch fa-spin"></i> '+file.name+' envois en cours...</span>', false, true);
                                    
                                    $.action({
                                        action : trumbowyg.o.uploadAction,
                                        stream : file,
                                        name : file.name
                                    },function(response){
                                        $('.wysiwyg-upload[data-uid="'+uid+'"]').remove();
                                        trumbowyg.execCmd('insertHtml',response.html, false, true);
                                    },function(error){

                                        $('.wysiwyg-upload[data-uid="'+uid+'"]').remove();
                                    });
                                });
                            }else{
                                //Si aucune action d'upload prévue
                                trumbowyg.execCmd('insertHtml','<span class="text-danger">Upload non implémenté dans ce composant</span>', false, true);
                            }
                        });
                    });
                  


                }
            }
        }
    });
})(jQuery);



/**/



/* ===========================================================
 * resize image
 * Permet le resize des images
 * ===========================================================
 * Authors : Valentin CARRUESCO
 */

 (function ($) {
    'use strict';


    $.extend(true, $.trumbowyg, {
        plugins: {
            resizeImage: {
                init: function (trumbowyg) {
                    
                    //Attens que le dom soit chargé (sinon  trumbowyg.$ed n'est pas accessible)
                    trumbowyg.$c.on('tbwinit', function () {
                     
                       trumbowyg.$ed.on('click','img',function(){

                        $(this).resizable({
                            aspectRatio : true,
                            resize : function(event, ui ){
                         
                                ui.originalElement.css({
                                    height : ui.size.height+'px',
                                    width : ui.size.width+'px'
                                });

                            }
                        });
                       });


                        trumbowyg.$ed.click(function(e){
                            if($(e.target).hasClass('ui-resizable')) return;
                            trumbowyg.$ed.find('.ui-resizable').resizable('destroy');
                        });

                    });

                }
            }
        }
    });
})(jQuery);



/* ===========================================================
 * trumbowyg.fontfamily.js v1.2
 */


 (function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            en: {
                fontFamily: 'Font'
            },
            da: {
                fontFamily: 'Skrifttype'
            },
            fr: {
                fontFamily: 'Police'
            },
            de: {
                fontFamily: 'Schriftart'
            },
            nl: {
                fontFamily: 'Lettertype'
            },
            tr: {
                fontFamily: 'Yazı Tipi'
            },
            zh_tw: {
                fontFamily: '字體',
            },
            pt_br: {
                fontFamily: 'Fonte',
            },
            ko: {
                fontFamily: '글꼴'
            },
        }
    });
    // jshint camelcase:true

    var defaultOptions = {
        fontList: [
            {name: 'Arial', family: 'Arial, Helvetica, sans-serif'},
            {name: 'Arial Black', family: 'Arial Black, Gadget, sans-serif'},
            {name: 'Comic Sans', family: 'Comic Sans MS, Textile, cursive, sans-serif'},
            {name: 'Courier New', family: 'Courier New, Courier, monospace'},
            {name: 'Georgia', family: 'Georgia, serif'},
            {name: 'Impact', family: 'Impact, Charcoal, sans-serif'},
            {name: 'Lucida Console', family: 'Lucida Console, Monaco, monospace'},
            {name: 'Lucida Sans', family: 'Lucida Sans Uncide, Lucida Grande, sans-serif'},
            {name: 'Palatino', family: 'Palatino Linotype, Book Antiqua, Palatino, serif'},
            {name: 'Tahoma', family: 'Tahoma, Geneva, sans-serif'},
            {name: 'Times New Roman', family: 'Times New Roman, Times, serif'},
            {name: 'Trebuchet', family: 'Trebuchet MS, Helvetica, sans-serif'},
            {name: 'Verdana', family: 'Verdana, Geneva, sans-serif'}
        ]
    };

    // Add dropdown with web safe fonts
    $.extend(true, $.trumbowyg, {
        plugins: {
            fontfamily: {
                init: function (trumbowyg) {
                    trumbowyg.o.plugins.fontfamily = $.extend({},
                        defaultOptions,
                        trumbowyg.o.plugins.fontfamily || {}
                    );

                    trumbowyg.addBtnDef('fontfamily', {
                        dropdown: buildDropdown(trumbowyg),
                        hasIcon: false,
                        text: trumbowyg.lang.fontFamily
                    });
                }
            }
        }
    });

    function buildDropdown(trumbowyg) {
        var dropdown = [];

        $.each(trumbowyg.o.plugins.fontfamily.fontList, function (index, font) {
            trumbowyg.addBtnDef('fontfamily_' + index, {
                title: '<span style="font-family: ' + font.family + ';">' + font.name + '</span>',
                hasIcon: false,
                fn: function () {
                    trumbowyg.execCmd('fontName', font.family, false);
                }
            });
            dropdown.push('fontfamily_' + index);
        });

        return dropdown;
    }
})(jQuery);


/* ===========================================================
 * trumbowyg.mention.js v1.0
 * Allow mention with @,# or any key
 * ===========================================================
 * Authors : Valentin CARRUESCO
 */

 (function ($) {
    'use strict';

    var o = {
        //Supprime la totalité du mention si un backspace entame le mention
        autoDelete : true,
        //Ne déclenche la touche que si elle est précédée d'un espace, d'un saut de ligne ou d'une nouvelle balise
        triggerBreak : true
    };

    $.extend(true, $.trumbowyg, {
        plugins: {
            mention: {
                init: function (trumbowyg) {
                    var object = this;
                    object.altGrPressed  = false;
                    trumbowyg.o.plugins.mention = $.extend(true, {},o,trumbowyg.o.plugins.mention || {});
                    
                    var keyMap = {
                        48 : {
                            label : '@',
                            check : function(event){
                               return object.altGrPressed;
                            }
                        },
                        51 : {
                            label : '#',
                            check : function(event){
                               return object.altGrPressed;
                            }
                        }
                    }

                    $(trumbowyg.$ta).parent().on('keydown', function (e) {
                        var code = e.which || e.keyCode ;

                        //verifie si alt+gr est enfoncé
                        if(code==18){ 
                            object.altGrPressed = true;
                            return;
                        }

                       

                        //Supprime les mentions en totalité sur un backspace
                        if(trumbowyg.o.plugins.mention.autoDelete && code==8){
                              var editor = trumbowyg.$ed;
                              var textarea = trumbowyg.$ta;
                              textarea.trumbowyg('saveRange');

                              if(!textarea.trumbowyg('getRange') || !textarea.trumbowyg('getRange').endContainer) return;
                              
                              var parent = $(textarea.trumbowyg('getRange').endContainer.parentElement);
                            if(parent.attr('data-mention-type')!= null){
                                parent.remove();
                                editor.trigger('keyup');
                            }
                        }

                        //if(Object.keys(trumbowyg.o.plugins.mention.keys).indexOf(""+code) == -1) return;

                        if(keyMap[code] == null) return;
                        
                        var char = keyMap[code].label;

                        if(!trumbowyg.o.plugins.mention.keys || Object.keys(trumbowyg.o.plugins.mention.keys).indexOf(char) == -1) return;
                        
                        if(keyMap[code].check && !keyMap[code].check(event)) return;
                        

                       

                        var editor = trumbowyg.$ed;
                        var textarea = trumbowyg.$ta;
                        var range = trumbowyg.$ta.trumbowyg('getRange');
                        var lastChar = !range || !range.startContainer || !range.startContainer.data ?  '' : range.startContainer.data;
                        lastChar = lastChar.length>0 ? lastChar.substring(lastChar.length-1) : '';
                       
                        //si le caractere trigger n'est pas lancé apres un espace ou un saut de ligne on ne fait rien
                        if(trumbowyg.o.plugins.mention.triggerBreak && lastChar != '' && lastChar != ' ') return;
                        
                        var key = trumbowyg.o.plugins.mention.keys[char];
                        if(!key.load) return;
                        var data = {
                            editor : editor,
                            event : e,
                            textarea : textarea,
                            lastChar : lastChar
                        }

                        data.textarea.trumbowyg('saveRange');
                        key.load(data);
                    }).on('keyup', function (e) {
                        var code = e.which || e.keyCode ;
                        //désactive le altgr mode si relaché
                        if(code==18){ 
                            object.altGrPressed = false;
                            return;
                        }

                    });
                }
                
            }
        }
    })
})(jQuery);


/**
Font size core, ne pas upgrade !!
*/

(function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            fr: {
                fontsize: 'Taille de la police',
                fontsizes: {
                    'x-small': 'Très petit',
                    'small': 'Petit',
                    'medium': 'Normal',
                    'large': 'Grand',
                    'x-large': 'Très grand',
                    'custom': 'Taille personnalisée'
                },
                fontCustomSize: {
                    title: 'Taille de police personnalisée',
                    label: 'Taille de la police',
                    value: '48px'
                }
            }
        }
    });

    // Add dropdown with font sizes
    $.extend(true, $.trumbowyg, {
        plugins: {
            fontsize: {
                init: function (trumbowyg) {
                    trumbowyg.o.plugins.fontsize = $.extend({},
                        {
                            sizeList: [
                                '8px',
                                '9px',
                                '10px',
                                '11px',
                                '12px',
                                '14px',
                                '16px',
                                '18px',
                                '20px',
                                '22px',
                                '24px',
                                '26px',
                                '28px',
                                '36px',
                                '48px',
                                '72px'
                            ],
                            allowCustomSize: true
                        },
                        trumbowyg.o.plugins.fontsize || {}
                    );

                    trumbowyg.addBtnDef('fontsize', {
                        dropdown: buildDropdown(trumbowyg)
                    });
                }
            }
        }
    });

    function setFontSize(trumbowyg, size) {
        trumbowyg.$ed.focus();
        trumbowyg.saveRange();

        // Temporary size
        trumbowyg.execCmd('fontSize', '1');

        // Find <font> elements that were added and change to <span> with chosen size
        trumbowyg.$ed.find('font[size="1"]').replaceWith(function() {
            return $('<span/>', {
                css: { 'font-size': size },
                html: this.innerHTML,
            });
        });

        // Remove and leftover <span> elements
        $(trumbowyg.range.startContainer.parentElement).find('span[style=""]').contents().unwrap();

        trumbowyg.restoreRange();
        trumbowyg.syncCode();
        trumbowyg.$c.trigger('tbwchange');
    }

    function buildDropdown(trumbowyg) {
        var dropdown = [];

        $.each(trumbowyg.o.plugins.fontsize.sizeList, function (index, size) {
            trumbowyg.addBtnDef('fontsize_' + size, {
                text: '<span style="">' + (trumbowyg.lang.fontsizes[size] || size) + '</span>',
                hasIcon: false,
                fn: function () {
                    setFontSize(trumbowyg, size);
                }
            });
            dropdown.push('fontsize_' + size);
        });

        if (trumbowyg.o.plugins.fontsize.allowCustomSize) {
            var customSizeButtonName = 'fontsize_custom';
            var customSizeBtnDef = {
                fn: function () {
                    trumbowyg.openModalInsert(trumbowyg.lang.fontCustomSize.title,
                        {
                            size: {
                                label: trumbowyg.lang.fontCustomSize.label,
                                value: trumbowyg.lang.fontCustomSize.value
                            }
                        },
                        function (form) {
                            setFontSize(trumbowyg, form.size);
                            return true;
                        }
                    );
                },
                text: '<span style="font-size: medium;">' + trumbowyg.lang.fontsizes.custom + '</span>',
                hasIcon: false
            };
            trumbowyg.addBtnDef(customSizeButtonName, customSizeBtnDef);
            dropdown.push(customSizeButtonName);
        }

        return dropdown;
    }
})(jQuery);
/* ===========================================================
 * trumbowyg.colors.js v1.2
 * Colors picker plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Alexandre Demode (Alex-D)
 *          Twitter : @AlexandreDemode
 *          Website : alex-d.fr
 */

 (function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            cs: {
                foreColor: 'Barva textu',
                backColor: 'Barva pozadí'
            },
            en: {
                foreColor: 'Text color',
                backColor: 'Background color'
            },
            fr: {
                foreColor: 'Couleur du texte',
                backColor: 'Couleur de fond'
            },
            nl: {
                foreColor: 'Tekstkleur',
                backColor: 'Achtergrondkleur'
            },
            sk: {
                foreColor: 'Farba textu',
                backColor: 'Farba pozadia'
            },
            zh_cn: {
                foreColor: '文字颜色',
                backColor: '背景颜色'
            },
            ru: {
                foreColor: 'Цвет текста',
                backColor: 'Цвет выделения текста'
            },
            ja: {
                foreColor: '文字色',
                backColor: '背景色'
            },
            tr: {
                foreColor: 'Yazı rengi',
                backColor: 'Arkaplan rengi'
            }
        }
    });

    // jshint camelcase:true


    function hex(x) {
        return ('0' + parseInt(x).toString(16)).slice(-2);
    }

    function colorToHex(rgb) {
        if (rgb.search('rgb') === -1) {
            return rgb.replace('#', '');
        } else if (rgb === 'rgba(0, 0, 0, 0)') {
            return 'transparent';
        } else {
            rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
            return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }
    }

    function colorTagHandler(element, trumbowyg) {
        var tags = [];

        if (!element.style) {
            return tags;
        }

        // background color
        if (element.style.backgroundColor !== '') {
            var backColor = colorToHex(element.style.backgroundColor);
            if (trumbowyg.o.plugins.colors.colorList.indexOf(backColor) >= 0) {
                tags.push('backColor' + backColor);
            } else {
                tags.push('backColorFree');
            }
        }

        // text color
        var foreColor;
        if (element.style.color !== '') {
            foreColor = colorToHex(element.style.color);
        } else if (element.hasAttribute('color')) {
            foreColor = colorToHex(element.getAttribute('color'));
        }
        if (foreColor) {
            if (trumbowyg.o.plugins.colors.colorList.indexOf(foreColor) >= 0) {
                tags.push('foreColor' + foreColor);
            } else {
                tags.push('foreColorFree');
            }
        }

        return tags;
    }

    var defaultOptions = {
        colorList: ['ffffff', '000000', 'eeece1', '1f497d', '4f81bd', 'c0504d', '9bbb59', '8064a2', '4bacc6', 'f79646', 'ffff00', 'f2f2f2', '7f7f7f', 'ddd9c3', 'c6d9f0', 'dbe5f1', 'f2dcdb', 'ebf1dd', 'e5e0ec', 'dbeef3', 'fdeada', 'fff2ca', 'd8d8d8', '595959', 'c4bd97', '8db3e2', 'b8cce4', 'e5b9b7', 'd7e3bc', 'ccc1d9', 'b7dde8', 'fbd5b5', 'ffe694', 'bfbfbf', '3f3f3f', '938953', '548dd4', '95b3d7', 'd99694', 'c3d69b', 'b2a2c7', 'b7dde8', 'fac08f', 'f2c314', 'a5a5a5', '262626', '494429', '17365d', '366092', '953734', '76923c', '5f497a', '92cddc', 'e36c09', 'c09100', '7f7f7f', '0c0c0c', '1d1b10', '0f243e', '244061', '632423', '4f6128', '3f3151', '31859b', '974806', '7f6000']
    };

    // Add all colors in two dropdowns
    $.extend(true, $.trumbowyg, {
        plugins: {
            color: {
                init: function (trumbowyg) {
                    trumbowyg.o.plugins.colors = trumbowyg.o.plugins.colors || defaultOptions;
                    var foreColorBtnDef = {
                        dropdown: buildDropdown('foreColor', trumbowyg)
                    },
                    backColorBtnDef = {
                        dropdown: buildDropdown('backColor', trumbowyg)
                    };

                    trumbowyg.addBtnDef('foreColor', foreColorBtnDef);
                    trumbowyg.addBtnDef('backColor', backColorBtnDef);
                },
                tagHandler: colorTagHandler
            }
        }
    });

    function buildDropdown(fn, trumbowyg) {
        var dropdown = [];

        $.each(trumbowyg.o.plugins.colors.colorList, function (i, color) {
            var btn = fn + color,
            btnDef = {
                fn: fn,
                forceCss: true,
                param: '#' + color,
                style: 'background-color: #' + color + ';'
            };
            trumbowyg.addBtnDef(btn, btnDef);
            dropdown.push(btn);
        });

        var removeColorButtonName = fn + 'Remove',
        removeColorBtnDef = {
            fn: 'removeFormat',
            param: fn,
            style: 'background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAG0lEQVQIW2NkQAAfEJMRmwBYhoGBYQtMBYoAADziAp0jtJTgAAAAAElFTkSuQmCC);'
        };
        trumbowyg.addBtnDef(removeColorButtonName, removeColorBtnDef);
        dropdown.push(removeColorButtonName);

        // add free color btn
        var freeColorButtonName = fn + 'Free',
        freeColorBtnDef = {
            fn: function () {
                trumbowyg.openModalInsert(trumbowyg.lang[fn],
                {
                    color: {
                        label: fn,
                        value: '#FFFFFF'
                    }
                },
                        // callback
                        function (values) {
                            trumbowyg.execCmd(fn, values.color);
                            return true;
                        }
                        );
            },
            text: '#',
                // style adjust for displaying the text
                style: 'text-indent: 0;line-height: 20px;padding: 0 5px;'
            };
            trumbowyg.addBtnDef(freeColorButtonName, freeColorBtnDef);
            dropdown.push(freeColorButtonName);

            return dropdown;
        }
    })(jQuery);



/* ===========================================================
 * trumbowyg.table.custom.js v2.0
 * Table plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Sven Dunemann [dunemann@forelabs.eu]
 */

(function ($) {
    'use strict';

    var defaultOptions = {
        rows: 8,
        columns: 8,
        styler: 'table'
    };

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            en: {
                table: 'Insert table',
                tableAddRow: 'Add row',
                tableAddRowAbove: 'Add row above',
                tableAddColumnLeft: 'Add column to the left',
                tableAddColumn: 'Add column to the right',
                tableDeleteRow: 'Delete row',
                tableDeleteColumn: 'Delete column',
                tableDestroy: 'Delete table',
                error: 'Error'
            },
            da: {
                table: 'Indsæt tabel',
                tableAddRow: 'Tilføj række',
                tableAddRowAbove: 'Tilføj række',
                tableAddColumnLeft: 'Tilføj kolonne',
                tableAddColumn: 'Tilføj kolonne',
                tableDeleteRow: 'Slet række',
                tableDeleteColumn: 'Slet kolonne',
                tableDestroy: 'Slet tabel',
                error: 'Fejl'
            },
            de: {
                table: 'Tabelle einfügen',
                tableAddRow: 'Zeile hinzufügen',
                tableAddRowAbove: 'Zeile hinzufügen',
                tableAddColumnLeft: 'Spalte hinzufügen',
                tableAddColumn: 'Spalte hinzufügen',
                tableDeleteRow: 'Zeile löschen',
                tableDeleteColumn: 'Spalte löschen',
                tableDestroy: 'Tabelle löschen',
                error: 'Error'
            },
            sk: {
                table: 'Vytvoriť tabuľky',
                tableAddRow: 'Pridať riadok',
                tableAddRowAbove: 'Pridať riadok',
                tableAddColumnLeft: 'Pridať stĺpec',
                tableAddColumn: 'Pridať stĺpec',
                error: 'Chyba'
            },
            fr: {
                table: 'Insérer un tableau',
                tableAddRow: 'Ajouter un ligne en dessous',
                tableAddRowAbove: 'Ajouter une ligne au dessus',
                tableAddColumnLeft: 'Ajouter une colonne à gauche',
                tableAddColumn: 'Ajouter une colonne à droite',
                tableDeleteRow: 'Effacer la ligne',
                tableDeleteColumn: 'Effacer la colonne',
                tableDestroy: 'Effacer le tableau',
                error: 'Erreur'
            },
            cs: {
                table: 'Vytvořit příkaz Table',
                tableAddRow: 'Přidat řádek',
                tableAddRowAbove: 'Přidat řádek',
                tableAddColumnLeft: 'Přidat sloupec',
                tableAddColumn: 'Přidat sloupec',
                error: 'Chyba'
            },
            ru: {
                table: 'Вставить таблицу',
                tableAddRow: 'Добавить строку',
                tableAddRowAbove: 'Добавить строку',
                tableAddColumnLeft: 'Добавить столбец',
                tableAddColumn: 'Добавить столбец',
                tableDeleteRow: 'Удалить строку',
                tableDeleteColumn: 'Удалить столбец',
                tableDestroy: 'Удалить таблицу',
                error: 'Ошибка'
            },
            ja: {
                table: '表の挿入',
                tableAddRow: '行の追加',
                tableAddRowAbove: '行の追加',
                tableAddColumnLeft: '列の追加',
                tableAddColumn: '列の追加',
                error: 'エラー'
            },
            tr: {
                table: 'Tablo ekle',
                tableAddRow: 'Satır ekle',
                tableAddRowAbove: 'Satır ekle',
                tableAddColumnLeft: 'Kolon ekle',
                tableAddColumn: 'Kolon ekle',
                error: 'Hata'
            },
            zh_tw: {
                table: '插入表格',
                tableAddRow: '加入行',
                tableAddRowAbove: '加入行',
                tableAddColumnLeft: '加入列',
                tableAddColumn: '加入列',
                tableDeleteRow: '刪除行',
                tableDeleteColumn: '刪除列',
                tableDestroy: '刪除表格',
                error: '錯誤'
            },
            id: {
                table: 'Sisipkan tabel',
                tableAddRow: 'Sisipkan baris',
                tableAddRowAbove: 'Sisipkan baris',
                tableAddColumnLeft: 'Sisipkan kolom',
                tableAddColumn: 'Sisipkan kolom',
                tableDeleteRow: 'Hapus baris',
                tableDeleteColumn: 'Hapus kolom',
                tableDestroy: 'Hapus tabel',
                error: 'Galat'
            },
            pt_br: {
                table: 'Inserir tabela',
                tableAddRow: 'Adicionar linha',
                tableAddRowAbove: 'Adicionar linha',
                tableAddColumnLeft: 'Adicionar coluna',
                tableAddColumn: 'Adicionar coluna',
                tableDeleteRow: 'Deletar linha',
                tableDeleteColumn: 'Deletar coluna',
                tableDestroy: 'Deletar tabela',
                error: 'Erro'
            },
            ko: {
                table: '표 넣기',
                tableAddRow: '줄 추가',
                tableAddRowAbove: '줄 추가',
                tableAddColumnLeft: '칸 추가',
                tableAddColumn: '칸 추가',
                tableDeleteRow: '줄 삭제',
                tableDeleteColumn: '칸 삭제',
                tableDestroy: '표 지우기',
                error: '에러'
            },
            // jshint camelcase:true
        },

        plugins: {
            table: {
                init: function (t) {
                    t.o.plugins.table = $.extend(true, {}, defaultOptions, t.o.plugins.table || {});

                    var buildButtonDef = {
                        fn: function () {
                            t.saveRange();

                            var btnName = 'table';

                            var dropdownPrefix = t.o.prefix + 'dropdown',
                                dropdownOptions = { // the dropdown
                                    class: dropdownPrefix + '-' + btnName + ' ' + dropdownPrefix + ' ' + t.o.prefix + 'fixed-top'
                                };
                            dropdownOptions['data-' + dropdownPrefix] = btnName;
                            var $dropdown = $('<div/>', dropdownOptions);

                            if (t.$box.find('.' + dropdownPrefix + '-' + btnName).length === 0) {
                                t.$box.append($dropdown.hide());
                            } else {
                                $dropdown = t.$box.find('.' + dropdownPrefix + '-' + btnName);
                            }

                            // clear dropdown
                            $dropdown.html('');

                            // when active table show AddRow / AddColumn
                            if (t.$box.find('.' + t.o.prefix + 'table-button').hasClass(t.o.prefix + 'active-button')) {
                                $dropdown.append(t.buildSubBtn('tableAddRowAbove'));
                                $dropdown.append(t.buildSubBtn('tableAddRow'));
                                $dropdown.append(t.buildSubBtn('tableAddColumnLeft'));
                                $dropdown.append(t.buildSubBtn('tableAddColumn'));
                                $dropdown.append(t.buildSubBtn('tableDeleteRow'));
                                $dropdown.append(t.buildSubBtn('tableDeleteColumn'));
                                $dropdown.append(t.buildSubBtn('tableDestroy'));
                            } else {
                                var tableSelect = $('<table/>');
                                $('<tbody/>').appendTo(tableSelect);
                                for (var i = 0; i < t.o.plugins.table.rows; i += 1) {
                                    var row = $('<tr/>').appendTo(tableSelect);
                                    for (var j = 0; j < t.o.plugins.table.columns; j += 1) {
                                        $('<td/>').appendTo(row);
                                    }
                                }
                                tableSelect.find('td').on('mouseover', tableAnimate);
                                tableSelect.find('td').on('mousedown', tableBuild);

                                $dropdown.append(tableSelect);
                                $dropdown.append($('<div class="trumbowyg-table-size">1x1</div>'));
                            }

                            t.dropdown(btnName);
                        }
                    };

                    var tableAnimate = function(columnEvent) {
                        var column = $(columnEvent.target),
                            table = column.closest('table'),
                            colIndex = this.cellIndex,
                            rowIndex = this.parentNode.rowIndex;

                        // reset all columns
                        table.find('td').removeClass('active');

                        for (var i = 0; i <= rowIndex; i += 1) {
                            for (var j = 0; j <= colIndex; j += 1) {
                                table.find('tr:nth-of-type('+(i+1)+')').find('td:nth-of-type('+(j+1)+')').addClass('active');
                            }
                        }

                        // set label
                        table.next('.trumbowyg-table-size').html((colIndex+1) + 'x' + (rowIndex+1));
                    };

                    var tableBuild = function() {
                        t.saveRange();

                        var tabler = $('<table/>');
                        $('<tbody/>').appendTo(tabler);
                        if (t.o.plugins.table.styler) {
                            tabler.attr('class', t.o.plugins.table.styler);
                        }

                        var colIndex = this.cellIndex,
                            rowIndex = this.parentNode.rowIndex;

                        for (var i = 0; i <= rowIndex; i += 1) {
                            var row = $('<tr></tr>').appendTo(tabler);
                            for (var j = 0; j <= colIndex; j += 1) {
                                $('<td/>').appendTo(row);
                            }
                        }

                        t.range.deleteContents();
                        t.range.insertNode(tabler[0]);
                        t.$c.trigger('tbwchange');
                    };

                    var addRow = {
                        title: t.lang.tableAddRow,
                        text: t.lang.tableAddRow,
                        ico: 'row-below',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode;
                            var focusedRow = $(node).closest('tr');
                            var table = $(node).closest('table');

                            if(table.length > 0) {
                                var row = $('<tr/>');
                                // add columns according to current columns count
                                for (var i = 0; i < table.find('tr')[0].childElementCount; i += 1) {
                                    $('<td/>').appendTo(row);
                                }
                                // add row to table
                                focusedRow.after(row);
                            }

                            t.syncCode();
                        }
                    };

                    var addRowAbove = {
                        title: t.lang.tableAddRowAbove,
                        text: t.lang.tableAddRowAbove,
                        ico: 'row-above',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode;
                            var focusedRow = $(node).closest('tr');
                            var table = $(node).closest('table');

                            if(table.length > 0) {
                                var row = $('<tr/>');
                                // add columns according to current columns count
                                for (var i = 0; i < table.find('tr')[0].childElementCount; i += 1) {
                                    $('<td/>').appendTo(row);
                                }
                                // add row to table
                                focusedRow.before(row);
                            }

                            t.syncCode();
                        }
                    };

                    var addColumn = {
                        title: t.lang.tableAddColumn,
                        text: t.lang.tableAddColumn,
                        ico: 'col-right',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode;
                            var focusedCol = $(node).closest('td');
                            var table = $(node).closest('table');
                            var focusedColIdx = focusedCol.index();

                            if(table.length > 0) {
                                $(table).find('tr').each(function() {
                                    $($(this).children()[focusedColIdx]).after('<td></td>');
                                });
                            }

                            t.syncCode();
                        }
                    };

                    var addColumnLeft = {
                        title: t.lang.tableAddColumnLeft,
                        text: t.lang.tableAddColumnLeft,
                        ico: 'col-left',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode;
                            var focusedCol = $(node).closest('td');
                            var table = $(node).closest('table');
                            var focusedColIdx = focusedCol.index();

                            if(table.length > 0) {
                                $(table).find('tr').each(function() {
                                    $($(this).children()[focusedColIdx]).before('<td></td>');
                                });
                            }

                            t.syncCode();
                        }
                    };

                    var destroy = {
                        title: t.lang.tableDestroy,
                        text: t.lang.tableDestroy,
                        ico: 'table-delete',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode,
                                table = $(node).closest('table');

                            table.remove();

                            t.syncCode();
                        }
                    };

                    var deleteRow = {
                        title: t.lang.tableDeleteRow,
                        text: t.lang.tableDeleteRow,
                        ico: 'row-delete',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode,
                                row = $(node).closest('tr');

                            row.remove();

                            t.syncCode();
                        }
                    };

                    var deleteColumn = {
                        title: t.lang.tableDeleteColumn,
                        text: t.lang.tableDeleteColumn,
                        ico: 'col-delete',

                        fn: function () {
                            t.saveRange();

                            var node = t.doc.getSelection().focusNode,
                                table = $(node).closest('table'),
                                td = $(node).closest('td'),
                                cellIndex = td.index();

                            $(table).find('tr').each(function() {
                                $(this).find('td:eq(' + cellIndex + ')').remove();
                            });

                            t.syncCode();
                        }
                    };

                    t.addBtnDef('table', buildButtonDef);
                    t.addBtnDef('tableAddRowAbove', addRowAbove);
                    t.addBtnDef('tableAddRow', addRow);
                    t.addBtnDef('tableAddColumnLeft', addColumnLeft);
                    t.addBtnDef('tableAddColumn', addColumn);
                    t.addBtnDef('tableDeleteRow', deleteRow);
                    t.addBtnDef('tableDeleteColumn', deleteColumn);
                    t.addBtnDef('tableDestroy', destroy);
                }
            }
        }
    });
})(jQuery);



/**
Vertical align custom, NE PAS UPGRADE !!
*/

(function ($) {
    'use strict';

    //Création d'un boutton spécial pour les sauts de page
    $.extend(true, $.trumbowyg, {
        plugins: {
            alignDropdown: {
                init: function (trumbowyg) {

                    var dropdownButtonName = {
                        dropdown: ['alignTop', 'alignBottom', 'alignMiddle'],
                        title: 'Alignement vertical',
                        // ico: 'iconName',
                        text: '<strong>↨</strong>',
                        hasIcon: false
                    }

                    var alignTopBtnDef = {
                        fn: function () {
                            trumbowyg.saveRange();

                            $(trumbowyg.range.startContainer.parentElement).css('vertical-align', 'top');
                            trumbowyg.syncCode();
                        },
                        title: 'Alignement en haut',
                        class: 'trumbowyg-aligntop-button fas fa-long-arrow-alt-up',
                        text: '<strong>↑</strong> Aligné en haut',
                        hasIcon: false
                    };
                    var alignBottomBtnDef = {
                        fn: function () {
                            trumbowyg.saveRange();

                            $(trumbowyg.range.startContainer.parentElement).css('vertical-align', 'bottom');
                            trumbowyg.syncCode();
                        },
                        title: 'Alignement en bas',
                        class: 'trumbowyg-aligntop-button fas fa-long-arrow-alt-up',
                        text: '<strong>↓</strong> Aligné en bas',
                        hasIcon: false
                    };
                    var alignMiddleBtnDef = {
                        fn: function () {
                            trumbowyg.saveRange();

                            $(trumbowyg.range.startContainer.parentElement).css('vertical-align', 'middle');
                            trumbowyg.syncCode();
                        },
                        title: 'Alignement au milieu',
                        class: 'trumbowyg-aligntop-button fas fa-long-arrow-alt-up',
                        text: '<strong>↔</strong> Aligné au milieu',
                        hasIcon: false
                    };
                    
                    trumbowyg.addBtnDef('alignDropdown',dropdownButtonName);
                    trumbowyg.addBtnDef('alignTop',alignTopBtnDef);
                    trumbowyg.addBtnDef('alignBottom',alignBottomBtnDef);
                    trumbowyg.addBtnDef('alignMiddle',alignMiddleBtnDef);
                }
            }
        }
    });

})(jQuery);



/**
Line height custom, NE PAS UPGRADE !!
*/
(function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            en: {
                lineheight: 'Line height',
                lineheights: {
                    '0.4': 'Extra small',
                    '0.9': 'Small',
                    'normal': 'Regular',
                    '1.5': 'Large',
                    '2.0': 'Extra large'
                }
            },
            fr: {
                lineheight: 'Hauteur de ligne',
                lineheights: {
                    '0.5': 'Très petite',
                    '0.9': 'Petite',
                    'normal': 'Normale',
                    '1.5': 'Grande',
                    '2.0': 'Très grande'
                }
            },
        }
    });

    var defaultOptions = {
        sizeList: [
            '0.5',
            '0.9',
            'normal',
            '1.5',
            '2.0'
        ]
    };

    $.extend(true, $.trumbowyg, {
        plugins: {
            lineheight: {
                init: function (trumbowyg) {
                    trumbowyg.o.plugins.lineheight = $.extend({},
                      defaultOptions,
                      trumbowyg.o.plugins.lineheight || {}
                    );

                    trumbowyg.addBtnDef('lineheight', {
                        dropdown: buildDropdown(trumbowyg)
                    });
                }
            }
        }
    });

    // Build the dropdown
    function buildDropdown(trumbowyg) {
        var dropdown = [];

        $.each(trumbowyg.o.plugins.lineheight.sizeList, function(index, size) {
            trumbowyg.addBtnDef('lineheight_' + size, {
                text: trumbowyg.lang.lineheights[size] || size,
                hasIcon: false,
                fn: function(){
                    trumbowyg.saveRange();
                    var text = trumbowyg.getRangeText();
                    if (text.replace(/\s/g, '') !== '') {

                        var parent = trumbowyg.doc.getSelection().getRangeAt(0).commonAncestorContainer;
                        $(parent).css('lineHeight', size);
                        try {
                            var parent = trumbowyg.doc.getSelection().getRangeAt(0).commonAncestorContainer;
                            $(parent).css('lineHeight', size);
                        } catch (e) {
                        }
                    }
                }
            });
            dropdown.push('lineheight_' + size);
        });

        return dropdown;
    }

})(jQuery);