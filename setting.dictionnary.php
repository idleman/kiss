<?php 
global $myUser;
User::check_access('dictionary','configure');
?>
<div class="row">
	<div class="col-md-12">
		<br>
		<h3>Réglages Listes</h3>
		<hr/>
		<div class="panel panel-default lists-settings">
			<div class="form-inline">
				<div class="lists-navigation-block">
					<div id="prev-button" class="btn btn-info hidden" data-tooltip title="Revenir à la liste parente" onclick="previous_list_dictionary(this)">
						<i class="fas fa-angle-left pointer noPrint"></i>
					</div>
					<span>Voir les éléments de la liste &nbsp;&nbsp;</span>
					<select id="parent" class="form-control lists-selector" onchange="core_dictionary_search();">
						<span class="hidden"><option value="{{id}}" class="hidden">{{label}}</option></span>
						<option value="" selected> - </option>
						<?php foreach(Dictionary::loadAll(array('parent'=>0, 'state'=>Dictionary::ACTIVE), array('id')) as $list): ?>
							<option value="<?php echo $list->id; ?>"><?php echo $list->label; ?></option>
						<?php endforeach; ?>
					</select>

					<?php if($myUser->superadmin == 1): ?>
						<code>Pour ajouter une liste racine, ne rien sélectionner dans cette liste</code><br/>
					<?php endif; ?>
				</div>
			</div>
			<br/>
			<table id="dictionnaries" class="table table-striped table-dictionaries">
				<thead class="bg-secondary text-light">
					<tr>
						<th class="py-2 text-right" style="width: 5%;">#</th>
						<th class="py-2">Valeur</th>
						<?php if($myUser->superadmin == 1): ?>
							<th class="py-2" style="width: 15%;">Slug</th>
							<th class="py-2" style="width: 30%;">Libellé de sous-liste</th>
						<?php endif; ?>
						<th class="py-2" style="width: 150px;"></th>
					</tr>
					<tr id="dictionaryForm" data-action="core_dictionary_save">
						<th class="py-2"></th>
						<th class="py-2"><input id="label" class="form-control form-control-sm" placeholder="Valeur" type="text"></th>
						<?php if($myUser->superadmin == 1): ?>
							<th class="py-2"><input id="slug" class="form-control form-control-sm" placeholder="slug" type="text"></th>
							<th class="py-2"><input id="sublistlabel" class="form-control form-control-sm" placeholder="Libellé de sous-liste" type="text"></th>
						<?php endif; ?>
						<th class="py-2 text-center"><div onclick="core_dictionary_save();" class="btn btn-small btn-success" data-tooltip title="Enregistrer"><i class="fas fa-check"></i></div></th>
					</tr>
				</thead>
				<tbody>
					<tr data-id="{{id}}" data-parent="{{parent}}" class="hidden">
						<td class="text-right">{{id}}</td>
						<td class="itemLabel">{{label}}</td>
						<?php if($myUser->superadmin == 1): ?>
							<td>{{slug}}</td>
							<td>{{sublistlabel}}</td>
						<?php endif; ?>
						<td class="text-center">
							<div onclick="add_sub_dictionary(this)" class="btn btn-success btn-squarred btn-mini" title="Consulter les sous-listes"><i class="fas fa-search-plus"></i></div>
							<div onclick="core_dictionary_edit(this)" class="btn btn-info btn-squarred btn-mini" title="Éditer la liste"><i class="fas fa-pencil-alt"></i></div>
							<div onclick="core_dictionary_delete(this)" class="btn btn-danger btn-squarred btn-mini" title="Supprimer la liste et ses enfants"><i class="fas fa-times"></i></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>