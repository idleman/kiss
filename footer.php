
</div>

<?php 
global $myUser,$conf,$myFirm;
$scheme = isset($scheme) ? $scheme : define_url_scheme();
$mediaRoot = isset($mediaRoot) ? $mediaRoot : define_media_root();
$loadingTime = isset($start_time) ? (round(microtime(TRUE) - $start_time,5)) : '';
$cacheVersion = isset($cacheVersion) ? $cacheVersion : SOURCE_VERSION;

$footerMenu = array();
Plugin::callHook("menu_footer", array(&$footerMenu));
uasort ($footerMenu , function($a,$b){return $a['sort']>$b['sort']?1:-1;});
?>
<?php if(!empty($footerMenu)): ?>
	<div class="footer-spacing"></div>
<?php endif; ?>
	<footer class="footer noPrint">
		<div class="container-fluid py-0 text-center">
			<span class="text-muted">
			<?php 
				echo $conf->get('show_application_name_footer') ? PROGRAM_NAME.' V'.SOURCE_VERSION.' ' : '';
				if($conf->get('show_application_author_footer')){
					if(!empty($conf->get('application_author_website_footer')))
						echo 'by <a href="'.$conf->get('application_author_website_footer').'" target="_blank">@'.PROGRAM_AUTHOR.'</a>';
					else
						echo 'by @'.PROGRAM_AUTHOR;
				}				
				echo !empty($conf->get('show_application_documentation_footer')) && file_exists('file/guide/'.$conf->get('show_application_documentation_footer')) && $myUser->connected() ? ' | <a href="file/guide/'.$conf->get('show_application_documentation_footer').'" target="_blank"><i class="far fa-file-pdf"></i> Documentation</a>' : '';
				echo $conf->get('show_process_time_footer') ? ' - '.$loadingTime : '';
			?>
			</span>
			<?php if(!empty($footerMenu)): ?>
			<ul class="footer-menu">
				<li>
				<?php foreach($footerMenu as $item):
					if(isset($item['custom'])):
						echo $item['custom'];
					else: ?>
					<a class="footer-menu-item mx-4" href="<?php echo $item['url']; ?>"
						target="<?php echo isset($item['target'])?$item['target']:''; ?>" >
						<?php echo (isset($item['icon'])?'<i class="'.$item['icon'].'"></i> ':'').$item['label']; ?>
					</a>
				<?php endif; ?>
			<?php endforeach; ?>
				</li>
			</ul>
			<?php endif; ?>
		</div>
		<div id="scroll-top" title="Retour en haut de page"></div>
	</footer>

	<!-- Composant filtre -->
	<div class="advanced-search-box hidden">
		
			<div class="input-group simple-search">
				<div class="input-group-prepend d-none d-md-flex">
					<div class="input-group-text data-search-label">Recherche</div>
				</div>
				<input type="text" class="form-control filter-keyword" autocomplete="off" placeholder="Mot clé">
				<span id="search-clear" class="hidden"><i class="fas fa-times my-auto ml-0"></i></span>
				<div class="input-group-append hidden">
					<div class="btn btn-info btn-search" title="Rechercher"><i class="fas fa-search mr-1"></i><span class="d-none d-md-inline">Rechercher</span></div>
				</div>
			</div>
	
		<div class="advanced-search">
		    <ul class="criterias group">
	            <li class="condition hidden">
	                <span class="filter-column">
	                	<select class="form-control">
	                	{{#columns}}
	                		<option value="{{value}}" data-filter-type="{{type}}">{{label}}</option>
	                	{{/columns}}
	                </select>
	                </span> 
	                <span class="filter-subcolumn"></span> 
	                <span class="filter-operator"></span> 
	                <span class="filter-value"></span> 
	                <span class="filter-option">
	                	<i title="Déplacer la ligne" class="fas fa-arrows-alt btn-move d-none d-md-inline-block"></i>
	                    <i title="Déplacer dans le groupe supérieur" class="btn-unindent fas fa-angle-left"></i>
	                    <i title="Déplacer dans un sous-groupe" class="btn-indent fas fa-angle-right"></i> 
	                    <i title="Supprimer la ligne" class="btn-delete far fa-trash-alt"></i>
	                    <i title="Ajouter une ligne après" class="btn-add fas fa-plus"></i>
	                </span>
	                <select class="filter-join"><option value="and">ET</option><option value="or">OU</option></select>
	            </li>
		    </ul>
		</div>
		<div class="options">
	    	<div class="btn btn-light btn-small btn-search"><i class="fas fa-search mr-1"></i> <span class="d-none d-md-inline">Rechercher</span></div>
    	    <div class="btn btn-light btn-small advanced-button-search pl-2 pointer" title="Mode simple / avancé"><i class="fas fa-filter fa-fw"></i></div>
	        <div class="dropdown preferences btn-light">
	            <button class="btn btn-light btn-small pr-1 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                <i class="fas fa-user-cog fa-fw"></i>
	            </button>
	            <div class="dropdown-menu tools py-1" aria-labelledby="dropdownMenuButton">
	                <a class="dropdown-item py-1 px-2 text-success hidden btn-search-save pointer">
	                	<span><i class="far fa-save fa-fw"></i> Enregistrer la recherche</span>
	                	<div class="input-group input-group-sm hidden">
	                		<input type="text" placeholder="Libellé" class="form-control search-save-label">
	                		<div class=" input-group-append">
	                			<div class="btn btn-small btn-success btn-search-save-submit"><i class="fas fa-check"></i></div>
	                		</div>
	                	</div>
	                </a>
	                <a class="dropdown-item py-1 px-2 text-muted btn-search-clean pointer"><i class="fas fa-broom fa-fw"></i> Nettoyer la recherche</a>
	                <div class="saved-search-container hidden">
		                <div class="dropdown-divider my-2"></div>
		                <h6 class="dropdown-header text-left font-weight-bold search-save-divider py-1 px-2 text-uppercase">Recherches :</h6>
		                <template class="filter-saved-search">
		                	<a class="dropdown-item py-1 py-1 pl-2 pr-1 pointer text-left btn-search-load" data-uid="{{uid}}" data-global="{{global}}">
		                		{{label}}
			                	<i class="fas fa-trash-alt fa-fw float-right text-danger mt-1 btn-search-delete" title="Supprimer"></i>
			                	<i class="far fa-eye fa-fw float-right text-muted mt-1 mr-1 btn-search-global" title="Afficher pour tous les utilisateurs"></i>
			                	<div class="clear"></div>
			                </a>
		                </template>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="clear"></div>
	</div>

	<div class="hidden">

		<?php 
		$fieldTypes = FieldType::available();
		foreach($fieldTypes as $fieldType): ?>
			<div class="available-field-type" data-field-type="<?php echo $fieldType->slug; ?>"><?php 
			$m = $fieldType->onInput;
			$type = isset($field['type']) && isset($fieldTypes[$field['type']]) ? $fieldTypes[$field['type']] : $fieldTypes['text'];

			$attributes = $fieldType->default_attributes;
			if(!isset($attributes['class'])) $attributes['class'] = '""';

			$attributes['class'] = preg_replace('/"$/i',' filter-value"',$attributes['class']);

			if(isset($attributes['data-type'])){
				$attributes['data-template'] = $attributes['data-type'];
				unset($attributes['data-type']);
			}

			if(isset($fieldType->filter['attributes']))
				$attributes = array_merge($attributes,$fieldType->filter['attributes']);

			echo $m(array('attributes'=>$attributes),array('type'=>$type,'label'=>'')); 
		?></div>
		<?php endforeach; ?>
		?>
	


		<?php foreach($fieldTypes as $fieldType): 
			if(!property_exists($fieldType,'filter')) continue;
			global $databases_credentials;
       		$connector = $databases_credentials['local']['connector'];
			$operators = $connector::operators();


			$selector = '';
			//sélecteurs spéciaux (eg : dictionary doit prendre uniquement la derniere filter-value car peut être a depth ++)
			if(isset($fieldType->filter['attributes']) && isset($fieldType->filter['attributes']['data-value-selector'])){
				$selector = "data-value-selector=".$fieldType->filter['attributes']['data-value-selector'];
			}
		?>
		<div class="filter-value-block" data-value-type="<?php echo $fieldType->slug; ?>"  <?php echo $selector ?> >
			<select class="form-control filter-operator border-0 text-primary">
				<?php foreach($fieldType->filter['operators'] as $operator=>$view): 
					$operator = $operators[$operator];
					$valuesNumber = isset($operator['values']) ? $operator['values'] : 1;
					if(count($view)==0) $valuesNumber = 0;

					
				?>
				<option 
					data-default-view="<?php echo base64_encode(json_encode($view)); ?>" <?php echo isset($operator['values'])?' data-values="'.$valuesNumber.'" ':''; ?> 
					value="<?php echo $operator['slug']; ?>"><?php echo $operator['label']; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<?php endforeach; ?>
	</div>

	<!-- Composant icone -->
	<div class="dropdown component-icon hidden">
		<button class="btn btn-primary dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		 	<i class="{{value}}"></i>
		</button>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<span class="dropdown-item py-0">
		  		<input type="text" placeholder="Mot clé" class="form-control form-control-sm w-100">
			</span>
			<div class="dropdown-divider"></div>
			<div class="icons-list">
		  		<span class="dropdown-icon-item no-icon" title="Pas d'icône" data-icon="hidden"><i class="far fa-eye-slash"></i></span>
		  		{{#choices}}{{#.}}<span class="dropdown-icon-item" title="{{.}}" data-icon="{{.}}"><i class="fa-fw {{.}}"></i></span>{{/.}}{{/choices}}
		  	</div>
		</div>
	</div>

	<!-- Composant quickform -->
	<div class="modal fade quickform-modal" id="quickform-modal" tabindex="-1" role="dialog" aria-labelledby="quickform-modal-label" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-info text-light">
					<h5 class="modal-title" id="quickform-modal-label"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<div class="btn btn-light" data-dismiss="modal">Fermer</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Bloc préférence de pagination -->
	<template id="pagination-preference-template">
	    <div class="dropdown d-inline-block pagination-preference">
	    	<button class="btn btn-light bg-white dropdown-toggle btn-sm"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{initial}}</button>
	    	<div class="dropdown-menu p-0 w-100" aria-labelledby="dropdownMenuButton">
    			{{#items}}<div class="dropdown-item pointer text-center">{{number}}</div>{{/items}}
	    	</div>
	    </div>
    </template>

	<!-- Composant file -->
	<div id="component-file-template" class="hidden">
		<div class="upload-header">
			<div class="data-type-file">
			<form method="post" action="" enctype="multipart/form-data">
				<input type="file" name="document[]" multiple="" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden; position: absolute;z-index: -1">
			</form>
			<div class="pointer text-center upload-zone"><span class="file-upload-label">Faites glisser vos documents ici</span></div>
			</div>
		</div>

		<ul class="upload-list">
			<li data-path="{{path}}" data-label="{{label}}" data-icon="{{icon}}" data-url="{{url}}">
				<div class="file-info">
					{{#url}}<a href="{{url}}" class="file-link">{{/url}}
						{{#preview}}<div class="file-preview" data-preview="{{preview}}"></div>{{/preview}}{{#icon}}<i class="{{icon}} file-icon"></i>{{/icon}}<span class="file-label">{{label}}</span>{{#readableSize}}<span class="file-size">{{readableSize}}</span>{{/readableSize}}
					{{#url}}</a>{{/url}}
					<span class="btn-bar">
						<span class="file-button"></span>
						<span class="btn-delete pointer"><i class="far fa-trash-alt"></i></span> 
					</span>
				</div>
				<div class="progress-bar"></div>
				<div class="error-message"></div>
			</li>
		</ul>
	</div>

	<!-- Panel historique -->
	<div class="history-panel fold hidden">
	    <h3 class="text-muted text-center text-uppercase">Historique</h3>
	    <template id="history-notification-tpl">
	    	<span class="badge badge-pill badge-danger history-notification" data-tooltip title="{{number}} message(s) important(s)">{{number}}</span>
	    </template>
	    <i class="btn-close fa-fw text-danger far fa-times-circle" title="Fermer le panneau" data-tooltip data-placement="left" onclick="$(this).parent().addClass('fold');"></i>
	    <div class="comment-search input-group input-group-sm ">
			<div class="input-group-prepend">
				<span class="input-group-text text-muted bg-light">Rechercher </span>
			</div>
			<input type="text" class="comment-keyword form-control">
			<div class="input-group-append">
				<div class="btn text-muted btn-light border" onclick="core_history_search()"><i class="fas fa-search"></i></div>
			</div>
	    </div>
	    <div class="history-add mb-2" data-tooltip   title="Ajouter un commentaire ici..." onclick="history_add()"><i class="far fa-comment-dots"></i></div>

	    <div class="comments-loader text-center p-3"><small class="text-muted"><i class="fas fa-circle-notch fa-spin-fast"></i> Chargement en cours....</small></div>
	    
	    <ul class="comments">
	        <li class="hidden comment" data-id="{{id}}" data-type="{{type.slug}}" data-importance="{{importance}}" data-sort="{{sort}}">
	            <div>
	                <div class="history-type d-inline-block"><i class="{{type.icon}}" style="color:{{type.color}};"></i></div>
	                <div class="history-header d-inline-block">le {{created.fullname}} ({{created.time}})<br> par {{creator.fullname}}</div>
	            </div>
	            <div class="history-content w-100" ondblclick="history_edit($(this).closest('.comment'))">{{{comment}}}</div>
	            <div class="history-importance">
	            	<input type="checkbox" data-type="checkbox" data-tooltip title="Important" onclick="core_history_importance_save(this)"/>
	         	</div>
	            <ul class="history-options">
	                <li data-tooltip title="Supprimer" class="pointer history-delete-btn" onclick="core_history_delete(this)"><i class="far fa-trash-alt"></i></li>
	            </ul>
	            <div class="history-add" data-tooltip title="Ajouter un commentaire ici..." onclick="history_add(this)">
	            	<i class="far fa-comment-dots"></i>
	            </div>
	        </li>
	    </ul>
	</div>

	<!-- Composant dictionary table -->
	<div class="hidden component-dictionary-table" data-id="" data-type="dictionary-table"  data-dictionary="{{parent.id}}">
	    <table id="" class="table table-striped table-bordered">
	        <thead class="bg-secondary text-light">
	            <tr>
	                <th colspan="<?php echo $myUser->superadmin ? 3 : 2; ?>" class="bg-secondary text-light py-2 align-middle"><h4 class="m-0"><i class="fas fa-angle-right"></i> {{parent.label}}</h4></th>
	            </tr>
	            <tr class="edit-line">
	                <th><input class="form-control form-control-sm edit-field list-label" type="text" placeholder="Libellé de la liste"></th>
	                <?php if($myUser->superadmin): ?>
	                <th><input class="form-control form-control-sm edit-field list-slug" type="text" placeholder="Slug de la liste"></th>
	                <?php endif; ?>
	                <th class="action-cell text-center"><div class="btn btn-small btn-success w-100" data-tooltip title="Ajouter un élément de liste"><i class="fas fa-check"></i></div></th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr data-id="{{id}}" class="hidden">
	                <td>{{label}}</td>
	                <?php if($myUser->superadmin): ?>
	                <td>{{slug}}</td>
	                <?php endif; ?>
	                <td class="action-cell text-center">
	                    <span class="btn btn-info btn-squarred btn-mini btn-edit" title="Éditer l'élément de liste"><i class="fas fa-pencil-alt"></i></span>
	                    <span class="btn btn-danger btn-squarred btn-mini" title="Supprimer l'élément de liste"><i class="fas fa-times"></i></span>
	                </td>
	            </tr>
	        </tbody>
	    </table>
	</div>

	<!-- Permission modal -->
	<div class="modal fade" id="permission-modal" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="permission-modal-title">Gestion des droits</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<!-- search results -->
						<div class="col-xl-12">
							<table id="permissions" class="table table-striped" data-entity-search="core_permission_right_search">
								<thead class="permission-firm-block">
					                <tr>
					                    <th class="col-target align-middle">Etablissement concerné</th>
					                    <th colspan="6" class="col-read">
					                    	<select id="permission-firm" class="form-control form-control-sm w-100" onchange="core_permission_right_search()"></select>
					                    </th>
										
					                  
					                </tr>
					            </thead>
					            <thead>
					                <tr>
					                    <th class="col-target">Utilisateur/Rang</th>
					                    <th class="col-read"><span class="label-diagonal">Lecture</span></th>
										<th class="col-edit"><span class="label-diagonal">Ecriture</span></th>
										<th class="col-delete"><span class="label-diagonal">Suppr.</span></th>
					                    <th class="col-configure"><span class="label-diagonal">Config.</span></th>
					                    <th class="col-recursive"><span class="label-diagonal">Récursif</span></th>
					                    <th></th>
					                </tr>
					            </thead>
					            <thead>
					                <tr id="permission-form" data-action="core_right_save" data-id="">
					                    <th class="col-target"><input id="target" class="form-control form-control-sm" placeholder="" value="" data-type="user" data-types="user,rank" type="text"></th>
					                    <th class="col-read"><input id="read" name="read" type="checkbox" data-type="checkbox"></th>
					                    <th class="col-edit"><input id="edit" name="edit" type="checkbox" data-type="checkbox"></th>
					                    <th class="col-delete"><input id="delete" name="delete" type="checkbox" data-type="checkbox"></th>
					                    <th class="col-configure text-center"><input id="configure" name="configure" type="checkbox" data-type="checkbox"></th>
					                    <th class="col-recursive text-center"><input id="recursive" name="recursive" type="checkbox" data-type="checkbox"></th>
					                    <th class="text-right"><div onclick="core_right_save();" class="btn btn-success btn-mini"><i class="fas fa-check"></i></div></th>
					                </tr>
					            </thead>
					            <tbody>
					                <tr data-id="{{id}}" class="hidden {{^firm}}permission-firm-all{{/firm}}">
						                <td class="col-target">{{{target}}}</td>
						                <td class="col-read">{{#read}}<i class="fas fa-check text-success"></i>{{/read}}{{^read}}<i class="fas fa-times text-danger"></i>{{/read}}</td>
						                <td class="col-edit">{{#edit}}<i class="fas fa-check text-success"></i>{{/edit}}{{^edit}}<i class="fas fa-times text-danger"></i>{{/edit}}</td>
						                <td class="col-delete">{{#delete}}<i class="fas fa-check text-success"></i>{{/delete}}{{^delete}}<i class="fas fa-times text-danger"></i>{{/delete}}</td>
						                <td class="col-configure">{{#configure}}<i class="fas fa-check text-success"></i>{{/configure}}{{^configure}}<i class="fas fa-times text-danger"></i>{{/configure}}</td>
						                <td class="col-recursive">{{#recursive}}<i class="fas fa-check text-success"></i>{{/recursive}}{{^recursive}}<i class="fas fa-times text-danger"></i>{{/recursive}}</td>
						                <td class="text-right">
				                            <div class="btn btn-delete text-danger btn-mini" onclick="core_right_delete(this);" title="Supprimer cette permission"><i class="far fa-trash-alt"></i></div>
				                            <div class="btn btn-delete-info text-muted btn-mini" title="Cette permission est générale a tous les établissement et ne peut être supprimé qu'en sélectionnant l'établissement 'Tous'"><i class="far fa-question-circle"></i></div>
						                </td>
					                </tr>
					           </tbody>
					        </table>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>

	<?php echo Plugin::callHook('application_bottom'); ?>
	<!-- Bootstrap core JavaScript -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?php if($conf->get('offline_mode')): ?>
		<script src="<?php echo $mediaRoot ?>/js/vendor/jquery.min.js"></script>
	<?php else: ?>
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="<?php echo $mediaRoot ?>/js/vendor/jquery.min.js"><\/script>');</script>
	<?php endif; ?>

	<!-- For fontawesome 5 pseudo-elements -->
	<script> window.FontAwesomeConfig = {searchPseudoElements: true}</script>
	<!-- lib js -->
	<script src="<?php echo $mediaRoot ?>/js/vendor/popper.min.js"></script>
	
	<script src="<?php echo $mediaRoot ?>/js/vendor/mustache.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/jquery-ui.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/jquery.timepicker.js"></script>
	<!-- prevent jquery ui / boostrap conflit on $.tooltip() -->
	<script>$.widget.bridge('uitooltip', $.ui.tooltip);</script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/bootstrap.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/bootstrap3-typeahead.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/trumbowyg.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/trumbowyg.plugins.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/moment.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/leaflet.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/leaflet.markercluster.js"></script>
	<!-- custom js -->
	<script src="<?php echo $mediaRoot ?>/js/fontawesome.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/vendor/Chart.min.js"></script>
	<script src="<?php echo $mediaRoot ?>/js/plugins.js?v=<?php echo $cacheVersion ?>"></script>
	<script src="<?php echo $mediaRoot ?>/js/filter.component.js?v=<?php echo $cacheVersion ?>"></script>
	<script src="<?php echo $mediaRoot ?>/js/component.js?v=<?php echo $cacheVersion ?>"></script>
	<script src="<?php echo $mediaRoot ?>/js/main.js?v=<?php echo $cacheVersion ?>"></script>
	<?php echo Plugin::callJs($mediaRoot,$cacheVersion); ?>
	</body>
</html>
